<apex:page standardStylesheets="false" showHeader="false" sidebar="false" applyBodyTag="false" controller="dsmtdsmtAirFlowInfoController" action="{!validateRequest}">
    <style>
        .fw-normal{
            font-weight: normal;
        }
    </style>
    
    <apex:composition template="dsmtEnergyAssessmentTemplate">
        <apex:define name="header-action-buttons">
            <button type="button" class="btn btn-primary pull-right continue-bt" onclick="saveAirFlowAirLeakageDetails();">
                <i class="glyphicon glyphicon-floppy-disk"></i>
                Save
            </button>
            <!--
            <button type="button" class="btn btn-primary pull-right continue-bt" onclick="$('#notes').slideDown();">
                <i class="glyphicon glyphicon-plus"></i> 
                Add Note
            </button>
            <button type="button" class="btn btn-primary pull-right continue-bt" onclick="$('#attachments').slideDown();" >
                <i class="glyphicon glyphicon-plus"></i>
                Add Attachment
            </button>
            <button type="button" class="btn btn-primary pull-right continue-bt" onclick="$('#preDiffReadings').slideDown();">
                <i class="glyphicon glyphicon-plus"></i>
                Add Pressure Differential Reading
            </button>
            <button type="button" class="btn btn-primary pull-right continue-bt" onclick="$('#blowerDoorReadings').slideDown();">
                <i class="glyphicon glyphicon-plus"></i>
                Add Blower Door Reading
            </button>
            -->
            <button type="button" class="btn btn-primary pull-right continue-bt" onclick="window.scrollTo(0, 0); $('#recomContainerDiv').slideDown();">
                <i class="glyphicon glyphicon-plus"></i>
                Add Recommendation
            </button>
        </apex:define>
        
        <apex:define name="body-content">
            <apex:actionFunction name="saveAirFlowAirLeakageDetails" action="{!saveAirFlowAirLeakageDetail}" status="actionStatusLoader" reRender="airFlowAirLieakageDetailsPanel, blowerDoorReadingDetailsPanel,pressureDifferentialReadingDetailsPanel" oncomplete="initDecimals(); initBootstrapSwitch(); gotoTabURL();" />
           
            <apex:outputPanel id="airFlowAirLieakageDetailsPanel" layout="block" styleClass="col-md-12">
                <div class="row">
                    <c:dsmtRecommendationComponent parentObjId="{!afal.Id}" parentObjField="Air_Flow_and_Air_Leakage__c"/>
                    
                    <div class="panel panel-primary" style="border: none; box-shadow: none;">
                        <div class="panel-body">
                            <div class="col-md-3">
                                <label>Leakiness</label>
                                <apex:inputField value="{!afal.Leakiness__c}" id="leakinessSelect" styleClass="form-control Leakiness__c"/>
                                <script>
                                    $("select[id$='leakinessSelect'] option[value='']").remove();
                                </script>
                            </div>
                            <div class="col-md-2">
                                <label>CFM50</label>
                                <apex:inputText value="{!afal.CFM50__c}" styleClass="form-control CFM50__c double number-input-field--5-0"/>
                                <apex:inputHidden id="OriginalCFM50__c" value="{!afal.CFM50__c}"/>
                                <apex:inputHidden id="ActualCFM50__c" value="{!afal.ActualCFM50__c}"/>
                                <apex:inputHidden id="ActualLeakiness__c" value="{!afal.ActualLeakiness__c}"/>
                                
                                <script>
                                    (function(){
                                        $('.CFM50__c').unbind('change');
                                        $(".CFM50__c").change(function(){
                                            $("[id$='ActualCFM50__c']").val($(this).val());
                                            $("[id$='OriginalCFM50__c']").val($(this).val());
                                        });
                                        
                                        $('.Leakiness__c').unbind('change');
                                        $(".Leakiness__c").change(function(){
                                            $("[id$='ActualLeakiness__c']").val($(this).val());
                                        });
                                            
                                        var CFM50__c = $('.CFM50__c').val();
                                        $('.CFM50__c').val(parseFloat(CFM50__c).toFixed(0));
                                    })();
                                </script>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-3">
                                <label>Building Tightness Limit <span id="btl">  
                                    <apex:outputText value="{0, number, ###,###,###,###}"> <apex:param value="{!afal.TightnessLimit__c}"/> </apex:outputText>
                                </span> </label>
                            </div>
                            <div class="col-md-4">
                                <label>Mechanical Ventilation Required CFM <span id="mvrc">  
                                    <apex:outputText value="{0, number, ###,###,###,###}"> <apex:param value="{!afal.MechanicalVentilationSystemsRequiredCFM__c}"/> </apex:outputText>
                                </span> </label>
                            </div>
                        </div>
                    </div>
                </div>
            </apex:outputPanel>
            <div class="clearfix"></div>
        
            <apex:actionFunction name="saveBlowerDoorReading_{!afal.Id}" action="{!saveBlowerDoorReading}" status="actionStatusLoader" reRender="blowerDoorReadingDetailsPanel" oncomplete="initDecimals(); initDates();initBootstrapSwitch();" />
            
            <apex:actionFunction name="deleteBlowerDoorReading" action="{!deleteBlowerDoorReading}" status="actionStatusLoader" reRender="blowerDoorReadingDetailsPanel" oncomplete="initDecimals(); initDates();initBootstrapSwitch();">
                <apex:param name="blowerDoorReadingId" id="blowerDoorReadingId" value=""/>
            </apex:actionFunction>
           
            <apex:actionFunction name="editBlowerDoorReading" action="{!editBlowerDoorReading}" status="actionStatusLoader" reRender="blowerDoorReadingDetailsPanel" oncomplete="initDecimals(); initDates();initBootstrapSwitch();$('#blowerDoorReadings').slideDown();" >
                <apex:param name="blowerDoorReadingIdToEdit" id="blowerDoorReadingIdToEdit" value=""/>
            </apex:actionFunction>
            
            <apex:outputPanel id="blowerDoorReadingDetailsPanel" layout="block" styleClass="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Blower Door Readings <span id="bdrHeading" class="fw-normal">(No calculated reduction yet)</span>
                        
                        <i class="glyphicon glyphicon-plus-sign pull-right" data-toggle="tooltip" onclick="$('#blowerDoorReadings').slideDown();" style="margin: -4px -5px 0 5px; font-size: 26px; color: #fff;" title="Add Blower Door Reading"></i>
                    </div>
                    
                    <apex:outputPanel id="newBlowerDoorReadingDetailsPanel" layout="block" styleClass="panel-body">
                        <div class="row input-section" id="blowerDoorReadings">
                            <apex:inputHidden value="{!newBlowerDoorReading.Id}" id="newBlowerDoorReadingId" />
                            
                            <div class="col-md-3">
                                <label>Date</label>
                                <apex:inputText value="{!newBlowerDoorReading.Blower_Door_Test_Date__c}" styleClass="form-control datetime"/>
                            </div>
                            <div class="col-md-3">
                                <label>Starting CFM50</label>
                                <apex:inputText value="{!newBlowerDoorReading.Starting_CFM50__c}" styleClass="form-control Starting_CFM50__c number-input-field--5-2"/>
                            </div>
                            <div class="col-md-3">
                                <label>Ending CFM50</label>
                                <apex:inputText value="{!newBlowerDoorReading.Ending_CFM50__c}" styleClass="form-control Ending_CFM50__c number-input-field--5-2"/>
                            </div>
                            <div class="col-md-3">
                                <label>Work done</label>
                                <apex:selectList value="{!newBlowerDoorReading.Recommendation__c}" styleClass="form-control" size="1" style="height: 34px;">
                                    <apex:selectOptions value="{!recommendationsSelectOptions}"></apex:selectOptions>
                                </apex:selectList>
                            </div>
                            
                            <div class="col-md-12">
                                <label>Notes</label>
                                <apex:inputField value="{!newBlowerDoorReading.Notes__c}" styleClass="form-control"/>
                            </div>
                            
                            <div class="col-md-12">
                                <input type="button" class="btn btn-primary pull-right continue-bt" value="Cancel" onclick="cancelRegion('#blowerDoorReadings');"/>
                                <input type="button" class="btn btn-primary pull-right continue-bt" value="Save" onclick="saveBlowerDoorReading_{!afal.Id}();"/>
                            </div>
                        </div>
                        
                        <apex:outputPanel layout="block" rendered="{!blowerDoorReadings.size > 0}">
                            <apex:variable value="{!0}" var="blowerDor"/>
                            <table class="tbl table_data" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <th width="20px">#</th>
                                        <th>Date</th>
                                        <th>Starting CFM50</th>
                                        <th>Ending CFM50</th>
                                        <th>Notes</th>
                                        <th>Recommendation</th>
                                        <th width="55px"></th>
                                    </tr>
                                    <apex:repeat value="{!blowerDoorReadings}" var="b">
                                        <tr>
                                            <td>
                                                <apex:variable value="{!blowerDor+1}" var="blowerDor"/>
                                                {!blowerDor}.
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Blower_Door_Test_Date__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Starting_CFM50__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Ending_CFM50__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Notes__c}"/>
                                            </td>
                                            <td>
                                                <apex:selectList value="{!b.Recommendation__c}" styleClass="form-control" size="1">
                                                    <apex:selectOptions value="{!recommendationsSelectOptions}"></apex:selectOptions>
                                                </apex:selectList>
                                            </td>
                                            <td>
                                                <i class="glyphicon glyphicon-edit" title="Edit" onclick="editBlowerDoorReading('{!b.Id}');"></i>
                                                <i class="glyphicon glyphicon-trash" title="Delete" onclick="if(confirm('Are you sure to delete ?'))deleteBlowerDoorReading('{!b.Id}');"></i>
                                            </td>
                                        </tr>
                                    </apex:repeat>
                                </tbody>
                            </table>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </div>
            </apex:outputPanel>
        
            <apex:actionFunction name="savePressureDifferentialReading" action="{!savePressureDifferentialReading}" status="actionStatusLoader" reRender="pressureDifferentialReadingDetailsPanel" oncomplete="initDecimals(); initDates();initBootstrapSwitch();" />
            
            <apex:actionFunction name="deletePressureDifferentialReading" action="{!deletePressureDifferentialReading}" status="actionStatusLoader" reRender="pressureDifferentialReadingDetailsPanel" oncomplete="initDecimals(); initDates();initBootstrapSwitch();">
                <apex:param name="pressureDifferentialReadingId" id="pressureDifferentialReadingId" value=""/>
            </apex:actionFunction>
            
            <apex:actionFunction name="editPressureDifferentialReading" action="{!editPressureDifferentialReading}" status="actionStatusLoader" reRender="pressureDifferentialReadingDetailsPanel" oncomplete="initDecimals(); initDates();initBootstrapSwitch();$('#preDiffReadings').slideDown();">
                <apex:param name="pressureDifferentialReadingIdToEdit" id="pressureDifferentialReadingIdToEdit" value=""/>
            </apex:actionFunction>
            
            <apex:outputPanel id="pressureDifferentialReadingDetailsPanel" layout="block" styleClass="col-md-12">
                <apex:pageMessages escape="false"></apex:pageMessages>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Pressure Differential Readings <span id="pdrHeading" class="fw-normal"></span>
                        <i class="glyphicon glyphicon-plus-sign pull-right" data-toggle="tooltip" onclick="$('#preDiffReadings').slideDown();" style="margin: -4px -5px 0 5px; font-size: 26px; color: #fff;" title="Add Pressure Differential Reading"></i>
                    </div>
                    
                    <apex:outputPanel id="newPressureDifferentialReadingDetailsPanel" layout="block" styleClass="panel-body">
                        <div class="row input-section" id="preDiffReadings">
                            <apex:inputHidden value="{!newPressureDifferentialReading.Id}" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-md-4">
                                        <label>Date Time</label>
                                        <apex:inputText value="{!newPressureDifferentialReading.Date__c}" styleClass="form-control datetime Date__c"/>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Location</label>
                                        <apex:inputText value="{!newPressureDifferentialReading.Location__c}" styleClass="form-control Location__c"/>
                                    </div>
                                    <div class="col-md-4">
                                        <label>WRT</label>
                                        <apex:inputText value="{!newPressureDifferentialReading.WRT__c}" styleClass="form-control WRT__c"/>
                                    </div> 
                                </div>  
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        <label>Pre Retrofit Pressure</label>
                                        <apex:inputField value="{!newPressureDifferentialReading.Pre_Retrofit_Pressure__c}" styleClass="form-control double Pre_Retrofit_Pressure__c number-input-field--6-0"/>
                                    </div> 
                                    <div class="col-md-6">
                                        <label>Post Retrofit Pressure</label>
                                        <apex:inputField value="{!newPressureDifferentialReading.Post_Retrofit_Pressure__c}" styleClass="form-control double Post_Retrofit_Pressure__c number-input-field--6-0"/>
                                    </div> 
                                </div> 
                            </div>
                            <div class="col-md-12">
                                <label>Notes</label>
                                <apex:inputField value="{!newPressureDifferentialReading.Notes__c}" styleClass="form-control"/>
                            </div> 
                            <div class="col-md-12">
                                <input type="button" class="btn btn-primary pull-right continue-bt" value="Cancel" onclick="cancelRegion('#preDiffReadings');"/>
                                <input type="button" class="btn btn-primary pull-right continue-bt" value="Save" onclick="savePressureDifferentialReading();"/>
                            </div>
                        </div>
                        
                        <apex:outputPanel layout="block" rendered="{!pressureDifferentialReadings.size > 0}">
                            <apex:variable value="{!0}" var="presDiff"/>
                            <table class="tbl table_data" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <th width="20px">#</th>
                                        <th>Date</th>
                                        <th>Location</th>
                                        <th>WRT</th>
                                        <th>Pre Retrofit Pressure</th>
                                        <th>Post Retrofit Pressure</th>
                                        <th>Notes</th>
                                        <th>Recommendation</th>
                                        <th width="55px"></th>
                                    </tr>
                                    <apex:repeat value="{!pressureDifferentialReadings}" var="b">
                                        <tr>
                                            <td>
                                                <apex:variable value="{!presDiff+1}" var="presDiff"/>
                                                {!presDiff}.
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Date__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Location__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.WRT__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Pre_Retrofit_Pressure__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Post_Retrofit_Pressure__c}"/>
                                            </td>
                                            <td>
                                                <apex:outputField value="{!b.Notes__c}"/>
                                            </td>
                                            <td>
                                                <apex:selectList value="{!b.Recommendation__c}" styleClass="form-control" size="1">
                                                    <apex:selectOptions value="{!recommendationsSelectOptions}"></apex:selectOptions>
                                                </apex:selectList>
                                            </td>
                                            <td>
                                                <i class="glyphicon glyphicon-edit" title="Edit" onclick="editPressureDifferentialReading('{!b.Id}');"></i>
                                                <i class="glyphicon glyphicon-trash" title="Delete" onclick="if(confirm('Are you sure to delete ?'))deletePressureDifferentialReading('{!b.Id}');"></i>
                                            </td>
                                        </tr>
                                    </apex:repeat>
                                </tbody>
                            </table>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </div>
            </apex:outputPanel>
            
            <!--<apex:actionRegion id="recommendationDetailsRegion">
                <apex:actionFunction name="saveRecommendation" action="{!saveRecommendation}" status="actionStatusLoader" reRender="recommendationDetailsPanel,blowerDoorReadingDetailsPanel,pressureDifferentialReadingDetailsPanel" oncomplete="initDates();initBootstrapSwitch();" />
                <apex:actionFunction name="deleteRecommendation" action="{!deleteRecommendation}" status="actionStatusLoader" reRender="recommendationDetailsPanel,blowerDoorReadingDetailsPanel,pressureDifferentialReadingDetailsPanel" oncomplete="initDates();initBootstrapSwitch();">
                    <apex:param name="recommendationId" id="recommendationId" value=""/>
                </apex:actionFunction>
                <apex:actionFunction name="editRecommendation" action="{!editRecommendation}" status="actionStatusLoader" reRender="recommendationDetailsPanel,blowerDoorReadingDetailsPanel,pressureDifferentialReadingDetailsPanel" oncomplete="initDates();initBootstrapSwitch();$('#recommendations').slideDown();">
                    <apex:param name="recommendationIdToEdit" id="recommendationIdToEdit" value=""/>
                </apex:actionFunction>
                <apex:outputPanel id="recommendationDetailsPanel" layout="block" styleClass="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Recommendations
                            <button type="button" class="btn btn-primary pull-right continue-bt" style="margin-top: -5px; background: transparent; color: #777; border: 1px solid #ccc;" onclick="$('#recommendations').slideDown();">
                                <i class="glyphicon glyphicon-plus"></i>
                                Add Recommendation
                            </button>
                        </div>
                        <apex:outputPanel id="newRecommendationDetailsPanel" layout="block" styleClass="panel-body">
                            <div class="row input-section" id="recommendations">
                                <apex:inputHidden value="{!newRecommendation.Id}" />
                                <div class="col-md-3">
                                    <label>Recommendation</label>
                                    <apex:inputField value="{!newRecommendation.Recommendation__c}" styleClass="form-control"/>
                                </div>
                                <div class="col-md-1">
                                    <label>Quantity</label>
                                    <apex:inputField value="{!newRecommendation.Quantity__c}" styleClass="form-control number-input-field--4-0"/>
                                </div>
                                <div class="col-md-1">
                                    <label>Hours</label>
                                    <apex:inputField value="{!newRecommendation.Hours__c}" styleClass="form-control"/>
                                </div>   
                                <div class="col-md-2">
                                    <label>Status Code</label>
                                    <apex:inputField value="{!newRecommendation.Status__c}" styleClass="form-control"/>
                                </div>
                                <div class="col-md-5">
                                    <label><br/></label>
                                    <input type="button" class="btn btn-primary pull-right continue-bt" value="Cancel" onclick="cancelRegion('#recommendations');"/>
                                    <input type="button" class="btn btn-primary pull-right continue-bt" value="Save" onclick="saveRecommendation();"/>
                                </div> 
                                <div class="col-md-12">
                                    <label>User Description</label>
                                    <apex:inputField value="{!newRecommendation.User_Description__c}" styleClass="form-control"/>
                                </div> 
                            </div>
                            <apex:outputPanel layout="block" rendered="{!recommendations.size > 0}">
                                <apex:variable value="{!0}" var="recommen"/>
                                <table class="tbl table_data" style="width: 100%;">
                                    <tbody>
                                        <tr>
                                            <th width="20px">#</th>
                                            <th>Recommendation</th>
                                            <th>Quantity</th>
                                            <th>Hours</th>
                                            <th>Status Code</th>
                                            <th>User Description</th>
                                            <th width="55px"></th>
                                        </tr>
                                        <apex:repeat value="{!recommendations}" var="b">
                                            <tr>
                                                <td>
                                                    <apex:variable value="{!recommen+1}" var="recommen"/>
                                                    {!recommen}.
                                                </td>
                                                <td>
                                                    <apex:outputField value="{!b.Recommendation__c}"/>
                                                </td>
                                                <td>
                                                    <apex:outputField value="{!b.Quantity__c}"/>
                                                </td>
                                                <td>
                                                    <apex:outputField value="{!b.Hours__c}"/>
                                                </td>
                                                <td>
                                                    <apex:outputField value="{!b.Status__c}"/>
                                                </td>
                                                <td>
                                                    <apex:outputField value="{!b.User_Description__c}"/>
                                                </td>
                                                <td>
                                                    <i class="glyphicon glyphicon-edit" title="Edit" onclick="editRecommendation('{!b.Id}');"></i>
                                                    <i class="glyphicon glyphicon-trash" title="Delete" onclick="if(confirm('Are you sure to delete ?'))deleteRecommendation('{!b.Id}');"></i>
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </tbody>
                                </table>
                            </apex:outputPanel>
                        </apex:outputPanel>
                    </div>
                </apex:outputPanel>
            </apex:actionRegion>-->
            
            <div class="col-sm-12">
                <c:EANotesComponent parentObjId="{!afal.Id}"></c:EANotesComponent>
            </div>
            <div class=" clearfix"></div>
            
            <div class="col-sm-12">
                <c:dsmtAttachmentComponent parentObjId="{!afal.Id}"></c:dsmtAttachmentComponent>
            </div>
            <div class=" clearfix"></div>
            
            <div class="col-sm-12">
                <c:dsmtRecommendationListComponent parentObjId="{!$CurrentPage.parameters.assessId}" parentObjField="Energy_Assessment__c"/>
            </div>
            <div class=" clearfix"></div>
        </apex:define>
        
        <apex:define name="footer-scripts-content">
            <script>
                $("#accessorName").html('{!accessorName}');
                $("#auditNumber").html('{!ea.Name}');
                function initDates(){
                    //$('.date').datetimepicker();
                    
                    $('.Starting_CFM50__c').unbind('change');
                    $('.Starting_CFM50__c').change(function(){
                        $('.CFM50__c').val($('.Starting_CFM50__c').val());
                        $("[id$='ActualCFM50__c']").val($(this).val());
                        $("[id$='OriginalCFM50__c']").val($(this).val());
                    });
                }
                function init(){
                    initDecimals(); 
                    initDates();
                    
                    $('.Starting_CFM50__c, .Ending_CFM50__c').unbind('keyup');
                    $('.Starting_CFM50__c, .Ending_CFM50__c').keyup(function(){
                        var start = $('.Starting_CFM50__c').val().trim();
                        var end = $('.Ending_CFM50__c').val().trim();
                        
                        if(start == '' || end == '')
                            $('#bdrHeading').text('(No calculated reduction yet)');
                        else
                            $('#bdrHeading').text('('+(parseFloat(start) - parseFloat(end)) + ' CFM50 reduction)');
                    });
                    
                    $('.Location__c, .WRT__c, .Pre_Retrofit_Pressure__c, .Post_Retrofit_Pressure__c').unbind('keyup');
                    $('.Location__c, .WRT__c, .Pre_Retrofit_Pressure__c, .Post_Retrofit_Pressure__c').keyup(function(){
                        var location = $('.Location__c').val().trim();
                        var wrt = $('.WRT__c').val().trim();
                        var pre = $('.Pre_Retrofit_Pressure__c').val().trim();
                        var post = $('.Post_Retrofit_Pressure__c').val().trim();
                        
                        if(location == '' && wrt == '' && pre == '' && post == '')
                            $('#pdrHeading').text('');
                        else
                            $('#pdrHeading').text(location +', ' + wrt + ' : ' + pre + ' - ' + post);
                    });
                }
                $(document).ready(function(){
                    $('.Leakiness__c').focus();
                    init();
                    initBootstrapSwitch();
                });
                
                function calcuateCFM50()
                {
                    
                }
            </script>
        </apex:define>
    </apex:composition>
</apex:page>