@isTest
Public class proposal_recommendation_BIBU_Test{
    @isTest
    public static void runTest(){
        
        Proposal__c po=new Proposal__c();
        insert po;
        
     	 DSMTracker_Product__c dsmp=new DSMTracker_Product__c();
        dsmp.Utility_Incentive_Cap__c=null;
        dsmp.Added_Install_Fee__c='Yes';
   		 insert dsmp;
        
        Recommendation__c re=new Recommendation__c();
        re.DSMTracker_Product__c=dsmp.id;
        re.Change_Order_Quantity__c=null;
        re.Quantity__c=1;
        insert re;
        
      
        Proposal_Recommendation__c pr=new Proposal_Recommendation__c();
        pr.Recommendation__c=re.id;
        pr.Proposal__c=po.id;
        pr.Is_Installed__c='Yes';
        insert pr;
        
       
        
        
    }
}