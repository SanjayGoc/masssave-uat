@istest
public class dsmtProposalViewCntrlTest
{
    @istest
    public static void test()
    {
        Energy_Assessment__c ea=Datagenerator.setupAssessment();
        test.startTest();
        
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        proposal.Signature_Date__c = date.today();
        proposal.Moderate_Income__c = false;
        proposal.Whole_Home__c = true;
        insert proposal;
        
        test.stoptest();
        ApexPages.StandardController sc = new ApexPages.StandardController(proposal);
        dsmtProposalViewCntrl cntrl =new dsmtProposalViewCntrl(sc);
        
    }
}