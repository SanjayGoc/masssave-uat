@istest
public class dsmtSignAndEmailDocumentControllerTest
{
  @istest
    static void dsmtSignAndEmailDocumentControllertest()
    {
        Energy_Assessment__c ea  =Datagenerator.setupAssessment();
        Attachment__c att =new Attachment__c();
        att.Status__c='UnSubmitted';
        insert att;
        
        test.startTest();
        
        List<APXTConga4__Conga_Template__c> actList = new List<APXTConga4__Conga_Template__c>();
        APXTConga4__Conga_Template__c act =new APXTConga4__Conga_Template__c();
        act.Unique_Template_Name__c='Enclosed Cavity Insulation Template';

        APXTConga4__Conga_Template__c act1 =new APXTConga4__Conga_Template__c();
        act1.Unique_Template_Name__c='Master Disclosure Form';

        APXTConga4__Conga_Template__c act2 =new APXTConga4__Conga_Template__c();
        act2.Unique_Template_Name__c='Specified Measures Agreement Form Template';

        APXTConga4__Conga_Template__c act3 =new APXTConga4__Conga_Template__c();
        act3.Unique_Template_Name__c='Early CAC and CHP Rebate Form Template';

        APXTConga4__Conga_Template__c act4 =new APXTConga4__Conga_Template__c();
        act4.Unique_Template_Name__c = 'Permit Authorization Customer Signoff Template';

        
        actList.add(act);
        actList.add(act1);
        actList.add(act2);
        actList.add(act3);
        actList.add(act4);

        insert actList;

        List<APXTConga4__Conga_Merge_Query__c> queryList = new List<APXTConga4__Conga_Merge_Query__c>();

        APXTConga4__Conga_Merge_Query__c query = new APXTConga4__Conga_Merge_Query__c();
        query.Unique_Conga_Query_Name__c =  'Enclosed Cavity Insulation Query';

        APXTConga4__Conga_Merge_Query__c query1 = new APXTConga4__Conga_Merge_Query__c();
        query1.Unique_Conga_Query_Name__c =  'Master Disclosure Query';

        APXTConga4__Conga_Merge_Query__c query2 = new APXTConga4__Conga_Merge_Query__c();
        query2.Unique_Conga_Query_Name__c =  'Specified Measures Agreement Form Query';

        APXTConga4__Conga_Merge_Query__c query3 = new APXTConga4__Conga_Merge_Query__c();
        query3.Unique_Conga_Query_Name__c =  'Early CAC and CHP Rebate Form Query';

        APXTConga4__Conga_Merge_Query__c query4 = new APXTConga4__Conga_Merge_Query__c();
        query4.Unique_Conga_Query_Name__c =  'Permit Authorization Customer Signoff Query';

        queryList.add(query);
        queryList.add(query1);
        queryList.add(query2);
        queryList.add(query3);
        queryList.add(query4);
        insert queryList;

        ApexPages.currentPage().getParameters().put('Id',ea.id);
        ApexPages.currentPage().getParameters().put('docName','Enclosed Cavity Sheet');
        ApexPages.currentPage().getParameters().put('attachIds',att.Id);
        
        dsmtSignAndEmailDocumentController cntrl = new dsmtSignAndEmailDocumentController();
        cntrl.generateCongaDocument();
        cntrl.clearMap = true;  
        cntrl.docName = 'Specified Measures Agreement';
        cntrl.generateCongaDocument();
        cntrl.docName = 'Early CAC and CHP Rebate Form';
        cntrl.generateCongaDocument();
        cntrl.docName = 'Permit Authorization Form';
        cntrl.generateCongaDocument();
        cntrl.docName = 'Master Disclosure Form';
        cntrl.generateCongaDocument();

        Attachment att1 = new Attachment();
        att1.parentID = [SELECT Id FROM  Attachment__c WHERE Id =: cntrl.newCustomAttachmentId].Id;
        att1.body = blob.valueOf('test');
        att1.Name = 'test';
        insert att1;
        cntrl.checkCongaStatus();
        dsmtSignAndEmailDocumentController.saveSign(ea.Id, 'test','Energy_Assessment__c','TET');
        cntrl.SendEmail();
        cntrl.Cancel();
        test.stopTest();
    }
}