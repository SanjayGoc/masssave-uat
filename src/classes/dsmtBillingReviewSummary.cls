public with sharing class dsmtBillingReviewSummary{

    public string recommXml {get;set;}
    public string prjId{get;set;}
    public Review__c rev{get;set;}
    public string dataSave{get;set;}
    public String revId{get;set;}
    public decimal subTotal {get;set;}
    public decimal subTotalVirtual {get;set;}
    public decimal utilityIncentive {get;set;}
    public decimal utilityIncentiveVirtual {get;set;}
    public decimal customerContribution {get;set;}
    public decimal customerContributionVirtual{get;set;}
    public decimal customerDeposit {get;set;}
    public decimal remainingCustomerContribution {get;set;}
    public decimal remainingCustomerContributionVirtual{get;set;}
    public decimal preWeatherization{get;set;}
    
    
    public List<SelectOption> projList {get;set;}
    
    public dsmtBillingReviewSummary(){
        rev = new Review__c();
        prjId = ApexPages.currentPage().getParameters().get('prjid');
        revId = ApexPages.currentPage().getParameters().get('id');
        
        //loadRecommendations();
    }
    
    public dsmtBillingReviewSummary(ApexPages.StandardController controller) {
        rev = new Review__c();
        prjId = ApexPages.currentPage().getParameters().get('prjid');
        revId = ApexPages.currentPage().getParameters().get('id');
        
        //loadRecommendations();
    }
    
    
    
    private set<String> fetchProjectIdSet(Id reviewId)
    {
        set<String> projectIdSet = new Set<String>();
        
        List<Project_Review__c> revList = [select id, Name, Review__c, Project__c 
                                           from Project_Review__c 
                                           where Review__c =: reviewId
                                           and Review__c != null];
        for(Project_Review__c pr : revList)
        {
            projectIdSet.add(pr.Project__c);
        }
        
        return projectIdSet;
    }
    
    public void CreateReviewRecord(){
        if(revId != null)
        {
            List<Review__C> lstPrj = [select Id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                        Energy_Assessment__r.Trade_Ally_Account__r.Email__c,First_Name__c,
                                        Last_Name__c,Phone__c,Email__c,Address__c,City__c,State__c,
                                        Zipcode__c,Trade_Ally_Account__c from Review__C 
                                        where Id =: revId limit 1]; 
            rev.Status__c = 'New';
            Id devRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();
    
            List<Dsmtracker_Contact__c> dsmtList = [select id,Dsmtracker_Contact__c,Trade_Ally_Account__c from Dsmtracker_Contact__c where Portal_User__c =: userinfo.getUSerID()
                                                        and Trade_Ally_Account__c  != null];
            if(dsmtList.size() > 0){
                rev.Trade_Ally_Account__c = dsmtList.get(0).Trade_Ally_Account__c;
            }                                   
            if(lstPrj.get(0).Energy_Assessment__c != null)    
                rev.Energy_Assessment__c = lstPrj.get(0).Energy_Assessment__c;
            
            rev.Type__c = 'Change Order Review';
            rev.First_Name__c = lstPrj.get(0).First_Name__c;
            rev.Last_Name__c = lstPrj.get(0).Last_Name__c;
            rev.Phone__c = lstPrj.get(0).Phone__c;
            rev.Email__c = lstPrj.get(0).Email__c;
            rev.Address__c = lstPrj.get(0).Address__c;
            rev.City__c = lstPrj.get(0).City__c;
            rev.State__c = lstPrj.get(0).State__c;
            rev.Zipcode__c = lstPrj.get(0).Zipcode__c;
            rev.Work_Scope_Review__c = lstPrj.get(0).Id;
            
            if(lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c != null)
                rev.Trade_Ally_Email__c = lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c;
            
            rev.RecordTypeId =  devRecordTypeId;  
            upsert rev; 
     
            rev = [select Id,Name,Project__c,Status__c,Work_Scope_Review__c,Trade_Ally_Account__r.Name,Energy_Assessment__r.Name from Review__c where Id = :rev.Id limit 1];
        }
    }
    
    public void loadRecommendations()
    {
     
        if(projList == null || projList.size() == 0)
        {
            projList = new List<SelectOption>();
            projList.add(new SelectOption('All', 'All Projects'));
        }
        
        remainingCustomerContribution = customerDeposit = utilityIncentive = subTotal = subTotalVirtual = customerContribution = preWeatherization = 0;
        utilityIncentiveVirtual = customerContributionVirtual = remainingCustomerContributionVirtual  = 0 ;
        
        recommXml = '<Recommendations>';
        
        Set<String> projIds = new Set<String>();
        
        string revId1 = ApexPages.currentPage().getParameters().get('id');
        
        if(revId  != null){
            projIds = fetchProjectIdSet(revId1);
        }
        else{
            String projId = ApexPages.currentPage().getParameters().get('projid');
            projIds.add(projId);
        }
        
        Set<Id> prjIds = new Set<Id>();
        map<string, decimal> projCollectedMap = new map<string, decimal>();
        
        String projects = ApexPages.currentPage().getParameters().get('projects');
        
        if(projects != null && projects != 'All'){
            projIds = new Set<string>();
            projIds.add(projects);
        }
        
        Map<Id,Decimal> changeOrderMap = new Map<Id,Decimal>();
        map<string, decimal> projBarrierIncMap = new map<string, decimal>();
        if(projIds != null && projIds.size() > 0){
             // DSST-16299 by HP on 1st Dec 2018
            //DSST - 11201 - START
            String Status = 'Inactive';
            String query = 'select ' + sObjectFields('Recommendation__c') +' Recommendation_Scenario__r.Total_Collected__c, Recommendation_Scenario__r.Total_CustomerIncentive__c, Recommendation_Scenario__r.Barrier_Incentive__c,Recommendation_Scenario__r.Customer_Cost__c, Id,DSMTracker_Product__r.Name,Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Type__c,Recommendation_Scenario__r.Status__c,Recommendation_Scenario__r.Installed_Date__c,Energy_Assessment__r.Primary_Provider__c,Dsmtracker_Product__r.EMHome_EMHub_PartID__c, Proposal__r.Final_Proposal_Incentive__c, Proposal__r.Barrier_Incentive_Amount__c ,(Select id,Project__c,Recommendation__c,Utility_Incentive_Share__c, Final_Recommendation_Incentive__c,proposal__r.current_Incentive__c,proposal__r.Base_Incentive__c ,proposal__r.Final_Proposal_Incentive__c,proposal__r.Barrier_Incentive_Amount__c from Proposal_Recommendations__r where Project__c =:projIds and Proposal__r.Status__c !=: Status ) from Recommendation__c where Recommendation_Scenario__c in :projIds and Recommendation_Scenario__c != null order by CreatedDate desc limit 300 ';
            //DSST - 11201 - END
            Set<Id> recId = new Set<Id>();
            
            List<Recommendation__c> recomlist = database.query(query);
            for (Recommendation__c ir: recomlist) {
                recId.add(ir.Id);
                changeOrderMap.put(ir.id,ir.Change_Order_Quantity__c);

            }
            
            
            /*
                if(recId.size() > 0){
                List<Review__c> changeRevList = [select id from Review__c where (Billing_Review__c =: revId or Work_Scope_Review__c =: revId)
                                                    and RecordType.Name = 'Change Order Review' order by CreatedDate Desc];
                
                if(changeRevList  != null && changeRevList.size() > 0){
                    
                    Set<Id> crId = new Set<Id>();
                    
                    for( Review__c c : changeRevList){
                        crId.add(c.Id);
                    }
                
                    List<Change_Order_Line_Item__c> chageorderList = [select id,Change_Order_Quantity__c,Recommendation__c from Change_Order_Line_Item__c
                                                                        where Recommendation__c in : recId and (Billing_Review__c =: revId or Review__c =: revId)
                                                                        and change_Order_Review__c =: changeRevList.get(0).Id
                                                                       // and Status__c = 'Approved'
                                                                        order by CreatedDate Desc];
                    for(Change_Order_Line_Item__c c : chageorderList){
                        if(changeOrderMap.get(c.Recommendation__c) == null && c.Change_Order_Quantity__c != null){
                            changeOrderMap.put(c.Recommendation__c,c.Change_Order_Quantity__c);
                        }
                    }
                }
            }
            */
            system.debug('--changeOrderMap---'+changeOrderMap);
            
            Double Partcost = 0;
             Set<String> proposalIds = new Set<String>();
            for (Recommendation__c ir: recomlist) {
                
                Proposal_Recommendation__c pr = null;
                if(!ir.Proposal_Recommendations__r.isEmpty()) { 
                    pr = ir.Proposal_Recommendations__r.get(0);
                }

                
                if(ir.Recommendation_Scenario__r.Barrier_Incentive__c != null){ 
                    projBarrierIncMap.put(ir.Recommendation_Scenario__c,ir.Recommendation_Scenario__r.Barrier_Incentive__c);
                }
                
                if(ir.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(ir.Recommendation_Scenario__c + '~~~' + ir.Recommendation_Scenario__r.Name, ir.Recommendation_Scenario__r.Total_Collected__c);
                
                recommXml += '<Recommendation>';
                recommXml += '<Checked>' + false + '</Checked>';
                recommXml += '<ProjectId>' + ir.Recommendation_Scenario__c + '</ProjectId>';
                recommXml += '<ProjectName><![CDATA[<a href="/'+ir.Recommendation_Scenario__c+'" target="_blank">' + checkStringNull(ir.Recommendation_Scenario__r.Name) + '</a>]]></ProjectName>';
                recommXml += '<RecommendationId>' + ir.Id + '</RecommendationId>';
                recommXml += '<RecommendationName><![CDATA[' + checkStringNull(ir.DSMTracker_Product__r.Name) + ']]></RecommendationName>';
                
                if(ir.Quantity__c == null || ir.Quantity__c == 0)
                    recommXml += '<Quantity>0</Quantity>';
                else
                    recommXml += '<Quantity><![CDATA[' + ir.Quantity__c + ']]></Quantity>';
                
                recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
            
                
                Partcost = 0;
                if(changeOrderMap.get(ir.Id) != null){
                     Partcost = changeOrderMap.get(ir.Id) * (ir.Unit_Cost__c != null ? ir.Unit_Cost__c : 0);
                     recommXml += '<PartCost><![CDATA[' + Partcost + ']]></PartCost>';
                }else{  
                    if(ir.Part_Cost__c != null && ir.Part_Cost__c != 0){
                        Partcost = ir.Part_Cost__c;
                        recommXml += '<PartCost><![CDATA[' + ir.Part_Cost__c+ ']]></PartCost>';
                    }else{
                        recommXml += '<PartCost></PartCost>';
                    }
                }
                
                recommXml += '<Reason><![CDATA[' + checkStringNull(ir.Change_Order_Reason__c)+ ']]></Reason>';
                
                if(ir.Total_Recommended_Incentive__c != null){
                    recommXml += '<UtilityIncentive><![CDATA[' + (Partcost  * (pr != null ? (pr.Proposal__r.Current_Incentive__c != null ? pr.Proposal__r.Current_Incentive__c * 0.01 : 0) : 0)).setscale(2)+ ']]></UtilityIncentive>';
                //    recommXml += '<UtilityIncentive>10</UtilityIncentive>';
                }else{
                    recommXml += '<UtilityIncentive>0</UtilityIncentive>';
                }
                
                recommXml += '<PartId><![CDATA[' + checkStringNull(ir.Dsmtracker_Product__r.EMHome_EMHub_PartID__c)+ ']]></PartId>';
                recommXml += '<UtilityBeingCharged><![CDATA[' + checkStringNull(ir.Energy_Assessment__r.Primary_Provider__c)+ ']]></UtilityBeingCharged>';
                
                if(ir.Installed_Date__c == null)
                    recommXml += '<InstallDate></InstallDate>';
                else
                    recommXml += '<InstallDate><![CDATA[' + ir.Installed_Date__c.format().replace(' 00:00:00', '') + ']]></InstallDate>';
                
                if(ir.Invoice_Type__c == null)
                    recommXml += '<InvoiceType></InvoiceType>';
                else
                    recommXml += '<InvoiceType><![CDATA[' + ir.Invoice_Type__c + ']]></InvoiceType>';
                
                /*if(ir.Installed_Quantity__c == null)
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                else
                    recommXml += '<InstalledQuantity><![CDATA[' + ir.Installed_Quantity__c + ']]></InstalledQuantity>';*/
                
                 if(changeOrderMap.get(ir.Id) == null)
                    recommXml += '<InstalledQuantity><![CDATA[' + ir.Quantity__c  + ']]></InstalledQuantity>';
                else if(changeOrderMap.get(ir.Id) != null){
                    recommXml += '<InstalledQuantity><![CDATA[' + changeOrderMap.get(ir.Id) + ']]></InstalledQuantity>';
                }else{
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                }
                
                if(ir.Status__c != null)
                    recommXml += '<Status><![CDATA[' + ir.Status__c + ']]></Status>';
                else
                    recommXml += '<Status></Status>';
                
                if(ir.Recommendation_Scenario__r.Barrier_Incentive__c == null){
                    recommXml += '<BarrierIncentive>0</BarrierIncentive>';
                }else{
                    recommXml += '<BarrierIncentive><![CDATA[' + ir.Recommendation_Scenario__r.Barrier_Incentive__c+ ']]></BarrierIncentive>';
                }
                recommXml += '</Recommendation>';
                
                if(ir.Unit_Cost__c != null && changeOrderMap.get(ir.Id) != null){
                    subTotalVirtual += ir.Unit_Cost__c * changeOrderMap.get(ir.Id);
                } else if(ir.Unit_Cost__c != null && ir.Quantity__c != null){
                    subTotalVirtual += ir.Unit_Cost__c * ir.Quantity__c;
                }
                
                if(ir.Unit_Cost__c != null && ir.Quantity__c != null)
                    subTotal += ir.Unit_Cost__c * ir.Quantity__c ;
                
                 if(pr != null){
                    if(proposalIds.add(pr.proposal__c)){
                        System.debug('--Final_Proposal_Incentive'+pr.Proposal__r.Final_Proposal_Incentive__c);
                        System.debug('--Barrier_Incentive_Amount'+pr.Proposal__r.Barrier_Incentive_Amount__c);
                        if(pr.Proposal__c != null && pr.Proposal__r.Final_Proposal_Incentive__c != null && pr.Proposal__r.Barrier_Incentive_Amount__c != null){
                            utilityIncentiveVirtual += (pr.Proposal__r.Final_Proposal_Incentive__c - pr.Proposal__r.Barrier_Incentive_Amount__c).setScale(2);                            
                        }

                        preWeatherization += (pr.Proposal__r.Barrier_Incentive_Amount__c != null)?pr.Proposal__r.Barrier_Incentive_Amount__c:0;
                    }
                    
                    //DSST - 11201 START
                    /*if(ir.part_Cost__c != null && pr.Proposal__r.Current_Incentive__c != null){
                        System.debug('--PartCost - '+ir.part_cost__c);
                        System.debug('--current_Incentive - '+pr.proposal__r.current_Incentive__c);
                        utilityIncentive += (ir.part_cost__c * pr.proposal__r.current_Incentive__c * 0.01).setScale(2);
                    }*/
                    
                    
                    if(pr.Proposal__r.Current_Incentive__c >= pr.Proposal__r.Base_Incentive__c){
                        system.debug('--Recomm--'+ir);
                        system.debug('--ir.Cost__c --'+ir.Cost__c);
                        system.debug('pr.proposal__r.current_Incentive__c--'+pr.proposal__r.current_Incentive__c);
                        if(pr.proposal__c != null && ir.Part_Cost__c != null && pr.Proposal__r.Current_Incentive__c != null){
                            utilityIncentive += (ir.Part_Cost__c * pr.proposal__r.current_Incentive__c * 0.01).setScale(2);  // DSST-14333 - PP
                        }
                    }else{
                        system.debug('pr.proposal__r.Base_Incentive__c-'+pr.proposal__r.Base_Incentive__c);     
                        if(pr.proposal__c != null && ir.Part_Cost__c != null && pr.Proposal__r.Base_Incentive__c != null){
                            utilityIncentive += (ir.Part_Cost__c * pr.proposal__r.Base_Incentive__c * 0.01).setScale(2); // DSST-14333 - PP
                        }
                    }
                    //DSST - 11201 END
                    
                }
                system.debug('--utilityIncentive --'+utilityIncentive );
                //if(ir.Recommendation_Scenario__r.Total_CustomerIncentive__c != null)
                //    utilityIncentive += ir.Recommendation_Scenario__r.Total_CustomerIncentive__c;
                
                //if(ir.Recommendation_Scenario__r.Customer_Cost__c != null && prjIds.add(ir.Recommendation_Scenario__c))
                //    customerContribution += ir.Recommendation_Scenario__r.Customer_Cost__c;
            }
        }else{
            String RevId =  ApexPages.currentPage().getParameters().get('revid');
            
            system.debug('--RevId ---'+RevId);
            if(RevId != null && RevId != ''){
                List<Project_Review__c> prList = [select id,Project__C from Project_Review__c where Review__c =: revID];
                
                if(prList  != null && prList.size() > 0){
                    Set<Id> prjId = new Set<ID>();
                    
                    for(Project_Review__c pr : prList){
                        prjId.add(pr.Project__c);
                    }
                    
                    List<Proposal_Recommendation__c> prRecList = [select id,Project__c,Recommendation__c from Proposal_Recommendation__c
                                                                            where Project__c in : prjID];
                    Set<Id> recId = new Set<Id>();
                    
                    Map<Id,Recommendation__c> recToupdate = new Map<Id,Recommendation__c >();
                    Recommendation__c newRec = null;
                    
                    for(Proposal_Recommendation__c pr : prRecList){
                        recId.add(pr.Recommendation__c );
                        newRec = new Recommendation__c();
                        newRec.Id = pr.Recommendation__c;
                        newRec.Recommendation_Scenario__c = pr.Project__c;
                        recToupdate.put(newRec.Id,newRec);
                    }
                    
                    if(recToupdate.keyset().size() > 0){
                        update recToupdate.values();
                    }
               
                // DSST-16299 by HP on 1st Dec 2018
               String Status = 'Inactive';
            String query = 'select ' + sObjectFields('Recommendation__c') +' Recommendation_Scenario__r.Barrier_Incentive__c, Recommendation_Scenario__r.Total_Collected__c, Recommendation_Scenario__r.Total_CustomerIncentive__c, Recommendation_Scenario__r.Customer_Cost__c, Id, DSMTracker_Product__r.Name, Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Type__c,Recommendation_Scenario__r.Status__c,Recommendation_Scenario__r.Installed_Date__c, (Select id,Project__c,Recommendation__c,Utility_Incentive_Share__c, Final_Recommendation_Incentive__c, Proposal__r.Current_Incentive__c from Proposal_Recommendations__r where Project__c =:prjId and Proposal__r.Status__c !=: Status) from Recommendation__c where ( Recommendation_Scenario__c in :prjId or id in : recID or (Review__c =: revID and Review__c != null)) and Recommendation_Scenario__c  != null order by CreatedDate desc limit 300 ';
            
            
            for (Recommendation__c ir: database.query(query)) {                
                if(ir.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(ir.Recommendation_Scenario__c + '~~~' + ir.Recommendation_Scenario__r.Name, ir.Recommendation_Scenario__r.Total_Collected__c);
                
                if(ir.Recommendation_Scenario__r.Barrier_Incentive__c != null) { 
                    projBarrierIncMap.put(ir.Recommendation_Scenario__c,ir.Recommendation_Scenario__r.Barrier_Incentive__c);
                }   
                recommXml += '<Recommendation>';
                recommXml += '<Checked>' + false + '</Checked>';
                recommXml += '<ProjectId>' + ir.Recommendation_Scenario__c + '</ProjectId>';
                recommXml += '<ProjectName><![CDATA[<a href="/'+ir.Recommendation_Scenario__c+'" target="_blank">' + checkStringNull(ir.Recommendation_Scenario__r.Name) + '</a>]]></ProjectName>';
                recommXml += '<RecommendationId>' + ir.Id + '</RecommendationId>';
                recommXml += '<RecommendationName><![CDATA[' + checkStringNull(ir.DSMTracker_Product__r.Name) + ']]></RecommendationName>';
                
                if(ir.Quantity__c  == null)
                    recommXml += '<Quantity>0</Quantity>';
                else
                    recommXml += '<Quantity><![CDATA[' + ir.Quantity__c + ']]></Quantity>';
                
               /* if(ir.Change_Order_Quantity__c == null)
                    recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
                else    
                    recommXml += '<ChangeOrderQuantity><![CDATA[' + ir.Change_Order_Quantity__c + ']]></ChangeOrderQuantity>';
                */
                recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
                
              /*  if(ir.Part_Cost__c != null)
                    recommXml += '<PartCost><![CDATA[' + ir.Part_Cost__c+ ']]></PartCost>';
                else
                    recommXml += '<PartCost></PartCost>';
                */
                  if(changeOrderMap.get(ir.Id) != null){
                        recommXml += '<PartCost><![CDATA[' + changeOrderMap.get(ir.Id) * ir.Unit_Cost__c+ ']]></PartCost>';
                }else{  
                    if(ir.Part_Cost__c != null && ir.Part_Cost__c != 0){
                        recommXml += '<PartCost><![CDATA[' + ir.Part_Cost__c+ ']]></PartCost>';
                    }else{
                        recommXml += '<PartCost></PartCost>';
                    }
                }
                /*else{
                    if(changeOrderMap.get(ir.Id) != null){
                        recommXml += '<PartCost><![CDATA[' + changeOrderMap.get(ir.Id) * ir.Unit_Cost__c+ ']]></PartCost>';
                    }else{
                        recommXml += '<PartCost></PartCost>';
                    }
                }*/
                    
                recommXml += '<Reason><![CDATA[' + checkStringNull(ir.Change_Order_Reason__c)+ ']]></Reason>';
                
                recommXml += '<UtilityIncentive></UtilityIncentive>';
                recommXml += '<PartId></PartId>';
                recommXml += '<UtilityBeingCharged></UtilityBeingCharged>';
                
                if(ir.Installed_Date__c == null)
                    recommXml += '<InstallDate></InstallDate>';
                else
                    recommXml += '<InstallDate><![CDATA[' + ir.Installed_Date__c.format().replace(' 00:00:00', '') + ']]></InstallDate>';
                
                if(ir.Invoice_Type__c == null)
                    recommXml += '<InvoiceType></InvoiceType>';
                else
                    recommXml += '<InvoiceType><![CDATA[' + ir.Invoice_Type__c + ']]></InvoiceType>';
                
               /* if(ir.Installed_Quantity__c == null)
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                else
                    recommXml += '<InstalledQuantity><![CDATA[' + ir.Installed_Quantity__c + ']]></InstalledQuantity>';
                */
                
                 if(changeOrderMap.get(ir.Id) == null)
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                else
                    recommXml += '<InstalledQuantity><![CDATA[' + changeOrderMap.get(ir.Id) + ']]></InstalledQuantity>';
                    
                if(ir.Status__c != null)
                    recommXml += '<Status><![CDATA[' + ir.Status__c + ']]></Status>';
                else
                    recommXml += '<Status></Status>';
                    
                recommXml += '</Recommendation>';
                
                if(ir.Unit_Cost__c != null && ir.Quantity__c != null)
                    subTotal += ir.Unit_Cost__c * ir.Quantity__c ;
                
             //   utilityIncentive += ir.Calculated_Incentive__c;
                
                system.debug('--ir.Unit_Cost__c --'+ir.Unit_Cost__c );
                system.debug('--changeOrderMap.get(ir.Id) --'+changeOrderMap.get(ir.Id));
                system.debug('--ir.Quantity__c--'+ir.Quantity__c);
                
                if(ir.Unit_Cost__c != null && changeOrderMap.get(ir.Id) != null){
                    subTotalVirtual += ir.Unit_Cost__c * changeOrderMap.get(ir.Id);
                }else if(ir.Unit_Cost__c != null && ir.Quantity__c != null){
                    subTotalVirtual += ir.Unit_Cost__c * ir.Quantity__c;
                }
                
                Proposal_Recommendation__c pr = null;
                if(!ir.Proposal_Recommendations__r.isEmpty()) { 
                    pr = ir.Proposal_Recommendations__r.get(0);
                }
                
                //if(ir.Recommendation_Scenario__r.Total_CustomerIncentive__c != null)
                //    utilityIncentive += ir.Recommendation_Scenario__r.Total_CustomerIncentive__c;
                
                //if(ir.Recommendation_Scenario__r.Customer_Cost__c != null && prjIds.add(ir.Recommendation_Scenario__c))
                //    customerContribution += ir.Recommendation_Scenario__r.Customer_Cost__c;
            }
             }
            }
        }
        
        system.debug('--recommXml --'+recommXml );
        customerContribution = subTotal - utilityIncentive - preWeatherization; // DSST-14253 - PP
        
        if(customerContribution != null){
            customerContribution.setScale(2);
        }
        
        customerContributionVirtual = subTotalVirtual - utilityIncentiveVirtual - preWeatherization; // DSST-14253 - PP
        
        boolean isAdd = false;
        
        if(projList.size() == 1)
            isAdd = true;
        
        for(String s : projCollectedMap.keyset())
        {
            customerDeposit += projCollectedMap.get(s);
            
            if(isAdd)
                projList.add(new SelectOption(s.split('~~~')[0], s.split('~~~')[1]));
        }
        
       /* for(String s : projBarrierIncMap.keyset())
        {
            preWeatherization += projBarrierIncMap.get(s);
        }*/
        
        remainingCustomerContribution = (customerContribution - customerDeposit);
        remainingCustomerContributionVirtual  = (customerContributionVirtual - customerDeposit);
        recommXml += '</Recommendations>';
    }
    
    public string checkStringNull(string str){
        return str == null?'':str;
    }
    
   
    
     private static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
}