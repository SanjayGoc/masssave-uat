@isTest(seeAllData = true)
public class dsmtRescheduleWorderControllerTest 
{
    static testMethod void validateHelloWorld() 
    {
        Workorder_Type__c woType = new Workorder_Type__c(Name = 'Test',Est_PreWork_Time__c =20,Est_PostWork_Time__c =20,Est_Work_Time__c =20,Est_Deliverable_Time__c=40);
        insert woType;
        Skill__c skill=new Skill__c(name='TestSkill',Maximum_Score_Level__c=10,Minimum_Score_Level__c=0);
        insert skill;
        Required_Skill__c Rskill=new Required_Skill__c(Minimum_Score__c=0,Skill__c=skill.id,Workorder_Type__c=woType.Id);
        insert Rskill;
        
        Account acc = Datagenerator.createAccount();
        insert acc;
        
        Premise__c premise = Datagenerator.createPremise();
        insert premise;
        
        Customer__c customer = Datagenerator.createCustomer(acc.Id, premise.Id);
        insert customer;
        
        //location
        Location__c location = new Location__c(Name = 'Fresno');
        insert location;
        
        //Scheduler
        Scheduler__c scheduler = new Scheduler__c(User__c = UserInfo.getUserId(),
                                                  Location__c = location.Id,
                                                  Default_Location__c = true);
        insert scheduler;
        
        Employee__c emp = Datagenerator.createEmployee(location.Id);
        insert emp;
        
        Employee_Skill__c EmpSkill=new Employee_Skill__c(Employee__c=emp.id,Employee_Skill_Set_Score__c=10,Expertise_Level__c='Professional',Skill__c=skill.Id,As_per_Internal_Evaluation__c='Professional',Survey_Score__c=10);
        insert EmpSkill;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(location.Id, emp.Id);
        wt.Service_Date__c = system.today();
        wt.Captain__c = emp.Id;
        insert wt;
        
        Time_Slots__c ts = new Time_Slots__c();
        ts.Work_Team__c = wt.Id;
        insert ts;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Workorder_Type__c=woType.id;
        wo.Work_Team__c = wt.Id;
        wo.Third_Visit__c = system.today();
        wo.Requested_Date__c = system.today().addDays(2);
        wo.Scheduled_Start_Date__c = system.today();
        wo.Scheduled_End_Date__c = system.today().addDays(2);
        wo.Requested_Start_Date__c = system.today();
        wo.Requested_End_Date__c = system.today().addDays(2);
        wo.Customer__c = customer.Id;
        wo.Address__c = 'test';
        wo.City__c = 'city';
        wo.State__c = 'state';
        wo.Zipcode__c = '12234';
        wo.Time_Slot__c = ts.Id;
        wo.Duration__c = 90;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();
        insert wo;
        test.startTest();
        dsmtRescheduleWorderController obj = new dsmtRescheduleWorderController();  
        obj.getWeeks();
        obj.getDays();
        obj.getTimes();
        apexpages.currentpage().getparameters().put('id' , wo.Id);
        obj.queryRescWo();
        dsmtRescheduleWorderController.ValidateAddress(wo.id);
        obj.getAppt();
        obj.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
        obj.CreateWorkOrder();
        obj.RescheduleWorkOrder();
        test.stopTest();
    }
}