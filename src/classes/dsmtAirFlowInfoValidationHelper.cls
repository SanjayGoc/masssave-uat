public class dsmtAirFlowInfoValidationHelper extends dsmtEnergyAssessmentValidationHelper{
    public ValidationCategory validate(String categoryTitle,String uniqueName,Building_Specification__c bs,ValidationCategory otherValidations){
        ValidationCategory airflow = new ValidationCategory(categoryTitle,uniqueName);
        
        if(bs != null && bs.Id != null){
            Air_Flow_and_Air_Leakage__c afal = new Air_Flow_and_Air_Leakage__c();
            List<Air_Flow_and_Air_Leakage__c> afals = [
                SELECT Id, CFM50__c, Leakiness__c, Building_Specifications__c, Next_Blower_Door_Reading_Number__c,Next_Pressure_Differential_Reading_Numbe__c
                FROM Air_Flow_and_Air_Leakage__c
                WHERE Building_Specifications__c =:bs.Id
                ORDER BY CreatedDate DESC
                LIMIT 1
            ];
            if(afals.size() > 0){
                afal = afals.get(0);
            }

            Map<String,String> reqFieldsMap = new Map<String,String>{
                'CFM50__c'=>'CFM50'
            };
            validateRequiredFields(afal,reqFieldsMap,airflow);
            
            if(afal.CFM50__c != null && Integer.valueOf(afal.CFM50__c) == 0){
                airflow.errors.add(new ValidationMessage('CFM50 Requires an explicit value'));
            }else if(afal.CFM50__c != null && Integer.valueOf(afal.CFM50__c) < 150){
                airflow.errors.add(new ValidationMessage('CFM50 must be greater than equal to 150'));
            }else if(afal.CFM50__c != null && Integer.valueOf(afal.CFM50__c) > 15000){
                airflow.errors.add(new ValidationMessage('CFM50 must be less than equal to 15000'));
            }
            
            List<Blower_Door_Reading__c> bdrs = [
                SELECT Id, Name, Blower_Door_Test_Date__c, Ending_CFM50__c, Notes__c, Starting_CFM50__c,Time_Spent__c, Recommendation__c
                FROM Blower_Door_Reading__c
                WHERE 
                    Air_Flow_and_Air_Leakage__c =: afal.Id AND 
                    Building_Specification__c =: bs.Id
            ];
            if(bdrs.size() > 0){
                Integer i = 0;
                for(Blower_Door_Reading__c bdr : bdrs){
                    i++;
                    ValidationCategory bdrValidations = new ValidationCategory('Blower Door Reading #'+i,'BlowerDoorReading_'+i);
                    
                    reqFieldsMap = new Map<String,String>{
                        'Starting_CFM50__c'=>'Starting CFM50',
                        'Ending_CFM50__c'=>'Ending CFM50'
                    };
                    validateRequiredFields(bdr,reqFieldsMap,bdrValidations);
                    
                    if(bdr.Starting_CFM50__c < bdr.Ending_CFM50__c){
                        bdrValidations.errors.add(new ValidationMessage('The Ending CFM50 cannot be more than the Starting CFM50'));
                    }
                    
                    if(bdr.Recommendation__c == null){
                        bdrValidations.errors.add(new ValidationMessage('Should have at least one recommendation attached'));
                    }
                    
                    
                    airflow.addCategory(bdrValidations);
                }
            }
        }
        return airflow;        
    }
}