public with sharing class TriggerFactory {
    public static void createHandler(Schema.sObjectType soType) {
        ITrigger handler = getHandler(soType);

        if (handler == null) {
            //throw new Exception('No Trigger Handler registered for Object Type: ' + soType);
        }

        // Execute the handler to fulfil the trigger
        execute(handler);
    }
    private static void execute(ITrigger handler) {
        // Before Trigger
        if (Trigger.isBefore) {
            handler.bulkBefore();
        } else {
            handler.bulkAfter();
        }
    }

    private static ITrigger getHandler(Schema.sObjectType soType) {
       if (soType == Invoice__c.sObjectType) {
           return new InvoiceTriggerHandler();
        }
        else if (soType == Recommendation__c.sObjectType) {
            return new RecommendationTriggerHandler();
        }
        else if (soType == Review__c.sObjectType) {
            return new ReviewTriggerHandler();
        }
        else if(soType == Project_and_Energy_Assessment__c.SobjectType){
            return new ProjectEnergyAssessmenthandler();
        }
        else if(soType == Appliance__c.SobjectType){
            return new ApplianceTriggerHandler();
        }
        else if(soType == Building_Specification__c.SobjectType){
            return new BuildingSpecificationTriggerHandler();
        }
        else if(soType == Water_Fixture__c.SobjectType){
            return new WaterFixtureTriggerHandler();
        }
        else if(soType == Lighting__c.SobjectType){
            return new LightingTriggerHandler();
        }
        else if(soType == Lighting_Location__c.SobjectType){
            return new LightingLocationTriggerHandler();
        }
        else if(soType == Mechanical_Sub_Type__c.SobjectType)
        {
            return new MechanicalSubTypeTriggerHandler();
        }
        else if(soType == Air_Flow_and_Air_Leakage__c.SobjectType)
        {
            return new AirFlowAirLeakageTriggerHandler();
        }
        else if(soType == Layer__c.SobjectType)
        {
            return new LayerTriggerHandler();
        }
        else if(soType == Wall__c.SobjectType)
        {
            return new WallTriggerHandler();
        }
        else if(soType == Floor__c.SobjectType)
        {
            return new FloorTriggerHandler();
        }
        else if(soType == Ceiling__c.SobjectType)
        {
            return new CeilingTriggerHandler();
        }
        else if(soType == Thermal_Envelope_Type__c.SobjectType)
        {
            return new dsmtThermalEnvelopTypeTriggerHandler();
        }

        return null;
    }
}