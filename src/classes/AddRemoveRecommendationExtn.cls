public with sharing class AddRemoveRecommendationExtn {
    
    public string engId{get;set;}
    public string lineItemXml {get;set;}
    public Map<String,List<Recommendation__c>> mapSecRec{get;set;}
    public Set<String> sectionList{get;set;}
    public String selectedRowsValues {get;set;}
    public List<Recommendation__c> lstRec;
    public string InspId{get;set;}
    
    public integer recordCount {get;set;}
    
    public string projectXml {get;set;}
    public string recommXml {get;set;}
    
    public List<Inspection_Line_Item__c> inspectionItemList{get;set;}
    public string SelectedRecId{get;set;}
    public string SelectedDelRecId{get;set;}
    public Map<string,string> mapRecom ;
    public string recId1{get;set;}
    Set<Id> recIds; 
    public String selectedPrjId {set;get;}
    
    public void saveProjId(){
    
    }
    
    //This var hold List of user

      public List<SelectOption> getListOfProj()

        {
    
                   
    
                   List<SelectOption> prjList = new List<SelectOption>();
    
                   prjList.add(new SelectOption( ' ' ,'---Select---'));
    
                   for(Recommendation_Scenario__c ir: [select Id,Name from Recommendation_Scenario__c where Energy_Assessment__c = :engId]){
                        prjList.add(new SelectOption(ir.Id , ir.Name));
                   }
    
                  return prjList ;
    
        }
    
    public AddRemoveRecommendationExtn(){
        recIds = new Set<Id>();
        recordCount = 0;
        selectedRowsValues = '';
        mapRecom = new Map<string,string>();
        mapSecRec = new Map<String,List<Recommendation__c>>();
        sectionList = new Set<String>();
        InspId = ApexPages.currentPage().getParameters().get('id');
        
        List<Inspection_Request__c> inrList = [select id,Energy_Assessment__c,Project__c from Inspection_Request__c where id =: InspId];
        
        engId = inrList.get(0).Energy_Assessment__c;
        if(inrList.get(0).Project__c != null){
           selectedPrjId = inrList.get(0).Project__c;
        }
        lstRec = new List<Recommendation__c>();
        lstRec = queryInspectionReq();
        
        for(Recommendation__c re : lstRec){
            
            if(sectionList.add(re.Recommendation_Scenario__r.Name)){
                List<Recommendation__c> tempLst = new List<Recommendation__c>();
                tempLst.add(re);
                mapSecRec.put(re.Recommendation_Scenario__r.Name,tempLst);
            }
            else{
                List<Recommendation__c> tempLst = new List<Recommendation__c>();
                tempLst = mapSecRec.get(re.Recommendation_Scenario__r.Name);
                tempLst.add(re);
                mapSecRec.put(re.Recommendation_Scenario__r.Name,tempLst);
            }
        }
        loadLineItemXml();
        
    }
    
    
    public void loadProjects()
    {
        recIds = new Set<Id>();
        projectXml = '<Ispections>';
        integer count = 1;
        for (Inspection_Line_Item__c ir: [select Id, Name,Recommendation__c,Recommendation__r.Name,Recommendation__r.Recommendation_Scenario__r.Name,Installed_Quantity__c from Inspection_Line_Item__c where Inspection_Request__c = :InspId order by Name]) {
            projectXml += '<Ispection>';
            projectXml += '<SrNo>' + count + '</SrNo>';
            projectXml += '<IRId>' + ir.Id + '</IRId>';
            projectXml += '<RecommendationId>' + ir.Recommendation__c + '</RecommendationId>';
            projectXml += '<RecommendationName><![CDATA[' + ir.Recommendation__r.Name + ']]></RecommendationName>';
            projectXml += '<ProjectName><![CDATA[' + ir.Recommendation__r.Recommendation_Scenario__r.Name + ']]></ProjectName>';
            if(ir.Installed_Quantity__c == null)
                ir.Installed_Quantity__c = 0;
            projectXml += '<Quantity><![CDATA[' + ir.Installed_Quantity__c  + ']]></Quantity>';
            projectXml += '</Ispection>';
            
            count++;
            recIds.add(ir.Recommendation__c);
        } 
        projectXml += '</Ispections>';
    }
    
    Map<Id,Recommendation__c> recMap = new Map<Id,Recommendation__c>();
    
    public void loadRecommendations()
    {
        loadProjects();
        Set<Id> setPrjId = new Set<Id>();
        recommXml = '<Recommendations>';
        selectedPrjId = ApexPages.currentPage().getParameters().GET('prjId');
        
        for (Inspection_Line_Item__c ir: [select Id, Name,Recommendation__c,Recommendation__r.Name,Recommendation__r.Recommendation_Scenario__r.Name,Installed_Quantity__c from Inspection_Line_Item__c where Recommendation__c != null order by Name]) {
            recIds.add(ir.Recommendation__c);
        }
        
        system.debug('--recIds---'+recIds);
        system.debug('--selectedPrjId ---'+selectedPrjId );
        
        if(selectedPrjId != null && selectedPrjId != ''){
        
            String query = 'select ' + sObjectFields('Recommendation__c') +' Id,Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Type__c,Recommendation_Scenario__r.Status__c,Recommendation_Scenario__r.Installed_Date__c from Recommendation__c where Recommendation_Scenario__c = :selectedPrjId AND Id Not In :recIds order by Recommendation_Scenario__c Desc limit 300 ';
            
            
            for (Recommendation__c ir: database.query(query)) {
                recMap.put(ir.Id,ir);
                recommXml += '<Recommendation>';
                recommXml += '<Checked>' + checkBox(ir.isChecked__c) + '</Checked>';
                recommXml += '<ProjectId>' + ir.Recommendation_Scenario__c + '</ProjectId>';
                recommXml += '<ProjectName><![CDATA[' + ir.Recommendation_Scenario__r.Name + ']]></ProjectName>';
                recommXml += '<RecommendationId>' + ir.Id + '</RecommendationId>';
                recommXml += '<RecommendationName><![CDATA[' + ir.Name + ']]></RecommendationName>';
                if(ir.Quantity__c == null)
                    ir.Quantity__c = 0;
                recommXml += '<Quantity><![CDATA[' + ir.Quantity__c+ ']]></Quantity>';
                recommXml += '</Recommendation>';
                
                if(ir.isChecked__c){
                    mapRecom.put(ir.Id,ir.Id);
                }
            }
        }
        recommXml += '</Recommendations>';
    }
    
    public boolean checkBox(Boolean chk){
        return chk?true:false;
    }
    
    public List<Recommendation__c> queryInspectionReq(){
        
        recordCount = 0;
        
        Set<Id> setPrjId = new Set<Id>();
        for(Recommendation_Scenario__c ir: [select Id from Recommendation_Scenario__c where Energy_Assessment__c = :engId]){
            setPrjId.add(ir.Id);
            recordCount++;
        }
        
        String query = 'select ' + sObjectFields('Recommendation__c') +' Id,Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Type__c,Recommendation_Scenario__r.Status__c,Recommendation_Scenario__r.Installed_Date__c from Recommendation__c where Recommendation_Scenario__c In :setPrjId order by Recommendation_Scenario__c Desc limit 300 ';
        
        return Database.query(query);
    }
    
    public void loadLineItemXml() {
        lineItemXml = '<LineItems>';
        for (Recommendation__c aCLLI: lstRec) {
                lineItemXml += '<LineItem>';
                lineItemXml += '<Checked>' + false + '</Checked>';
                lineItemXml += '<LineItemId><![CDATA[' + aCLLI.Id + ']]></LineItemId>';
                lineItemXml += '<ProjectId><![CDATA[' + aCLLI.Recommendation_Scenario__c + ']]></ProjectId>';
                lineItemXml += '<ProjectName><![CDATA[' + checkStringNULL(aCLLI.Recommendation_Scenario__r.Name)+ ']]></ProjectName>';
                lineItemXml += '<Name><![CDATA[' + checkStringNULL(aCLLI.Name) + ']]></Name>';
                lineItemXml += '</LineItem>';
            
        }
        lineItemXml += '</LineItems>';
    }
    
    public void saveInspectionItem(){
        String query = 'select ' + sObjectFields('Recommendation__c') +' Id,Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Type__c,Recommendation_Scenario__r.Status__c,Recommendation_Scenario__r.Installed_Date__c from Recommendation__c where Recommendation_Scenario__c = :selectedPrjId AND Id Not In :recIds order by Recommendation_Scenario__c Desc limit 300 ';
        for (Recommendation__c ir: database.query(query)) {
                recMap.put(ir.Id,ir);
        }
        List<Inspection_Line_Item__c> inspectionItemtoinsert = new List<Inspection_Line_Item__c>();
        List<Inspection_Line_Item__c> inspectionItemtodelete = new List<Inspection_Line_Item__c>();
        List<Recommendation__c> lstRecc = new List<Recommendation__c>();
        Inspection_Line_Item__c inspectionitem = null;
        
        if(SelectedRecId != null){
            
            for(String str : SelectedRecId.split(',')){
                if(str.length() > 0)
                {
                    if(mapRecom.get(str) == null){
                    
                    inspectionitem = new Inspection_Line_Item__c();
                    inspectionitem.Inspection_Request__c = InspId ;
                    inspectionitem.Recommendation__c = str;
                    
                    if(recMap.get(str) != null){
                        inspectionitem.Installed_Quantity__c = recMap.get(str).Quantity__c;
                    }
                    inspectionItemtoinsert.add(inspectionitem);
                    
                    
                   }
                }
            } 
            
            if(inspectionItemtoinsert.size() > 0)
                insert inspectionItemtoinsert;
             
        }
        
        if(SelectedDelRecId != null){
            for(String str : SelectedDelRecId.split(',')){
                if(str.length() > 0)
                {
                    if(mapRecom.get(str) == null){
                    
                    inspectionitem = new Inspection_Line_Item__c();
                    inspectionitem.Id = str ;
                    inspectionItemtodelete.add(inspectionitem);
                    
                    
                   }
                }
            } 
        }
        
        if(inspectionItemtodelete.size()>0)
            delete inspectionItemtodelete;
    }
    
    public PageReference removeRecord(){
        string recId = ApexPages.currentPage().getParameters().get('recid');
        
        List<Inspection_Line_Item__c> lstILI = [select Id from Inspection_Line_Item__c where Recommendation__c = :recId1];
        if(lstILI.size()>0)
            delete lstILI;
        
        Recommendation__c recc = new Recommendation__c();
        recc.Id = recId1;
        recc.isChecked__c = false;
        update recc;
        
        
        Pagereference pg= new Pagereference('/apex/AddRemoveRecommendation?id='+InspId );
       pg.setRedirect(true);
        return pg;
    }
    
     private static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
    
    private string checkStringNULL(string stringVal) {
        if (stringVal != null) {
            return stringVal.replace('<', '&lt;').replace('>', '&gt;');
        } else {
            return '';
        }
    }
}