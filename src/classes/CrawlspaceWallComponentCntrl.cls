public with sharing class CrawlspaceWallComponentCntrl{
    public Id crawlspaceWallId{get;set;}
    public String newLayerType{get;set;}
    public Wall__c crawlspaceWall{get{
        if(crawlspaceWall == null){
            List<Wall__c> walls = Database.query('SELECT ' + getSObjectFields('Wall__c') + ',Thermal_Envelope_Type__r.SpaceConditioning__c FROM Wall__c WHERE Id=:crawlspaceWallId'); 
            if(walls.size() > 0){
                crawlspaceWall = walls.get(0);
            }
        }
        return crawlspaceWall;
    }set;}
    public void saveWall(){
        saveWallLayers();
        if(crawlspaceWall != null && crawlspaceWall.Id != null){
            crawlspaceWall.Exposed_To__c = crawlspaceWall.Wall_To__c;
            UPDATE crawlspaceWall;
            crawlspaceWall = null;
        }
    }
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    public List<Layer__c> wallLayers{get;set;}
    public Map<Id,Layer__c> getWallLayersMap(){
        Id wallId = crawlspaceWall.Id;
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Wall__c =:wallId AND HideLayer__c = false';
        wallLayers = Database.query(query);
        return new Map<Id,Layer__c>(wallLayers);
    }
    public void saveWallLayers(){
        if(wallLayers != null && wallLayers.size() > 0){
            UPSERT wallLayers;
        }
    }
    public void addNewCrawlspaceWallLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Wall__c =:crawlspaceWall.Id AND RecordTypeId =:recordTypeId];
        if(layerType != null && m.containsKey(layerType)){
            Decimal nextNumber= layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Wall__c = crawlspaceWall.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            if(newLayer.Id != null){
                crawlspaceWall.Next_Crawlspace_Wall_Layer_Number__c = nextNumber + 1;
                UPDATE crawlspaceWall;
            }
            wallLayers.add(newLayer);
        }
    }
    public void deleteWallLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = wallLayers.get(index).Id);
        wallLayers.remove(index);
        saveWall();
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}