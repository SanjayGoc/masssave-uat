@istest(seeAllData=true)
public class CalculationTriggerHelperTest
{
	@istest
    static void runtest()
    {

        Building_Specification__c bs =new Building_Specification__c ();
         bs.Floor_Area__c=1800;
         //bs.Previous_Floor_Area__c=5;
         //bs.Appointment__c=app.id;
         bs.Year_Built__c='1950';
         bs.Bedromm__c=3;
         bs.Occupants__c=3;
         bs.Natural_Gas__c='Gas';
         bs.Orientation__c='South';
         bs.Ceiling_Heights__c=8.00;
         bs.Floors__c=1;
         //bs.Floor_Area__c=1800.00;
         bs.Finished_Basement_Area__c=0;
         bs.Total_Floor_Area__c=1800;
         bs.Total_Volume__c=14400;
         
         insert bs;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea=[select id,name from Energy_Assessment__c where Status__c='Assessment Complete' order by LastmodifiedDate desc limit 1];
        
        Thermal_Envelope_Type__c te = new Thermal_Envelope_Type__c();
        te.Finished__c = true;
        te.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Garage Wall').getRecordTypeId();
        te.Energy_Assessment__c = ea.Id;
        te.SpaceConditioning__c = 'Conditioned';
        insert te;
        
        Thermal_Envelope_Type__c te2 = new Thermal_Envelope_Type__c();
        te2.Finished__c = true;
        te2.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Exterior Wall').getRecordTypeId();
        te2.Energy_Assessment__c = ea.Id;
        te2.SpaceConditioning__c = 'Conditioned';
        //insert te2;
        
        /*Energy_Assessment__c ea = new Energy_Assessment__c();
		//tet.id=ea.id;
        insert ea;*/
        
        wall__c wall =new wall__c();
        wall.Area__c=10;
        wall.Thermal_Envelope_Type__c=te.id;
        insert wall;
        
        Window__c wi =new Window__c ();
        wi.CLF__c=5;
        wi.Wall1__c=wall.id;
        wi.Actual_Wall__c=te.id;
        insert wi;
        
        Ceiling__c ar=new Ceiling__c();
        ar.Thermal_Envelope_Type__c=te.id;
        insert ar;
        
		Floor__c af = new Floor__c();
        af.FloorType__c='test';
        af.Thermal_Envelope_Type__c=te.id;
        insert af;
        
        Layer__c ly=new Layer__c();
        ly.Ceiling__c=ar.id;
        ly.Floor__c=af.id;
        ly.Thermal_Envelope_Type__c=te.id;
        ly.RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get('Insulation').getRecordTypeId();
        ly.Wall__c=wall.id;
        insert ly;
        /*Set<Id> atticIds = new Set<Id>();
        atticIds.add(tet.Id);*/
        
        CalculationTriggerHelper cth =new CalculationTriggerHelper();
        
        //CalculationTriggerHelper.buildingSpecificationChanged(ea, bs);
        CalculationTriggerHelper.WallChanged(te, wall);
        Test.startTest();
        CalculationTriggerHelper.buildingSpecificationChanged(ea,bs);
        
        CalculationTriggerHelper.updateAndReCalculateExteriorWallNetArea(te.id,ea.id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeRoofs(te.id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeWalls(te.id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeFloors(te.id);
        CalculationTriggerHelper.updateFirstBasement(ea.id);
        CalculationTriggerHelper.updateFirstBasementCeilings(ea.id);
        CalculationTriggerHelper.updateFirstBasementFloors(ea.id);
        Test.stopTest();
        //CalculationTriggerHelper.WallChanged(tet, wall);
 

    }
}