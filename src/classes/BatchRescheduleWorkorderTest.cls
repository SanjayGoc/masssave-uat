@istest
public class BatchRescheduleWorkorderTest
{
	@istest
    static void test()
    {
        Workorder_Type__c wt =new Workorder_Type__c();
        wt.Est_Deliverable_Time__c=2;
        insert wt;

        Workorder__c wo =new Workorder__c();
        wo.Status__c='Requires Review';
        /*wo.External_Reference_ID__c='test';
        wo.Early_Arrival_Time__c = date.today();*/
        wo.Workorder_Type__c =wt.id;
        insert wo;
        
        Skill__c sk =new Skill__c();
        insert sk;
        
        Required_Skill__c rs =new Required_Skill__c();
        rs.Minimum_Score__c=5;
        rs.Skill__c=sk.id;
        insert rs;
                      
        Employee_Skill__c es =new Employee_Skill__c();
        insert es;
        
        //workOrderDetail wod =new workOrderDetail();
        
        BatchRescheduleWorkorder brw= new BatchRescheduleWorkorder();
        DataBase.executeBatch(brw);
    }
}