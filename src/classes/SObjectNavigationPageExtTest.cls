@isTest
private class SObjectNavigationPageExtTest {

	@isTest static void test_dsmtCLRESTilesDetails_Page_Controller(){
        dsmtRelatedListController cntrl = new dsmtRelatedListController();
        cntrl.isNotNullOrEmpty(null);
        cntrl.getHasPageError();

        PageReference pageRef = Page.dsmtRelatedList;

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId',null);
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','invalid');
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','HESReviews');
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','HESServiceRequests');
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','HESServiceRequests');
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','HESOpenContracts');
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','HESProjectsReadyForReview');
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','HESProjectsReadyToInstall');
        cntrl.loadCLRESTilesDetails();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('tileId','HESProjectsInstalled');
        cntrl.loadCLRESTilesDetails();
    }

    @isTest static void test_RelatedList_Page_Controller(){
    	Energy_Assessment__c ea = DataGenerator.setupAssessment();

        dsmtRelatedListController cntrl = new dsmtRelatedListController();
        cntrl.isNotNullOrEmpty(null);
        cntrl.getHasPageError();

        Id billingReviewRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Billing Review').getRecordTypeId();

        PageReference pageRef = Page.dsmtRelatedList;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('prId',ea.Id);
        ApexPages.currentPage().getParameters().put('pRType','Energy_Assessment__c');
        ApexPages.currentPage().getParameters().put('cRPFName','none');
        ApexPages.currentPage().getParameters().put('cRType','Recommendation_Scenario__c');
        ApexPages.currentPage().getParameters().put('cRFName','AllRecommendationProjects');
        ApexPages.currentPage().getParameters().put('cRRTId',billingReviewRecordTypeId);
        ApexPages.currentPage().getParameters().put('recommendationProjects','true');
        cntrl.initRelatedList();

        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('prId',ea.Id);
        ApexPages.currentPage().getParameters().put('pRType','Energy_Assessment__c');
        ApexPages.currentPage().getParameters().put('cRPFName','Energy_Assessment__c');
        ApexPages.currentPage().getParameters().put('cRType','Recommendation_Scenario__c');
        ApexPages.currentPage().getParameters().put('cRFName','AllRecommendationProjects');
        ApexPages.currentPage().getParameters().put('cRRTId',billingReviewRecordTypeId);
        ApexPages.currentPage().getParameters().put('recommendationProjects','false');
        cntrl.initRelatedList();
    }

	@isTest static void test_EnergyAssessmentNavigationPageExt() {
		Energy_Assessment__c stdObj = DataGenerator.setupAssessment();

		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		EnergyAssessmentNavigationPageExt ext = new EnergyAssessmentNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_AppointmentNavigationPageExt() {
		Appointment__c stdObj = DataGenerator.createAppointment(null);

		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		AppointmentNavigationPageExt ext = new AppointmentNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_WorkorderNavigationPageExt() {
		Workorder__c stdObj = DataGenerator.createWo();
		stdObj.Early_Arrival_Time__c = System.now();
        stdObj.Late_Arrival_Time__c = System.now();
		Insert stdObj;

		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		WorkorderNavigationPageExt ext = new WorkorderNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_ProjectNavigationPageExt() {
		Recommendation_Scenario__c stdObj = new Recommendation_Scenario__c();
		Insert stdObj;
		
		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		ProjectNavigationPageExt ext = new ProjectNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_ProjectReviewNavigationPageExt() {
		Project_Review__c stdObj = new Project_Review__c();
		Insert stdObj;
		
		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		ProjectReviewNavigationPageExt ext = new ProjectReviewNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_RecommendationNavigationPageExt() {
		Recommendation__c stdObj = new Recommendation__c();
		Insert stdObj;
		
		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		RecommendationNavigationPageExt ext = new RecommendationNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_ReviewNavigationPageExt() {
		Review__c stdObj = new Review__c();
		Insert stdObj;
		
		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		ReviewNavigationPageExt ext = new ReviewNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_InspectionRequestNavigationPageExt() {
		Inspection_Request__c stdObj = new Inspection_Request__c();
		Insert stdObj;
		
		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		InspectionRequestNavigationPageExt ext = new InspectionRequestNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_InvoiceLineItemNavigationPageExt() {
		Invoice__c inv = new Invoice__c();
		Insert inv;
		Invoice_Line_Item__c stdObj = new Invoice_Line_Item__c(Invoice__c = inv.Id);
		Insert stdObj;
		
		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		InvoiceLineItemNavigationPageExt ext = new InvoiceLineItemNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}

	@isTest static void test_PaymentLineItemNavigationPageExt() {
		Payment_Line_Item__c stdObj = new Payment_Line_Item__c();
		Insert stdObj;
		
		ApexPages.StandardController std = new ApexPages.StandardController(stdObj);
		PaymentLineItemNavigationPageExt ext = new PaymentLineItemNavigationPageExt(std);
		boolean isHPCorIIC = ext.isHPCorIIC;
	}
}