@isTest
public class dsmtSafetyControllerTest {

    static testmethod void test(){
        
        Safety_Aspect__c sa  = new Safety_Aspect__c();
        insert sa;
        
        Asbestos_Location__c al = new Asbestos_Location__c();
        al.Safety_Aspect__c = sa.Id;
        insert al;
        
        Account  acc= Datagenerator.createAccount();
        insert acc;
    
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        insert cust;
        
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Trade_Ally_Type__c='HPC';
        Tacc.Internal_Account__c = true;
        update Tacc;
     
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        dsmt.Status__c = 'Approved';
        dsmt.Trade_Ally_Type_PL__c = 'HPC';
        dsmt.Email__c = 'test@ucs.com';
        update dsmt;
        
        System_Config__c config = new System_Config__c();
        config.Arrival_Window_min_after_Appointment__c = 30;
        config.Arrival_Window_min_before_Appointment__c = 10;
        insert config;
        
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        app.Customer_Reference__c = cust.Id;
        app.Appointment_Status__c = 'Scheduled';
        Workorder__c wo = new Workorder__c(Status__c='Unscheduled');
        wo.Requested_Start_Date__c  = Datetime.now();
        wo.Requested_End_Date__c  = Datetime.now().Addhours(2);
        wo.Scheduled_Start_Date__c = Datetime.now();
        wo.Scheduled_End_Date__c= Datetime.now().Addhours(2);
        wo.Scheduled_Start_Date__c = datetime.newInstance(2018, 3, 2, 12, 30, 0);
        wo.Scheduled_End_Date__c =  datetime.newInstance(2018, 3, 2, 13, 30, 0);
        Workorder_Type__c woType = new Workorder_Type__c(Name='Wifi');
        woType.Qualifying_Audit__c  = true;
        insert woType;
        wo.workorder_Type__c = woType.Id;
        insert wo;
        app.Workorder__c = wo.Id;
        update app;
    
           /*Energy_Assessment__c ea = new  Energy_Assessment__c();
           ea.Appointment__c = app.Id;
           insert ea;*/
        
           
           
        List<Asbestos_Location__c> alst = new List<Asbestos_Location__c>();
        alst.add(al);
        
        ApexPages.currentPage().getParameters().put('viewName','safetyAspectItemPanel');
        ApexPages.currentPage().getParameters().put('locationIndex','0');
        ApexPages.currentPage().getParameters().put('recordTypeId',Schema.SObjectType.Safety_Test__c.getRecordTypeInfosByName().get('Combustion Test').getRecordTypeId());
        
        dsmtSafetyController cntrl = new dsmtSafetyController();
        //cntrl.ea = ea;
        cntrl.safetyAspect = sa;
        cntrl.newSafetyTestFieldsetName = 'Combustion Test';
        cntrl.addAsbestosLocation();
        //cntrl.validateRequestAndSafetyAspect();
        cntrl.openView();
        cntrl.saveSafetyAspect();
        cntrl.addNewSafetyAspectItem();
        cntrl.getCombustionDevices();   
        cntrl.getCAZTests();
        cntrl.getCombustionTests();
        cntrl.getApplianceTests();
        cntrl.addNewSafetyTest();
        cntrl.saveSafetyTest();
        cntrl.addNewCombustionDeviceTest();
        cntrl.removeCombustionDeviceTest();
        cntrl.deleteSafetyTest();
        cntrl.saveSafetyAspectItem();
        cntrl.saveSafetyAspectItem();
        cntrl.deleteAsbestosLocation();
        
    }
}