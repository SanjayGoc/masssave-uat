@isTest
Public class dsmtEligibilityCheckControllerTest{
    @isTest
    public static void runTest(){
                Account  acc= Datagenerator.createAccount();
                acc.Billing_Account_Number__c= 'Gas-~~~1234344';
                insert acc;
                
                Account acc2 = new Account();
                acc2.Name = 'Energy NM';
                acc2.Billing_Account_Number__c= 'Gas-~~~1234';
                acc2.Utility_Service_Type__c= 'Gas';
                acc2.Account_Status__c= 'Active';
                insert acc2;
                
                Account acc3 = new Account();
                acc3.Name = 'Xcel Energy NM';
                acc3.Billing_Account_Number__c= 'Ele-~~~1234';
                acc3.Utility_Service_Type__c= 'Electric';
                acc3.Account_Status__c= 'Active';
                insert acc3;
                
                
                Customer__c newg  = new Customer__c();
                newg.Account__c = acc2.Id;
                insert newg;
                
                Customer__c newe = new Customer__c();
                newe.Account__c = acc3.id;
                insert newe;
                
                Call_List__c callListObj = new Call_List__c();
                callListObj.Status__c = 'Active';
                insert callListObj;
                
                Lead l = new Lead();
                l.LastName = 'test';
                l.Company = 'test';
                l.Read_date__c = Date.Today()-2;
                insert l;
                
                Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
                obj.Call_List__c = callListObj.Id;
                obj.Status__c = 'Ready To Call';
                obj.First_Name_New__c = 'Test';
                obj.Last_Name_New__c = 'Test';
                obj.Next_Followup_date__c  = Date.Today();
                obj.Lead__c = l.Id;
                obj.Phone_New__c  = '9909240666';
                obj.Followup_Date__c = date.Today();
                insert obj;
                     
                
                Skill__c sk= Datagenerator.createSkill();
                insert sk;
                
                
                Location__c loc= Datagenerator.createLocation();
                insert loc;
                Employee__c em = Datagenerator.createEmployee(loc.id);
                em.Status__c='Approved';
                insert em;
                Employee__c em2 = Datagenerator.createEmployee(loc.id);
                em2.Status__c='Approved';
                insert em2;
                
                Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
                insert wt;
                Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
                emskill.Survey_Score__c=89;
                insert emskill;
                Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                            Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
                insert woTypeList;
                Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
                insert rsk;
                
                Eligibility_Check__c EL= Datagenerator.createELCheck();
               /* EL.Workorder_Type__c=woTypeList.Id;
                EL.Service_Address__c='test';
                EL.City__c='test';
                EL.Start_Date__c=date.today().addDays(-5);
                EL.End_Date__c=date.today().addDays(5);
                
                update EL;
                EL.How_many_units__c='Single family';
                update EL;*/
 
                
                
                Program_Eligibility__c PE= Datagenerator.createProgramEL();
                Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
                Premise__c pre= Datagenerator.createPremise();
                insert pre;
                Customer__c cust = Datagenerator.createCustomer(acc2.id,pre.id);
                insert cust;
                Customer__c cust2 = Datagenerator.createCustomer(acc3.Id, pre.id);
                insert cust2;
                
                
                DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
                dsmt.Trade_Ally_Account__c =Tacc.id; 
                update dsmt;
                Appointment__c app= Datagenerator.createAppointment(EL.Id);
                app.DSMTracker_Contact__c=dsmt.id;
                update app;
                //  dsmt.Contact__c= cont.id;
                //update dsmt;
                Registration_Request__c reg= Datagenerator.createRegistration();
                reg.DSMTracker_Contact__c=dsmt.id; 
                reg.Trade_Ally_Account__c =Tacc.ID;
                reg.HIC_Attached__c=true;
                update reg;
                 customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
                 cEL.Workorder_Type__c=woTypeList.Id;
                 cEL.Electric_Customer__c=cust2.Id;
                 cEL.Gas_Customer__c= cust.id;
                 cEL.Interested_in_receiving_a_visit__c = false;
                  cEL.Excluded_From_Scheduling__c = false;
                 update cEL;
                 customer_Eligibility__c cEL2= Datagenerator.createCustomerEL(EL.id);
                 cEL2.Eligibility_Check__c = EL.id;
                 cEL2.Workorder_Type__c=woTypeList.Id;
                  cEL2.Gas_Customer__c= cust.id;
                 cEL2.Electric_Customer__c=cust2.Id;
                  cEL2.Interested_in_receiving_a_visit__c = false;
                  cEL2.Excluded_From_Scheduling__c = false;
                // cEL2.Gas_Customer__c= cust.id;
                 update cEL2;
                ApexPages.currentPage().getParameters().put('ecid',EL.id);
                
                dsmtEligibilityCheckController controller = new dsmtEligibilityCheckController();
                dsmtEligibilityCheckController.GECheckWrapper wrap= new dsmtEligibilityCheckController.GECheckWrapper();
                controller.eleCheckId= EL.ID;
                controller.customerId= cust.Id;
                controller.getAppt();
                controller.saveEligibility();
                // controller.getApartmentList();
                dsmtEligibilityCheckController.getContractors();
                dsmtEligibilityCheckController.fetchCustomer(obj.Id);
                dsmtEligibilityCheckController.LetsGo(false,false);
                controller.SaveRecord();
                controller.customerId= cust.id;
                
                dsmtEligibilityCheckController.getContactValues(EL.Id);
                dsmtEligibilityCheckController.checkZipEligibility('1234','test','2','1','test',false,false);
                dsmtEligibilityCheckController.GetCity('1234');
                dsmtEligibilityCheckController.checkGEUtility('Gas');
                controller.getEmployee();
                dsmtEligibilityCheckController.CheckCustomer('1Gas-1234~~~1234','test','test','test');
                controller.getEmployee();
                controller.WoTypeId=woTypeList.Id;
               

                controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
                controller.WoTypeId=woTypeList.Id;
               // controller.CreateWorkOrder();
                /*EL.How_many_units__c='2 units';
                update EL;*/
                //  controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
           /*    controller.CreateWorkOrder();
                //   controller.Selectedcontacts =dsmt.id;
                 controller.updateRecord();
                controller.upsertCustomerEligibility();
                controller.EligibilityFaliRecord();*/
              //  controller.VerifyMultiFamily();
             //   controller.dsmtVerifyMultiFamily();
              //  controller.EligibilityFaliRecord();
                //  controller.updateIntakePending12();
                
                
                }
                
     @isTest
    public static void runTest2(){
                Account  acc= Datagenerator.createAccount();
                acc.Billing_Account_Number__c= 'Gas-~~~1234344';
                insert acc;
                
                Account acc2 = new Account();
                acc2.Name = 'Energy NM';
                acc2.Billing_Account_Number__c= 'Gas-~~~1234';
                acc2.Utility_Service_Type__c= 'Gas';
                acc2.Account_Status__c= 'Active';
                insert acc2;
                
                Account acc3 = new Account();
                acc3.Name = 'Xcel Energy NM';
                acc3.Billing_Account_Number__c= 'Ele-~~~1234';
                acc3.Utility_Service_Type__c= 'Electric';
                acc3.Account_Status__c= 'Active';
                insert acc3;
                
                
                Customer__c newg  = new Customer__c();
                newg.Account__c = acc2.Id;
                insert newg;
                
                Customer__c newe = new Customer__c();
                newe.Account__c = acc3.id;
                insert newe;
                
                Call_List__c callListObj = new Call_List__c();
                callListObj.Status__c = 'Active';
                insert callListObj;
                
                Lead l = new Lead();
                l.LastName = 'test';
                l.Company = 'test';
                l.Read_date__c = Date.Today()-2;
                insert l;
                
                Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
                obj.Call_List__c = callListObj.Id;
                obj.Status__c = 'Ready To Call';
                obj.First_Name_New__c = 'Test';
                obj.Last_Name_New__c = 'Test';
                obj.Next_Followup_date__c  = Date.Today();
                obj.Lead__c = l.Id;
                obj.Phone_New__c  = '9909240666';
                obj.Followup_Date__c = date.Today();
                insert obj;
                     
                
                Skill__c sk= Datagenerator.createSkill();
                insert sk;
                
                
                Location__c loc= Datagenerator.createLocation();
                insert loc;
                Employee__c em = Datagenerator.createEmployee(loc.id);
                em.Status__c='Approved';
                insert em;
                Employee__c em2 = Datagenerator.createEmployee(loc.id);
                em2.Status__c='Approved';
                insert em2;
                
                Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
                insert wt;
                Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
                emskill.Survey_Score__c=89;
                insert emskill;
                Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                            Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
                insert woTypeList;
                Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
                insert rsk;
                
                Eligibility_Check__c EL= Datagenerator.createELCheck();
                EL.Workorder_Type__c=woTypeList.Id;
                EL.Service_Address__c='test';
                EL.City__c='test';
                EL.Start_Date__c=date.today().addDays(-5);
                EL.End_Date__c=date.today().addDays(5);
                
                update EL;
                EL.How_many_units__c='Single family';
                update EL;
 
                
                
                Program_Eligibility__c PE= Datagenerator.createProgramEL();
                Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
                Premise__c pre= Datagenerator.createPremise();
                insert pre;
                Customer__c cust = Datagenerator.createCustomer(acc2.id,pre.id);
                insert cust;
                Customer__c cust2 = Datagenerator.createCustomer(acc3.Id, pre.id);
                insert cust2;
                
                
                DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
                dsmt.Trade_Ally_Account__c =Tacc.id; 
                update dsmt;
                Appointment__c app= Datagenerator.createAppointment(EL.Id);
                app.DSMTracker_Contact__c=dsmt.id;
                update app;
                //  dsmt.Contact__c= cont.id;
                //update dsmt;
                Registration_Request__c reg= Datagenerator.createRegistration();
                reg.DSMTracker_Contact__c=dsmt.id; 
                reg.Trade_Ally_Account__c =Tacc.ID;
                reg.HIC_Attached__c=true;
                update reg;
                 customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
                 cEL.Workorder_Type__c=woTypeList.Id;
                 cEL.Electric_Customer__c=cust2.Id;
                 cEL.Gas_Customer__c= cust.id;
                 cEL.Interested_in_receiving_a_visit__c = false;
                  cEL.Excluded_From_Scheduling__c = false;
                 update cEL;
                 customer_Eligibility__c cEL2= Datagenerator.createCustomerEL(EL.id);
                 cEL2.Eligibility_Check__c = EL.id;
                 cEL2.Workorder_Type__c=woTypeList.Id;
                  cEL2.Gas_Customer__c= cust.id;
                 cEL2.Electric_Customer__c=cust2.Id;
                  cEL2.Interested_in_receiving_a_visit__c = false;
                  cEL2.Excluded_From_Scheduling__c = false;
                // cEL2.Gas_Customer__c= cust.id;
                 update cEL2;
                ApexPages.currentPage().getParameters().put('ecid',EL.id);
                
                dsmtEligibilityCheckController controller = new dsmtEligibilityCheckController();
                
              
                
                controller.CreateWorkOrder();
                EL.How_many_units__c='2 units';
                update EL;
                //  controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
              //   controller.CreateWorkOrder();
                //   controller.Selectedcontacts =dsmt.id;
                 
                controller.VerifyMultiFamily();
                controller.dsmtVerifyMultiFamily();
                controller.EligibilityFaliRecord();
               // controller.updateIntakePending12();
                controller.getApartmentList();
                
                }
                
        @isTest
    public static void runTest3(){
            Account  acc= Datagenerator.createAccount();
            acc.Billing_Account_Number__c= 'Gas-~~~1234344';
            insert acc;
            
            Account acc2 = new Account();
            acc2.Name = 'Energy NM';
            acc2.Billing_Account_Number__c= 'Gas-~~~1234';
            acc2.Utility_Service_Type__c= 'Gas';
            acc2.Account_Status__c= 'Active';
            insert acc2;
            
            Account acc3 = new Account();
            acc3.Name = 'Xcel Energy NM';
            acc3.Billing_Account_Number__c= 'Ele-~~~1234';
            acc3.Utility_Service_Type__c= 'Electric';
            acc3.Account_Status__c= 'Active';
            insert acc3;
            
            
            Customer__c newg  = new Customer__c();
            newg.Account__c = acc2.Id;
            insert newg;
            
            Customer__c newe = new Customer__c();
            newe.Account__c = acc3.id;
            insert newe;
            
            Call_List__c callListObj = new Call_List__c();
            callListObj.Status__c = 'Active';
            insert callListObj;
            
            Lead l = new Lead();
            l.LastName = 'test';
            l.Company = 'test';
            l.Read_date__c = Date.Today()-2;
            insert l;
            
            Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
            obj.Call_List__c = callListObj.Id;
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            obj.Followup_Date__c = date.Today();
            insert obj;
                 
            
            Skill__c sk= Datagenerator.createSkill();
            insert sk;
            
            
            Location__c loc= Datagenerator.createLocation();
            insert loc;
            Employee__c em = Datagenerator.createEmployee(loc.id);
            em.Status__c='Approved';
            insert em;
            Employee__c em2 = Datagenerator.createEmployee(loc.id);
            em2.Status__c='Approved';
            insert em2;
            
            Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
            insert wt;
            Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
            emskill.Survey_Score__c=89;
            insert emskill;
            Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                        Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
            insert woTypeList;
            Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
            insert rsk;
            
            Eligibility_Check__c EL= Datagenerator.createELCheck();
           /* EL.Workorder_Type__c=woTypeList.Id;
            EL.Service_Address__c='test';
            EL.City__c='test';
            EL.Start_Date__c=date.today().addDays(-5);
            EL.End_Date__c=date.today().addDays(5);
            
            update EL;
            EL.How_many_units__c='Single family';
            update EL;*/

            
            
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Premise__c pre= Datagenerator.createPremise();
        insert pre;
        Customer__c cust = Datagenerator.createCustomer(acc2.id,pre.id);
        insert cust;
        Customer__c cust2 = Datagenerator.createCustomer(acc3.Id, pre.id);
        insert cust2;
            
            
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        update app;
        //  dsmt.Contact__c= cont.id;
        //update dsmt;
        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.HIC_Attached__c=true;
        update reg;
        customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
        cEL.Workorder_Type__c=woTypeList.Id;
        cEL.Electric_Customer__c=cust2.Id;
        cEL.Gas_Customer__c= cust.id;
        cEL.Interested_in_receiving_a_visit__c = false;
        cEL.Excluded_From_Scheduling__c = false;
        update cEL;
        customer_Eligibility__c cEL2= Datagenerator.createCustomerEL(EL.id);
        cEL2.Eligibility_Check__c = EL.id;
        cEL2.Workorder_Type__c=woTypeList.Id;
        cEL2.Gas_Customer__c= cust.id;
        cEL2.Electric_Customer__c=cust2.Id;
        cEL2.Interested_in_receiving_a_visit__c = false;
        cEL2.Excluded_From_Scheduling__c = false;
        // cEL2.Gas_Customer__c= cust.id;
        update cEL2;
        ApexPages.currentPage().getParameters().put('ecid',EL.id);
        
        dsmtEligibilityCheckController controller = new dsmtEligibilityCheckController();
        dsmtEligibilityCheckController.GECheckWrapper wrap= new dsmtEligibilityCheckController.GECheckWrapper();
        controller.eleCheckId= EL.ID;
        controller.customerId= cust.Id;
        controller.getAppt();
        controller.saveEligibility();
        // controller.getApartmentList();
        dsmtEligibilityCheckController.getContractors();
        dsmtEligibilityCheckController.fetchCustomer(obj.Id);
        //dsmtEligibilityCheckController.LetsGo(false,false);
        //controller.SaveRecord();
        controller.customerId= cust.id;
        
        dsmtEligibilityCheckController.getContactValues(EL.Id);
        dsmtEligibilityCheckController.checkZipEligibility('1234','test','2','1','test',false,false);
        dsmtEligibilityCheckController.GetCity('1234');
        dsmtEligibilityCheckController.checkGEUtility('Gas');
        //controller.getEmployee();
        dsmtEligibilityCheckController.CheckCustomer('1Gas-1234~~~1234','test','test','test');
        //controller.getEmployee();
        controller.WoTypeId=woTypeList.Id;
           

        controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
        controller.WoTypeId=woTypeList.Id;
        controller.CreateWorkOrder();
    }      
}