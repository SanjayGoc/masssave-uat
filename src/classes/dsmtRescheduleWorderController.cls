public class dsmtRescheduleWorderController{
    
    // added by SK
    public Eligibility_Check__c custInteraction {get;set;} 
    public string timeFilter {get;set;}
    public string dayOfWeekFilter {get;set;}
    public string weekFilter {get;set;}
    
    public string customerId{get;set;}
    public string woType{get;set;}
    public string worderId{get;set;}
    public string SelectedWorkTeam{get;set;}
    public boolean callMeBack {get;set;}
    public boolean showbutton{get;set;}
    
    public list<SelectOption> getWeeks(){
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--None--'));
        soList.add(new SelectOption('1', '1'));
        soList.add(new SelectOption('2', '2'));
        soList.add(new SelectOption('3', '3'));
        soList.add(new SelectOption('4', '4'));
        soList.add(new SelectOption('5', '5'));
        soList.add(new SelectOption('6', '6'));
        soList.add(new SelectOption('7', '7'));
        soList.add(new SelectOption('8', '8'));
        soList.add(new SelectOption('9', '9'));
        soList.add(new SelectOption('10', '10'));
        soList.add(new SelectOption('11', '11'));
        soList.add(new SelectOption('12', '12'));

        return soList;
    }

    public list<SelectOption> getDays(){
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--None--'));
        soList.add(new SelectOption('Sunday', 'Sunday'));
        soList.add(new SelectOption('Monday', 'Monday'));
        soList.add(new SelectOption('Tuesday', 'Tuesday'));
        soList.add(new SelectOption('Wednesday', 'Wednesday'));
        soList.add(new SelectOption('Thursday', 'Thursday'));
        soList.add(new SelectOption('Friday', 'Friday'));
        soList.add(new SelectOption('Saturday', 'Saturday'));

        return soList;
    }

    public list<SelectOption> getTimes(){
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--None--'));
        soList.add(new SelectOption('Before Noon', 'Before Noon'));
        soList.add(new SelectOption('After Noon', 'After Noon'));

        return soList;
    }
    // End addition by sk
    
    public Workorder__c rescWO {get;set;}
    
    public dsmtRescheduleWorderController(){
        callMeBack = false;
        rescWO = new Workorder__c();
        apptMap = new List<SelectOption>();
        custInteraction = new Eligibility_Check__c();
       // custInteraction.Start_Date__c = Date.Today().AddDays(2);
       // custInteraction.End_Date__c = Date.Today().AddDays(32);
        queryRescWo();
    }
    
    public string timeslot{get;set;}
    public string arrivalTime{get;set;}
    
    public void queryRescWo(){
        rescWO = new Workorder__c();
        list<Workorder__c> woList = [select id, name, Scheduled_Date__c, Scheduled_Start_Date__c, Work_Team__r.Name,
                                     Status__c, Customer__r.Name, Address__c, Phone__c, Reschedule_Reason__c, Reschedule_Notes__c,
                                     Customer__c,WorkOrder_Type__c,city__c,Zipcode__c,Early_Arrival_Time__c,
                                     Scheduled_End_Date__c,Late_Arrival_Time__c,External_Reference_ID__c,
                                     Call_me_back_to_reschedule__c, Reschedule_Callback_Date__c
                                     from Workorder__c
                                     where id =: ApexPages.currentPage().getParameters().get('id')];
        if(woList.size() > 0){
            rescWo = woList[0];
            callMeBack = rescWo.Call_me_back_to_reschedule__c;
            rescWo.Call_me_back_to_reschedule__c = false;
            customerId = rescWo.Customer__c;
            woType = rescWo.WorkOrder_Type__c;
            worderId = rescWo.Id;
            system.debug(woType);
            system.debug(worderId);
            String startHour = '00';
            String startMin = '00';
            String EndHour = '00';
            String EndMin = '00';
            Integer hour = 0;
            
            if(rescWo.Scheduled_Start_Date__c != null){
                if(rescWo.Scheduled_Start_Date__c != null){
                    startHour = String.valueOf(rescWo.Scheduled_Start_Date__c.hour());
                    startMin = String.valueOf(rescWo.Scheduled_Start_Date__c.Minute());
                }
                if(rescWo.Scheduled_End_Date__c != null){
                    EndHour = String.valueOf(rescWo.Scheduled_End_Date__c.hour());
                    EndMin = String.valueOf(rescWo.Scheduled_End_Date__c.Minute());
                }
                
                if(rescWo.Scheduled_Start_Date__c != null && rescWo.Scheduled_Start_Date__c.hour() < 12){
                    startHour = startHour+':'+startMin+ ' AM ';
                }else if(rescWo.Scheduled_Start_Date__c != null){
                    if(rescWo.Scheduled_Start_Date__c.hour() != 12){
                        hour  = rescWo.Scheduled_Start_Date__c.hour() - 12;
                    }else{
                        hour  = rescWo.Scheduled_Start_Date__c.hour();
                    }
                    if(startMin != null && startMin.length() == 1){
                        startMin = startMin+'0';
                    }
                    startHour = string.valueOf(hour)+':'+startMin+ ' PM ';
                }
                
                if(rescWo.Scheduled_End_Date__c != null && rescWo.Scheduled_End_Date__c.hour() < 12){
                    Endhour = Endhour +':'+EndMin+ ' AM ';
                }else if(rescWo.Scheduled_End_Date__c != null ){
                    if(rescWo.Scheduled_End_Date__c.hour() != 12){
                        hour  = rescWo.Scheduled_End_Date__c.hour() - 12;
                    }else{
                        hour  = rescWo.Scheduled_End_Date__c.hour();
                    }
                    if(EndMin != null && EndMin.length() == 1){
                        EndMin = EndMin+'0';
                    }
                    EndHour = string.valueOf(hour)+':'+EndMin+ ' PM ';
                }
                
               // timeslot = startHour+'-'+Endhour;
               arrivalTime = startHour;//+' - '+Endhour;
                
                if(rescWo.Early_Arrival_Time__c!= null){
                    startHour = String.valueOf(rescWo.Early_Arrival_Time__c.hour());
                    startMin = String.valueOf(rescWo.Early_Arrival_Time__c.Minute());
                }
                
                
                if(rescWo.Late_Arrival_Time__c != null){
                    EndHour = String.valueOf(rescWo.Late_Arrival_Time__c.hour());
                    EndMin = String.valueOf(rescWo.Late_Arrival_Time__c.Minute());
                }
                
                if(rescWo.Early_Arrival_Time__c != null && rescWo.Early_Arrival_Time__c.hour() < 12){
                    startHour = startHour+':'+startMin+ ' AM ';
                }else if(rescWo.Early_Arrival_Time__c != null){
                    if(rescWo.Early_Arrival_Time__c.hour() != 12){
                        hour  = rescWo.Early_Arrival_Time__c.hour() - 12;
                    }else{
                        hour  = rescWo.Early_Arrival_Time__c.hour();
                    }
                    if(startMin != null && startMin.length() == 1){
                        startMin = startMin+'0';
                    }
                    startHour = string.valueOf(hour)+':'+startMin+ ' PM ';
                }
                
                if(rescWo.Late_Arrival_Time__c != null && rescWo.Late_Arrival_Time__c .hour() < 12){
                    Endhour = Endhour +':'+EndMin+ ' AM ';
                }else if(rescWo.Late_Arrival_Time__c != null){
                    if(rescWo.Late_Arrival_Time__c.hour() != 12){
                        hour  = rescWo.Late_Arrival_Time__c.hour() - 12;
                    }else{
                        hour  = rescWo.Late_Arrival_Time__c.hour();
                    }
                    if(EndMin != null && EndMin.length() == 1){
                        EndMin = EndMin+'0';
                    }
                    EndHour = string.valueOf(hour)+':'+EndMin+ ' PM ';
                }
                timeslot = startHour+' - '+Endhour;
            }
        }
    }
    
    Map<String,dsmtCallOut.AppointmentOption> apptMap1 = null;
    
    Map<Integer,Map<String,dsmtCallOut.AppointmentOption>> newapptmap = new map<Integer,Map<String,dsmtCallOut.AppointmentOption>>();
    
    public string WoTypeId{get;set;}
    public string SelectedEmployee{get;set;}
    public List<SelectOption> apptMap{get;set;}
    
    
    public List<Selectoption> employeeOption{get;set;}
    public string SessionId;
    public boolean isError{get;set;}
    public string AppterrorMsg{get;set;}

    
    public void getAppt(){   
        DSMTracker_Log__c log = new DSMTracker_Log__c();
        String logstr = '';
       try{
        log.Time__c = Datetime.now();
        log.Type__c = 'Reschedule Get Appointments';
          
        List<Workorder_Type__c> woTypeList = [select id,Est_Total_Time__c from Workorder_Type__c where id =: woType];
        integer duration = 0;
        
        logstr += '---WoTypeId ---'+woTypeList.get(0) + '\n';
        
        if(woTypeList != null && woTypeList.size() > 0){
            duration = Integer.valueOf(woTypeList.get(0).Est_Total_Time__c);
        }    
        else{
            duration = 120;
        }
        logstr += '---duration---'+duration+ '\n';
        
        Date myDate = Date.Today().Adddays(2);
        if(custInteraction.Start_Date__c != null)
             myDate = Date.newInstance(custInteraction.Start_Date__c.year(), custInteraction.Start_Date__c.month(), custInteraction.Start_Date__c.day());

        Date myDate1 = Date.Today().Adddays(Integer.valueof(system.Label.Reschedule_Workorder_Days));
        if(custInteraction.End_Date__c != null)    
            myDate1 = Date.newInstance(custInteraction.End_Date__c.year(), custInteraction.End_Date__c.month(), custInteraction.End_Date__c.day());
        
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        dsmtRouteParser.AddressParser obj1 = null;
        
        string address = EncodingUtil.urlEncode(rescWo.Address__c+' '+rescWo.City__c+' '+rescwo.Zipcode__c, 'UTF-8');
        logstr += '---address---'+address+ '\n';
        
        req.setEndpoint(System_Config__c.getInstance().URL__c+'GetAddress?type=json&address='+Address+'&orgId='+userinfo.getOrganizationId());
        
        req.setMethod('GET');
        logstr += '---req---'+req+ '\n';
        
        http = new Http();
        if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
            logstr += '---res.getBody---'+res.getBody()+ '\n';
        }else{
        
            Map<String, String> body2 = new Map<String, String>();
           /* body2.put('value1', 'test');
            body2.put('value2', 'test');
            String sbody = JSON.serialize(body2);
            res.setBody(sbody); */
            
             body2.put('status','test');
                    body2.put('address','test');
                    body2.put('City','test');
                    body2.put('latitude','1.32');
                    body2.put('longitude','12.4');
                    body2.put('formatedAddress','test');
                    body2.put('state_short','test');
                    body2.put('postalCode','test');
                    String sbody = JSON.serialize(body2);
               res.setBody(sbody);
        }
        
        obj1 = (dsmtRouteParser.AddressParser) System.JSON.deserialize(res.getBody(), dsmtRouteParser.AddressParser.class);
        
        system.debug('--obj--'+obj1.formatedAddress);
        logstr += '---obj1.formatedAddress---'+obj1.formatedAddress+ '\n';
        
        dsmtCallOut.StaticSessionId = SessionId;

        if(dayOfWeekFilter == '[]'){
            dayOfWeekFilter = null;
        }else if(dayOfWeekFilter != null){
            dayOfWeekFilter = dayOfWeekFilter.replace(']','').replace('[','');
        }
        Set<Id> woTypeIdLst = new Set<Id>();
       woTypeIdLst.add(woType);
       if(woTypeIdLst != null && woTypeIdLst.size() > 0){
            list<Required_Skill__c> rsLst = [select id, Skill__c,Skill__r.Name,Minimum_Score__c from Required_Skill__c where Workorder_Type__c In :woTypeIdLst ];
            
            set<string> skillIds = new set<string>();
            
            for(Required_Skill__c r : rsLst){
                skillIds.add(r.Skill__c);
            }
            
            system.debug('--skillIds---'+skillIds);
            logstr += '---skillIds---'+skillIds+ '\n';
            
            list<Employee_Skill__c> esLst = [select id, Skill__c,Employee__c,Employee__r.Name,Survey_Score__c from Employee_Skill__c where skill__c in : skillIds];
    
            
            Map<Id,list<Employee_Skill__c>> esMap = new Map<Id,list<Employee_Skill__c>>();
            list<Employee_Skill__c> temp = null;
            
            for(Employee_Skill__c  es : esLst){
                    
                    if(esMap.get(es.Employee__c) != null){
                        temp = esMap.get(es.Employee__c);
                        temp.add(es);
                        esmap.put(es.Employee__c,temp);
                    }else{
                        temp = new list<Employee_Skill__c>();
                        temp.add(es);
                        esmap.put(es.Employee__c,temp);
                    }
            }
            system.debug('--esmap--'+esmap);
            logstr += '---esmap---'+esmap+ '\n';
            Set<Id> empSkill = new Set<Id>();
            boolean add = false;
            Set<Id> empIdset = new Set<Id>();
            boolean reqadd = false;
            string strEmpId ='';
            
           /* for(Id empId : esMap.keyset()){
                add = false;
                temp = esmap.get(empId);
                
                for(Required_Skill__c r : rsLst){
                    reqadd = false;
                    for(Employee_Skill__c  es : temp){
                        if(es.Survey_Score__c >= r.Minimum_Score__c){
                            if(r.Skill__c == es.Skill__c){
                                reqadd = true;
                            }
                        }
                    }
                    if(reqadd == false){
                    break;
                    }
                }
                if(!reqadd){
                    add = false;
                }else{
                    add = true;
                }
                
                if(add){
                    
                    if(strEmpId  == ''){
                        strEmpId = EmpId ;
                    }else{
                        strEmpId += '~~~'+ EmpId ;
                    }
                }
            }*/
             map<string, decimal> empSkillRateMap = new map<string, decimal>();

            
             for(Id empId : esMap.keyset()){
                add = false;
                temp = esmap.get(empId);
                decimal count = 0;
                for(Required_Skill__c r : rsLst){
                    reqadd = false;
                    for(Employee_Skill__c  es : temp){
                        if(es.Survey_Score__c >= r.Minimum_Score__c){
                            if(r.Skill__c == es.Skill__c){
                                count += es.Survey_Score__c;
                                reqadd = true;
                            }
                        }
                    }
                    if(reqadd == false){
                        break;
                    }
                }
                if(!reqadd){
                    add = false;
                }else{
                    add = true;
                }
                
                if(add){
                    empSkillRateMap.put(empId, count);
                }
            }
            
            system.debug('empSkillRateMap :::::'+ empSkillRateMap);
            
            if(empSkillRateMap.size() > 0){
                list<decimal> rateList = empSkillRateMap.values();
                rateList.sort();
                system.debug('rateList :::::'+ rateList);
                for(integer i = rateList.size()-1; i >= 0; i--){
                    for(string s : empSkillRateMap.keyset()){
                        if(rateList[i] == empSkillRateMap.get(s))
                        {
                            if(strEmpId  == ''){
                                strEmpId = s;
                            }else{
                                strEmpId += '~~~'+ s;
                            }
                        }
                    }
                }
            }
            
            SelectedEmployee = strEmpId ;
        }
        
        apptMap1  =  dsmtCallOut.GetAppoitments(SelectedEmployee,myDate,myDate1,obj1.formatedAddress.replace('#',''),duration,timeFilter,dayOfWeekFilter,null);
        
        system.debug('--apptMap1---'+apptMap1);
        logstr += '---apptMap1---'+apptMap1+ '\n';
        
        if(apptMap1.get('false') != null){
            showbutton = true;
            apptMap = new List<SelectOption>();
            isError = true;
            dsmtCallOut.AppointmentOption obj = apptMap1.get('false');  
            system.debug('--obj.error---'+obj.error); 
            logstr += '---obj.error---'+obj.error+ '\n';
            
            AppterrorMsg =   obj.error;
        }
        else{
            isError = false;
            
            system.debug('--apptMap1---'+apptMap1);
            apptMap = new List<SelectOption>();
            //boolean isSendEmail = false;
            //string strHtml = '<html><head><style>table.woCLS {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;} .woCLS td,.woCLS th {border: 1px solid #dddddd;text-align: left;padding: 8px;} .woCLS tr:nth-child(even) {background-color: #dddddd;}</style></head><table class="woCLS" border="1"><tr><th>Workorder #</th><th>Start Date</th><th>End Date</th></tr>';
            for(String str : apptMap1.keySet()){
                dsmtCallOut.AppointmentOption obj = apptMap1.get(str); 
                string strRes = dsmtAvailableAppointment.getAvailableAppoit1(obj);
               if(strRes == 'success'){ 
                system.debug('--obj.SessionId--'+obj.SessionId);
                system.debug('--obj---'+obj);
                logstr += '---obj--'+obj+ '\n';
                SessionId =  obj.SessionId;
                system.debug('--SessionId--'+SessionId );
                dsmtCallOut.StaticSessionId =  obj.SessionId;
                //apptMap.add(new SelectOption(str,obj.earlyArrivalTime+' - '+obj.LateArrivalTime + ' on '+ obj.ServiceDate));   
                String timeslotDescription = dsmtArrivalWindowCalculator.buildTimeslotDescription(obj.serviceDate, obj.serviceTime); 
                apptMap.add(new SelectOption(str, timeslotDescription));  
               }else{
                  //isSendEmail = true;
                  //strHtml +=strRes;
              } 
            }
            /*if(isSendEmail){
                strHtml += '</table></html>';
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                  List<String> sendTo = new List<String>();
                  //sendTo.add('hemanshu.patel89@gmail.com');
                  string reciever = system.Label.Email_Receiver_for_Overlap_WO;
                  sendTo = reciever.split(';');
                  mail.setToAddresses(sendTo);
                
                  mail.setSubject('Overlapping Work Order Report');
                  mail.setHtmlBody(strHtml);
                
                  mails.add(mail);
                  
                  if(!Test.isRunningTest())
                      Messaging.sendEmail(mails);
            }*/
        system.debug('---apptMap---'+apptMap);
        logstr += '---apptMap--'+apptMap+ '\n';
        
        }
         log.Log__c = logstr;
         upsert log;
        }catch(Exception e){
            logstr += '---Exception--'+e.GetMessage()+ '\n';
            log.Log__c = logstr;
            upsert log;
        }
    }
    
    
    public string NewWorkOrderId{get;set;}
    
    public void CreateWorkOrder(){
        if(apptMap1 != null){
            System.debug(apptMap1);
            dsmtCallOut.AppointmentOption obj = apptMap1.get(SelectedWorkTeam);   
            system.debug('--obj ---'+obj);
            
            Date ScheduledDate = rescWo.Scheduled_Date__c;
            //dsmtCallOut.ScheduleWorkOrder(obj.workOrderRefId,null,ScheduledDate ,obj.workTeamId,obj.serviceTimeSlot);
            
            String workOrderRefId = obj.workOrderRefId;
            String workTeamId  = obj.workTeamId;
            String timeSlot = obj.serviceTimeSlot;
            
            dsmtCallOut.ScheduleWorkOrderRequest swrkorder = new dsmtCallOut.ScheduleWorkOrderRequest();
        
            swrkorder.workOrderRefId = workOrderRefId ;
            swrkorder.workOrdreId = null;
            swrkorder.scheduleDate = ScheduledDate ;
            swrkorder.workTeamId = workTeamId;
            swrkorder.timeSlot = timeSlot;
            swrkorder.orgId = userinfo.getOrganizationId();
            String str1 = JSON.serialize(swrkorder);
            
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            
            req.setEndpoint(System_Config__c.getInstance().URL__c+'ScheduleWorkOrder?type=json');
            req.setMethod('POST');
            
            String body = str1;
            system.debug('--str+str1--'+body);
            req.setBody(body);
            req.setHeader('content-type', 'application/json');
            http = new Http();
            if(!test.isRunningTest()){
                res = http.send(req);
            }else{
                callMeBack=true;
            }
          //  System.debug(res.getBody());
            
            system.debug('--callMeBack--'+callMeBack);
            String str = '';
            String str2 = rescwo.External_Reference_ID__c;
            if(!callMeBack)
            {    
                if(str2!=null && str2 != '')
                {
                    str = dsmtWorkOrderHelperController.updateWorkOrder(rescWo.Id+'|'+rescwo.External_Reference_ID__c,'Rescheduled');
                }
                else
                {
                    str = dsmtWorkOrderHelperController.updateWorkOrder(rescWo.Id,'Rescheduled');
                }    
            }    
            
            if(str  == '"Success"' || callMeBack){
                try
                {
                    String woFields = dsmtMasSaveCustomerSearch.sObjectFields1('Workorder__c');
                    String qry = 'select '+woFields +' Id From Workorder__c where id  = \''+worderId+'\'';
                    
                    list<Workorder__c> woList = database.query(qry);
                    
                    Workorder__c newWo = null;
                    
                    if(woList != null && woList.size() > 0){
                        woList.get(0).Reschedule_Reason__c = rescWO.Reschedule_Reason__c;
                        woList.get(0).Reschedule_Notes__c = rescWO.Reschedule_Notes__c;
                        woList.get(0).Status__c = 'Rescheduled';
                        update woList;
                        
                        newwo =  woList.get(0).clone(false,false);
                        newwo.Parent_WorkOrder__c = woList.get(0).Id;
                        newWo.Reschedule_Reason__c = rescWO.Reschedule_Reason__c;
                        newWo.Reschedule_Notes__c = rescWO.Reschedule_Notes__c;
                        newWo.Work_Team__c = obj.workTeamId;
                        newWo.External_Reference_ID__c = obj.workOrderRefId;
                        newwo.Call_me_back_to_reschedule__c  = false;
                        Integer year = integer.valueOf(obj.serviceDate.split('/').get(2));
                        Integer month = integer.valueOf(obj.serviceDate.split('/').get(0));
                        Integer day  = integer.valueOf(obj.serviceDate.split('/').get(1));
                        Integer hour = integer.valueOf(obj.serviceTime.split(':').get(0));
                        Integer min = integer.valueOf(obj.serviceTime.split(':').get(1));
                        
                        datetime StartDate = datetime.newInstance(year,month,day,hour,min,0);
                        datetime EndDate = StartDate.AddMinutes(integer.valueOf(obj.serviceDuration));
                        
                        newwo.Requested_Start_Date__c = StartDate;
                        newwo.Requested_End_Date__c = EndDate;
                        newwo.Requested_Date__c = StartDate.Date();
                        
                        newwo.Early_Arrival_Time__c = StartDate.AddMinutes(-30);
                        newwo.Late_Arrival_Time__c = StartDate.AddMinutes(30);
                        
                        newwo.Scheduled_Start_Date__c = StartDate;
                        newwo.Scheduled_End_Date__c = EndDate;
                        newwo.Scheduled_Date__c = newwo.Requested_Date__c;
                        newwo.Status__c = 'Scheduled';
                        
                        newwo.Energy_Assessment__c = ApexPages.currentPage().getParameters().get('assessId');
                        
                        Id RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('WorkOrder').getRecordTypeId();
                        
                        newwo.RecordTypeId = RecordTypeId;
                        insert newWo;
                        
                        dsmtCallOut.ScheduleWorkOrder(obj.workOrderRefId,newwo.Id,newwo.Scheduled_Date__c,obj.workTeamId,obj.serviceTimeSlot);
                        NewWorkOrderId = newWo.Id;
                    }
                }
                catch(Exception ex){
                    DSMTracker_Log__c log = new DSMTracker_Log__c();
                String logstr = '';
                log.Time__c = Datetime.now();
                log.Type__c = 'Overlap Appointment';
                
                logstr +='---Exception--'+ex.GetMessage()+ '\n';
                if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                    logstr +='---Ovelapped Workorder--'+ex.getMessage().split('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION,')[1];
                log.Log__c = logstr;
                upsert log;
                
                List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    
                      List<String> sendTo = new List<String>();
                      //sendTo.add('hemanshu.patel89@gmail.com');
                      string reciever = system.Label.Email_Receiver_for_Overlap_WO;
                      sendTo = reciever.split(';');
                      mail.setToAddresses(sendTo);
                    
                      mail.setSubject('Overlapping Work Order Report');
                      if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                          mail.setHtmlBody(ex.getMessage().split('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION,')[1]);
                      else
                          mail.setHtmlBody(ex.getMessage());
                    
                      mails.add(mail);
                      
                      if(!Test.isRunningTest())
                          Messaging.sendEmail(mails);
        
                    ApexPages.getMessages().clear();
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Invalid_Workorder_Message);
                    system.debug('@@myMsg@@'+myMsg);
                    
                    ApexPages.addMessage(myMsg);
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                }
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, str);
                ApexPages.addMessage(myMsg);
            }
        }
    }
    
    public string error{get;set;}
    
    public void RescheduleWorkOrder(){
        String woFields = dsmtMasSaveCustomerSearch.sObjectFields1('Workorder__c');
        String qry = 'select '+woFields +' Id From Workorder__c where id  = \''+worderId+'\'';
        
        list<Workorder__c> woList = database.query(qry);
        
        Workorder__c newWo = null;
        
        if(woList != null && woList.size() > 0){
            
            woList.get(0).Reschedule_Reason__c = rescWO.Reschedule_Reason__c;
            woList.get(0).Reschedule_Notes__c = rescWO.Reschedule_Notes__c;
            woList.get(0).Status__c = 'Rescheduled';
            update woList;
            
            newwo =  woList.get(0).clone(false,false);
            newwo.Parent_WorkOrder__c = woList.get(0).Id;
            newWo.Reschedule_Reason__c = null;
            newWo.Reschedule_Notes__c = null;
            newWo.Work_Team__c = null;
            newWo.External_Reference_ID__c = null;
            newwo.Requested_Start_Date__c = null;
            newwo.Requested_End_Date__c = null;
            newwo.Requested_Date__c = null;
            
            newwo.Early_Arrival_Time__c = DateTime.newInstance(1700, 1, 1);
            newwo.Late_Arrival_Time__c = DateTime.newInstance(2400, 12, 31);
            
            newwo.Scheduled_Start_Date__c = null;
            newwo.Scheduled_End_Date__c = null;
            newwo.Scheduled_Date__c = null;
            newwo.Status__c = 'Unscheduled';
            newWo.Call_me_back_to_reschedule__c = true;
            newwo.Reschedule_Callback_Date__c  = rescWO.Reschedule_Callback_Date__c;
            newwo.Energy_Assessment__c = ApexPages.currentPage().getParameters().get('assessId');
            Id RecordTypeId = Schema.SObjectType.Workorder__c.getRecordTypeInfosByName().get('WorkOrder').getRecordTypeId();
            
            newwo.RecordTypeId = RecordTypeId;
            insert newWo;
            
            NewWorkOrderId = newWo.Id;
        }
    }
    
    @RemoteAction
    public static string ValidateAddress(string woId){
        DSMTracker_Log__c log = new DSMTracker_Log__c();
        try{      
            
            list<Workorder__c> woList = [select id, Address__c, City__c, State__c, Zipcode__c
                                         from Workorder__c
                                         where id =: woId];
            
            if(woList.size() > 0)
            {
                String Address1, City, Zip;
                
                Address1 = woList[0].Address__c;
                City = woList[0].City__c;
                Zip = woList[0].Zipcode__c;
                
                log.Time__c = Datetime.now();
                log.Type__c = 'Address Validation';
                  
                HttpRequest req = new HttpRequest();
                Http http = new Http();
                HTTPResponse res = new HTTPResponse();
                dsmtRouteParser.AddressParser obj1 = null;
                
                string address = EncodingUtil.urlEncode(Address1  +' '+City+' '+Zip, 'UTF-8');
                req.setEndpoint(System_Config__c.getInstance().URL__c+'GetAddress?type=json&address='+Address+'&orgId='+userinfo.getOrganizationId());
                req.setMethod('GET');
                
                log.Request__c = req.getBody();
                
                http = new Http();
                if(!Test.isRunningTest()){
                    res = http.send(req);
                }else{
                 Map<String, String> boady2 = new Map<String, String>();
                    boady2.put('status','test');
                    boady2.put('address','test');
                    boady2.put('City','test');
                    boady2.put('latitude','1.32');
                    boady2.put('longitude','12.4');
                    boady2.put('formatedAddress','test');
                    boady2.put('state_short','test');
                    boady2.put('postalCode','test');
                    String sbody = JSON.serialize(boady2);
                    res.setBody(sbody);
                    
                }
                
                log.Response__c = res.getBody();
                //  System.debug(res.getBody());
                
                obj1 = (dsmtRouteParser.AddressParser) System.JSON.deserialize(res.getBody(), dsmtRouteParser.AddressParser.class);
                
                system.debug('--obj--'+obj1.formatedAddress); 
            
                if(obj1.postalCode != Zip){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Invalid_Address);
                    ApexPages.addMessage(myMsg); 
                    return system.label.Invalid_Address;
                }
            }
        }catch(Exception e){
            system.debug('--e---'+e);
            log.Response__c = e.getMessage();
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Invalid_Address);
            ApexPages.addMessage(myMsg);
            insert log;
            //return 'Address Is Invalid';
            return system.label.Invalid_Address;
        }
        insert log;
        return 'Valid';
    }
    
}