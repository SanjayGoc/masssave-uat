public with sharing class AtticThermalEnvelopeTypeModel{
    public String visiblePanel{get;set;}
    public Thermal_Envelope_Type__c envelopeType{get;set;}
    //public Attic_Space_Work_Order_Data__c aswod{get;set;}
    public Attic_Venting__c atticVenting{get;set;}
    public String fieldUniqueDataId{get;set;}
    public Integer atticAWHFIndex{get;set;}
    
    public AtticThermalEnvelopeTypeModel(Thermal_Envelope_Type__c envelopeType,Attic_Space_Work_Order_Data__c aswod,Attic_Venting__c atticVenting)
    {
        this.envelopeType = envelopeType;
        //this.aswod = aswod;
        this.atticVenting = atticVenting;
        this.updateAtticChildComponents();
        refreshAtticPanel();
    }
    
    public void refreshAtticPanel()
    {
        System.debug('VISIBLE PANEL : ' + visiblePanel);
        //save(); // 12298 | Kaushik Rathore | 02 Sept, 2018
        
        queryAtticWalls();
        queryAtticRoofs();
        queryAtticFloors();
    }
    
    private void updateAtticChildComponents()
    {
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeFloors(envelopeType.Id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeWalls(envelopeType.Id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeRoofs(envelopeType.Id);
    }
    
    public void save()
    {
        //aswod = aswod.clone(true);
        //UPDATE aswod;
        String assessId = ApexPages.currentPage().getParameters().get('assessId');
        atticVenting = atticVenting.clone(true);
        atticVenting.Energy_Assessment__c = assessId;
        UPDATE atticVenting;
        //saveAtticVentSets();
        saveAtticAWHFs();
        //this.updateAtticChildComponents();
    }
    
    /* ATTIC WALLS -------------------------------------------------------------------------------------------------------*/
    public transient List<Wall__c> atticWalls{get;set;}
    public Wall__c editAtticWall{get;set;}
    public transient Wall__c newAtticWall{get;set;}
    
    public void queryAtticWalls()
    {
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'AtticHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            atticWalls = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            atticWalls = new List<Wall__c>();
        }
    }
    
    public void addNewAtticWall(String wallType ,String exposedTo)
    {
        if(envelopeType != null && envelopeType.Id != null){
            /* DSST-12295 | Kaushik Rathore | 8 Oct, 2018 : START*/
            Thermal_Envelope_Type__c tet = [SELECT Id,Next_Attic_Wall_Number__c FROM Thermal_Envelope_Type__c WHERE Id=:envelopeType.Id LIMIT 1];
            Decimal nextNumber = (tet.Next_Attic_Wall_Number__c != null) ? tet.Next_Attic_Wall_Number__c : 1;
            /* DSST-12295 | Kaushik Rathore | 8 Oct, 2018 : END*/
            
            newAtticWall = new Wall__c();
            newAtticWall.Thermal_Envelope_Type__c = envelopeType.Id;
            newAtticWall.Name = 'Attic Wall ' + nextNumber;
            newAtticWall.Exposed_To__c = exposedTo;
            newAtticWall.Add__c = wallType; 
               
            INSERT newAtticWall;
            if(newAtticWall.Id != null){
                //envelopeType.Next_Attic_Wall_Number__c = nextNumber + 1;
                //UPDATE envelopeType;
                
                updateThermalEnvelopeForWall(nextNumber, envelopeType.Id);
                
                if(updateAtticWallById(newAtticWall.Id) != null){
                    queryAtticWallById(newAtticWall.Id);
                }
            }
        }
        refreshAtticPanel();
    }
    
    /*
    New Method For DSST-10315
    Added by: Prasanjeet Sharma
    */
    @future
    public static void updateThermalEnvelopeForWall(decimal nextNumber, String envId)
    {
        Thermal_Envelope_Type__c envelopeType = new Thermal_Envelope_Type__c(id = envId);
        envelopeType.Next_Attic_Wall_Number__c = nextNumber + 1;
        UPDATE envelopeType;
    }
    
    public Wall__c updateAtticWallById(String wallId)
    {
        if(wallId != null){
            Wall__c wall = new Wall__c(Id = wallId);
            UPDATE [SELECT Id FROM Layer__c WHERE Wall__c =:wall.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE wall;
            return wall;
        }
        return null;
    }
    
    public void queryAtticWallById(String wallId)
    {
        if(wallId != null){
            List<Wall__c> walls = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Id=:wallId');
            if(walls.size() > 0){
                editAtticWall = walls.get(0);
                visiblePanel = 'AtticWallPanel';
            }
        }
    }
    
    public void deleteAtticWallById(String wallId)
    {
        if(wallId != null){
            DELETE new Wall__c(Id = wallId);
        }
        refreshAtticPanel();
    }
    
    /* ATTIC ROOFS ------------------------------------------------------------------------------------------------------------*/
    public transient List<Ceiling__c> atticRoofs{get;set;}
    public Ceiling__c editAtticRoof{get;set;}
    public transient Ceiling__c newAtticRoof{get;set;}
    
    public void queryAtticRoofs()
    {
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'AtticHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            atticRoofs = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            atticRoofs = new List<Ceiling__c>();
        }
    }
    
    public void addNewAtticRoof(String roofType ,String exposedTo, boolean isRoofFlat)
    {
        if(envelopeType != null && envelopeType.Id != null){
            /* DSST-12295 | Kaushik Rathore | 8 Oct, 2018 : START*/
            Thermal_Envelope_Type__c tet = [SELECT Id,Next_Attic_Roof_Number__c FROM Thermal_Envelope_Type__c WHERE Id=:envelopeType.Id LIMIT 1];
            Decimal nextNumber = (tet.Next_Attic_Roof_Number__c != null) ? tet.Next_Attic_Roof_Number__c : 1;
            /* DSST-12295 | Kaushik Rathore | 8 Oct, 2018 : END*/
            newAtticRoof = new Ceiling__c();
            newAtticRoof.Thermal_Envelope_Type__c = envelopeType.Id;
            newAtticRoof.Name = 'Attic Roof ' + nextNumber;
            newAtticRoof.Exposed_To__c = exposedTo;
            newAtticRoof.Add__c = roofType; 
            newAtticRoof.Roof_Is_Flat__c = isRoofFlat;
               
            INSERT newAtticRoof;
            if(newAtticRoof.Id != null){
                //envelopeType.Next_Attic_Roof_Number__c = nextNumber + 1;
                //UPDATE envelopeType;
                
                updateThermalEnvelopeForRoof(nextNumber, envelopeType.Id);
                
                if(updateAtticRoofById(newAtticRoof.Id) != null){
                    queryAtticRoofById(newAtticRoof.Id);
                }
            }
        }
        refreshAtticPanel();
    }
    
    /*
    New Method For DSST-9482
    Added by: Prasanjeet Sharma
    */
    @future
    public static void updateThermalEnvelopeForRoof(decimal nextNumber, String envId)
    {
        Thermal_Envelope_Type__c envelopeType = new Thermal_Envelope_Type__c(id = envId);
        envelopeType.Next_Attic_Roof_Number__c = nextNumber + 1;
        UPDATE envelopeType;
    }
    
    public Ceiling__c updateAtticRoofById(String roofId)
    {
        if(roofId != null){
            Ceiling__c roof = new Ceiling__c(Id = roofId);
            UPDATE [SELECT Id FROM Layer__c WHERE Ceiling__c =:roof.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE roof;
            return roof;
        }
        return null;
    }
    
    public void queryAtticRoofById(String roofId)
    {
        if(roofId != null){
            List<Ceiling__c> roofs = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Id=:roofId');
            if(roofs.size() > 0){
                editAtticRoof = roofs.get(0);
                visiblePanel = 'AtticRoofPanel';
            }
        }
    }
    
    public void deleteAtticRoofById(String roofId)
    {
        if(roofId != null){
            DELETE new Ceiling__c(Id = roofId);
        }
        refreshAtticPanel();
    }
    
    /* ATTIC FLOORS -----------------------------------------------------------------------------------------------------------*/
    public transient List<Floor__c> atticFloors{get;set;}
    public Floor__c editAtticFloor{get;set;}
    public transient Floor__c newAtticFloor{get;set;}
    
    public void queryAtticFloors()
    {
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'AtticHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            atticFloors = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Floor__c') + ' FROM Floor__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            atticFloors = new List<Floor__c>();
        }
    }
    
    public void addNewAtticFloor(String floorType ,String exposedTo)
    {
        if(envelopeType != null && envelopeType.Id != null){
            /* DSST-12295 | Kaushik Rathore | 8 Oct, 2018 : START*/
            Thermal_Envelope_Type__c tet = [SELECT Id,Next_Attic_Floor_Number__c FROM Thermal_Envelope_Type__c WHERE Id=:envelopeType.Id LIMIT 1];
            Decimal nextNumber = (tet.Next_Attic_Floor_Number__c != null) ? tet.Next_Attic_Floor_Number__c : 1;
            /* DSST-12295 | Kaushik Rathore | 8 Oct, 2018 : END*/
            newAtticFloor = new Floor__c();
            newAtticFloor.Thermal_Envelope_Type__c = envelopeType.Id;
            newAtticFloor.Name = 'Attic Floor ' + nextNumber;
            newAtticFloor.Exposed_To__c = exposedTo;
            newAtticFloor.Add__c = floorType; 
               
            INSERT newAtticFloor;
            if(newAtticFloor.Id != null)
            {
                /*
                New Method For DSST-7806
                Sub Task : DSST-9913 (Too Many SOQL)
                */
                //envelopeType.Next_Attic_Floor_Number__c = nextNumber + 1;
                //UPDATE envelopeType;
                updateThermalEnvelope(nextNumber, envelopeType.Id);
                
                if(updateAtticFloorById(newAtticFloor.Id) != null){
                    queryAtticFloorById(newAtticFloor.Id);
                }
            }
        }
        refreshAtticPanel();
    }
    
    /*
    New Method For DSST-7806
    Sub Task : DSST-9913 (Too Many SOQL)
    */
    @future
    public static void updateThermalEnvelope(decimal nextNumber, String envId)
    {
        Thermal_Envelope_Type__c envelopeType = new Thermal_Envelope_Type__c(id = envId);
        envelopeType.Next_Attic_Floor_Number__c = nextNumber + 1;
        UPDATE envelopeType;
    }
    
    public Floor__c updateAtticFloorById(String floorId)
    {
        if(floorId != null){
            Floor__c floor = new Floor__c(Id = floorId);
            UPDATE [SELECT Id FROM Layer__c WHERE Floor__c =:floor.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE floor;
            return floor;
        }
        return null;
    }
    
    public void queryAtticFloorById(String floorId)
    {
        if(floorId != null){
            List<Floor__c> floors = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Floor__c') + ' FROM Floor__c WHERE Id=:floorId');
            if(floors.size() > 0){
                editAtticFloor = floors.get(0);
                visiblePanel = 'AtticFloorPanel';
            }
        }
    }
    
    public void deleteAtticFloorById(String floorId)
    {
        if(floorId != null){
            DELETE new Floor__c(Id = floorId);
        }
        refreshAtticPanel();
    }
    
    /* ATTIC VENTING -----------------------------------------------------------------------------------------------------------*/
    /*public List<Attic_Vent_Set__c> atticVentSets{get{
        if(atticVentSets == null){
            queryAtticVentingSets();
        }
        return atticVentSets;
    }set;}
    public void queryAtticVentingSets(){
        atticVentSets = new List<Attic_Vent_Set__c>();
        if(this.atticVenting != null && this.atticVenting.Id != null){
            Id atticVentingId = atticVenting.Id;
            atticVentSets = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Attic_Vent_Set__c') + ' FROM Attic_Vent_Set__c WHERE Attic_Venting__c=:atticVentingId');
        }
    }  
    public void saveAtticVentSets(){
        if(atticVentSets.size() > 0){
            UPSERT atticVentSets;
            queryAtticVentingSets();
        }
    }
    public void addNewAtticVentSet(){
        saveAtticVentSets();
        if(atticVenting != null && atticVenting.Id != null){
            Attic_Vent_Set__c newAtticVentSet = new Attic_Vent_Set__c(
                Attic_Venting__c = atticVenting.Id,
                Name = 'Attic Vent Set ' + (atticVentSets.size() + 1)
            );
            INSERT newAtticVentSet;
            if(atticVentSets == null){
                atticVentSets = new List<Attic_Vent_Set__c>();
            }
            atticVentSets.add(newAtticVentSet);
        }
    }
    
    public void deleteAtticVentingSet(){
        saveAtticVentSets();
        String atticVentSetIndexStr = ApexPages.currentPage().getParameters().get('atticVentSetIndex');
        if(atticVentSetIndexStr != null && atticVentSetIndexStr.trim() != ''){
            Attic_Vent_Set__c atticVentSet = atticVentSets.get(Integer.valueOf(atticVentSetIndexStr));
            if(atticVentSet != null){
                DELETE new Attic_Vent_Set__c(Id = atticVentSet.Id);
                atticVentSets.remove(Integer.valueOf(atticVentSetIndexStr));
            }
        }
    }*/
    
    /* ATTIC ACCESS AND WHOLE HOUSE FANS --------------------------------------------------------------------------------*/
    public List<Attic_Access_and_Whole_House_Fan__c> atticAWHFs{get{
        if(atticAWHFs == null){
            queryAtticAWHFs();
        }
        return atticAWHFs;
    }set;}
    
    public void queryAtticAWHFs()
    {
        atticAWHFs = new List<Attic_Access_and_Whole_House_Fan__c>();
        if(envelopeType != null && envelopeType.Id != null){
            String atticId = envelopeType.Id;
            String SOQL = 'SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Attic_Access_and_Whole_House_Fan__c') + ' FROM Attic_Access_and_Whole_House_Fan__c WHERE Thermal_Envelope_Type__c =:atticId';        
            System.debug('@@@ SOQL : ' + SOQL);
            atticAWHFs = Database.query(SOQL);
        }
    }
    
    public void addNewAtticAWHF()
    {
        if(envelopeType != null && envelopeType.Id != null){
            saveAtticAWHFs();
            Attic_Access_and_Whole_House_Fan__c newAtticAWHF = new Attic_Access_and_Whole_House_Fan__c(
                Thermal_Envelope_Type__c = envelopeType.Id,
                Name = 'Access and Whole House Fan ' + (atticAWHFs.size() + 1),
                Access_Type__c = 'Ceiling Hatch',
                Location__c = 'Living Space'
            );
            INSERT newAtticAWHF;
            if(newAtticAWHF.Id != null){
                CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeFloors(envelopeType.Id);
                CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeRoofs(envelopeType.Id);
            }
            queryAtticAWHFs();
        }
    }
    
    public void deleteAtticAWHF()
    {
        String AtticAWHFIndexStr = ApexPages.currentPage().getParameters().get('AtticAWHFIndex');
        if(AtticAWHFIndexStr != null){
            saveAtticAWHFs();
            Integer AtticAWHFIndex = Integer.valueOf(AtticAWHFIndexStr);
            DELETE new Attic_Access_and_Whole_House_Fan__c(Id = atticAWHFs.get(AtticAWHFIndex).Id);
            atticAWHFs.remove(AtticAWHFIndex);
        }
    }
    
    public void saveAtticAWHFs()
    {
        if(atticAWHFs != null && atticAWHFs.size() > 0){
            UPSERT atticAWHFs;
            queryAtticAWHFs();
        }
    }
    /*-------------------------------------------------------------------------------------------------------------------------*/
    private String getPageParam(String key)
    {
        return ApexPages.currentPage().getParameters().get('key');
    }
    
    public Component.Apex.OutputPanel getDynamicComponent()
    {
        Component.Apex.OutputPanel panel = new Component.Apex.OutputPanel();
        panel.layout = 'block';
        if(visiblePanel == 'AtticWallPanel'){
            Component.c.AtticWallComponent wall = new Component.c.AtticWallComponent();
            wall.atticWallObjId = editAtticWall.Id;
            wall.onSave = 'showAtticPanel("AtticHomePanel");';
            panel.childComponents.add(wall);
        }else if(visiblePanel == 'AtticRoofPanel'){
            Component.c.AtticRoofComponent roof= new Component.c.AtticRoofComponent();
            roof.atticRoofObjId = editAtticRoof.Id;
            roof.onSave = 'showAtticPanel("AtticHomePanel");';
            panel.childComponents.add(roof);
        }else if(visiblePanel == 'AtticFloorPanel'){
            Component.c.AtticFloorComponent floor = new Component.c.AtticFloorComponent();
            floor.atticFloorObjId = editAtticFloor.Id;
            floor.onSave = 'showAtticPanel("AtticHomePanel");';
            panel.childComponents.add(floor);
        }else{
            panel.rendered=false;
        }
        return panel;
    }
    
    public void saveDynamicComponent()
    {
        if(visiblePanel == 'AtticWallPanel'){
            UPSERT editAtticWall;
        }else if(visiblePanel == 'AtticRoofPanel'){
            UPSERT editAtticRoof;
        }else if(visiblePanel == 'AtticFloorPanel'){
            UPSERT editAtticFloor;
        }
    }
}