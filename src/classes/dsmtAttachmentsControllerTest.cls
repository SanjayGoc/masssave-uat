@isTest
private class dsmtAttachmentsControllerTest {
	
	@isTest static void test_method_one() {
		
        Energy_Assessment__c testEA=Datagenerator.setupAssessment();
        PageReference dsmtAttachments = Page.dsmtAttachments;
        dsmtAttachments.getParameters().put('assessid',testEA.Id); 
        
        Test.startTest();
        Test.setCurrentPage(dsmtAttachments);
		dsmtAttachmentsController cntrl = new dsmtAttachmentsController();
		List<Attachment__c> attachments = cntrl.getEAAttachments();
		Test.stopTest();

	}
	
}