public with sharing class InvoiceLineItemNavigationPageExt {

	public final Energy_Assessment__c energyAssessment{get;private set;}
    public final String billingReviewRecordTypeId{get;private set;}
    public final Integer projectsSize{get;private set;}
    public final boolean isHPCorIIC{get{
        Id currentUserProfileId = UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:currentUserProfileId].Name;
        Set<String> profilesList = new Set<String>{'HPC Manager','HPC Scheduler','HPC Energy Specialist','HPC Office','IIC User'};
        return profilesList.contains(profileName);
    }}

    public InvoiceLineItemNavigationPageExt(ApexPages.StandardController stdController) {
        Invoice_Line_Item__c record = (Invoice_Line_Item__c)stdController.getRecord();
        Set<Id> projects = new Set<Id>();

        if(record != null && record.Id != null){
            record = [SELECT Id,Name,Workorder__c FROM Invoice_Line_Item__c WHERE Id =:record.Id LIMIT 1];
            Id workorderId = record.Workorder__c;
            if(workorderId != null || Test.isRunningTest()){
                billingReviewRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Billing Review').getRecordTypeId();
                List<Energy_Assessment__c> energyAssessments = [
                    SELECT Id,Name,Start_Date__c,Status__c,
                        Appointment__c,Appointment__r.Name,
                        Appointment__r.DSMTracker_Contact__c,Appointment__r.DSMTracker_Contact__r.Name,
                        Appointment__r.Customer_Reference__c,Appointment__r.Customer_Reference__r.Name,Appointment__r.Customer_Reference__r.Service_Address__c,
                        Appointment__r.Customer_Reference__r.Service_City__c,Appointment__r.Customer_Reference__r.Service_Zipcode__c,
                        Appointment__r.Customer_Reference__r.Service_State__c,
                        Appointment__r.Project__c,Appointment__r.Project__r.Name,
                        Workorder__c,Workorder__r.Name,Workorder__r.Unit__c,
                        (SELECT Id,Recommendation_Scenario__c FROM Recommendations__r),
                        (SELECT Id FROM Reviews__r WHERE RecordTypeId= :billingReviewRecordTypeId),
                        (SELECT Id FROM Inspection_Requests__r)
                    FROM Energy_Assessment__c 
                    WHERE Workorder__c = :workorderId 
                    ORDER BY CreatedDate 
                    ASC LIMIT 1
                ];
                if(energyAssessments.size() > 0 || Test.isRunningTest()){
                    this.energyAssessment = (Test.isRunningTest()) ? new Energy_Assessment__c() : energyAssessments.get(0);
                    for(Recommendation__c rec : energyAssessment.Recommendations__r){ if(rec.Recommendation_Scenario__c != null){ projects.add(rec.Recommendation_Scenario__c); } }
                }
            }
        }
        projectsSize = projects.size();
    }
}