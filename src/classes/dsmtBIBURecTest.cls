@isTest
public class dsmtBIBURecTest {
    @istest
    public static void dsmtBIBURec(){
        
         dsmtEAModel.Surface newSurfaceObj = new dsmtEAModel.Surface();
        newSurfaceObj.EAObj = new Energy_Assessment__c();    
        newSurfaceObj.rcmd = new Recommendation__c();

        
        Energy_Assessment__c ea=Datagenerator.setupAssessment();
    test.startTest();
        Set<Id> projId = new Set<Id>();
        Set<Id> eaIds = new Set<Id>();
        
        Map<Id,Exception__c> eaExcMap = new Map<Id,Exception__c>();
        
        Map<Id,Recommendation_Scenario__c> recMap = new Map<Id,Recommendation_Scenario__c>();
        
        Trigger_Configuration__c tc = new Trigger_Configuration__c();
        tc.Default_Recommendation_Saving__c = true;
        insert tc;
        
        Recommendation_Scenario__c rs = new Recommendation_Scenario__c();
        //rs.Energy_Assessment__c = ea.id;
        //rs.Override_Special_Incentive__c = false;
        rs.Status__c = 'Installed';
        insert rs;
        
        Exception__c ex = new Exception__c();
        ex.Energy_Assessment__c = ea.id;
        insert ex;
        
        Saving_Constant__c sc = new Saving_Constant__c();
        sc.Kwh_To_BTUH__c = 1.00;
        sc.DefaultDoorWidth__c = 1.00;
        sc.DefaultDoorHeight__c = 1.00;
        sc.FrontBackToLeftRightWindowAreaFactor__c = 1.00;
        insert sc;
        
        Thermal_Envelope__c te = new Thermal_Envelope__c();
        insert te;
       
        Id RecordTypeId1 = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Basement').getRecordTypeId();
        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        tet.Energy_Assessment__c = ea.Id;
        tet.Name = 'Windows';
        tet.Thermal_Envelope__c =te.id; 
        tet.RecordTypeId = RecordTypeId1;
        tet.Net_Area__c = 1.00;
        tet.AreaBack__c = 1.00;
        tet.AreaFront__c = 1.00;
        tet.AreaLeft__c = 1.00;
        tet.AreaRight__c = 1.00;
        tet.FrontWindowQualitativeAmount__c = '';
        insert tet;
        
        
        //Id devRecordTypeId = Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().get('Air Sealing Recommendation').getRecordTypeId();
        Recommendation__c rec = new Recommendation__c();
        rec.Energy_Assessment__c = ea.id;
        //rec.RecordTypeId = devRecordTypeId;
        rec.Recommendation_Scenario__c = rs.id;
        rec.Total_Recommended_Therms_BTUh_Savings__c = 1.00;
        rec.Total_Recommended_Therms_Savings__c = 1.00;
        rec.Kwh__c = 1.00;
        rec.KW__c = 1.00;
        rec.BTU__c = 1.00;
        rec.Total_Recommended_Incentive__c = 1.00;
        rec.Total_Recommended_Kwh_Savings__c = 1.00;
        rec.Total_Recommended_KW_Savings__c = 1.00;
        rec.Total_Recommended_BTUh_Savings__c = 1.00;
        rec.Total_Recommended_Savings__c = 1.00;
        rec.Total_Rec_Inc_After_Special_Incentive__c = 1.00;
        rec.Quantity__c = 1.00;
        rec.Units__c = '1.00';
        rec.SubCategory__c = 'Water Fixture';
        rec.status__c = 'Recommended';
        rec.IsChangeorder__c = true;
        rec.Change_Order_Quantity__c = 1.00;
        insert rec;
        
        
    }
}