public with sharing class dsmtInspectLineItems{
    
    public string irId;
    
    public string lineItemXml {
        get;
        set;
    }
    public String selectedRowsValues {get;set;}
    public List<Inspection_Line_Item__c> lstILI;
    
    public dsmtInspectLineItems(){
        selectedRowsValues = '';
        irId = ApexPages.currentPage().getParameters().get('id');
        lstILI = new List<Inspection_Line_Item__c>();
        lstILI = queryInspectionLI();
        loadLineItemXml();
    }
    
    public void init(){
        lstILI = queryInspectionLI();
    }
    
    //LoadXml
    public void loadLineItemXml() {
        //irId = ApexPages.currentPage().getParameters().get('id');
        lineItemXml = '<LineItems>';
        
        for (Inspection_Line_Item__c aCLLI: lstILI) {
                lineItemXml += '<LineItem>';
                lineItemXml += '<Checked>' + false + '</Checked>';
                lineItemXml += '<LineItemId><![CDATA[' + aCLLI.Id + ']]></LineItemId>';

                lineItemXml += '<Name><![CDATA[' + (aCLLI.Name != null ? aCLLI.Name + '' : '') + ']]></Name>';
                lineItemXml += '<Quantity><![CDATA[' + (aCLLI.Inspected_Quantity__c != null ? aCLLI.Inspected_Quantity__c : 0 ) + ']]></Quantity>';
                lineItemXml += '<InstalledQuantity><![CDATA[' + (aCLLI.Installed_Quantity__c != null ? aCLLI.Installed_Quantity__c : 0) + ']]></InstalledQuantity>';
                
                lineItemXml += '<MeasureName><![CDATA[' + checkStringNULL(aCLLI.Measure_Name__c) + ']]></MeasureName>';
                lineItemXml += '<Zipcode><![CDATA[' + checkStringNULL(aCLLI.Zip__c) + ']]></Zipcode>';
                

                lineItemXml += '</LineItem>';
            
        }
        lineItemXml += '</LineItems>';
    }
    
    private string checkStringNULL(string stringVal) {
        if (stringVal != null) {
            return stringVal.replace('<', '&lt;').replace('>', '&gt;');
        } else {
            return '';
        }
    }
    
    public List<Inspection_Line_Item__c> queryInspectionLI(){
        
        String query = 'select ' + sObjectFields('Inspection_Line_Item__c') +' Id from Inspection_Line_Item__c where Inspection_Request__c = :irId order by CreatedDate Desc limit 300 ';
        
        return Database.query(query);
    }
    
    private static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
    
    public PageReference save(){
        List<Inspection_Line_Item__c> lstUpdateILI = new List<Inspection_Line_Item__c>();
        
        List<String>lstRows = selectedRowsValues.split('\n');
        //Payment__c payment = new Payment__c(Amount__c =paymentRequest.Requested_Amount__c,Enrollment_Application__c=paymentRequest.Enrollment_Application__c  );
        
        if(lstRows.size()>0){
            try{
                
                //insert payment;
                for(String row : lstRows){
                    List<String>cols = row.split(',');
                    if(cols.size()==2){
                        Inspection_Line_Item__c pli = new Inspection_Line_Item__c();
                        pli.Id =cols[0]; 
                        if(cols[1]== '' || cols[1].trim().length()==0)
                            pli.Installed_Quantity__c = 0;
                        else
                            pli.Installed_Quantity__c =Decimal.valueOf(cols[1]);
                        
                        lstUpdateILI.add(pli); 
                        
                    }
                }
                if(lstUpdateILI.size()>0){
                    update lstUpdateILI ;
                    
                }
                return new PageReference ('/'+irId);
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
                return null;
            }
        }
        return null;
    }
}