@isTest
public class dsmtBatchCreateWorkteamTest{
    @isTest public static void runTest(){
    
       Schedules__c sc = new Schedules__c();
        sc.Name = 'Test SC';
        insert sc;
        
        List<Schedule_Line_Item__c> slilist = new list<Schedule_Line_Item__c>();
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c();
        sli.Schedules__c = sc.Id;
        sli.Schedule_Week__c='WEEK A';
        sli.WeekDay__c = 'Sunday';
        sli.Start_Time__c='08:00 AM';
        sli.End_time__c='06:15 PM';
        slilist.add(sli);
        
        Schedule_Line_Item__c sli1 = sli.clone();
        sli1.WeekDay__c = 'Monday';
        slilist.add(sli1);
        
        Schedule_Line_Item__c sli2 = sli.clone();
        sli2.WeekDay__c = 'Tuesday';
        slilist.add(sli2);
        
        Schedule_Line_Item__c sli3 = sli.clone();
        sli3.WeekDay__c = 'Wednesday';
        slilist.add(sli3);
        
        Schedule_Line_Item__c sli4 = sli.clone();
        sli4.WeekDay__c = 'Thursday';
        slilist.add(sli4);
        
        Schedule_Line_Item__c sli5 = sli.clone();
        sli5.WeekDay__c = 'Friday';
        slilist.add(sli5);
        
        Schedule_Line_Item__c sli6 = sli.clone();
        sli6.WeekDay__c = 'Saturday';
        slilist.add(sli6);
        
        insert slilist;
        
        List<Schedule_Line_Item__c> slilist1 = new list<Schedule_Line_Item__c>();
        
        Schedule_Line_Item__c sli7 = new Schedule_Line_Item__c();
        sli7.Schedules__c = sc.Id;
        sli7.Schedule_Week__c='WEEK B';
        sli7.WeekDay__c = 'Sunday';
        sli7.Start_Time__c='08:00 AM';
        sli7.End_time__c='06:15 PM';
        slilist1.add(sli7);
        
        Schedule_Line_Item__c sli8 = sli7.clone();
        sli8.WeekDay__c = 'Monday';
        slilist1.add(sli8);
        
        Schedule_Line_Item__c sli9 = sli7.clone();
        sli9.WeekDay__c = 'Tuesday';
        slilist1.add(sli9);
        
        Schedule_Line_Item__c sli10 = sli7.clone();
        sli10.WeekDay__c = 'Wednesday';
        slilist1.add(sli10);
        
        Schedule_Line_Item__c sli11 = sli7.clone();
        sli11.WeekDay__c = 'Thursday';
        slilist1.add(sli11);
        
        Schedule_Line_Item__c sli12 = sli7.clone();
        sli12.WeekDay__c = 'Friday';
        slilist1.add(sli12);
        
        Schedule_Line_Item__c sli13 = sli7.clone();
        sli13.WeekDay__c = 'Saturday';
        slilist1.add(sli13);
        
        insert slilist1;
        
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        insert loc;
        
        Employee__c emp = new Employee__c ();
        emp.Name = 'emp name';
        emp.Location__c = Loc.Id;
        emp.Employee_Id__c = '123546';
        emp.Status__c = 'Active';
        emp.Schedule__c = sc.id;
        insert emp;
        
        List<Work_Team__c> wtList = [select id from work_team__C where Captain__c =: emp.ID];
        
        if(wtList != null && wtList.size() > 0){
            delete wtList;
        }
        
        dsmtBatchCreateWorkteam cntrl = new dsmtBatchCreateWorkteam ();
        cntrl.execute(null);
    }


    @isTest static void itShouldPickCorrectScheduleLineItem(){
        dsmtBatchCreateWorkTeam script = new dsmtBatchCreateWorkTeam();
        Location__c location = new Location__c();
        insert location;
        Schedules__c schedule = new Schedules__c();
        insert schedule;
        List<Schedule_Line_Item__c> scheduleLineItems = new List<Schedule_Line_Item__c>{
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule.Id, WeekDay__c = 'Monday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule.Id, WeekDay__c = 'Tuesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule.Id, WeekDay__c = 'Wednesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule.Id, WeekDay__c = 'Monday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule.Id, WeekDay__c = 'Tuesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule.Id, WeekDay__c = 'Wednesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM')
        };
        insert scheduleLineItems;
        dsmtBatchCreateWorkteam.startDate = Date.newInstance(2018, 3, 13);
        dsmtBatchCreateWorkteam.daysOfCoverageNeeded = 0;
        Employee__c employee = new Employee__c(
                Schedule__c = schedule.Id,
                Name = 'Test Employee',
                Location__c = location.Id,
                Employee_Id__c = '09875',
                Status__c = 'Active');
        insert employee;
        delete [SELECT Id FROM Work_Team__c];
        Test.startTest();
        script.execute(null, new List<Employee__c>{employee});
        Test.stopTest();
        List<Schedule_Line_Item__c> scheduleLineItemsUsedByWorkTeams = [SELECT Schedule_Week__c, WeekDay__c
                FROM Schedule_Line_Item__c 
                WHERE Id IN (SELECT Schedule_Line_Item__c FROM Work_Team__c)];
        System.assertEquals(1, scheduleLineItemsUsedByWorkTeams.size(), 'Expected only one Schedule Line Item to be referenced by a Workteam');
        Schedule_Line_Item__c selectedScheduleLineItem = scheduleLineItemsUsedByWorkTeams[0];
        System.assertEquals('WEEK A', selectedScheduleLineItem.Schedule_Week__c, '');
        System.assertEquals('Tuesday', selectedScheduleLineItem.WeekDay__c, '');
    }
}