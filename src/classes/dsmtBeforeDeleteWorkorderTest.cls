@isTest
public class dsmtBeforeDeleteWorkorderTest {

    static testmethod void test(){
        
        Workorder__c w = new Workorder__c();
        insert w;
        
        w.Early_Arrival_Time__c = DateTime.now();
        update w;
        
       
        delete w;
    }
    
    static testmethod void test1(){
         Workorder__c w = new Workorder__c();
         w.Early_Arrival_Time__c = DateTime.now();
         w.Scheduled_Start_Date__c = DateTime.now();
         w.Scheduled_End_Date__c = DateTime.now().addDays(5);
         insert w;
        
         Set<Id> idset = new Set<Id>();
         idset.add(w.Id);
        
         dsmtworkorderHelper.updateWorkOrder(idset);
        
    }
}