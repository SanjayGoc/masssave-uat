public class InspectionRequestExceptions
{
   public static boolean isStopValidateException = false;
   public static void InsertExceptionsOnir(List<Inspection_Request__c> IRlist){
       
        Map<String,Exception_Template__c> mapExt = new Map<String,Exception_Template__c>();     
        //Gets all expection with that check box checked.
        for(Exception_Template__c ext : [select id,name,Reference_ID__c,Type__c,Online_Portal_Title__c,Online_Portal_Text__c,Online_Application_Section__c,Required_Attachment_Type__c,Internal__c,Exception_Message__c,Outbound_Message__c,Short_Rejection_Reason__c,Attachment_Needed__c from Exception_template__c where Automatic__c = true and Active__c = true and Inspection_Request_Level_Message__c = true ]) {
          mapExt.put(ext.Reference_Id__c,ext);
        } 
        
        Set<Id> irids = new Set<id>();
        for(Inspection_Request__c ir :IRlist){
             irids.add(ir.id);   
        }
        
        Map<String,Exception__c> mapExceptions = new Map<String,Exception__c>();
        for(Exception__c Ex :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Inspection_Request__c, Inspection_Request__r.id FROM Exception__c 
                                WHERE Inspection_Request__r.id IN:irids AND Disposition__c  != 'Resolved']){    
            mapExceptions.put(Ex.Inspection_Request__c+'-'+Ex.Exception_Template__r.Reference_ID__c,Ex);     
        }      
        
        List<Exception__c> ExceptionsCreated = new List<Exception__c>();
          
        for(Inspection_Request__c irNew : IRlist) {
            
            List<string> AutoExceptionsList = new List<string>();
            
            for(String refId : mapExt.keyset()){
                Exception_Template__c temp = mapExt.get(refId);
               
                if(temp != null){
                      
                     if(refId == 'IR-00001' && !irNew.COI_signed__c){
                        If(mapExceptions.get(irNew.id+'-IR-00001')== null) 
                            AutoExceptionsList.add('IR-00001'); 
                     }
                     if(refId == 'IR-00002' && !irNew.COI_is_attached__c ){
                        If(mapExceptions.get(irNew.id+'-IR-00002')== null) 
                            AutoExceptionsList.add('IR-00002'); 
                     }
                     
               }
                
           }
           
            system.debug('--AutoExceptionsList--'+AutoExceptionsList);
            for(string autolist :AutoExceptionsList){
                if(mapExt.get(autolist) !=null) {
                    Exception_Template__c Exceptiontemp = mapExt.get(autolist);
                    Exception__c ExceptionstoAdd = new Exception__c(); 
                    ExceptionstoAdd.Internal__c = Exceptiontemp.Internal__c; 
                    ExceptionstoAdd.Exception_Message__c= Exceptiontemp.Exception_Message__c; 
                    ExceptionstoAdd.Outbound_Message__c=Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Inspection_Request__c=  irNew.Id;
                    ExceptionstoAdd.Exception_Template__c = Exceptiontemp.id;
                    ExceptionstoAdd.FInal_Outbound_Message__c = Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Short_Rejection_Reason__c = Exceptiontemp.Short_Rejection_Reason__c ;
                    ExceptionstoAdd.Automated__c = true;
                    ExceptionstoAdd.OwnerId =irNew.OwnerId;
                    ExceptionstoAdd.Attachment_Needed__c = Exceptiontemp.Attachment_Needed__c;
                    ExceptionstoAdd.Required_Attachment_Type__c = Exceptiontemp.Required_Attachment_Type__c;
                    ExceptionstoAdd.Online_Application_Section__c = Exceptiontemp.Online_Application_Section__c;
                    ExceptionstoAdd.Type__c = Exceptiontemp.Type__c;
                    //ExceptionstoAdd.Automatic_Communication__c = Exceptiontemp.Automatic_Communication__c;
                    //ExceptionstoAdd.Automatic_Rejection__c = Exceptiontemp.Automatic_Rejection__c;
                    //ExceptionstoAdd.Move_to_Incomplete_Queue__c = Exceptiontemp.Move_to_Incomplete_Queue__c;
                    ExceptionstoAdd.Online_Portal_Title__c  = Exceptiontemp.Online_Portal_Title__c;
                    ExceptionstoAdd.Online_Portal_Text__c = Exceptiontemp.Online_Portal_Text__c;
                    ExceptionsCreated.add(ExceptionstoAdd);    
                }
            }     
       }
        
        system.debug('--ExceptionsCreated--'+ExceptionsCreated);
        InspectionRequestExceptions.isStopValidateException = true;
        if(ExceptionsCreated.size()>0){
           insert ExceptionsCreated;  
        }   
   }
   
   public static void CheckUpdateExceptions(List<Inspection_Request__c> IRlist , Map<Id,Inspection_Request__c> oldmap){
        Map<string,List<Exception__c>> ExceptionsMap = new Map<string,List<Exception__c>>();
        for(Exception__c Exceptions :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Inspection_Request__c, Inspection_Request__r.id FROM Exception__c 
                                WHERE Inspection_Request__r.id=:oldmap.keyset() AND Disposition__c  != 'Resolved']){
                List<Exception__c> lsExceptions = ExceptionsMap.get(Exceptions.Inspection_Request__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c);
                if(lsExceptions  == NULL)
                    lsExceptions  = new List<Exception__c>();
                lsExceptions.add(Exceptions );
                ExceptionsMap.put(Exceptions.Inspection_Request__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c,lsExceptions);        
        }
        
         List<Exception__c> ExceptionstoUpdate = new List<Exception__c>();
         Set<Id> irId = new Set<Id>();
         for(Inspection_Request__c ir : IRlist){
              irId.add(ir.Id);        
         }
         
         for(Inspection_Request__c ir : IRlist){
             
                //String keyValue = ir.id + '-Project-001';
                 if(ExceptionsMap.containskey(ir.id + '-IR-00001')){  
                     if(ir.COI_signed__c){
                        for(Exception__c exp : ExceptionsMap.get(ir.id + '-IR-00001')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(ir.id + '-IR-00002')){  
                     if(ir.COI_is_attached__c){
                        for(Exception__c exp : ExceptionsMap.get(ir.id + '-IR-00002')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
            }
         
          InspectionRequestExceptions.isStopValidateException = true;
            system.debug('--ExceptionstoUpdate--'+ExceptionstoUpdate);
            if(ExceptionstoUpdate.size()>0){
                update ExceptionstoUpdate;
            }
   }
}