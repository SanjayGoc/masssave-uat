@istest
public class dsmtBuildingModelHelperTest 
{
    @istest
    public static void dsmtBuildingModelHelpertest1()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        ea.Floor_Area__c=20;
        ea.RoofAreaGross__c=1.00;
        ea.FrontLength__c=2.00;
        ea.Total_Volume__c = 100;
        ea.FrontLength__c = 50;
        update ea;
        
        Test.startTest();
        
        dsmtEAModel.BuildingModel moObj = new dsmtEAModel.BuildingModel();  
        Set<Id> eaId = new Set<Id>();
        eaId.add(ea.Id);
        dsmtEAModel.Surface surface = dsmtEAModel.InitializeBuildingProfile(eaId);
        
        integer operationType=1;
        
        surface.temp = new dsmteamodel.Temprature();
        surface.temp.FractionDaysC = 1;
        surface.temp.FractionDaysH = 1;
        surface.temp.FractionDaysSh = 1;
        surface.temp.InfiltNatCFMH=1;
        
        dsmtBuildingModelHelper cntrl =new dsmtBuildingModelHelper();
        dsmtBuildingModelHelper.ComputeWallUAEff(surface, 'C');
        surface.months='January;February;March;April;May;June;July;August;September;October;November;December';
        dsmtBuildingModelHelper.ComputeMonthsSeasonFractionCommon(surface);
        dsmtBuildingModelHelper.IsLocationInThermalSpace(surface,'AtticSpace');
        dsmtBuildingModelHelper.ComputeTotalLoadCommon(surface,'H');
        dsmtBuildingModelHelper.UpdateBuildingModelForLighting(surface);
        //dsmtBuildingModelHelper.UpdateAppliance(ap,surface);
        dsmtBuildingModelHelper.ComputeDuctSystemInfiltAdjCommon(surface,'H');

        //dsmtBuildingModelHelper.UpdateBaseBuildingThermal(surface);
        
        //dsmtBuildingModelHelper.UpdateAppliance(ap1,surface);
        //dsmtBuildingModelHelper.UpdateFloorCeilingModel(surface);
        //dsmtBuildingModelHelper.UpdateAtticVentingModel(avc,surface);
        //dsmtBuildingModelHelper.UpdateCeilingModel(ce,surface,true);
        //dsmtBuildingModelHelper.UpdateLayerModel(llist, surface);
        
        for(Integer operation=0;operation<22;operation++)
        { 
            dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(operation);
            dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(operation);
            dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(operation);
            dsmtBuildingModelHelper.GetWaterFixtureRecommendationOperation(operation);
        }
        for(Integer numHeatingSystem=1;numHeatingSystem<22;numHeatingSystem++)
        {
            dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(numHeatingSystem);
            dsmtBuildingModelHelper.GetHeatingSystemTypeFromNum(numHeatingSystem);
            
        }
        for(Integer ventType=1;ventType<100;ventType++)
        {
            dsmtBuildingModelHelper.GetAtticVentType(ventType);
        }
        Test.stopTest();
    }
    
    @istest
    public static void dsmtBuildingModelHelpertest2()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        ea.Floor_Area__c=20;
        ea.RoofAreaGross__c=1.00;
        ea.Total_Volume__c = 100;
        ea.FrontLength__c = 50;
        update ea;
        
        /*WeatherStation__c wst=[select id from WeatherStation__c where id =: ea.WeatherStation__c];
        wst.HSolarH__c=10;
        wst.NormalNFactor__c=10;
        update wst;*/
        
        List<Appliance__c> aplist  = new List<Appliance__c> ();
        Appliance__c ap = new Appliance__c();
        ap.Energy_Assessment__c = ea.Id;
        ap.Category__c = 'Kitchen';
        ap.Type__c='Dishwasher';
        ap.Fuel2Type__c='test';  
        insert ap;
        
        aplist.add(ap);
        
        test.startTest();
        Building_Specification__c bsc=[select id from Building_Specification__c where id =: ea.Building_Specification__c];
        bsc.Floors__c=10;
        bsc.Bedromm__c=1;
        bsc.Ceiling_Heights__c=15;
        bsc.Occupants__c=3;
        bsc.Floor_Area__c = 100;
        update bsc;  

        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = True;
        List<Thermal_Envelope_Type__c> tetlist =new List<Thermal_Envelope_Type__c>();
        Thermal_Envelope_Type__c tetype1 = new Thermal_Envelope_Type__c();
        tetype1.Energy_Assessment__c=ea.id;
        tetype1.SpaceConditioning__c='Vented';
        tetype1.recordtypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Basement').getRecordTypeId();        
        tetype1.ActualPerimeter__c = 5;
        
        tetype1.LoadH__c = 10;
        tetype1.RValueConst__c = 5;
       
        insert tetype1;
        
        tetlist.add(tetype1);
        
        List<Ceiling__c> cllist =new  List<Ceiling__c>();
        List<Wall__c> wllist =new  List<Wall__c>();
        List<Window__c> windlist =new List<Window__c>();
        List<SKylight__c> skylList =new List<SKylight__c>();
        List<Floor__c>flist=new List<Floor__c>();
        List<Mechanical_Sub_Type__c> mstlist =new List<Mechanical_Sub_Type__c>();
        
        Id wallids = Schema.SObjectType.Wall__c.getRecordTypeInfosByName().get('Attic Wall').getRecordTypeId(); 
        
        LightingAndApplianceConstant__c lightAndAppsetting = new LightingAndApplianceConstant__c();
        lightAndAppsetting.DefaultIndoorLightingPerSqFt__c = 10;
        lightAndAppsetting.DefaultOutdoorLightingPerSqFt__c = 10;
        insert lightAndAppsetting;
        /*Wall__c wal =new Wall__c();
        wal.Type__c='Basement Rim Joist';
        wal.Thermal_Envelope_Type__c=tetype1.id;
        wal.RecordTypeId=wallids;
        insert wal;
        wal.Type__c='Knee Wall';
        update wal;
        wllist.add(wal);
        
        Window__c wind= new Window__c(); 
        wind.Thermal_Envelope_Type__c=tetype1.id;
        insert wind;
        windlist.add(wind);
        
        Ceiling__c ce =new Ceiling__c();
        ce.Type__c='Attic Floor';
        ce.Area__c=1.00;
        //ce.Thermal_Envelope_Type__c=tetype1.id;
        insert ce;
        cllist.add(ce);
        
        SkyLight__c sky =new SkyLight__c();
        sky.Thermal_Envelope_Type__c=tetype1.id;
        sky.Skylight_Roof__c=ce.Id;
        insert sky;
        skylList.add(sky);*/
        

        Map<Id, List<Layer__c>> allLayerMap1 = new Map<Id, List<Layer__c>>();
        List<Layer__c> llist =new List<Layer__c>();       
        Layer__c ly =new Layer__c();
        ly.Thermal_Envelope_Type__c=tetype1.id;
        ly.RValuePerInch__c = 5;
        ly.Type__c = 'Wood Frame';
        ly.R_Value__c = 10;
        insert ly;
        llist.add(ly);
        allLayerMap1.get(tetype1.Id);
        
        List<Windows_Skylights_And_Set__c> wsslist = new List<Windows_Skylights_And_Set__c> ();
        Windows_Skylights_And_Set__c wss =new Windows_Skylights_And_Set__c();
        insert wss;
        wsslist.add(wss);
        
        List<Mechanical_Type__c> mtlist = new List<Mechanical_Type__c>();
        Mechanical_Type__c mt =new Mechanical_Type__c();
        mt.Name='test';
        mt.Fuel_Type__c='Oil';
        mt.Capacity_kBtu_h__c=10;
        mt.Combustion_Type__c='Atmospheric';
        insert mt;
        mtlist.add(mt);
        
        Window__c win =new Window__c();
        insert win;
        
        Id RecordTypeIdMechanical1 = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();     
        
        Mechanical__c m1=new Mechanical__c();
        m1.RecordTypeId=RecordTypeIdMechanical1;
        Insert m1;
        
        Id RecordTypeIdMechanicalSub_Type1 = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();
        
        Mechanical_Sub_Type__c mst1=new Mechanical_Sub_Type__c();
        mst1.RecordTypeId=RecordTypeIdMechanicalSub_Type1;
        mst1.Tank_Gallons__c=24;
        mst1.Mechanical__c=m1.Id;
        mst1.DHWType__c='Combo';
        mst1.Fuel_Type__c='Electricity';
        mstlist.add(mst1);

        Lighting__c ligh =new Lighting__c();
        ligh.Energy_Assessment__c=ea.id;
        ligh.DefaultIndoorLighting__c=10;
        ligh.Name = 'General Lighting';
        ligh.General_Indoor_Lighting__c = 'Medium';
        ligh.Efficient_Lights__c = 'Some';
        ligh.General_Outdoor_Lighting__c = 'Medium';
        ligh.Has_Outdoor_Fixtures__c = true;
        insert ligh;
        
        List<Lighting_Location__c> llclist =new List<Lighting_Location__c>();
        Lighting_Location__c llc = new Lighting_Location__c();
        
        insert llc;
        llclist.add(llc);
        Recommendation__c rec =new Recommendation__c();
        rec.Lighting_Location__c=llc.id;
        insert rec;
        
        dsmtEAModel.BuildingModel moObj = new dsmtEAModel.BuildingModel();  
        dsmtEAModel.Surface surface = new dsmtEAModel.Surface();
        
        Map<Id, string> recordTypeMap1 =new Map<Id, string>();
        recordTypeMap1.get(tetype1.Id);
        surface.recordTypeMap =dsmtHelperClass.getRecordType();
        
        surface.EAObj =ea;
        surface.mstObj=mst1;
        surface.teType=tetype1;
        surface.mo=moObj;
        surface.layer=ly;        
        surface.winSet=wss;
        surface.window=win;
        surface.applObj=ap;
        surface.lightLocObj=llc;
        surface.genLigtObj=ligh;
        //surface.ws=wst;
        surface.bs=bsc;
        //surface.wall=wal;
        //surface.ceiling=ce;
        integer operationType=1;
        test.stopTest();
        
        surface.wallList =wllist;
        surface.winList =windlist;
        surface.skyList=skylList;
        surface.floorList =flist;
        surface.ceilList=cllist;
        surface.lighlocList=llclist;
        surface.allLayerMap=allLayerMap1;
        surface.mst=mstlist;
        surface.teList=tetlist;
        surface.layers=llist;
        surface.winSetList=wsslist;
        surface.mechList=mtlist;
        surface.aplList=aplist;
        
        surface.temp = new dsmteamodel.Temprature();
        surface.temp.FractionDaysC = 1;
        surface.temp.FractionDaysH = 1;
        surface.temp.FractionDaysSh = 1;
        surface.temp.InfiltNatCFMH=1;
        MaterialRValuePerInch__c matSetting = new  MaterialRValuePerInch__c();
        matSetting.Value__c = 5;
        matSetting.name = 'test1';
        insert matSetting;

        dsmtBuildingModelHelper cntrl =new dsmtBuildingModelHelper();
        //dsmtBuildingModelHelper.UpdateBaseBuildingThermal(surface);
        dsmtBuildingModelHelper.UpdateThermalTypeModel(tetype1,surface,false);
        dsmtBuildingModelHelper.UpdateBuildingThermalLoadModel(surface);
        dsmtBuildingModelHelper.UpdateBuildingProfileForDHWInsert(surface);
        dsmtBuildingModelHelper.AssignLightingPartToSurfaceModel(rec,surface);
        dsmtBuildingModelHelper.AssignThermalPartToSurfaceModel(rec,surface);
        dsmtBuildingModelHelper.UpdateDefaultLight(ligh,surface);
        dsmtBuildingModelHelper.UpdateLightLocations(llc,surface);
        dsmtBuildingModelHelper.ComputeDuctSystemInfiltAdjCommon(surface,'H');
        dsmtBuildingModelHelper.UpdateLayerModel(llist, surface);
        
    }
}