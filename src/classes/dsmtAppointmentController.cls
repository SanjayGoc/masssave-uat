public class dsmtAppointmentController
{
    
    public List<SelectOption> AppointmentType{get;set;}
    public List<SelectOption> AppointmentStatus{get;set;}
    public List<SelectOption> dsmtContactOption{get;set;}
    public List<SelectOption> dsmtstatusoption{get;set;}
    
    public String SelectedAppointmentType{get;set;}
    public String SelectedAppointmentStatus{get;set;}
    public String Selectedcontacts{get;set;}
    public string Selectedstatus{get;set;}

    public Workorder__c worder {get;set;}
    public list<Work_Team__c> workteams{get;set;}
    public string wtId {get;set;}
    public string woId {get;set;}
    public string startTme {get;set;}
    public string endTme {get;set;}
    
    public string custName {get;set;}
    public string custAccNo {get;set;}
    public boolean overlap {get;set;}
    public Integer minutesToAdd{get;set;}
    
    public String defaultworkorderType {
        get;
        set;
    }
    
    private static datetime checkDatetime(string dt) {
        if (dt != null && dt.length() > 0) {
            list<string> dateTimeList = dt.split(' ');
            list<string> dayMonList = dateTimeList[0].split('/');
            list<string> hourMinList = dateTimeList[1].split(':');
            
            return Datetime.newInstance(Integer.valueOf(dayMonList[2]), Integer.valueOf(dayMonList[0]), Integer.valueOf(dayMonList[1]), Integer.valueOf(hourMinList[0]), Integer.valueOf(hourMinList[1]), 00);
        }

        return null;
    }
    
    private Date checkDatee(string dt) {
        if (dt != null && dt.length() > 0) {
            list<string> dateTimeList = dt.split(' ');
            list<string> dayMonList = dateTimeList[0].split('/');
            list<string> hourMinList = dateTimeList[1].split(':');
            
            return Date.newInstance(Integer.valueOf(dayMonList[2]), Integer.valueOf(dayMonList[0]), Integer.valueOf(dayMonList[1]));
        }

        return null;
    }
    
    public void fetchworkorder(){
        worder = new Workorder__c();
        list<Appointment__c> aList = [select Workorder__c from Appointment__c where id =: woId];
        
        if(aList.size() > 0)
        {
            list<Workorder__c> wrkOdrs = [select id, Name,Requested_End_Date__c,Requested_Start_Date__c,
                                            Scheduled_Start_Date__c,Scheduled_End_Date__c,Work_Team__r.Id,
                                            Time_Slot__r.Time_Slot__c, Work_Team__c,Workorder_Type__c,
                                            Work_Team__r.Name,Duration__c,Status__c,Priority__c,
                                            Preferred_Contact_Method__c,Preferred_Contact_Number_Email__c,Notes__c,
                                            Verified_Address__c,Date_Of_Verification__c, Time_Slot__c,
                                            Customer__r.NAme, Customer__r.Address__c, 
                                            Customer__r.Customer_Account_Number__c,
                                            Customer__r.City__c, Customer__r.Email__c,
                                            Customer__r.State__c, Customer__r.Phone__c, 
                                            Customer__r.Zip_Code__c, Customer__r.Mobile__c,
                                            Address__c, Scheduled_Date__c, Requested_Date__c,
                                            City__c, Email__c, Alert__c,
                                            State__c, Phone__c, 
                                            ZipCode__c, Mobile__c,
                                            Work_Performed_Start_Date__c, Work_Performed_End_Date__c,
                                            Work_Performed_Date__c,Early_Arrival_Time__c,Late_Arrival_Time__c
                                            from Workorder__c 
                                            where id =: aList[0].Workorder__c];        
            if(wrkOdrs.size() > 0){
                defaultworkorderType = wrkOdrs[0].Workorder_Type__c;
                
                worder = wrkOdrs[0];
                custAccNo = worder.Customer__r.Customer_Account_Number__c;
                custName = worder.Customer__r.Name;
                
                list<Work_Team__c> wteams = [select id, Name from Work_Team__c where id =: wtId];
                if(wteams.size() > 0){
                    wtName = wteams[0].Name;
                    worder.Work_Team__c = wteams[0].Id;
                    worder.Scheduled_Start_Date__c = checkDateTime(startTme);
                    worder.Scheduled_End_Date__c = checkDateTime(endTme);
                }
                
                //timeSlotName = worder.Scheduled_Start_Date__c.format('hh:mm a') + ' - ' + worder.Scheduled_End_Date__c.format('hh:mm a');
                timeSlotName = worder.Early_Arrival_Time__c.format('hh:mm a') + ' - ' + worder.Late_Arrival_Time__c.format('hh:mm a');
                calculateEstimateDuration();
            }else
            {
                list<Work_Team__c> wtList = [select id, Name from Work_Team__c where id =: wtId];
                if(wtList.size() > 0)
                    wtName = wtList[0].Name;
                
                worder.Work_Team__c = wtId;
                worder.Scheduled_Date__c = checkDatee(startTme);
                worder.Scheduled_Start_Date__c = checkDateTime(startTme);
                worder.Scheduled_End_Date__c = checkDateTime(endTme);
            } 
        }
    }
    
    public Integer EstdurHr{get;set;}
    public Integer EstdurMn{get;set;}
    public string wtName {get;set;}
    public string timeSlotName {get;set;}
    
    public void calculateEstimateDuration() {
        EstdurHr = 0;
        EstdurMn = 0;
        list<Workorder_Type__c > wotypelist = [select id, Est_Work_Time__c, Est_Total_Time__c from Workorder_Type__c where id = : defaultworkorderType];
        if (wotypelist.size() > 0) {
            Decimal worktime = wotypelist.get(0).Est_Total_Time__c;
            if (worktime != 0) {
                Integer wt = worktime.intValue();
                EstdurHr = wt / 60;
                EstdurMn = math.mod(wt, 60);
            }
        }
    }
    
    public list<SelectOption > getWorkTypeslist() {
        list<WorkOrder_Type__c > wolist = [select id, name from workOrder_Type__c ];
        list<SelectOption > workoTypes = new list<SelectOption > ();
        for (WorkOrder_Type__c wo: wolist) {
            workoTypes.add(new SelectOption(wo.id, wo.name));
        }
        return workoTypes;
    }    
    
    Set<Id> dsmtcId = new Set<Id>();
    Set<Id> empId = new Set<Id>();
    
    public transient string eventsJsonStr{get;set;}
    
    public Appointment__c newApp{get;set;}
    public Appointment__c editApp{get;set;}
    public String AppointmentId{get;set;}
    
    public List<Checklist_Items__c> ciList{get;set;}
    public Dsmtracker_Contact__c dsmt{get;set;}
    public Trade_Ally_Account__c ta{get;set;}
    public string portalRole {get;set;}
    
    public String CancelNotes{get;set;}
    
    String userType = '';
    
    public dsmtAppointmentController(){
        newApp = new Appointment__c();
        editApp = new Appointment__c();
        GetContactInfo();
        Selectedcontacts = '';
        SelectedAppointmentType = '';
        SelectedAppointmentStatus = '';
        GetAppointments();
        GetCheckListInfo();
        FillSelectOption();
    }
    
    public void GetContactInfo(){
        dsmtContactOption = new List<SelectOption>(); 
        dsmtContactOption.add(new SelectOption('', '--Select Team Member--'));      
        
        List<User> userList = [select id,contactId from user where id =: userinfo.getUserId()];
        
        if(userList != null && userList.size() > 0 && userList.get(0).ContactId != null){
            
            List<DSMTracker_Contact__c> dsmtconList = [select id, Name, Portal_Role__c, Trade_Ally_Account__c,(select id from employees__r) 
                                                       from DSMTracker_Contact__c 
                                                       where contact__c =: userList.get(0).ContactId];
            
           
            if(dsmtConList != null){
                dsmt = dsmtconList.get(0);
                userType = dsmt.Portal_Role__c;
                dsmtcId.add(dsmt.Id);
                if(dsmt.employees__r.size() > 0){
                    empid.add(dsmt.employees__r.get(0).Id);
                }
                portalRole = dsmt.Portal_Role__c; 
                List<Trade_Ally_Account__c> talist = [select id,name from Trade_Ally_Account__c where id =: dsmt.Trade_Ally_Account__c];
                if(talist.size() > 0){
                   ta = talist.get(0);
                   //if(dsmt.Portal_Role__c == 'Manager'){
                   if(dsmt.Portal_Role__c != 'Energy Specialist'){
                       List<DSMTracker_Contact__c> dsmtconList1 = [select id,Super_User__c,Name from DSMTracker_Contact__c where Trade_Ally_Account__c  =: dsmtConList.get(0).Trade_Ally_Account__c];
                       for(DSMTracker_Contact__c dsmtc : dsmtConList1){
                           dsmtcId.add(dsmtc.Id);
                           dsmtContactOption.add(new SelectOption(dsmtc.Id, dsmtc.Name));
                       }   
                   }
                }
            }
            
        }
    }
    public void FillSelectOption(){
        
        AppointmentType = new List<SelectOption>();
        AppointmentStatus = new List<SelectOption>();
        dsmtstatusoption = new List<SelectOption>();

        AppointmentType.add(new SelectOption('', '--Select Type--'));  
        
       /* Schema.DescribeFieldResult fieldResult = Appointment__c.Appointment_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : ple)
        {
          if(f.getvalue() != 'Lunch' && f.getvalue() != 'PTO' && f.getvalue() != 'Sick')
          AppointmentType.add(new SelectOption(f.getLabel(), f.getValue()));
        }*/
        
        List<workorder_Type__c> wtype = [select id,name from workorder_Type__c where Show_As_Filter_In_Trade_Ally__c = true limit 999];
        
        Set<String> newwtype = new Set<String>();
        
        for(workorder_Type__c wt : wtype){
            
            if(newwtype.add(wt.Name)){
                AppointmentType.add(new SelectOption(wt.Name,wt.Name));  
            }
        }
        
        AppointmentStatus.add(new SelectOption('', '--Select Status--'));  
        
        Schema.DescribeFieldResult fieldResult = Appointment__c.Appointment_Status__c.getDescribe();
        List<Schema.PicklistEntry> ple  = fieldResult.getPicklistValues();
            
        if(userType == 'Energy Specialist'){
            AppointmentStatus.add(new SelectOption('Scheduled', 'Scheduled'));
            AppointmentStatus.add(new SelectOption('Rescheduled', 'Rescheduled'));
            AppointmentStatus.add(new SelectOption('Unavailable', 'Unavailable'));
            AppointmentStatus.add(new SelectOption('Cancelled', 'Cancelled'));
        }else
        {
            for( Schema.PicklistEntry f : ple)
            {
                AppointmentStatus.add(new SelectOption(f.getLabel(), f.getValue()));
            }
            
            dsmtstatusoption.add(new SelectOption('', '--Select Type--'));
            dsmtstatusoption.add(new SelectOption('All', 'All'));
            dsmtstatusoption.add(new SelectOption('Assessments', 'Assessments'));
            dsmtstatusoption.add(new SelectOption('Installations', 'Installations'));
        }     
   
    }
    
    public void SaveUnAvailableAppointment(){
        newApp.Appointment_Status__c = 'Unavailable';
        insert newApp;
        GetAppointments();
        
        newApp = new Appointment__c();
    }
    
    public void updateAppoinment(){
        Date dt = editApp.Appointment_Start_Time__c.Date();
        List<Appointment__c> appList = [select id,Appointment_Start_Time__c,Appointment_End_Time__c 
                                        from appointment__c 
                                        where Appointment_Start_Date__c =: dt 
                                        and dsmTracker_Contact__c =: editApp.DSMTracker_Contact__c and Id !=: editApp.id];
        
        overlap = false;
        
        // 10:00 - 11:00
        //  10:30 - 11:30 Handled
        //  9:30 - 10:30 handled
        // 9 - 12
        
        for(Appointment__c app : appList){
            
            if(editApp.Appointment_Start_Time__c >= app.Appointment_Start_Time__c &&
                editApp.Appointment_Start_Time__c <= app.Appointment_End_Time__c ){
                    overlap = true;
            }
            
            if(editApp.Appointment_End_Time__c >= app.Appointment_Start_Time__c &&
                editApp.Appointment_End_Time__c <= app.Appointment_End_Time__c ){
                    overlap = true;
            }
            if(editApp.Appointment_Start_Time__c <= app.Appointment_Start_Time__c &&
                editApp.Appointment_End_Time__c  >= app.Appointment_End_Time__c){
                
                overlap = true;    
            }
        }
        
        if(!overlap){
            editApp.Notes__c = editApp.Description__c;
            editApp.Appointment_Status__c = 'Rescheduled';
            update editApp;
             if(editApp.workorder__c  != null){
                List<workorder__c> woList = [select id,notes__c,Status__c from workorder__c where id =: editApp.workorder__c];
                
                if(woList != null && woList.size() > 0){
                    woList.get(0).Notes__c = editApp.notes__c;
                    woList.get(0).Status__c = 'Rescheduled';
                    update woList;
                }
            }
            editApp = new Appointment__c();
            GetAppointments();
        }else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.AppointmentOverlap));
        }
    }
    
    public void CancenAppointment(){
        //AppointmentId
        List<Appointment__c> appList  = [select Id,Cancel_Notes__c,Appointment_Status__c,Workorder__c from Appointment__c where id =: AppointmentId];
        
        if(appList != null && appList.size() > 0){
            if(appList.get(0).Cancel_Notes__c == null){
                appList.get(0).Cancel_Notes__c = '';
            }
            appList.get(0).Cancel_Notes__c = CancelNotes;
            appList.get(0).Appointment_Status__c = 'Cancelled';
        }
        
        update appList;
        
        Workorder__c wo = new Workorder__c ();
        wo.Id = appList.get(0).workorder__c;
        wo.Status__c = 'Cancelled';
        update wo;
        GetAppointments();
    }
    
    public void GetCheckListInfo(){
        ciList = [select Checklist__c,Checklist_Information__c from Checklist_Items__c where Parent_Checklist__r.Reference_ID__c = 'CKLST-000003' order by Sequence__c];
    }
    
    public void queryAppointment(){
        Integer duration = 0;
        editApp = new Appointment__c();
        list<Appointment__c> appList = [select 
                                   id, 
                                   Name,
                                   Appointment_Status__c ,
                                   Description__c,
                                   DSMTracker_Contact__c,
                                   Appointment_Start_Time__c,
                                   Appointment_End_Time__c,
                                   Workorder__r.Workorder_Type__r.Est_Total_Time__c,
                                   workorder__C
                               from 
                                   Appointment__c
                                   where id =: AppointmentId];
                                   
        if(appList.size() > 0){
            editApp = appList[0];
            if(editApp.Workorder__r.Workorder_Type__r.Est_Total_Time__c != null)
                minutesToAdd = integer.valueOf(editApp.Workorder__r.Workorder_Type__r.Est_Total_Time__c);
            else
                minutesToAdd = 120;
        }
    }
    
    public void GetAppointments(){
      try{
        eventsJsonStr = '';
        
        String filterquery = '';
        Set<Id> taId = new Set<Id>();
        taId.add(ta.Id);
        if(userType == 'Energy Specialist'){
            //filterquery =  ' Where Id != null And Workorder__c != null And Appointment_Start_Time__c != null and Appointment_End_Time__c != null And DSMTracker_Contact__c in :dsmtcId And';        
            filterquery =  ' Where Id != null And Workorder__c != null And Appointment_Start_Time__c != null and Appointment_End_Time__c != null And (Employee__c in :empid OR DSMTracker_Contact__c in :dsmtcId) And';        
        }else{
            filterquery =  ' Where Id != null  And Appointment_Start_Time__c != null and Appointment_End_Time__c != null And trade_Ally_Account__c in :taId And';
        }
        
        //dsmt.Portal_Role__c != 'Energy Specialist'
        
        if(SelectedAppointmentType != '' && SelectedAppointmentType != null){
            filterquery += ' Appointment_Type__c = '+ '\'' + SelectedAppointmentType +'\' And';
        }

        if(SelectedAppointmentStatus != '' && SelectedAppointmentStatus != null){
            filterquery += ' Appointment_Status__c = '+ '\'' + SelectedAppointmentStatus +'\' And';
        }
        
        if(Selectedcontacts != null && Selectedcontacts != ''){
            filterquery += ' DSMTracker_Contact__c = '+ '\'' + Selectedcontacts +'\' And';
        }
        
        if(filterquery.endswith('And')){
            filterquery = filterquery.substring(0,filterquery.length() - 3);
        }
        
        datetime sdt = system.now().addMonths(-1);
        datetime edt = system.now().addMonths(3);  
        
        filterquery += ' AND Appointment_Start_Time__c >=: sdt ';
        filterquery += ' AND Appointment_End_Time__c <=: edt ';
        
        if(Selectedstatus == 'Assessments'){
            filterquery += ' AND Workorder__c != null ';
        }
        
        if(Selectedstatus == 'Installations'){
            filterquery += ' AND Project__c != null ';
        }

        String query = ' select id, Name, Appointment_Status__c, Customer_Reference__c , Appointment_Start_Time__c, ' + 
                       ' Appointment_End_Time__c, Workorder__r.Customer__r.Name, Workorder__r.Customer__r.Service_Address__c, ' +
                       ' Workorder__r.Customer__r.Service_City__c, Workorder__r.Customer__r.Service_State__c, Workorder__r.Customer__r.Zip_Code__c, '+
                       ' Workorder__r.Customer__r.Phone__c, Appointment_Type__c '+
                       ' from Appointment__c';
        
        query += filterquery ;
        
        system.debug('--filterquery---'+taId);
        system.debug('--query---'+query);
        
        list<Appointment__c> appointmentList = database.query( query);                     
                                
        list<EventsWrapper> eventWrapperList = new list<EventsWrapper>();
        for(Appointment__c a : appointmentList)
        {
            string evnt_desc = '';
            
            string eve_color = '#3a6690';
            
            if(a.Appointment_Status__c == 'Completed'){
                eve_color = 'grey';
            }else if(a.Appointment_Status__c == 'Rescheduled'){
                eve_color = '#7a7b37';
            }else if(a.Appointment_Status__c == 'Cancelled'){
                eve_color = '#ec7676';
            }else if(a.Appointment_Status__c == 'Unavailable'){
                eve_color = '#ffce5a';
            }
            
            evnt_desc += a.Name.replace('A-00','')+', ';
            if (a.Customer_Reference__c != null) {
                evnt_desc += a.Workorder__r.Customer__r.Name + ', <br/>';
            }
        
            evnt_desc += a.Appointment_Start_Time__c.format('hh:mm a') + ' - ' + a.Appointment_End_Time__c.format('hh:mm a') + ', ';
            evnt_desc += a.Appointment_Type__c != null ? a.Appointment_Type__c + ', <br/>' : '';
            
            //system.debug('Workorder__r.Customer__r.Address__c :::::'+ a.Workorder__r.Customer__r.Address__c);
            if (a.Customer_Reference__c != null) {
                evnt_desc += a.Workorder__r.Customer__r.Service_Address__c != null ? a.Workorder__r.Customer__r.Service_Address__c + ', ' : '';
                evnt_desc += a.Workorder__r.Customer__r.Service_City__c != null ? a.Workorder__r.Customer__r.Service_City__c + ' ' : '';
                evnt_desc += a.Workorder__r.Customer__r.Service_State__c != null ? a.Workorder__r.Customer__r.Service_State__c + ' ' : '';
                evnt_desc += a.Workorder__r.Customer__r.Zip_Code__c != null ? a.Workorder__r.Customer__r.Zip_Code__c + ', ' : '';
                evnt_desc += a.Workorder__r.Customer__r.Phone__c != null ? a.Workorder__r.Customer__r.Phone__c + '' : '';
            }
            
            if(portalRole != 'Energy Specialist' && a.Appointment_Status__c!='Cancelled'){ 
               if(a.Appointment_Status__c=='Rescheduled'){
                   evnt_desc += '<a href="javascript:void(0)" class="eventa" id="'+a.Id+'" onclick=openCancel(this);>C</a><br/>';
               }
               else{                                    
                   evnt_desc += '<a href="javascript:void(0)" class="eventa" id="'+a.Id+'" onclick=openCancel(this);>C</a><br/>';
                   evnt_desc += '<a href="javascript:void(0)" class="eventa lft" id="'+a.Id+'" onclick=openReschedule(this);>R</a><br/>';
               }
            }
            
            eventWrapperList.add(new EventsWrapper(a.Id,
                                                   a.Appointment_Start_Time__c.format('yyyy-MM-dd hh:mm a'),
                                                   a.Appointment_End_Time__c.format('yyyy-MM-dd hh:mm a'),
                                                   evnt_desc,
                                                   1,
                                                   eve_color));                                  
        }
        /*JSONGenerator jgen = JSON.createGenerator(false);
        jgen.writeStartArray();
          for(EventsWrapper ew : eventWrapperList){
              jgen.writeStartObject();
              jgen.writeObject(ew);
              jgen.writeEndObject();
          }
        jgen.writeEndArray();
        eventsJsonStr = jgen.getAsString();*/
        eventsJsonStr = JSON.serialize(eventWrapperList); //.replace('\'','~~');
        system.debug('--eventsJsonStr --'+eventsJsonStr );
       }catch(Exception e){
          system.debug('--e--'+e.getStackTraceString());
       } 
    }
    
    public class EventsWrapper{
        public string id;
        public string start_date;
        public string end_date;
        public string text;
        public integer section_id;
        public string color;
        
        public EventsWrapper(string id,
                             string start_date,
                             string end_date,
                             string text,
                             integer section_id,
                             string color)
        {
            this.id = id;
            this.start_date = start_date;
            this.end_date = end_date;
            this.text = text;
            this.section_id = section_id;
            this.color = color;
        }
    }
}