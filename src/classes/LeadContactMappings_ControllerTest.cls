@isTest
Public class LeadContactMappings_ControllerTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        insert obj;
        
        Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today()-1;
        insert obj;
        
        Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        insert obj;
        
        Task tsk = new Task();    
        tsk.WhatId = obj.Id;
       // tsk.WhoId = l.Id;
        tsk.CallDisposition = 'Outbound';
        tsk.Status = 'Completed';
        tsk.Subject = ' Call To ';
        tsk.Description = 'test';
        tsk.ActivityDate = Date.Today();
        tsk.Type = 'Call';
        insert tsk;
        
        Note n = new Note();
        n.ParentId = obj.Id;
        n.Title = 'test';
        n.Body = 'test';
        insert n;
        Skill__c sk= Datagenerator.createSkill();
        insert sk;
        
        Skill__c sk2= Datagenerator.createSkill();
        sk2.Name= 'QA-002';
        insert sk2;
        
         
        Location__c loc= Datagenerator.createLocation();
        insert loc;
        Employee__c em = Datagenerator.createEmployee(loc.id);
        em.Employee_Id__c = 'test123';
        em.Status__c='Approved';
        insert em;
        Employee__c em2 = Datagenerator.createEmployee(loc.id);
        em2.Employee_Id__c = 'test1234';
        em2.Status__c='Approved';
        insert em2;
        
        Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
        insert wt;
        
        Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
        emskill.Survey_Score__c=89;
        insert emskill;
        
        Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                        Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
        insert woTypeList;
        
        Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
        insert rsk;
        
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        EL.Workorder_Type__c=woTypeList.Id;
        EL.Service_Address__c='test';
        EL.City__c='test';
        EL.Start_Date__c=date.today().addDays(-5);
        EL.End_Date__c=date.today().addDays(5);
        update EL;
        
        EL.How_many_units__c='Single family';
        update EL;
        
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        update app;
        
        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.HIC_Attached__c=true;
        update reg;
        
        //Apexpages.currentPage().getParameters().put('empId',em.id);
        
        LeadContactMappings_Controller controller = new LeadContactMappings_Controller();    
        LeadContactMappings_Controller.Model model = new LeadContactMappings_Controller.Model();
        model.saveMappings();
        List<Schema.FieldSetMember> a = model.callListLineItemFields;
        List<SelectOption> b = model.contactFields;
        List<SelectOption> c = model.leadFields;
        List<SelectOption> d = model.customerFields;
    }
}