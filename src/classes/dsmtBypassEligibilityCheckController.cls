public class dsmtBypassEligibilityCheckController
{
    public Eligibility_Check__c custInteraction {get;set;}
    public string customerId {get;set;}
    public List<Customer_Eligibility__c> ecaList{get;set;}
    public integer numberOfApartment{get;set;}
    
    public boolean ShowEligibleMessage{get;set;}
    
    public String callListLineItemId;
    public string timeFilter {get;set;}
    public string dayOfWeekFilter {get;set;}
    public string weekFilter {get;set;}
    public String notes{get;set;}
    public boolean isNotValidAddress{get;set;}
    public Appointment__c newApp{get;set;}
    public boolean overlap {get;set;}
    public string reviewId{get;set;}
    
    public list<SelectOption> getWeeks(){
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--None--'));
        soList.add(new SelectOption('1', '1'));
        soList.add(new SelectOption('2', '2'));
        soList.add(new SelectOption('3', '3'));
        soList.add(new SelectOption('4', '4'));
        soList.add(new SelectOption('5', '5'));
        soList.add(new SelectOption('6', '6'));
        soList.add(new SelectOption('7', '7'));
        soList.add(new SelectOption('8', '8'));
        soList.add(new SelectOption('9', '9'));
        soList.add(new SelectOption('10', '10'));
        soList.add(new SelectOption('11', '11'));
        soList.add(new SelectOption('12', '12'));
        
        return soList;
    }
    
    public list<SelectOption> getDays(){
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--None--'));
        soList.add(new SelectOption('Sunday', 'Sunday'));
        soList.add(new SelectOption('Monday', 'Monday'));
        soList.add(new SelectOption('Tuesday', 'Tuesday'));
        soList.add(new SelectOption('Wednesday', 'Wednesday'));
        soList.add(new SelectOption('Thursday', 'Thursday'));
        soList.add(new SelectOption('Friday', 'Friday'));
        soList.add(new SelectOption('Saturday', 'Saturday'));
        
        return soList;
    }
    
    public list<SelectOption> getTimes(){
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--None--'));
        soList.add(new SelectOption('Before Noon', 'Before Noon'));
        soList.add(new SelectOption('After Noon', 'After Noon'));
        
        return soList;
    }
    
    public dsmtBypassEligibilityCheckController()
    {
        isError = true;
        init();
        showbutton = false;
    }
    
    private void init()
    {
        
        newApp = new Appointment__c();
        custInteraction = new Eligibility_Check__c();
        callListLineItemId  = ApexPages.currentPage().getParameters().get('clId');
        reviewId = ApexPages.currentPage().getParameters().get('ReviewId');
        if(callListLineItemId!=null && callListLineItemId  !=''){
            List<Call_List_Line_Item__c> callItem = [select Id,Customer__c from Call_List_Line_Item__c where id =: callListLineItemId];
            if(callItem!=null && callItem.size()>0){
                customerId = callItem[0].Customer__c;
            }
        }
        
        GetWoType();
        apptMap = new List<SelectOption>();

    }
    
    
    
    @RemoteAction
    public static List<Trade_Ally_Account__c> getContractors(){
        List<Trade_Ally_Account__c> tacList = [select id, Name from Trade_Ally_Account__c where Status__c = 'Eligible' or Status__c = 'Provisional'];
            
        return tacList;
    }
    
    
    
    @RemoteAction
    public static Customer__c fetchCustomer(string clineitemId){
        Customer__c custObj = new Customer__c();
        List<Call_List_Line_Item__c> cliList = [select id,customer__c from Call_List_Line_Item__c where id =: clineitemId];
        
        ID recordId = null;
        
        if(cliList != null && cliList.size() > 0){
            recordId  = cliList.get(0).customer__c ;
        }else{
            recordId  = clineitemId ;
        }
             
        if(recordId  != null){
            DescribeSObjectResult describeResult = recordId.getSObjectType().getDescribe();
            
            List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
            
            String query =
              ' SELECT ' +
                  String.join( fieldNames, ',' ) +
              ' FROM ' +
                  describeResult.getName() +
              ' WHERE ' +
                  ' id = :recordId ' +
              ' LIMIT 1 '
            ;
            
            // return generic list of sobjects or typecast to expected type
            List<SObject> records = Database.query( query );
            custObj = (customer__c)records.get(0);
        }
        return custObj ;
    }
    
    @RemoteAction
    public static string LetsGo(boolean  therm, boolean hes){
        Eligibility_Check__c custInteraction = new Eligibility_Check__c();
        custInteraction.Purchase_a_discounted_smart_thermostat__c = therm;
        custInteraction.No_cost_home_energy_assessment__c = hes;
        custInteraction.Channel__c = 'Online';
        upsert custInteraction;
        return custInteraction.Id;
    }
    
    public void SaveRecord(){
        custInteraction.Customer__c = customerId;
        upsert custInteraction;
        
        if(ecaList != null && ecaList.size() > 0){
            for(Customer_Eligibility__c ec : ecaList){
                ec.Eligibility_Check__c = custInteraction.Id;
            }
            insert ecaList;
        }
        
    }
    
    
    
    Map<String,dsmtCallOut.AppointmentOption> apptMap1 = null;
    
    Map<Integer,Map<String,dsmtCallOut.AppointmentOption>> newapptmap = new map<Integer,Map<String,dsmtCallOut.AppointmentOption>>();
    
    public string WoTypeId{get;set;}
    public string SelectedEmployee{get;set;}
    public List<SelectOption> apptMap{get;set;}
    
    
    public List<Selectoption> employeeOption{get;set;}
   
  
    public void getEmployee(){
        //WoTypeId
       // woTypeId = 'a1F4D00000095qF';
       system.debug('--woTypeId --'+woTypeId );
       
        
        if(CustomerId == null || CustomerId == ''){
            List<Call_List_Line_Item__c> callItem = [select id, customer__c from Call_List_Line_Item__c  where id =: callListLineItemId];
            if(callItem  != null && callItem.size() > 0){
                customerId = callItem.get(0).customer__c ;
            }
        }
        
        if(woTypeId == null || woTypeId == ''){
            woTypeId = 'a1F4D00000095q2';
        }
        if(WoTypeId != null && WoTypeId != ''){
            list<Required_Skill__c> rsLst = [select id, Skill__c from Required_Skill__c where Workorder_Type__c =: WoTypeId];
            
            set<string> skillIds = new set<string>();
            
            for(Required_Skill__c r : rsLst){
                skillIds.add(r.Skill__c);
            }
            
            list<Employee_Skill__c> esLst = [select id, Skill__c,Employee__c,Employee__r.Name from Employee_Skill__c where skill__c in : skillIds];
    
            Set<String> empId = new Set<String>();
            employeeOption = new List<SelectOption>();
            employeeOption.add(new SelectOption('','--Select Employee--'));
            
            for(Employee_Skill__c  es : esLst){
                if(empId.add(es.Employee__r.Name)){
                    if(es.employee__c != null){
                        employeeOption.add(new SelectOption(es.employee__c,es.Employee__r.Name));
                    }
                }
            }
        } 
        custInteraction.Start_Date__c = Date.Today().Adddays(2);
        custInteraction.End_Date__c = Date.Today().Adddays(180);
        
          
    }
    
    
    public boolean isError{get;set;}
    public String AppterrorMsg{get;set;}
    public String TimeHours{get;set;}
    
    public string SessionId;
    
    public boolean showbutton{get;set;}
    
    public void getAppt(){  
        
        //woTypeId = 'a1F4D00000095qF';
        Set<Id> CustId = new Set<Id>();
        String Address1 = '';
        String City1 = '';
        
        Set<String> woTypeIdLst = new Set<String>();
        
        Integer NumberOFgas = 0;
        Integer NumberOFElec = 0;
        Set<Id> newGasCustId = new Set<Id>();
        Set<Id> newElecCustId = new Set<Id>();
           
        Integer cnt = 0;
        system.debug('--WoTypeId---'+WoTypeId);
         List<Workorder_Type__c> woTypeList = [select id,Est_Total_Time__c from Workorder_Type__c where id =: WoTypeId];
        woTypeIdLst.add(WoTypeId);
        Integer duration = 0;
        
        system.debug('--cnt --'+cnt );
        if(woTypeList != null && woTypeList.size() > 0){
            if(cnt != 0){
                duration = Integer.valueOf(woTypeList.get(0).Est_Total_Time__c)*cnt ;
            }else{
                duration = Integer.valueOf(woTypeList.get(0).Est_Total_Time__c);
            }
        }
        else{
            duration = 120;
        }
     //   Date myDate = Date.newInstance(custInteraction.Start_Date__c.year(), custInteraction.Start_Date__c.month(), custInteraction.Start_Date__c.day());

       // Date myDate1 = Date.newInstance(custInteraction.End_Date__c.year(), custInteraction.End_Date__c.month(), custInteraction.End_Date__c.day());
        
        system.debug('---duration---'+duration);
        //system.debug('--woTypeIdLst---'+woTypeIdLst);
        
        Decimal duration1 = duration/60;
        
        Integer i1 = Math.round(duration1);
        
        if( i1 == 0){
            TimeHours = ' up to one hour';  
        }else{
            TimeHours = ' up to '+i1 + ' hours';  
        }
        

        system.debug('---duration --'+duration1 );
       // system.debug('---'+duration/60);
        
      /*  if(duration1 == 0){
            TimeHours = duration/60 + ' To ' +(duration/60+1) + ' hours';
        }else{
            TimeHours = duration/60 + '.' + duration1 + ' To ' +(duration/60+1) + '.' + duration1 + ' hours';
        }*/
        
        system.debug('--TimeHours ---'+TimeHours);
        
        Date myDate  = null;
        Date myDate1 = null;
        
        if(custInteraction.Start_Date__c == null){
            custInteraction.Start_Date__c = Date.Today().Adddays(2);
        }
        if(custInteraction.End_Date__c == null){
            custInteraction.End_Date__c = Date.Today().Adddays(180);
        }
        
        if(custInteraction.Start_Date__c != null){
          //  custInteraction.Start_Date__c = Date.Today().Adddays(2);
              myDate = Date.newInstance(custInteraction.Start_Date__c.year(), custInteraction.Start_Date__c.month(), custInteraction.Start_Date__c.day());
        }
        if(custInteraction.End_Date__c != null){
            //custInteraction.End_Date__c = Date.Today().Adddays(32);
            myDate1 = Date.newInstance(custInteraction.End_Date__c.year(), custInteraction.End_Date__c.month(), custInteraction.End_Date__c.day());
        }
        
         
        
        if(NumberOFgas == 0 && NumberOFElec == 0){
            List<Call_List_Line_Item__c> callItems = [select id, customer__c,customer__r.Service_Address__c,customer__r.Service_City__c from 
                Call_List_Line_Item__c where id =: callListLineItemId];
                
                Address1 = callItems.get(0).customer__r.Service_Address__c; 
                City1 = callItems.get(0).customer__r.Service_City__c; 
        }
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        dsmtRouteParser.AddressParser obj1 = null;
        
        string address = EncodingUtil.urlEncode(Address1  +' '+City1, 'UTF-8');
        
        system.debug('--address ---'+address);
        
        //req.setEndpoint('https://gtesdemo.dsmtracker.com/RouteOptimizer/api/GetAddress?type=json&address='+Address);
        req.setEndpoint('http://gtesdemo.dsmtracker.com/RouteOptimizer/api/GetAddress?type=json&address='+Address+'&orgId='+userinfo.getOrganizationId());
        
        req.setMethod('GET');
        
        http = new Http();
        if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
        }else{
        
             Map<String, String> body2 = new Map<String, String>();
                body2.put('status','test');
                body2.put('Address','test');
                body2.put('City','test');
                body2.put('county','test');
                body2.put('latitude','1.32');
                body2.put('longitude','12.4');
                body2.put('state_long','test');
                body2.put('country_long','test');
                body2.put('state_short','test');
                body2.put('country_short','test');
                body2.put('postalCode','test');
                String sbody = JSON.serialize(body2);
                res.setBody(sbody);
               
        }
        
        obj1 = (dsmtRouteParser.AddressParser) System.JSON.deserialize(res.getBody(), dsmtRouteParser.AddressParser.class);
        
        system.debug('--obj--'+obj1.formatedAddress);
        
        
        if(woTypeIdLst != null && woTypeIdLst.size() > 0){
            list<Required_Skill__c> rsLst = [select id, Skill__c,Skill__r.Name,Minimum_Score__c from Required_Skill__c where Workorder_Type__c In :woTypeIdLst ];
            
            set<string> skillIds = new set<string>();
            
            for(Required_Skill__c r : rsLst){
                skillIds.add(r.Skill__c);
            }
            
            system.debug('--skillIds---'+skillIds);
            
            list<Employee_Skill__c> esLst = [select id, Skill__c,Employee__c,Employee__r.Name,Survey_Score__c from Employee_Skill__c where skill__c in : skillIds];
    
            
            Map<Id,list<Employee_Skill__c>> esMap = new Map<Id,list<Employee_Skill__c>>();
            list<Employee_Skill__c> temp = null;
            
            for(Employee_Skill__c  es : esLst){
                    
                    if(esMap.get(es.Employee__c) != null){
                        temp = esMap.get(es.Employee__c);
                        temp.add(es);
                        esmap.put(es.Employee__c,temp);
                    }else{
                        temp = new list<Employee_Skill__c>();
                        temp.add(es);
                        esmap.put(es.Employee__c,temp);
                    }
            }
            system.debug('--esmap--'+esmap);
            Set<Id> empSkill = new Set<Id>();
            boolean add = false;
            Set<Id> empIdset = new Set<Id>();
            boolean reqadd = false;
            string strEmpId ='';
            
            for(Id empId : esMap.keyset()){
                add = false;
                temp = esmap.get(empId);
                
                for(Required_Skill__c r : rsLst){
                    reqadd = false;
                    for(Employee_Skill__c  es : temp){
                        if(es.Survey_Score__c >= r.Minimum_Score__c){
                            if(r.Skill__c == es.Skill__c){
                                reqadd = true;
                            }
                        }
                    }
                    if(reqadd == false){
                    break;
                    }
                }
                if(!reqadd){
                    add = false;
                }else{
                    add = true;
                }
                
                if(add){
                    
                    if(strEmpId  == ''){
                        strEmpId = EmpId ;
                    }else{
                        strEmpId += '~~~'+ EmpId ;
                    }
                }
            }
            
            SelectedEmployee = strEmpId ;
        }
        
        if(duration > 480){
            duration = 480;
        }
        
      //  apptMap1  =  dsmtCallOut.GetAppoitments(SelectedEmployee,myDate,myDate1,'15 Medway Rd, Milford, MA 01757',duration );
       
        dsmtCallOut.StaticSessionId = SessionId;
        system.debug('--dayOfWeekFilter ---'+dayOfWeekFilter);
        if(dayOfWeekFilter == '[]'){
            dayOfWeekFilter = null;
        }else if(dayOfWeekFilter != null){
            dayOfWeekFilter = dayOfWeekFilter.replace(']','').replace('[','');
        }
        if(!Test.isRunningTest()){
            apptMap1  =  dsmtCallOut.GetAppoitments(SelectedEmployee,myDate,myDate1,obj1.formatedAddress.replace('#',''),duration,timeFilter,dayOfWeekFilter,null);
        }
        else{
            apptMap1  =  dsmtCallOut.GetAppoitments(SelectedEmployee,myDate,myDate1,obj1.formatedAddress,duration,timeFilter,dayOfWeekFilter,null);
            
        }
        
        system.debug('--apptMap1---'+apptMap1);
        
        showbutton = false;
        
        if(apptMap1.get('false') != null){
            apptMap = new List<SelectOption>();
            isError = true;
            dsmtCallOut.AppointmentOption obj = apptMap1.get('false');  
            system.debug('--obj.error---'+obj.error); 
            
            List<DSM_Tracker_Configuration_Line_Item__c> dsmtList = [select Config_Value__c,Config_HTML_Value__c from DSM_Tracker_Configuration_Line_Item__c where
                                                    Config_Name__c = 'No Appointment Found' and DSM_Tracker_Configuration__r.Configuration_Name__c = 'Program Eligibility'];
                                                     
            if(dsmtList != null && dsmtList.size() > 0){
               // AppterrorMsg  = dsmtList.get(0).Config_Value__c;
            }
            AppterrorMsg = obj.error;
            
            showbutton = true;
            
            //AppterrorMsg = AppterrorMsg.replace('[[CUSTOMER PHONE]]',custinteraction.phone__c);
              List<call_List__c> callList = [select id from call_List__c where name = 'Requires Call Back Queue'];
                
                Call_List_Line_Item__c cli = new Call_List_Line_Item__c();
                
                if(callList != null && callList.size() > 0){
                    cli.Call_List__c = callList.get(0).Id;
                }
                cli.Call_Type__c = 'Outbound';
                
                List<Eligibility_Check__c> newecList = [select id,First_Name__c,Last_Name__c,Phone__c,Email__c from Eligibility_Check__c
                                                            where id =: custinteraction.Id];
                
                cli.Call_Notes__c = AppterrorMsg;
                cli.Status__c = 'Ready To Call';
                cli.Call_List_Type__c = 'Requires Call Backs';
                insert cli;
            //AppterrorMsg =   obj.error;
        }
        else{
            isError = false;
            system.debug('--SelectedEmployee---'+SelectedEmployee);
            
            system.debug('--apptMap1---'+apptMap1);
            apptMap = new List<SelectOption>();
            
            for(String str : apptMap1.keySet()){
                dsmtCallOut.AppointmentOption obj = apptMap1.get(str); 
                
                system.debug('--obj.SessionId--'+obj.SessionId);
                system.debug('--obj---'+obj);
                SessionId =  obj.SessionId;
                system.debug('--SessionId--'+SessionId );
                dsmtCallOut.StaticSessionId =  obj.SessionId;
                //apptMap.add(new SelectOption(str,obj.earlyArrivalTime+' - '+obj.LateArrivalTime + ' on '+ obj.ServiceDate));   
               // apptMap.add(new SelectOption(str,obj.serviceTimeSlot + ' on '+ obj.ServiceDate));   
               apptMap.add(new SelectOption(str,obj.displaySlotText+ ' on '+ obj.ServiceDate)); 
            }
        system.debug('---apptMap---'+apptMap);
        //getEmployee();
        }
    }
    
    public string SelectedWorkTeam{get;set;}
    
    public void CreateWorkOrder(){
        
        system.debug('----'+custInteraction.Customer_is_Flexible__c);
        
        system.debug('--notes--'+notes);
        List<Call_List_Line_Item__c> callItem = null;
        if(CustomerId == null || CustomerId == ''){
            callItem = [select id, customer__c from Call_List_Line_Item__c  where id =: callListLineItemId];
            if(callItem  != null && callItem.size() > 0){
                customerId = callItem.get(0).customer__c ;
            }
        }
        
        if(apptMap1 != null){
            dsmtCallOut.AppointmentOption obj = apptMap1.get(SelectedWorkTeam);   
            system.debug('--obj ---'+obj);
            Integer year = integer.valueOf(obj.serviceDate.split('/').get(2));
            Integer month = integer.valueOf(obj.serviceDate.split('/').get(0));
            Integer day  = integer.valueOf(obj.serviceDate.split('/').get(1));
            Integer hour = integer.valueOf(obj.serviceTime.split(':').get(0));
            Integer min = integer.valueOf(obj.serviceTime.split(':').get(1));
            
            datetime StartDate = datetime.newInstance(year,month,day,hour,min,0);
            datetime EndDate = StartDate.AddMinutes(integer.valueOf(obj.serviceDuration));
            
            datetime ArrvStartDate = null;
            String str = '';
            
            if(obj.earlyArrivalTime != null){
                if(obj.earlyArrivalTime.contains('AM')){
                    str = obj.earlyArrivalTime.split(' ').get(0);
                    ArrvStartDate = datetime.newInstance(year,month,day,integer.valueOf(str.split(':').get(0)),integer.valueOf(str.split(':').get(1)),0);
                }else{
                    str = obj.earlyArrivalTime.split(' ').get(0);
                    hour = integer.valueOf(str.split(':').get(0));
                    min = integer.valueOf(str.split(':').get(1));
                    if( hour > 12){
                        hour += 12;    
                    }
                    ArrvStartDate = datetime.newInstance(year,month,day,hour,min,0);
                }
            }
            system.debug('--ArrvStartDate ---'+ArrvStartDate);
            
            datetime LateArrvDate = null;
            str = '';
            
            if(obj.lateArrivalTime != null){
                if(obj.lateArrivalTime.contains('AM')){
                    str = obj.lateArrivalTime.split(' ').get(0);
                    LateArrvDate = datetime.newInstance(year,month,day,integer.valueOf(str.split(':').get(0)),integer.valueOf(str.split(':').get(1)),0);
                }else{
                    str = obj.lateArrivalTime.split(' ').get(0);
                    hour = integer.valueOf(str.split(':').get(0));
                    min = integer.valueOf(str.split(':').get(1));
                    if( hour > 12){
                        hour += 12;    
                    }
                    LateArrvDate = datetime.newInstance(year,month,day,hour,min,0);
                }
            }
            system.debug('--LateArrvDate ---'+LateArrvDate );  
            
            String Type = '';
            String OwnRent = '';
            string opt = ApexPages.currentPage().getParameters().get('opt');
            
                Workorder__c wo = new Workorder__c();
                wo.Requested_Start_Date__c = StartDate;
                wo.Requested_End_Date__c = EndDate;
                wo.Requested_Date__c = StartDate.Date();
                
                wo.Early_Arrival_Time__c = ArrvStartDate;
                wo.Late_Arrival_Time__c = LateArrvDate ;
                
                wo.Scheduled_Start_Date__c = StartDate;
                wo.Scheduled_End_Date__c = EndDate;
                wo.Scheduled_Date__c = wo.Requested_Date__c;
                if(!Test.IsrunningTest()){
                    wo.Work_Team__c = obj.workTeamId;
                }
                //wo.Eligibility_Check__c = eleCheckId;
                if(reviewId != null && reviewId != ''){
                   wo.Review__c = reviewId;
                }
                if(callItem  != null && callItem.size() > 0){
                    wo.Call_List_Line_Item__c = callItem.get(0).Id;
                }
                
                List<Customer__c> customerList = [select id,Service_Address_Full__c,Service_City__c,Service_State__c,Service_Postal_Code__c
                            from customer__c where Id =: customerId];
                
                if(customerList != null && customerList.size() > 0){
                    wo.Address__c = customerList.get(0).Service_Address_Full__c;
                    wo.City__c = customerList.get(0).Service_City__c;
                    wo.State__c = customerList.get(0).Service_State__c;
                    wo.Zipcode__c = customerList.get(0).Service_Postal_Code__c;
                    wo.Zip__c = customerList.get(0).Service_Postal_Code__c;
                    
                    wo.customer__c = customerList.get(0).Id;
                }
                wo.Address__c = custInteraction.Service_Address__c;
                
                List<Work_Team__c> wtList = [select id,Captain__r.DSMTracker_Contact__r.Trade_Ally_Account__c,location__c from work_Team__c where id =: obj.workTeamId];
                
                if(wtList != null && wtList.size() > 0){
                    wo.location__c = wtList.get(0).location__c;
                    wo.Trade_Ally_Account_New__c = wtList.get(0).Captain__r.DSMTracker_Contact__r.Trade_Ally_Account__c;
                }
                wo.Status__c = 'Scheduled';
                if(woTypeId != null && woTypeId != ''){
                    wo.WorkOrder_Type__c = woTypeId;
                }
                if(obj.serviceDuration != null && obj.serviceDuration != ''){
                    wo.Duration__c = double.valueOf(obj.serviceDuration);
                }
                if(custinteraction.Phone__c != null){
                    wo.Phone__c = custinteraction.Phone__c;
                }
                wo.External_Reference_ID__c = obj.workOrderRefId;
                wo.Flexible__c = custInteraction.Customer_is_Flexible__c;
                wo.notes__c = notes;
                
                if(ReviewId != null && opt == 'Inspection'){
                    List<Review__c> rvlist = [Select id,name,Energy_Assessment__c,Energy_Assessment__r.Workorder__r.Customer_Eligibility__c,
                                              Energy_Assessment__r.Workorder__r.Eligibility_Check__c from Review__c where id =: ReviewId];
                    if(rvlist.size() > 0){
                        Review__c rv = rvlist.get(0);
                            if(rv.Energy_Assessment__r.Workorder__c != null){
                                if(rv.Energy_Assessment__r.Workorder__r.Customer_Eligibility__c != null){
                                    wo.Customer_Eligibility__c = rv.Energy_Assessment__r.Workorder__r.Customer_Eligibility__c;
                                }
                                if(rv.Energy_Assessment__r.Workorder__r.Eligibility_Check__c != null){
                                    wo.Eligibility_Check__c = rv.Energy_Assessment__r.Workorder__r.Eligibility_Check__c;
                                }
                            }
                    }
                }
                
                insert wo;
                List<Eligibility_Check__c> ecList = [select id,Status__c from Eligibility_Check__c where id =: eleCheckId];
            
                if(ecList != null && ecList.size() > 0){
                    ecList.get(0).Status__c = 'Passed';
                    update ecList;
                }
                
                Customer_Interaction__c ci = new Customer_Interaction__c();
                ci.Customer__c = wo.customer__c;
                //ci.Eligibility_Check__c = eleCheckId;
                ci.Type__c = 'Online Eligibility check';
                ci.Interaction_Date_Time__c = Datetime.now();
                
                String dtConverted = DateTime.now().format('MM/dd/yyyy h:mm a');
            
                ci.Interaction_Notes__c = 'Customer check eligibility on ' ;
                ci.Interaction_Notes__c += dtConverted;
                ci.Interaction_Notes__c += ' and eligibility check passed';
                insert ci;
                
                List<Workorder__c> woList = [select id,name,Work_Team__r.Name,Workorder_Type__r.Name from workOrder__c where id =: wo.Id];
                
                ci = new Customer_Interaction__c();
                ci.Customer__c = wo.customer__c;
                //ci.Eligibility_Check__c = eleCheckId;
                ci.Type__c = 'Work Order Created';
                ci.Interaction_Date_Time__c = Datetime.now();
               // ci.Interaction_Notes__c = 'Work Order ' + woList.get(0).Name + ' Created on ' +DateTime.now().Month() + '/'+DateTime.now().Day() + '/'+DateTime.now().Year() + ' at ' +DateTime.now().Hour() +':'+DateTime.now().Minute() + ' For ' +woList.get(0).work_team__r.Name;
                ci.Interaction_Notes__c = 'Work Order ' + woList.get(0).Name + ' of type '+ woList.get(0).Workorder_Type__r.Name+' has been scheduled for ' ;
                
                dtConverted = wo.Scheduled_Start_Date__c.format('MM/dd/yyyy h:mm a');
                
                ci.Interaction_Notes__c += dtConverted;
                
                ci.Interaction_Notes__c += ' For ' +woList.get(0).work_team__r.Name;
                
                //+wo.Scheduled_Start_Date__c.Month() + '/'+wo.Scheduled_Start_Date__c.Day() + '/'+wo.Scheduled_Start_Date__c.Year() + ' at ' +wo.Scheduled_Start_Date__c.Hour() +':'+wo.Scheduled_Start_Date__c.Minute() + ' For ' +woList.get(0).work_team__r.Name;
                insert ci;
                
                dsmtCallOut.ScheduleWorkOrder(obj.workOrderRefId,wo.Id,wo.Scheduled_Date__c,obj.workTeamId,obj.serviceTimeSlot);
                //DMSPT-2 by HP on 21st Sep 2018
                if(customerId != null){
                    customer__c objCust = new Customer__c();
                    objCust.Id = customerId;
                    objCust.Preferred_Language__c = custinteraction.Preferred_Language__c;
                    objCust.Other_Language__c = custinteraction.Other_Language__c;
                    update objCust;
                }
            
                //update custInteraction;
        
           

        }
    }
    public string eleCheckId{get;set;}
    public string ErrorMsg{get;set;}
   
    
    
    
    public string  multiFamilyResult{get;set;}
    
    
    
    public List<SelectOption> WoType{get;set;}
    public Integer minutesToAdd{get;set;}
    public void GetWoType(){
        minutesToAdd = 0;
        newApp.Appointment_Start_Time__c = null;
        newApp.Appointment_End_Time__c = null;
        woType = new List<SelectOption>();
        string opt = ApexPages.currentPage().getParameters().get('opt');

            List<Workorder_Type__c> newworderType = null;
            
            //Integer duration = integer.valueOf(newworderType.get(0).Est_Total_Time__c);
        
            //minutesToAdd = duration;
            woType.add(new Selectoption('', '--Select--'));
            
                   if(opt != null && opt == 'Inspection'){
                      newworderType = [select id, name from Workorder_Type__c WHERE For_Inspection_Workorder__c = true order by name asc];
                      for(Workorder_Type__c wo : newworderType ){  
                          system.debug('--wo--'+wo);     
                        //if(wo.name.contains('Inspection'))
                            woType.add(new Selectoption(wo.Id,wo.Name));
                      }
                   }else{
                      newworderType = [select id, name from Workorder_Type__c WHERE Requires_Eligibility__c = false];
                      for(Workorder_Type__c wo : newworderType ){       
                        woType.add(new Selectoption(wo.Id,wo.Name));
                      }
                   }
           
            
    
    }
}