@isTest
public class CreateProjectEnergyAssesmentExtnTest{
    public testmethod static void test1(){
       
        
        DSMTracker_Product__c dp = new DSMTracker_Product__c();
        dp.category__c='Test';
        dp.Sub_Category__c='Lighting';
        dp.Start_Date__c =null; 
        dp.End_date__c =null;
        insert dp;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
      
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Building_Shape__c='circle';
        ea.BuildingSize__c='cross';
        insert ea;
        
        Recommendation__c r = new Recommendation__c();
        r.Energy_Assessment__c = ea.id;
        //r.Recommendation_Scenario__c = proj.Id;
        r.DSMTracker_Product__c =dp.id;
        r.Type__c='Major Measure';
        r.category__c = 'Test';
        insert r; 
      
        ApexPages.currentPage().getParameters().put('projId',proj.id);
        ApexPages.currentPage().getParameters().put('ID',ea.id);
       
       CreateProjectEnergyAssesmentExtn.addReccomModel  cntrl1 = new CreateProjectEnergyAssesmentExtn.addReccomModel();
       cntrl1.category = 'Test';
       CreateProjectEnergyAssesmentExtn cntrl = new CreateProjectEnergyAssesmentExtn();
       cntrl.availableSkill();
       cntrl.addReccomView = cntrl1;
       cntrl.getCategoryOptions();
       cntrl.getSubcategoryOptions();
       cntrl.getHubIdOptions();
       cntrl.initRecommendations();
       cntrl.DeleteRecommendation();
       cntrl.getRecommendationTypeOptions();
       cntrl.callSubCategory();
       r.Type__c= 'Direct Install';
       update r;
       cntrl.availableSkill();
       cntrl.availabledirectInstallList();
       cntrl.saveProject();
       cntrl.save();
       cntrl.saveNewReccom();
       
   }
}