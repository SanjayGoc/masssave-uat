public class dsmtSkylightComponentController {
    public Id SkylightId{get;set;}
    
    public Skylight__c Skylight{get{
        if(Skylight == null){
            Skylight = new Skylight__c();
        }
        return Skylight;
    }set;}
    
    public dsmtSkylightComponentController(){
        if(SkylightId != null){
            String query = 'SELECT '+ getSObjectFields('Skylight__c') + ' FROM Skylight__c WHERE Id=:SkylightId';
            System.debug('query--'+query);
            List<Skylight__c> Skylights = Database.query(query);
            if(Skylights.size() > 0){
                Skylight = Skylights.get(0);
            }
        }
    }
    
    private static Schema.DescribeSObjectResult dsr = Skylight__c.sObjectType.getDescribe();
    private static Schema.FieldSet getFieldSet(String fieldSetName){
        Schema.FieldSet fset = null;
        Map<String,Schema.FieldSet> fsMap = dsr.fieldSets.getMap();
        for(String key : fsMap.keySet()){
            if(key.equalsIgnoreCase(fieldSetName)){
                fset = fsMap.get(fieldSetName);
            }
        }
        return fset;
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        List<String> fields = new List<String>();
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            fields.add(dfield.getName());
        }
        return String.valueOf(fields).replace('(','').replace(')','');
    }
}