@isTest
public class dsmtProposalControllerTest 
{
    testMethod static void test() 
    {
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        test.startTest();
       
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        proposal.Current_Incentive__c = 10;
        proposal.Status__c='Contracted';
        proposal.Current_Incentive__c=10;
        insert proposal;
        
        system.debug('current incentive ====='+proposal.Current_Incentive__c);
        proposal.Create_Project_Manually__c=true;
        proposal.Current_Incentive__c=15;
        proposal.Status__c='Signed';
         proposal.Name_Type__c='Air Sealing';
        update proposal;
        
        system.debug('current incentive ====='+proposal.Current_Incentive__c);
        
        DSMTracker_Product__c dsmtp1 = new DSMTracker_Product__c();
        dsmtp1.Incentive_Type__c = 'Dollars';
        insert dsmtp1;
        
        DSMTracker_Product__c dsmtp = new DSMTracker_Product__c();
        dsmtp.Air_Sealing_Proposal_Type_Dependency__c = dsmtp1.Id;
        dsmtp.Incentive_Type__c = 'Percentage';
        insert dsmtp;
  
        Recommendation__c rem = new Recommendation__c();
        rem.Energy_Assessment__c = ea.id;
        rem.Dsmtracker_Product__c =dsmtp.id;
        
        rem.Proposal__c=proposal.id;
        rem.Invoice_Type__c =  'NSTAR418';
        insert rem;
        
        Proposal_Recommendation__c pro =new Proposal_Recommendation__c();
        pro.Proposal__c=proposal.id;
        pro.Recommendation__c=rem.id;
        pro.Project__c = pro.id;
        insert pro; 
       
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
       
        dsmtProposalController cntrl =new dsmtProposalController();
        cntrl.newProposal.name_type__c='Weatherization';
        cntrl.setShowProposal();
        boolean recomAdded = cntrl.recomAdded;
        String congatemplate = cntrl.congatemplate;
        String congaQuery = cntrl.congaQuery;
        String contractorInitials = cntrl.contractorInitials;
        cntrl.proposalId=proposal.Id;
        cntrl.ShowSelectedProposals();
        cntrl.recomIds=rem.Id;
        cntrl.addRecommendations();
        cntrl.saveCloneProposal();
        cntrl.deleteProposal();
        cntrl.editProposal();
        test.stopTest();
        
    }
    testMethod static void test1() 
    {
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        test.startTest();
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Air Sealing';
        insert proposal;
        
        APXTConga4__Conga_Template__c act =new APXTConga4__Conga_Template__c();
        act.Unique_Template_Name__c='Renter Energy Report';
        insert act;
        
        APXTConga4__Conga_Merge_Query__c accmq =new APXTConga4__Conga_Merge_Query__c();
        accmq.Unique_Conga_Query_Name__c='Audit_Report_Query';
        insert accmq;
        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        
        dsmtProposalController cntrl =new dsmtProposalController();
        cntrl.newProposal.name_type__c='Weatherization';
        cntrl.proposalId=proposal.Id;
        
        cntrl.viewName='Audit Report';
        dsmtProposalController.saveDualSign(ea.id, proposal.id, 'imgURI1', 'imgURI2', 'Audit Report', 'contractorInitials', 'customerSignDateStr', 'esSignDateStr',true,'');
        dsmtProposalController.saveSign(ea.id, proposal.id, 'imgURI', 'Audit Report');
        cntrl.check_print_collateral_status();
        cntrl.ReportType ='Renter Receipt/Report';
        cntrl.generateReportData();    
        test.stopTest();
    }
    testMethod static void test2() 
    {
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        insert proposal;
        test.startTest();
        Recommendation__c rem = new Recommendation__c();
        rem.Energy_Assessment__c = ea.id;
        rem.Proposal__c=proposal.id;
        rem.Invoice_Type__c =  'NSTAR418';
        insert rem;
        
        Proposal_Recommendation__c pro =new Proposal_Recommendation__c();
        pro.Proposal__c=proposal.id;
        pro.Recommendation__c=rem.id;
        insert pro; 
        

        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        
        dsmtProposalController cntrl =new dsmtProposalController();
        cntrl.newProposal.name_type__c='Weatherization';
        cntrl.proposalId=proposal.Id;
        
        cntrl.viewName='contractView';
        dsmtProposalController.saveDualSign(ea.id, proposal.id, 'imgURI1', 'imgURI2', 'contractView', 'contractorInitials', 'customerSignDateStr', 'esSignDateStr',true,'');
        dsmtProposalController.saveSign(ea.id, proposal.id, 'imgURI', 'contractView');
        
        test.stopTest();
    }
   testMethod static void test3() 
    {
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        insert proposal;
        test.startTest();
        Recommendation__c rem = new Recommendation__c();
        rem.Energy_Assessment__c = ea.id;
        rem.Proposal__c=proposal.id;
        rem.Invoice_Type__c =  'NSTAR418';
        insert rem;
        
        Proposal_Recommendation__c pro =new Proposal_Recommendation__c();
        pro.Proposal__c=proposal.id;
        pro.Recommendation__c=rem.id;
        insert pro;
        
        APXTConga4__Conga_Template__c act =new APXTConga4__Conga_Template__c();
        act.Unique_Template_Name__c='Homeowner Energy Report';
        insert act;
        
        APXTConga4__Conga_Merge_Query__c accmq =new APXTConga4__Conga_Merge_Query__c();
        accmq.Unique_Conga_Query_Name__c='Audit_Report_Query';
        insert accmq;
        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        
        dsmtProposalController cntrl =new dsmtProposalController();
        
        cntrl.newProposal.name_type__c='Weatherization';
        cntrl.proposalId=proposal.Id;
        cntrl.recomIds=rem.Id;
        cntrl.getdataForSignContractDocument();
        cntrl.ReportType='Audit Report';
        cntrl.generateReportData();
        cntrl.getdataForSignDocument();
        test.stopTest();
    }
    testMethod static void test4() 
    {
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        insert proposal;
        test.startTest();
        Recommendation__c rem = new Recommendation__c();
        rem.Energy_Assessment__c = ea.id;
        rem.Proposal__c=proposal.id;
        rem.Invoice_Type__c =  'NSTAR418';
        insert rem;
        
        Proposal_Recommendation__c pro =new Proposal_Recommendation__c();
        pro.Proposal__c=proposal.id;
        pro.Recommendation__c=rem.id;
        insert pro;
        
        Attachment__c att = new Attachment__c();
        //att.Project__c = pro.Id;
        att.attachment_Type__c='Contract Document';
        att.status__c = 'Completed';
        att.Proposal__c = proposal.Id;
        insert att;
        
        Attachment attch= new Attachment();
        attch.Name= 'test';
        attch.ParentId= att.id;
        attch.Body= blob.ValueOf('test');
        insert attch;
        
        Proposal__c proposal1 =new Proposal__c();
        proposal1.Energy_Assessment__c=ea.id;
        proposal1.Name_Type__c='Air Sealing';
        insert proposal1;
        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        
        dsmtProposalController cntrl =new dsmtProposalController();
        cntrl.newProposal.name_type__c='Weatherization';
        cntrl.proposalId=proposal.Id;
        cntrl.newCustomAttachmentId=att.id;
        cntrl.viewName='contractView';
        cntrl.migrateAttachServiceCall = true;
        cntrl.checkCongaStatus();
        cntrl.showProposalRecommendations();
        test.stopTest();
    }
    testMethod static void test5()
    {
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        test.startTest();
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        insert proposal;
         
        Recommendation__c rem = new Recommendation__c();
        rem.Energy_Assessment__c = ea.id;
        rem.Proposal__c=proposal.id;
        rem.Invoice_Type__c =  'NSTAR418';
        insert rem;
       
        Proposal_Recommendation__c pro =new Proposal_Recommendation__c();
        pro.Proposal__c=proposal.id;
        pro.Recommendation__c=rem.id;
        insert pro;
        
        APXTConga4__Conga_Template__c act =new APXTConga4__Conga_Template__c();
        act.Unique_Template_Name__c='Customer Receipt SHV Form';
        insert act;
        
        APXTConga4__Conga_Merge_Query__c accmq =new APXTConga4__Conga_Merge_Query__c();
        accmq.Unique_Conga_Query_Name__c='Audit_Report_Query';
        insert accmq;
        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        
        dsmtProposalController cntrl =new dsmtProposalController();
        
        cntrl.newProposal.name_type__c='Weatherization';
        cntrl.proposalId=proposal.Id;
        cntrl.recomIds=rem.Id;
        //cntrl.getdataForSignContractDocument();
        cntrl.ReportType='Customer Receipt SHV';
        cntrl.generateReportData();
        //cntrl.getdataForSignDocument();
        cntrl.getTechnicalBundleList();
        test.stopTest();
    }
    testMethod static void test6() 
    {
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        insert proposal;
        test.startTest();
        Recommendation__c rem = new Recommendation__c();
        rem.Energy_Assessment__c = ea.id;
        rem.Proposal__c=proposal.id;
        rem.Invoice_Type__c =  'NSTAR418';
        insert rem;
        
        Proposal_Recommendation__c pro =new Proposal_Recommendation__c();
        pro.Proposal__c=proposal.id;
        pro.Recommendation__c=rem.id;
        insert pro;
        
        Attachment__c att = new Attachment__c();
        //att.Project__c = pro.Id;
        att.attachment_Type__c='Contract Document';
        att.status__c = 'Completed';
        att.Proposal__c = proposal.Id;
        insert att;
        
        Attachment attch= new Attachment();
        attch.Name= 'test';
        attch.ParentId= att.id;
        attch.Body= blob.ValueOf('test');
        insert attch;
        
        Proposal__c proposal1 =new Proposal__c();
        proposal1.Energy_Assessment__c=ea.id;
        proposal1.Name_Type__c='Air Sealing';
        insert proposal1;
        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        
        dsmtProposalController cntrl =new dsmtProposalController();
        cntrl.newProposal.name_type__c='Weatherization';
        cntrl.proposalId=proposal.Id;
        cntrl.newCustomAttachmentId=att.id;
        cntrl.viewName='Customer Receipt SHV';
        cntrl.checkCongaStatus();
        cntrl.showProposalRecommendations();
        test.stopTest();
    }
}