@IsTest
public with sharing class dsmtAllAppointmentsCntrlTest {
    @IsTest
    static void testCase1(){
        test.startTest();
        Account acc = Datagenerator.createAccount();
        Premise__c prem = Datagenerator.createPremise();
        Customer__c cust = Datagenerator.createCustomer(acc.Id,prem.Id);
        
        User portalUser = Datagenerator.CreatePortalUser();
        DSMTracker_Contact__c newDSMTracker = Datagenerator.createDSMTracker();
        Eligibility_Check__c elCheck = Datagenerator.createELCheck();
        elCheck.Customer__c = cust.Id;
        Appointment__c app = Datagenerator.createAppointment(elCheck.Id);
        app.Appointment_Status__c = 'Completed';
        app.DSMTracker_Contact__c = newDSMTracker.Id;
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        insert ta;
        
        newDSMTracker.Trade_Ally_Account__c=ta.id;
        
        newDSMTracker.Contact__c = portalUser.contactid;
        update  newDSMTracker;
        
        Workorder__c wo = Datagenerator.Createwo();
        wo.Early_Arrival_Time__c = Datetime.now();
        wo.Late_Arrival_Time__c = Datetime.now();
        insert wo;
        
        app.Workorder__c = wo.Id;
        update app;
        update elCheck;
        
        Appointment__c ap= new Appointment__c();
        ap.Appointment_Start_Time__c=datetime.newInstance(2014, 9, 15, 12, 30, 0);
        insert ap;
        
        System.runAs(portalUser){
            dsmtAllAppointmentsCntrl cntrl = new dsmtAllAppointmentsCntrl();
            cntrl.initAllAppointments();
            cntrl.custId = cust.Id;
            cntrl.AppointmentId = app.Id;
            cntrl.dsmtcId = new Set<Id>{newDSMTracker.Id};
            cntrl.queryAppointment();
            cntrl.CancenAppointment();
            cntrl.updateAppoinment();
            
            dsmtAllAppointmentsCntrl.queryAppList(newDSMTracker.Id);
            
            dsmtAllAppointmentsCntrl.AppointmentModal aptModel = new dsmtAllAppointmentsCntrl.AppointmentModal();
            aptModel.Id = 'test';
            aptModel.CId = 'test';
            aptModel.Cnm= 'test';
            aptModel.FNm= 'test';
            aptModel.Lnm= 'test';
            aptModel.Pn= 'test';
            aptModel.Em= 'test';
            aptModel.SS= 'test';
            aptModel.SC= 'test';
            aptModel.SSt= 'test';
            aptModel.SZ= 'test';
            aptModel.EP= 'test';
            aptModel.EA= 'test';
            aptModel.GP= 'test';
            aptModel.GA= 'test';
            
            aptModel.AT= 'test';
            aptModel.AN= 'test';
            aptModel.ST= 'test';
            aptModel.ET= 'test';
            aptModel.Emp= 'test';
            aptModel.Sts= 'test';
            aptModel.CD= 'test';
            aptModel.CI= 'test';
            dsmtAllAppointmentsCntrl.AppointmentModal aptModel2 = new dsmtAllAppointmentsCntrl.AppointmentModal();
        }
        test.stopTest();        
    }
    
    @IsTest
    static void testCase2(){
        test.startTest();
        Account acc = Datagenerator.createAccount();
        Premise__c prem = Datagenerator.createPremise();
        Customer__c cust = Datagenerator.createCustomer(acc.Id,prem.Id);
        
        User portalUser = Datagenerator.CreatePortalUser();
        DSMTracker_Contact__c newDSMTracker = Datagenerator.createDSMTracker();
        Eligibility_Check__c elCheck = Datagenerator.createELCheck();
        elCheck.Customer__c = cust.Id;
        Appointment__c app = Datagenerator.createAppointment(elCheck.Id);
        app.Appointment_Status__c = 'Completed';
        app.DSMTracker_Contact__c = newDSMTracker.Id;
        //Trade_Ally_Account__c newTAaccount = Datagenerator.createTradeAccount();
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        insert ta;
        
        newDSMTracker.Trade_Ally_Account__c=ta.id;
        newDSMTracker.Contact__c = portalUser.contactid;
        update  newDSMTracker;
        update app;
        update elCheck;
        
        System.runAs(portalUser){
            dsmtAllAppointmentsCntrl cntrl = new dsmtAllAppointmentsCntrl();
            cntrl.custId = null;
            cntrl.addNewAppointment();
        }
        test.stopTest();
    }
 }