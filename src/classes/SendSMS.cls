/*

Developer : Santosh M Reddy
Description : Send reminder SMS to opt in customers 24 hours prior to Appointment date.
JIRA #  : REN-2359.
Version : 1.0
Dependencies : Twillio API.Remote site settings of Twillio api.

*/

public class SendSMS{

    public String phNumber{get;set;}
    public String smsBody{get;set;}
    String accountSid;
    String serviceSid;
    string token;
    String fromPhNumber;
    public String smsRespBody {get;set;}
    public smsRespWrapper smsResp {get;set;}
    public errorResponseWrapper erw {get;set;}
    public String customerid{get;set;}
    public String employeeId{get;set;}
    public String appointmentId{get;set;}

    public sendsms(){

        if(Apexpages.currentpage() != null) {
            phNumber ='+'+Apexpages.currentpage().getparameters().get('phNumber');
            customerid = Apexpages.currentpage().getparameters().get('cid');
            employeeId = Apexpages.currentpage().getparameters().get('eid');
        }

        // Test Credenetial
        //accountSid = 'AC3f46579b4e92ffd850b1ec526f1953bd'; //ACa7431fecf29186b1d1afee5893327d2a
        //token = 'c069ccfdc7be76f6ec6955f22845eb91'; //2ede2648dc5121251a75e5b7e0661f5f
        
        // Original Credential
        accountSid = Mobile_Setting__c.getorgdefaults().SMS_Account_Id__c; //'ACa7431fecf29186b1d1afee5893327d2a'; 
        token = Mobile_Setting__c.getorgdefaults().SMS_Token__c; //'2ede2648dc5121251a75e5b7e0661f5f'; //
        fromPhNumber = Mobile_Setting__c.getorgdefaults().SMS_From_Phone_Number__c; //'8447357606'; //(678) 974-0740
       
        //serviceSid = 'PN69dfa073e100a52f967bcbc79bb949ec';

    }
    
    public PageReference startSendSms(){
        
            
        SMS__c s = processSms();
        
        insert s;
        
        PageReference myVFPage = new PageReference('/'+ (s.Customer__c != null ? s.Customer__c : s.Employee__c));
        myVFPage.setRedirect(true);
        return myVFPage;

    }

    public SMS__c processSms(){
    
        if(Test.isRunningTest()) {
            accountSid = 'ACaXXXXXXXXXXXXXXXXXXXXXXXXXXXXd2a';
        }
        
        String url = Mobile_Setting__c.getorgdefaults().SMS_URL__c; 
        if(url != null){
            url += accountSid+'/SMS/Messages.json';
        }
        system.debug('--accountSid --'+accountSid );
        system.debug('--token--'+token);
        
        HttpRequest req = new HttpRequest();
        system.debug('--url--'+url);
        req.setEndpoint(url);
        req.setMethod('POST');
        String VERSION  = '3.2.0';
        req.setHeader('X-Twilio-Client', 'salesforce-' + VERSION);
        req.setHeader('User-Agent', 'twilio-salesforce/' + VERSION);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Accept-Charset', 'utf-8');
        req.setHeader('Authorization','Basic '+EncodingUtil.base64Encode(Blob.valueOf(accountSid+':' +token)));
        String sid =  fromPhNumber + String.valueof(System.currentTimeMillis());
        
        System.debug('phNumber' + phNumber);
        if(phNumber != null) {
            phNumber = phNumber.replaceAll('[^\\d]', '');
            System.debug('phNumber' + phNumber);    
            /*if(phNumber.length() <= 10) {
                phNumber = '1'+phNumber;
            }*/
            phNumber = '+1'+phNumber;
        }
        System.debug('phNumber : ' + phNumber); 
        System.debug('serviceSid :' + serviceSid);
        System.debug('fromPhNumber' + fromPhNumber);
        req.setBody('To='+EncodingUtil.urlEncode(phNumber,'UTF-8')+'&From='+EncodingUtil.urlEncode(fromPhNumber,'UTF-8')+'&Body='+smsBody);

        Http http = new Http();

        HTTPResponse res = http.send(req);

        System.debug(res.getBody());
        smsRespBody = res.getBody();
        
        if(res.getStatusCode()==201){
            smsResp = (smsRespWrapper)json.deserialize(smsRespBody,smsRespWrapper.class);
            if(Apexpages.currentpage() != null) {                               
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'SMS Sent Successfully'));
            }            
        } else{
            smsResp = null;
            erw =(errorResponseWrapper)json.deserialize(res.getBody(),errorResponseWrapper.class);
            if(Apexpages.currentpage() != null) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,erw.message));
            }
        }
        
        SMS__c s = new SMS__c();
        s.Mobile_No__c = phNumber;
        s.Message_Text__c = smsBody;
        s.Customer__c = customerId;
        s.Employee__c = EmployeeId;
        s.Appointment__c = appointmentID;
        if(smsResp != null) {
            s.Message_Id__c = smsResp.sid;
            s.Status__c = 'Sent';
        } else {
            s.Status__c = 'Failed';
        }
        
        s.Response__c = smsRespBody;
        
        return s;
    }
    
    @future(callout=true)
    public static void startsendSMS(set<String> appointmentIds){
       
       List<SMS__c> smslist = new List<SMS__c>();
      if(appointmentIds.size() > 0){
         List<Appointment__c> applist = [select id, Customer_Reference__c,Customer_Reference__r.Send_SMS_Notification__c,WorkOrder__c,Workorder__r.Name,WorkOrder__r.Early_Arrival_Time__c,WorkOrder__r.Late_Arrival_Time__c,WorkOrder__r.Address_Formula__c,Workorder__r.Scheduled_Start_Date__c,Customer_Reference__r.Name,Customer_Reference__r.Phone__c,Appointment_Status__c,Employee__c,DSMTracker_Contact__r.Portal_User__r.MobilePhone,DSMTracker_Contact__r.Portal_User__r.Send_SMS_Notification__c from appointment__c where id in: appointmentIds];
         for(Appointment__c app : applist){
             
               //for scheduled appointment SMS to customer and Employee
               if(app.Appointment_status__c == 'Scheduled'){
                    
                    //for customer 
                    system.debug('--for customer scheduled --'+app.Customer_Reference__r.Phone__c);                
                    if(app.Customer_Reference__r.Phone__c != null && app.Customer_Reference__r.Send_SMS_Notification__c){
                        
                        String smsText = Mobile_Setting__c.getorgdefaults().SMS_Scheduled_Text_For_Customer__c;
                        
                        //replace dynamic value in sms text
                        system.debug('--smsText--'+smsText);
                        smsText = smsText.Replace('[Scheduled Date]',app.WorkOrder__r.Scheduled_Start_Date__c.format());
                        smsText = smsText.Replace('[Early Arrival]',app.WorkOrder__r.Early_Arrival_Time__c.format('HH:mm a'));
                        smsText = smsText.Replace('[Late Arrival]',app.WorkOrder__r.Late_Arrival_Time__c.format('HH:mm a'));
                        smsText = smsText.Replace('[Work Order Number]',app.WorkOrder__r.Name);
                        system.debug('--smsText--'+smsText);
                        system.debug('--smsText.length()--'+smsText.length());
                        Integer smscount = ((smsText.length()-1) / 160) + 1;
                        
                        system.debug('--smscount--'+smscount);
                        
                        for(Integer i = 1;i<=smscount;i++){
                             
                             integer startindex = 160* (i-1);
                             integer endindex = startindex + 159;
                             
                             system.debug('--startindex--'+startindex);
                             system.debug('--endindex--'+endindex);
                             
                             if(endindex > smsText.length()){
                                endindex = smsText.length();
                             }
                                  
                             String sendText = smsText.substring(startindex,endindex);
                             system.debug('--sendText--'+sendText);
                             
                             SendSMS sms = new SendSMS();
                             sms.phNumber = app.Customer_Reference__r.Phone__c;
                             sms.customerid = app.Customer_Reference__c;
                             sms.appointmentID = app.id;
                             sms.smsBody = sendText;
                            
                             smslist.add(sms.processSms());     
                       }
                    }
                    
                    //for ES
                    system.debug('--for ES scheduled --'+app.DSMTracker_Contact__r.Portal_User__r.MobilePhone);                
                    if(app.DSMTracker_Contact__r.Portal_User__r.MobilePhone != null && app.DSMTracker_Contact__r.Portal_User__r.Send_SMS_Notification__c){
                       
                        String smsText = Mobile_Setting__c.getorgdefaults().SMS_Scheduled_Text_for_ES__c;
                        
                        //replace dynamic value in sms text
                        system.debug('--smsText--'+smsText);
                        smsText = smsText.Replace('[Customer Name]',app.Customer_Reference__r.Name);
                        smsText = smsText.Replace('[Scheduled Date]',app.WorkOrder__r.Scheduled_Start_Date__c.format());
                        smsText = smsText.Replace('[Address]',app.WorkOrder__r.Address_Formula__c);
                        smsText = smsText.Replace('[Work Order Number]',app.WorkOrder__r.Name);
                        smsText = smsText.Replace('[Phone]',app.Customer_Reference__r.Phone__c);
                        system.debug('--smsText--'+smsText);
                        system.debug('--smsText.length()--'+smsText.length());
                        Integer smscount = ((smsText.length()-1) / 160) + 1;
                        
                        system.debug('--smscount--'+smscount);
                        
                        for(Integer i = 1;i<=smscount;i++){
                             
                             integer startindex = 160* (i-1);
                             integer endindex = startindex + 159;
                             
                             system.debug('--startindex--'+startindex);
                             system.debug('--endindex--'+endindex);
                             
                             if(endindex > smsText.length()){
                                endindex = smsText.length();
                             }
                                  
                             String sendText = smsText.substring(startindex,endindex);
                             system.debug('--sendText--'+sendText);
                             
                             SendSMS sms = new SendSMS();
                             sms.phNumber = app.DSMTracker_Contact__r.Portal_User__r.MobilePhone;
                             sms.EmployeeId = app.Employee__c;
                             sms.appointmentID = app.id;
                             sms.smsBody = sendText;
                            
                             smslist.add(sms.processSms());     
                       }
                        
                    }
                    
               }
               
               //for Cancelled appointment send sms to ES
             if(app.Appointment_status__c == 'Cancelled'){   
               system.debug('--for ES Cancelled --'+app.DSMTracker_Contact__r.Portal_User__r.MobilePhone);                
                if(app.DSMTracker_Contact__r.Portal_User__r.MobilePhone != null && app.DSMTracker_Contact__r.Portal_User__r.Send_SMS_Notification__c){
                    
                    String smsText = Mobile_Setting__c.getorgdefaults().SMS_Cancelled_Text_for_ES__c;
                    
                    //replace dynamic value in sms text
                    system.debug('--smsText--'+smsText);
                    smsText = smsText.Replace('[Customer Name]',app.Customer_Reference__r.Name);
                    smsText = smsText.Replace('[Scheduled Date]',app.WorkOrder__r.Scheduled_Start_Date__c.format());
                    smsText = smsText.Replace('[Address]',app.WorkOrder__r.Address_Formula__c);
                    smsText = smsText.Replace('[Work Order Number]',app.WorkOrder__r.Name);
                    smsText = smsText.Replace('[Phone]',app.Customer_Reference__r.Phone__c);
                    system.debug('--smsText--'+smsText);
                    system.debug('--smsText.length()--'+smsText.length());
                    Integer smscount = ((smsText.length()-1) / 160) + 1;
                    
                    system.debug('--smscount--'+smscount);
                    
                    for(Integer i = 1;i<=smscount;i++){
                         
                         integer startindex = 160* (i-1);
                         integer endindex = startindex + 159;
                         
                         system.debug('--startindex--'+startindex);
                         system.debug('--endindex--'+endindex);
                         
                         if(endindex > smsText.length()){
                            endindex = smsText.length();
                         }
                              
                         String sendText = smsText.substring(startindex,endindex);
                         system.debug('--sendText--'+sendText);
                         
                         SendSMS sms = new SendSMS();
                         sms.phNumber = app.DSMTracker_Contact__r.Portal_User__r.MobilePhone;
                         sms.EmployeeId = app.Employee__c;
                         sms.appointmentID = app.id;
                         sms.smsBody = sendText;
                        
                         smslist.add(sms.processSms());     
                   }
                    
                }
             }    
             
              //for Cancel Requested appointment send sms to ES
             if(app.Appointment_status__c == 'Cancel Requested'){   
               system.debug('--for ES Cancelled --'+app.DSMTracker_Contact__r.Portal_User__r.MobilePhone);                
                if(app.DSMTracker_Contact__r.Portal_User__r.MobilePhone != null && app.DSMTracker_Contact__r.Portal_User__r.Send_SMS_Notification__c){
                    
                    String smsText = Mobile_Setting__c.getorgdefaults().SMS_Cancel_Requested_Text_For_ES__c;
                    
                    //replace dynamic value in sms text
                    system.debug('--smsText--'+smsText);
                    smsText = smsText.Replace('[Customer Name]',app.Customer_Reference__r.Name);
                    smsText = smsText.Replace('[Scheduled Date]',app.WorkOrder__r.Scheduled_Start_Date__c.format());
                    smsText = smsText.Replace('[Address]',app.WorkOrder__r.Address_Formula__c);
                    smsText = smsText.Replace('[Work Order Number]',app.WorkOrder__r.Name);
                    smsText = smsText.Replace('[Phone]',app.Customer_Reference__r.Phone__c);
                    system.debug('--smsText--'+smsText);
                    system.debug('--smsText.length()--'+smsText.length());
                    Integer smscount = ((smsText.length()-1) / 160) + 1;
                    
                    system.debug('--smscount--'+smscount);
                    
                    for(Integer i = 1;i<=smscount;i++){
                         
                         integer startindex = 160* (i-1);
                         integer endindex = startindex + 159;
                         
                         system.debug('--startindex--'+startindex);
                         system.debug('--endindex--'+endindex);
                         
                         if(endindex > smsText.length()){
                            endindex = smsText.length();
                         }
                              
                         String sendText = smsText.substring(startindex,endindex);
                         system.debug('--sendText--'+sendText);
                         
                         SendSMS sms = new SendSMS();
                         sms.phNumber = app.DSMTracker_Contact__r.Portal_User__r.MobilePhone;
                         sms.EmployeeId = app.Employee__c;
                         sms.appointmentID = app.id;
                         sms.smsBody = sendText;
                        
                         smslist.add(sms.processSms());     
                   }
                }
             }  
             
             //for reschedule appointment requested SMS to customer and Employee
               if(app.Appointment_status__c == 'Reschedule Appointment Requested'){
                    
                    //for customer 
                    system.debug('--for customer scheduled --'+app.Customer_Reference__r.Phone__c);                
                    if(app.Customer_Reference__r.Phone__c != null && app.Customer_Reference__r.Send_SMS_Notification__c){
                        
                        String smsText = Mobile_Setting__c.getorgdefaults().SMS_Rescheduled_Text_for_Customer__c;
                        
                        //replace dynamic value in sms text
                        system.debug('--smsText--'+smsText);
                        smsText = smsText.Replace('[Scheduled Date]',app.WorkOrder__r.Scheduled_Start_Date__c.format());
                        smsText = smsText.Replace('[Early Arrival]',app.WorkOrder__r.Early_Arrival_Time__c.format('HH:mm a'));
                        smsText = smsText.Replace('[Late Arrival]',app.WorkOrder__r.Late_Arrival_Time__c.format('HH:mm a'));
                        smsText = smsText.Replace('[Work Order Number]',app.WorkOrder__r.Name);
                        smsText = smsText.Replace('[Work Order Id]',app.WorkOrder__c);
                        system.debug('--smsText--'+smsText);
                        system.debug('--smsText.length()--'+smsText.length());
                        Integer smscount = ((smsText.length()-1) / 160) + 1;
                        
                        system.debug('--smscount--'+smscount);
                        
                        for(Integer i = 1;i<=smscount;i++){
                             
                             integer startindex = 160* (i-1);
                             integer endindex = startindex + 159;
                             
                             system.debug('--startindex--'+startindex);
                             system.debug('--endindex--'+endindex);
                             
                             if(endindex > smsText.length()){
                                endindex = smsText.length();
                             }
                                  
                             String sendText = smsText.substring(startindex,endindex);
                             system.debug('--sendText--'+sendText);
                             
                             SendSMS sms = new SendSMS();
                             sms.phNumber = app.Customer_Reference__r.Phone__c;
                             sms.customerid = app.Customer_Reference__c;
                             sms.appointmentID = app.id;
                             sms.smsBody = sendText;
                            
                             smslist.add(sms.processSms());     
                       }
                    }
                    
                    
                    
               }
               
         }
      } 
      
      if(smslist.size() > 0){
         insert smslist;
      }
    }
   
    public class errorResponseWrapper{

        public String code{get;set;}
        public String message{get;set;}
        public String moreInfo{get;set;}
        public String status{get;set;}    

    }
   
    public class smsRespWrapper {
        public String sid{get;set;}
        public String status{get;set;}
    }

}