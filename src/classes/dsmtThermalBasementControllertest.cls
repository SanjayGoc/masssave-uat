@isTest
Public class dsmtThermalBasementControllertest{
 
    public testmethod static void test1(){
        
        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        insert tet;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        rc.Thermal_Envelope_Type__c=tet.id;
        insert rc;
        
        layer__c  la = new layer__c();
        la.name = 'test';
        insert la;
        
        apexpages.currentpage().getparameters().put('layerType' , 'Insulation');
       // apexpages.currentpage().getparameters().put('index' , la);
        dsmtThermalBasementController cntrl= new dsmtThermalBasementController();
        cntrl.RecommendationId =rc.id;
        
        list<layer__c> lalist = cntrl.layers;
        map<id,layer__c> idlamap = cntrl.getLayersMap();
        
        cntrl.savelayers();
        cntrl.addNewLayer();
        cntrl.getLayerTypeOptions();
        cntrl.saveNewReccom();
        list<recommendation__c> rclist = cntrl.getBaseCeilingRecom();
        cntrl.editReccom();
        //cntrl.deleteLayer();
        cntrl.DeleteRecommendation();
  }
}