@isTest
private class dsmtCompletionDocumentsControllerTest 
{
    /*@isTest
    private static void test1()
    {

        Test.startTest();
        
        dsmtCompletionDocumentsController cls = new dsmtCompletionDocumentsController();
        
        List<sObject> templateslist =new List<sObject>();
        
        List<Document> documentList = new List<Document>();
        Document document; 
        
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.FolderId = UserInfo.getUserId();
        document.Name = 'Contractor Work Order';
        insert document;
        
        documentList.add(document);
        
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'my_document2';
        document.IsPublic = true;
        document.Name = 'certificate of Completion';
        document.FolderId = UserInfo.getUserId();
        insert document;            
        
        documentList.add(document);
        cls.selectedDocuments = 'Contractor Work Order';
        cls.saveOutboundConcate();

        cls.documents = new List<string>{'Contractor Work Order'};
        cls.documents.add('certificate of Completion');
        cls.counter = 0;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();  
        insert proj;
        Review__c rev = new Review__c(Project__c = proj.id);
        insert rev;
        
        Project_Review__c pr = new Project_Review__c();
        pr.Project__c = proj.Id;
                
        Attachment__c attc = new Attachment__c();
        insert attc;
         
        cls.generateCongaDocument();
        cls.counter = 1;
        cls.generateCongaDocument();
        cls.enablePollar = false;  
        
        cls.newCustomAttachmentId = attc.Id;    
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cls.newCustomAttachmentId;
        insert attach;  
        
        cls.checkCongaStatus(); 
        cls.generateCongaDocument();
        Test.stopTest(); 
    }*/
    @istest
    static void runtest()
    {
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Energy_Assessment__c=ea.id;
        insert proj;
        
        test.startTest();
        Review__c rev = new Review__c(Project__c = proj.id);
        insert rev;
        
        APXTConga4__Conga_Template__c act =new APXTConga4__Conga_Template__c();
        act.Unique_Template_Name__c='Contractor Work Order';
        
        insert act;

        APXTConga4__Conga_Merge_Query__c acmq =new APXTConga4__Conga_Merge_Query__c();
        acmq.Unique_Conga_Query_Name__c='Contractor_Work_Order3';
        insert acmq;
        
        Project_Review__c pr =new Project_Review__c();
        pr.Project__c=proj.id;
        pr.Review__c=rev.id;
        insert pr;
        
        Attachment__c attc = new Attachment__c();
        attc.Status__c='New';
        attc.Review__c=rev.id;
        insert attc;
   

        
        apexpages.currentpage().getparameters().put('Id',rev.id);
        
        dsmtCompletionDocumentsController cls = new dsmtCompletionDocumentsController();
        

        cls.selectedDocuments='Contractor Work Order:';
        cls.saveOutboundConcate();
        cls.counter=0;
        cls.generateCongaDocument();
        cls.newCustomAttachmentId = attc.Id;
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cls.newCustomAttachmentId;
        insert attach;
        cls.checkCongaStatus();
        test.stopTest();
    }
    
    @istest
    static void runtest1()
    {
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Energy_Assessment__c=ea.id;
        insert proj;
        
        Test.startTest();
        
        Review__c rev = new Review__c(Project__c = proj.id);
        insert rev;
        
        APXTConga4__Conga_Template__c act =new APXTConga4__Conga_Template__c();
        act.Unique_Template_Name__c='CERTIFICATE_OF_COMPLETION';
        
        insert act;

        APXTConga4__Conga_Merge_Query__c acmq =new APXTConga4__Conga_Merge_Query__c();
        acmq.Unique_Conga_Query_Name__c='Certificate_of_completionform_Query';
        insert acmq;
        
        Project_Review__c pr =new Project_Review__c();
        pr.Project__c=proj.id;
        pr.Review__c=rev.id;
        insert pr;
        
        apexpages.currentpage().getparameters().put('Id',rev.id);
        
        dsmtCompletionDocumentsController cls = new dsmtCompletionDocumentsController();

        cls.selectedDocuments='Certificate of Completion:';
        cls.saveOutboundConcate();
        cls.counter=0;
        cls.generateCongaDocument();
        Test.stopTest();
    }
}