@istest(seeAllData=true)
public class LightingTriggerHandlerTest 
{
    static Energy_Assessment__c ea=new Energy_Assessment__c();
 	static void setUpData()
    {
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        Account acc = Datagenerator.createAccount();
        Premise__c prem = Datagenerator.createPremise();
        Customer__c cust = Datagenerator.createCustomer(acc.Id,prem.Id);
        Building_Specification__c bs = new Building_Specification__c();
        bs.Floors__c = 1;
        bs.Bedromm__c=2;
        bs.Occupants__c = 2;
        bs.Ceiling_Heights__c = 2;
        bs.Year_Built__c = '1900';
        insert bs;
        Lighting__c light = new Lighting__c();
        light.Name = 'test';
        insert light;
        
        Eligibility_Check__c elCheck = Datagenerator.createELCheck();
        elCheck.Customer__c = cust.Id;
        Appointment__c app = Datagenerator.createAppointment(elCheck.Id);
        WeatherStation__c ws = new WeatherStation__c(WeatherStationName__c = 'station1',NormalNFactor__c=1);
        
        insert ws;
        
        ea = new Energy_Assessment__c();
        ea.Trade_Ally_Account__c = ta.Id;
        ea.Dsmtracker_contact__c = dsmt.Id;
        ea.Building_Specification__c = bs.Id;
        ea.Lighting__c = light.Id;
        ea.Appointment__c = app.id;
        ea.FootprintArea__c = 2;
        ea.WeatherStation__c = ws.Id;
        ea.Building_Shape__c = '10';
        ea.FrontLength__c = 1;
        ea.SideLength__c = 1;
        insert ea;
        
    }
    static testmethod void test()
    {
        setupData();
        Test.startTest();
        Lighting__c lig =new Lighting__c();
        lig.Energy_Assessment__c=ea.id;
        lig.Efficient_Lights__c='Some';
        lig.Name='General Lighting';
        lig.General_Indoor_Lighting__c='';
        lig.General_Outdoor_Lighting__c='';
        lig.Has_Outdoor_Fixtures__c=false;
        insert lig;
        
        Building_Specification__c bs =new Building_Specification__c();
        bs.Floor_Area__c=12;
        bs.Occupants__c=5;
        insert bs;
        Map<Id, Lighting__c> oldMap = new Map<Id, Lighting__c>();
        //lig.General_Indoor_Lighting__c != oldmap.get(lig.Id).General_Indoor_Lighting__c;
        LightingTriggerHandler lth =new LightingTriggerHandler();
        Test.stopTest();
    }
}