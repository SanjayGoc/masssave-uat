public class dsmtInvoiceTypeWOHelper{
    
    public static void setInvoiceType(List<Workorder__c> recList){
        
        Set<Id> prodId = new Set<Id>();
        Set<Id> wtypeId = new Set<Id>();
        
        for(Workorder__c rec : recList){
                        
            if(rec.Dsmtracker_Product__c != null){
                prodId.add(rec.Dsmtracker_Product__c);
            }
            
            if(rec.Workorder_Type__c != null){
                wtypeId.add(rec.Workorder_Type__c);
            }
        }
        
        if(prodId.size() > 0){
                List<Dsmtracker_Product__c> dsmtProdList = [select id,name,Category__c,RecordType.Name,Sub_Category__c,Surface__c
                                                                 from Dsmtracker_Product__c where id in : prodId];
                
                Map<Id,Dsmtracker_Product__c> dsmtProdMap = new Map<Id,Dsmtracker_Product__c>();
                
                for(Dsmtracker_Product__c dsmt : dsmtProdList){
                    dsmtProdMap.put(dsmt.Id,dsmt);
                }
                
                List<Workorder_Type__c> wtypeList = [select id,name from Workorder_Type__c where id in : wtypeId];
                
                Map<Id,String> wtypeMap = new Map<Id,String>();
                
                for(Workorder_type__c wtype : wtypeList){
                    wtypeMap.put(wtype.Id,wtype.Name);
                }
                
                String Type = null;
                String PrimaryProvider = null;
                String SecondaryProvider = null;
                String RecTypeName = null;
                
                for(Workorder__c rec : recList){
                    Type = null;
                    PrimaryProvider = null;
                    SecondaryProvider = null;
                    RecTypeName = null;
                
                    if(rec.Dsmtracker_Product__c != null){
                        
                        system.debug('--dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c---'+dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c);
                        
                        RecTypeName = dsmtProdMap.get(rec.Dsmtracker_Product__c).RecordType.Name;
                        
                       // if(dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c == 'Direct Install'){
                            
                            if(rec.Workorder_Type__c != null){
                                //if(eaMap.get(rec.Energy_Assessment__c) != null){
                                    Type = wtypeMap.get(rec.Workorder_Type__c);
                                    PrimaryProvider = rec.Primary_Provider__c;
                                    SecondaryProvider = rec.Secondary_Provider__c;
                                    
                                    system.debug('--Type ---'+Type );
                                    
                                    if(Type == 'HEA (Home Energy Assessment)' || Type == 'Landlord Visit' || Type == 'Expanded HEA' ||
                                        Type == 'Duct Specification' || Type == 'Combustion Safety Return Visit'){
                                        
                                        if(PrimaryProvider == 'Eversource East Electric'){
                                            if(SecondaryProvider == 'Eversource East Gas' || SecondaryProvider == null || SecondaryProvider == ''){
                                                rec.Invoice_Type__c = 'NSTAR418';
                                            }
                                        }else if(PrimaryProvider == 'Eversource West Electric'){
                                            if(SecondaryProvider == null || SecondaryProvider == ''){
                                                rec.Invoice_Type__c = 'WMECO418';
                                            }
                                        }else if(PrimaryProvider == 'Eversource East Gas'){
                                            if(SecondaryProvider == 'Eversource East Electric'){
                                               /* if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                                    rec.Invoice_Type__c = 'NSTAR416';
                                                }else{*/
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                //}
                                            }/*else if(SecondaryProvider == 'Eversource West Electric'){
                                                if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                                    rec.Invoice_Type__c = 'WMECO416';
                                                }else{
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                }
                                            }*/else if(SecondaryProvider == 'National Grid Electric'){
                                                /*if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                                    rec.Invoice_Type__c = 'NATGRID418';
                                                }else{*/
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                //}
                                            }else if(SecondaryProvider == null || SecondaryProvider == ''){
                                                rec.Invoice_Type__c = 'NSTARGAS416';
                                            }
                                        }
                                    } else if(Type == 'SHV (Special Home Visit)' || Type == 'Renter Visit'){
                                            
                                            if(PrimaryProvider == 'Eversource East Electric'){
                                                if(SecondaryProvider == 'Eversource East Gas' || SecondaryProvider == null || SecondaryProvider == ''){
                                                    rec.Invoice_Type__c = 'NSTAR416S';
                                                }
                                            }else if(PrimaryProvider == 'Eversource West Electric'){
                                                if(SecondaryProvider == null || SecondaryProvider == ''){
                                                    rec.Invoice_Type__c = 'WMECO416S';
                                                }
                                            }else if(PrimaryProvider == 'Eversource East Gas'){
                                                if(SecondaryProvider == 'Eversource East Electric'){
                                                    /*if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                                        rec.Invoice_Type__c = 'NSTAR416S';
                                                    }else{*/
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    //}
                                                }/*else if(SecondaryProvider == 'Eversource West Electric'){
                                                    if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                                        rec.Invoice_Type__c = 'WMECO416S';
                                                    }else{
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    }
                                                }*/else if(SecondaryProvider == 'National Grid Electric'){
                                                   /* if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                                        //rec.Invoice_Type__c = 'NSTAR418';
                                                        rec.Invoice_Type__c = 'NATGRID418';
                                                    }else{*/
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                   // }
                                                }else if(SecondaryProvider == null || SecondaryProvider == ''){
                                                    rec.Invoice_Type__c  = 'NSTARGAS416S';
                                                }
                                            }
                                    }else if(Type == 'In-Process Inspection' || Type == 'Post Work Inspection' || Type == 'Post Work Inspection with Loan Verification' ||
                                                Type == 'Loan Verification' || Type == 'Re-Inspection'){
                                                
                                        if(PrimaryProvider == 'Eversource East Gas'){
                                            rec.Invoice_Type__c = 'NSTARGAS413';
                                        }
                                        else if(PrimaryProvider == 'Eversource East Electric'){
                                            rec.Invoice_Type__c = 'NSTAR413';
                                        }
                                        
                                        else if(PrimaryProvider == 'Eversource West Electric'){
                                            rec.Invoice_Type__c = 'WMECO413';
                                        }
                                    }
                               // }
                            }
                       /* }else{
                            String Category = dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c;
                            String SubCategory = dsmtProdMap.get(rec.Dsmtracker_Product__c).Sub_Category__c;
                            String Surface = dsmtProdMap.get(rec.Dsmtracker_Product__c).Surface__c;
                            
                            system.debug('--Category---'+Category);
                            system.debug('--SubCategory---'+SubCategory);
                            system.debug('--Surface---'+Surface);
                           
                            if(Category == 'Air Sealing' || Category == 'Air Sealing/Insulation' || Category == 'Electric Heat Thermostat' ||
                                Category == 'HVAC' || Category == 'Insulation' || Category == 'Programmable Thermostat' ||
                                Category == 'Refrigerator' || Category == 'Ventilation' ||
                                Category == 'Duct Measures'){
                                 
                                if(rec.Workorder_Type__c != null){
                                //if(eaMap.get(rec.Energy_Assessment__c) != null){
                                    Type = rec.Workorder_Type__r.Name;
                                    PrimaryProvider = rec.Primary_Provider__c;
                                    SecondaryProvider = rec.Secondary_Provider__c;
                                    
                                    system.debug('--Type---'+Type);
                                    system.debug('--PrimaryProvider---'+PrimaryProvider);
                                    system.debug('--SecondaryProvider---'+SecondaryProvider);
                            
                                     
                                        if(PrimaryProvider == 'Eversource East Gas'){
                                            if(SecondaryProvider == 'Eversource East Electric'){
                                                if(Category == 'Air Sealing' || Category == 'Air Sealing/Insulation' ||
                                                    Category == 'Insulation' || Category == 'Duct Measures'){
                                                    rec.Invoice_Type__c = 'NSTARGAS413';
                                                }else if(Category == 'HVAC' && (Subcategory == 'Heating' || Surface == 'Thermostat')){
                                                    rec.Invoice_Type__c = 'NSTAR413';
                                                }
                                            }else if(SecondaryProvider == 'Eversource West Electric'){
                                                if(Category == 'Air Sealing' || Category == 'Air Sealing/Insulation' ||
                                                    Category == 'Insulation' || Category == 'Duct Measures'){
                                                    rec.Invoice_Type__c = 'NSTARGAS413';
                                                }else if(Category == 'HVAC' && (Subcategory == 'Heating' || Surface == 'Thermostat')){
                                                    rec.Invoice_Type__c = 'WME413';
                                                }
                                            }
                                        }
                                        else if(PrimaryProvider == 'Eversource East Electric'){
                                            rec.Invoice_Type__c = 'NSTAR413';
                                        }
                                        
                                        else if(PrimaryProvider == 'Eversource West Electric'){
                                            rec.Invoice_Type__c = 'WMECO413';
                                        }
                               // }
                                }    
                            }
                        }*/
                    }
                    
                    if(rec.Invoice_Type__c == null){
                        rec.Invoice_Type__c = 'Not Found';
                    }
                }
            }
            
        }
    //}    
}