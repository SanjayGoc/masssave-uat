global class dsmtAddEditServiceRequestCntrl{
    public boolean editable{get;set;}
    public string ticketId{get;set;}
    public Service_Request__c objTicket{get;set;}
    public Phase_I_Ticket_Detail__c objPTicket{get;set;}
    public string body{get;set;}
    public String pageSectionTitle{get;set;}
    public String WorokOrdersJSON{get;set;}
    public DSMTracker_Contact__c dsmtContact{get;set;}
    public String CongaTemplateId{get;set;}
    public String CongaEmailTemplateId{get;set;}
    public String orgWideId{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}   

    public Note newNote{get{
        if(this.newNote == null){
            newNote = new Note(ParentId = objTicket.Id);
        }
        return newNote;
    }set;}
    /*public Task newCallLog{get{
        if(newCallLog == null){
            newCallLog = new Task(
                WhatId = objTicket.Id,
                TaskSubType = 'Call'
            );    
        }
        return newCallLog;
    }set;}*/
    public CallLogModel newCallLog{get{
        if(this.newCallLog == null){
            this.newCallLog = new CallLogModel(objTicket.Id);
        }
        return this.newCallLog;
    }set;}
    public boolean dmlSuccess{get;set;}
    public boolean editMode{get;set;}
    public String srStatus{get;set;}
    public Customer__c customerToUpdate{get;set;}
    
    public dsmtAddEditServiceRequestCntrl(){
        
            isPortal = true;
            headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
            List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
                if(lstUser.size()>0){
                    if(lstUser[0].IsPortalEnabled){
                        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                    }
                    else{
                        headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                        isPortal = false;
                    }
                }
        
       ticketId = ApexPages.currentPage().getParameters().get('id');
        if(ticketId == null || ticketId == ''){
            editMode = false;
            ticketId = ApexPages.currentPage().getParameters().get('tid');
            if(ticketId == null || ticketId == ''){
                editable = false;
            }
        }else{
            editable = true;
            editMode = true;
        }

        
        User usr = [SELECT Id,Name,ContactId FROM User WHERE Id =:UserInfo.getUserId() LIMIT 1];
        List<Dsmtracker_Contact__c> dsmtContacts = null; 
        if(usr != null && usr.ContactId != null){
           dsmtContacts = [SELECT Id,Name,Super_User__c,Trade_Ally_Account__c,contact__c FROM DSMTracker_Contact__c WHERE (contact__c =: usr.ContactId ) LIMIT 1];    
        }else{
           dsmtContacts = [SELECT Id,Name,Super_User__c,Trade_Ally_Account__c,contact__c FROM DSMTracker_Contact__c WHERE (portal_User__c =: usr.Id) LIMIT 1];            
        }
        if(dsmtContacts.size() > 0){
            dsmtContact = dsmtContacts.get(0);
        }
        system.debug('--dsmtContact--'+dsmtContact);
        
       if(ticketId!=null){
           List<Phase_I_Ticket_Detail__c> lstPTicekts = [
               SELECT 
                    Id,Name,Service_Request__c,Service_Request__r.Customer_Contact__c,Rebate_Status__c,Number_of_Heating_Systems__c,Manufacturer__c,Model__c,Serial_Number__c,MFG_Year__c,
                    Boiler_Type__c,Furnace_Type__c,Fuel_Type__c,Original_Audit_Date__c,Original_Audit_Type__c,Owner_Occupied__c,Customer_Name__c,
                    Program_Utility_Provider__c,Provider__c,Provider__r.Name,Offer_Reason__c,Rejection_Reason__c
               FROM Phase_I_Ticket_Detail__c
               WHERE Service_Request__c =: ticketId
           ];
           List<Service_Request__c> lstTickets = [
               select 
                    Id,Name,Customer_Contact__r.Id,Subject__c,Sub_Type__c,Type__c,Status__c,Priority__c,Description__c,RecordTypeId,Phone_Number__c,VIP__c,Energy_Specialist__c,Energy_Specialist__r.Name,
                    Customer__c,Customer__r.Name,Customer__r.Phone__c,Customer__r.Email__c,Customer__r.Service_Address__c,Customer__r.Service_Street_Number__c,Customer__r.Service_Street__c,Customer__r.Service_City__c,Customer__r.Service_State__c,Customer__r.Service_Postal_Code__c,
                    Workorder__c,Workorder__r.Name,Workorder__r.Customer_Name__c,Workorder__r.Address__c,Workorder__r.City__c,Workorder__r.State__c,Workorder__r.Zipcode__c,Workorder__r.Phone__c,Workorder__r.Email__C,
                    Program_Utility_Provider__c,Primary_Provider__c,Rebate_Status__c,Number_of_Heating_Systems__c,Manufacturer__c,Serial_Number__c,MFG_Year__c,Boiler_Type__c,Furnace_Type__c,Fuel_Type__c,Original_Audit_Date__c,Original_Audit_Type__c,Owner_Occupied__c
               from Service_Request__c
               where Id = :ticketId Limit 1
           ];
           if(lstTickets.size()>0){
               objTicket = lstTickets[0];
               //body = objTicket.Description__c;
               pageSectionTitle = 'Edit Service Request';
               if(lstPTicekts.size() > 0){
                   objPTicket = lstPTicekts.get(0);
               }else{
                   objPTicket = new Phase_I_Ticket_Detail__c();
               }
               if(objTicket.Customer__c != null){
                   customerToUpdate = new Customer__c(
                       Id = objTicket.Customer__c,
                       Email__c = objTicket.Customer__r.Email__c,
                       Phone__c = objTicket.Customer__r.Phone__c
                   ); 
               }else{
                   customerToUpdate = new Customer__c();
               }
           }

       }
        
        if(objTicket == null){
            objTicket = new Service_Request__c();
            objTicket.DSMTracker_Contact__c = dsmtContact.Id;
            objTicket.Status__c= 'Draft';
            
            List<User> userList = [select id,profile.Name from user where id =: userinfo.getUSerId()];
            if(userList != null && userList.size() > 0 && userList.get(0).Profile.Name == 'DSMT Heat Loan Office'){
                objTicket.Type__c = 'Heat Loan';
                Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Heat Loan').getRecordTypeId();
                objTicket.RecordTypeId = devRecordTypeId; 
            }else{
                objTicket.Type__c = 'Early Boiler & Furnace Request';
                Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Phase I Ticket Detail').getRecordTypeId();
                objTicket.RecordTypeId = devRecordTypeId; 
            }
            
            customerToUpdate = new Customer__c();
            
            editable = true;
            pageSectionTitle = 'New Service Request';
            
            
            objPTicket = new Phase_I_Ticket_Detail__c();
            objPTicket.RecordTypeId = Schema.SObjectType.Phase_I_Ticket_Detail__c.getRecordTypeInfosByName().get('Early Boiler & Furnace Request').getRecordTypeId();
        }
        srStatus = objTicket.Status__c;
    }
    
    public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    public void saveAndSubmit(){
        //objTicket.Status__c= 'Submitted';
        if(objTicket.Id == null || srStatus == null){
            srStatus = 'New';
        }
        objTicket.Status__c = srStatus;
        this.saveTicket();
    }
    public void saveTicket(){
        dmlSuccess = false;
        try{
            List<User> userList = [select id,contactId from user where id =: userinfo.getUserId()];
            if(userList != null && userList.size() > 0 && userList.get(0).ContactId != null){
                
                system.debug('--userList.get(0).ContactId---'+userList.get(0).ContactId);
                
                List<DSMTracker_Contact__c> dsmtconList = [
                    select 
                        id,Name,First_Name__c,Last_Name__c,Super_User__c,Trade_Ally_Account__c,Trade_Ally_Account__r.Email__c,
                        Trade_Ally_Account__r.First_Name__c,Trade_Ally_Account__r.Last_Name__c,
                        Trade_Ally_Account__r.OwnerId,Trade_Ally_Account__r.Owner.Email,contact__c,Trade_Ally_Account__r.Account_Manager__c,Trade_Ally_Account__r.Account_Manager__r.Email
                    from 
                        DSMTracker_Contact__c 
                    where 
                        contact__c =: userList.get(0).ContactId];
                
                if(dsmtConList != null){
                    objTicket.Trade_Ally_Account__c = dsmtConList.get(0).Trade_Ally_Account__c;
                    //objTicket.DSMTracker_Contact__c = dsmtConList.get(0).Id;
                    objTicket.Trade_Ally_Name__c = dsmtConList[0].Trade_Ally_Account__r.First_Name__c+' '+dsmtConList[0].Trade_Ally_Account__r.Last_Name__c;
                    objTicket.Trade_Ally_Main_Email__c = dsmtConList[0].Trade_Ally_Account__r.Email__c;
                    objTicket.DSMT_Contact_Name__c = dsmtConList[0].First_Name__c+' '+dsmtConList[0].Last_Name__c;                
                    
                    if(dsmtConList.get(0).Trade_Ally_Account__r.Account_Manager__c != null){
                        objTicket.Trade_Ally_Email__c = dsmtConList.get(0).Trade_Ally_Account__r.Account_Manager__r.Email; //lstUser[0].Email;
                        objTicket.Trade_Ally_Owner__c = dsmtConList.get(0).Trade_Ally_Account__r.Account_Manager__c; //lstUser[0].Id;    
                    }else if(dsmtConList.get(0).Trade_Ally_Account__r.OwnerId!=null){
                        objTicket.Trade_Ally_Email__c = dsmtConList.get(0).Trade_Ally_Account__r.Owner.Email; //lstUser[0].Email;
                        objTicket.Trade_Ally_Owner__c = dsmtConList.get(0).Trade_Ally_Account__r.OwnerId; //lstUser[0].Id;    
                    }
                }
            }
            objTicket.Requested_By__c = Userinfo.getuserid();  
            //objTicket.Description__c = body;
            
            if(customerToUpdate != null && objTicket.Customer__c != null){
                customerToUpdate.Id = objTicket.Customer__c;
                update customerToUpdate; 
            }
            
            System.debug('AFTER UPDATE customerToUpdate :: ' + customerToUpdate);
            System.debug('BEFORE UPSERT SR :: ' + objTicket);
            upsert objTicket;
            System.debug('AFTER UPSERT SR :: ' + objTicket);
            
            ticketId = objTicket.Id;
            if(objTicket != null && objTicket.Id != null){
                if(objPTicket.Id == null){
                    objPTicket.Service_Request__c = objTicket.Id;    
                }
                upsert objPTicket;
            }   
            dmlSuccess = true;
        }catch(Exception ex){
            dmlSuccess = false;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
            System.debug('Exception : ' + ex);
        }
    }
    
    public List<NoteModel> getSrNotes(){
        if(objTicket != null && objTicket.Id != null){
            NoteModel nm = new NoteModel([SELECT Id,Title,Body,CreatedDate,CreatedById,CreatedBy.Name FROM Note WHERE ParentId =: objTicket.Id ORDER BY CreatedDate]);
            return nm.notes;
        }
        return new List<NoteModel>();
    }
    public List<CallLogModel> getCallLogs(){
        if(objTicket != null && objTicket.Id != null){
            CallLogModel task = new CallLogModel([SELECT Id,WhatId,Subject,Description,CreatedDate,CreatedById,CreatedBy.Name FROM Task WHERE WhatId =: objTicket.Id AND TaskSubType = 'Call' ORDER BY CreatedDate]);
            return task.callLogs;
        }
        return new List<CallLogModel>();
    } 
    public void loadWorkOrders(){
        if(dsmtContact != null){
            String searchValue = ApexPages.currentPage().getParameters().get('searchValue');
            if(searchValue == null){
                searchValue = '';
            }
            searchValue = searchValue+'%';
            List<Appointment__c> appts = [
                SELECT Id,Name,Appointment_Start_Date__c,Workorder__c 
                FROM Appointment__c 
                WHERE 
                    DSMTracker_Contact__c =: dsmtContact.Id AND 
                    Workorder__c != NULL AND
                  Workorder__r.Name LIKE :searchValue AND
                    Appointment_Start_Date__c <= TODAY
                ORDER BY Appointment_Start_Date__c DESC 
                LIMIT 10
            ];
            Map<Id,Appointment__c> woAppts = new Map<Id,Appointment__c>();
            for(Appointment__c appt : appts){
                woAppts.put(appt.Workorder__c,appt);
            }
            List<Workorder__c> wos = [
                SELECT 
                    Id,Name,CreatedDate,Customer_Name__c,Address__c,Address_Formula__c,City__c,State__c,Zipcode__c,Phone__c,Email__c,
                    Eligibility_Check__c,Eligibility_Check__r.Do_you_heat_with_natural_Gas__c,
                    Customer__c,Customer__r.Phone__c,Customer__r.Email__c,Customer__r.Electric_provider__c,Customer__r.Electric_provider__r.Name,Customer__r.Gas_provider__c,Customer__r.Gas_provider__r.Name,                  
                    Customer_Eligibility__c,Customer_Eligibility__r.Validated_Electric_Account__c,Customer_Eligibility__r.Validated_Electric_Account__r.Name,Customer_Eligibility__r.Validated_Gas_Account__c,Customer_Eligibility__r.Validated_Gas_Account__r.Name
                FROM Workorder__c 
                WHERE Id IN :woAppts.keySet() 
                LIMIT 1000
            ];
            
            List<Object> jsList = new List<Object>();
            for(Workorder__c wo : wos){
                Appointment__c appt = woAppts.get(wo.Id);
                Id providerId = null;
                String providerName = null;
                
                if(wo.Eligibility_Check__c != null){
                    if(Boolean.valueOf(wo.Eligibility_Check__r.Do_you_heat_with_natural_Gas__c) == true){
                        if(wo.Customer_Eligibility__c != null && wo.Customer_Eligibility__r.Validated_Gas_Account__c != null){
                            providerId = wo.Customer_Eligibility__r.Validated_Gas_Account__c;
                            providerName = wo.Customer_Eligibility__r.Validated_Gas_Account__r.Name;
                        }else if(wo.Customer__c != null && wo.Customer__r.Gas_provider__c != null){
                            providerId = wo.Customer__r.Gas_provider__c;
                            providerName = wo.Customer__r.Gas_provider__r.Name;
                        }
                    }else{
                        if(wo.Customer_Eligibility__c != null && wo.Customer_Eligibility__r.Validated_Electric_Account__c != null){
                            providerId = wo.Customer_Eligibility__r.Validated_Electric_Account__c;
                            providerName = wo.Customer_Eligibility__r.Validated_Electric_Account__r.Name;
                        }else if(wo.Customer__c != null && wo.Customer__r.Electric_provider__c != null){
                            providerId = wo.Customer__r.Electric_provider__c;
                            providerName = wo.Customer__r.Electric_provider__r.Name;
                        }
                    }
                }
                
                jsList.add(new Map<String,Object>{
                    'Id' => wo.Id,
                        'Name' => wo.Name,
                            'CreatedDate' => wo.CreatedDate,
                                'Customer__c' => wo.Customer__c,
                                    'Customer_Name__c' => wo.Customer_Name__c,
                                        'Address__c' => wo.Address__c,
                                            'Address_Formula__c' => wo.Address_Formula__c,
                                                'City__c' => wo.City__c,
                                                    'State__c' => wo.State__c,
                                                        'Zipcode__c' => wo.Zipcode__c,
                                                            'Phone__c' => wo.Customer__r.Phone__c,
                                                                'Email__c' => wo.Customer__r.Email__c,
                                                                    'appt_Id' => appt.Id,
                                                                        'app_Name' => appt.Name,
                                                                            'app_Appointment_Start_Date__c' => appt.Appointment_Start_Date__c.format(),
                                                                                'providerId' => providerId,
                                                                                    'providerName' => providerName
                });
            }
            //List<Workorder__c> wos = [SELECT Id,Name,CreatedDate FROM Workorder__c LIMIT 1000];
            WorokOrdersJSON = JSON.serialize(jsList);
        }else {
            WorokOrdersJSON = JSON.serialize(new List<Object>());
        }
    }   
      @RemoteAction
    global static String getAttachmentsByTicketId(String pid,String p1dId) {
            list<Object> listAtts = new List<Object>();
            if(pid!=null && pid!=''){
                //Integer count = [Select COUNT() from Attachment__c where Attachment_Type__c = 'OfferLetter' and CreatedBy =: uid and Message__c = :pid];
                Integer count = [Select COUNT() from Attachment__c where Service_Request__c = :pid];
                if(count > 0){
                    for(Attachment__c att : [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Service_Request__c = :pid order by CreatedDate desc]){
                        listAtts.add(att);
                    }
                } 
            }
            if(p1dId != null && p1dId != ''){
                for(Attachment att : [SELECT Id,Name,CreatedDate FROM Attachment WHERE ParentId =:p1dId]){
                    listAtts.add(att);
                }
            }
            if(listAtts.size() == 0){
                return null;
            }
            return JSON.serializePretty(listAtts);
          // return count;
            //return listAtts      
     }
    @RemoteAction
    global static String getOtherAttachmentsByTicketId(String pid) {
        list<Attachment> listAtts;
        if(pid!=null && pid!=''){
            listAtts = [SELECT Id,Name FROM Attachment WHERE ParentId =:pid];
        }else{
            return null;
        }
        return JSON.serializePretty(listAtts);
    }
     
     public void getBoilerFormDetails(){
         
         List<APXTConga4__Conga_Template__c> ctemplist = [select id from APXTConga4__Conga_Template__c where APXTConga4__Name__c = 'Boiler Information Package_2017'];
         if(ctemplist.size() > 0){
            CongaTemplateId = ctemplist.get(0).id;
         }
         
         List<APXTConga4__Conga_Email_Template__c> cetemplist = [select id from APXTConga4__Conga_Email_Template__c where APXTConga4__Name__c = 'EBR Rebate Form_2017'];
         if(cetemplist.size() > 0){
            CongaEmailTemplateId = cetemplist.get(0).id;
         }
         
         List<OrgWideEmailAddress> owdlist = [select id from OrgWideEmailAddress where displayName = 'Early Boiler Rebate'];
         if(owdlist.size() > 0){
            orgWideId = owdlist.get(0).id;
         }
     }
     
     public void getFurnaceFormDetails(){
         
         List<APXTConga4__Conga_Template__c> ctemplist = [select id from APXTConga4__Conga_Template__c where APXTConga4__Name__c = 'Furnace Information Package_2017'];
         if(ctemplist.size() > 0){
            CongaTemplateId = ctemplist.get(0).id;
         }
         
         List<APXTConga4__Conga_Email_Template__c> cetemplist = [select id from APXTConga4__Conga_Email_Template__c where APXTConga4__Name__c = 'EFR Rebate Form_2017'];
         if(cetemplist.size() > 0){
            CongaEmailTemplateId = cetemplist.get(0).id;
         }
         
         List<OrgWideEmailAddress> owdlist = [select id from OrgWideEmailAddress where displayName = 'Early Furnace Rebate'];
         if(owdlist.size() > 0){
            orgWideId = owdlist.get(0).id;
         }
     }
     
     @RemoteAction
     global static String  delAttachmentsById(String aid, String tid) {
                
            Attachment__c a = [select id from Attachment__c where id=:aid];
            delete a;
            list<Attachment__c> listAtts;
            Integer count = [Select COUNT() from Attachment__c where  Ticket__c = :tid];
            if(count > 0){
              listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Service_Request__c = :tid order by CreatedDate desc];
            } 
            return JSON.serializePretty(listAtts);
            //return listAtts      
      }
      public List<SelectOption> getTicketTypes()
        {
          List<SelectOption> options = new List<SelectOption>();
                
           Schema.DescribeFieldResult fieldResult =Service_Request__c.Type__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
             if(f.getValue()=='Question' || f.getValue()=='Complaint' || f.getValue()=='Bug' || f.getValue()=='Enhancement')
                  options.add(new SelectOption(f.getLabel(), f.getValue()));
           }       
           return options;
        }
    
    public void saveCallLog(){
        if(newCallLog != null){
            INSERT newCallLog.createTaskObject();
            newCallLog = null;
        }
    }
    public class CallLogModel{
        public Id WhatId{get;set;}
        public String Id{get;set;}
        public String Subject{get;set;}
        public String Description{get;set;}
        public DateTime CreatedDate{get;set;}
        public String CreatedByName{get;set;}
        public List<CallLogModel> callLogs{get;set;}
        public String asJson{get{ return JSON.serialize(new Map<String,Object>{
            'Id'=> this.Id,
                'Subject' => this.Subject,
                    'Description' => this.Description
        });}}
        public CallLogModel(List<Task> callLogs){
            this.callLogs = new List<CallLogModel>();
            for(Task log : callLogs){
                this.callLogs.add(new CallLogModel(log));
            }
        }
        private CallLogModel(Task callLog){
            this.Id = callLog.Id;
            this.Subject = callLog.Subject;
            this.Description = callLog.Description;
            this.CreatedDate = callLog.CreatedDate;
            this.CreatedByName = callLog.CreatedBy.Name;
        }
        public CallLogModel(Id WhatId){
            this.WhatId = WhatId;
        }
        public Task createTaskObject(){
            Energy_Specialist_Portal_Config__c ESPConfig = Energy_Specialist_Portal_Config__c.getInstance();
            List<User> taskOwners = [SELECT Id,Name FROM User WHERE Username =:ESPConfig.Admin_Username__c];
            User taskOwner = (taskOwners.size() == 0) ? new User(Id = UserInfo.getUserId()) : taskOwners.get(0); 
            return new Task(
                WhatId = this.WhatId,
                Subject = this.Subject,
                Description = this.Description,
                TaskSubType = 'Call',
                Status = 'Completed',
                OwnerId = taskOwner.Id
            );
        }
    }
    public void saveNote(){
        if(newNote != null){
            INSERT newNote;
            newNote = null;
        }
    }
    public class NoteModel{
        public String Id{get;set;}
        public String Title{get;set;}
        public String Body{get;set;}
        public DateTime CreatedDate{get;set;}
        public String CreatedByName{get;set;}
        public transient List<NoteModel> notes{get;set;}
        public String asJson{get{return JSON.serialize(new Map<String,String>{
            'Id' => this.Id,
                'Title' => this.Title,
                    'Body' => this.Body,
                        'CreatedDate' => this.CreatedDate.format(),
                            'CreatedByName' => this.CreatedByName
        });}}
        
        public NoteModel(List<Note> notes){
            this.notes = new List<NoteModel>();
            for(Note n : notes){
                this.notes.add(new NoteModel(n));
            }
        }
        private NoteModel(Note n){
            this.Id = n.Id;
            this.Title = n.Title;
            this.Body = n.Body;
            this.CreatedDate = n.CreatedDate;
            this.CreatedByName = n.CreatedBy.Name;
        }
    }
    
    public boolean enablePollar{get;set;}
    public Integer oldCount{get{if(oldCount == null)return 0; return oldCount;}set;}
    public Integer newCount{get{if(newCount == null)return 0; return newCount;}set;}
    public Id congaAttachmentId {get;set;}
    public boolean pollarSuccess{get;set;}
    public void checkCongaStatus(){
        try{
            pollarSuccess = false;
            enablePollar = true;
            List<Attachment> attachmentList = [SELECT Id,Name FROM Attachment WHERE ParentId=:objPTicket.Id ORDER BY CreatedDate DESC];
            if(attachmentList.size() > 0){
                newCount = attachmentList.size();
                if(oldCount == 0){
                    oldCount = newCount;
                }
            }
            
            if(newCount > oldCount){
                pollarSuccess = true;
                enablePollar = false;
                congaAttachmentId = attachmentList.get(0).Id;
                DELETE [SELECT Id,Name FROM Attachment WHERE ParentId=:objPTicket.Id AND Id !=:congaAttachmentId AND Name =:attachmentList.get(0).Name ];
            }
        }catch(Exception ex){
            enablePollar = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
}