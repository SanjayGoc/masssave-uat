public class BatchUpdateTotalCountOnWTM implements Database.batchable<SObject>,Database.Stateful,schedulable{  
    public database.querylocator start(Database.batchableContext batchableContext){
         String sQuery ='select id,Work_Team__c,Total_Booked_Time__c,Total_Blocked_Time__c,Total_Available_Time__c,Total_Unavailable_Time__c,Work_Team__r.Unavailable__c,Work_Team__r.Duration__c from Work_Team_Member__c where Work_Team__c != null';
         System.debug('===sQuery===='+sQuery);
         return Database.getQueryLocator(sQuery);
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> scope){ 
        try{
            Set<Id> wtId = new Set<Id>();
            Work_Team_Member__c wtm = null;
           
            for(sObject o : scope){
                wtm = (Work_Team_Member__c)o;
                wtId.add(wtm.Work_Team__c);
            }
                  
                Map<Id,Integer> totalScheduledTimeMap = new Map<Id,Integer>();
                Map<Id,Integer> totalBlockTimeMap = new Map<Id,Integer>();
                
                AggregateResult[] groupedResults
                  = [SELECT SUM(Duration__c)mySum,Work_Team__c
                      FROM Appointment__c  WHERE (Appointment_Status__c = 'Scheduled' OR Appointment_Status__c = 'Completed') and Work_Team__c in : wtId GROUP BY Work_Team__c];
               
                for (AggregateResult ar : groupedResults)  {
                    System.debug('WorkTeam ID' + ar.get('Work_Team__c'));
                    System.debug('Average amount' + ar.get('mySum'));
                    totalScheduledTimeMap.put(String.valueOf(ar.get('Work_Team__c')),integer.valueOf(ar.get('mySum')));
                }
                
                groupedResults
                  = [SELECT SUM(Duration_Formula__c)mySum,Work_Team__c
                      FROM Appointment__c  WHERE Appointment_Status__c = 'Unavailable' and Work_Team__c in : wtId GROUP BY Work_Team__c];
               
                for (AggregateResult ar : groupedResults)  {
                    System.debug('WorkTeam ID' + ar.get('Work_Team__c'));
                    System.debug('Average amount' + ar.get('mySum'));
                    totalBlockTimeMap.put(String.valueOf(ar.get('Work_Team__c')),integer.valueOf(ar.get('mySum')));
                }
            
            List<Work_Team_Member__c> lstWTM = new List<Work_Team_Member__c>();    
            for(sObject o : scope){
                boolean isUpdate = false;
                wtm = (Work_Team_Member__c)o;
                if(totalScheduledTimeMap.get(wtm.Work_Team__c)!=null){
                    wtm.Total_Booked_Time__c = totalScheduledTimeMap.get(wtm.Work_Team__c);
                    isUpdate = true;
                }
                if(totalBlockTimeMap.get(wtm.Work_Team__c)!=null){
                    wtm.Total_Blocked_Time__c = totalBlockTimeMap.get(wtm.Work_Team__c);
                    isUpdate = true;
                }
                if(wtm.Work_Team__r.Unavailable__c == false){
                    wtm.Total_Available_Time__c = wtm.Work_Team__r.Duration__c;
                    isUpdate = true;
                }else{
                    wtm.Total_Unavailable_Time__c = wtm.Work_Team__r.Duration__c;
                    isUpdate = true;
                }
                
                if(isUpdate)
                    lstWTM.add(wtm);
                
            }
            
            if(lstWTM.size()>0)
                update lstWTM;
            
        }catch(exception e){
            System.debug('#####===e===='+e);
        }
    }
    
    public void finish(Database.BatchableContext batchableContext) {
    
    }
    
    public void execute(SchedulableContext sc) {
        BatchUpdateTotalCountOnWTM obj = new BatchUpdateTotalCountOnWTM();
        database.executebatch(obj,200);
    }
}