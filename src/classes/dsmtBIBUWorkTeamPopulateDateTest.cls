@isTest
public class dsmtBIBUWorkTeamPopulateDateTest {

    public static testmethod void test(){
        
          Location__c loc = new Location__c();
          loc.Name = 'test loc';
          insert loc;
        
          
          Employee__c e = new Employee__c ();
          e.Name = 'emp name';
          e.Location__c = loc.Id;
          String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
          Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
          String hexDigest = EncodingUtil.convertToHex(hash);
          e.Employee_Id__c = hexDigest.substring(0,11);
          insert e;
        
          Schedule_Line_Item__c sl = new Schedule_Line_Item__c();
          sl.Start_Time__c = '08:00 AM';
          sl.End_Time__c ='08:30 PM';
          sl.Lunch_Start_Time__c = '01:30 PM';
          sl.Lunch_End_Time__c = '02:00 PM';
          insert sl;
        
          Schedule_Line_Item__c sl2 = new Schedule_Line_Item__c();
          sl2.Start_Time__c = '08:30 AM';
          sl2.End_Time__c ='08:00 PM';
          sl2.Lunch_Start_Time__c = '01:30 PM';
          sl2.Lunch_End_Time__c = '02:00 PM';
          insert sl2;
         
        
          Work_Team__c wt = new Work_Team__c();
          wt.Captain__c = e.Id;
          wt.Schedule_Line_Item__c = sl.Id;
          wt.Service_Date__c = date.today().addDays(1);
          insert wt;
        
          wt.Service_Date__c = date.today().addDays(3);
          wt.Schedule_Line_Item__c = sl2.Id;
          update wt;
             
    }
    
    public static testmethod void test2(){
        
          Location__c loc = new Location__c();
          loc.Name = 'test loc';
          insert loc;
        
          
          Employee__c e = new Employee__c ();
          e.Name = 'emp name';
          e.Location__c = loc.Id;
          String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
          Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
          String hexDigest = EncodingUtil.convertToHex(hash);
          e.Employee_Id__c = hexDigest.substring(0,11);
          insert e;
        
          Schedule_Line_Item__c sl = new Schedule_Line_Item__c();
          sl.Start_Time__c = '08:30 PM';
          sl.End_Time__c ='08:00 AM';
          sl.Lunch_Start_Time__c = '01:30 AM';
          sl.Lunch_End_Time__c = '02:00 AM';
          insert sl;
        
          Schedule_Line_Item__c sl2 = new Schedule_Line_Item__c();
          sl2.Start_Time__c = '08:00 AM';
          sl2.End_Time__c ='08:30 PM';
          sl2.Lunch_Start_Time__c = '01:30 PM';
          sl2.Lunch_End_Time__c = '02:00 PM';
          insert sl2;
        
          Work_Team__c wt = new Work_Team__c();
          wt.Captain__c = e.Id;
          wt.Schedule_Line_Item__c = sl.Id;
          wt.Service_Date__c = date.today().addDays(1);
          insert wt;
        
          wt.Service_Date__c = date.today().addDays(3);
          wt.Schedule_Line_Item__c = sl2.Id;
          update wt;
             
    }
}