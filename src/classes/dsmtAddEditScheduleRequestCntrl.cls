global class dsmtAddEditScheduleRequestCntrl{
    public boolean editable{get;set;}
    public string srId{get;set;}
    public Service_Request__c objSr{get;set;}
    public string body{get;set;}
    public String ConId{get;set;}
    public Dsmtracker_contact__c dsmtCon{get;set;}
    public Trade_Ally_Account__c ta{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
    
    public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }set;
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }set;
    }
    
    public dsmtAddEditScheduleRequestCntrl(){
    
        isPortal = true;
        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
        List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
            if(lstUser.size()>0){
                if(lstUser[0].IsPortalEnabled){
                    headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                }
                else{
                    headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                    isPortal = false;
                }
            }
             
       srId = ApexPages.currentPage().getParameters().get('id');
        if(srId == null || srId == ''){
            srId = ApexPages.currentPage().getParameters().get('srid');
            if(srId != null || srId != ''){
                editable = false;
            }
        }else{
            editable = true;
        }
        
       
       loadContactAndTA();
       init();
       
    }
    
    public void init(){
       if(srId!=null){
           List<Service_Request__c> lstTickets = [select Id,Name,Subject__c,Sub_Type__c,Type__c,Ticket_Request_Type__c,Status__c,Priority__c,Description__c,RecordTypeId,Work_Order_Text__c,Reason_for_Request__c,Customer_Account_Text__c,Schedule_Request_From_Date_Time__c,Schedule_Request_To_Date_Time__c from Service_Request__c where Id = :srId Limit 1];
           if(lstTickets.size()>0){
               objSr = lstTickets[0];
           }
       }
       
       if(objSr == null){
          Id RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
          
          objSr = new Service_Request__c(); 
          objSr.Type__c = 'Schedule Request';
          objSr.RecordTypeId = RecordTypeId;
          objSr.Dsmtracker_Contact__c = dsmtCon.Id;
          objSr.Dsmtracker_Contact_Email__c = dsmtCon.email__c;
          objSr.Dsmtracker_Contact_Name__c = dsmtCon.Name;

          if(ta != null){
             objSr.Trade_Ally_Account__c = ta.id;
          }
          objSr.Requested_by__c = Userinfo.getUserid();
           editable = true;
       }
    }
    
    public void loadContactAndTA(){
        String usrid = UserInfo.getUserId();
        List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
        if(usrList != null && usrList.size() > 0){
            if(usrList.get(0).ContactId != null){ //for portal user
                conId   = usrList.get(0).ContactId;
                List<DSMTracker_Contact__c> dsmtList = [select id,Trade_Ally_Account__c,Portal_Role__c,email__c,Name from DSMTracker_Contact__c where Contact__c =: conId limit 1];
                if(dsmtList != null && dsmtList.size() > 0){
                    dsmtCon = dsmtList.get(0);
                }
            }else{ // for internal DSMT
                String userid = usrList.get(0).id;
                List<DSMTracker_Contact__c> dsmtList = [select id,Trade_Ally_Account__c,Portal_Role__c,email__c,Name from DSMTracker_Contact__c where Portal_User__c =: userid AND Trade_Ally_Account__c != null limit 1];
                if(dsmtList != null && dsmtList.size() > 0){
                    dsmtCon = dsmtList.get(0);
                }
            }
            
            List<Trade_Ally_Account__c> taList = [select id,Owner.Email from Trade_Ally_Account__c where Id =: dsmtCon.Trade_Ally_Account__c];
            if(taList != null && taList.size() > 0){
                ta = taList.get(0);
            }
        }
    }
    
    public void SaveAsDraft(){
         if(objSr != null){
            objSr.Status__c = 'Draft';
            upsert objsr;
         }
         
         srId = objsr.id;
    }
    
    public void SaveAsSubmit(){
          
          objSr.Status__c = 'Submitted';//'New';
          objSr.Sub_type__c = objSr.Ticket_Request_Type__c;
          
          if(objSr.Id != null){
              String attachmentUrl = '';
              list<Attachment__c> listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,Email_File_Download_URL__c,CreatedDate   from Attachment__c where  Service_Request__c = :objSr.Id order by CreatedDate desc];
              for (Attachment__c attach: listAtts) {
                     String newURL = '';
                     String oldURL =  attach.Email_File_Download_URL__c;
                     String[] questionsplit = oldURL.split('\\?');
                     String[] ampSplit = questionsplit[1].split('&');
                     newURL = questionsplit[0]+'?';
                     system.debug('--newURL--'+newURL);
                     system.debug('--ampSplit'+ampSplit);
                     for(String parameter : ampSplit){
                       String[] equalsplit = parameter.split('=');
                       newURL += equalsplit[0]+'='+EncodingUtil.urlEncode(equalsplit[1],'UTF-8')+'&'; 
                     }
                     newURL = newURL.subString(0,newURL.length() - 1);
                     system.debug('--newURL--'+newURL);
                    attachmentUrl += '<li><a target="_self" href="' +newURL+ '">' + attach.Attachment_Name__c + '</a></li>';
              }
            
              if (attachmentUrl != '') {
                 attachmentUrl = '<br/><br/><b>Download Attachments Here:</b><br/>' + attachmentUrl;
             }
            
             system.debug('--attachmentUrl--'+attachmentUrl );
            
             objSr.Attachments__c = attachmentUrl;
           }  
           upsert objSr;
    }
    
    
    @RemoteAction
    global static String getAttachmentsBysrId(String pid) {
            list<Attachment__c> listAtts;
            if(pid!=null && pid!=''){
                //Integer count = [Select COUNT() from Attachment__c where Attachment_Type__c = 'OfferLetter' and CreatedBy =: uid and Message__c = :pid];
                Integer count = [Select COUNT() from Attachment__c where Service_Request__c = :pid];
                if(count > 0){
                  listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Service_Request__c = :pid order by CreatedDate desc];
                } 
            }else{
                return null;
            }
            return JSON.serializePretty(listAtts);
          // return count;
            //return listAtts      
     }
     @RemoteAction
     global static String  delAttachmentsById(String aid, String tid) {
                
            Attachment__c a = [select id from Attachment__c where id=:aid];
            delete a;
            list<Attachment__c> listAtts;
            Integer count = [Select COUNT() from Attachment__c where  Ticket__c = :tid];
            if(count > 0){
              listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Service_Request__c = :tid order by CreatedDate desc];
            } 
            return JSON.serializePretty(listAtts);
            //return listAtts      
      }
      
}