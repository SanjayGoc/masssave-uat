/*
        Name           : dsmtScheduleBoardControllerTest
        Author         : 
        Date           : 12th May 2017
        Description    : Test class of dsmtScheduleBoardController
*/

@isTest
private class dsmtScheduleBoardControllerTest{
    // test method
    private static testMethod void test(){
        
        //Create Loacation
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        insert loc;

        //Create Location Event 
        Event event = new Event();
        event.Subject = 'Holiday';
        event.StartDateTime = DateTime.now().addDays(-5);
        event.EndDateTime = DateTime.now().addDays(5);
        event.WhatId = loc.Id;
        
        //Create employee
        Employee__c emp = new Employee__c();
        emp.Name = 'emp name';
        emp.Location__c = loc.Id;
        emp.Employee_Id__c = '2345';
        emp.Status__c = 'Active';
        insert emp;
        
        Employee__c emp2 = new Employee__c();
        emp2.Name = 'emp2 name';
        emp2.Location__c = loc.Id;
        emp2.Employee_Id__c = '23452';
        emp2.Status__c = 'Active';
        insert emp2;
        
        //Create work team
        List<Work_Team__c> lstWorkTeam = new List<Work_Team__c>();
        Work_Team__c workTeam = new Work_Team__c();
        workTeam.Name = 'wrokteam';
        workTeam.Location__c = loc.Id;
        workTeam.Service_Date__c = System.today();
        workTeam.Deleted__c = false;
        workTeam.Captain__c = emp.Id;
        lstWorkTeam.add(workTeam);
        
        Work_Team__c workTeam1 = new Work_Team__c();
        workTeam1.Name = 'wrokteam1';
        workTeam1.Location__c = loc.Id;
        workTeam1.Service_Date__c = System.today();
        workTeam1.Deleted__c = true;
        workTeam1.Captain__c = emp.Id;
        lstWorkTeam.add(workTeam1);
        
        insert lstWorkTeam;
        
        //create work team member
        List<Team_Member__c> lstWorkTemMem = new List<Team_Member__c>();
        Team_Member__c workMem = new Team_Member__c();
        workMem.Employee__c = emp.Id;
        workMem.Work_Team__c = workTeam.Id;
        lstWorkTemMem.add(workMem);
        
        Team_Member__c workMem1 = new Team_Member__c();
        workMem1.Employee__c = emp.Id;
        workMem1.Work_Team__c = workTeam.Id;
        workMem1.Team_Role__c = 'Captain';
        lstWorkTemMem.add(workMem1);
        
        Team_Member__c workMem2 = new Team_Member__c();
        workMem2.Employee__c = emp2.Id;
        workMem2.Work_Team__c = workTeam1.Id;
        workMem2.Team_Role__c = 'Captain';
        lstWorkTemMem.add(workMem2);
        
        insert lstWorkTemMem;
        
        //create employee
        Employee__c emp1 = new Employee__c();
        emp1.Name = 'emp name1';
        emp1.Location__c = loc.Id;
        emp1.Work_Teams__c = workTeam.Id;
        emp1.Employee_Id__c = '2345232';
        emp1.Status__c = 'Active';
        insert emp1;
        
        Account acc = Datagenerator.createAccount();
        insert acc;
        
        Premise__c premise = Datagenerator.createPremise();
        insert premise;
        
        Customer__c customer = Datagenerator.createCustomer(acc.Id, premise.Id);
        customer.Location__c = loc.Id;
        insert customer;
        
        Time_Slots__c ts = new Time_Slots__c();
        ts.Work_Team__c = workTeam.Id;
        insert ts;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Work_Team__c = workTeam.Id;
        wo.Third_Visit__c = system.today();
        wo.Requested_Date__c = system.today().addDays(2);
        wo.Scheduled_Start_Date__c = system.today();
        wo.Scheduled_End_Date__c = system.today().addDays(2);
        wo.Requested_Start_Date__c = system.today();
        wo.Requested_End_Date__c = system.today().addDays(2);
        wo.Customer__c = customer.Id;
        wo.Address__c = 'test';
        wo.City__c = 'city';
        wo.State__c = 'state';
        wo.Zipcode__c = '12234';
        wo.Time_Slot__c = ts.Id;
        wo.Duration__c = 90;
        wo.Status__c = 'Scheduled';
        insert wo;
        
        dsmtScheduleBoardController scheduleBoard = new dsmtScheduleBoardController();
        
        //Calls Static Methods that Return All Location List
        List<Location__c> locations = dsmtScheduleBoardController.getLocations();
        System.AssertEquals(locations.get(0).Name,'test loc');
        
        set<string> teamIdset = new set<string>();
        teamIdset.add(workteam.Id);
        dsmtScheduleBoardController.getTeamWorkOrdersMap(system.today().month()+'/'+system.today().day()+'/'+system.today().year(),
                                                         teamIdset,
                                                         loc.Id);
        
        //Static Methos that return all Teams for that location for perticular Service date
        dsmtScheduleBoardController.getDeletedWOTeamsByServiceDate(loc.Id,null);
        List<Work_Team__c> todayServiceDateTeams = dsmtScheduleBoardController.getDeletedWOTeamsByServiceDate(loc.Id,System.today().month()+'/'+System.today().day()+'/'+System.today().year());
        //System.AssertEquals(todayServiceDateTeams.size(),2);
        
        //Static Method that return all Employees for that Location
        List<Employee__c> allEmps = dsmtScheduleBoardController.allEmploysByLocation(loc.Id);
        //System.AssertEquals(allEmps.size(), 3);
        
        dsmtScheduleBoardController.getWeeks(null,loc.Id);
        
        //Static Method that Update update WorkTeamMember's Team 
        dsmtScheduleBoardController.updateWorkTeamMembersTeam(workTeam.Id,workMem1.Id);
        dsmtScheduleBoardController.updateWorkTeamMembersTeam(workTeam.Id,emp.Id);
        
        //Static Method that Delete Work Order Team Members  
        dsmtScheduleBoardController.removeWorkOrdersTeamMembers(WorkMem2.Id);
        
        dsmtScheduleBoardController.removeTeams(workTeam.Id,'');
        
        scheduleBoard.chartlocation = loc.Id;
        scheduleBoard.getChartData();
        
        set<string> teamIds = new set<string>();
        teamIds.add(workTeam.Id);
        dsmtScheduleBoardController.getWorkTeamMenbersMap(teamIds);
        dsmtScheduleBoardController.assignWorkOrder(workTeam.Id, wo.Id);
        dsmtScheduleBoardController.getNotes(wo.Id, null);
        dsmtScheduleBoardController.updateWorkOrdersTeam(workTeam.Id, wo.Id);
        dsmtScheduleBoardController.updateWorkOrdersTeam('accordion', wo.Id);
        dsmtScheduleBoardController.createNewTeamWithCaptain(lstWorkTemMem[0].Id, loc.Id, 'column1', system.today().month()+'/'+system.today().day()+'/'+system.today().year(), 2);
        dsmtScheduleBoardController.createNewTeamWithCaptain(emp1.Id, loc.Id, 'column1', system.today().month()+'/'+system.today().day()+'/'+system.today().year(), 2);
        dsmtScheduleBoardController.removeWorkOrdersTeam(null, null);
        dsmtScheduleBoardController.unassignCustomerWorkOrders(wo.Id);
        dsmtScheduleBoardController.getCustomerInfo(wo.Id);
        scheduleBoard.getReasons();
        
        Test.startTest();
            scheduleBoard.getStatusList();
            scheduleBoard.getReasonsCancel();
            dsmtScheduleBoardController.reassgnWorkorders(workTeam.Id, workTeam.Id);
            scheduleBoard.reassignWorkorders();
            dsmtScheduleBoardController.getWorkorderDetails(wo.Id);
            dsmtScheduleBoardController.addNotes(wo.Id, 'test', system.today().month()+'/'+system.today().day()+'/'+system.today().year(), 'wo');
            dsmtScheduleBoardController.getAllNotes(wo.Id, true);
            dsmtScheduleBoardController.getAllNotes(wo.Id, false);
            dsmtScheduleBoardController.getAllNoteDetails(wo.Id, true);
            dsmtScheduleBoardController.getAllNoteDetails(wo.Id, false);
            dsmtScheduleBoardController.getEventEmpTeams(loc.Id, system.today().month()+'/'+system.today().day()+'/'+system.today().year());
            dsmtScheduleBoardController.getCurrentEvents(loc.Id, system.today().month()+'/'+system.today().day()+'/'+system.today().year());
            dsmtScheduleBoardController.updateVisitOrder(wo.Id, 1);
            dsmtScheduleBoardController.updateTeamPlace(workTeam.Id+'=1=1');
            
            try{
                dsmtScheduleBoardController.rescheduleWorder(wo.Id, 'te', system.today().month()+'/'+system.today().day()+'/'+system.today().year());
            }catch(Exception ex){}
            dsmtScheduleBoardController.cancelRescheduleWorder(wo.Id, 'te', system.today().month()+'/'+system.today().day()+'/'+system.today().year(), '2.3');
            
            dsmtScheduleBoardController.TeamWorkOrderModal tm = new dsmtScheduleBoardController.TeamWorkOrderModal();
            dsmtScheduleBoardController.InventoryModal im = new dsmtScheduleBoardController.InventoryModal('df', 1);
            dsmtScheduleBoardController.EventsModel em = new dsmtScheduleBoardController.EventsModel();
            dsmtScheduleBoardController.WorkOrderNotesModel wem = new dsmtScheduleBoardController.WorkOrderNotesModel();
            
            set<string> empIdSet = new set<string>();
            empIdSet.add(emp.Id);
            dsmtScheduleBoardController.unAssignedEmploysTerritory(loc.Id, system.today().month()+'/'+system.today().day()+'/'+system.today().year(), empIdSet);
        Test.stopTest();
    }
    
    
    //=======================================================================================================
    private static testMethod void test2(){
        //Create Loacation
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        insert loc;
        
        //Create employee
        Employee__c emp = new Employee__c();
        emp.Name = 'emp name';
        emp.Location__c = loc.Id;
        emp.Employee_Id__c = '234523';
        emp.Status__c = 'Active';
        insert emp;
        
        //Create Employee Event 
        Event event = new Event();
        event.Subject = 'Holiday';
        event.StartDateTime = DateTime.now().addDays(-5);
        event.EndDateTime = DateTime.now().addDays(5);
        event.WhatId = emp.Id;
        
        insert event;
        
        //Create work team
        List<Work_Team__c> lstWorkTeam = new List<Work_Team__c>();
        Work_Team__c workTeam = new Work_Team__c();
        workTeam.Name = 'wrokteam';
        workTeam.Location__c = loc.Id;
        workTeam.Service_Date__c = System.today();
        workTeam.Deleted__c = false;
        workTeam.Captain__c = emp.Id;
        lstWorkTeam.add(workTeam);
        
        Work_Team__c workTeam1 = new Work_Team__c();
        workTeam1.Name = 'wrokteam';
        workTeam1.Location__c = loc.Id;
        workTeam1.Service_Date__c = System.today();
        workTeam1.Deleted__c = true;
        workTeam1.Captain__c = emp.Id;
        lstWorkTeam.add(workTeam1);
        
        insert lstWorkTeam;
        
        //create work team member
        List<Team_Member__c> lstWorkTemMem = new List<Team_Member__c>();
        Team_Member__c workMem = new Team_Member__c();
        workMem.Employee__c = emp.Id;
        workMem.Work_Team__c = workTeam.Id;
        lstWorkTemMem.add(workMem);
        
        Team_Member__c workMem1 = new Team_Member__c();
        workMem1.Employee__c = emp.Id;
        workMem1.Work_Team__c = workTeam.Id;
        lstWorkTemMem.add(workMem1);
        insert lstWorkTemMem;
        
        //create employee
        Employee__c emp1 = new Employee__c();
        emp1.Name = 'emp name1';
        emp1.Location__c = loc.Id;
        emp1.Employee_Id__c = '2345221';
        emp1.Status__c = 'Active';
        insert emp1;
        
        dsmtScheduleBoardController scheduleBoard = new dsmtScheduleBoardController();
        
        dsmtScheduleBoardController.changeWTMRole(workMem1.Id,'Captain');
        scheduleBoard.getTeamRoles(); 
        
        //Filling Team WorkOrder Model
        scheduleBoard.SelectedLocation = loc.Id;
        String activedate = String.valueOf(Date.Today());
        List<String> splitedDate = activeDate.split('-');
        String newDate = splitedDate.get(1)+'/'+splitedDate.get(2)+'/'+splitedDate.get(0);
        scheduleBoard.SelectedDate = newDate;
        //scheduleBoard.getTeamWorkOrderModal();
        
        String activedate1 = String.valueOf(Date.Today().addDays(-1));
        List<String> splitedDate1 = activeDate1.split('-');
        String newDate1 = splitedDate1.get(1)+'/'+splitedDate1.get(2)+'/'+splitedDate1.get(0);
        scheduleBoard.SelectedDate = newDate1;
        scheduleBoard.prevWorkOrders();
        
        String activedate2 = String.valueOf(Date.Today().addDays(1));
        List<String> splitedDate2 = activeDate2.split('-');
        String newDate2 = splitedDate2.get(1)+'/'+splitedDate2.get(2)+'/'+splitedDate2.get(0);
        scheduleBoard.SelectedDate = newDate2;
        scheduleBoard.nextWorkOrders();
        
        newDate2 = '2/'+splitedDate2.get(2)+'/'+splitedDate2.get(0);
        scheduleBoard.SelectedDate = newDate2;
        scheduleBoard.nextWorkOrders();
        
        dsmtScheduleBoardController.Data d = new dsmtScheduleBoardController.Data('', 6);
    }
}