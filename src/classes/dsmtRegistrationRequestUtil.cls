global class dsmtRegistrationRequestUtil{
    
    public static Set<String> allowedRolesForAccountManager = new Set<String>{
        'DSMTracker_IT_Administrator','DSMTracker_Trade_Ally_Manager','DSMTracker_Account_Manager_TA'
    };

    webservice static String approveRequest(String rrid) { 
        system.debug('--rrid--'+rrid);
        
        List<Registration_Request__c> RRlist = Database.query(dsmtFuture.getCreatableFieldsSOQL('Registration_Request__c','Id =: rrid',''));   
        if(RRlist.size() > 0){
            Registration_Request__c rr = RRlist.get(0);
            
            if(rr.Status__c == 'Approved'){
              return 'This registration request is already in approved status.';
            }
            
            List<Exception__c> explist = [select id from Exception__c where is_Open__c = 1 and Soft_Exception__c = false and Registration_request__c =: rrid];
            if(explist.size() > 0){
               return 'Registration Request can not be Approve as it\'s one or more Exception are not Resolved.';
            }
            
            List<Attachment__c> attachlist = [select id from Attachment__c where status__c != 'Approved' and DSMTracker_Contact__c = null and Registration_Request__c =: rrid];    
            if(attachlist.size() > 0){
               return 'Registration Request can not be Approve as it\'s one or more Attachments are not Approved.';
            }
            
            String error = '';
            boolean crewChiefRequired = rr.Trade_Ally_Type__c == 'HPC' || rr.Trade_Ally_Type__c == 'IIC';
            boolean buildingEnvelopRequired = rr.Trade_Ally_Type__c == 'HPC';
            Integer crewChiefCount = 0, buildingEnvelopCount = 0;
            boolean isCSL = false;
            
            List<Dsmtracker_contact__c> dsmtlist = [select id,Title_Formula__c,BPI_Building_Analyst_Attached__c,BPI_Envelope_Attached__c,CSL_Attached__c,(select id,Request_Date_Of_Hire__c from Attachments__r) from Dsmtracker_Contact__c where status__c = 'Approved' and Registration_Request__c =: rr.id];
            for(Dsmtracker_Contact__c dsmt : dsmtlist){
               if(rr.Trade_Ally_Type__c == 'HPC' || rr.Trade_Ally_Type__c == 'IIC'){
                 if(dsmt.Title_Formula__c == 'Crew Chief'){
                     crewChiefCount++;
                 }
               }
               
               if(rr.Trade_Ally_Type__c == 'HPC'){
                  if(dsmt.BPI_Envelope_Attached__c == true){
                     buildingEnvelopCount++;
                  }
                  if(dsmt.CSL_Attached__c){
                     isCSL = true;
                  }
               }else{
                  isCSL = true;
               }
            }
            
             if(crewChiefRequired == true && crewChiefCount == 0){
                 error += 'Atleast one crew chief is required.\n';
             }
             
             if(!isCSL){
                error += 'Atleast one CSL document is required.\n';
             }
             
             if(buildingEnvelopRequired == true && buildingEnvelopCount == 0){//if(!isbuildingEnvelop){
                error += 'Atleast one Building Envelope document is required.\n';
             }
             
             system.debug('--error--'+error);
             if(error.trim().length() > 0){
                return error;
             }
            
             rr.Status__c = 'Approved';
             update rr;
            
            String taid = rr.Trade_ally_account__c;
            List<Trade_ally_account__c> TAlist = Database.query(dsmtFuture.getCreatableFieldsSOQL('Trade_ally_account__c','Id =: taid',''));  
            if(TAlist.size() > 0){
               Trade_ally_account__c TA = TAlist.get(0);
               TA.Street_Address__c = rr.Street_Address__c;
               TA.Street_City__c = rr.Street_City__c;
               TA.Street_State__c = rr.Street_State__c;
               TA.Street_Zip__c = rr.Street_Zip__c;
               
               TA.Mailing_Address__c = rr.Mailing_Address__c;
               TA.Mailing_City__c = rr.Mailing_City__c;
               TA.Mailing_State__c = rr.Mailing_State__c;
               TA.Mailing_Zip__c = rr.Mailing_Zip__c;
               
               TA.Operational_First_Name__c = rr.Operational_First_Name__c;
               TA.Operational_Last_Name__c = rr.Operational_Last_Name__c;
               TA.Operational_Phone__c = rr.Operational_Phone__c;
               TA.Operational_Email__c = rr.Operational_Email__c;
               
               TA.Legal_First_Name__c = rr.Legal_First_Name__c;
               TA.Legal_Last_Name__c = rr.Legal_Last_Name__c;
               TA.Legal_Phone__c = rr.Legal_Phone__c;
               TA.Legal_Email__c = rr.Legal_Email__c;
               
               TA.Online_Profile_Company_Name__c = rr.Online_Profile_Company_Name__c;
               TA.Online_Profile_Company_Website__c = rr.Online_Profile_Company_Website__c;
               TA.Online_Profile_City__c = rr.Online_Profile_City__c;
               
               TA.Counties__c = rr.Counties__c;
               TA.Sub_Counties__c = rr.Sub_Counties__c;
               TA.Program_Sponsors__c = rr.Program_Sponsors__c;
               TA.Proficiences__c = rr.Proficiences__c;
               
               update TA;
               
               attachlist = [select id from Attachment__c where Registration_Request__c =: rrid and status__c = 'Approved'];    
               
               for(Attachment__c attach : attachlist){
                   attach.Trade_Ally_Account__c = TA.id;
                }
                
                update attachlist;
            }
            
            
        }else{
           return 'Registration Request is not found..';
        }
        
        
        return null;
    }
    
    webservice static String approveDSMTContact(String[] contactids) { 
        system.debug('--contactids--'+contactids);
        
        Set<String> dsmtNamelist = new Set<String>();
        
        List<DSMTracker_Contact__c> dsmtlist = [select id,Name,Status__c,Account_Manager_Id__c from DSMTracker_Contact__c where id in: contactids];
        for(DSMTracker_Contact__c dsmt : dsmtlist){
            if(dsmt.Account_Manager_Id__c != null && !Userinfo.getuserid().contains(dsmt.Account_Manager_Id__c)){
                dsmtNamelist.add(dsmt.Name);
            }
        }
        
        UserRole userRole = [SELECT Name,DeveloperName FROM UserRole WHERE Id =:UserInfo.getUserRoleId() LIMIT 1];
        if(dsmtNamelist.size() > 0 && userRole != null && !allowedRolesForAccountManager.contains(userRole.DeveloperName)){
           return 'You are not account manager of this trade ally account so you can not approve this Dsmtracker Contacts.';
        }
        
        
        List<Attachment__c> attachlist = [select id,DSMTracker_Contact__r.Name from Attachment__c where status__c != 'Approved' and DSMTracker_Contact__c in: contactids];    
        if(attachlist.size() > 0){
           for(Attachment__c attach : attachlist){
               dsmtNamelist.add(attach.DSMTracker_Contact__r.Name);
           }
           
           return 'DSMTracker contacts '+dsmtNamelist+' can not be Approve as it\'s one or more Attachments are not Approved.';
        }
        
        
        for(DSMTracker_Contact__c dsmt : dsmtlist){
           dsmt.status__c = 'Approved';
        }
        
        update dsmtlist;
        
        return null;
    }   
    
    webservice static String RejectDSMTContact(String[] contactids) { 
        system.debug('--contactids--'+contactids);
        
        Set<String> dsmtNamelist = new Set<String>();
        
        List<DSMTracker_Contact__c> dsmtlist = [select id,Name,Status__c,Account_Manager_Id__c from DSMTracker_Contact__c where id in: contactids];
        for(DSMTracker_Contact__c dsmt : dsmtlist){
            if(dsmt.Account_Manager_Id__c != null && !Userinfo.getuserid().contains(dsmt.Account_Manager_Id__c)){
                dsmtNamelist.add(dsmt.Name);
            }
        }
        
        UserRole userRole = [SELECT Name,DeveloperName FROM UserRole WHERE Id =:UserInfo.getUserRoleId() LIMIT 1];
        if(dsmtNamelist.size() > 0 && userRole != null && !allowedRolesForAccountManager.contains(userRole.DeveloperName)){       
           return 'You are not account manager of this trade ally account so you can not Reject this Dsmtracker Contacts.';
        }
        
        for(DSMTracker_Contact__c dsmt : dsmtlist){
           dsmt.status__c = 'Rejected';
        }
        
        update dsmtlist;
        
        List<Attachment__c> attachlist = [select id,DSMTracker_Contact__r.Name,Status__c from Attachment__c where DSMTracker_Contact__c in: contactids];    
        if(attachlist.size() > 0){
            for(Attachment__c attach : attachlist){
               attach.Status__c = 'Rejected';
            }
            
            update attachlist;
        }
        
        
        
        return null;
    }   
    
    
     webservice static String approveAttachments(String[] attachmentids) { 
        system.debug('--attachmentids--'+attachmentids);
        
        boolean notexist = false;
        boolean invalid = false;
        
        List<Attachment__c> attachlist = [select id,Name,Status__c,Account_Manager_Id__c,Registration_Request__c from Attachment__c where id in: attachmentids];
        for(Attachment__c attach: attachlist){
            system.debug('--attach.Account_Manager_Id__c --'+attach.Account_Manager_Id__c );
            system.debug('--Userinfo.getuserid()--'+Userinfo.getuserid());
            if(attach.Registration_Request__c  != null && attach.Account_Manager_Id__c == null){
               notexist = true;
            }
            if(attach.Account_Manager_Id__c != null && !Userinfo.getuserid().contains(attach.Account_Manager_Id__c)){
               invalid = true;
            }
        }
        
         
        if(notexist){
           return 'Account Manager is not found for this Request so you can not approve this Attachments.';
        }
        system.debug('--invalid --'+invalid );
         
        UserRole userRole = [SELECT Name,DeveloperName FROM UserRole WHERE Id =:UserInfo.getUserRoleId() LIMIT 1];
        if(invalid && userRole != null && !allowedRolesForAccountManager.contains(userRole.DeveloperName)){
           return 'You are not account manager of this attachment\'s Request so you can not approve this Attachments.';
        }
        
        
        for(Attachment__c attach: attachlist){
           attach.status__c = 'Approved';
        }
        
        update attachlist;
        
        return null;
    }  
    
    
    
     webservice static String rejectAttachments(String[] attachmentids) { 
        system.debug('--attachmentids--'+attachmentids);
        
        boolean notexist = false;
        boolean invalid = false;
        
        List<Attachment__c> attachlist = [select id,Name,Status__c,Account_Manager_Id__c,Registration_Request__c,Registration_Request__r.Dsmtracker_Contact__r.Contact__c,Registration_Request__r.Email__c from Attachment__c where id in: attachmentids];
        for(Attachment__c attach: attachlist){
            if(attach.Registration_Request__c  != null && attach.Account_Manager_Id__c == null){
               notexist = true;
            }
            if(attach.Account_Manager_Id__c != null && !Userinfo.getuserid().contains(attach.Account_Manager_Id__c)){
               invalid = true;
            }
        }
        
        if(notexist){
           return 'Account Manager is not found for this Request so you can not reject this Attachments.';
        }
        UserRole userRole = [SELECT Name,DeveloperName FROM UserRole WHERE Id =:UserInfo.getUserRoleId() LIMIT 1];
        if(invalid && userRole != null && !allowedRolesForAccountManager.contains(userRole.DeveloperName)){
           return 'You are not account manager of this attachment\'s Request so you can not reject this Attachments.';
        }
        
        List<String> toEmailList = new List<String>();
        String objType = 'Attachment__c';
        String emailTemplate = Email_Custom_Setting__c.getorgDefaults().Notification_to_TA_Owner_of_Rejected_Att__c;
         system.debug('--emailTemplate--'+emailTemplate);
        for(Attachment__c attach: attachlist){
           attach.status__c = 'Rejected';
           
           String fromaddress = attach.Owner.Email;
           
           String targetObjectId = attach.Registration_Request__r.Dsmtracker_Contact__r.Contact__c;
            toEmailList.add(attach.Registration_Request__r.Email__c);

            sendEmail(targetObjectId,attach,objType ,toEmailList,new List<String>(),new List<String>(),emailTemplate,null,fromaddress,'Outbound');
            
        
        }
        
        update attachlist;



        

        return null;
    }   
    
     public static void sendEmail(Id TargetObjectId,Sobject obj,String ObjType ,List<String> toAddressList,List<String> ccAddressList, List<String> bccAddressList,String templateUniqueName,OrgWideEmailAddress owe,String fromEmailAddress,String msgdirection){
        
        List<EmailTemplate> emailTemplatelist = [SELECT Id FROM EmailTemplate WHERE DeveloperName =:templateUniqueName LIMIT 1];
        ID conID =[SELECT ID FROM Contact WHERE Name=:'Do Not Delete'].Id; // added by Puneet for DSST-2793, need to contact developer 
        EmailTemplate emailTemplate = null;
        if(emailTemplatelist.size() > 0){
            emailTemplate = emailTemplatelist.get(0);
        }
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<Messaging.SingleEmailMessage> emaillist = new List<Messaging.SingleEmailMessage>();
        
        if(TargetObjectId != null)
            email.setTargetObjectId(TargetObjectId);
        
        
        if(emailTemplate != null){
           email.setTemplateId(emailTemplate.Id);
        }    
        if (toAddressList != null && toAddressList.size() > 0)
            email.setToAddresses(toAddressList);
            
        if (ccAddressList!= null && ccAddressList.size() > 0)
            email.setCcAddresses(ccAddressList);
        if (bccAddressList!= null && bccAddressList.size() > 0)
            email.setBCcAddresses(bccAddressList);
       
        email.setSaveAsActivity(false);
        email.setWhatId(obj.id);
        
        if(owe != null){
           email.setOrgWideEmailAddressId(owe.id);
        }
        
        email.setSenderDisplayName(fromEmailAddress);
        email.setTreatTargetObjectAsRecipient(false);
        
        emaillist.add(email);
        if(!Test.isRunningTest()){
           List<Messaging.SendEmailResult> sendEmailResults = Messaging.sendEmail(emaillist);
        }
        
        
        List<Task> tasklist = new List<Task>();
        List<Messages__c> msglist = new List<Messages__c>();
        
        String ownerid = null;
        if(ObjType == 'Dsmtracker_Contact__c'){
           ownerid = ((Dsmtracker_contact__c)obj).OwnerId;
        }
        
        for(Messaging.SingleEmailMessage mail : emaillist){
             
             if(ownerid != null){
                 //tasklist.add(createTask(mail.getSubject(),email.getHtmlBody(),obj.id,ownerid));
             }
             String toaddress = (mail.toAddresses != null)?String.join(email.toAddresses,','):'';
             msglist.add(createMessage(mail.getSubject(),fromEmailAddress,toaddress,mail.getHtmlBody(),obj, ObjType ,msgdirection));
        }
        
        if(tasklist.size() > 0){
           insert tasklist;
        }
        
        if(msglist.size() > 0){
           insert msglist;
        }
    } 
    
    
    public static Task createTask(String subject, String description, String whatid,String ownerid){
        Task task = new Task();
        task.Subject = 'Email: ' + subject;
        task.Description = convertHtmlToString(description);
        task.OwnerId = ownerId;
        task.WhatId = whatid;
        task.Status = 'Completed';
        task.ActivityDate = System.today();
        return task;
    }
    
    public static Messages__c createMessage(String subject, String fromEmailAddress,String toEmailAddresses,String body,Sobject obj, String objType,String direction){
        Messages__c msg = new Messages__c();
        msg.From__c = fromEmailAddress;
        msg.To__c = toEmailAddresses;
        
        msg.Subject__c = subject;
        msg.Message__c = body;
        msg.Mesage_Text_Only__c  = convertHtmlToString(body);
        msg.Message_Type__c  = 'Send Email';
        msg.Message_Direction__c  = direction;
        msg.Status__c = 'Sent';
        msg.Sent__c = Datetime.Now();
        if(objType== 'Registration_Request__c'){
            Registration_Request__c RR = (Registration_Request__c)obj;
            msg.Registration_Request__c  = RR.Id;
            if(RR.Dsmtracker_contact__c != null){
                msg.DSMTracker_Contact__c = RR.Dsmtracker_contact__c;
            }
            if(RR.Trade_Ally_Account__c !=null)
                msg.Trade_Ally_Account__c = RR.Trade_Ally_Account__c;
        }else if(objType == 'DSMTracker_Contact__c'){
            Dsmtracker_Contact__c dsmt = (Dsmtracker_contact__c)obj;
            msg.DSMTracker_Contact__c = dsmt.id;
            if(dsmt.Trade_Ally_Account__c !=null)
                msg.Trade_Ally_Account__c = dsmt.Trade_Ally_Account__c;
        }else if (objType == 'Service_Request__c'){
           Service_Request__c sr = (Service_Request__c)obj;
           msg.Service_Request__c = sr.id;
           if(sr.Dsmtracker_Contact__c != null){
              msg.Dsmtracker_contact__c = sr.Dsmtracker_Contact__c;
           }
           if(sr.Trade_Ally_Account__c != null){
             msg.Trade_ally_Account__c = sr.Trade_Ally_Account__c;
           }
        }
        
        return msg;
    }
    
    private static String convertHtmlToString(String htmlString){
        if(htmlString == null)
            htmlString = '';
        
        return htmlString.replaceAll('&nbsp;', '').replaceAll(';','').replaceAll('(</{0,1}[^>]+>)','');
    }
    
    webservice static String createPortalAccess(String employeeid) { 
        system.debug('--employee id --'+employeeid);
        
        List<Employee__c> emplist = [select id,First_Name__c,Last_Name__c,Address__c,city__c,State__c,Zip__c,Email__c,Home_Phone__c,Mobile_Phone__c from Employee__c where id =: employeeid];
        if(emplist.size() > 0){
           Employee__c emp = emplist.get(0);
           
           List<Trade_Ally_Account__c> talist = [select id,Account__c,name from Trade_Ally_Account__c where Internal_Account__c = true and Stage__c = 'Active'];
           if(talist.size() > 0){
              Trade_Ally_Account__c ta = talist.get(0);
              
              List<Dsmtracker_Contact__c> dsmtlist = [select id,Status__c from dsmtracker_Contact__c where Trade_Ally_Account__c =: ta.id and Email__c =: emp.Email__c];
              
              Dsmtracker_Contact__c dsmt = null;
              
              if(dsmtlist.size() > 0){
                  dsmt = dsmtlist.get(0);
              }else{
                  dsmt = new Dsmtracker_Contact__c();
                  String name = '';
                  if(emp.first_Name__c != null){
                     name += emp.First_Name__c+' ';
                  }
                  if(emp.last_Name__c != null){
                     name += emp.last_name__c;
                  } 
                  dsmt.Name = name;
                  dsmt.First_Name__c = emp.first_Name__c;
                  dsmt.Last_Name__c = emp.Last_Name__c;
                  dsmt.Phone__c = emp.Home_Phone__c;
                  dsmt.Mobile__c = emp.Mobile_Phone__c;
                  dsmt.Email__c = emp.email__c;
                  dsmt.Address__c = emp.Address__c;
                  dsmt.City__c = emp.City__c;
                  dsmt.State__c = emp.State__c;
                  dsmt.Zip__c = emp.zip__c;
                  dsmt.Title__c = 'Energy Specialist (all)';
                  
                  dsmt.Trade_ally_Account__c = ta.id;
                  dsmt.Account__c = ta.Account__c;
                  dsmt.Company_Name__c = ta.Name;
                  
                  
              }   
              
              dsmt.Status__c = 'Approved';   
              upsert dsmt;
              
              emp.dsmtracker_contact__c = dsmt.id;
              
              update emp;
              
              List<Appointment__c> appointmentlist = [select id,dsmtracker_contact__c,Trade_Ally_Account__c from Appointment__c where dsmtracker_Contact__c = null and Employee__c =: emp.id];
              if(appointmentlist.size() > 0){
                  for(Appointment__c appointment : appointmentlist){
                     appointment.Dsmtracker_Contact__c = dsmt.id;
                     appointment.Trade_Ally_Account__c = ta.id;
                  }
                  
                  update appointmentlist;
              }
              
           }else{
              return 'There is no active internal Trade Ally Account found in system.';
           }
        }
        return null;
    }
    
    webservice static String SendPortalRegistrationEmail(String TAid) {
    try{
       List<DSMTracker_Contact__c> dsmtlist = [select id,Name,Email__c,Portal_User__c,Contact__c,Trade_Ally_Account__c,Owner.Email from Dsmtracker_Contact__c where /*Status__c = 'Approved' and */ Portal_User__c = null and Trade_Ally_Account__c =: TAid and Email__c != null and Portal_Role__c  != null];
       if(dsmtlist.size() > 0){
           // Send registration mail to ES Dsmtracker contact for internal account
            String dsmtTemplate = Email_Custom_Setting__c.getOrgDefaults().DSMT_ES_Registration_Email_Template__c;
          
          for(Dsmtracker_Contact__c dsmt : dsmtlist){
              system.debug('--dsmt--'+dsmt);
              List<String> toaddressList = new List<String>();
              toaddressList.add(dsmt.Email__c);
                
              // Send notification to DSMT Contact for Registration to portal
             dsmtRegistrationRequestUtil.sendEmail(dsmt.Contact__c,dsmt,'Dsmtracker_Contact__c',toaddressList,new list < string > (),new list < string > (),dsmtTemplate,null,dsmt.Owner.Email,'Outbound');
          }
          
       }else{
          return 'There are no contact is available for registration.!!';
       }
       return 'Successfully Send Portal Registration Email...';
       }catch(Exception e){
           System.debug('--Exception--'+e);
           return e.getMessage();
       }
    }
   
}