public class dsmtTradeAllyProfileUpdateCont{
    
    public static String REVIEW_TYPE = 'Document';
    
    public String TAId{get;set;}
    public String DsmtConId{get;set;}
    public String RevId{get;set;}
    public Trade_Ally_Account__c taaccount{get;set;}
    public Dsmtracker_contact__c dsmtContact{get;set;}
    public Review__c review{get;set;}
    public List<Attachment__c> attachlist{get;set;}
    public String attachId{get;set;}
    public String attachType{get;set;}
    public String attachLic{get;set;}
    public String attachExpdate{get;set;}
    public boolean isSubmitReviewexist{get;set;}
    public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    
    public dsmtTradeAllyProfileUpdateCont(){
        
        TAId = ApexPages.currentPage().getParameters().get('taid');
        DsmtConId = ApexPages.currentPage().getParameters().get('dcid');
        RevId = ApexPages.currentPage().getParameters().get('id');
        
        init();
    }
    
    public void init(){
    
        isSubmitReviewexist =false;
        loadTradeallyAccount();
        
        loadDsmtrackerContact();
        loadReviewData();
        
        
    }
    
    public void loadTradeallyAccount(){
        if(TAId != null){
            String query = dsmtFuture.getCreatableFieldsSOQL('Trade_Ally_Account__c','id=:TAId','');
            List<Trade_Ally_Account__c> talist = Database.query(query);
            if(talist.size() > 0){
                taaccount = talist.get(0);
            }
            List<Review__c > reviewlist = [select id,name,status__c from Review__c where Trade_Ally_Account__c =: taaccount.Id and Dsmtracker_contact__c = null and Status__c = 'Submitted For Review' and Review_Type__c =:REVIEW_TYPE and Requested_By__c =: Userinfo.getuserid()];
            if(reviewlist.size() > 0){
                isSubmitReviewexist = true;
            }
          
        }
    }
    
    public void loadReviewData(){
        if(RevId != null){
            String query = dsmtFuture.getCreatableFieldsSOQL('Review__c','id=:RevId','');
            List<Review__c> rlist = Database.query(query);
            if(rlist.size() > 0){
                review = rlist.get(0);
            }else{
                review = new Review__c();
            }
            
            String whereclause = 'Review__c=\''+RevId+'\'';
            query = dsmtFuture.getCreatableFieldsSOQL('Attachment__c',whereclause,'');
            attachlist = database.query(query);
        }
    }
    
    
    public void loadDsmtrackerContact(){
        if(DsmtConId != null){
            String query = dsmtFuture.getCreatableFieldsSOQL('Dsmtracker_contact__c','id=:DsmtConId','');
            List<Dsmtracker_contact__c > dclist = Database.query(query);
            if(dclist.size() > 0){
                dsmtContact = dclist.get(0);
            }
            List<Review__c > reviewlist = [select id,name,status__c from Review__c where Dsmtracker_contact__c =: dsmtContact.Id and Status__c = 'Submitted For Review' and Review_Type__c =:REVIEW_TYPE and Requested_By__c =: Userinfo.getuserid()];
            if(reviewlist.size() > 0){
                isSubmitReviewexist = true;
            }
        }
    }
    
    public PageReference checkReview(){
        
        if (!apexpages.currentPage().getParameters().containsKey('id') && isSubmitReviewexist == false) {
            String reviewId = null;
            system.debug('--taaccount --'+taaccount );
            system.debug('--dsmtContact --'+dsmtContact );
            
            List<Review__c> reviewlist;
            if(taaccount != null){
                reviewlist = [select id,name,status__c from Review__c where Trade_Ally_Account__c =: taaccount.Id and Dsmtracker_contact__c = null and Status__c = 'Draft' and Review_Type__c =:REVIEW_TYPE and Requested_By__c =: Userinfo.getuserid()];
                system.debug('--reviewlist--'+reviewlist);
                if(reviewlist.size() > 0){
                    reviewId = reviewlist.get(0).Id;
                }   
            }else if(dsmtContact != null){
                reviewlist = [select id,name,status__c from Review__c where Dsmtracker_contact__c =: dsmtContact.Id and Status__c = 'Draft' and Review_Type__c =:REVIEW_TYPE];
                if(reviewlist.size() > 0){
                    reviewId = reviewlist.get(0).Id;
                }    
            }
            
            if(reviewId == null){
                Schema.DescribeSObjectResult d = Schema.SObjectType.Review__c;
                Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
                
                
                Review__c newReview = new Review__c(status__c ='Draft',Review_Type__c = REVIEW_TYPE);
                newReview.Requested_by__c = Userinfo.getuserid();
                
                boolean isInsert = false;
                if(taaccount != null){
                    Schema.RecordTypeInfo rtByName =  rtMapByName.get('Trade Ally Account');
                    
                    newReview.Trade_Ally_Account__c = taaccount.id;
                    newReview.Company_Address__c = taaccount.Street_Address__c;
                    newReview.Company_City__c =  taaccount.Street_City__c;
                    newReview.Company_State__c =  taaccount.Street_State__c;
                    newReview.Company_Zipcode__c = taaccount.Street_Zip__c;
                    newReview.Company_Name__c = taaccount.Name;
                    newReview.Company_Email__c = taaccount.Email__c;
                    newReview.Company_Phone__c = taaccount.Phone__c; 
                    newReview.Company_Website__c = taaccount.Website__c;
                    newReview.RecordTypeId = rtByName.getRecordTypeId();
                    isInsert =true;
                }
                
                if(dsmtContact != null){
                    Schema.RecordTypeInfo rtByName =  rtMapByName.get('Trade Ally Contact');
                    
                    
                    newReview.Dsmtracker_contact__c = dsmtconid;
                   // newReview.Trade_Ally_Account__c = dsmtcontact.Trade_Ally_Account__c;
                    newReview.First_Name__c = dsmtContact.First_Name__c;
                    newReview.Last_Name__c = dsmtContact.Last_Name__c;
                    newReview.Phone__c = dsmtContact.Phone__c;
                    newReview.Email__c = dsmtContact.Email__c;
                    newReview.Address__c =dsmtContact.Address__c;
                    newReview.City__c = dsmtContact.City__c;
                    newReview.State__c = dsmtContact.State__c; 
                    newReview.Zipcode__c = dsmtContact.Zip__c;
                    newReview.RecordTypeId = rtByName.getRecordTypeId();
                    isInsert = true;
                }
                
                
                if(isInsert == true){
                    insert newReview;
                    reviewId = newReview.id;
                    copyAttachmentToReview(newReview.id);
                }   
                
                
            }
            
            PageReference pg = Page.dsmtTradeAllyProfileUpdate;
            pg.getParameters().put('id', reviewId); 
            pg.setRedirect(true);
            return pg;
        }
        
        return null;
    }
    
    
    public void copyAttachmentToReview(String revId){
        
            List<Attachment__c> newattachlist = new List<Attachment__c>();
            String whereclause = '';
            if(taaccount != null){    
                whereclause = 'Trade_Ally_Account__c=\''+taaccount.Id+'\' and Dsmtracker_Contact__c = null';
            }
            if(dsmtContact != null){
                whereclause = 'Dsmtracker_Contact__c = \''+dsmtcontact.Id+'\'';
            }
            String query = dsmtFuture.getCreatableFieldsSOQL('Attachment__c',whereclause,'');
            system.debug('--query--'+query);
            List<Attachment__c> attachlist = Database.query(query);
            for(Attachment__c attach : attachlist){
                Attachment__c newattach = attach.clone(false);
                newattach.Trade_ally_Account__c = null;
                newattach.Review__c = revId;
                newattach.Status__c = 'UnSubmitted';
                newattach.Shadow_Attachment__c = attach.id;
                newattach.OwnerId = userinfo.getuserid();
                newattachlist.add(newattach);
            }
            
            if(newattachlist.size() > 0){
                insert newattachlist;
            }  
        
    }
    
    public PageReference saveAsDraft(){
        if(attachId != null && attachExpdate != null && attachLic != null){ 
            Attachment__c att = [SELECT Id,Name FROM Attachment__c WHERE Id =:attachId LIMIT 1];
            if(att != null){
                att.Lic_Number__c = attachLic;
                att.Expires_On__c = Date.parse(attachExpdate);
                update att;
            }
        }
        if(review != null){
            update review;
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Your request has been successfully save as draft..'));            
            return null;

        } 
        return null;
    }
    
    public PageReference saveAsSubmit(){
        set<String> deleteattachids = new Set<String>();
        
        for(Attachment__c attach : attachlist){
           if(attach.Status__c == 'UnSubmitted'){
              deleteattachids.add(attach.id);
           }
        }
        
        if(deleteattachids.size() > 0){
           delete [select id from attachment__c where id in: deleteattachids];
        }
        
        Review.Status__c = 'Submitted For Review';
        Review.Requested_By__c = Userinfo.getuserid();
        update Review;
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Your request has been successfully submitted for review..'));            
        return null;

    }
    
    /******************* Fetching States from Custom Settings*******/
    public List < SelectOption > getStateList() {
        List < SelectOption > options = new List < SelectOption > ();
        options.add(new SelectOption(' ', '-- Select State --'));
        
        // Find all the countries in the custom setting
        Map < String, dsmt_Reg_States__c > states = dsmt_Reg_States__c.getAll();
        // Sort them by name
        List < String > stateNames = new List < String > ();
        stateNames.addAll(states.keySet());
        stateNames.sort();
        
        for (String statName: stateNames) {
            dsmt_Reg_States__c state = states.get(statName);
            options.add(new SelectOption(state.State_Name__c, state.Name));
        }
        return options;
    }
}