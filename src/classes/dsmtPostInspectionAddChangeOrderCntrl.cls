public with sharing class dsmtPostInspectionAddChangeOrderCntrl {

    public string recommXml {get;set;}
    public Review__c rev{get;set;}
    public string dataSave{get;set;}
    public String revId{get;set;}
    public String reviewType {get;set;}
    public decimal subTotal {get;set;}
    public decimal utilityIncentive {get;set;}
    public decimal customerContribution {get;set;}
    public decimal customerDeposit {get;set;}
    public decimal remainingCustomerContribution {get;set;}
    public string EAName{get;set;}
    public string TAName{get;set;}
    public boolean showApproved{get;set;}
    public String inspRequestId{get;set;}
    public Id BillingReviewId;
    public Date InstalledDate{get;set;}
    public string assessId{get;set;}
    
    public dsmtPostInspectionAddChangeOrderCntrl (){
        init();
    }
    
    public dsmtPostInspectionAddChangeOrderCntrl(ApexPages.StandardController controller) {
        init();
    }
    
    private void init()
    {
        showApproved = false;
        rev = new Review__c();
        
        assessId = ApexPages.currentPage().getParameters().get('assessId');
        revId = ApexPages.currentPage().getParameters().get('id');
        
        checkReviewType();
        loadRecommendations();
        
        List<User> userList = [select id,profile.Name from user where id =: userinfo.getuserId()];
        
        if(userList != null && userList.get(0).Profile.Name == 'DSMTracker Back Office'){
            showApproved  = true;
        }
        
    }
    
    private void checkReviewType()
    {
        List<Review__C> reviewList = [select Id,inspection_Request__c,inspection_Request__r.Review__r.Installed_Date__c,RecordType.Name,Trade_Ally_Account__r.Name, Energy_Assessment__r.Name,Status__c
                                      from Review__C where Id =: revId limit 1];
        if(reviewList.size() > 0){
            Review__C review = reviewList[0];
            reviewType = review.RecordType.Name; 
            if(review.Status__c == 'Failed Completion Review'){
                reviewType = 'Failed Billing Review';
            }
            EAName = review.Energy_Assessment__r.Name;
            TAName = review.Trade_Ally_Account__r.Name;
            inspRequestId = review.inspection_Request__c; 
            InstalledDate = review.Inspection_Request__r.Review__r.Installed_Date__c;
        }
    }
    
    public Pagereference createReviewRec(){
        
        String projId = ApexPages.currentPage().getParameters().get('projid');
        
        if(revId != null)
        {
            loadChangeOrderReview();  
        }
        else if(projId != null)
        {    
            List<Project_Review__c> prList = [select id, Review__c, Project__c 
                                              from Project_Review__c 
                                              where Project__c =: projId
                                              and Review__c != null
                                              and (Review__r.RecordType.Name = 'Work Scope Review'
                                              or Review__r.RecordType.Name = 'Work Scope Review Manual')];
            
            if(prList.size() > 0)
                return new PageReference('/apex/AddRemoveReviewRecommendation?id='+prList[0].Review__c+'&projid='+projId).setRedirect(true);
        }
        
        return null;
    }
    
    private set<String> fetchProjectIdSet(Id reviewId)
    {
        set<String> projectIdSet = new Set<String>();
        
        List<Project_Review__c> revList = [select id, Name, Review__c, Project__c 
                                           from Project_Review__c 
                                           where Review__c =: reviewId
                                           and Review__c != null];
        for(Project_Review__c pr : revList)
        {
            projectIdSet.add(pr.Project__c);
        }
        
        return projectIdSet;
    }
    
    public void CreateReviewRecord(){
        if(revId != null)
        {
            List<Review__c> reviewList = [select Id,Name,Project__c,Status__c,Work_Scope_Review__c,Billing_Review__c,Trade_Ally_Account__r.Name,
                                          Energy_Assessment__r.Name,Energy_Assessment__c
                                          from Review__c 
                                          where (Post_Inspection_Review__c = :revId)
                                          and Type__c = 'Change Order Review'
                                          and (Status__c = 'New' or Status__c = 'Rejected')
                                          limit 1];
            
            if(reviewList.size() > 0)
            {
                rev = reviewList[0];
                
                system.debug('--BillingReviewId ---'+BillingReviewId);
                
                if(rev.Billing_Review__c != null){
                    BillingReviewId = rev.Billing_Review__c;
                }else{
                    List<Review__c> billingRevlist = [select id from Review__c where Energy_Assessment__c =: rev.Energy_Assessment__c
                                                        and Type__c = 'Billing Review'];
                    
                    if(billingRevlist != null && billingRevlist.size() > 0){
                        BillingReviewId = billingRevlist.get(0).Id;
                    }
                    
                }
                system.debug('--BillingReviewId ---'+BillingReviewId);
            }
            else
            {
                loadChangeOrderReview();
                upsert rev; 
                if(rev.Billing_Review__c != null){
                    BillingReviewId = rev.Billing_Review__c;
                }else{
                    List<Review__c> billingRevlist = [select id from Review__c where Energy_Assessment__c =: rev.Energy_Assessment__c
                                                        and Type__c = 'Billing Review'];
                    
                    if(billingRevlist != null && billingRevlist.size() > 0){
                        BillingReviewId = billingRevlist.get(0).Id;
                    }
                    
                }
                system.debug('--BillingReviewId ---'+BillingReviewId);
            }
        }
    }
    
    private void loadChangeOrderReview()
    {
        List<Review__C> lstPrj = [select Id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                    Energy_Assessment__r.Trade_Ally_Account__r.Email__c,First_Name__c,
                                    Last_Name__c,Phone__c,Email__c,Address__c,City__c,State__c,
                                    Zipcode__c,Trade_Ally_Account__c,Billing_Review__c from Review__C 
                                    where Id =: revId limit 1]; 
        rev.Status__c = 'Approved';
        Id devRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();

        rev.Trade_Ally_Account__c = lstPrj.get(0).Trade_Ally_Account__c;
                                           
        if(lstPrj.get(0).Energy_Assessment__c != null)    
            rev.Energy_Assessment__c = lstPrj.get(0).Energy_Assessment__c;
        
        rev.Type__c = 'Change Order Review';
        rev.First_Name__c = lstPrj.get(0).First_Name__c;
        rev.Last_Name__c = lstPrj.get(0).Last_Name__c;
        rev.Phone__c = lstPrj.get(0).Phone__c;
        rev.Email__c = lstPrj.get(0).Email__c;
        rev.Address__c = lstPrj.get(0).Address__c;
        rev.City__c = lstPrj.get(0).City__c;
        rev.State__c = lstPrj.get(0).State__c;
        rev.Zipcode__c = lstPrj.get(0).Zipcode__c;
        rev.Post_Inspection_Review__c = lstPrj.get(0).Id;
        rev.Billing_Review__c = lstPrj.get(0).Billing_Review__c ;
        
        if(lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c != null)
            rev.Trade_Ally_Email__c = lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c;
        
        rev.RecordTypeId =  devRecordTypeId;
    }
    
    public PageReference cancelCO(){
         
         if(draftRecIds.size() > 0){
            delete [select id from Recommendation__c where id in : draftRecIds];
         }
         
         return new PageReference('/'+revId).setRedirect(true);
    }
    
    public Set<String> draftRecIds{get;set;}
    public void loadRecommendations()
    {
        
        draftRecIds = new Set<String>();
        remainingCustomerContribution = customerDeposit = utilityIncentive = subTotal = customerContribution = 0;
        
        recommXml = '<Recommendations>';
        
        Set<String> projIds = new Set<String>();
        
        string revId1 = ApexPages.currentPage().getParameters().get('id');
        
        if(revId1  != null){
            projIds = fetchProjectIdSet(revId1);
        }else{
            String projId = ApexPages.currentPage().getParameters().get('projid');
            projIds.add(projId);
        }
        
        map<string, decimal> projCollectedMap = new map<string, decimal>();
        
        
        Map<Id,Decimal> changeOrderMap = new Map<Id,Decimal>();
        
        if(inspRequestId != null){
            /*String query = 'select ' + sObjectFields('Inspection_Line_Item__c') +' Recommendation__r.Recommendation_Scenario__r.Total_Collected__c, Recommendation__r.Recommendation_Scenario__r.Total_CustomerIncentive__c, Recommendation__r.Recommendation_Scenario__r.Customer_Cost__c, Id,Recommendation__r.DSMTracker_Product__r.Name,Recommendation__r.Recommendation_Scenario__r.Name,Recommendation__r.Recommendation_Scenario__r.Type__c,Recommendation__r.Recommendation_Scenario__r.Status__c,Recommendation__r.Recommendation_Scenario__r.Installed_Date__c,Inspection_Request__r.Energy_Assessment__r.Primary_Provider__c,Recommendation__r.Dsmtracker_Product__r.EMHome_EMHub_PartID__c,Recommendation__r.Part_Cost__c,Recommendation__r.Change_Order_Reason__c from Inspection_Line_Item__c where Inspection_Request__c =:inspRequestId  order by CreatedDate desc limit 300 ';
            
            Set<Id> recId = new Set<Id>();
            
            List<Inspection_Line_Item__c> insplineitemList = database.query(query);
            
            for (Inspection_Line_Item__c irline: insplineitemList) {
                recId.add(irline.Recommendation__c);
            }
            
            if(recId.size() > 0){
                List<Review__c> changeRevList = [select id 
                                                 from Review__c 
                                                 where (Billing_Review__c =: revId
                                                 OR Work_Scope_Review__c =: revId) 
                                                 and RecordType.Name = 'Change Order Review'
                                                 and (Status__c = 'New' or Status__c = 'Rejected')
                                                 order by CreatedDate Desc];
                
                if(changeRevList  != null && changeRevList.size() > 0){
                    
                    Set<Id> crId = new Set<Id>();
                    
                    for( Review__c c : changeRevList){
                        crId.add(c.Id);
                    }
                
                    List<Change_Order_Line_Item__c> chageorderList = [select id,Change_Order_Quantity__c,Recommendation__c from Change_Order_Line_Item__c
                                                                      where Recommendation__c in : recId 
                                                                      and (Billing_Review__c =: revId
                                                                      OR Review__c =: revId)
                                                                      and Change_Order_Review__c =: changeRevList.get(0).Id
                                                                      order by CreatedDate Desc];
                                                                      
                    for(Change_Order_Line_Item__c c : chageorderList){
                        if(changeOrderMap.get(c.Recommendation__c) == null && c.Change_Order_Quantity__c != null){
                            changeOrderMap.put(c.Recommendation__c, c);
                        }
                    }
                }
            }*/
            
            
            //Pradip Patel - 26-4
            String queryreco = '';
            queryreco += 'select '+ sObjectFields('Recommendation__c') +' Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Barrier_Incentive__c,';
            queryreco += ' Recommendation_Scenario__r.Total_Collected__c,DSMTracker_Product__r.Name,(select id,spec_d_Quantity__c,Installed_Quantity__c,Inspected_Quantity__c,Description__c,Flag_for_Billing_Adjustment__c,Status__c,Quick_Notes__c,Inspection_Request__c from Inspection_Line_Items__r where inspection_request__c =: inspRequestId),(Select id,Project__c,Recommendation__c,Utility_Incentive_Share__c, Final_Recommendation_Incentive__c,Proposal__r.Current_Incentive__c,proposal__r.Final_Proposal_Incentive__c,proposal__r.Barrier_Incentive_Amount__c from Proposal_Recommendations__r where Project__c =:projIds) from Recommendation__c ';
            queryreco += '  where Recommendation_Scenario__c in : projIds ';
            queryreco += '  order by createddate desc,Recommendation_Scenario__r.name asc,Recommendation_Description__c asc limit 300'; 
            
            List<Recommendation__c> recommendlist = database.query(queryreco);
            for (Recommendation__c ir: recommendlist) {
                changeOrderMap.put(ir.id,ir.Change_Order_Quantity__c);
            }
            
              Set<String> proposalIds = new Set<String>();
            For(Recommendation__c recomm:recommendlist){
                List<Inspection_Line_Item__c> lineitems = recomm.Inspection_Line_Items__r;
                system.debug('--recomm.Part_Cost__c--'+recomm.Part_Cost__c);
                
                if(recomm.Unit_Cost__c != null && changeOrderMap.get(recomm.id) != null){
                    subTotal += recomm.Unit_Cost__c * changeOrderMap.get(recomm.id);
                 } else if(recomm.Unit_Cost__c != null && recomm.Quantity__c != null){
                    subTotal += recomm.Unit_Cost__c * recomm.Quantity__c;
                 }
                 
                 Proposal_Recommendation__c pr = null;

                if(!recomm.Proposal_Recommendations__r.isEmpty()) { 
                    pr = recomm.Proposal_Recommendations__r.get(0);
                }
                
                 if(pr != null){
                    if(proposalIds.add(pr.proposal__c)){
                        System.debug('--Final_Proposal_Incentive'+pr.Proposal__r.Final_Proposal_Incentive__c);
                        utilityIncentive += pr.Proposal__r.Final_Proposal_Incentive__c ;    
                    }
                }
                
                
                if(lineitems != null && lineitems.size() > 0){
                    Inspection_Line_Item__c line = lineitems.get(0);
                    
                    if(recomm.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(recomm.Recommendation_Scenario__c + '~~~' + recomm.Recommendation_Scenario__r.Name, recomm.Recommendation_Scenario__r.Total_Collected__c);
               
                    recommXml += '<Recommendation>';
                    
                    recommXml += '<Checked>' + false + '</Checked>';
                    recommXml += '<ProjectId>' + recomm.Recommendation_Scenario__c + '</ProjectId>';
                    recommXml += '<ProjectName><![CDATA[<a href="/'+recomm.Recommendation_Scenario__c+'" target="_blank">' + checkStringNull(recomm.Recommendation_Scenario__r.Name) + '</a>]]></ProjectName>';
                    recommXml += '<RecommendationId>' + recomm.Id + '</RecommendationId>';
                    recommXml += '<RecommendationName><![CDATA[' + checkStringNull(recomm.DSMTracker_Product__r.Name) + ']]></RecommendationName>';
                    
                    if(recomm.Installed_Quantity__c == null)
                        recommXml += '<Quantity></Quantity>';
                    else
                        recommXml += '<Quantity><![CDATA[' + recomm.Installed_Quantity__c + ']]></Quantity>'; 
                    
                    recommXml += '<InspectionLineItemId>' + line.Id + '</InspectionLineItemId>';
                    recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
                    
                    if(recomm.Part_Cost__c != null)
                        recommXml += '<PartCost><![CDATA[' + recomm.Part_Cost__c+ ']]></PartCost>';
                    else
                        recommXml += '<PartCost></PartCost>';
                    
                    recommXml += '<Reason><![CDATA[' + checkStringNull(recomm.Change_Order_Reason__c)+ ']]></Reason>';
                    
                    recommXml += '</Recommendation>';
                
                    
                    
                }else{
                
                    if(recomm.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(recomm.Recommendation_Scenario__c + '~~~' + recomm.Recommendation_Scenario__r.Name, recomm.Recommendation_Scenario__r.Total_Collected__c);
               
                    recommXml += '<Recommendation>';
                    
                    recommXml += '<Checked>' + false + '</Checked>';
                    recommXml += '<ProjectId>' + recomm.Recommendation_Scenario__c + '</ProjectId>';
                    recommXml += '<ProjectName><![CDATA[<a href="/'+recomm.Recommendation_Scenario__c+'" target="_blank">' + checkStringNull(recomm.Recommendation_Scenario__r.Name) + '</a>]]></ProjectName>';
                    recommXml += '<RecommendationId>' + recomm.Id + '</RecommendationId>';
                    recommXml += '<RecommendationName><![CDATA[' + checkStringNull(recomm.DSMTracker_Product__r.Name) + ']]></RecommendationName>';
                    if(recomm.Installed_Quantity__c == null)
                        recommXml += '<Quantity></Quantity>';
                    else
                        recommXml += '<Quantity><![CDATA[' + recomm.Installed_Quantity__c + ']]></Quantity>'; 
                    
                    if(recomm.Change_Order_Quantity__c == null)
                        recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
                    else
                         recommXml += '<ChangeOrderQuantity><![CDATA[' + recomm.Change_Order_Quantity__c + ']]></ChangeOrderQuantity>';
                    
                    if(recomm.Part_Cost__c != null){
                        recommXml += '<PartCost><![CDATA[' + recomm.Part_Cost__c+ ']]></PartCost>';
                    }
                    else{
                        recommXml += '<PartCost></PartCost>';
                    }
                        
                    recommXml += '<Reason><![CDATA[' + checkStringNull(recomm.Change_Order_Reason__c)+ ']]></Reason>';
                   
                    recommXml += '</Recommendation>';
                    
                    if(recomm.Status__c == 'Draft'){
                       draftRecIds.add(recomm.id);
                    }
                    
                   
                }
            }
        }
        
        customerContribution = subTotal - utilityIncentive;
        
        if(customerContribution != null){
            customerContribution.setScale(2);
        }
        
        for(String s : projCollectedMap.keyset())
        {
            customerDeposit += projCollectedMap.get(s);
        }
        
        remainingCustomerContribution = (customerContribution - customerDeposit);
        
        recommXml += '</Recommendations>';
        
        system.debug('--recommXml ---'+recommXml );
    }
    
    public string checkStringNull(string str){
        return str == null?'':str;
    }
    
    public void saveProjId(){
        
        CreateReviewRecord();
    try{     
        List<Recommendation__c> lstRec = new List<Recommendation__c>();
        List<Change_Order_Line_Item__c> lstChangeOrderItems = new List<Change_Order_Line_Item__c >();
        
        
        if(dataSave != null && dataSave != ''){
            map<String, String> recQMap = new map<String, String>();
            list<String> splitedData = dataSave.split(';');
            
            for(string s: splitedData){
                recQMap.put(s.split('-')[0], s.split('-')[1]);
            }
            
            List<Recommendation__c> recomList = [select id, Name, Recommendation_Scenario__c, Recommendation_Description__c, Quantity__c, Units__c,status__c,
                                                 isChecked__c, Recommendation_Scenario__r.Name, Inspected_Quantity__c, Change_Order_Quantity__c,
                                                 Change_Order_Reason__c,IsChangeOrder__c 
                                                 from Recommendation__c 
                                                 where id in : recQMap.keyset()];
                                                 
            for(Recommendation__c r : recomList){
                
                    system.debug('--r.IsChangeOrder__c---'+r.IsChangeOrder__c+'- '+r.Recommendation_Description__c);
                    Change_Order_Line_Item__c rec = new Change_Order_Line_Item__c();
                    
                    system.debug('--recQMap.get(r.Id)--'+recQMap.get(r.Id));
                    List<String> splitedQnty = recQMap.get(r.Id).split(',');
                  
                    decimal changeOrderQn = r.Change_Order_Quantity__c;
                  
                 //   if(decimal.valueof(splitedQnty[0]) != decimal.valueof(splitedQnty[2])){
                       
                        rec.Recommendation__c = r.Id;
                        
                        if(splitedQnty != null && splitedQnty.size() > 0 && splitedQnty[0] != null){
                            rec.Change_Order_Quantity__c = splitedQnty[0] == null ? null: decimal.valueof(splitedQnty[0]);
                        }
                        if(splitedQnty != null && splitedQnty.size() > 1 && splitedQnty[1] != null){
                            rec.Change_Order_Reason__c   = splitedQnty[1];
                            r.Change_Order_Reason__c   = splitedQnty[1];
                        }
                        if(splitedQnty != null && splitedQnty.size() > 2 && splitedQnty[2] != null && splitedQnty[2] != ''){
                          try{
                            rec.Original_Quantity__c = splitedQnty[2] == null ? null: decimal.valueof(splitedQnty[2]);
                          }catch(Exception e){
                             system.debug('--e--'+e);
                          }  
                        }
                        if(splitedQnty != null && splitedQnty.size() > 0 && splitedQnty[0] != null){
                          try{
                              r.Change_Order_Quantity__c = splitedQnty[0] == null ? null: decimal.valueof(splitedQnty[0]);
                              r.Installed_Quantity__c = r.Change_Order_Quantity__c;
                           }catch(Exception e){
                               system.debug('--e--'+e); 
                           } 
                        }
                                            
                        rec.Review__c = revId;
                        rec.Change_Order_Review__c = rev.Id;
                        rec.Project__c = r.Recommendation_Scenario__c;
                        rec.Status__c = 'Approved';
                        
                        lstChangeOrderItems.add(rec);
                 //   }
                
                   if(r.status__c == 'Draft'){
                       r.status__c = 'Installed';
                       if(InstalledDate != null){
                          r.Installed_Date__c = InstalledDate;
                       }
                   }
            }
            
            if(lstChangeOrderItems.size()>0){
                dsmtRecommendationHelper.ChangeorderLineTrigger = true;
                upsert lstChangeOrderItems;
                system.debug('--lstChangeOrderItems---'+lstChangeOrderItems.get(0).Id);
            }
            update recomList;
            
            if(BillingReviewId != null){
                dsmtFuture.voidInvoiceLineItems(BillingReviewId);
                dsmtFuture.voidPayments(BillingReviewId);
                //dsmtFuture.InsertInvoiceLineItemWhenStatusBatched(BillingReviewId);
            }
          }
        
          loadRecommendations();
        } catch(Exception ex){
            system.debug('Exception :::::' + ex.getMessage());
        } 
    }
    
     private static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
}