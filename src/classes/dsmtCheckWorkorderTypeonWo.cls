global class dsmtCheckWorkorderTypeonWo implements Schedulable,database.batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
       // String appId = 'a0p4D0000007X6Y';
        String query = 'SELECT Id,Appointment_Type_Friendly_Name__c,Workorder__c';
        query += ' from Appointment__c where Workorder__r.Workorder_Type__c = null and ';
        query+= ' Workorder__r.Workorder_Type_Not_Found__c = false and Appointment_Type_Friendly_Name__c != null Order by Name Desc';
        system.debug('--query---'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Appointment__c> scope)
    {
          Set<String> woTypes = new Set<String>();
          
          for(Appointment__c app : scope){
              if(app.Appointment_Type_Friendly_Name__c != null){
                 woTypes.add(app.Appointment_Type_Friendly_Name__c);
              }
          }
          
          Map<String,Workorder_Type__c> woTypeMap = new Map<String,Workorder_Type__c>();
          List<Workorder_Type__c> wtypelist = [select id,name,Friendly_Name__c from Workorder_Type__c where Friendly_Name__c in: woTypes];
          if(wtypelist.size() > 0){
              for(Workorder_Type__c wotype : wtypelist){
                 woTypeMap.put(wotype.Friendly_Name__c,wotype);
              }
          }
          
          List<Workorder__c> wotoupdate = new List<Workorder__c>();
          Workorder__c wo = null;
          
          for(Appointment__c app : scope){
              
              if(app.workorder__c != null){
                  wo = new Workorder__c();
                  wo.Id = app.workorder__c;
                 
                  if(woTypeMap.get(app.Appointment_Type_Friendly_Name__c) != null){
                     wo.Workorder_Type__c = woTypeMap.get(app.Appointment_Type_Friendly_Name__c).Id;
                   //  wotoupdate.add(wo);
                  }else{
                      wo.Workorder_Type_Not_Found__c  = true;
                  }
                  wo.Early_Arrival_Time__c = System.now(); 
                  wo.Late_Arrival_Time__c = System.now().addHours(2);
                  wotoupdate.add(wo);
              }
          }
          
          if(wotoupdate.size() > 0){
              dsmtFutureHelper.woupdatedFromSchedueled  = false;
              update wotoupdate;
          }
    }  
    
    global void finish(Database.BatchableContext BC)
    {
    
    }
    
    global void  execute(SchedulableContext sc){
        dsmtCheckWorkorderTypeonWo obj = new dsmtCheckWorkorderTypeonWo();
        database.executebatch(obj,200);    
    }
}