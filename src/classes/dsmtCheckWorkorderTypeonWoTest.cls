@isTest
public class dsmtCheckWorkorderTypeonWoTest {

    static testmethod void test(){
        
         Workorder_Type__c wt = new Workorder_Type__c();
         wt.Friendly_Name__c = 'Test';
         insert wt;
        
         Workorder__c wo = new Workorder__c();
         wo.Early_Arrival_Time__c = Datetime.now();
         wo.Late_Arrival_Time__c = Datetime.now();
         wo.Workorder_Type_Not_Found__c = false;
         wo.Workorder_Type__c = null;
         insert wo;
        
        Appointment__c a = new Appointment__c();
        a.Appointment_Type_Friendly_Name__c='Test';
        a.Workorder__c = wo.Id;
        insert a;
        
        dsmtCheckWorkorderTypeonWo obj = new dsmtCheckWorkorderTypeonWo();
        obj.execute(null);
        
        
    }
}