@istest
public class AtticTrmalEnvelopTypeCompoCntrlTest
{
	@istest
    static void runtest()
        {
            Attic_Space_Work_Order_Data__c aswod = new Attic_Space_Work_Order_Data__c();
            insert aswod;
            
			Ceiling__c ce =new Ceiling__c();
            insert ce;  
            
            Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
            insert tet;
            
            Wall__c wall =new Wall__c();
            insert wall;
            
            Floor__c fl = new Floor__c();
            insert fl;
            
			AtticThermalEnvelopeTypeComponentCntrl atc =new AtticThermalEnvelopeTypeComponentCntrl();
            //atc.thermalEnvelopeTypeId=tet.Id;
            ApexPages.currentPage().getParameters().put('thermalEnvelopeTypeId',tet.id);
			atc.showVisiblePanel();
            atc.saveAtticVisiblePanel();
            atc.queryAtticAWHFs();
            atc.addNewAtticAWHF();
            atc.deleteAtticAWHF();
            atc.saveAtticAWHFs();
            atc.saveAtticVentSets();
            atc.addNewAtticVentSet();
            atc.deleteAtticVentingSet();
            atc.queryAtticVentingSets();
            atc.addNewAtticRoof();
            atc.editAtticWall();
        }
}