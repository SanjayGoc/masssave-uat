@isTest
Public class dsmtLocationScheduleCntrlTest{
    @isTest
    public static void runTest(){
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Location__c loc = Datagenerator.createLocation();
        insert loc;
        
        Employee__c emp = Datagenerator.createEmployee(loc.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, emp.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Status__c = 'Unscheduled';
        wo.Work_Team__c = wt.Id;
        wo.Requested_Start_Date__c = System.now();
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();
        insert wo;
        
        Appointment__c a = new Appointment__c(Appointment_Type__c = 'df',
                                              Appointment_Status__c = 'sd',
                                              Appointment_Start_Time__c = system.now(),
                                              Appointment_End_Time__c = system.now().addDays(1));
        insert a;
        
        ApexPages.currentPage().getParameters().put('sel_loc', loc.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(loc);
        dsmtLocationScheduleController cntrl = new dsmtLocationScheduleController(sc);
    }
}