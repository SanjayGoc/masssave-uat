global class dsmtbatchScheduledWorkOrder implements Schedulable,database.batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Date dt = Date.Today().Adddays(-1);
        String query = 'SELECT Id,Status__c from workorder__c where Status__c=\'Scheduled\' and Scheduled_Date__c <=: dt ';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<workorder__c> scope)
    {
        
        List<workorder__c> woToupdate = new List<workorder__c>();
        
        for (workorder__c w : scope)
        {
            w.status__c = 'Completed';
            woToupdate.add(w);
        }
        if(woToupdate.size() > 0){
            update woToupdate;
        }
          
    }  
    global void finish(Database.BatchableContext BC)
    {
    
    }
    
    global void  execute(SchedulableContext sc){
        dsmtbatchScheduledWorkOrder obj = new dsmtbatchScheduledWorkOrder();
        database.executebatch(obj,1);    
    }
}