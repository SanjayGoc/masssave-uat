public class dsmtUnscheduledWorkorderController{
    public string us_workorder_xml {get;set;}
    
    public dsmtUnscheduledWorkorderController(){
    
    }
    
    @TestVisible
    private datetime CalculateDatetime(string dt, boolean is_for_now) {
        if (dt != null && dt.length() > 0) {
            list < string > splited_date = dt.split('/');
            if (is_for_now)
                return Datetime.newInstance(Integer.valueOf(splited_date[2]), Integer.valueOf(splited_date[0]), 1, 0, 0, 0);
            else
                return Datetime.newInstance(Integer.valueOf(splited_date[2]), Integer.valueOf(splited_date[0]), Integer.valueOf(splited_date[1]), 23, 59, 59);
        }

        return Datetime.now();
    }
    
    //GET ALL EVENTS AS JOSN STRING
    public void getWorkordersJSON() {
        string selected_date = ApexPages.currentPage().getParameters().get('sel_dt');
        
        datetime sel_dt = CalculateDatetime(selected_date, true);
        datetime e_dt = sel_dt.addMonths(1); //CalculateDatetime(selected_date, false).addMonths(1);
        
        set < Integer > event_days = new set < Integer > ();
        set < Integer > non_event_days = new set < Integer > ();
        
        us_workorder_xml = '<Workorders>';
        
        for (Workorder__c wo_rec: [select id, Employee__c, Employee__r.name, name, Description__c, Requested_Start_Date__c, Requested_Date__c,
                                Requested_End_Date__c, User__c, User__r.Name, Work_Team__c, Customer__c, Work_Team__r.NAme,
                                Customer__r.Address__c, Customer__r.City__c, Customer__r.State__c, Customer__r.Zip_Code__c,
                                Customer__r.Name, Notes__c, Priority__c, Promise_Time_End__c, Promise_Time_Start__c, Status__c,
                                Scheduled_Start_Date__c, Scheduled_End_Date__c,
                                Time_Slot__c, Time_Slot__r.Time_Slot__c, Work_Order_Notes__c, Emergency__c,
                                Workorder_Type__c, Workorder_Type__r.Name, Workorder_Type__r.Event_Color__c,
                                Customer__r.Phone__c, Scheduled_Date__c, ZipCode__c,
                                Address__c, City__c, State__c
                                from Workorder__c
                                where (Status__c = 'Unscheduled' or
                                Status__c = 'Requires Rescheduling')
                                and Work_Team__r.Location__c =: ApexPages.currentPage().getParameters().get('sel_loc')
                                order by Priority__c, Requested_Start_Date__c asc
            ]) {
            
            string customer_add = wo_rec.Address__c != null ? wo_rec.Address__c + ',' : '';
            customer_add += wo_rec.City__c != null ? wo_rec.City__c + ',' : '';
            customer_add += wo_rec.State__c != null ? wo_rec.State__c + ',' : '';
            //customer_add += wo_rec.Customer__r.Country__c != null ? wo_rec.Customer__r.Country__c + ',' : '';
            customer_add += wo_rec.ZipCode__c != null ? wo_rec.ZipCode__c : '';

            
            us_workorder_xml += '<Workorder>';
            
            us_workorder_xml += '<WorkorderId><![CDATA['+wo_rec.Id+']]></WorkorderId>';
            us_workorder_xml += '<WorkorderName><![CDATA['+wo_rec.Name.replace('WRK-000','')+']]></WorkorderName>';
            
            if(wo_rec.Requested_Start_Date__c != null)
                us_workorder_xml += '<RequestedDate><![CDATA['+wo_rec.Requested_Start_Date__c.month()+'/'+wo_rec.Requested_Start_Date__c.day()+'/'+wo_rec.Requested_Start_Date__c.year()+ ' ' + wo_rec.Requested_Start_Date__c.hour() +':'+ wo_rec.Requested_Start_Date__c.minute() + (wo_rec.Time_Slot__r.Time_Slot__c != null ? '<br>'+wo_rec.Time_Slot__r.Time_Slot__c : '') + ']]></RequestedDate>';
            else
                us_workorder_xml += '<RequestedDate></RequestedDate>';
                
            if(wo_rec.Workorder_Type__r.Name == 'Emergency Service Order')
                us_workorder_xml += '<RowClass><![CDATA[red]]></RowClass>';
            else if(wo_rec.Requested_Start_Date__c != null && wo_rec.Requested_Start_Date__c < system.today())
                us_workorder_xml += '<RowClass><![CDATA[yellow]]></RowClass>';
            else
                us_workorder_xml += '<RowClass><![CDATA[white]]></RowClass>';
            
            us_workorder_xml += '<CustomerId><![CDATA['+wo_rec.Customer__c+']]></CustomerId>';
            us_workorder_xml += '<CustomerName><![CDATA['+wo_rec.Customer__r.Name+ '<br>' + customer_add + ']]></CustomerName>';
            us_workorder_xml += '<Address><![CDATA['+customer_add+']]></Address>';
            us_workorder_xml += '<ContactInfo><![CDATA['+''+']]></ContactInfo>';
            us_workorder_xml += '<Type><![CDATA['+wo_rec.Workorder_Type__r.Name+']]></Type>';
            us_workorder_xml += '<Status><![CDATA['+wo_rec.Status__c+']]></Status>';
            us_workorder_xml += '<Schedule><![CDATA['+''+']]></Schedule>';
            us_workorder_xml += '<ReqDate><![CDATA['+ (wo_rec.Requested_Start_Date__c != null ? wo_rec.Requested_Start_Date__c.format('MM/dd/yyyy hh:mm a') : '') +']]></ReqDate>';
            us_workorder_xml += '<Priority><![CDATA['+ wo_rec.Priority__c +']]></Priority>';
            us_workorder_xml += '<ScheDate><![CDATA['+ (wo_rec.Scheduled_Start_Date__c != null ? wo_rec.Scheduled_Start_Date__c.format('MM/dd/yyyy hh:mm a') : '') +']]></ScheDate>';
            us_workorder_xml += '<Notes><![CDATA['+ (wo_rec.Work_Order_Notes__c != null ? wo_rec.Work_Order_Notes__c : '') +']]></Notes>';
            us_workorder_xml += '<WorkTeamId><![CDATA['+wo_rec.Work_Team__c+']]></WorkTeamId>';
            us_workorder_xml += '<WorkTeam><![CDATA['+wo_rec.Work_Team__r.Name+']]></WorkTeam>';
            us_workorder_xml += '</Workorder>';
        }
        
        us_workorder_xml += '</Workorders>';
    }
}