public class dsmtEnergyAssessmentSendEmail implements Schedulable{

    public void execute(SchedulableContext sc){
        
        DateTime dt = DateTime.newInstance( Date.Today().year(), Date.Today().Month(), Date.Today().day(), 0, 0,0);
        
        List<Energy_Assessment__c> EAslist= [Select Assessment_Type__c,name,Appointment_Date__c,Status__c,Customer__r.name
                        ,DSMTracker_Contact__r.name,Trade_Ally_Name__c,Assessment_Complete_Date__c,CreatedDate  from Energy_Assessment__c
                         where CreatedDate >=: dt And Is_Added_In_Report__c =:false ];
        
        
        
      
       
        String InnerHTML = '';
        
        InnerHTML += '<table style="width:100%" border="1">';
        
        InnerHTML += '<br></br>';
        InnerHTML += '<tr style="color:#FEFFFF;height:30px;"  align="Center" >';
        InnerHTML += '<th> Energy Assessment';
        InnerHTML += '</th>';
        InnerHTML += '<th> Assessment Type';
        InnerHTML += '</th>';
        InnerHTML += '<th> Appointment Date';
        InnerHTML += '</th>';
        InnerHTML += '<th> Status';
        InnerHTML += '</th>';
        InnerHTML += '<th> Customer';
        InnerHTML += '</th>';
        InnerHTML += '<th> DSMTracker Contact';
        InnerHTML += '</th>';
        InnerHTML += '<th> Trade Ally Name';
        InnerHTML += '</th>';
        InnerHTML += '<th>  Assessment Complete Date';
        InnerHTML += '</th>';

InnerHTML += '</tr>';
        
        Id easId = null;
       
        
         for(Energy_Assessment__c eas : EAslist){
                eas.Is_Added_In_Report__c =true;
                DateTime easdate=eas.Appointment_Date__c;
                easdate.format('dd-MM-yyyy');
                //easId = eas.OpportunityId;
                InnerHTML += '<tr>';
                //innerHTML += oppMap.get(oh.OpportunityId) + ',';
                InnerHTML += '<td>'+eas.Name;
                InnerHTML += '</td>';
                
              
                InnerHTML += '<td>'+easdate;
                InnerHTML += '</td>';
                
                
                InnerHTML += '<td>'+eas.Appointment_Date__c;
                InnerHTML += '</td>';
                
                InnerHTML += '<td>'+eas.Status__c;
                InnerHTML += '</td>';
                
                InnerHTML += '<td>'+eas.Customer__r.name;
                InnerHTML += '</td>';
                
                InnerHTML += '<td>'+eas.DSMTracker_Contact__r.name;
                InnerHTML += '</td>';
                
                 InnerHTML += '<td>'+eas.Trade_Ally_Name__c;
                InnerHTML += '</td>';
                 InnerHTML += '<td>'+eas.Assessment_Complete_Date__c;
                InnerHTML += '</td>';
                InnerHTML += '</tr>';
                
            }
            
            
       
        
        
        InnerHTML += '</table>';
        
        dsmtCreateEAssessRevisionController.stopTrigger =true;
        if(EAslist != null && EAslist.size()>0){
            update EAslist;
        }
        system.debug('--InnerHTML--'+InnerHTML);
        Messaging.SingleEmailMessage m = new Messaging.SingleEmailMessage();
       // m.setTargetObjectId(OwId);
        m.setHtmlBody(InnerHTML);
        
        m.setSubject('Energy Assessments');
        m.setSaveAsActivity(false);
        String[] ToAddress = new String[] {};
        
        for(String str : Label.Email_Address.split(';')){
           ToAddress.add(str.trim());
        }
        m.SetToAddresses(ToAddress);
       
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { m });
    }
}