@isTest
Public class dsmtMessagesCntlrTest
{
    @isTest
    public static void runTest()
    {
        Account  acc= Datagenerator.createAccount();
        acc.Billing_Account_Number__c= 'Gas-~~~1234445';
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();

        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 

        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.HIC_Attached__c=true;
        update reg;
        
         Messages__c msg= Datagenerator.createMessage();
        
         Attachment__c att= Datagenerator.createAttachment(Tacc.ID);
         att.Message__c= msg.id;
         update att;
        
        ApexPages.currentPage().getParameters().put('id',msg.id);
        
        dsmtMessagesCntlr controller = new dsmtMessagesCntlr();
        
        dsmtMessagesCntlr.MessageModal wrap= new dsmtMessagesCntlr.MessageModal();
        
        controller.initMessages();
        controller.openMessage();
        controller.backToMessage();
                
    }
    
 }