@isTest
public class dsmtPreviewAndEmailDocumentCntlrTest
{ 
    public testmethod static void test1()
    {
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea = Datagenerator.setupAssessment();
       
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
       
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Name='test';
        ir.Energy_Assessment__c=ea.id;
        ir.Contractor_Name__c=ta.id;
        //ir.Contractor_Name__c='test';
        insert ir;
        
        Proposal__c proposal =new Proposal__c();
        proposal.Name='test';
        proposal.Name_Type__c = 'Air Sealing';
        proposal.Energy_Assessment__c=ea.id;
        insert proposal;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Energy_Assessment__c = ea.Id;
        insert proj;
        
        /*Barrier__c br = new Barrier__c();
        br.Energy_Assessment__c = ea1.Id;
        insert br;*/
        test.startTest();
        Attachment__c attach=new Attachment__c();
        //attach.Email_File_Download_URL__c='pdf';
        //attach.File_Url__c='test';        
        attach.Attachment_Name__c='test';
        attach.Energy_Assessment__c = ea.Id;
        attach.Status__c='Completed';
        attach.Attachment_Type__c='attachType';
        attach.DSMTracker_Contact__c=dsmt.id;
        attach.Project__c=proj.id;
        //attach.Barrier__c=br.id;
        insert attach;
        
        /*Attachment att =new Attachment();
        att.Name='test';
        att.ParentId=attach.Id;
        insert att;*/

        String name = 'test';
        string obj = 'Inspection_Request__c';
        
        ApexPages.currentPage().getParameters().put('Id',proj.id);
        ApexPages.currentPage().getParameters().put('viewName',name);
        ApexPages.currentPage().getParameters().put('objectType',obj);
        ApexPages.currentPage().getParameters().put('attachType',attach.Attachment_Type__c);
        system.debug('attach Id '+attach.Id);
       
        ApexPages.currentPage().getParameters().put('attachmentId',attach.Id);
        
        dsmtPreviewAndEmailDocumentController cont = new dsmtPreviewAndEmailDocumentController();
        cont.signatureAttachmentType = '';
        cont.templateID = '';
        cont.query = '';
        cont.queryID = '';
        cont.ofn = '';
        cont.assessId = '';
        cont.proposalId = '';
        dsmtPreviewAndEmailDocumentController.saveSign(ir.Id, 'docname', 'imgURI', 'Certificate Of Inspection', 'Inspection_Request__c');
        dsmtPreviewAndEmailDocumentController.saveDualSign(ea.id, proposal.Id, 'imgURI1', 'imgURI2', 'attachType', 'Proposal__c', '06/06/2018', '06/06/2018');
        cont.checkCongaStatus();
        cont.Cancel();
        cont.SendEmail();
        cont.getdataForInspectionReportDocument();
        cont.redirectToCompleted();

        test.stopTest();
        /*String name1 = 'test';
        string obj1 = 'Barrier__c';
        
        ApexPages.currentPage().getParameters().put('Id',br.id);
        ApexPages.currentPage().getParameters().put('viewName',name1);
        ApexPages.currentPage().getParameters().put('objectType',obj1);
        ApexPages.currentPage().getParameters().put('attachType',attach.Attachment_Type__c);
        ApexPages.currentPage().getParameters().put('attachmentId',attach.id);
        
        dsmtPreviewAndEmailDocumentController cont1 = new dsmtPreviewAndEmailDocumentController();*/

    }
    
     public testmethod static void test2()
    {
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea = Datagenerator.setupAssessment();
       
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        test.startTest();
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Name='test';
        ir.Energy_Assessment__c=ea.id;
        ir.Contractor_Name__c=ta.id;
        //ir.Contractor_Name__c='test';
        insert ir;
        
        Proposal__c proposal =new Proposal__c();
        proposal.Name='test';
        proposal.Name_Type__c = 'Air Sealing';
        proposal.Energy_Assessment__c=ea.id;
        insert proposal;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Energy_Assessment__c = ea.Id;
        insert proj;
        
        /*Barrier__c br = new Barrier__c();
        br.Energy_Assessment__c = ea1.Id;
        insert br;*/
        
        Attachment__c attach=new Attachment__c();
        //attach.Email_File_Download_URL__c='pdf';
        //attach.File_Url__c='test';        
        attach.Attachment_Name__c='test';
        attach.Energy_Assessment__c = ea.Id;
        attach.Status__c='Completed';
        attach.Attachment_Type__c='attachType';
        attach.DSMTracker_Contact__c=dsmt.id;
        attach.Project__c=proj.id;
        //attach.Barrier__c=br.id;
        insert attach;
        
        /*Attachment att =new Attachment();
        att.Name='test';
        att.ParentId=attach.Id;
        insert att;*/

        String name = 'test';
        string obj = 'Inspection_Request__c';
        
        ApexPages.currentPage().getParameters().put('Id',proj.id);
        ApexPages.currentPage().getParameters().put('viewName',name);
        ApexPages.currentPage().getParameters().put('objectType',obj);
        ApexPages.currentPage().getParameters().put('attachType',attach.Attachment_Type__c);
        
        system.debug('attach Id '+attach.Id);
       
        ApexPages.currentPage().getParameters().put('attachmentId',attach.Id);
        
        dsmtPreviewAndEmailDocumentController cont = new dsmtPreviewAndEmailDocumentController();

        dsmtPreviewAndEmailDocumentController.saveDualSignCOI(ir.Id, 'imgURI1', 'imgURI2', 'Certificate Of Inspection', 'Inspection_Request__c');
        cont.checkCongaStatus();
        cont.Cancel();
        cont.SendEmail();
        cont.getdataForInspectionReportDocument();
        cont.redirectToCompleted();
        
        List<APXTConga4__Conga_Template__c> templates = new List<APXTConga4__Conga_Template__c>();
        APXTConga4__Conga_Template__c template = new APXTConga4__Conga_Template__c();
        template.Unique_Template_Name__c = 'Customer Service Survey Form';
        
        List<APXTConga4__Conga_Merge_Query__c> queries = new List<APXTConga4__Conga_Merge_Query__c>();
        APXTConga4__Conga_Merge_Query__c query1 = new APXTConga4__Conga_Merge_Query__c();
        query1.Unique_Conga_Query_Name__c = 'Customer Service Survey Query';
        
        insert query1;
        insert template;
        
        ApexPages.currentPage().getParameters().put('Id',ir.id);
        cont = new dsmtPreviewAndEmailDocumentController();
        cont.getDataforCustomerSurveyDocument();
        
        ApexPages.currentPage().getParameters().put('Id',proposal.id);
        cont = new dsmtPreviewAndEmailDocumentController();
        cont.getdataForSignContractDocument();
        
        ApexPages.currentPage().getParameters().put('objectType', 'Barrier__c');
        cont = new dsmtPreviewAndEmailDocumentController();
        
        ApexPages.currentPage().getParameters().put('objectType', 'Project__c');
        cont = new dsmtPreviewAndEmailDocumentController();
        test.stopTest();
    }
    
    public testmethod static void test3()
    {
        Energy_Assessment__c ea = Datagenerator.setupAssessment();
       
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        test.startTest();
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Name='test';
        ir.Energy_Assessment__c=ea.id;
        ir.Contractor_Name__c=ta.id;
        //ir.Contractor_Name__c='test';
        insert ir;
        
        Proposal__c proposal =new Proposal__c();
        proposal.Name='test';
        proposal.Name_Type__c = 'Air Sealing';
        proposal.Energy_Assessment__c=ea.id;
        insert proposal;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Energy_Assessment__c = ea.Id;
        proj.Preferred_Contractor__c=ta.id;
        insert proj;
        
        /*Barrier__c br = new Barrier__c();
        br.Energy_Assessment__c = ea1.Id;
        insert br;*/
        
        Attachment__c attach=new Attachment__c();
        //attach.Email_File_Download_URL__c='pdf';
        //attach.File_Url__c='test';        
        attach.Attachment_Name__c='test';
        attach.Energy_Assessment__c = ea.Id;
        attach.Status__c='Completed';
        attach.Attachment_Type__c='attachType';
        attach.DSMTracker_Contact__c=dsmt.id;
        attach.Project__c=proj.id;
        //attach.Barrier__c=br.id;
        insert attach;
        
        /*Attachment att =new Attachment();
        att.Name='test';
        att.ParentId=attach.Id;
        insert att;*/

        String name = 'test';
        string obj = 'Inspection_Request__c';
        
        ApexPages.currentPage().getParameters().put('Id',proj.id);
        ApexPages.currentPage().getParameters().put('viewName',name);
        ApexPages.currentPage().getParameters().put('objectType',obj);
        ApexPages.currentPage().getParameters().put('attachType',attach.Attachment_Type__c);
        
        system.debug('attach Id '+attach.Id);
       
        ApexPages.currentPage().getParameters().put('attachmentId',attach.Id);
        
        dsmtPreviewAndEmailDocumentController cont = new dsmtPreviewAndEmailDocumentController();
        
        ApexPages.currentPage().getParameters().put('objectType', 'Proposal__c');
        cont = new dsmtPreviewAndEmailDocumentController();
        
        test.stopTest();
    }
}