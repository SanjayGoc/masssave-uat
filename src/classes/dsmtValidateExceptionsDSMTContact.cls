public class dsmtValidateExceptionsDSMTContact
{
   public static boolean isStopValidateException = false;
   public static void InsertExceptionsOnDSMT(List<DSMTracker_Contact__c> dsmtlist){
       
        Map<String,Exception_Template__c> mapExt = new Map<String,Exception_Template__c>();     
        //Gets all expection with that check box checked.
        for(Exception_Template__c ext : [SELECT id,Online_Application_Section__c,Name, Reference_ID__c,Attachment_Needed__c,Required_Attachment_Type__c, 
                                            Outbound_Message__c,Internal__c, Exception_Message__c,Short_Rejection_Reason__c ,OwnerId,Type__c,Object_Sub_Type__c FROM Exception_Template__c  
                                        WHERE Automatic__c = true and Active__c = true and DSMTracker_Contact_Level_Message__c = true]) {
          mapExt.put(ext.Reference_Id__c,ext);
        } 
        system.debug('--mapExt--'+mapExt); 
       
        Set<Id> dsmtids = new Set<id>();
        for(DSMTracker_Contact__c dsmt : dsmtlist){
             dsmtids.add(dsmt.id);   
        }
        
        Map<String,Exception__c> mapExceptions = new Map<String,Exception__c>();
        for(Exception__c Ex :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c, DSMTracker_Contact__c,DSMTracker_Contact__r.id FROM Exception__c 
                                WHERE DSMTracker_Contact__r.Id IN: dsmtids AND Disposition__c  != 'Resolved' ]){    
            mapExceptions.put(Ex.DSMTracker_Contact__c+'-'+Ex.Exception_Template__r.Reference_ID__c,Ex);     
        }      
        system.debug('--mapExceptions--'+mapExceptions);
        
        List<Exception__c> ExceptionsCreated = new List<Exception__c>();
          
        for(DSMTracker_Contact__c dsmtNew : dsmtlist) {
            
            List<string> AutoExceptionsList = new List<string>();
            system.debug('--dsmtNew--'+dsmtNew);
            
            for(String refId : mapExt.keyset()){
                Exception_Template__c temp = mapExt.get(refId);
                
                if(dsmtNew.Trade_Ally_Type__c != null && temp != null && temp.Object_Sub_Type__c != null && temp.Object_Sub_Type__c.contains(dsmtNew.Trade_Ally_Type__c)){
                      
                     if(refId == 'ETN-0000011' && dsmtNew.Title_Formula__c == 'Crew Chief' && dsmtNew.Personal_Lead_Safe_Attached__c == false){
                        If(mapExceptions.get(dsmtNew.id+'-ETN-0000011')==null) 
                            AutoExceptionsList.add('ETN-0000011'); //Personal Lead Safe
                     }
                     
                     if(refId == 'ETN-0000012' && dsmtNew.Title_Formula__c == 'Crew Chief' && dsmtNew.Crew_Chief_Certification_Attached__c == false){
                        If(mapExceptions.get(dsmtNew.id+'-ETN-0000012')==null) 
                            AutoExceptionsList.add('ETN-0000012'); //Crew Chief Certification
                     }
                     
                     if(refId == 'ETN-0000013' && dsmtNew.Title_Formula__c == 'Energy specialist' && dsmtNew.BPI_Building_Analyst_Attached__c == false){
                        If(mapExceptions.get(dsmtNew.id+'-ETN-0000013')==null) 
                            AutoExceptionsList.add('ETN-0000013'); //BPI Building Analyst
                     }
                     
                     if(refId == 'ETN-0000014' && dsmtNew.BPI_Envelope_Attached__c == false){
                        If(mapExceptions.get(dsmtNew.id+'-ETN-0000014')==null) 
                            AutoExceptionsList.add('ETN-0000014'); //BPI Envelope
                     }
                     
                     if(refId == 'ETN-0000015' && dsmtNew.CSL_Attached__c == false){
                        If(mapExceptions.get(dsmtNew.id+'-ETN-0000015')==null) 
                            AutoExceptionsList.add('ETN-0000015'); //CSL
                     }
                     
                     
               }
                
           }
            
            
            system.debug('--AutoExceptionsList--'+AutoExceptionsList);
            for(string autolist :AutoExceptionsList){
                if(mapExt.get(autolist) !=null) {
                    Exception_Template__c Exceptiontemp = mapExt.get(autolist);
                    Exception__c ExceptionstoAdd = new Exception__c(); 
                    ExceptionstoAdd.Internal__c = Exceptiontemp.Internal__c; 
                    ExceptionstoAdd.Exception_Message__c= Exceptiontemp.Exception_Message__c; 
                    ExceptionstoAdd.Outbound_Message__c=Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Dsmtracker_Contact__c =  dsmtNew.Id;
                    ExceptionstoAdd.Exception_Template__c = Exceptiontemp.id;
                    ExceptionstoAdd.FInal_Outbound_Message__c = Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Short_Rejection_Reason__c = Exceptiontemp.Short_Rejection_Reason__c ;
                    ExceptionstoAdd.Automated__c = true;
                    ExceptionstoAdd.OwnerId =dsmtNew.OwnerId;
                    ExceptionstoAdd.Attachment_Needed__c = Exceptiontemp.Attachment_Needed__c;
                    ExceptionstoAdd.Required_Attachment_Type__c = Exceptiontemp.Required_Attachment_Type__c;
                    ExceptionstoAdd.Online_Application_Section__c = Exceptiontemp.Online_Application_Section__c;
                    ExceptionstoAdd.Type__c = Exceptiontemp.Type__c;
                    ExceptionsCreated.add(ExceptionstoAdd);    
                }
            }     
       }
        
        system.debug('--ExceptionsCreated--'+ExceptionsCreated);
        dsmtValidateExceptionsDSMTContact.isStopValidateException = true;
        if(ExceptionsCreated.size()>0){
           insert ExceptionsCreated;  
        }   
   }
   
   public static void CheckUpdateExceptions(List<DSMTracker_Contact__c> dsmtlist , Map<Id,DSMTracker_Contact__c> oldmap){
        Map<string,List<Exception__c>> ExceptionsMap = new Map<string,List<Exception__c>>();
        for(Exception__c Exceptions :[SELECT id,name,Exception_Message__c , Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c, DSMTracker_Contact__c,DSMTracker_Contact__r.id FROM Exception__c 
                                            WHERE DSMTracker_Contact__r.Id=: oldmap.keyset() AND Disposition__c  != 'Resolved' ]){
                List<Exception__c> lsExceptions = ExceptionsMap.get(Exceptions.DSMTracker_Contact__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c);
                if(lsExceptions  == NULL)
                    lsExceptions  = new List<Exception__c>();
                lsExceptions.add(Exceptions );
                ExceptionsMap.put(Exceptions.DSMTracker_Contact__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c,lsExceptions);        
        }
        
         List<Exception__c> ExceptionstoUpdate = new List<Exception__c>();
         Set<Id> dsmtId = new Set<Id>();
         for(DSMTracker_Contact__c dsmt : dsmtlist){
              dsmtId.add(dsmt.Id);        
         }
         
         system.debug('--ExceptionsMap--'+ExceptionsMap);
         
         for(DSMTracker_Contact__c dsmt : dsmtlist){
                
                system.debug('--dsmt.Title_Formula__c --'+dsmt.Title_Formula__c );    
                String keyValue = dsmt.id + '-ETN-0000011'; //Personal Lead Safe
                if(ExceptionsMap.containskey(keyValue)){
                     if(dsmt.Personal_Lead_Safe_Attached__c == true || dsmt.Title_Formula__c != 'Crew Chief'){
                        for(Exception__c exp : ExceptionsMap.get(keyValue)){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(dsmt.id + '-ETN-0000012')){  //crew chief certification
                     if(dsmt.Crew_Chief_Certification_Attached__c== true || dsmt.Title_Formula__c != 'Crew Chief'){
                        for(Exception__c exp : ExceptionsMap.get(dsmt.id + '-ETN-0000012')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(dsmt.id + '-ETN-0000013')){ //BPI Building Analyst
                     if(dsmt.BPI_Building_Analyst_Attached__c == true || dsmt.Title_Formula__c != 'Energy specialist'){
                        for(Exception__c exp : ExceptionsMap.get(dsmt.id + '-ETN-0000013')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(dsmt.id + '-ETN-0000014')){ //BPI Envelope
                     if(dsmt.BPI_Envelope_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(dsmt.id + '-ETN-0000014')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(dsmt.id + '-ETN-0000015')){ //CSL
                     if(dsmt.CSL_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(dsmt.id + '-ETN-0000015')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
              
                
         }
         
          dsmtValidateExceptionsDSMTContact.isStopValidateException = true;
            system.debug('--ExceptionstoUpdate--'+ExceptionstoUpdate);
            if(ExceptionstoUpdate.size()>0){
                update ExceptionstoUpdate;
            }
   }
}