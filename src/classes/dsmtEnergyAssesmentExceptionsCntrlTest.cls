@isTest
public with sharing class dsmtEnergyAssesmentExceptionsCntrlTest
{
    
    public static testmethod void test()
    {   
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
         test.startTest();
        Recommendation_Scenario__c pro = new Recommendation_Scenario__c();
        pro.Status__c='Installed';
        pro.Moderate_Income__c = true;
        pro.Duplex_Triple_Decker__c = true;
        pro.No_Cap__c = true;
        pro.Override_Special_Incentive__c = false;
        pro.Energy_Assessment__c = ea.id;
        insert pro;
        
        Recommendation__c r = new Recommendation__c();
        r.Energy_Assessment__c = ea.id;
        r.Recommendation_Scenario__c = pro.id;
        r.Status__c='Installed';
        r.Total_Rec_Inc_After_Special_Incentive__c = 1.00;
        r.Total_Recommended_Incentive__c = 1.00;
        r.Incentive__c = 1.00;
        insert r;

        Barrier__c bar = new Barrier__c();
        bar.Type__c='Type2';
        insert bar;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ea);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(r);
        ApexPages.StandardController sc2 = new ApexPages.StandardController(pro);
        ApexPages.StandardController sc5 = new ApexPages.StandardController(bar);

       

        Exception_Template__c emailTemp= new Exception_Template__c(Object_Sub_Type__c='HPC', Reference_ID__c='ETN-0000003', Automatic__c = true, Active__c = true,
                                                                   Registration_Request_Level_Message__c= true);
        insert emailTemp;

        
        Exception__c  exe =new Exception__c ();
        exe.Energy_Assessment__c=ea.id;
        exe.Override_Exception__c=false;
        exe.Exception_Template__c = emailTemp.id;
        insert exe;
        
        dsmtEnergyAssesmentExceptionsController cntrl = new dsmtEnergyAssesmentExceptionsController(sc);      
        cntrl.HideException = true;
        apexpages.currentpage().getparameters().put('id',ea.id);
        cntrl.overrideAllExceptions();
        cntrl.getAllExistingExceptions();

        exe = new Exception__c ();
        exe.Recommendation__c=r.id;
        exe.Override_Exception__c=false;
        exe.Exception_Template__c = emailTemp.id;
        insert exe;
        
        dsmtEnergyAssesmentExceptionsController cntrl1 = new dsmtEnergyAssesmentExceptionsController(sc1);
        cntrl1.HideException = true;
        apexpages.currentpage().getparameters().put('id',r.id);
        cntrl1.overrideAllExceptions();
        cntrl1.getAllExistingExceptions();
        
        exe =new Exception__c ();
        exe.Project__c=pro.id;
        exe.Override_Exception__c=false;
        exe.Exception_Template__c = emailTemp.id;
        insert exe;
        
        dsmtEnergyAssesmentExceptionsController cntrl2= new dsmtEnergyAssesmentExceptionsController(sc2);
        cntrl2.HideException = true;
        apexpages.currentpage().getparameters().put('id',pro.id);
        cntrl2.overrideAllExceptions();
        cntrl2.getAllExistingExceptions();
        
        exe =new Exception__c ();
        exe.Barrier__c=bar.id;
        exe.Override_Exception__c=false;
        exe.Exception_Template__c = emailTemp.id;
        insert exe;
        
        dsmtEnergyAssesmentExceptionsController cntrl3 = new dsmtEnergyAssesmentExceptionsController(sc5);
        cntrl3.HideException = true;
        apexpages.currentpage().getparameters().put('id',bar.id);
        cntrl3.overrideAllExceptions();
        cntrl3.getAllExistingExceptions();
        
        test.stopTest();
    }
    public static testmethod void runTest2()
    {
        Account acc = new Account(name= 'test');
        insert acc;
        
        Invoice__c invobj = new Invoice__c();
        invobj.invoice_date__c = date.Today();
        invobj.Account__c = acc.Id;
        invobj.Status__c='Approved';
        invobj.Invoice_Type__c='NSTAR413';
        insert invObj;
    
        /*Invoice_Line_Item__c ili = new Invoice_Line_Item__c();
        ili.Invoice__c=invobj.id;
        ili.QTY__c=5;
        ili.Program_Price__c=5;
        ili.Fuel_Type__c='Electric';
        insert ili;*/

        Inspection_Request__c IR = new Inspection_Request__c();
        ir.Status__c='Completed';
        insert IR;
        
        Inspection_Line_Item__c ILT= new Inspection_Line_Item__c();
        ILT.Inspection_Request__c=IR.ID;
        insert ILT;
        
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Trade_Ally_Type__c='HPC';
        update Tacc;
        
        Review__c rev = new Review__c(Email__c='test@test.com',Trade_Ally_Account__c=TAcc.id,Status__c = 'Approved', Requested_By__c = Userinfo.getuserid(),Review_Type__c='Document');
        insert rev;

        ApexPages.StandardController sc4 = new ApexPages.StandardController(invObj);
        //ApexPages.StandardController sc5 = new ApexPages.StandardController(ili);
        ApexPages.StandardController sc6 = new ApexPages.StandardController(IR);
        ApexPages.StandardController sc7 = new ApexPages.StandardController(ILT);
        ApexPages.StandardController sc8 = new ApexPages.StandardController(rev);
        
        
        test.startTest();
        
        Exception_Template__c emailTemp= new Exception_Template__c(Object_Sub_Type__c='HPC', Reference_ID__c='ETN-0000003', Automatic__c = true, Active__c = true,
                                                                   Registration_Request_Level_Message__c= true);
        insert emailTemp;
        
        Exception__c  exe =new Exception__c ();
        exe.invoice__c=invobj.id;
        exe.Override_Exception__c=false;
        exe.Exception_Template__c = emailTemp.id;
        insert exe;
        
        dsmtEnergyAssesmentExceptionsController cntrl4 = new dsmtEnergyAssesmentExceptionsController(sc4); 
        cntrl4.HideException = true;
        apexpages.currentpage().getparameters().put('id',invObj.id);
        cntrl4.overrideAllExceptions();
        cntrl4.getAllExistingExceptions();
        //dsmtEnergyAssesmentExceptionsController cntrl5 = new dsmtEnergyAssesmentExceptionsController(sc5);

        exe =new Exception__c ();
        exe.invoice__c=invobj.id;
        exe.Override_Exception__c=false;
        exe.Exception_Template__c = emailTemp.id;
        insert exe;
        
        dsmtEnergyAssesmentExceptionsController cntrl6 = new dsmtEnergyAssesmentExceptionsController(sc6);
        cntrl6.HideException = true;
        apexpages.currentpage().getparameters().put('id',IR.id);
        cntrl6.overrideAllExceptions();
        cntrl6.getAllExistingExceptions();

        dsmtEnergyAssesmentExceptionsController cntrl7 = new dsmtEnergyAssesmentExceptionsController(sc7);
        cntrl7.HideException = true;
        apexpages.currentpage().getparameters().put('id',ILT.id);
        cntrl7.overrideAllExceptions();
        cntrl7.getAllExistingExceptions();

        exe = new Exception__c ();
        exe.review__c=rev.id;
        exe.Override_Exception__c=false;
        exe.Exception_Template__c = emailTemp.id;
        insert exe;
        
        dsmtEnergyAssesmentExceptionsController cntrl8 = new dsmtEnergyAssesmentExceptionsController(sc8);
        cntrl8.HideException = true;
        apexpages.currentpage().getparameters().put('id',rev.id);
        cntrl8.overrideAllExceptions();
        cntrl8.getAllExistingExceptions();
        test.stopTest();
    }
    public static testmethod void runTest3()
    {
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Exception__c  exe =new Exception__c ();
        exe.Energy_Assessment__c=ea.id;
        exe.Override_Exception__c=false;
        exe.Type__c='Insurance';
        insert exe;

        Exception_Template__c emailTemp= new Exception_Template__c(Object_Sub_Type__c='HPC', Reference_ID__c='ETN-0000003', Automatic__c = true, Active__c = true,
                                                                   Registration_Request_Level_Message__c= true);
        insert emailTemp;
        
        test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(ea);
        dsmtEnergyAssesmentExceptionsController cntrl4 = new dsmtEnergyAssesmentExceptionsController(sc);
        
        cntrl4.overrideAllExceptions();      
        cntrl4.expId = exe.id;
        cntrl4.type = 'Override';
        //cntrl4.type = 'Manual';
        cntrl4.vlu = 'true';
        cntrl4.saveExp();
        cntrl4.getTypeOptions();
        cntrl4.getStatusOptions();
        cntrl4.savedString = 'IDcolumnTestcolumntestcolumntestcolumntest';
        cntrl4.save();
        cntrl4.getDespostionOptions();
        cntrl4.getUserOptions();
        cntrl4.expTempId = emailTemp.id;
        cntrl4.popexpmsg = 'test';
        cntrl4.saveExceptionPopup();
        cntrl4.newExpId = new List<String>();
        cntrl4.selectedStandardExcpetion = 'test';
        cntrl4.lstaddException = new List<Exception__c>();
        cntrl4.lstaddException.add(exe);
        test.stopTest();
    }    
}