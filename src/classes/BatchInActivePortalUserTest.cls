/*
	Date         Author             Story #     Description
	---------------------------------------------------------------------
    2018-01-03   stephan.spiegel    DSST-544    Rewrite for functional tests with assertions 

*/

@isTest
public class BatchInActivePortalUserTest {

    @isTest
    static void itShouldDeactivatePortalUser() {
        User testUser;
        User currentUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        System.runAs(currentUser){// runAs to avoid MIXED_DML_OPERATION on User update 
            testUser = Datagenerator.CreatePortalUser();
            testUser.isActive = true;
            update testUser;
        }
        Test.startTest();
        BatchInActivePortalUser deactivationScript = new BatchInActivePortalUser();
        deactivationScript.start(null);
        deactivationScript.execute(null, new List<User>{testUser});
        deactivationScript.finish(null);
        Test.stopTest();
        testuser = [SELECT isActive FROM User WHERE Id = :testUser.Id LIMIT 1];
        System.AssertEquals(false, testUser.isActive, 'BatchInActivePortalUser should deactive User who has not recently logged in or made changes');
    }

    @isTest
    static void itShouldNotDeactivatePortalUserWithRecentActivity() {
        User testUser;
        User currentUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        System.runAs(currentUser){// runAs to avoid MIXED_DML_OPERATION on User update 
            testUser = Datagenerator.CreatePortalUser();
            testUser.isActive = true;
            update testUser;
        }
        System.runAs(testUser){
            insert new WorkOrder__c();
        }
        Test.startTest();
        BatchInActivePortalUser deactivationScript = new BatchInActivePortalUser();
        deactivationScript.start(null);
        deactivationScript.execute(null, new List<User>{testUser});
        Test.stopTest();
        testUser = [SELECT isActive FROM User WHERE Id = :testUser.Id LIMIT 1];
        System.AssertEquals(true, testUser.isActive, 'BatchInActivePortalUser should not deactivate User who has recently made changes');
    }

    @isTest
    static void itShouldNotDeactivatePortalUserWhoRecentlyLoggedIn() {
        User testUser;
        User currentUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1];
        System.runAs(currentUser){// runAs to avoid MIXED_DML_OPERATION on User update 
            testUser = Datagenerator.CreatePortalUser();
            testUser.isActive = true;
            update testUser;
        }
        BatchInActivePortalUser deactivationScript = new BatchInActivePortalUser();
        deactivationScript.start(null);
        deactivationScript.recentlyLoggedInUserIds = new Set<Id>{testUser.Id};
        Test.startTest();
        deactivationScript.execute(null, new List<User>{testUser});
        Test.stopTest();
        testUser = [SELECT isActive FROM User WHERE Id = :testUser.Id LIMIT 1];
        System.assertEquals(true, testUser.isActive, 'BatchInActivePortalUser should not deactivate User who has recently logged in');
    }

    @isTest
    static void itShouldLogExceptions() {
        User testUser = new User();
        BatchInActivePortalUser deactivationScript = new BatchInActivePortalUser();
        deactivationScript.start(null);
        Test.startTest();
        deactivationScript.execute(null, new List<User>{testUser}); // pass user without Id to trigger DML exception
        deactivationScript.finish(null);
        Test.stopTest();
        System.assertEquals('\ninvalid ID field: null', deactivationScript.errorsForLog, 'BatchInActivePortalUser should log any exceptions');
    }
}