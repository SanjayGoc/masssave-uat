public class dsmtdsmtAirFlowInfoController extends dsmtEnergyAssessmentBaseController{
    
    /* BLOWER DOOR READING */
    public Blower_Door_Reading__c newBlowerDoorReading{get{
        if(this.newBlowerDoorReading == null){
            this.newBlowerDoorReading = new Blower_Door_Reading__c();
        }
        return this.newBlowerDoorReading;
    }set;}
    
    public List<Blower_Door_Reading__c> getBlowerDoorReadings(){
        if(this.afal != null && this.afal.Id != null){
            return [
                SELECT Id, Name, Blower_Door_Test_Date__c, Ending_CFM50__c, Notes__c, Starting_CFM50__c,Time_Spent__c, Recommendation__c
                FROM Blower_Door_Reading__c
                WHERE 
                    Air_Flow_and_Air_Leakage__c =: afal.Id AND 
                    Energy_Assessment__c =: ea.Id AND 
                    Building_Specification__c =: bs.Id
            ];
        }else{
            return new List<Blower_Door_Reading__c>();   
        }
    }
    
    public void saveAirFlowAirLeakageDetail(){
        if(afal != null && afal.Id != null){
            UPDATE afal;
            
            if(this.newPressureDifferentialReading != null && 
              (this.newPressureDifferentialReading.Date__c != null ||
               checkNull(this.newPressureDifferentialReading.Location__c) ||
               checkNull(this.newPressureDifferentialReading.WRT__c) ||
               this.newPressureDifferentialReading.Pre_Retrofit_Pressure__c != null ||
               this.newPressureDifferentialReading.Post_Retrofit_Pressure__c != null ||
               checkNull(this.newPressureDifferentialReading.Notes__c)))
            {
                savePressureDifferentialReading();
            }
            
            if(this.newBlowerDoorReading != null && 
              (this.newBlowerDoorReading.Blower_Door_Test_Date__c != null || 
               this.newBlowerDoorReading.Starting_CFM50__c > 0 || 
               this.newBlowerDoorReading.Ending_CFM50__c > 0 || 
               checkNull(this.newBlowerDoorReading.Notes__c) || 
               checkNull(this.newBlowerDoorReading.Recommendation__c)))
            {
                saveBlowerDoorReading();
            }
                    
            String assessId = getParam(KEY_ASSESSMENT_ID);
            loadBuildingSpecificationData(assessId);
            loadAirFlowAirLeakageData(afal.Building_Specifications__c);
        }
    }
    
    public boolean checkNull(String str)
    {
        if(str == null || str.trim().length() == 0)
            return false;
            
        return true;
    }
    
    public void saveBlowerDoorReading(){
        System.debug('saveBlowerDoorReading => ' + newBlowerDoorreading);
        if(this.newBlowerDoorreading != null){
            this.newBlowerDoorReading.Air_Flow_and_Air_Leakage__c = afal.Id;
            this.newBlowerDoorReading.Energy_Assessment__c = ea.Id;
            this.newBlowerDoorReading.Building_Specification__c = bs.Id;
            if(!isValidSalesforceId(this.newBlowerDoorReading.Id,Blower_Door_Reading__c.Class)){
                this.newBlowerDoorReading.Id = null;
            }
            boolean updateAFAL = false;
            Decimal nextNumber = (afal.Next_Blower_Door_Reading_Number__c != null) ? afal.Next_Blower_Door_Reading_Number__c : 1;
            if(this.newBlowerDoorReading.Id == null){
                this.newBlowerDoorReading.Name = 'Blower Door Reading ' + nextNumber;
                updateAFAL = true;
            }
            UPSERT this.newBlowerDoorReading;
            if(updateAFAL){
                afal.Next_Blower_Door_Reading_Number__c = (nextNumber + 1);
                UPDATE afal;
            }
            this.newBlowerDoorReading = null;
            loadAirFlowAirLeakageData(afal.Building_Specifications__c);
        }
    }
    
    public void deleteBlowerDoorReading(){
        String blowerDoorReadingId = getParam('blowerDoorReadingId');
        if(blowerDoorReadingId != null){
            delete new Blower_Door_Reading__c(Id = blowerDoorReadingId);
            loadAirFlowAirLeakageData(afal.Building_Specifications__c);
        }
    }
    public void editBlowerDoorReading(){
        String blowerDoorReadingIdToEdit = getParam('blowerDoorReadingIdToEdit');
        if(blowerDoorReadingIdToEdit != null && blowerDoorReadingIdToEdit.trim() != ''){
            List<Blower_Door_Reading__c> result = [
                SELECT Id, Name, Blower_Door_Test_Date__c, Ending_CFM50__c, Notes__c, Starting_CFM50__c,Time_Spent__c, Recommendation__c
                FROM Blower_Door_Reading__c
                WHERE Id =:blowerDoorReadingIdToEdit
            ];
            if(result.size() > 0){
                this.newBlowerDoorReading = result.get(0);
            }
        }
    }
        
    /* PRESSURE DIFFERENTIAL READING */
    public Pressure_Differential_Reading__c newPressureDifferentialReading{get{
        if(this.newPressureDifferentialReading == null){
            this.newPressureDifferentialReading = new Pressure_Differential_Reading__c();
        }
        return this.newPressureDifferentialReading;
    }set;}
    
    public List<Pressure_Differential_Reading__c> getPressureDifferentialReadings(){
        if(this.afal != null && this.afal.Id != null){
            return [
                SELECT Id, Name, Date__c, Location__c, WRT__c, Pre_Retrofit_Pressure__c,Post_Retrofit_Pressure__c, Notes__c, Recommendation__c
                FROM Pressure_Differential_Reading__c
                WHERE 
                    Air_Flow_and_Air_Leakage__c =: afal.Id AND
                    Energy_Assessment__c =: ea.Id AND 
                    Building_Specification__c =: bs.Id
            ];
        }else{
            return new List<Pressure_Differential_Reading__c>();   
        }
    }
    
    public void savePressureDifferentialReading(){
        if(this.newPressureDifferentialReading != null){
        
            this.newPressureDifferentialReading.Air_Flow_and_Air_Leakage__c = afal.Id;
            this.newPressureDifferentialReading.Energy_Assessment__c = ea.Id;
            this.newPressureDifferentialReading.Building_Specification__c = bs.Id;
            
            if(!isValidSalesforceId(this.newPressureDifferentialReading.Id,Pressure_Differential_Reading__c.Class)){
                this.newPressureDifferentialReading.Id = null;
            }
            boolean updateAFAL = false;
            Decimal nextNumber = (afal.Next_Pressure_Differential_Reading_Numbe__c != null) ? afal.Next_Pressure_Differential_Reading_Numbe__c: 1;
            if(this.newPressureDifferentialReading.Id == null){
                this.newPressureDifferentialReading.Name = 'Pressure differential Reading ' + nextNumber;
                updateAFAL = true;
            }
            
            UPSERT this.newPressureDifferentialReading;
            system.debug('newPressureDifferentialReading :::::' + newPressureDifferentialReading.Id);
            
            if(updateAFAL){
                afal.Next_Pressure_Differential_Reading_Numbe__c = (nextNumber + 1);
                UPDATE afal;
            }
            this.newPressureDifferentialReading = null;
            loadAirFlowAirLeakageData(afal.Building_Specifications__c);
        }
    }
    
    public void deletePressureDifferentialReading(){
        String pressureDifferentialReadingId = getParam('pressureDifferentialReadingId');
        if(pressureDifferentialReadingId != null){
            delete new Pressure_Differential_Reading__c(Id = pressureDifferentialReadingId);
            loadAirFlowAirLeakageData(afal.Building_Specifications__c);
        }
    }
    public void editPressureDifferentialReading(){
        String pressureDifferentialReadingIdToEdit = getParam('pressureDifferentialReadingIdToEdit');
        if(pressureDifferentialReadingIdToEdit != null && pressureDifferentialReadingIdToEdit.trim() != ''){
            List<Pressure_Differential_Reading__c> result = [
                SELECT Id, Name, Date__c, Location__c, WRT__c, Pre_Retrofit_Pressure__c,Post_Retrofit_Pressure__c, Notes__c, Recommendation__c
                FROM Pressure_Differential_Reading__c
                WHERE Id =:pressureDifferentialReadingIdToEdit
            ];
            if(result.size() > 0){
                this.newPressureDifferentialReading = result.get(0);
            }
        }
    }
    
    /* RECOMMENDATION */
    public Recommendation__c newRecommendation{get{
        if(this.newRecommendation == null){
            this.newRecommendation = new Recommendation__c();
        }
        return this.newRecommendation;
    }set;}
    
    public List<Recommendation__c> getRecommendations(){
        if(this.afal != null && this.afal.Id != null){
            return [
                SELECT Id, Name, Recommendation__c, Quantity__c, Hours__c, Status__c, User_Description__c, Recommendation_Description__c
                FROM Recommendation__c
                WHERE 
                    Air_Flow_and_Air_Leakage__c =: afal.Id AND 
                    Air_Flow_and_Air_Leakage__c != null AND
                    Subcategory__c = 'Airflow' AND
                    Category__c = 'Air Sealing' AND
                    Energy_Assessment__c =: ea.Id //AND Building_Specification__c =: bs.Id
            ];
        }else{
            return new List<Recommendation__c>();   
        }
    }
    
    public void saveRecommendation(){
        if(this.newRecommendation != null){
            this.newRecommendation.Air_Flow_and_Air_Leakage__c = afal.Id;
            this.newRecommendation.Energy_Assessment__c = ea.Id;
            this.newRecommendation.Building_Specification__c = bs.Id;
            if(!isValidSalesforceId(this.newRecommendation.Id,Recommendation__c.Class)){
                this.newRecommendation.Id = null;
            }
            UPSERT this.newRecommendation;
            this.newRecommendation = null;
        }
    }
    public void deleteRecommendation(){
        String recommendationId = getParam('recommendationId');
        if(recommendationId != null)
            delete new Recommendation__c(Id = recommendationId);
    }
    public void editRecommendation(){
        String recommendationIdToEdit = getParam('recommendationIdToEdit');
        if(recommendationIdToEdit != null && recommendationIdToEdit.trim() != ''){
            List<Recommendation__c> result = [
                SELECT Id, Name, Recommendation__c, Quantity__c, Hours__c, Status__c, User_Description__c
                FROM Recommendation__c
                WHERE Id =:recommendationIdToEdit
            ];
            if(result.size() > 0){
                this.newRecommendation = result.get(0);
            }
        }
    }
    public List<SelectOption> getRecommendationsSelectOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- None --'));
        for(Recommendation__c r : getRecommendations()){
            //options.add(new SelectOption(r.Id,r.Recommendation__c));
            options.add(new SelectOption(r.Id,r.Recommendation_Description__c));
        }
        options.sort();
        return options;
    }
    
    /* NOTES & ATTACHMENTS */
    public List<Note> getNotes(){
        if(this.afal != null && this.afal.Id != null){
            return loadNotes(this.afal.Id);  
        }else{
            return new List<Note>();   
        }
    }
    public List<Attachment> getAttachments(){
        if(this.afal != null && this.afal.Id != null){
            return loadAttachments(this.afal.Id);  
        }else{
            return new List<Attachment>();   
        }
    }
}