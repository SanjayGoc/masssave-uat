@isTest
private class dsmtAirFlowHelperTest {
	
	@isTest static void test1() 
	{

		Energy_Assessment__c ea = dsmtEAModel.setupAssessment();

		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		Test.startTest();

		dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		if(bp.ai !=null)
		{
			bp.ai.ActualCFM50__c = 3000;
			update bp.ai;

			dsmtAirFlowHelper.GetNewACH(bp);
			dsmtAirFlowHelper.GetNewCFM50(bp);
		}
		else 
		{
			Air_Flow_and_Air_Leakage__c ai = new Air_Flow_and_Air_Leakage__c();
			ai.Energy_Assessment__c = ea.Id;

			insert ai;
		}


		Test.stopTest();
	}
	
	
	
}