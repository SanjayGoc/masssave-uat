public class QueueClass1 implements Queueable {
    List<Available_Slot__c> lstASlot;
    Set<Id> empId = new Set<Id>();
    
    public QueueClass1(List<Available_Slot__c > lstASlot) {
        this.lstASlot = lstASlot;
        
        
    }
    public void execute(QueueableContext context) {
        upsert lstASlot;
        
        Set<Id> asId = new Set<Id>();
        
        for(Available_Slot__c newas : lstASlot){
            //empId.add(newas.
            asId.add(newas.Id);
        }
        
        List<Available_Slot__c> newasLst = [select id,Work_Team_Member__r.Employee__c from Available_Slot__c  where id in : asId];
        
        for(Available_Slot__c newas : newasLst){
            empId.add(newas.Work_Team_Member__r.Employee__c);
        }
        
        List<Possible_Workorder_Types__c> lstPSW = new List<Possible_Workorder_Types__c>();
        Set<Id> woTypeIdLst = new Set<Id>();
        Map<Id,Workorder_Type__c> mapWoType = new Map<Id,Workorder_Type__c>();
        
        if(lstASlot.size()>0){
            Decimal estLimit = 120;
            for(Workorder_Type__c w: [select Id,Est_Total_Time__c,Friendly_Name__c from Workorder_Type__c where Est_Total_Time__c <= :estLimit ]){
                woTypeIdLst.add(w.Id);
                mapWoType.put(w.Id,w);
                
            }
            
             Map<Id,Boolean> mapWorkorderTypePos = new Map<Id,Boolean>();
             
             Map<Id,List<Required_Skill__c>> WTypeReqSkill = new Map<Id,List<Required_Skill__c>>();
             
             if(woTypeIdLst != null && woTypeIdLst.size() > 0){
           
                list<Required_Skill__c> rsLst = [select id, Skill__c,Skill__r.Name,Minimum_Score__c,Workorder_Type__c from Required_Skill__c where Workorder_Type__c In :woTypeIdLst ];
                
                set<string> skillIds = new set<string>();
                
                List<Required_Skill__c> tempType = null;
                
                for(Required_Skill__c r : rsLst){
                    tempType = WTypeReqSkill.get(r.Workorder_Type__c);
                    if(tempType == null){
                        tempType = new List<Required_Skill__c>();
                        tempType.add(r);
                        WTypeReqSkill.put(r.Workorder_Type__c,tempType);   
                    }else{
                        tempType.add(r);
                        WTypeReqSkill.put(r.Workorder_Type__c,tempType);   
                    }
                    skillIds.add(r.Skill__c);
                }
                
                system.debug('--skillIds---'+skillIds);
                
                list<Employee_Skill__c> esLst = [select id, Skill__c,Employee__c,Employee__r.Name,Survey_Score__c from 
                                                        Employee_Skill__c where skill__c in : skillIds
                                                        and Employee__c in : empId];
        
                
                if(esLst  != null && esLst.size() > 0){
                    Map<Id,list<Employee_Skill__c>> esMap = new Map<Id,list<Employee_Skill__c>>();
                    list<Employee_Skill__c> temp = null;
                    
                    for(Employee_Skill__c  es : esLst){
                            
                            if(esMap.get(es.Employee__c) != null){
                                temp = esMap.get(es.Employee__c);
                                temp.add(es);
                                esmap.put(es.Employee__c,temp);
                            }else{
                                temp = new list<Employee_Skill__c>();
                                temp.add(es);
                                esmap.put(es.Employee__c,temp);
                            }
                    }
                    system.debug('--esmap--'+esmap);
                    Set<Id> empSkill = new Set<Id>();
                    boolean add = false;
                    Set<Id> empIdset = new Set<Id>();
                    boolean reqadd = false;
                    string strEmpId ='';
                    map<string, decimal> empSkillRateMap = new map<string, decimal>();
        
                    
                     for(Id empId : esMap.keyset()){
                        add = false;
                        temp = esmap.get(empId);
                        decimal count = 0;
                        for(Id id1 : WTypeReqSkill.keyset()){
                        for(Required_Skill__c r : WTypeReqSkill.get(id1)){
                            reqadd = false;
                            for(Employee_Skill__c  es : temp){
                                system.debug('--es.Survey_Score__c---'+es.Survey_Score__c);
                                system.debug('--r.Minimum_Score__c---'+r.Minimum_Score__c);
                                
                                if(es.Survey_Score__c >= r.Minimum_Score__c){
                                    system.debug('--r.Skill__c--'+r.Skill__c);
                                    system.debug('--es.Skill__c--'+es.Skill__c);
                                    
                                    if(r.Skill__c == es.Skill__c){
                                        count += es.Survey_Score__c;
                                        reqadd = true;
                                        //break;
                                    }
                                    else{
                                        reqadd = false;
                                        break;
                                    }
                                }
                                else{
                                    reqadd = false;
                                    break;
                                }
                            }
                            system.debug('--reqadd ---'+reqadd);
                            if(reqadd == true){
                               // break;
                               mapWorkorderTypePos.put(r.Workorder_Type__c,true);
                            }else{
                                break;
                            }
                        }
                        }
                        if(!reqadd){
                            add = false;
                        }else{
                            add = true;
                        }
                        
                        if(add){
                            
                            empSkillRateMap.put(empId, count);
                        }
                    }
               }
               
               system.debug('--mapWoType---'+mapWoType);
               system.debug('--mapWorkorderTypePos---'+mapWorkorderTypePos);
               
                for(Id key: mapWoType.keyset()){
                    
                    system.debug('---key--'+key);
                    
                    if(mapWorkorderTypePos.get(key)!=null && mapWorkorderTypePos.get(key) == true){
                        Workorder_Type__c w = mapWoType.get(key);
                        system.debug('---w----'+w);
                        for(Available_Slot__c av: lstASlot){
                                Decimal timediff = ((av.End_Time__c.getTime())/1000/60 - (av.Start_Time__c.getTime())/1000/60);
                                if(w.Est_Total_Time__c <= timediff){
                                    Decimal defaultTime = Decimal.valueof(system.Label.Default_Label_Time);
                                    Decimal totalTime = defaultTime + w.Est_Total_Time__c;
                                    Integer cnt = 0;
                                    for(integer i=0;totalTime <=timediff;i++){
                                        totalTime =totalTime + defaultTime + w.Est_Total_Time__c;
                                        cnt = cnt+1;
                                    }
                                    Possible_Workorder_Types__c psw =new Possible_Workorder_Types__c();
                                    psw.Available_Slot__c = av.Id;
                                    psw.Workorder_Type__c = w.Id;
                                    psw.Quantity__c = cnt;
                                    psw.Friendly_Workorder_Type_Name__c = mapWoType.get(w.Id).Friendly_Name__c;
                                    
                                    lstPSW.add(psw);
                                }
                        }
                    }
                }
                if(lstPSW.size()>0){
                    insert lstPSW;
                    
                }
            }
        }
    }
}