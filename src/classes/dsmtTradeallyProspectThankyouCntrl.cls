public with sharing class dsmtTradeallyProspectThankyouCntrl {
    public String sectionTitle{get;set;}
    public Checklist__c pgCheckList{get{return [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Trade_Ally_Prospect_Thank_You'];}}
    
    public dsmtTradeallyProspectThankyouCntrl (){}
    
   
}