public with sharing class dsmtBarrierProposalController{
    @testvisible
    String  RecId = '';  
    @testvisible
    Barrier__c barrier;
  
    public List<ProposalWrapper> ProposalWrapperList{get;set;}
    
    public dsmtBarrierProposalController(ApexPages.StandardController sc){
        RecId = sc.getId(); 
        loadProposalWrapperList();
    }
    
    public void loadProposalWrapperList(){
        barrier = new Barrier__c();
        ProposalWrapperList = new List<ProposalWrapper>();
        ProposalWrapper newWrapper = null;
        
        List<Barrier__c> barrierList = [select id, Energy_Assessment__c, Proposal__c, Project_Incentive__c from Barrier__c where id =: RecId];
        
        if(barrierList != null && barrierList.size() > 0){
            barrier = barrierList[0];
            
            List<Proposal__c> proposalList = [select id,name,Total_Cost__c,Total_Annual_Energy_Savings__c,Total_Incentive__c,
                                                Total_Payback__c,Customer_Name__c,Status__c from Proposal__c 
                                                where Energy_Assessment__c =: barrierList.get(0).Energy_Assessment__c and Type__c = 'Insulation'];
            
            if(ProposalList != null && ProposalList.size() > 0){
                Set<Id> proposalId = new Set<Id>();
                
                for(Proposal__c p : ProposalList){
                    proposalId.add(p.Id);
                }
                
                List<Recommendation_Scenario__c> projList = [select id,Proposal__c from Recommendation_Scenario__c 
                                                                where Proposal__c in : ProposalId];
                if(projList != null && projList.size() > 0){
                    Map<Id,Recommendation_Scenario__c> proposalProjMap = new Map<Id,Recommendation_Scenario__c>();
                    
                    for(Recommendation_Scenario__c proj : projList){
                        proposalProjMap.put(proj.Proposal__c,proj);
                    }
                    
                    for(Proposal__c p : ProposalList){
                        newWrapper = new ProposalWrapper();
                        
                        newWrapper.proposalId = p.Id;
                        newWrapper.ProposalName = p.Name;
                        if(proposalProjMap.get(p.Id) != null){
                            newWrapper.isProjectFound = true;
                            newWrapper.projectId = proposalProjMap.get(p.Id).Id;
                        }else{
                            newWrapper.isProjectFound = false;
                        }
                        
                        if(p.Id == barrier.Proposal__c)
                            newWrapper.isSelected = true;
                            
                        newWrapper.TotalCost = p.Total_Cost__c;
                        newWrapper.TotalEnergySavings = p.Total_Annual_Energy_Savings__c;
                        newWrapper.TotalIncentive = p.Total_Incentive__c;
                        newWrapper.TotalPayback = p.Total_Payback__c;
                        newWrapper.CustomerName = p.Customer_Name__c;
                        newWrapper.Status = p.Status__c;
                        
                        ProposalWrapperList.add(newWrapper);
                    }
                }else{
                    for(Proposal__c p : ProposalList){
                        newWrapper = new ProposalWrapper();
                        
                        newWrapper.proposalId = p.Id;
                        newWrapper.ProposalName = p.Name;
                        newWrapper.isProjectFound = false;
                        newWrapper.TotalCost = p.Total_Cost__c;
                        newWrapper.TotalEnergySavings = p.Total_Annual_Energy_Savings__c;
                        newWrapper.TotalIncentive = p.Total_Incentive__c;
                        newWrapper.TotalPayback = p.Total_Payback__c;
                        newWrapper.CustomerName = p.Customer_Name__c;
                        newWrapper.Status = p.Status__c;
                        
                        if(p.Id == barrier.Proposal__c)
                            newWrapper.isSelected = true;
                       
                        ProposalWrapperList.add(newWrapper);
                    }
                }
            }
        }
    }
    
    public void updateBarrier()
    {
        String selectedProposalId = ApexPages.currentPage().getParameters().get('selectedProposalId');
        
        List<Proposal__c> proposalList = [select id, Barrier_Incentive_Amount__c
                                          from Proposal__c
                                          where id =: selectedProposalId];
        
        if(proposalList.size() > 0)
        {
            List<Proposal__c> updateList = new List<Proposal__c>();
            List<Recommendation_Scenario__c> updateListProj = new List<Recommendation_Scenario__c>();
            String projectId = null;
            
            for(ProposalWrapper p : ProposalWrapperList)
            {
                if(p.proposalId != selectedProposalId)
                {
                    updateList.add(new Proposal__c(Id = p.proposalId, Barrier_Incentive_Amount__c = null));
                    if(p.projectId != null){
                        updateListProj.add(new Recommendation_Scenario__c(Id = p.projectId, Barrier_Incentive_Trigger__c = null));    
                    }
                }
                else
                    projectId = p.projectId;
            }
            
            proposalList[0].Barrier_Incentive_Amount__c = barrier.Project_Incentive__c;
            
            update proposalList;
            
            if(updateList.size() > 0)
                update updateList;
            
            if(updateListProj.size() > 0){
                update updateListProj;
            }
            
            system.debug('---recId---'+recId);
            system.debug('--projectId---'+projectId);
            
            update new Barrier__c(id = recId, Proposal__c = selectedProposalId, Project__c = projectId);
            
            loadProposalWrapperList();
        }
    }
    
    
    public class ProposalWrapper{
        public string proposalId {get;set;}
        public string ProposalName{get;set;}
        public string projectId {get;set;}
        public boolean isProjectFound{get;set;}
        public boolean isSelected {get;set;}
        public decimal TotalCost{get;set;}
        public decimal TotalEnergySavings{get;set;}
        public decimal TotalIncentive{get;set;}
        public decimal TotalPayback{get;set;}
        public string CustomerName{get;set;}
        public string Status{get;set;}
        
        public ProposalWrapper(){
        
        }
    }
}