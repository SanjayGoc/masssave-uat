@isTest
public class ReviewExceptionstest
{
    public static testmethod void test()
    {
        
        Exception_Template__c et = new Exception_Template__c ();
        et.Automatic__c = true;
        et.Active__c = true;
        et.Review_Level_Message__c  = true;
        et.Reference_Id__c ='Review-001';
        insert et;
        
        ID rectypeid = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();
        
        Review__c rs = new Review__c();
        //rs.Energy_Assessment__c =ea.id;
        rs.Status__c='New';
        rs.Reviewed_COC__c = false;
        rs.RecordTypeId = rectypeid;
        insert rs;
        
        Exception__c expc =new Exception__c();
        expc.Review__c=rs.id;
        expc.Exception_Template__c=et.id;
        insert expc;
        
        List<Review__c> lstRev = new List<Review__c>();
        lstRev.add(rs);
        
        Map<Id,Review__c> mapRev = new Map<Id,Review__c>();
        mapRev.put(rs.Id,rs);
        
        ReviewExceptions.CheckUpdateExceptions(lstRev,mapRev);
        
        
        
    }
}