public with sharing class dsmtUpdateStatusCntrl {
    public Inspection_Request__c InspecReq{get;set;}
    
    private string stage;
    
    public dsmtUpdateStatusCntrl(ApexPages.StandardController controller){
    
    String IrId = controller.getId();
    InspecReq = new Inspection_Request__c();
    
    List<Inspection_Request__c> IRlist = [Select id,Name,Status__c,Scoring_Date__c from Inspection_Request__c where id =: IrId];
        if(IRlist.size() > 0){
            InspecReq = IRlist[0]; 
            stage = IRlist.get(0).Status__c== null ? '' : IRlist.get(0).Status__c;                           
        }
    }
    
    public PageReference saveAndUpdate(){
        String IrId = InspecReq.Id;
        update InspecReq;    
        
        List<Inspection_Request__c> IRlist = [Select id,Name,Status__c,Scoring_Date__c from Inspection_Request__c where id =: IrId];
        
        if(IRlist.size() > 0){
            return new PageReference('/'+InspecReq.id);
        }else{
             return new PageReference('/home/home.jsp');
        }
    }
    public PageReference Cancel(){
        return new PageReference('/'+InspecReq.id);
    }

}