public class dsmtWaterFixturesValidationHelper extends dsmtEnergyAssessmentValidationHelper{
    public ValidationCategory validate(String categoryTitle,String uniqueName,Id eaId,ValidationCategory otherValidations){
        ValidationCategory validations = new ValidationCategory(categoryTitle,uniqueName);
        
        if(eaId != null){
            Integer i = 0;
            for(Water_Fixture__c wf : Database.query('SELECT ' + getSObjectFields('Water_Fixture__c') + ',RecordType.Name FROM Water_Fixture__c WHERE Energy_Assessment__c=:eaId')){
                i++;
                if(wf.RecordType.Name != null){
                    ValidationCategory wfValidations = new ValidationCategory(wf.Name,wf.Id);
                    validateGeneral(wfValidations,wf,otherValidations);
                    if(wf.Location__c == null || wf.Location__c.trim() == ''){
                        wfValidations.errors.add(new ValidationMessage('Location is required'));
                    }
                    if(wf.RecordType.Name.equalsIgnoreCase('Shower')){
                        validateShower(wfValidations,wf,otherValidations);
                    }else if(wf.RecordType.Name.equalsIgnoreCase('Spa')){
                        validateSpa(wfValidations,wf,otherValidations);
                    }else if(wf.RecordType.Name.equalsIgnoreCase('Pool')){
                        validatePool(wfValidations,wf,otherValidations);
                    }
                    validations.addCategory(wfValidations);
                }
            }
        }
        
        return validations;
    }
    
    private void validatePool(ValidationCategory validations,Water_Fixture__c wf,ValidationCategory otherValidations){
        if(wf.Covered_Amount__c == null || wf.Covered_Amount__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Covered Amount is required'));
        }
        
        if(wf.Length__c == null || String.valueOf(wf.Length__c).trim() == '' || wf.Length__c < 8){
            validations.errors.add(new ValidationMessage('Length must be greater than equal to 8'));
        }else if(wf.Length__c != null && wf.Length__c > 30){
            validations.errors.add(new ValidationMessage('Length must be less than equal to 30'));
        }
        
        if(wf.Width__c == null || String.valueOf(wf.Width__c).trim() == '' || wf.Width__c < 8){
            validations.errors.add(new ValidationMessage('Width must be greater than equal to 8'));
        }else if(wf.Width__c != null && wf.Width__c > 30){
            validations.errors.add(new ValidationMessage('Width must be less than equal to 30'));
        }
        
        if(wf.In_Season_Hours_Day__c == null || String.valueOf(wf.In_Season_Hours_Day__c).trim() == '' || wf.In_Season_Hours_Day__c < 1){
            validations.errors.add(new ValidationMessage('In Season Hours / Day must be greater than equal to 1'));
        }else if(wf.In_Season_Hours_Day__c != null && wf.In_Season_Hours_Day__c > 24){
            validations.errors.add(new ValidationMessage('In Season Hours / Day must be less than equal to 24'));
        }
        
        if(wf.Off_Season_Hours_Day__c != null && wf.Off_Season_Hours_Day__c > 24){
            validations.errors.add(new ValidationMessage('Off Season Hours / Day must be less than equal to 24'));
        }
        
        if(wf.Pump_HP__c == null || String.valueOf(wf.Pump_HP__c).trim() == '' || wf.Pump_HP__c < 0.5){
            validations.errors.add(new ValidationMessage('Pump HP must be greater than equal to 0.5'));
        }else if(wf.Pump_HP__c != null && wf.Pump_HP__c > 2.5){
            validations.errors.add(new ValidationMessage('Pump HP must be less than equal to 2.5'));
        }
        
        if(wf.Pump_Type__c == null || wf.Pump_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Pump type is required'));
        }
        
        if(wf.Pool_Heat_pump__c == null || String.valueOf(wf.Pool_Heat_pump__c).trim() == '' || wf.Pool_Heat_pump__c < 70){
            validations.errors.add(new ValidationMessage('Pool Heat Temp must be greater than equal to 70'));
        }else if(wf.Pool_Heat_pump__c != null && wf.Pool_Heat_pump__c > 85){
            validations.errors.add(new ValidationMessage('Pool Heat Temp must be less than equal to 85'));
        }
    }
    private void validateSpa(ValidationCategory validations,Water_Fixture__c wf,ValidationCategory otherValidations){
        if(wf.Spa_Insulation__c == null || wf.Spa_Insulation__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Spa insulation is required'));
        }
    }
    private void validateShower(ValidationCategory validations,Water_Fixture__c wf,ValidationCategory otherValidations){
        if(wf.Low_Flow_GPM__c == null || String.valueOf(wf.Low_Flow_GPM__c).trim() == '' || wf.Low_Flow_GPM__c < 0.5){
            validations.errors.add(new ValidationMessage('Low-Flow GPM must be greater than equal to 0.5'));
        }else if(wf.Low_Flow_GPM__c != null && Integer.valueOf(wf.Low_Flow_GPM__c) > 5){
            validations.errors.add(new ValidationMessage('Low-Flow GPM must be less than equal to 5'));
        }
        
        if(wf.Low_Flow_Quantity__c == null || wf.Quantity__c == null || wf.Quantity__c != wf.Low_Flow_Quantity__c){
            if(wf.GPM__c == null || String.valueOf(wf.GPM__c).trim() == '' || wf.GPM__c < 1){
                validations.errors.add(new ValidationMessage('GPM must be greater than equal to 1'));
            }else if(wf.GPM__c != null && wf.GPM__c > 10){
                validations.errors.add(new ValidationMessage('GPM must be less than equal to 10'));
            }
        }
    }
    private void validateGeneral(ValidationCategory validations,Water_Fixture__c wf,ValidationCategory otherValidations){
        if(wf.Quantity__c != null && wf.Low_Flow_Quantity__c != null && wf.Low_Flow_Quantity__c > wf.Quantity__c){
            validations.errors.add(new ValidationMessage('The number of Low Flow fixtures should not exceed the total number of fixtures.'));
        }
    }
}