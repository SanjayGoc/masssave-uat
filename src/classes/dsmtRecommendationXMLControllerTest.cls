@isTest
public class dsmtRecommendationXMLControllerTest{
    public testmethod static void test1(){

        
        Recommendation_Scenario__c prj = new Recommendation_Scenario__c();
        prj.Name = 'test';
        insert prj;
        
        Proposal__c ps = new Proposal__c();
        ps.Name = 'test';
        insert ps;
        
        Recommendation__c rc =new Recommendation__c();
        rc.Category__c='test';
        rc.SubCategory__c='test';
        
        /*Proposal_Recommendation__c pr = new Proposal_Recommendation__c();
        pr.Project__c = prj.Id;
        pr.Proposal__c = ps.Id;
        //pr.Recommendation__c = rc.Id;
        insert pr;*/
        
        //ApexPages.currentPage().getParameters().put('eassid',ea.id);
        //ApexPages.currentPage().getParameters().put('parentObjId',ea.id);
        ApexPages.currentPage().getParameters().put('parentField','Energy_Assessment__c');
        
        dsmtRecommendationXMLController cont = new dsmtRecommendationXMLController();
        
        cont.recommendationList();
        cont.checkNull('test');
        
        ApexPages.currentPage().getParameters().put('parentObjId',ps.id);
        ApexPages.currentPage().getParameters().put('parentField','Proposal__c');
        
        dsmtRecommendationXMLController cont1 = new dsmtRecommendationXMLController();
        
        cont1.proposalRecommendationList();
    }
     
}