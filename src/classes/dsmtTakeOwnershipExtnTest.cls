@istest
public class dsmtTakeOwnershipExtnTest
{
	@istest
    static void run()
    {
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
                Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        Review__c rv =new Review__c();
        rv.Status__c='Passed Review';
        rv.Trade_Ally_Account__c=ta.id;
        rv.Project__c=proj.Id;
        insert rv;
        
        Registration_Request__c rr =new Registration_Request__c();
        insert rr;
        
        Service_Request__c sr = new Service_Request__c();
        insert sr;
        
        ApexPages.currentPage().getParameters().put('id',ta.id);
        ApexPages.currentPage().getParameters().put('type','Trade_Ally_Account__c');
        dsmtTakeOwnershipExtn dts =new dsmtTakeOwnershipExtn();
        dts.assigneOwnerToCurrentLoggedInUser();
        
        ApexPages.currentPage().getParameters().put('id',rv.id);
        ApexPages.currentPage().getParameters().put('type','Review__c');
        dsmtTakeOwnershipExtn dts1 =new dsmtTakeOwnershipExtn();
        dts1.assigneOwnerToCurrentLoggedInUser();
        
        ApexPages.currentPage().getParameters().put('id',rr.id);
        ApexPages.currentPage().getParameters().put('type','Registration_Request__c');
        dsmtTakeOwnershipExtn dts2 =new dsmtTakeOwnershipExtn();
        dts2.assigneOwnerToCurrentLoggedInUser();
        
        ApexPages.currentPage().getParameters().put('id',sr.id);
        ApexPages.currentPage().getParameters().put('type','Service_Request__c');
        dsmtTakeOwnershipExtn dts3 =new dsmtTakeOwnershipExtn();
        dts3.assigneOwnerToCurrentLoggedInUser();
    }
}