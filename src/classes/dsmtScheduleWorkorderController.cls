public class dsmtScheduleWorkorderController{
    
    public string woId{get;set;}
    public dsmtScheduleWorkorderController(){
        woId = ApexPages.currentPage().getParameters().get('id');
        
    }
    
    public PageReference CreateWorkOrder(){
       try{
        List<Workorder__c> woList = [select id,Address__c,City__c,State__c,Zipcode__c,work_Team__c,Duration__c,Service_Date__c,
                                        Manually_Created_Workorder__c,Early_Arrival_Time__c,Late_Arrival_Time__c,
                                        Fixed_Workteam__c,Scheduled_Start_Date__c,Scheduled_End_Date__c,External_Reference_ID__c,Status__c 
                                        from workorder__c where id  =: woId];
        
        List<workOrderDetail> wdList = new List<workOrderDetail>();
        
        String dtConverted = '';
        for(Workorder__c wo : woList){
            workOrderDetail wod = new workOrderDetail();
            wod.workOrderId = wo.Id;
            wod.serviceWorkTeamId = wo.Work_Team__c;
            wod.serviceDate = wo.Service_Date__c;
            wod.ManualWorkOrder = true;
            wod.serviceDuration = integer.valueOf(wo.duration__c);
            wod.IsFixedResource = wo.Fixed_Workteam__c;
            
            if(wo.Early_Arrival_Time__c != null){
                dtConverted = wo.Early_Arrival_Time__c.format('MM/dd/yyyy h:mm a');
                wod.earlyArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);
            }else{
                dtConverted = wo.Scheduled_Start_Date__c.AddMinutes(-30).format('MM/dd/yyyy h:mm a');
                wod.earlyArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);   
            }
            if(wo.Late_Arrival_Time__c != null){
                dtConverted = wo.Late_Arrival_Time__c.format('MM/dd/yyyy h:mm a');
                wod.LateArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);
            }else{
                dtConverted = wo.Scheduled_Start_Date__c.AddMinutes(30).format('MM/dd/yyyy h:mm a');
                wod.LateArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);   
            }
            dtConverted = wo.Scheduled_Start_Date__c.format('MM/dd/yyyy h:mm a');
            wod.arrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
            dtConverted = wo.Scheduled_End_Date__c.format('MM/dd/yyyy h:mm a');
            wod.serviceFinishTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
            
             HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = new HTTPResponse();
            dsmtRouteParser.AddressParser obj1 = null;
            
            string address = EncodingUtil.urlEncode(wo.Address__c+' '+wo.City__c + ' '+wo.Zipcode__c, 'UTF-8');
            Address = Address.replace('#','');
            req.setEndpoint(System_Config__c.getInstance().URL__c+'GetAddress?type=json&address='+Address);
            req.setMethod('GET');
            
            //log.Request__c = req.getBody();
            
            http = new Http();
            if(!Test.isRunningTest()){
                res = http.send(req);
            }else{
                Map<String, String> boady2 = new Map<String, String>();
                boady2.put('status','test');
                boady2.put('address','test');
                boady2.put('City','test');
                boady2.put('latitude','1.32');
                boady2.put('longitude','12.4');
                boady2.put('formatedAddress','test');
                boady2.put('state_short','test');
                boady2.put('postalCode','test');
                String sbody = JSON.serialize(boady2);
                res.setBody(sbody);
            
            }
            
            obj1 = (dsmtRouteParser.AddressParser) System.JSON.deserialize(res.getBody(), dsmtRouteParser.AddressParser.class);
            wod.serviceAddress = obj1.formatedAddress;
            
            wdList.add(wod);
        }
        String str = JSON.serialize(wdList);
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        
        req.setEndpoint(System_Config__c.getInstance().URL__c+'CreateWorkOrder?type=json');
        req.setMethod('POST');
        
        //String body = str;
        String body = '{"workOrderDetail":'+str+'}';
        system.debug('--str+str1--'+body);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        http = new Http();
        if(!Test.isRunningTest()){
        res = http.send(req);
            system.debug('---'+res.getBody());
            dsmtCallOut.refIdResult obj1 = (dsmtCallOut.refIdResult) System.JSON.deserialize(res.getBody(), dsmtCallOut.refIdResult.class);
            if(obj1.workOrderRefId == null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Can not Schedule this workorder');
                ApexPages.addMessage(myMsg);
                return null; 
            }
            system.debug('--'+obj1.workOrderRefId);
            // a1G4D0000000ScpUAE~~~774502aa-7473-449e-9905-62a03d08b9bf
            woList.get(0).External_Reference_ID__c  = obj1.workOrderRefId.split('~~~').get(1);
            woList.get(0).Status__c = 'Scheduled';
            update woList;
            
            

        }
            PageReference pg = new Pagereference('/'+woId);
            pg.setRedirect(true);
            return pg;
       }
       catch(Exception ex){
           ApexPages.addMessages(ex);
           return null;
       }
    }
    
    public class workOrderDetail{
       // public string workOrderId;
        public Set<string> employeeId;
        public Set<string> workTeamId;
        public string serviceAddress;
       // public string territoryId;
        public List<Date> PlanDate;
        public Integer serviceDuration;
        public string sessionId;
        public string timeOfDay;
        public string dayNames;
        public string SQLRefId;
        public string workOrderId;
        public string serviceWorkTeamId;
        public boolean IsFixedResource;
        public date serviceDate;
        public boolean ManualWorkOrder;
        public string earlyArrivalTime;
        public string lateArrivalTime;
        public string arrivalTime;
        public string serviceFinishTime;
    }
}