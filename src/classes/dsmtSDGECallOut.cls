public class dsmtSDGECallOut{
    
    public static string getAuthToken(){
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
             
        req.setEndpoint(SDGE_Api_Credentials__c.getInstance().Endpoint__c+'oauth/token');
        req.setMethod('POST');
        
        String str1 = '';
        
        String body = 'grant_type=password&username='+SDGE_Api_Credentials__c.getInstance().Username__c+'&password='+SDGE_Api_Credentials__c.getInstance().Password__c;
        req.setBody(body);
        req.setTimeout(20000);
        
        http = new Http();
        
        if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
            
            ResponseWrapper responseWrap = (ResponseWrapper) JSON.deserialize(res.getBody(), ResponseWrapper.class);
            
            return responseWrap.access_token;
        }   
        
        return null;
    }
    
    public class ResponseWrapper
    {
        public string access_token;
        public string token_type;
    }
    
    public static CustomerWrapper searchCustomer(string accessToken, string searchString, string searchType){
        String str = '';
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        if(searchString==null && Test.isRunningTest()){
            searchString = 'test';
        }
        if(searchType ==null && Test.isRunningTest()){
            searchType= 'test';
        }
        string body = 'searchTerm='+EncodingUtil.urlEncode(searchString, 'UTF-8')+'&searchType='+EncodingUtil.urlEncode(searchType, 'UTF-8');
        req.setEndpoint(SDGE_Api_Credentials__c.getInstance().Endpoint__c+'api/v1/Customer/Search?pageSize=500&page=1&'+body);
        req.setMethod('GET');
        
        String str1 = '';
        
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setTimeout(20000);
        
        http = new Http();
        
        if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
            CustomerWrapper responseWrap = (CustomerWrapper) JSON.deserialize(res.getBody(), CustomerWrapper.class);
            
            return responseWrap;
        }
        else{
            CustomerDataWrapper cdr = new CustomerDataWrapper();
            cdr.PremiseAddress='null';
            cdr.CustomerName = 'test';
            List<CustomerDataWrapper> lstCDR = new List<CustomerDataWrapper>();
            lstCDR.add(cdr);
            CustomerWrapper cw = new CustomerWrapper();
            cw.success=true;
            cw.data=lstCDR;
            
            return cw;
        }
        
        //return null;       
    }
    
    public static DoubleDipWrapper checkDoubleDipByAccount(string accessToken, string serviceAccountNo){
        String str = '';
        system.debug('accessToken :::::'+ accessToken);
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        string body = 'searchTerm='+EncodingUtil.urlEncode('s', 'UTF-8');
        req.setEndpoint(SDGE_Api_Credentials__c.getInstance().Endpoint__c+'api/v1/Enrollment/GetDoubleDipByAccount?serviceAccountNo='+EncodingUtil.urlEncode(serviceAccountNo, 'UTF-8'));
        req.setMethod('GET');
        
        String str1 = '';
        
        req.setHeader('Authorization', 'Bearer ' + accessToken);
        req.setTimeout(20000);
        
        http = new Http();
        
        if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug('Double dip :::::' + res.getBody());
            
            DoubleDipWrapper responseWrap = (DoubleDipWrapper) JSON.deserialize(res.getBody().replace('Date":', 'DateString":'), DoubleDipWrapper.class);
            
            System.debug('Double dip :::::' + res.getBody());
            System.debug('responseWrap :::::' + responseWrap);
            
            return responseWrap;
        }  
        
        return null;      
    }
    
    public class CustomerWrapper {
        public boolean Success;
        public list<CustomerDataWrapper> Data;
    }
    
    public class CustomerDataWrapper {
        public string CustomerId;
        public string CustomerNumber;
        public string CustomerName;
        public string PremiseAddress;
        public string MailingAddress;
        public string Phone;
        public string AltPhone;
        public string CellPhone;
        public string CustomerClassName;
        public string AccountClassName;
        public string ServiceAccountNo;
        public string AccountId;
        public string AccountStatus;
        public string PremiseId;
        public string VendorPremiseId;
        public boolean IsCurrent;
        public boolean HasPreviousParticipation;
        public boolean HasLeads;
        public string ESIID;
        public string ServicePointId;
        public string ServicePointNumber;
        public string MeterNumber;
        public string ServicePointDescription;
        public string RateAccountClass;
        public string RateCode;
    }
    
    public class DoubleDipWrapper {
        public boolean Success;
        public list<DoubleDipDataWrapper> Data;
    }
    
    public class DoubleDipDataWrapper {
        public string DateString;
        public string MeasureName;
        public string Quantity;
        public string UnitOfMeasureDecimalPlaces;
        public string AmountUnitPrice;
        public string AmountNetValue;
        public string ProgramName;
        public string TransactionNumber;
        public string Source;
        public string CustomerNumber;
        public string CustomerName;
        public string AccountNumber;
        public string FormattedAccountNumber;
        public string Address;
        public string Phase;
        public string Amount;
        public string EnrollmentStatus;
        public string MeterNumbers;
    }
}