public class dsmtBuildingInfoController extends dsmtEnergyAssessmentBaseController {
    
    public Lighting__c lighting{get;set;}
    public String thermalId {get;set;}

    public Map<String,List<Layer__c>> thermalEnvelopeEdits{get;set;}  
    public Thermal_Envelope_Type__c teWindows{get;set;}
    public Integer thermalEnvelopeEditsSize{get;set;}

    public void deleteEnvType(){
        String envTypeId = ApexPages.currentPage().getParameters().get('enTypeId');
        if(envTypeId != null && envTypeId.trim().length() > 0){
            delete new Thermal_Envelope_Type__c(Id = envTypeId);
            queryThermalEnvelopeEdits();
        }
    }
    public void deleteAppliance(){
        String applianceId = ApexPages.currentPage().getParameters().get('applianceId');
        if(applianceId != null && applianceId.trim().length() > 0){
            delete new Appliance__c(Id = applianceId);
            queryAppliancesEdits();
        }
    }

    public Map<String,List<Appliance__c>> applianceEdits{get;set;}
    public void queryAppliancesEdits(){
        String eaId = getParam('assessId');
        if(eaId != null && eaId.trim() != ''){
            applianceEdits = new Map<String,List<Appliance__c>>();
            Set<String> applianceTypes = new Set<String>{'Kitchen Refrigeration','Laundry Washer'};
            for(Appliance__c apl : Database.query('SELECT Id,Name,Rated_kWh__c,Actual_Rated_kWh__c,Send_for_Early_Rebate_Eligibility__c , RecordType.Name, RecordType.DeveloperName FROM Appliance__c WHERE RecordType.Name IN :applianceTypes AND Energy_Assessment__c=:eaId order by CreatedDate desc')){ // DSST-12294 | Kaushik Rathore | 24 Oct, 2018
                //if(apl.Category__c != null){
                    String applianceType = (apl.RecordType.Name == 'Kitchen Refrigeration') ? 'Refrigerators' : 'Washers';
                    List<Appliance__c> tempList = applianceEdits.get(applianceType);
                    if(tempList == null){
                        tempList = new List<Appliance__c>();
                    }
                    tempList.add(apl);
                    
                    applianceEdits.put(applianceType, tempList);
                //}
            }  
        }
    }
    public void saveAppliancesEdits(){
        if(applianceEdits != null && applianceEdits.size() > 0){
            List<Appliance__c> appliances = new List<Appliance__c>();
            for(String key :applianceEdits.keySet()){
                appliances.addAll(applianceEdits.get(key));
            }
            if(appliances.size() > 0){
                UPDATE appliances;
            }
        }
    }
    public Map<String,Decimal> tetAggregateMap{get;set;} // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
    public Map<String,Thermal_Envelope_Type__c> exteriorWallAreaMap{get;set;} // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
    public Map<String,Floor__c> floorsMap{get;set;} // DSST-12294 | Kaushik Rathore | 26 Oct, 2018
    public void queryThermalEnvelopeEdits(){
        if(thermalId != null){
            // DSST-12294 | Kaushik Rathore | 26 Sept, 2018 : START
            thermalEnvelopeEdits = new Map<String,List<Layer__c>>();
            List<Layer__c> layers = [
            SELECT 
                Id,Name,RecordType.Name,Insul_Amount__c,ActualCavityInsulationAmount__c, // DSST-12294 | Kaushik Rathore | 26 Oct, 2018
                Thermal_Envelope_Type__c,Thermal_Envelope_Type__r.Name,Thermal_Envelope_Type__r.RecordType.Name,
                Thermal_Envelope_Type__r.Exterior_Area__c,Thermal_Envelope_Type__r.Actual_Area__c, // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
                Floor__c,Floor__r.Name,Floor__r.Insul_Amount__c,Floor__r.Thermal_Envelope_Type__c,Floor__r.Thermal_Envelope_Type__r.Name,Floor__r.Thermal_Envelope_Type__r.SpaceConditioning__c,Floor__r.Thermal_Envelope_Type__r.RecordType.Name, // DSST-12294 | Kaushik Rathore | 26 Oct, 2018
                Ceiling__c,Ceiling__r.Name,Ceiling__r.Insul_Amount__c,Ceiling__r.Thermal_Envelope_Type__c,Ceiling__r.Thermal_Envelope_Type__r.Name,Ceiling__r.Thermal_Envelope_Type__r.SpaceConditioning__c,Ceiling__r.Thermal_Envelope_Type__r.RecordType.Name, // DSST-12294 | Kaushik Rathore | 19 Nov, 2018
                Cavity_Insul_Type__c,ActualCavityInsulationType__c,Cavity_Insul_Depth__c,ActualCavityInsulationDepth__c // DSST-12294 | Kaushik Rathore | 24 Oct, 2018
            FROM Layer__c 
            WHERE Thermal_Envelope__c =:thermalId AND HideLayer__c = false
                AND (
                        (Thermal_Envelope_Type__c != null AND Thermal_Envelope_Type__r.RecordType.Name = 'Exterior Wall') OR 
                        (Floor__c!= null AND Floor__r.Thermal_Envelope_Type__c != null AND Floor__r.Thermal_Envelope_Type__r.RecordType.Name = 'Attic') OR
                        (Ceiling__c!= null AND Ceiling__r.Thermal_Envelope_Type__c != null AND Ceiling__r.Thermal_Envelope_Type__r.RecordType.Name = 'Attic') // DSST-12294 | Kaushik Rathore | 19 Nov, 2018
                    )
            ];
            tetAggregateMap = new Map<String,Decimal>(); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
            exteriorWallAreaMap = new Map<String,Thermal_Envelope_Type__c>(); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
            floorsMap = new Map<String,Floor__c>(); // DSST-12294 | Kaushik Rathore | 26 Oct, 2018
            Set<String> thermalTypeIdSet = new Set<String>(); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
            Set<String> conditionedThermalTypeIdSet = new Set<String>(); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
            for(Layer__c layer : layers){
                if(layer.Thermal_Envelope_Type__c != null && layer.Thermal_Envelope_Type__r.RecordType.Name == 'Exterior Wall'){
                    List<Layer__c> exteriorWallLayers = (thermalEnvelopeEdits.containsKey('Exterior Wall')) ? thermalEnvelopeEdits.get('Exterior Wall') : new List<Layer__c>();
                    exteriorWallLayers.add(layer);
                    thermalEnvelopeEdits.put('Exterior Wall',exteriorWallLayers);
                    Decimal area = layer.Thermal_Envelope_Type__r.Exterior_Area__c; // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
                    Decimal actualArea = layer.Thermal_Envelope_Type__r.Actual_Area__c; // DSST-12294 | Kaushik Rathore | 24 Oct, 2018
                    tetAggregateMap.put(String.valueOf(layer.Thermal_Envelope_Type__c).subString(0,15),area); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
                    exteriorWallAreaMap.put(String.valueOf(layer.Thermal_Envelope_Type__c).subString(0,15),new Thermal_Envelope_Type__c(Id = layer.Thermal_Envelope_Type__c,Exterior_Area__c = area,Actual_Area__c = actualArea)); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
                }
                if(layer.Floor__c != null && layer.Floor__r.Thermal_Envelope_Type__c != null && layer.Floor__r.Thermal_Envelope_Type__r.RecordType.Name == 'Attic' && layer.Floor__r.Thermal_Envelope_Type__r.SpaceConditioning__c != 'Conditioned'){
                    List<Layer__c> atticFloorLayers = (thermalEnvelopeEdits.containsKey('Attic')) ? thermalEnvelopeEdits.get('Attic') : new List<Layer__c>();
                    atticFloorLayers.add(layer);
                    thermalEnvelopeEdits.put('Attic',atticFloorLayers);   
                    tetAggregateMap.put(String.valueOf(layer.Floor__r.Thermal_Envelope_Type__c).subString(0,15),0); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
                    thermalTypeIdSet.add(layer.Floor__r.Thermal_Envelope_Type__c); // DSST-12294 | Kaushik Rathore | 26 Sept, 2018
                    floorsMap.put(String.valueOf(layer.Floor__c).subString(0,15),new Floor__c(Id = layer.Floor__c,Insul_Amount__c = layer.Floor__r.Insul_Amount__c)); // DSST-12294 | Kaushik Rathore | 26 Oct, 2018
                }

                /* DSST-12294 | Kaushik Rathore | 19 Nov, 2018 */
                if(layer.Ceiling__c != null && layer.Ceiling__r.Thermal_Envelope_Type__c != null && layer.Ceiling__r.Thermal_Envelope_Type__r.RecordType.Name == 'Attic' && layer.Ceiling__r.Thermal_Envelope_Type__r.SpaceConditioning__c == 'Conditioned'){
                    List<Layer__c> atticRoofLayers = (thermalEnvelopeEdits.containsKey('Attic')) ? thermalEnvelopeEdits.get('Attic') : new List<Layer__c>();
                    atticRoofLayers.add(layer);
                    thermalEnvelopeEdits.put('Attic',atticRoofLayers);   
                    tetAggregateMap.put(String.valueOf(layer.Ceiling__r.Thermal_Envelope_Type__c).subString(0,15),0);
                    conditionedThermalTypeIdSet.add(layer.Ceiling__r.Thermal_Envelope_Type__c); 
                    //floorsMap.put(String.valueOf(layer.Floor__c).subString(0,15),new Floor__c(Id = layer.Floor__c,Insul_Amount__c = layer.Floor__r.Insul_Amount__c)); // DSST-12294 | Kaushik Rathore | 26 Oct, 2018
                }
                /* DSST-12294 | Kaushik Rathore | 19 Nov, 2018 */
            } 
            thermalEnvelopeEditsSize = thermalEnvelopeEdits.size();
            if(thermalTypeIdSet.size() > 0){
                for(AggregateResult result : [SELECT Thermal_Envelope_Type__c,SUM(Area__c)totalArea FROM Floor__c WHERE Thermal_Envelope_Type__c IN :thermalTypeIdSet GROUP BY Thermal_Envelope_Type__c]){
                    Id tetId = (Id)result.get('Thermal_Envelope_Type__c');
                    Decimal totalArea = (Decimal)result.get('totalArea');
                    if(totalArea != null){
                        tetAggregateMap.put(String.valueOf(tetId).subString(0,15),totalArea);
                    }
                }
            }
            if(conditionedThermalTypeIdSet.size() > 0){
                for(AggregateResult result : [SELECT Thermal_Envelope_Type__c,SUM(Area__c)totalArea FROM Ceiling__c WHERE Thermal_Envelope_Type__c IN :conditionedThermalTypeIdSet GROUP BY Thermal_Envelope_Type__c]){
                    Id tetId = (Id)result.get('Thermal_Envelope_Type__c');
                    Decimal totalArea = (Decimal)result.get('totalArea');
                    if(totalArea != null){
                        tetAggregateMap.put(String.valueOf(tetId).subString(0,15),totalArea);
                    }
                }
            }           
            // DSST-12294 | Kaushik Rathore | 26 Sept, 2018 : END
            List<Thermal_Envelope_Type__c> tetWindows = [SELECT Id,Name,Window_Type__c,Single_Pane_Windows__c,(SELECT Id,Type__c FROM Windows_And_Skylights_Sets__r) FROM Thermal_Envelope_Type__c WHERE Thermal_Envelope__c =:thermalId AND RecordType.Name = 'Windows & Skylights'];
            if(tetWindows.size() > 0){
                teWindows = tetWindows.get(0);
                if(teWindows.Windows_And_Skylights_Sets__r.size() > 0){
                    teWindows.Window_Type__c = teWindows.Windows_And_Skylights_Sets__r.get(0).Type__c;
                }
            }
            
        }
    }
    public void SaveThermalEnvelopeEdits()
    {
        //BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true; /* DSST-12294 | Kaushik Rathore | 26 Oct, 2018 */
        if(thermalEnvelopeEdits != null && thermalEnvelopeEdits.size() > 0){
            List<Layer__c> layers = new List<Layer__c>(); 
            for(String key :thermalEnvelopeEdits.keySet()){
                layers.addAll(thermalEnvelopeEdits.get(key));
            }

            if(layers.size() > 0){
                /*for(Layer__c l : layers){
                    if(l.Insul_Amount__c != null && l.Insul_Amount__c.equalsIgnoreCase('none') ){
                        l.Insul_Amount__c = null;
                    }
                }*/
                //system.debug('LAYERS :: ' + layers);
                UPDATE layers;
            }
            /* DSST-12294 | Kaushik Rathore | 26 Oct, 2018 : START */
            // SAVE ATTIC FLOORS
            if(floorsMap.size() > 0){
                //UPDATE floorsMap.values();
            }
            /* DSST-12294 | Kaushik Rathore | 26 Oct, 2018 : END */

        }
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true; /* DSST-12294 | Kaushik Rathore | 26 Oct, 2018 */
        if(teWindows != null && teWindows.Id != null){
            List<SObject> objToUpdate = new List<SObject>();
            objToUpdate.add(new Thermal_Envelope_Type__c(
                Id = teWindows.Id,
                Window_Type__c = teWindows.Window_Type__c,
                Single_Pane_Windows__c = teWindows.Single_Pane_Windows__c
            ));
            if(teWindows.Windows_And_Skylights_Sets__r.size() > 0){
                objToUpdate.add(new Windows_Skylights_And_Set__c(
                    Id = teWindows.Windows_And_Skylights_Sets__r.get(0).Id,
                    Type__c = teWindows.Window_Type__c
                ));
            }
            UPDATE objToUpdate;
        }
        // DSST-12294 | Kaushik Rathore | 26 Sept, 2018 : START
        System.debug('exteriorWallAreaMap :: ' + exteriorWallAreaMap);
        if(exteriorWallAreaMap != null && exteriorWallAreaMap.size() > 0){
            List<SObject> objToUpdate = exteriorWallAreaMap.values();
            //BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = false;
            UPDATE objToUpdate;
        }
        
        // DSST-15797 | 27th Nov 2018
        // Rohit Sharma
        // Added appliance calculation skip
        dsmtRecursiveTriggerHandler.preventApplianceTrigger=true;

        // DSST-12294 | Kaushik Rathore | 26 Sept, 2018 : END
        saveAppliancesEdits();
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = false; /* DSST-12294 | Kaushik Rathore | 26 Oct, 2018 */
    }

    public PageReference validateBuildingInfo(){
        PageReference pg = validateRequest();
        if(pg == null){
            String assessId = getParam(KEY_ASSESSMENT_ID),
            custId = getParam(KEY_CUSTOMER_ID),
            appointmentId = getParam(KEY_APPOINTMENT_ID);

            if(assessId != null && pg == null)
            {
                list<Thermal_Envelope__c> teList = [
                    SELECT id,
                        Next_Slab_Number__c,Next_Attic_Number__c,Next_Band_Joist_Number__c,Next_Basement_Number__c,Next_Crawlspace_Number__c,
                        Next_Exterior_Wall_Number__c,Next_Floor_Over_Other_Dwelling_Number__c,Next_Garage_Ceiling_Number__c,Next_Flat_Roof_Number__c,
                        Next_Overhang_Number__c,Next_Vaulted_Roof_Number__c,Next_Windows_Skylights_Number__c,Next_Garage_Wall_Number__c
                    FROM Thermal_Envelope__c
                    WHERE Energy_Assessment__c =: assessId
                    ORDER BY CreatedDate DESC
                    LIMIT 1
                ];
                
                if(teList.size() > 0)
                {
                    te = teList.get(0);
                    thermalId = te.Id;
                }
                else{
                    te = new Thermal_Envelope__c(
                        Energy_Assessment__c = assessId,
                        Next_Slab_Number__c = 2,Next_Attic_Number__c = 2,Next_Band_Joist_Number__c = 2,Next_Basement_Number__c = 2,Next_Crawlspace_Number__c = 2,
                        Next_Exterior_Wall_Number__c=2,Next_Floor_Over_Other_Dwelling_Number__c=2,Next_Garage_Ceiling_Number__c =2,
                        Next_Overhang_Number__c=2,Next_Vaulted_Roof_Number__c =2,Next_Windows_Skylights_Number__c = 2
                    );
                    insert te;
                    thermalId = te.Id;
                }
                queryThermalEnvelopeEdits();
                queryAppliancesEdits();
            }
        }
        return pg;
    }
    
    public dsmtBuildingInfoController()
    {
        String eaId = getParam('assessId');
        List<Lighting__c> lightings = Database.query('SELECT ' + getSObjectFields('Lighting__c') + ' FROM Lighting__c WHERE Energy_Assessment__c=:eaId ');
        if(lightings.size() > 0){
            lighting = lightings.get(0);
        }
    }
    
    public List<Note> getNotes(){
        if(this.bs != null && this.bs.Id != null){
            return loadNotes(this.bs.Id);  
        }else{
            return new List<Note>();   
        }
    }
    
    public List<Attachment> getAttachments(){
        if(this.bs != null && this.bs.Id != null){
            return loadAttachments(this.bs.Id);  
        }else{
            return new List<Attachment>();   
        }
    }
}