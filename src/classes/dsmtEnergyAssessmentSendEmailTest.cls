@isTest
public class dsmtEnergyAssessmentSendEmailTest
{
    public testmethod static void test1()
    {
        
        Appointment__c app=new Appointment__c();
        app.Appointment_Start_Time__c=date.today();
        app.Appointment_End_Time__c= date.today();
        //app.Appointment_Status__c='New';
        insert app;
        
        /*app.Appointment_Status__c='Completed';
        update app;*/
        
        Energy_Assessment__c ea = Datagenerator.setupAssessment();
        
        Test.startTest();
        
        ea.Appointment__c=app.id;
        ea.Is_Added_In_Report__c=false;
        update ea;
        
        dsmtEnergyAssessmentSendEmail dea=new dsmtEnergyAssessmentSendEmail();
        
        dea.execute(null);
        Test.stopTest();
    }
}