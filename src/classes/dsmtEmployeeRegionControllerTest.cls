@isTest
Public class dsmtEmployeeRegionControllerTest {
    @isTest
    public static void runTest(){
        
        Location__c  loc = Datagenerator.createLocation();
        insert loc;
        
        Region__c  reg = Datagenerator.CreateRegion(loc.Id);
        insert reg;
        
        Employee__c emp = Datagenerator.createEmployee(loc.Id);
        emp.Employee_Id__c = 'test123';
        insert emp;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id,emp.Id);
        insert wt;
        
        Territory__c ter = Datagenerator.createTerritory(reg.Id,emp.Id,wt.Id);
        ter.Name = 'test';
        ter.Location__c=loc.id;
        insert ter;
        
        Territory_Detail__c td = Datagenerator.createTerritoryDetail(ter.Id);
        insert td;
        
        
        
        ApexPages.currentPage().getParameters().put('id',reg.Id);
        ApexPages.StandardController std=new ApexPages.StandardController(reg);
        dsmtEmployeeRegionController cntrl =new dsmtEmployeeRegionController(std);
        cntrl.empDeleteId = emp.Id;
        cntrl.DeleteEmpTerritory();
        cntrl.empId = emp.Id;
        cntrl.coordinates  = '32.010404958190534,-104.512939453125';
        try{
        	cntrl.SaveTerritory();
        }catch(exception ex){}
    }
}