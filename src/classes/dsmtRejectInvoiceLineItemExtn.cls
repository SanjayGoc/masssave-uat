public with sharing class dsmtRejectInvoiceLineItemExtn {
    public Invoice_Line_Item__c invLI{get;set;}
    String reId;
    
    private final ApexPages.standardController theController;
    
    public dsmtRejectInvoiceLineItemExtn (ApexPages.StandardController controller){
        theController = controller;
        invLI = new Invoice_Line_Item__c();
        
        reId = controller.getId();
        List<Invoice_Line_Item__c> lstRev = [select id, Name, Status__c, Rejection_Notes__c
                                             from Invoice_Line_Item__c 
                                             WHERE Id =: reId];
                                                  
        if(lstRev.size()>0){
            invLI = lstRev[0];
        }  
    }
    
    public PageReference saveAndUpdate(){
        system.debug('invLI.Rejection_Notes__c :::::' + invLI.Rejection_Notes__c);
        
        
        if(invLI.Rejection_Notes__c != null && invLI.Rejection_Notes__c != ''){
            list<Invoice_Line_Item__c> iliList = Database.query('SELECT ' + getSObjectFields('Invoice_Line_Item__c') + ' FROM Invoice_Line_Item__c WHERE id =: reId');
            system.debug('invLI.Rejection_Notes__c :::::' + iliList);
            if(iliList.size() > 0)
            {
                if(iliList[0].Unit_Cost__c != null && iliList[0].Unit_Cost__c > 0)
                {
                    Invoice_Line_Item__c iliClone = iliList[0].clone(false, false);
                    iliClone.Unit_Cost__c = (iliList[0].Unit_Cost__c * -1);
                    iliClone.Reversal_Created__c = true;
                    
                    insert iliClone;
                    
                    invLI.Reversal_Created__c = true;
                }
            }
            
            invLI.Status__c = 'Rejected';
            update invLI;
        }
         
        return new PageReference('/'+invLI.id);
    }
    
    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
    
    public PageReference Cancel(){
        return new PageReference('/'+invLI.id);
    }
}