@isTest
public class dsmtRejectRegistrationCntrlTest {
	testmethod static void testdsmtRejectRegistrationCntrl()
    {
        Registration_Request__c rr = new Registration_Request__c();
        insert rr;
        
        Exception__c ec = new Exception__c();
        ec.Registration_Request__c = rr.Id;
        ec.Short_Rejection_Reason__c = 'Short_Rejection_Reason__c';
        ec.Exception_Message__c = 'Exception_Message__c';
        insert ec;
        
        Exception__c ec1 = new Exception__c();
        ec1.Registration_Request__c = rr.Id;
        ec1.Short_Rejection_Reason__c = 'Short_Rejection_Reason__c';
        ec1.Exception_Message__c = 'Exception_Message__c';
        insert ec1;
        
        dsmtRejectRegistrationCntrl cntrl = new dsmtRejectRegistrationCntrl(new ApexPages.StandardController(rr));
        cntrl.selectedExcptions = 'AAA,BBB,'+ec.Id+','+ec1.Id+',DD';
        cntrl.noteBody='Note Body';
        System.debug('cntrl.listExceptions' + cntrl.listExceptions);
        cntrl.saveAndUpdate();
    }
    
    testmethod static void testdsmtRejectRegistrationCntrlDel()
    {
        Registration_Request__c rr = new Registration_Request__c();
        insert rr;
        
        Exception__c ec = new Exception__c();
        ec.Registration_Request__c = rr.Id;
        ec.Short_Rejection_Reason__c = 'Short_Rejection_Reason__c';
        ec.Exception_Message__c = 'Exception_Message__c';
        insert ec;
        
        Exception__c ec1 = new Exception__c();
        ec1.Registration_Request__c = rr.Id;
        ec1.Short_Rejection_Reason__c = 'Short_Rejection_Reason__c';
        ec1.Exception_Message__c = 'Exception_Message__c';
        insert ec1;
        
        dsmtRejectRegistrationCntrl cntrl = new dsmtRejectRegistrationCntrl(new ApexPages.StandardController(rr));
        cntrl.selectedExcptions = 'AAA,BBB,'+ec.Id+','+ec1.Id+',DD';
        cntrl.noteBody='Note Body';
        System.debug('cntrl.listExceptions' + cntrl.listExceptions);
        
        String noteTitle = cntrl.noteTitle;
        String selectedStatus = cntrl.selectedStatus;
        
        delete rr;
        cntrl.saveAndUpdate();
    }
}