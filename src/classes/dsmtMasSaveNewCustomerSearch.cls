/**
 * <h1>Code Change History</h1>
 * 
*  2017-01-26 - HP
     *               MSD Application issues
     *  
*/

public class dsmtMasSaveNewCustomerSearch {
    
   
    
    public static List<Sobject> SearchCustomer(Map<String,String> filter_map,String Result_Fields){
    
        
        Set<String> commonFields = new Set<String>();
        
        Set<String> newaccFields = new Set<String>();
        
        Set<String> newCustFields = new Set<String>();
        
        String AccFields = sObjectFields('Account');
        
        String CustFields = sObjectFields('Customer__c');
         
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get('Account').getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id' && dfield.getName() != 'RecordTypeId' ){
                newaccFields.add(dfield.getName());
            }
        }
        
        fieldsMap = Schema.getGlobalDescribe()
            .get('Customer__c').getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id' && dfield.isCreateable() && dfield.isUpdateable()){
                newcustFields.add(dfield.getName());
            }
        }
        
        for(String str : newaccFields){
            if(newcustFields.contains(str)){
                commonFields.add(str);    
            }
        }
    
        Map<String,String> filter_map_Account = filter_map;
      
        System.debug('entering customer search initiated @ ' + system.now());
        
        String masterQuery = '';
            //Searching Customers
        List<Customer__c> cstmr_List = new List<Customer__c>();
        String qry = 'select '+CustFields +' Id,Account__r.Id,Full_Service_Address__c From Customer__c';
        
        Set<Id> customerAlreadyCreated = new Set<Id>();
        cstmr_List  = new List<Customer__c>();
        // Query Accounts 
        List<Account> account_List = new List<Account>();
        List<Customer__c> newcustomers = new List<Customer__c>();
        Map<String,Customer__c> newCustomerMap = new Map<String,Customer__c>();
        
        qry = 'select '+AccFields +' Id,Service_Address_Full__c From Account';

        String whereclause = '';
        
         Map<String,Customer__c> custMap = new Map<String,Customer__c>();
         
        if(filter_map_Account != null && filter_map_Account.size() > 0){
            boolean AddFlag = false;
            qry += ' where ';
            
            for(String fld : filter_map_Account.keySet()){
                if(fld != 'Phone__c' && fld != 'Gas_Provider__c' && fld != 'Electric_Provider__c' && fld != 'Program_Id'
                    && fld != 'Program_Name' && fld != 'heatingSource' && fld != 'newheatingSource' && fld != 'ServiceAddress'
                    && fld != 'Electric_Account__c' && fld != 'Gas_Account__c'){
                    if(addFlag){
                        qry += ' AND ';
                        whereclause += ' AND ';
                    }
                    AddFlag = true;
                    qry +=  fld +' LIKE \'%'+ filter_map_Account.get(fld).replace('*','') + '%\''; 
                    whereclause +=  fld +' LIKE \'%'+ filter_map_Account.get(fld).replace('*','') + '%\''; 
                }
                
                if(fld == 'Phone__c'){
                    if(addFlag){
                        qry += ' AND ';
                        whereclause += ' AND ';
                    }
                    AddFlag = true;
                    qry +=  'Phone' +' LIKE \'%'+ filter_map_Account.get(fld).replace('*','') + '%\''; 
                    whereclause +=  'Phone'+' LIKE \'%'+ filter_map_Account.get(fld).replace('*','') + '%\''; 
                }
                
                if(fld == 'Gas_Provider__c'){
                    if(addFlag){
                        qry += ' AND ';
                        whereclause += ' AND ';
                    }
                    AddFlag = true;
                    qry +=  'Provider__c= \''+filter_map.get('Gas_Provider__c')+'\''; 
                    whereclause +=  'Provider__c= \''+filter_map.get('Gas_Provider__c')+'\''; 
                }
                
                if(fld == 'Electric_Provider__c'){
                    if(addFlag){
                        qry += ' AND ';
                        whereclause += ' AND ';
                    }
                    AddFlag = true;
                    qry +=  'Provider__c= \''+filter_map.get('Electric_Provider__c')+'\''; 
                    whereclause +=  'Provider__c= \''+filter_map.get('Electric_Provider__c')+'\''; 
                }
            }
        
            if(qry.contains('where') && AddFlag){
                qry += ' and Billing_Account_Number__c != null';
                whereclause  += ' and Billing_Account_Number__c != null';
            }
            else{
                qry += ' Billing_Account_Number__c != null';
                whereclause  += ' Billing_Account_Number__c != null';
            }
            
            if(filter_map.get('heatingSource') != null){
                qry +=  ' And Utility_Service_Type__c = \''+filter_map.get('heatingSource')+'\''; 
            }
            
            if(filter_map_Account.get('Gas_Account__c') != null && filter_map_Account.get('Electric_Account__c') != null){
                qry +=  ' And ( Billing_Account_Number__c' +' LIKE \'%'+ filter_map_Account.get('Gas_Account__c').replace('*','') + '%\''; 
                qry +=  ' OR  Billing_Account_Number__c' +' LIKE \'%'+ filter_map_Account.get('Electric_Account__c').replace('*','') + '%\' )'; 
            }
            else if(filter_map_Account.get('Gas_Account__c') != null){
                qry +=  ' And  Billing_Account_Number__c' +' LIKE \'%'+ filter_map_Account.get('Gas_Account__c').replace('*','') + '%\'';
            }
            
            else if(filter_map_Account.get('Electric_Account__c') != null ){
                qry +=  ' And  Billing_Account_Number__c' +' LIKE \'%'+ filter_map_Account.get('Electric_Account__c').replace('*','') + '%\'';
            }
            
            qry +=  ' And RecordType.Name= \'Customer\'';
            
            qry += ' order by Name LIMIT 250';        
         
            System.debug('Query ' + qry);                
            
            account_list  = database.Query(qry);//dsmtMasSaveCustomerSearch.AccountSearch(whereclause);
            
            system.debug('--account_list ---'+account_list );
            
             Set<Id> accountIds = new Set<Id>();
            
            if(account_list != null && account_list.size() > 0) {
               
            Map<String,Account> accountMap = new Map<String,Account>();
            
            for(Account acc : account_list){
                accountMap.put(acc.Id,acc);
                accountIds.add(acc.Id);
            }  
               String accQuery = '';
              // if(filter_map.get('Utility_Service_Type__c') != null && filter_map.get('Utility_Service_Type__c') == 'Gas'){
                  accQuery  = ' where Gas_Account__c in : accountIds';
              // } 
              // else if(filter_map.get('Utility_Service_Type__c') != null && filter_map.get('Utility_Service_Type__c') == 'Electric'){
                  accQuery += ' OR Electric_Account__c in : accountIds';
              // }
               
               system.debug('--accQuery ---'+accQuery );
               if(accQuery != ''){
               List<Customer__c> existingCustomers = database.query('select ' +custFields+ 'Id,Account__r.Id,Full_Service_Address__c From Customer__c ' + accQuery + ' Limit 250');     
               
              
               for(Customer__c cust : existingCustomers){
                   system.debug('--cust--'+cust);
                  // cstmr_List.add(cust);
                   if(cust.Electric_Account__c != null){
                       system.debug('--cust.Electric_Account_Number__c--'+cust.Electric_Account_Number__c);
                       //custMap.put((cust.first_Name__c+''+cust.Last_Name__c+cust.Service_Street_Number__c+cust.Service_Street__c+cust.Service_Postal_Code__c).replaceAll( '\\s+', ''),cust);
                       custMap.put(cust.Electric_Account_Number__c,cust);
                      // customerAlreadyCreated.add(cust.Electric_Account_Number__c);
                   }
                   if(cust.Gas_Account__c != null){
                       system.debug('--cust.Gas_Account_Number__c--'+cust.Gas_Account_Number__c);
                       //custMap.put((cust.first_Name__c+''+cust.Last_Name__c+cust.Service_Street_Number__c+cust.Service_Street__c+cust.Service_Postal_Code__c).replaceAll( '\\s+', ''),cust);
                       custMap.put(cust.Gas_Account_Number__c,cust);
                      // customerAlreadyCreated.add(cust.Gas_Account_Number__c);
                   }
               }
               }
            }
            
            system.debug('--custMap--'+custMap);
            List<Account> AccountListWithNoCustomersAcct = new List<Account>();
            
            Set<String> uniqueExtIds = new Set<String>(); // Keeps a unique list of external Ids
            
            Set<String> customerAdded = new Set<String>();
            
            if(account_list != null) {
            
                String actstr = '';
            
                for(Account acnt : account_list) {
                
                  //  if(!customerAlreadyCreated.contains(acnt.Id)){
                        Customer__c newCustomer = new Customer__c();
                        
                        /*if(newCustomerMap.get((acnt.first_Name__c+''+acnt.Last_Name__c+acnt.Service_Street__c+acnt.Service_Postal_Code__c).replaceAll( '\\s+', '')) != null){
                            newCustomer = newCustomerMap.get((acnt.first_Name__c+''+acnt.Last_Name__c+acnt.Service_Street__c+acnt.Service_Postal_Code__c).replaceAll( '\\s+', ''));
                        }*/
                       /* if(newCustomerMap.get(acnt.Billing_Account_Number__c) != null){
                            newCustomer = newCustomerMap.get(acnt.Billing_Account_Number__c);
                        }*/
                        actstr  = '';
                        
                        system.debug('--newCustomer ---'+newCustomer );
                        
                        //if(newCustomer == null){
                            if(acnt.Service_Street_Number__c != null){
                                actstr = acnt.Service_Street_Number__c;
                            }
                            if(acnt.Service_Street__c != null){
                                actstr += acnt.Service_Street__c;
                            }
                            if(acnt.Service_Street_Suffix__c != null){
                                actstr += acnt.Service_Street_Suffix__c;
                            }
                            if(acnt.Service_Postal_Code__c != null){
                                actstr += acnt.Service_Postal_Code__c;
                            }
                            system.debug('--actstr ---'+actstr);
                            newCustomer = newCustomerMap.get((acnt.first_Name__c+''+acnt.Last_Name__c+actstr).replaceAll( '\\s+', ''));
                       // }
                        system.debug('--newCustomer ---'+newCustomer );
                        if(acnt.Account_Status__c == 'Inactive' && newCustomer == null){ // Added By Eshan - DSST-2885
                        
                        }
                        else{
                            if(newCustomer == null){
                                newCustomer = new customer__c();
                            }
                           
                           if(acnt.Service_Street_Misc__c == null)
                                acnt.Service_Street_Misc__c = '';
                            
                            if(acnt.Service_Street_Number__c == null)
                                acnt.Service_Street_Number__c = '';
                            
                            if(acnt.Service_Street_Suffix__c == null)
                                acnt.Service_Street_Suffix__c = '';
                            
                             if(acnt.Service_Street__c == null)
                                acnt.Service_Street__c  = '';
                            
                          //  newcustomer.Service_Address__c = acnt.Service_Street_Number__c + ' '+acnt.Service_Street__c + ' '+acnt.Service_Street_Suffix__c + ' '+acnt.Service_Street_Misc__c;
                            system.debug('--custMap---'+custMap);
                            system.debug('--acnt.Billing_Account_Number__c--'+acnt.Billing_Account_Number__c);
                            system.debug('--custMap.get(acnt.Billing_Account_Number__c)---'+custMap.get(acnt.Billing_Account_Number__c));
                            /*if(custMap.get((acnt.first_Name__c+''+acnt.Last_Name__c+acnt.Service_Street_Number__c + ''+acnt.Service_Street__c+acnt.Service_Postal_Code__c).replaceAll( '\\s+', '')) != null){
                                newcustomer = custMap.get((acnt.first_Name__c+''+acnt.Last_Name__c+acnt.Service_Street_Number__c + ''+acnt.Service_Street__c+acnt.Service_Postal_Code__c).replaceAll( '\\s+', ''));
                            }*/
                          /*  if(custMap.get(acnt.Billing_Account_Number__c) != null){
                                newCustomer = custMap.get(acnt.Billing_Account_Number__c);
                            }else{*/
                                newcustomer.name =  acnt.First_Name__c + ' '+acnt.Last_Name__c;
                                newCustomer.Phone__c = acnt.Phone;
                                For(String str : CommonFields){
                                    if(acnt.get(str) != null && acnt.get(str) != ''){
                                        newcustomer.put(str,acnt.get(str));
                                    }
                                }
                           // }
                        //}
                        
                        if(acnt.Utility_Service_Type__c == 'Electric'){
                            newcustomer.Electric_Account_Number__c = acnt.Billing_Account_Number__c;
                            newcustomer.Electric_Provider_Name__c = acnt.Provider__c;
                            newcustomer.Electric_Account__c = acnt.Id;
                            newcustomer.Electric_Rate_Code__c = acnt.Rate_Code__c;
                            if(custMap.get( newcustomer.Electric_Account_Number__c) != null)
                                newcustomer.Id = custMap.get( newcustomer.Electric_Account_Number__c).Id;
                        }
                        if(acnt.Utility_Service_Type__c == 'Gas'){
                            newcustomer.Gas_Account_Number__c = acnt.Billing_Account_Number__c;
                            newcustomer.Gas_Provider_Name__c = acnt.Provider__c;
                            newcustomer.Gas_Account__c = acnt.Id;
                            newcustomer.Gas_Rate_Code__c = acnt.Rate_Code__c;
                            if(custMap.get( newcustomer.Gas_Account_Number__c) != null)
                                newcustomer.Id = custMap.get( newcustomer.Gas_Account_Number__c).Id;
                        }
                        newcustomer.Phone__c = acnt.Phone;
                        
                        if(acnt.Service_Street_Misc__c == null)
                            acnt.Service_Street_Misc__c = '';
                        
                        if(acnt.Service_Street_Number__c == null)
                            acnt.Service_Street_Number__c = '';
                        
                        if(acnt.Service_Street_Suffix__c == null)
                            acnt.Service_Street_Suffix__c = '';
                        
                         if(acnt.Service_Street__c == null)
                            acnt.Service_Street__c  = '';
                        
                        if(newcustomer.Verified_On__c == null){
                            newcustomer.Service_Address__c = acnt.Service_Street_Number__c + ' '+acnt.Service_Street__c + ' '+acnt.Service_Street_Suffix__c;
                        }
                      //  newcustomer.Service_Address__c = acnt.Service_Street_Number__c + ' '+acnt.Service_Street__c + ' '+acnt.Service_Street_Suffix__c + ' '+acnt.Service_Street_Misc__c;
                        system.debug('--newcustomer.Id---'+newcustomer.Id);
                       // newCustomerMap.put(acnt.Billing_Account_Number__c,newCustomer);
                       
                       actstr  = '';
                       
                       if(acnt.Service_Street_Number__c != null){
                            actstr = acnt.Service_Street_Number__c;
                        }
                        if(acnt.Service_Street__c != null){
                            actstr += acnt.Service_Street__c;
                        }
                        if(acnt.Service_Street_Suffix__c != null){
                            actstr += acnt.Service_Street_Suffix__c;
                        }
                        if(acnt.Service_Postal_Code__c != null){
                            actstr += acnt.Service_Postal_Code__c;
                        }
                        
                        newCustomerMap.put((acnt.first_Name__c+''+acnt.Last_Name__c+actstr).replaceAll( '\\s+', ''),newCustomer);
                        
                        system.debug('--newCustomerMap---'+newCustomerMap);
                       
                    //}   
                   }
                }
            }
        }
        
        List<Customer__c> allCustomer = new List<Customer__c>();
      //  allCustomer.AddAll(cstmr_List);
        allCustomer.AddAll(newCustomers);
        
        Set<String> custIds = new Set<String>();
        
        system.debug('--newCustomerMap--'+newCustomerMap);
        
        for(String str : newCustomerMap.keyset()){
            if(newCustomerMap.get(str).Id != null){
               if(custIds.add(newCustomerMap.get(str).Id)){
                  allCustomer.add(newCustomerMap.get(str));
               }
            } else{
               allCustomer.add(newCustomerMap.get(str));
            }
        }
        
        return allCustomer;
    }
    
    
    public static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id' && dfield .isCreateable() && dfield.isUpdateable())
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
    
}