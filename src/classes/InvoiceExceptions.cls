public class InvoiceExceptions
{
   public static boolean isStopValidateException = false;
   public static void InsertExceptionsOnRR(List<Invoice__c> RRlist){
       
        Map<String,Exception_Template__c> mapExt = new Map<String,Exception_Template__c>();     
        //Gets all expection with that check box checked.
        for(Exception_Template__c ext : [select id,name,Reference_ID__c,Type__c,Online_Portal_Title__c,Online_Portal_Text__c,Online_Application_Section__c,Required_Attachment_Type__c,Internal__c,Exception_Message__c,Outbound_Message__c,Short_Rejection_Reason__c,Attachment_Needed__c from Exception_template__c 
        where Automatic__c = true and Active__c = true and Invoice_Level_Message__c = true ]) {
          mapExt.put(ext.Reference_Id__c,ext);
        } 
        
        Set<Id> rrids = new Set<id>();
        for(Invoice__c rr :RRlist){
             rrids.add(rr.id);   
        }
        
        
         Set<Id> RRId = new Set<Id>();
         for(Invoice__c RR : RRlist){
              RRId.add(RR.Id);        
         }
         
         list<Invoice_line_item__c> invlist =[select id,name,invoice__c from Invoice_line_item__c where invoice__c in :rrids];
         Map<Id,Id> invLineMap = new Map<Id,Id>();
         
         for(Invoice_line_item__c invl : invList){
             invLineMap.put(invl.Invoice__c,invl.Id);
         }
         
        Map<String,Exception__c> mapExceptions = new Map<String,Exception__c>();
        for(Exception__c Ex :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Invoice__c, Invoice__r.id FROM Exception__c 
                                WHERE Invoice__r.id IN:rrids AND Disposition__c  != 'Resolved']){    
            mapExceptions.put(Ex.Invoice__c+'-'+Ex.Exception_Template__r.Reference_ID__c,Ex);     
        }      
        
        List<Exception__c> ExceptionsCreated = new List<Exception__c>();
          
        for(Invoice__c RRNew : RRlist) {
            
            List<string> AutoExceptionsList = new List<string>();
            
            for(String refId : mapExt.keyset()){
                Exception_Template__c temp = mapExt.get(refId);
               
                if(temp != null){
                      
                     if(refId == 'INV-00001' && RRNew.Contractor_Invoice_Submitted__c==false){
                        If(mapExceptions.get(RRNew.id+'-INV-00001')==null) 
                            AutoExceptionsList.add('INV-00001'); 
                     }
                     
                      //list<Invoice_line_item__c> invl =[select id,name from Invoice_line_item__c where invoice__c=:RRNew.id];
                     if(refId == 'INV-00002' && invLineMap.get(RRNEw.Id) == null){
                        If(mapExceptions.get(RRNew.id+'-INV-00002')==null) 
                            AutoExceptionsList.add('INV-00002'); 
                     }                     
                     
               }
                
           }
           
            system.debug('--AutoExceptionsList--'+AutoExceptionsList);
            for(string autolist :AutoExceptionsList){
                if(mapExt.get(autolist) !=null) {
                    Exception_Template__c Exceptiontemp = mapExt.get(autolist);
                    Exception__c ExceptionstoAdd = new Exception__c(); 
                    ExceptionstoAdd.Internal__c = Exceptiontemp.Internal__c; 
                    ExceptionstoAdd.Exception_Message__c= Exceptiontemp.Exception_Message__c; 
                    ExceptionstoAdd.Outbound_Message__c=Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Invoice__c=  RRNew.Id;
                    ExceptionstoAdd.Exception_Template__c = Exceptiontemp.id;
                    ExceptionstoAdd.FInal_Outbound_Message__c = Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Short_Rejection_Reason__c = Exceptiontemp.Short_Rejection_Reason__c ;
                    ExceptionstoAdd.Automated__c = true;
                    ExceptionstoAdd.OwnerId =RRNew.OwnerId;
                    ExceptionstoAdd.Attachment_Needed__c = Exceptiontemp.Attachment_Needed__c;
                    ExceptionstoAdd.Required_Attachment_Type__c = Exceptiontemp.Required_Attachment_Type__c;
                    ExceptionstoAdd.Online_Application_Section__c = Exceptiontemp.Online_Application_Section__c;
                    ExceptionstoAdd.Type__c = Exceptiontemp.Type__c;
                    //ExceptionstoAdd.Automatic_Communication__c = Exceptiontemp.Automatic_Communication__c;
                    //ExceptionstoAdd.Automatic_Rejection__c = Exceptiontemp.Automatic_Rejection__c;
                    //ExceptionstoAdd.Move_to_Incomplete_Queue__c = Exceptiontemp.Move_to_Incomplete_Queue__c;
                    ExceptionstoAdd.Online_Portal_Title__c  = Exceptiontemp.Online_Portal_Title__c;
                    ExceptionstoAdd.Online_Portal_Text__c = Exceptiontemp.Online_Portal_Text__c;
                    ExceptionsCreated.add(ExceptionstoAdd);    
                }
            }     
       }
        
        system.debug('--ExceptionsCreated--'+ExceptionsCreated);
        InvoiceExceptions.isStopValidateException = true;
        if(ExceptionsCreated.size()>0){
           insert ExceptionsCreated;  
        }   
   }
   
   public static void CheckUpdateExceptions(List<Invoice__c> RRlist , Map<Id,Invoice__c> oldmap){
        Map<string,List<Exception__c>> ExceptionsMap = new Map<string,List<Exception__c>>();
        for(Exception__c Exceptions :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Invoice__c, Invoice__r.id FROM Exception__c 
                                WHERE Invoice__r.id=:oldmap.keyset() AND Disposition__c  != 'Resolved']){
                List<Exception__c> lsExceptions = ExceptionsMap.get(Exceptions.Invoice__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c);
                if(lsExceptions  == NULL)
                    lsExceptions  = new List<Exception__c>();
                lsExceptions.add(Exceptions );
                ExceptionsMap.put(Exceptions.Invoice__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c,lsExceptions);        
        }
        
         List<Exception__c> ExceptionstoUpdate = new List<Exception__c>();
         Set<Id> RRId = new Set<Id>();
         for(Invoice__c RR : RRlist){
              RRId.add(RR.Id);        
         }
         
         list<Invoice_line_item__c> invlist =[select id,name,invoice__c from Invoice_line_item__c where invoice__c in :rrid];
         Map<Id,Id> invLineMap = new Map<Id,Id>();
         
         for(Invoice_line_item__c invl : invList){
             invLineMap.put(invl.Invoice__c,invl.Id);
         }
         
         for(Invoice__c rr : RRlist){
             
                //String keyValue = rr.id + '-INV-00001';
                 if(ExceptionsMap.containskey(rr.id + '-INV-00001')){  
                     if(rr.Contractor_Invoice_Submitted__c== true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-INV-00001')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                //list<Invoice_line_item__c> invl =[select id,name from Invoice_line_item__c where invoice__c=:rr.id];
                if(ExceptionsMap.containskey(rr.id + '-INV-00002')){  
                     if(invLineMap.get(rr.Id) != null){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-INV-00002')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
            }
         
          InvoiceExceptions.isStopValidateException = true;
            system.debug('--ExceptionstoUpdate--'+ExceptionstoUpdate);
            if(ExceptionstoUpdate.size()>0){
                update ExceptionstoUpdate;
            }
   }
}