public with sharing class ProjectEnergyAssessmenthandler implements ITrigger {
     public ProjectEnergyAssessmenthandler () {}

    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            
        } else if (trigger.isUpdate) {
            system.debug('--trigger.OldMap--'+trigger.OldMap);
        } else if (trigger.isDelete) {
            setFieldsOnProjectOnDelete(trigger.Old);
            //Here we will call before delete actions
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) {
            //Here we will call after insert actions
            setFieldsOnProject(trigger.New,null);
            
        } else if (trigger.isUpdate) {
            setFieldsOnProject(trigger.New,trigger.OldMap);
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }
    
    public static void setFieldsOnProject(List<Project_and_Energy_Assessment__c> peaList,Map<Id,Sobject> oldMap){
        
        Map<Id,Recommendation_Scenario__c> projtoUpdate = new MAp<Id,Recommendation_Scenario__c>();
        Recommendation_Scenario__c proj = null;
        
        if(oldMap == null){
            for(Project_and_Energy_Assessment__c pea : peaList){
                if(pea.project__c != null && pea.Special_Incentive__c != null){
                    
                    proj = new Recommendation_Scenario__c();
                    
                    if(projtoUpdate.get(pea.Project__c) != null){
                        proj = projtoUpdate.get(pea.Project__c) ;
                    }
                    
                    proj.Id = pea.Project__c;
                    if(pea.Special_Incentive__c.Contains('Duplex & Triple Decker')){
                        proj.Duplex_Triple_Decker__c = true;
                    }
                    if(pea.Special_Incentive__c.Contains('Moderate Income')){
                        proj.Moderate_Income__c = true;
                    }
                    if(pea.Special_Incentive__c.Contains('No Cap')){
                        proj.No_Cap__c = true;
                    }
                    projtoUpdate.put(proj.Id,Proj);
                }
            }
        }else{
            for(Project_and_Energy_Assessment__c pea : peaList){
                Project_and_Energy_Assessment__c  peaOld = (Project_and_Energy_Assessment__c)OldMap.get(pea.Id);
                if(pea.project__c != null && pea.Special_Incentive__c != null && 
                                (pea.Project__c != peaOld.Project__c || pea.Special_Incentive__c  != peaOld.Special_Incentive__c)){
                    
                    proj = new Recommendation_Scenario__c();
                    
                    if(projtoUpdate.get(pea.Project__c) != null){
                        proj = projtoUpdate.get(pea.Project__c) ;
                    }
                    
                    proj.Id = pea.Project__c;
                    if(pea.Special_Incentive__c.Contains('Duplex & Triple Decker')){
                        proj.Duplex_Triple_Decker__c = true;
                    }
                    if(pea.Special_Incentive__c.Contains('Moderate Income')){
                        proj.Moderate_Income__c = true;
                    }
                    if(pea.Special_Incentive__c.Contains('No Cap')){
                        proj.No_Cap__c = true;
                    }
                    projtoUpdate.put(proj.Id,Proj);
                }
            }
        }
        if(projtoUpdate.keyset().size() > 0){
            update projtoUpdate.values();
            
            List<Recommendation__c> recList = [select id,Recommendation_Scenario__c,Recommendation_Scenario__r.Override_Special_Incentive__c,
                                                     Recommendation_Scenario__r.Status__c,Total_Rec_Inc_After_Special_Incentive__c,
                                                     Total_Recommended_Incentive__c from Recommendation__c where Recommendation_Scenario__c in : projtoUpdate.keyset()];
                                                        
            if(recList != null && recList.size() > 0){
                for(Recommendation__c rec : recList){
                    
                    if(rec.Recommendation_Scenario__r.Status__c == 'Installed' && !rec.Recommendation_Scenario__r.Override_Special_Incentive__c){
                    
                    }
                }
            }
        }
        
        
    }
    
    public static void setFieldsOnProjectOnDelete(List<SObject> peaList){
        Map<Id,Recommendation_Scenario__c> projtoUpdate = new MAp<Id,Recommendation_Scenario__c>();
        Recommendation_Scenario__c proj = null;
        
        Set<Id> peaId = new Set<Id>();
        
        for(SObject sobj : peaList){
            peaId.add(sobj.Id);
        }
        
        for(SObject sobj : peaList){
            Project_and_Energy_Assessment__c  pea = (Project_and_Energy_Assessment__c)sobj;
            if(pea.project__c != null && pea.Special_Incentive__c != null){
                
                proj = new Recommendation_Scenario__c();
                    
                if(projtoUpdate.get(pea.Project__c) != null){
                    proj = projtoUpdate.get(pea.Project__c) ;
                }
                proj.Id = pea.Project__c;
                if(pea.Special_Incentive__c.Contains('Duplex & Triple Decker')){
                    proj.Duplex_Triple_Decker__c = false;
                }
                if(pea.Special_Incentive__c.Contains('Moderate Income')){
                    proj.Moderate_Income__c = false;
                }
                if(pea.Special_Incentive__c.Contains('No Cap')){
                    proj.No_Cap__c = false;
                }
                projtoUpdate.put(proj.Id,Proj);
            }
        }
        
        if(peaId.size() > 0){
            List<Exception__c> excList = [select id from Exception__c where Project_and_Energy_Assessment__c in : peaId];
            
            if(excList != null && excList.size() > 0){
                delete excList;
            }
        }
        
        if(projtoUpdate.keyset().size() > 0){
            update projtoUpdate.values();
        }
        
    }
    
}