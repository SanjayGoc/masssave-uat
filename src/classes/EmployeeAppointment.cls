public class EmployeeAppointment
{
    public string empoyeeId {get; set;}
    public List<SelectOption> AppointmentType{get;set;}
    public List<SelectOption> AppointmentStatus{get;set;}
    public List<SelectOption> dsmtContactOption{get;set;}
    
    public String SelectedAppointmentType{get;set;}
    public String SelectedAppointmentStatus{get;set;}
    public String Selectedcontacts{get;set;}
    
    
    public boolean IsSuperUser{get;set;}
    public List<Appointment__c> appointmentList{get;set;}
    
    Set<Id> dsmtcId = new Set<Id>();
    
    public Appointment__c newApp{get;set;}
    public Appointment__c editApp{get;set;}
    public String AppointmentId{get;set;}
    
    public List<Checklist_Items__c> ciList{get;set;}
    
    public String CancelNotes{get;set;}
    
    public EmployeeAppointment(){
        system.debug('empoyeeId  '+empoyeeId);
        newApp = new Appointment__c();
        editApp = new Appointment__c();
        GetContactInfo();
        FillSelectOption();
        IsSuperUser = false;
        appointmentList = new List<Appointment__c>();
        Selectedcontacts = '';
        SelectedAppointmentType = '';
        SelectedAppointmentStatus = '';
        //GetAppointments();
        GetCheckListInfo();
    }
    
    public void GetContactInfo(){
        dsmtContactOption = new List<SelectOption>(); 
        dsmtContactOption.add(new SelectOption('', '--Select Team Member--'));      
        
        List<User> userList = [select id,contactId from user where id =: userinfo.getUserId()];
        
        if(userList != null && userList.size() > 0 && userList.get(0).ContactId != null){
            
            system.debug('--userList.get(0).ContactId---'+userList.get(0).ContactId);
            
            List<DSMTracker_Contact__c> dsmtconList = [select id,Name,Super_User__c,Trade_Ally_Account__c from DSMTracker_Contact__c where 
                                                            contact__c =: userList.get(0).ContactId];
            
            if(dsmtConList != null && dsmtConList.get(0).Super_User__c){
                
                IsSuperUser  = true;
                
                dsmtconList = [select id,Super_User__c,Name from DSMTracker_Contact__c where 
                                                            Trade_Ally_Account__c  =: dsmtConList.get(0).Trade_Ally_Account__c and Super_User__c = false];
                for(DSMTracker_Contact__c dsmtc : dsmtConList){
                    dsmtcId.add(dsmtc.Id);
                    dsmtContactOption.add(new SelectOption(dsmtc.Id, dsmtc.Name));
                }
            }else if(dsmtConList != null){
                IsSuperUser = false;
                dsmtcId.add(dsmtConList.get(0).Id);
                dsmtContactOption.add(new SelectOption(dsmtConList.get(0).Id, dsmtConList.get(0).Name));        
            }
            
        }
    }
    public void FillSelectOption(){
        
        AppointmentType = new List<SelectOption>();
        AppointmentStatus = new List<SelectOption>();
        
        
        AppointmentType.add(new SelectOption('', '--Select Type--'));  
        
        Schema.DescribeFieldResult fieldResult = Appointment__c.Appointment_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : ple)
        {
          AppointmentType.add(new SelectOption(f.getLabel(), f.getValue()));
        }
        
        AppointmentStatus.add(new SelectOption('', '--Select Status--'));  
        
        fieldResult = Appointment__c.Appointment_Status__c.getDescribe();
        ple = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : ple)
        {
          AppointmentStatus.add(new SelectOption(f.getLabel(), f.getValue()));
        }     
   
    }
    
    public void SaveUnAvailableAppointment(){
        newApp.Appointment_Status__c = 'Unavailable';
        insert newApp;
        //GetAppointments();
        
        newApp = new Appointment__c();
    }
    
    public void updateAppoinment(){
        update editApp;
        editApp = new Appointment__c();
    }
    
    public void CancenAppointment(){
        //AppointmentId
        List<Appointment__c> appList  = [select Id,Cancel_Notes__c,Appointment_Status__c from Appointment__c where id =: AppointmentId];
        
        if(appList != null && appList.size() > 0){
            if(appList.get(0).Cancel_Notes__c == null){
                appList.get(0).Cancel_Notes__c = '';
            }
            appList.get(0).Cancel_Notes__c = CancelNotes;
            appList.get(0).Appointment_Status__c = 'Cancelled';
        }
        
        update appList;
    }
    
    public void GetCheckListInfo(){
        ciList = [select Checklist__c,Checklist_Information__c from Checklist_Items__c where Parent_Checklist__r.Reference_ID__c = 'CKLST-000003' order by Sequence__c];
    }
    
    public void queryAppointment(){
        editApp = new Appointment__c();
        list<Appointment__c> appList = [select 
                                   id, 
                                   Name,
                                   Appointment_Status__c ,
                                   Description__c,
                                   DSMTracker_Contact__c,
                                   Appointment_Start_Time__c,
                                   Appointment_End_Time__c 
                               from 
                                   Appointment__c
                                   where id =: AppointmentId];
                                   
        if(appList.size() > 0){
            editApp = appList[0];
        }
    }
    public string eventsJsonStr{
        get{
        string eventsJsonStr = '';
        system.debug('empoyeeId  '+empoyeeId);
        String filterquery =  ' Where Employee__c =:empoyeeId and';
        
        if(SelectedAppointmentType != '' && SelectedAppointmentType != null){
            filterquery += ' Appointment_Type__c = '+ '\'' + SelectedAppointmentType +'\' And';
        }
        
        if(SelectedAppointmentStatus != '' && SelectedAppointmentStatus != null){
            filterquery += ' Appointment_Status__c = '+ '\'' + SelectedAppointmentStatus +'\' And';
        }
        
        if(Selectedcontacts != null && Selectedcontacts != ''){
           // List<String> SelectedDsmtcId = Selectedcontacts.split('~~~');
            filterquery += ' DSMTracker_Contact__c = '+ '\'' + Selectedcontacts +'\' And';
        }
        
        if(filterquery.endswith('And')){
            filterquery = filterquery.substring(0,filterquery.length() - 3);
        }
        String query = 'select id, Name,Appointment_Status__c ,Appointment_Start_Time__c,Appointment_End_Time__c from Appointment__c';
        
        query += filterquery ;
        
        system.debug('--query---'+query);
        
        appointmentList = database.query( query);                     
        
        list<EventsWrapper> eventWrapperList = new list<EventsWrapper>();
        for(Appointment__c a : appointmentList)
        {
            if(a.Appointment_Start_Time__c != null && a.Appointment_End_Time__c != null){
                string evnt_desc = '';
                evnt_desc += a.Name + '<br/>';
                evnt_desc += a.Appointment_Status__c + '<br/>';
                evnt_desc += '<a href="javascript:void(0)" class="eventa" id="'+a.Id+'" onclick=openCancel(this);>C</a><br/>';
                evnt_desc += '<a href="javascript:void(0)" class="eventa lft" id="'+a.Id+'" onclick=openReschedule(this);>R</a><br/>';
                eventWrapperList.add(new EventsWrapper(a.Appointment_Start_Time__c.format('yyyy-MM-dd hh:mm'),
                                                       a.Appointment_End_Time__c.format('yyyy-MM-dd hh:mm'),
                                                       evnt_desc,
                                                       1));
            }
            
        }
        eventsJsonStr = JSON.serialize(eventWrapperList);
        return eventsJsonStr;
        }set;
    }
    
    public class EventsWrapper{
        public string start_date;
        public string end_date;
        public string text;
        public integer section_id;
        
        public EventsWrapper(string start_date,
                             string end_date,
                             string text,
                             integer section_id)
        {
            this.start_date = start_date;
            this.end_date = end_date;
            this.text = text;
            this.section_id = section_id;
        }
    }
}