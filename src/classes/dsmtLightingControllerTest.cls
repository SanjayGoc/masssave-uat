@isTest
public class dsmtLightingControllerTest {

    static testmethod void test(){
    
       Saving_Constant__c sc = new Saving_Constant__c();
       sc.LightingLocation_DaysPerYear__c = 1;
       sc.Kwh_To_BTUH__c = 1;
       insert sc;
       
       Lighting__c l = new Lighting__c();
       insert l;
    
       Lighting_Location__c ll = new Lighting_Location__c();
       insert ll;
       
       Account  acc= Datagenerator.createAccount();
        insert acc;
    
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        insert cust;
        
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Trade_Ally_Type__c='HPC';
        Tacc.Internal_Account__c = true;
        update Tacc;
     
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        dsmt.Status__c = 'Approved';
        dsmt.Trade_Ally_Type_PL__c = 'HPC';
        dsmt.Email__c = 'test@ucs.com';
        update dsmt;
        
        System_Config__c config = new System_Config__c();
        config.Arrival_Window_min_after_Appointment__c = 30;
        config.Arrival_Window_min_before_Appointment__c = 10;
        insert config;
        
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        app.Customer_Reference__c = cust.Id;
        app.Appointment_Status__c = 'Scheduled';
        Workorder__c wo = new Workorder__c(Status__c='Unscheduled');
        wo.Requested_Start_Date__c  = Datetime.now();
        wo.Requested_End_Date__c  = Datetime.now().Addhours(2);
        wo.Scheduled_Start_Date__c = Datetime.now();
        wo.Scheduled_End_Date__c= Datetime.now().Addhours(2);
        wo.Scheduled_Start_Date__c = datetime.newInstance(2018, 3, 2, 12, 30, 0);
        wo.Scheduled_End_Date__c =  datetime.newInstance(2018, 3, 2, 13, 30, 0);
        Workorder_Type__c woType = new Workorder_Type__c(Name='Wifi');
        woType.Qualifying_Audit__c  = true;
        insert woType;
        wo.workorder_Type__c = woType.Id;
        insert wo;
        app.Workorder__c = wo.Id;
        update app;
    
           /*Energy_Assessment__c ea = new  Energy_Assessment__c();
           ea.Appointment__c = app.Id;
           insert ea;*/
       
       ApexPages.currentPage().getParameters().put('viewName','lightingLocationPanel');
       ApexPages.currentPage().getParameters().put('sObjectId',ll.id);
       ApexPages.currentPage().getParameters().put('location','Family Room');
       ApexPages.currentPage().getParameters().put('locationId',ll.id);
       
       ApexPages.currentPage().getParameters().put('KEY_CUSTOMER_ID',cust.id);
       ApexPages.currentPage().getParameters().put('KEY_APPOINTMENT_ID',app.id);
       ApexPages.currentPage().getParameters().put('KEY_ASSESSMENT_ID','');
       
       
       
       dsmtLightingController cntrl = new dsmtLightingController();
       //cntrl.ea = ea;41
       //cntrl.validateRequestAndLighting();
       cntrl.lighting = l;
       cntrl.openView();
       cntrl.addNewLightingLocation();
       cntrl.saveLocationDetails();
       cntrl.deleteLocation();
       cntrl.getLightingLocationOptions();
       
        
    }
    
}