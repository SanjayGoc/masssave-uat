@isTest
public class dsmtEverySourceCouponsUtilTest {
	@isTest
    public static void runTest(){  
        EverSourceIR_Site__c cr = new EverSourceIR_Site__c();
        cr.Account_Name__c='EverSource Instant Rebate';
        cr.Allowed_Attempt__c=2;
        cr.Checklist_Name__c='EverSouce_IR_Coupons_Portal';
        cr.Eligible_Measure_Name__c='Smart Thermostat';
        INSERT cr;
        
        EverSource_API_Credentials__c evrsC = new EverSource_API_Credentials__c();  
        evrsC.Auth_Id__c='test';
        evrsC.Auth_Token__c='test';
        INSERT evrsC; 
        
        Account utlAcc = new Account();
        utlAcc.Name = cr.Account_Name__c; 
        INSERT utlAcc;
         
        Account retAcc = new Account();
        retAcc.Name = 'Test Retailer'; 
        INSERT retAcc;
        
        Portfolio__c p=new Portfolio__c();
        p.Account__c = utlAcc.Id;
        INSERT p;
        
        Program__c prog = new Program__c();
        prog.Name = 'EverSource Instant Rebate';
        prog.GL_String__c='test';
        prog.Portfolio__c = p.Id;
        prog.Account__c=utlAcc.Id;
        prog.Eligibility_Timeframe__c='Calendar Year';
        prog.Eligibility_Number_of_Years__c=2;
        prog.Participation_Limit__c=2;
        INSERT prog;
        
        MOU__c mou= new MOU__c();
        mou.Retailer__c = retAcc.Id;
        mou.Account__c = utlAcc.Id; 
        INSERT mou;
                
        Qualified_Product_List_Master__c Obj1 = new Qualified_Product_List_Master__c(Name = 'test', MOU__c = mou.Id, Program__c = prog.Id);
        insert Obj1;
        
        DSMTracker_Product__c Obj2 = new DSMTracker_Product__c();
        insert Obj2;
        
        Measure__c mesure = new Measure__c();
        mesure.Measure_Name__c = cr.Eligible_Measure_Name__c;
        INSERT mesure;
        
        Eligible_Measure__c em = new Eligible_Measure__c(Measure__c = mesure.Id,Program__c=prog.Id, Portfolio__c=p.Id);
        INSERT em;
        
        Qualified_Product_List__c Obj = new Qualified_Product_List__c(Qualified_Product_List_Master__c = Obj1.id, DSMTracker_Product__c = Obj2.id);
        insert Obj;
        
        Checklist__c Obj3 = new Checklist__c(Unique_Name__c = cr.Checklist_Name__c,Reference_ID__c = 'test');
        insert Obj3;
        
        Checklist_Items__c Obj4 = new Checklist_Items__c(Parent_Checklist__c = Obj3.id, Checklist__c = 'test',Reference_ID__c = 'Confirmation_Message', 
                                                         Retailer_Name__c=retAcc.Name, Section__c=retAcc.Name,
                                                        Checklist_Information__c='{!mdl.coupon.Amount__c} test {!mdl.coupon.Name} test {!mdl.coupon.Expiration_Date__c} test href=""');
        insert Obj4;
        
        Account custAcc = new Account();
        custAcc.Name = 'Test Customer'; 
        custAcc.BillingStreet='test';
        custAcc.BillingCity='test';
        custAcc.BillingState='test';        
        INSERT custAcc;
        
        Customer__c cust = new Customer__c();
        cust.Account__c = custAcc.Id;
        INSERT cust;
        
        Account acct = [SELECT Id, Name, First_Name__c, Last_Name__c, BillingStreet, BillingCity, BillingState, BillingPostalcode, Email__c, Phone,
                            (SELECT Id FROM Customers__r)
                            FROM Account WHERE Id=:custAcc.Id];
        
        Date sDate = Date.newInstance(2018, 3, 31);
        Date eDate = Date.newInstance(2019, 1, 1);   
        
        Coupon__c cop = new Coupon__c();
        cop.Name = '32132162132';
        cop.Retailer_Name__c = retAcc.Name;
        cop.Status__c = 'New';
        cop.Qualified_Product_List__c = Obj.Id;
        cop.Start_Date__c = sDate;
        cop.Amount__c=100;
        cop.Expiration_Date__c = eDate;
        INSERT cop;
        
        Coupon__c cop1 = new Coupon__c();
        cop1.Name = '32132162132';
        cop1.Retailer_Name__c = retAcc.Name;
        cop1.Status__c = 'Reserved';
        cop1.Qualified_Product_List__c = Obj.Id;
        cop1.Start_Date__c = sDate;
        cop1.Amount__c=100;
        cop1.Expiration_Date__c = eDate;
        cop1.Customer__c=cust.Id;
        cop1.Customer_Date__c=sDate;
        INSERT cop1;
        
        ApexPages.currentPage().getParameters().put('id',Obj2.id);
        dsmtEverySourceCouponsController ctr = new dsmtEverySourceCouponsController(); 
        ctr.init();
        List<Account> lst = new List<Account>();
        lst.add(custAcc);
        ctr.mdl.account= lst;
        
        dsmtEverySourceCouponsUtil uit = new dsmtEverySourceCouponsUtil(); 
        dsmtEverySourceCouponsUtil.testAcc = lst;  
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        dsmtEverySourceCouponsUtil.searchCustomer(ctr.mdl);
        dsmtEverySourceCouponsUtil.isEligible(ctr.mdl); 
        Test.stopTest();
    }
}