public class dsmtCreatePreWorkAppReviewController{
    
    public string ids {get;set;}
    public list<Recommendation_Scenario__c> projectList {get;set;}
    public dsmtCreatePreWorkAppReviewController()
    {
        init();
    }
    
    private void init(){
        ids = ApexPages.currentPage().getParameters().get('ids');
        projectList = new list<Recommendation_Scenario__c>();
        
        if(ids != null)
        {
            projectList = [select id, Name, Signature_Date__c
                           from Recommendation_Scenario__c
                           where id in : ids.split(',')];
        }
    }
    
    public boolean isError{get;set;}
    public PageReference saveProject()
    {
        isError = false;
        update projectList;
        List<Exception__c> lstExcep = [select Id,Exception_Template__r.Reference_ID__c,Project__r.Name from Exception__c where Project__c In :ids.split(',') AND (Exception_Template__r.Reference_ID__c = 'Project-001' OR Exception_Template__r.Reference_ID__c = 'Project-003' OR Exception_Template__r.Reference_ID__c = 'Project-005' OR Exception_Template__r.Reference_ID__c = 'Project-006') and Is_Open__c = 1];
        
        system.debug('--lstExcep--'+lstExcep.size());
        
        if(lstExcep.size()>0){
           isError = true;
           // return 'Exceptions must be resolved in order to create Pre Work Review';
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Exceptions must be resolved in order to create Pre Work Review.'));
           return null;
        }
        else{
            
             List<Recommendation_Scenario__c> projList = [select id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                                            Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c,
                                                            Energy_Assessment__r.Customer__r.First_Name__c,Energy_Assessment__r.Customer__r.Last_Name__c,
                                                            Energy_Assessment__r.Customer__c,Is_Heat_Loan__c
                                                            ,Address__c,City__c,State__c,Zip__c,Phone__c,Email__c from Recommendation_Scenario__c where id in: ids.split(',')];
            
                        
            Recommendation_Scenario__c Proj = projList.get(0);
            
            Id preRevRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Pre Work Review').getRecordTypeId();
            Review__c rev = new Review__c();
            //rev.Project__c = projList.get(0).Id;
            rev.Type__c = 'Pre Work Review';
            rev.Review_Type__c = 'Pre Work Review';
            rev.Requested_By__c = userinfo.getUSerId();
            rev.RecordTypeId = preRevRecordTypeId ;
            rev.Status__c = 'Pending Review';
            rev.Address__c = Proj.Address__c;
            rev.City__c = Proj.City__c ;
            rev.Zipcode__c = Proj.Zip__c;
            rev.State__c = Proj.State__c;
            rev.Phone__c = proj.Phone__c;
            rev.Email__c = Proj.Email__c;
            rev.Trade_Ally_Account__c = proj.Energy_Assessment__r.Trade_Ally_Account__c;
            rev.DSMTracker_Contact__c= proj.Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c;
            rev.First_Name__c = proj.Energy_Assessment__r.Customer__r.First_Name__c;
            rev.Last_Name__c = proj.Energy_Assessment__r.Customer__r.Last_Name__c;
            rev.Energy_Assessment__c = Proj.Energy_Assessment__c;
            rev.Customer__c = proj.Energy_Assessment__r.Customer__c;
            rev.Heat_Loan__c = proj.Is_Heat_Loan__c ;
            rev.Projects__c = ids;
            insert rev;
            
            List<Project_Review__c> prList = new List<Project_Review__c>();
            Project_Review__c pr = null;
            
            for(String str : ids.split(',')){
                pr = new Project_Review__c();
                pr.Project__c = str;
                pr.Review__c = rev.Id;
                prList.add(pr);
            }
            
            if(prList.size() > 0){
                insert prList;
            }
            
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.confirm,'Your Projects have been submitted for approval.'));
            return null;
        }
        
        
    }
}