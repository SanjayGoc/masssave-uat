public class WaterFixtureTriggerHandler extends TriggerHandler implements ITrigger{
    
    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            calculateDefaultValues(trigger.new, (Map<Id, Water_Fixture__c>) trigger.oldmap);
            dsmtWaterFixtureHelper.CalculateCostAndSavings(trigger.New);
        } else if (trigger.isUpdate) {
            //Here we will call before update actions
            calculateDefaultValues(trigger.new, (Map<Id, Water_Fixture__c>) trigger.oldmap);
            dsmtWaterFixtureHelper.CalculateCostAndSavings(trigger.New);
        } else if (trigger.isDelete) {
            //Here we will call before delete actions
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) {
            //Here we will call after insert actions
        } else if (trigger.isUpdate) {
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }
    
    private static void calculateDefaultValues(list<Water_Fixture__c> newList, Map<Id, Water_Fixture__c> oldMap)
    {
        Set<Id> eaIdSet = new Set<Id>();
        
        for(Water_Fixture__c a : newList)
        {
            if(a.IsFromFieldTool__c) continue;
            eaIdSet.add(a.Energy_Assessment__c);
        }

        if(eaIdSet ==null || eaIdSet.size()==0) return;

        dsmtEAModel.Surface bp  = dsmtEAModel.InitializeBuildingProfile(eaIdSet);
        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        Date dt = Date.today();

        //////// Update surface model to latest
        //bp = dsmtBuildingModelHelper.UpdateBaseBuildingThermal(bp);
        //bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        //bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);
        
        //Map<String, Building_Specification__c> buildSpecMap = dsmtApplianceHelper.queryBuildingSpecificationMap(eaIdSet);
        //Map<Id, Energy_Assessment__c> eaMap = new Map<Id, Energy_Assessment__c>((List<Energy_Assessment__c>) database.query('select ' + getSObjectFields('Energy_Assessment__c') + ' from Energy_Assessment__c where id in: eaIdSet'));
        
        List<Water_Fixture__c> waterFixtureList = bp.waterList;
        
        Map<String, List<Water_Fixture__c>> showerMap = new Map<String, List<Water_Fixture__c>>();
        Map<String, List<Water_Fixture__c>> sinkMap = new Map<String, List<Water_Fixture__c>>();
        Map<String, List<Water_Fixture__c>> bathMap = new Map<String, List<Water_Fixture__c>>();
        
        for(Water_Fixture__c a : waterFixtureList)
        {
            List<Water_Fixture__c> tempList = showerMap.get(a.Energy_Assessment__c);
            if(a.Type__c == 'Shower')
            {
                if(tempList == null)
                    tempList = new List<Water_Fixture__c>();
                
                tempList.add(a);
                
                showerMap.put(a.Energy_Assessment__c, tempList);
            }
            
            tempList = new List<Water_Fixture__c>();
            tempList = sinkMap.get(a.Energy_Assessment__c);
            if(a.Type__c == 'Sink')
            {
                if(tempList == null)
                    tempList = new List<Water_Fixture__c>();
                
                tempList.add(a);
                
                sinkMap.put(a.Energy_Assessment__c, tempList);
            }
            
            tempList = new List<Water_Fixture__c>();
            tempList = bathMap.get(a.Energy_Assessment__c);
            if(a.Type__c == 'Bath')
            {
                if(tempList == null)
                    tempList = new List<Water_Fixture__c>();
                
                tempList.add(a);
                
                bathMap.put(a.Energy_Assessment__c, tempList);
            }
        }
        
        //Map<Id, RecordType> recordTypeMap = new Map<Id, RecordType>([select id, Name from RecordType where sObjectType = 'Water_Fixture__c']);
        
        for(Water_Fixture__c a : newList)
        {
            if(oldMap == null)
            {
                /// its a insert call so we must add the current mech sub type to current list
                if(bp.waterList ==null) bp.waterList = new List<Water_Fixture__c>();
                bp.waterList.add(a);
            }
            else 
            {
                /// its a edit call so we must update existing one to current variable 
                if(bp.waterList ==null || bp.waterList.size() ==0) 
                {
                    bp.waterList = new List<Water_Fixture__c>();
                    bp.waterList.add(a);   
                }
                else 
                {
                    integer idx =0;
                    For(Water_Fixture__c m : bp.waterList)   
                    {
                        if(m.Id == a.Id)
                        {
                            bp.waterList[idx] = a;
                            break;
                        }
                        idx++;
                    }
                }                
            }
        }

        for(Water_Fixture__c a : newList)
        {
            bp.waterObj = a;
            
            a.Type__c = bp.recordTypeMap.get(a.RecordTypeId);
            
            a.Efficiency__c = 1;
            
            a.NumberOperatingMonths__c = 12;
            
            if(a.Months_Used__c != null)
                a.NumberOperatingMonths__c = a.Months_Used__c.split(';').size();
            
            if(a.Fuel_Type__c == null)
                a.Fuel_Type__c = dsmtEnergyConsumptionHelper.ComputeFuelTypeCommon(bp);
            
            if(a.Fuel_Type__c != null)
                a.Fuel_Unit__c = SurfaceConstants.FuelUnitsTable.get(a.Fuel_Type__c).get(0);
            
            if(a.Location__c == null)
                a.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);
            
            bp.Location = a.Location__c;
            
            a.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
            
            bp.FuelType = a.Fuel_Type__c;
            bp.FuelUnits = a.Fuel_Unit__c;
            bp.Efficiency = a.Efficiency__c;
            bp.LocationSpace = a.LocationSpace__c;
            bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
            bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
            bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);
            
            a.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
            bp.LocationType = a.LocationType__c;
            
            a.RegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');
            a.RegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');
            
            if(oldMap == null || (oldMap != null && (a.Actual_Year__c == null || a.Actual_Year__c == 0)))
            {
                a.Year__c = dsmtWaterFixtureHelper.CalculateApplianceYear(bp.bs, false);
            }
            
            if(a.Type__c == 'Shower')
            {
                List<Water_Fixture__c> showerList = showerMap.get(a.Energy_Assessment__c);
                
                if(showerList == null)
                    showerList = new List<Water_Fixture__c>();
                    
                if(oldMap == null)
                    showerList.add(a);
                
                if(a.Quantity__c == null || a.ActualQuantity__c == null || a.ActualQuantity__c == 0)
                    a.Quantity__c = dsmtWaterFixtureHelper.ComputeWaterFixtureQuantityCommon(a, bp.bs, showerList);
                    
                if(a.LowFlowQuantity__c == null || a.ActualLowFlowQuantity__c == null || a.ActualLowFlowQuantity__c == 0)
                    a.LowFlowQuantity__c = dsmtWaterFixtureHelper.ComputeWaterFixtureLowFlowQuantityCommon(a);
                
                if(a.ActualShowersPerDay__c == null || a.ActualShowersPerDay__c == 0)
                    a.ShowersPerDay__c = dsmtWaterFixtureHelper.ComputeWaterFixtureShowersPerDayCommon(a, bp.bs, showerList);
                
                if(a.GPM__c == null)
                    a.GPM__c = dsmtWaterFixtureHelper.ComputeWaterFixtureShowerExGPMCommon(a);
                
                a.ShowerInternalExGPM__c = dsmtWaterFixtureHelper.ComputeWaterFixtureShowerInternalExGPMCommon(a);
                
                if(a.Low_Flow_GPM__c == null)
                    a.Low_Flow_GPM__c = dsmtWaterFixtureHelper.ComputeWaterFixtureShowerNewGPMCommon(a);
                
                a.ShowerInternalNewGPM__c = dsmtWaterFixtureHelper.ComputeWaterFixtureShowerInternalNewGPMCommon(a);
            }
            else if(a.Type__c == 'Sink')
            {
                List<Water_Fixture__c> sinkList = sinkMap.get(a.Energy_Assessment__c);
                
                if(sinkList == null)
                    sinkList = new List<Water_Fixture__c>();
                    
                if(oldMap == null)
                    sinkList.add(a);
                    
                if(a.Quantity__c == null || a.ActualQuantity__c == null || a.ActualQuantity__c == 0)
                    a.Quantity__c = dsmtWaterFixtureHelper.ComputeWaterFixtureQuantityCommon(a, bp.bs, sinkList);
                
                if(a.LowFlowQuantity__c == null || a.ActualLowFlowQuantity__c == null || a.ActualLowFlowQuantity__c == 0)
                    a.LowFlowQuantity__c = dsmtWaterFixtureHelper.ComputeWaterFixtureLowFlowQuantityCommon(a);
            }
            else if(a.Type__c == 'Bath')
            {
                List<Water_Fixture__c> bathList = bathMap.get(a.Energy_Assessment__c);
                
                if(bathList == null)
                    bathList = new List<Water_Fixture__c>();
                    
                if(oldMap == null)
                    bathList.add(a);
                    
                if(a.Quantity__c == null || a.ActualQuantity__c == null || a.ActualQuantity__c == 0)
                    a.Quantity__c = dsmtWaterFixtureHelper.ComputeWaterFixtureQuantityCommon(a, bp.bs, bathList);
            }
            else if(a.Type__c == 'Pool')
            {
                if(a.Pool_Use_Season__c == null)
                    a.Pool_Use_Season__c = 'Jul;Aug;';
                
                if(a.Covered_Amount__c == null)
                    a.Covered_Amount__c = LightingAndApplianceConstant__c.getOrgDefaults().DefaultPoolOrSpaCovered__c;
                
                if(a.In_Season_Hours_Day__c == null)
                    a.In_Season_Hours_Day__c = LightingAndApplianceConstant__c.getOrgDefaults().DefaultPoolTimerHours__c;
                
                if(a.Pool_Size__c == null)
                    a.Pool_Size__c = LightingAndApplianceConstant__c.getOrgDefaults().DefaultPoolSize__c;
                
                if(a.Pump_Type__c == null)
                    a.Pump_Type__c = LightingAndApplianceConstant__c.getOrgDefaults().DefaultPoolPumpType__c;
                
                if(a.Heater_Type__c == null)
                    a.Heater_Type__c = LightingAndApplianceConstant__c.getOrgDefaults().DefaultPoolHeaterType__c;
                
                if(a.Off_Season_Use__c == null)
                    a.Off_Season_Use__c = dsmtWaterFixtureHelper.ComputePoolHasOffSeasonUseCommon(a);
                
                if(a.Pump_HP__c == null ||
                  (oldmap != null &&
                   a.Pool_Size__c != oldmap.get(a.Id).Pool_Size__c))
                    a.Pump_HP__c = dsmtWaterFixtureHelper.ComputePoolPumpHPCommon(a);
                
                if(a.Area__c == null ||
                  (oldmap != null &&
                  (a.Length__c != oldmap.get(a.Id).Length__c ||
                   a.Pool_Size__c != oldmap.get(a.Id).Pool_Size__c ||
                   a.Width__c != oldmap.get(a.Id).Width__c)))
                    a.Area__c = dsmtWaterFixtureHelper.ComputePoolAreaCommon(a);
                
                a.Heater_Months__c = dsmtWaterFixtureHelper.ComputeWaterFixturePoolHeatMonthsCommon(a);
                
                if(a.Heater_Months__c != null)
                    a.NumberPoolHeatMonths__c = a.Heater_Months__c.split(';').size();
            }
            else if(a.Type__c == 'Spa')
            {
                if(a.Spa_Insulation__c == null)
                    a.Spa_Insulation__c = LightingAndApplianceConstant__c.getOrgDefaults().DefaultSpaInsulation__c;
                
                if(a.Spa_Usage__c == null)
                    a.Spa_Usage__c      = LightingAndApplianceConstant__c.getOrgDefaults().DefaultSpaUsage__c;
                
                if(a.Months_Used__c == null)
                    a.Months_Used__c    = 'May;Jun;Jul;Aug;Sep';
            }
            
            a.Expected_Usefule_Life__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedUsefulLifeCommon('WaterFixture');
            a.Expected_Remaining_Life__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedRemainingLifeCommon(a.Expected_Usefule_Life__c, a.Year__c);
            
            a.Load__c = dsmtWaterFixtureHelper.ComputeWaterFixtureLoadCommon(a);
            bp.Load = a.Load__c;
            
            a.EnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'H');
            a.EnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'C');
            a.EnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'Sh');
            
            bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
            bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
            bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;

            
            //bp.AnnualFuelConsumption = a.Actual_Annual_Fuel_Consumption__c;
            
            
            a.FuelConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp,'H');
            a.FuelConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp,'C');
            a.FuelConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp,'Sh');
            
            a.HotWaterGallonsPerDay__c = dsmtWaterFixtureHelper.ComputeWaterFixtureHotWaterGallonsPerDayCommon(a, bp.bs, sinkMap.get(a.Energy_Assessment__c), bathMap.get(a.Energy_Assessment__c));
            a.ColdWaterGallonsPerDay__c = dsmtWaterFixtureHelper.ComputeWaterFixtureColdWaterGallonsPerDayCommon(a);
            a.TotalWaterGallonsPerDay__c = dsmtWaterFixtureHelper.ComputeWaterFixtureTotalWaterGallonsPerDayCommon(a);
            
            a.HotWaterGallonsPerDayH__c = dsmtWaterFixtureHelper.ComputeWaterFixtureHotWaterGallonsPerDayHCommon(a, bp);
            a.ColdWaterGallonsPerDayH__c = dsmtWaterFixtureHelper.ComputeWaterFixtureColdWaterGallonsPerDayHCommon(a, bp);
            a.TotalWaterGallonsPerDayH__c = dsmtWaterFixtureHelper.ComputeWaterFixtureTotalWaterGallonsPerDayHCommon(a);
            
            a.HotWaterGallonsPerDayC__c = dsmtWaterFixtureHelper.ComputeWaterFixtureHotWaterGallonsPerDayCCommon(a, bp);
            a.ColdWaterGallonsPerDayC__c = dsmtWaterFixtureHelper.ComputeWaterFixtureColdWaterGallonsPerDayCCommon(a, bp);
            a.TotalWaterGallonsPerDayC__c = dsmtWaterFixtureHelper.ComputeWaterFixtureTotalWaterGallonsPerDayCCommon(a);
            
            a.HotWaterGallonsPerDaySh__c = dsmtWaterFixtureHelper.ComputeWaterFixtureHotWaterGallonsPerDayShCommon(a, bp);
            a.ColdWaterGallonsPerDaySh__c = dsmtWaterFixtureHelper.ComputeWaterFixtureColdWaterGallonsPerDayShCommon(a, bp);
            a.TotalWaterGallonsPerDaySh__c = dsmtWaterFixtureHelper.ComputeWaterFixtureTotalWaterGallonsPerDayShCommon(a);
            
            a.HotWaterMixTemp__c = dsmtWaterFixtureHelper.ComputeHotWaterMixTempCommon(a, bp);
            
            a.HotWaterGallonsPerDayMixAdjH__c = dsmtWaterFixtureHelper.ComputeHotWaterGallonsPerDayMixAdjHCommon(a, bp);
            a.HotWaterGallonsPerDayMixAdjC__c = dsmtWaterFixtureHelper.ComputeHotWaterGallonsPerDayMixAdjCCommon(a, bp);
            a.HotWaterGallonsPerDayMixAdjSh__c = dsmtWaterFixtureHelper.ComputeHotWaterGallonsPerDayMixAdjShCommon(a, bp);
            a.HotWaterGallonsPerDayMixAdj__c = dsmtWaterFixtureHelper.ComputeHotWaterGallonsPerDayMixAdjCommon(a, bp);
        
            a.IndoorGainPlugH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp, 'H');
            a.IndoorGainPlugC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp, 'C');
            a.IndoorGainPlugCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp, 'CLat');
            
            a.IndoorGainGasH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainGasCommon(bp, 'H');
            a.IndoorGainGasC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainGasCommon(bp, 'C');
            a.IndoorGainGasCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainGasCommon(bp, 'CLat');
            
            a.IndoorGainOtherH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainOtherCommon(bp, 'H');
            a.IndoorGainOtherC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainOtherCommon(bp, 'C');
            a.IndoorGainOtherCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainOtherCommon(bp, 'CLat');
            
            a.IndoorGainHWH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainHWCommon(bp, 'H');
            a.IndoorGainHWC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainHWCommon(bp, 'C');
            a.IndoorGainHWCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainHWCommon(bp, 'CLat');
            
            a.IndoorGainH__c = (a.IndoorGainPlugH__c != null ? a.IndoorGainPlugH__c : 0) +
                               (a.IndoorGainGasH__c != null ? a.IndoorGainGasH__c : 0) +
                               (a.IndoorGainOtherH__c != null ? a.IndoorGainOtherH__c : 0) +
                               (a.IndoorGainHWH__c != null ? a.IndoorGainHWH__c : 0);
                               
            a.IndoorGainC__c = (a.IndoorGainPlugC__c != null ? a.IndoorGainPlugC__c : 0) +
                               (a.IndoorGainGasC__c != null ? a.IndoorGainGasC__c : 0) +
                               (a.IndoorGainOtherC__c != null ? a.IndoorGainOtherC__c : 0) +
                               (a.IndoorGainHWC__c != null ? a.IndoorGainHWC__c : 0);
            
            a.IndoorGainCLat__c = (a.IndoorGainPlugCLat__c != null ? a.IndoorGainPlugCLat__c : 0) +
                                  (a.IndoorGainGasCLat__c != null ? a.IndoorGainGasCLat__c : 0) +
                                  (a.IndoorGainOtherCLat__c != null ? a.IndoorGainOtherCLat__c : 0) +
                                  (a.IndoorGainHWCLat__c != null ? a.IndoorGainHWCLat__c : 0);
            //bp.AnnualFuelConsumption = a.Actual_Annual_Fuel_Consumption__c;

            bp.FuelUsage = dsmtEnergyConsumptionHelper.GetFuelUsageList(bp, dt);
            
            a.AnnualOperatingCost__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualOperatingCostCommon(bp);
        }
    }

    public static string getSObjectFields(string objName)
    {
        return dsmtHelperClass.sObjectFields(objName);
    }
}