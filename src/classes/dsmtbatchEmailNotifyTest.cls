@isTest
public class dsmtbatchEmailNotifyTest 
{
    static testMethod void TestUnitMethod() 
    {
        Trade_Ally_Account__c acc1 = new Trade_Ally_Account__c();
        acc1.Name ='test1';
        acc1.Status__c = 'Prospecting';
        acc1.Stage__c  = 'Prospecting';
        acc1.First_Name__c='test';
        acc1.Last_Name__c='test1';
        acc1.Email__c='test@gmail.com';
        acc1.Phone__c='9632587410';
        acc1.Title__c='GM';
        insert acc1;
        Test.startTest();
        dsmtbatchEmailNotify obj = new dsmtbatchEmailNotify();
        DataBase.executeBatch(obj);
        Test.stopTest();
    }
}