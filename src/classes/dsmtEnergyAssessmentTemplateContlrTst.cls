@isTest
public class dsmtEnergyAssessmentTemplateContlrTst{
    public testmethod static void test1(){
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        insert ea;
        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        string assessid = ea.id;
        //dsmtEnergyAssessmentTemplateController.SideBarMenuItem testWrap=new dsmtEnergyAssessmentTemplateController.SideBarMenuItem('test','test','test','test',true); 
        dsmtEnergyAssessmentTemplateController deass = new dsmtEnergyAssessmentTemplateController();
        deass.getShowIAmDone();
        deass.completeAssessment();
        dsmtEnergyAssessmentTemplateController.totalRecommendation(ea.id);     
        deass.getMenuItems(); 
    }
}