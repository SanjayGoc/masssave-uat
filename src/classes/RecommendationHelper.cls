public class RecommendationHelper{
    
    public static void CalculateCostAndSavings(List<Recommendation__c> RecObj){
        
        //try{
        /*Set<Id> dsmtProdId = new Set<Id>();
        Set<Id> lightingLocId = new Set<Id>();
        Set<ID> eaId = new Set<Id>();
        Set<Id> appId = new Set<Id>();
        set<Id> wfId = new Set<Id>();
        
        for(Recommendation__c rec : recObj){
            
            if(rec.DSMTracker_Product__c != null){
                dsmtProdId.add(rec.DSMTracker_Product__c);
            }
            if(rec.Lighting_Location__c != null){
                lightingLocId.add(rec.Lighting_Location__c);
            } 
            
            if(rec.Appliance__c != null){
                 rec.Units__c = '1';
                appId.add(rec.Appliance__c);
            }
            
            if(rec.Water_Fixture__c != null){
                wfId.add(rec.Water_Fixture__c);
            }
            
            eaId.add(rec.Energy_Assessment__c);
        }
        
        if(wfId.size() > 0){
            
            List<Water_Fixture__c> wfList  = [select id,name,Mechanical_Sub_Type__c,Location__c from Water_Fixture__c where id in : wfId];
            
            if(wfList != null && wfList.size() > 0){
                
                Map<Id,Water_Fixture__c> wfMap = new Map<Id,Water_Fixture__c>();
                
                for( Water_Fixture__c wf : wfList){
                    wfMap.put(wf.Id,wf);
                }
                
                for(Recommendation__c rec : recObj){
                    
                    if(rec.Water_Fixture__c != null && wfMap.get(rec.Water_Fixture__c) != null){
                        rec.Mechanical_Sub_Type__c = wfMap.get(rec.Water_Fixture__c).Mechanical_Sub_Type__c;
                        rec.Location__c  = wfMap.get(rec.Water_Fixture__c).Location__c ;
                    }
                }
            }
                 

        }
        system.debug('--appId---'+appId);
        Map<Id,DSMTracker_Product__c> dsmtProdMap = new Map<Id,DSMTracker_Product__c>();
        Map<Id,List<Lighting_Part_Detail__c>> lpdMap = new Map<Id,List<Lighting_Part_Detail__c>>();
        
        if(dsmtProdId.size() > 0){
            List<DSMTracker_Product__c> dsmtProdList = [select id,GPM__c,Eversource_Program_Price__c,Total_Watts__c,Usefull_Life__c from DSMTracker_Product__c
                                                                where id in : dsmtProdId];
            
            for(DSMTracker_Product__c dsmt : dsmtProdList){
                dsmtProdMap.put(dsmt.Id,dsmt);
            }
            
            List<Lighting_Part_Detail__c> lpdList = [select id,DSMTracker_Product__c ,LightBulbType__c,Wattage__c from Lighting_Part_Detail__c
                                                        where DSMTracker_Product__c in : dsmtProdId];
            
            
            List<Lighting_Part_Detail__c> templpdList = null;
            
            for(Lighting_Part_Detail__c lpd : lpdList){
                if(lpdMap.get(lpd.DSMTracker_Product__c) != null){
                    tempLpdList = lpdMap.get(lpd.DSMTracker_Product__c);
                    tempLpdList.add(lpd);
                    lpdMap.put(lpd.DSMTracker_Product__c,tempLpdList);
                }else{
                    tempLpdList = new List<Lighting_Part_Detail__c>();
                    tempLpdList.add(lpd);
                    lpdMap.put(lpd.DSMTracker_Product__c,tempLpdList);
                }
            }
            
            for(Recommendation__c rec : recObj){
            
                if(rec.DSMTracker_Product__c != null && dsmtProdMap.get(rec.DSMTracker_Product__c) != null){
                    rec.Usefull_Life__c = dsmtProdMap.get(rec.DSMTracker_Product__c).Usefull_Life__c ;
                    rec.GPM__c = dsmtProdMap.get(rec.DSMTracker_Product__c).GPM__c;
                    rec.Unit_Cost__c = double.valueOf(dsmtProdMap.get(rec.DSMTracker_Product__c).Eversource_Program_Price__c);
                    
                    if(dsmtProdMap.get(rec.DSMTracker_Product__c).Eversource_Program_Price__c != null && rec.Units__c != null)
                        rec.Cost__c = double.valueOf(dsmtProdMap.get(rec.DSMTracker_Product__c).Eversource_Program_Price__c) * double.valueOf(rec.Units__c);     
                    if(rec.Usage__c == null) {
                        rec.Usage__c = 1;
                    }
                    
                    if(lpdMap.get(rec.DSMTracker_Product__c) != null){
                        Lighting_Part_Detail__c lpdObj = null;
                        for(Lighting_Part_Detail__c lpd : lpdMap.get(rec.DSMTracker_Product__c)){
                            // To - Do check by LightBulbType__c
                            lpdObj = lpd;
                        }
                        
                        if(lpdObj != null){
                            try{
                                rec.Kwh__c = (lpdObj.Wattage__c * double.valueOf(rec.Units__c) * rec.Usage__c * double.valueOf(Saving_Constant__c.getOrgDefaults().LightingLocation_DaysPerYear__c)) /1000;
                                rec.Kw__c = lpdObj.Wattage__c * double.valueOf(rec.Units__c) *  rec.Usage__c *  double.valueOf(Saving_Constant__c.getOrgDefaults().LightingLocation_DaysPerYear__c) ;
                                rec.Incentive__c = rec.Cost__c;
                                rec.BTU__c = rec.Kwh__c * double.valueOf(Saving_Constant__c.getOrgDefaults().Kwh_To_BTUH__c)/1000000;
                            }catch(Exception ex){}
                        }
                    }
                } 
            }
        }
        if(lightingLocId.size() > 0){
            List<Lighting_Location__c> LightingLocList = [select id,LightBulbSetLoadKWH__c,LightBulbSetLoadBTU__c,lighting__c,Location__c from Lighting_Location__c
                                                                where id in : lightingLocId];
            
            Map<Id,Lighting_Location__c> LightingLocMap = new Map<Id,Lighting_Location__c>();
            
            for(Lighting_Location__c ll : LightingLocList){
                LightingLocMap.put(ll.Id,ll);
            }
            
             List<Energy_Assessment__c> appList = [select id, Appointment__c,Appointment__r.Appointment_Start_Date__c from Energy_Assessment__c  where Id in : eaId];
        
            Map<Id,Date> appMap = new Map<Id,Date>();
            
            if(appList != null && appList.size() > 0){
                
                for(Energy_Assessment__c app : appList){
                    appMap.put(app.Id,app.Appointment__r.Appointment_Start_Date__c);
                }
            }   
            
            Date dt = null;
            
            for(Recommendation__c rec : recObj){
                if(rec.Lighting_Location__c != null && LightingLocMap.get(rec.Lighting_Location__c) != null){
                    Lighting_Location__c lloc = LightingLocMap.get(rec.Lighting_Location__c);
                    rec.lighting__c  = lloc.lighting__c ; 
                    rec.Location__c = lloc.Location__c;
                    rec.Total_Recommended_Incentive__c = rec.Incentive__c;
                    rec.Total_Recommended_Kwh_Savings__c = (lloc.LightBulbSetLoadKWH__c != null && rec.Kwh__c != null) ? lloc.LightBulbSetLoadKWH__c - rec.Kwh__c : 0;
                    rec.Total_Recommended_KW_Savings__c = rec.Total_Recommended_Kwh_Savings__c * 1000;
                    rec.Total_Recommended_BTUh_Savings__c = (lloc.LightBulbSetLoadBTU__c != null && rec.BTU__c != null) ? lloc.LightBulbSetLoadBTU__c - rec.BTU__c : 0;
                    //rec.Total_Recommended_Incentive__c   = rec.Total_Recommended_Kwh_Savings__c ;
                     if(appMap.get(rec.Energy_Assessment__c) != null){
                    
                        dt = appMap.get(rec.Energy_Assessment__c);
                        
                        List<Fuel_Price__c> fpList = [select id,UnitCost__c from  Fuel_Price__c where FuelType__c = 'ELECTRIC'
                                                            and ValidFrom__c <= : dt and ValidTo__c >= : dt];
                        
                        if(fpList != null && fpList.size() > 0){
                            //l.Total_Energy_Cost__c = l.Total_Energy_Consumption__c * fpList.get(0).UnitCost__c ;
                            rec.Total_Recommended_Savings__c = rec.Total_Recommended_Kwh_Savings__c  * fpList.get(0).UnitCost__c ;
                        }   
                        
                        Double P1 = null;
                        Double DiscRate = Saving_Constant__c.getOrgDefaults().DiscountRate__c;
                        Double EnergyInflationRate = Saving_Constant__c.getOrgDefaults().EnergyInflationRate__c;
                        double UsefullLife = rec.Usefull_Life__c;//dsmtProdMap.get(rec.Dsmtracker_Product__c).usefull_Life__c;
                        if(DiscRate  == EnergyInflationRate ){
                            P1 = (UsefullLife)/(1+DiscRate);
                        }else{
                           // 1 / (DR - ER) * (1 - (decimal) Math.Pow((1 + (double) ER) / (1 + (double) DR), (double) nAP));
                            
                            // P1 =   1 - Math.pow( 1/(DiscRate -EnergyInflationRate)*(1-((1+EnergyInflationRate)/(1+DiscRate ))), UsefullLife);
                            try{
                                p1 = 1 / (DiscRate -EnergyInflationRate) * (1 - Math.Pow((1+EnergyInflationRate)/(1+DiscRate),UsefullLife));
                            }catch(Exception ex){
                                System.debug('EXCEPTION : ' + ex.getMessage());
                                p1 = 1;
                            }
                        }
                        try{
                            rec.SIR__c = p1 * rec.Total_Recommended_Savings__c/rec.Cost__c;
                            rec.simplepayback__c = rec.Cost__c / rec.Total_Recommended_Savings__c;
                        }catch(Exception ex){
                            System.debug('EXCEPTION : ' + ex.getMessage());
                            System.debug(' LINE NO : ' + ex.getLineNumber());
                        }

                    }
                }
            }
        }
        system.debug('--appId---'+appId);
        if(appId.size() > 0){
            List<Appliance__c> appList = [select id,RecordType.Name,Location__c,Electricity_Usage_kWh_per_Year__c,Electricity_Usage_BTU_per_Year__c,Smart_Power_Strip__c,Timer__c,Computed_Room_Temperature__c 
                                                              from Appliance__c where id in : appId];
            
            Map<Id,Appliance__c> appMap = new Map<Id,Appliance__c>();
            
            for(Appliance__c app : appList){
                appMap.put(app.Id,app);
            }
            system.debug('---appMap---'+appMap);
            
            
             List<Energy_Assessment__c> eaList= [select id, Appointment__c,Appointment__r.Appointment_Start_Date__c,Building_Specification__r.Occupants__c from Energy_Assessment__c  where Id in : eaId];
        
            Map<Id,Date> eaMap = new Map<Id,Date>();
            Map<Id,Energy_Assessment__c> mapBuilding = new Map<Id,Energy_Assessment__c>();
            
            
            if(eaList != null && eaList.size() > 0){
                
                for(Energy_Assessment__c app : eaList){
                    eaMap.put(app.Id,app.Appointment__r.Appointment_Start_Date__c);
                    mapBuilding.put(app.Id,app);
                }
            }   
            
            Date dt = null;
            
            for(Recommendation__c rec : recObj){
                system.debug('---recappMap---'+appMap.get(rec.Appliance__c));
                if(rec.Appliance__c != null && appMap.get(rec.Appliance__c) != null){
                   
                    if(appMap.get(rec.Appliance__c).RecordType.Name == 'Electronics TV'){
                        rec.Location__c = appMap.get(rec.Appliance__c).Location__c;
                        rec.Unit_Cost__c = double.valueOf(dsmtProdMap.get(rec.DSMTracker_Product__c).Eversource_Program_Price__c);
                        rec.Cost__c = double.valueOf(dsmtProdMap.get(rec.DSMTracker_Product__c).Eversource_Program_Price__c) * double.valueOf(rec.Units__c); 
                        
                        if(!appMap.get(rec.Appliance__c).Smart_Power_Strip__c){
                            if(appMap.get(rec.Appliance__c).Electricity_Usage_kWh_per_Year__c != null){
                                rec.Kwh__c = appMap.get(rec.Appliance__c).Electricity_Usage_kWh_per_Year__c - Saving_Constant__c.GetorgDefaults().TVSmartPowerStripKwhReductionPerYear__c;
                            }
                        }else{
                            if(appMap.get(rec.Appliance__c).Electricity_Usage_kWh_per_Year__c != null){
                                rec.Kwh__c = appMap.get(rec.Appliance__c).Electricity_Usage_kWh_per_Year__c;
                            }
                        }
                    }
                    
                    if(appMap.get(rec.Appliance__c).RecordType.Name == 'Kitchen Refrigeration'){
                        Decimal RefrigeratorIsPrimary = 0.5;
                        
                        if(rec.Primary_Refrigerator__c){
                            if(mapBuilding!= null && mapBuilding.get(rec.Energy_Assessment__c) != null){
                               RefrigeratorIsPrimary = mapBuilding.get(rec.Energy_Assessment__c).Building_Specification__r.Occupants__c ;
                            }
                        }else{
                            RefrigeratorIsPrimary = 0;
                        }
                        Decimal RefrigeratorHasAntiSweatSwitch  = 0;
                    
                        if(rec.Anti_Sweat_Switch__c){
                            RefrigeratorHasAntiSweatSwitch = 0.2;
                        }
                        
                        Decimal RefrigeratorPurchasedUsed  = 0;
                        
                        Decimal RefrigeratorHasThroughDoorIce  = 0;
                        if(rec.Ice_in_Door__c){
                            RefrigeratorHasThroughDoorIce  = 0.15;  
                        }
                        
                        Decimal RefrigeratorHasDoorSealGaps  = 0;
                        if(rec.Door_Seal_Gaps__c){
                            RefrigeratorHasDoorSealGaps = 0.15;
                        }
                        
                        
                       //rec.Appliance__.Room_Temperature__c = 61.97;
                       Decimal retval = 0;
                       if(lpdMap.get(rec.DSMTracker_Product__c) != null){
                            Lighting_Part_Detail__c lpdObj = null;
                            for(Lighting_Part_Detail__c lpd : lpdMap.get(rec.DSMTracker_Product__c)){
                                // To - Do check by LightBulbType__c
                                lpdObj = lpd;
                            }
                        
                            if(lpdObj != null){
                                retval  = lpdObj.Wattage__c * (.85 + (0.05 * RefrigeratorIsPrimary ) + RefrigeratorHasAntiSweatSwitch + RefrigeratorPurchasedUsed + RefrigeratorHasThroughDoorIce + RefrigeratorHasDoorSealGaps  + .025 * (appMap.get(rec.Appliance__c).Computed_Room_Temperature__c - 68) );   
                            }
                        }
                        
                        //app.Adjusted_kWh__c = retval;
                        
                        rec.Kwh__c = retval;//appMap.get(rec.Appliance__c).Electricity_Usage_kWh_per_Year__c;
                        
                    }
                    rec.Kw__c = rec.Kwh__c * 1000;
                    rec.Incentive__c = rec.Cost__c;
                    rec.BTU__c = rec.Kwh__c * double.valueOf(Saving_Constant__c.getOrgDefaults().Kwh_To_BTUH__c)/1000000;  
                    
                    if(appMap.get(rec.Appliance__c).RecordType.Name == 'Kitchen Refrigeration'){
                        rec.Total_Recommended_Kwh_Savings__c  = ((appMap.get(rec.Appliance__c).Electricity_Usage_kWh_per_Year__c != null) ? appMap.get(rec.Appliance__c).Electricity_Usage_kWh_per_Year__c : 0) - rec.Kwh__c;
                        rec.Total_Recommended_KW_Savings__c = rec.Total_Recommended_Kwh_Savings__c * 1000;
                        rec.Total_Recommended_BTUh_Savings__c = rec.Total_Recommended_Kwh_Savings__c * double.valueOf(Saving_Constant__c.getOrgDefaults().Kwh_To_BTUH__c)/1000000; 
                        
                         if(eaMap.get(rec.Energy_Assessment__c) != null){
                        
                            dt = eaMap.get(rec.Energy_Assessment__c);
                            
                            List<Fuel_Price__c> fpList = [select id,UnitCost__c from  Fuel_Price__c where FuelType__c = 'ELECTRIC'
                                                                and ValidFrom__c <= : dt and ValidTo__c >= : dt];
                            
                            if(fpList != null && fpList.size() > 0){
                                //l.Total_Energy_Cost__c = l.Total_Energy_Consumption__c * fpList.get(0).UnitCost__c ;
                                rec.Total_Recommended_Savings__c = rec.Total_Recommended_Kwh_Savings__c  * fpList.get(0).UnitCost__c ;
                            }   
                            
                           // rec.Total_Recommended_Savings__c =Saving_Constant__c.GetorgDefaults().TVSmartPowerStripKwhReductionPerYear__c;
                            
                            Double P1 = null;
                            Double DiscRate = Saving_Constant__c.getOrgDefaults().DiscountRate__c;
                            Double EnergyInflationRate = Saving_Constant__c.getOrgDefaults().EnergyInflationRate__c;
                            double UsefullLife = rec.Usefull_Life__c;//dsmtProdMap.get(rec.Dsmtracker_Product__c).usefull_Life__c;
                            if(DiscRate  == EnergyInflationRate ){
                                P1 = (UsefullLife)/(1+DiscRate);
                            }else{
                               // 1 / (DR - ER) * (1 - (decimal) Math.Pow((1 + (double) ER) / (1 + (double) DR), (double) nAP));
                                
                                // P1 =   1 - Math.pow( 1/(DiscRate -EnergyInflationRate)*(1-((1+EnergyInflationRate)/(1+DiscRate ))), UsefullLife);
                                
                                if(EnergyInflationRate != null && DiscRate != null && UsefullLife != null){
                                    p1 = 1 / (DiscRate -EnergyInflationRate) * (1 - Math.Pow((1+EnergyInflationRate)/(1+DiscRate),UsefullLife));
                                }
                            }
                            if(rec.Total_Recommended_Savings__c != null && rec.Cost__c != null){
                                rec.SIR__c = p1 * rec.Total_Recommended_Savings__c/rec.Cost__c;
                                rec.simplepayback__c = rec.Cost__c / rec.Total_Recommended_Savings__c;
                            }
                            rec.Incentive__c = 0;
                            rec.Total_Recommended_Incentive__c = 0;
                        }
                    }else{
                    if(!appMap.get(rec.Appliance__c).Smart_Power_Strip__c){
                        rec.Total_Recommended_Incentive__c = rec.Incentive__c;
                        rec.Total_Recommended_Kwh_Savings__c = Saving_Constant__c.GetorgDefaults().TVSmartPowerStripKwhReductionPerYear__c;
                        rec.Total_Recommended_KW_Savings__c = rec.Total_Recommended_Kwh_Savings__c * 1000;
                        rec.Total_Recommended_BTUh_Savings__c = rec.Total_Recommended_Kwh_Savings__c * double.valueOf(Saving_Constant__c.getOrgDefaults().Kwh_To_BTUH__c)/1000000; 
                        
                         if(eaMap.get(rec.Energy_Assessment__c) != null){
                        
                            dt = eaMap.get(rec.Energy_Assessment__c);
                            
                            List<Fuel_Price__c> fpList = [select id,UnitCost__c from  Fuel_Price__c where FuelType__c = 'ELECTRIC'
                                                                and ValidFrom__c <= : dt and ValidTo__c >= : dt];
                            
                            if(fpList != null && fpList.size() > 0){
                                //l.Total_Energy_Cost__c = l.Total_Energy_Consumption__c * fpList.get(0).UnitCost__c ;
                                rec.Total_Recommended_Savings__c = rec.Total_Recommended_Kwh_Savings__c  * fpList.get(0).UnitCost__c ;
                            }   
                            
                           // rec.Total_Recommended_Savings__c =Saving_Constant__c.GetorgDefaults().TVSmartPowerStripKwhReductionPerYear__c;
                            
                            Double P1 = 1;
                            Double DiscRate = Saving_Constant__c.getOrgDefaults().DiscountRate__c;
                            Double EnergyInflationRate = Saving_Constant__c.getOrgDefaults().EnergyInflationRate__c;
                            double UsefullLife = rec.Usefull_Life__c;//dsmtProdMap.get(rec.Dsmtracker_Product__c).usefull_Life__c;
                            if(DiscRate  == EnergyInflationRate ){
                                P1 = (UsefullLife)/(1+DiscRate);
                            }else{
                               // 1 / (DR - ER) * (1 - (decimal) Math.Pow((1 + (double) ER) / (1 + (double) DR), (double) nAP));
                                
                                // P1 =   1 - Math.pow( 1/(DiscRate -EnergyInflationRate)*(1-((1+EnergyInflationRate)/(1+DiscRate ))), UsefullLife);
                                if(EnergyInflationRate != null && DiscRate != null && UsefullLife != null){
                                    p1 = 1 / (DiscRate -EnergyInflationRate) * (1 - Math.Pow((1+EnergyInflationRate)/(1+DiscRate),UsefullLife));
                                }
                            }
                            if(p1 != null && rec.Total_Recommended_Savings__c != null && rec.Cost__c != null){
                                rec.SIR__c = p1 * rec.Total_Recommended_Savings__c/rec.Cost__c;
                                rec.simplepayback__c = rec.Cost__c / rec.Total_Recommended_Savings__c;
                            }
                            rec.Incentive__c = 0;
                            rec.Total_Recommended_Incentive__c = 0;
                        } 
                        }  
                        else{
                            rec.Total_Recommended_Incentive__c = 0;
                            rec.Total_Recommended_Kwh_Savings__c = 0;
                            rec.Total_Recommended_KW_Savings__c = 0;
                            rec.Total_Recommended_BTUh_Savings__c = 0;
                            rec.Incentive__c = 0;
                            rec.Total_Recommended_Savings__c = 0;
                            rec.SIR__c = 0;
                            rec.simplepayback__c = 0;
                        }
                    }

                }else if(rec.Appliance__c != null && appMap.get(rec.Appliance__c) != null && appMap.get(rec.Appliance__c).Smart_Power_Strip__c){
                    
                }
            }
            
        }
        
     /*  }catch(Exception e){
            system.debug('--e---'+e);
       }*/
    }
}