@istest(seeAllData=true)
public class dsmtdsmtAirFlowInfoControllerTest
{
    @istest
    static void test()
    {
         Building_Specification__c bs =new Building_Specification__c ();
         bs.Floor_Area__c=1800;
         //bs.Previous_Floor_Area__c=5;
         //bs.Appointment__c=app.id;
         bs.Year_Built__c='1950';
         bs.Bedromm__c=3;
         bs.Occupants__c=3;
         bs.Natural_Gas__c='Gas';
         bs.Orientation__c='South';
         bs.Ceiling_Heights__c=8.00;
         bs.Floors__c=1;
         //bs.Floor_Area__c=1800.00;
         bs.Finished_Basement_Area__c=0;
         bs.Total_Floor_Area__c=1800;
         bs.Total_Volume__c=14400;
         
         insert bs;
        Air_Flow_and_Air_Leakage__c afl = new Air_Flow_and_Air_Leakage__c(Building_Specifications__c = bs.id);
        insert afl;
        
        Blower_Door_Reading__c bdr =new Blower_Door_Reading__c();
        insert bdr;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea=[select id,name from Energy_Assessment__c where Status__c='Assessment Complete' order by LastmodifiedDate desc limit 1];
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        rc.Installed_Quantity__c = 10;
        rc.Change_Order_Quantity__c =5;
        rc.Invoice_Type__c ='NSTAR413';
        rc.Installed_Quantity__c =5;
        rc.Air_Flow_and_Air_Leakage__c=afl.id;
        rc.Energy_Assessment__c=ea.id;
        rc.Recommended_Product__c='testing';
        insert rc;
        
        dsmtdsmtAirFlowInfoController  cont=new dsmtdsmtAirFlowInfoController();
        cont.ea=ea;
        cont.afal=afl; 
        cont.bs=bs;
        cont.saveBlowerDoorReading();
        cont.editBlowerDoorReading();
        Test.startTest();
        //cont.newPressureDifferentialReading;
        cont.savePressureDifferentialReading();
        cont.getRecommendations();
        cont.saveRecommendation();
        cont.getNotes();
        cont.getAttachments();
        cont.getBlowerDoorReadings();
        cont.getPressureDifferentialReadings();
        cont.editPressureDifferentialReading();
        cont.editRecommendation();
        //cont.getRecommendationsSelectOptions();
        cont.deleteBlowerDoorReading();
        cont.deletePressureDifferentialReading();
        cont.deleteRecommendation();
        Test.stopTest();
    }
}