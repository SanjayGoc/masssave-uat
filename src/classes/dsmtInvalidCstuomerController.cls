public class dsmtInvalidCstuomerController {


    public PageReference init() {
        string custId = ApexPages.currentPage().getParameters().get('id');
        if(custId != null)
            update new Customer__c(id = custId, Customer_Not_Validated__c = true, Failed_Registration_Complete__c = true);
        
        return new PageReference('/'+ custId).setRedirect(true);
    }
}