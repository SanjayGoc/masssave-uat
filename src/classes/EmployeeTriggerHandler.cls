public class EmployeeTriggerHandler extends TriggerHandler {

    public override void afterInsert(){
        createWorkTeams(Trigger.new);
    }

    public override void afterUpdate(){
        List<Employee__c> employeesWithScheduleChanges = getEmployeesWithScheduleChanges((List<Employee__c>)Trigger.new, (Map<Id, Employee__c>)Trigger.oldMap);
        if (employeesWithScheduleChanges.isEmpty()) return;
        accomodateScheduleChange(employeesWithScheduleChanges);
        createWorkTeams(employeesWithScheduleChanges);
    }

    private static List<Employee__c> getEmployeesWithScheduleChanges(List<Employee__c> employees, Map<Id, Employee__c> oldMap){
        List<Employee__c> employeesWithScheduleChanges = new List<Employee__c>();
        for(Employee__c employee :  employees){
            Id newScheduleId = employee.Schedule__c;
            Id oldScheduleId = oldMap.get(employee.Id).Schedule__c;
            if (newScheduleId != null && newScheduleId != oldScheduleId) employeesWithScheduleChanges.add(employee);
        }
        return employeesWithScheduleChanges;
    }

    private static void createWorkTeams(List<Employee__c> employees){
        WorkTeamMaker workTeamCreator = new WorkTeamMaker(Date.today(), Integer.valueOf(Label.EmpDayLabel)); 
        workTeamCreator.createWorkTeamsForEmployees(employees);
    }

    private static void accomodateScheduleChange(List<Employee__c> employeesWithScheduleChange){
        Map<Id, Work_Team__c> existingWorkTeamsById = getExistingWorkTeams(employeesWithScheduleChange);
        if(!existingWorkTeamsById.isEmpty()){
            clearWorkTeamsAndAppointments(existingWorkTeamsById);
        }
    }

    private static List<Id> getIdsForEmployeesWithNewSchedules(List<Employee__c> employees, Map<Id, Employee__c> oldMap){
        List<Id> idsForEmployeesWithNewSchedules = new List<Id>();
        for(Employee__c employee : employees){
            Id newScheduleId = employee.Schedule__c;
            Id oldScheduleId = oldMap.get(employee.Id).Schedule__c;
            if (newScheduleId != null && newScheduleId != oldScheduleId) idsForEmployeesWithNewSchedules.add(employee.Id);
        }
        return idsForEmployeesWithNewSchedules;
    }

    private static Map<Id, Work_Team__c> getExistingWorkTeams(List<Employee__c> employeesWithNewSchedules){
        Set<Id> employeeIds = (new Map<Id, SObject>(employeesWithNewSchedules)).keySet();
        return new Map<Id, Work_Team__c>([SELECT Id FROM Work_Team__c WHERE Captain__c IN : employeeIds]);
    }

    private static void clearWorkTeamsAndAppointments(Map<Id, Work_Team__c> existingWorkTeamsById){
        List<Workorder__c> workOrders = [SELECT Id, Work_Team__c, Status__c,
                    (SELECT Id, Appointment_Status__c
                        FROM Appointments__r
                        WHERE Appointment_Status__c NOT IN ('Lunch', 'PTO', 'Sick'))
                FROM Workorder__c 
                WHERE Work_Team__c IN : existingWorkTeamsById.keySet()
                AND Status__c NOT IN ('Completed', 'Cancelled', 'Requires Rescheduling')];
        List<Appointment__c> appointments = new List<Appointment__c>();
        if(!workOrders.isEmpty()){
            for(Workorder__c workOrder : workOrders){
                workOrder.Status__c = 'Requires Rescheduling';
                appointments.addAll(workOrder.Appointments__r);
            }
            update workOrders;
            for(Appointment__c appointment : appointments){
                appointment.Appointment_Status__c = 'Cancelled';
            }
            if(!appointments.isEmpty()) update appointments;
        }
        delete existingWorkTeamsById.values();
    }    

}