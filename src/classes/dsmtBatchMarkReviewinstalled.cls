global class dsmtBatchMarkReviewinstalled implements Schedulable {

    global void execute(SchedulableContext ctx) {
    
        List<Review__c> revList  =  [SELECT Id, Name  
                FROM Review__c
                WHERE Schedule_Date__c=: Date.Today() and Status__c= 'Scheduled'];
                
        if(revList != null && revList.size() > 0){
            for(Review__c rev : revList){
                rev.Status__c = 'Past Install';
            }
            update revList;
        }
   } 

}