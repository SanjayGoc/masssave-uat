public class dsmtassignInventoryCntrl {

    public List<Location__c> locationlist{get;set;}
    public List<Warehouse__c> warehouselist{get;set;}
    public List<Product__c> productlist{get;set;}
    public List<Employee__c> employeelist{get;set;}
    public String selectedLocation{get;set;}
    public dsmtassignInventoryCntrl(){
       warehouselist = new List<Warehouse__c>();
       employeelist = new List<Employee__c>();
       getlocations();
       getProducts();
       
       String empid = ApexPages.currentPage().getParameters().get('id');
       system.debug('--empid--'+empid);
       if(empid != null){
          List<Employee__c> emplist = [select id,name,Location__c,Location__r.name from Employee__c where id =: empid];
          if(emplist.size() > 0){
            Employee__c emp = emplist.get(0);
            selectedEmp = emp.Id;
            selectedLocation = emp.Location__c;
            locationChanged();
          }
       }
       
       
      
    }
    
    public void getlocations(){
       locationlist = new List<Location__c>();
       locationlist = [select id,name from Location__c order by name];
    }
    
    public void getProducts(){
       productlist = new List<Product__c>();
       productlist = [select id,name from Product__c order by name];
    }
    
    public void locationChanged(){
       getWarehouses();
       getEmployees();
    }
    public void getWarehouses(){
       system.debug('--selectedLocation--'+selectedLocation);
       warehouselist = [select id,name from Warehouse__c where location__c =: selectedLocation];  
    }
    
    public void getEmployees(){
      system.debug('--selectedLocation--'+selectedLocation);
      employeelist = [select id,name from Employee__c where Location__c =: selectedLocation];
    }
    
    public string selectedInvIds{get;set;}
    public String selectedEmp{get;set;}
    public void assignInventory(){
      try{
         
         system.debug('--selectedInvIds--'+selectedInvIds);
         system.debug('--selectedEmp--'+selectedEmp);
         if(selectedInvIds != null){
            List<String> invitemids = selectedInvIds.split(',');
            List<Inventory_Item__c> invitemlist = [select id,name,Employee__c,Status__c from Inventory_Item__c where id in : invitemids];
            for(Inventory_Item__c invitem : invitemlist){
               invitem.Employee__c = selectedEmp;
               invitem.Status__c = 'Assigned';
            }
            update invitemlist;
         }
      }catch(Exception e){
        system.debug('--e--'+e);
      }
    }
    
    public void removeAssignInventory(){
       try{
         system.debug('--selectedInvIds--'+selectedInvIds);
         system.debug('--selectedEmp--'+selectedEmp);
         if(selectedInvIds != null){
            List<String> invitemids = selectedInvIds.split(',');
            List<Inventory_Item__c> invitemlist = [select id,name,Employee__c,Status__c from Inventory_Item__c where id in : invitemids];
            for(Inventory_Item__c invitem : invitemlist){
               invitem.Employee__c = null;
               invitem.Status__c = 'Available';
            }
            update invitemlist;
         }
      }catch(Exception e){
        system.debug('--e--'+e);
      }
    }
}