@isTest
Public class dsmtBypassEligibilityCheckControllerTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        acc.Billing_Account_Number__c= 'Gas-~~~1234343';
        insert acc;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Premise__c pre= Datagenerator.createPremise();
        insert pre;
        
        Customer__c cust = Datagenerator.createCustomer(acc.id,pre.ID);
        insert cust;
        
        Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                        Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
            insert woTypeList;
        
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        obj.Customer__c = cust.Id;
        insert obj;
        
        Location__c loc= Datagenerator.createLocation();
        insert loc;
        
        Employee__c em = Datagenerator.createEmployee(loc.id);
        em.Status__c='Approved';
        em.Employee_Id__c = 'test123';
        insert em;
        
        Skill__c sk= Datagenerator.createSkill();
        insert sk;
        
        Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
            insert rsk;
        
        Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
         insert wt;
        Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
         emskill.Survey_Score__c=89;
         insert emskill;
         
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        EL.Workorder_Type__c=woTypeList.Id;
        EL.Service_Address__c='test';
        EL.City__c='test';
        EL.Start_Date__c=date.today().addDays(-5);
        EL.End_Date__c=date.today().addDays(5);
        update EL;
        EL.How_many_units__c='Single family';
        EL.How_many_do_you_own__c  = '1';
        update EL;
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
         
         customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
         cEL.Workorder_Type__c=woTypeList.Id;
         cEL.Gas_Account__c='Gas-~~~123445';
         update cEL;
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        update app;
        
        customer_Eligibility__c  CustEl = new Customer_Eligibility__c (Eligibility_Check__c =EL.Id,Electric_Account__c='Electric',Gas_Account__c='Gas-~~~123445');
        
        ApexPages.currentPage().getParameters().put('clId',obj.Id);
        dsmtBypassEligibilityCheckController bpEC = new dsmtBypassEligibilityCheckController();
        bpEC.getEmployee();
        bpEC.SelectedEmployee = em.Id;
        bpEC.WoTypeId = woTypeList.Id;
        bpEC.customerId= cust.Id;
        bpEC.eleCheckId=EL.ID;
        bpEC.SelectedWorkTeam=wt.id;
        bpEC.custInteraction =EL;
        
        bpEC.getWeeks();
        bpEC.getDays();
        bpEC.getTimes();
        bpEC.customerId = cust.Id;
        bpEC.ecaList = new List<customer_Eligibility__c>();
        bpEC.ecaList.add(CustEl);
        bpEC.SaveRecord();
        bpEC.getEmployee();
        bpEC.getAppt();
        bpEC.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';

        bpEC.CreateWorkOrder();
        
        dsmtBypassEligibilityCheckController.fetchCustomer(obj.Id);
        dsmtBypassEligibilityCheckController.LetsGo(true,false);
        dsmtBypassEligibilityCheckController.getContractors();
        
    }
    
}