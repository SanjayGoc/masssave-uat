@isTest 
public class DefaultHomeDetailMappingTest {
    
   static testMethod void testschedule() 
    {
        
        //Create Energy_Assessment__c record
        
        
        Eligibility_Check__c elCheck = new  Eligibility_Check__c( Zip__c='1234',First_Name__c='Test',Last_Name__c='Test',Gas_Account_Holder_First_Name__c ='Gas-123',Gas_Account_Holder_Last_Name__c='test-123');
        insert elCheck;    
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        insert loc;
        Employee__c emp = Datagenerator.createEmployee(loc.Id);//elCheck
        insert emp;
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, emp.Id);
        Workorder__c wo = new Workorder__c();
        wo.Status__c = 'Scheduled';
        wo.Work_Team__c = wt.Id;
        wo.Early_Arrival_Time__c = date.today();
        wo.Late_Arrival_Time__c = date.today();
        insert wo;        
        Appointment__c app = new Appointment__c(Appointment_Type__c='test',
                                                Eligibility_Check__c=elCheck.Id,
                                                Appointment_Start_Time__c=date.today(),
                                                Appointment_End_Time__c= date.today(),
                                                Appointment_Status__c = 'Unavailable',
                                                Employee__c = emp.Id,
                                                Work_Team__c=wt.Id);
        insert app;        
        
        
        Test.startTest();

        Test.stopTest(); 
    }

}