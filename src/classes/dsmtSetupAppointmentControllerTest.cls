@istest
public class dsmtSetupAppointmentControllerTest 
{
	@istest
    static void test()
    {

        
        Appointment__c App = new Appointment__c();
        //App.Work_Team__c = Wt.Id;
        App.Appointment_Status__c = 'Scheduled';
        App.Appointment_Start_Time__c = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        App.Appointment_End_Time__c = datetime.newInstance(2015, 9, 15, 12, 30, 0);
        insert App;
        
        
        DSMTracker_Contact__c dc =new DSMTracker_Contact__c();
        dc.Name='test';
        insert dc;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
       
        ApexPages.currentPage().getParameters().put('id',proj.id);
        dsmtSetupAppointmentController dsc =new dsmtSetupAppointmentController();
        dsc.saveAppt();
    }
}