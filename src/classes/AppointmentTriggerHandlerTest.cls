@istest
public class AppointmentTriggerHandlerTest 
{
    @istest
    static void test()
    {
        Workorder__c wo =new Workorder__c ();
        insert wo;
        
        Appointment__c app =new Appointment__c ();
        app.Appointment_Status__c='Cancelled';
        app.Workorder__c=wo.id;
        insert app;
            
        list<Appointment__c> newlist = new list<Appointment__c>();
       
        newlist.add(app);
        
        Map<ID, Appointment__c> oldmap = new Map<ID, Appointment__c>(newlist);

        
        AppointmentTriggerHandler ath = new AppointmentTriggerHandler();
        AppointmentTriggerHandler.updateWorkorderCancelled(newlist, oldmap);
    }
}