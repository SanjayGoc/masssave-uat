public class dsmtThermalEnvelopeValidationHelper extends dsmtEnergyAssessmentValidationHelper{
	public ValidationCategory validate(String categoryTitle,String uniqueName,Id eaId,Id bsId,ValidationCategory otherValidations){
		ValidationCategory validations= new ValidationCategory(categoryTitle,uniqueName);

		if(eaId != null){
			Building_Specification__c bs = new Building_Specification__c();
			if(bsId != null){
				List<Building_Specification__c> bss = Database.query('SELECT ' + getSObjectFields('Building_Specification__c') + ' FROM Building_Specification__c WHERE Id=:bsId');
				if(bss.size() > 0){
					bs = bss.get(0);
				}
			}

			Integer i = 0;
			Map<String,ThermalModel> tMap = new Map<String,ThermalModel>();
			for(Thermal_Envelope_Type__c t : [SELECT Id,Name,Thermal_Envelope__c,RecordType.Name FROM Thermal_Envelope_Type__c WHERE Energy_Assessment__c =:eaId]){
				ThermalModel model = new ThermalModel();
				if(tMap.containsKey(t.RecordType.Name.toLowerCase())){
					model = tMap.get(t.RecordType.Name.toLowerCase());
				}
				model.recordsMap.put(t.Id,t);
				tMap.put(t.RecordType.Name.toLowerCase(),model);
			}

			if(tMap.size() > 0){
				/*Set<Id> atticIdSet = new Set<Id>();
				for(String recordTypeName : tMap.keySet()){
					ThermalModel model = tMap.get(recordTypeName);
					if(recordTypeName.equalsIgnoreCase('Attic')){
						atticIdSet.addAll(model.recordsMap.keySet());	
					}
				}*/

				if(tMap.containsKey('attic')){
					Set<Id> atticIdSet = tMap.get('attic').recordsMap.keySet();

					for(Thermal_Envelope_Type__c attic : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ',Attic_Venting__r.Name,Attic_Venting__r.Venting_Amount__c FROM Thermal_Envelope_Type__c WHERE Id IN :atticIdSet')){
						/*if(thermalModel.recordsMap.containsKey(attic.Id)){
							thermalModel.recordsMap.put(attic.Id,attic);
						}

						AtticModel atticModel = new AtticModel();
						if(thermalModel.atticsMap.containsKey(attic.Thermal_Envelope__c)){
							atticModel = thermalModel.atticsMap.get(attic.Thermal_Envelope__c);
						}
						atticModel.recordsMap.put(attic.Id,attic);
						thermalModel.atticsMap.put(attic.Thermal_Envelope__c,atticModel);*/

						ValidationCategory atticValidations = new ValidationCategory(attic.Name,attic.Id);
						validateAttic(atticValidations,attic,otherValidations);
						validations.addCategory(atticValidations);
					}

					for(Ceiling__c roof : Database.query('SELECT ' + '(SELECT '+ getSObjectFields('Layer__c') +',RecordType.Name FROM Layers__r ),' + getSObjectFields('Ceiling__c') + ',Thermal_Envelope_Type__r.RecordTypeId,Thermal_Envelope_Type__r.RecordType.Name,Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Ceiling__c WHERE Thermal_Envelope_Type__c IN :atticIdSet')){
						/*if(thermalModel.atticsMap.containsKey(roof.Thermal_Envelope_Type__r.Thermal_Envelope__c)){
							AtticModel atticModel = thermalModel.atticsMap.get(roof.Thermal_Envelope_Type__r.Thermal_Envelope__c);
							CeilingModel roofModel = new CeilingModel();
							if(atticModel.atticRoofsMap.containsKey(roof.Thermal_Envelope_Type__c)){
								roofModel = atticModel.atticRoofsMap.get(roof.Thermal_Envelope_Type__c);
							}
							roofModel.recordsMap.put(roof.Id,roof);
							atticModel.atticRoofsMap.put(roof.Thermal_Envelope_Type__c,roofModel);
						}*/

						ValidationCategory atticRoofValidations = new ValidationCategory('Attic :: ' + roof.Name,roof.Id);
						validateCeiling(atticRoofValidations,roof,otherValidations,'Attic');
						validations.addCategory(atticRoofValidations);
					}

					for(Wall__c wall : Database.query('SELECT ' + getSObjectFields('Wall__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Wall__c WHERE Thermal_Envelope_Type__c IN :atticIdSet')){
						/*if(thermalModel.atticsMap.containsKey(wall.Thermal_Envelope_Type__r.Thermal_Envelope__c)){
							AtticModel atticModel = thermalModel.atticsMap.get(wall.Thermal_Envelope_Type__r.Thermal_Envelope__c);
							WallModel wallModel = new WallModel();
							if(atticModel.atticWallsMap.containsKey(wall.Thermal_Envelope_Type__c)){
								wallModel = atticModel.atticWallsMap.get(wall.Thermal_Envelope_Type__c);
							}
							wallModel.recordsMap.put(wall.Id,wall);
							atticModel.atticWallsMap.put(wall.Thermal_Envelope_Type__c,wallModel);
						}*/

						ValidationCategory atticWallValidations = new ValidationCategory(wall.Name,wall.Id);
						validateWall(atticWallValidations,wall,otherValidations,'Attic');
						validations.addCategory(atticWallValidations);
					}

					for(Floor__c floor : Database.query('SELECT ' + getSObjectFields('Floor__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Floor__c WHERE Thermal_Envelope_Type__c IN :atticIdSet')){
						/*if(thermalModel.atticsMap.containsKey(floor.Thermal_Envelope_Type__r.Thermal_Envelope__c)){
							AtticModel atticModel = thermalModel.atticsMap.get(floor.Thermal_Envelope_Type__r.Thermal_Envelope__c);
							FloorModel floorModel = new FloorModel();
							if(atticModel.atticFloorsMap.containsKey(floor.Thermal_Envelope_Type__c)){
								floorModel = atticModel.atticFloorsMap.get(floor.Thermal_Envelope_Type__c);
							}
							floorModel.recordsMap.put(floor.Id,floor);
							atticModel.atticFloorsMap.put(floor.Thermal_Envelope_Type__c,floorModel);
						}*/

						ValidationCategory atticFloorValidations = new ValidationCategory(floor.Name,floor.Id);
						validateFloor(atticFloorValidations,floor,otherValidations,'Attic',bs);
						validations.addCategory(atticFloorValidations);
					}

					for(Attic_Access_and_Whole_House_Fan__c awhf : Database.query('SELECT '+getSObjectFields('Attic_Access_and_Whole_House_Fan__c') + ' FROM Attic_Access_and_Whole_House_Fan__c WHERE Thermal_Envelope_Type__c IN :atticIdSet')){
						ValidationCategory awhfValidations = new ValidationCategory(awhf.Name,awhf.Id);
						validateAtticAQHF(awhfValidations,awhf,otherValidations);
						validations.addCategory(awhfValidations);
					}
				}
				if(tMap.containsKey('basement')){
					Set<Id> basementIds = tMap.get('basement').recordsMap.keySet();
					for(Thermal_Envelope_Type__c basement : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :basementIds')){
						ValidationCategory basementValidations = new ValidationCategory(basement.Name,basement.Id);
						validateBasement(basementValidations,basement,otherValidations);
						validations.addCategory(basementValidations);
					}

					for(Ceiling__c ceiling : Database.query('SELECT ' + getSObjectFields('Ceiling__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Ceiling__c WHERE Thermal_Envelope_Type__c IN :basementIds')){
						ValidationCategory ceilingValidations = new ValidationCategory('Basement :: '+ ceiling.Name,ceiling.Id);
						validateCeiling(ceilingValidations,ceiling,otherValidations,'Basement');
						validations.addCategory(ceilingValidations);
					}

					for(Wall__c wall : Database.query('SELECT ' + getSObjectFields('Wall__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Wall__c WHERE Thermal_Envelope_Type__c IN :basementIds')){
						ValidationCategory wallValidations = new ValidationCategory(wall.Name,wall.Id);
						validateWall(wallValidations,wall,otherValidations,'Basement');
						validations.addCategory(wallValidations);
					}

					for(Floor__c floor : Database.query('SELECT ' + getSObjectFields('Floor__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Floor__c WHERE Thermal_Envelope_Type__c IN :basementIds')){
						ValidationCategory floorValidations = new ValidationCategory(floor.Name,floor.Id);
						validateFloor(floorValidations,floor,otherValidations,'Basement',bs);
						validations.addCategory(floorValidations);
					}
				}
				if(tMap.containsKey('crawlspace')){
					Set<Id> crawlspaceIds = tMap.get('crawlspace').recordsMap.keySet();
					for(Thermal_Envelope_Type__c crawlspace : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :crawlspaceIds')){
						ValidationCategory crawlspaceValidations = new ValidationCategory(crawlspace.Name,crawlspace.Id);
						validateBasement(crawlspaceValidations,crawlspace,otherValidations);
						validations.addCategory(crawlspaceValidations);
					}

					for(Ceiling__c ceiling : Database.query('SELECT ' + getSObjectFields('Ceiling__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Ceiling__c WHERE Thermal_Envelope_Type__c IN :crawlspaceIds')){
						ValidationCategory ceilingValidations = new ValidationCategory('Crawlspace :: '+ ceiling.Name,ceiling.Id);
						validateCeiling(ceilingValidations,ceiling,otherValidations,'Crawlpace');
						validations.addCategory(ceilingValidations);
					}

					for(Wall__c wall : Database.query('SELECT ' + getSObjectFields('Wall__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Wall__c WHERE Thermal_Envelope_Type__c IN :crawlspaceIds')){
						ValidationCategory wallValidations = new ValidationCategory(wall.Name,wall.Id);
						validateWall(wallValidations,wall,otherValidations,'Crawlspace');
						validations.addCategory(wallValidations);
					}

					for(Floor__c floor : Database.query('SELECT ' + getSObjectFields('Floor__c') + ',Thermal_Envelope_Type__r.Thermal_Envelope__c FROM Floor__c WHERE Thermal_Envelope_Type__c IN :crawlspaceIds')){
						ValidationCategory floorValidations = new ValidationCategory(floor.Name,floor.Id);
						validateFloor(floorValidations,floor,otherValidations,'Crawlspace',bs);
						validations.addCategory(floorValidations);
					}
				}
				if(tMap.containsKey('exterior wall')){
					Set<Id> exteriorWallIds = tMap.get('exterior wall').recordsMap.keySet();
					for(Thermal_Envelope_Type__c exteriorWall : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :exteriorWallIds')){
						ValidationCategory exteriorWallValidations = new ValidationCategory(exteriorWall.Name,exteriorWall.Id);
						validateExteriorWall(exteriorWallValidations,exteriorWall,otherValidations);
						validations.addCategory(exteriorWallValidations);
					}
				}
				if(tMap.containsKey('garage ceiling')){
					Set<Id> garageCeilingIds = tMap.get('garage ceiling').recordsMap.keySet();
					for(Thermal_Envelope_Type__c garageCeiling : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :garageCeilingIds')){
						ValidationCategory garageCeilingValidations = new ValidationCategory(garageCeiling.Name,garageCeiling.Id);
						validateGarageCeiling(garageCeilingValidations,garageCeiling,otherValidations);
						validations.addCategory(garageCeilingValidations);
					}
				}
				if(tMap.containsKey('garage wall')){
					Set<Id> garageWallIds = tMap.get('garage wall').recordsMap.keySet();
					for(Thermal_Envelope_Type__c garageWall : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :garageWallIds')){
						ValidationCategory garageWallValidations = new ValidationCategory(garageWall.Name,garageWall.Id);
						validateGarageWall(garageWallValidations,garageWall,otherValidations);
						validations.addCategory(garageWallValidations);
					}
				}
				if(tMap.containsKey('overhang')){
					Set<Id> overhangIds = tMap.get('overhang').recordsMap.keySet();
					for(Thermal_Envelope_Type__c overhang : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :overhangIds')){
						ValidationCategory overhangValidations = new ValidationCategory(overhang.Name,overhang.Id);
						validateGarageCeiling(overhangValidations,overhang,otherValidations);
						validations.addCategory(overhangValidations);
					}
				}
				if(tMap.containsKey('floor over other dwelling')){
					Set<Id> foodIds = tMap.get('floor over other dwelling').recordsMap.keySet();
					for(Thermal_Envelope_Type__c food : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :foodIds')){
						ValidationCategory foodValidations = new ValidationCategory(food.Name,food.Id);
						validateFloorOverOtherDwelling(foodValidations,food,otherValidations);
						validations.addCategory(foodValidations);
					}
				}
				if(tMap.containsKey('slab')){
					Set<Id> slabIds = tMap.get('slab').recordsMap.keySet();
					for(Thermal_Envelope_Type__c slab : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :slabIds')){
						ValidationCategory slabValidations = new ValidationCategory(slab.Name,slab.Id);
						validateSlab(slabValidations,slab,otherValidations);
						validations.addCategory(slabValidations);
					}
				}
				if(tMap.containsKey('vaulted roof')){
					Set<Id> vaultedRoofIds = tMap.get('raulted roof').recordsMap.keySet();
					for(Thermal_Envelope_Type__c vaultedRoof : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :vaultedRoofIds')){
						ValidationCategory vaultedRoofValidations = new ValidationCategory(vaultedRoof.Name,vaultedRoof.Id);
						validateVaultedRoof(vaultedRoofValidations,vaultedRoof,otherValidations);
						validations.addCategory(vaultedRoofValidations);
					}
				}
				if(tMap.containsKey('windows & skylights')){
					Set<Id> windowIds = tMap.get('windows & skylights').recordsMap.keySet();
					for(Thermal_Envelope_Type__c window : Database.query('SELECT ' + getSObjectFields('Thermal_Envelope_Type__c') + ' FROM Thermal_Envelope_Type__c WHERE Id IN :windowIds')){
						ValidationCategory windowsValidations = new ValidationCategory(window.Name,window.Id);
						validateWindows(windowsValidations,window,otherValidations);
						validations.addCategory(windowsValidations);
					}
				}
			}
		}

		return validations;
	}

	public void validateLayer(ValidationCategory validations,Layer__c layer,ValidationCategory otherValidations){
		Set<String> recordTypeNames = new Set<String>{
			'Roofing','Siding','Insulation','Masonry','Flooring'
		};
		if(recordTypeNames.contains(layer.RecordType.Name)){
			if(layer.Material_Type__c == null || String.valueOf(layer.Material_Type__c).trim() == ''){
				validations.errors.add(new ValidationMessage('Material type is required.'));
			}
		}
		if(layer.RecordType.Name.equalsIgnoreCase('Wood Frame')){
			if(Layer.Framing_Type__c == null || String.valueOf(Layer.Framing_Type__c).trim() == ''){
				validations.errors.add(new ValidationMessage('Framing type is required.'));	
			}
			if(Layer.Insul_Amount__c != null && (Layer.Gaps__c == null || String.valueOf(Layer.Gaps__c).trim() == '')){
				validations.errors.add(new ValidationMessage('Gaps are required.'));	
			}
			if(Layer.Insul_Amount__c != null && String.valueOf(Layer.Insul_Amount__c).trim() == '' && (Layer.Cavity_Insul_Type__c == null || String.valueOf(Layer.Cavity_Insul_Type__c).trim() == '')){
				validations.errors.add(new ValidationMessage('Cavity insulation type is required.'));	
			}
		}
		
	}

	public void validateCeiling(ValidationCategory validations,Ceiling__c roof,ValidationCategory otherValidations,String parentRecordType){
		if(roof.Area__c == null || String.valueOf(roof.Area__c).trim() == '' || roof.Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(roof.Area__c != null && roof.Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}

		if(parentRecordType.equalsIgnoreCase('Attic')){
			if(roof.Slope__c != null && roof.Slope__c > 30){
				validations.errors.add(new ValidationMessage('Slope must be less than equal to 30.'));
			}
			if(roof.Reflectivity__c == null && String.valueOf(roof.Reflectivity__c).trim() == ''){
				validations.errors.add(new ValidationMessage('Reflectivity is required.'));
			}
		}
		if(parentRecordType.equalsIgnoreCase('Attic') || parentRecordType.equalsIgnoreCase('Basement') || parentRecordType.equalsIgnoreCase('Crawlspace')){
			if((roof.Insul_Amount__c == null || String.valueOf(roof.Insul_Amount__c).trim() == '' || roof.Insul_Amount__c.equalsIgnoreCase('none')) && (roof.Gaps__c == null || String.valueof(roof.Gaps__c).trim() == '')){
				validations.errors.add(new ValidationMessage('Gaps are required'));
			}
		}

		if(parentRecordType.equalsIgnoreCase('Basement') || parentRecordType.equalsIgnoreCase('Crawlspace')){
			if(roof.Perimeter__c != null && roof.Perimeter__c > 600){
				validations.errors.add(new ValidationMessage('Perimeter must be less than equal to 600'));
			}
		}

		if(roof.Layers__r != null && roof.Layers__r.size() > 0){
			for(Layer__c layer : roof.Layers__r){
				ValidationCategory layerValidations = new ValidationCategory(layer.Name,layer.Id);
				validateLayer(layerValidations,layer,otherValidations);
				validations.addCategory(layerValidations);
			}
		}

	}
	public void validateWall(ValidationCategory validations,Wall__c wall,ValidationCategory otherValidations,String parentRecordType){
		if(wall.Area__c == null || String.valueOf(wall.Area__c).trim() == '' || wall.Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(wall.Area__c != null && wall.Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if(parentRecordType.equalsIgnoreCase('Attic') || parentRecordType.equalsIgnoreCase('Basement') || parentRecordType.equalsIgnoreCase('Crawlspace')){
			if((wall.Insul_Amount__c == null || String.valueOf(wall.Insul_Amount__c).trim() == '' || wall.Insul_Amount__c.equalsIgnoreCase('none')) && (wall.Gaps__c == null || String.valueof(wall.Gaps__c).trim() == '')){
				validations.errors.add(new ValidationMessage('Gaps are required'));
			}
		}
		if(parentRecordType.equalsIgnoreCase('Basement')){
			if(wall.Width__c == null || String.valueOf(wall.Width__c).trim() == '' || wall.Width__c < 1){
				validations.errors.add(new ValidationMessage('Length must be greater than equal to 1'));
			}
			if(wall.Exposed_ft__c == null || String.valueOf(wall.Exposed_ft__c).trim() == '' || wall.Exposed_ft__c < 1){
				validations.errors.add(new ValidationMessage('Exposed (ft) must be greater than equal to 1'));
			}else if(wall.Exposed_ft__c != null && wall.Height__c != null && wall.Exposed_ft__c > wall.Height__c){
				validations.errors.add(new ValidationMessage('Exposed (ft) must be less than equal to ' + (wall.Height__c - 1)));
			}
		}
		if(parentRecordType.equalsIgnoreCase('Crawlspace')){
			if(wall.Width__c == null || String.valueOf(wall.Width__c).trim() == '' || wall.Width__c < 1){
				validations.errors.add(new ValidationMessage('Length must be greater than equal to 1'));
			}
			if(wall.Type__c != null && wall.Type__c.equalsIgnoreCase('Basement Rim Joist')){
				if(wall.Width__c != null && wall.Width__c > 400){
					validations.errors.add(new ValidationMessage('Length (ft) must be less than equal to 400'));
				}
				if(wall.Height__c == null || String.valueOf(wall.Height__c).trim() == '' || wall.Height__c < 1){
					validations.errors.add(new ValidationMessage('Height (in) must be greater than equal to 1'));
				}else if(wall.Height__c != null && wall.Height__c > 36){
					validations.errors.add(new ValidationMessage('Height (in) must be less than equal to 36'));
				}
			}

			if(wall.Exposed_ft__c == null || String.valueOf(wall.Exposed_ft__c).trim() == '' || wall.Exposed_ft__c < 1){
				validations.errors.add(new ValidationMessage('Exposed (ft) must be greater than equal to 1'));
			}else if(wall.Exposed_ft__c != null && wall.Height__c != null && wall.Exposed_ft__c > wall.Height__c){
				validations.errors.add(new ValidationMessage('Exposed (ft) must be less than equal to ' + (wall.Height__c - 1)));
			}

		}
	}
	public void validateFloor(ValidationCategory validations,Floor__c floor,ValidationCategory otherValidations,String parentRecordType,Building_Specification__c bs){
		if(floor.Area__c == null || String.valueOf(floor.Area__c).trim() == '' || floor.Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(parentRecordType.equalsIgnoreCase('Crawlspace')){
			if(floor.Area__c != null && bs.Floor_Area__c != null && floor.Area__c > bs.Floor_Area__c){
				validations.errors.add(new ValidationMessage('Area must be less than <FloorArea>'.replace('<FloorArea>',String.valueOf(bs.Floor_Area__c))));
			}
		}else{
			if(floor.Area__c != null && floor.Area__c > 15000){
				validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
			}
		}
		if(parentRecordType.equalsIgnoreCase('Basement')){
			if(floor.Perimeter__c != null && floor.Perimeter__c > 600){
				validations.errors.add(new ValidationMessage('Perimeter must be less than equal to 600'));
			}
		}
		
	}
	public void validateAtticAQHF(ValidationCategory validations,Attic_Access_and_Whole_House_Fan__c awhf,ValidationCategory otherValidations){
		if(awhf.Location__c == null || String.valueOf(awhf.Location__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Location is required'));
		}
		if(awhf.Access_Type__c == null || String.valueOf(awhf.Access_Type__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Accesses Type is required'));	
		}
		if(awhf.Area__c == null || String.valueOf(awhf.Area__c).trim() == '' || awhf.Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(awhf.Area__c != null && awhf.Area__c > 30){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 30.'));
		}
		if(awhf.Insulated__c == true){
			if(awhf.Insulation_Thickness_inch__c == null || String.valueOf(awhf.Insulation_Thickness_inch__c).trim() == ''){
				validations.errors.add(new ValidationMessage('Insulation Thickness is Required.'));
			}else if(awhf.Insulation_Thickness_inch__c != null && awhf.Insulation_Thickness_inch__c < 1){
				validations.errors.add(new ValidationMessage('Insulation Thickness must be greater than or equal to 1.'));
			}else if(awhf.Insulation_Thickness_inch__c != null && awhf.Insulation_Thickness_inch__c > 12){
				validations.errors.add(new ValidationMessage('Insulation Thickness must be less than or equal to 12.'));
			}
		}
	}

	/* THERMAL ENVELOPE TYPE VALIDATIONS */
	public void validateAttic(ValidationCategory validations, Thermal_Envelope_Type__c attic,ValidationCategory otherValidations){
		if(attic.SpaceConditioning__c == null || String.valueOf(attic.SpaceConditioning__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Space Conditioned is required.'));
		}
		if(attic.Attic_Venting__c != null && attic.Attic_Venting__r != null){
			ValidationCategory atticVentingValidations = new ValidationCategory(validations.title + ' :: ' + attic.Attic_Venting__r.Name,attic.Attic_Venting__c);
			if(attic.Attic_Venting__r.Venting_Amount__c == null || String.valueOf(attic.Attic_Venting__r.Venting_Amount__c).trim() == ''){
				atticVentingValidations.errors.add(new ValidationMessage('Venting Amount is required.'));
			}
			validations.addCategory(atticVentingValidations);
		}
	}
	public void validateBasement(ValidationCategory validations, Thermal_Envelope_Type__c basement,ValidationCategory otherValidations){
		if(basement.SpaceConditioning__c == null || String.valueOf(basement.SpaceConditioning__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Space Conditioned is required.'));
		}
	}
	public void validateCrawlspace(ValidationCategory validations, Thermal_Envelope_Type__c crawlspace,ValidationCategory otherValidations){
		if(crawlspace.SpaceConditioning__c == null || String.valueOf(crawlspace.SpaceConditioning__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Space Conditioned is required.'));
		}
	}

	public void validateExteriorWall(ValidationCategory validations, Thermal_Envelope_Type__c exteriorWall,ValidationCategory otherValidations){
		if(exteriorWall.Exterior_Area__c == null || String.valueOf(exteriorWall.Exterior_Area__c).trim() == '' || exteriorWall.Exterior_Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(exteriorWall.Exterior_Area__c != null && exteriorWall.Exterior_Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if((exteriorWall.Insul_Amount__c == null || String.valueOf(exteriorWall.Insul_Amount__c).trim() == '' || exteriorWall.Insul_Amount__c.equalsIgnoreCase('none')) 
			&& (exteriorWall.Gaps__c == null || String.valueof(exteriorWall.Gaps__c).trim() == '')){
			validations.errors.add(new ValidationMessage('Gaps are required'));
		}
	}
	public void validateGarageCeiling(ValidationCategory validations, Thermal_Envelope_Type__c garageCeiling,ValidationCategory otherValidations){
		if(garageCeiling.Exterior_Area__c == null || String.valueOf(garageCeiling.Exterior_Area__c).trim() == '' || garageCeiling.Exterior_Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(garageCeiling.Exterior_Area__c != null && garageCeiling.Exterior_Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if((garageCeiling.Insul_Amount__c == null || String.valueOf(garageCeiling.Insul_Amount__c).trim() == '' || garageCeiling.Insul_Amount__c.equalsIgnoreCase('none')) 
			&& (garageCeiling.Gaps__c == null || String.valueof(garageCeiling.Gaps__c).trim() == '')){
			validations.errors.add(new ValidationMessage('Gaps are required'));
		}
	}
	public void validateGarageWall(ValidationCategory validations, Thermal_Envelope_Type__c garageWall,ValidationCategory otherValidations){
		if(garageWall.Exterior_Area__c == null || String.valueOf(garageWall.Exterior_Area__c).trim() == '' || garageWall.Exterior_Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(garageWall.Exterior_Area__c != null && garageWall.Exterior_Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if((garageWall.Insul_Amount__c == null || String.valueOf(garageWall.Insul_Amount__c).trim() == '' || garageWall.Insul_Amount__c.equalsIgnoreCase('none')) 
			&& (garageWall.Gaps__c == null || String.valueof(garageWall.Gaps__c).trim() == '')){
			validations.errors.add(new ValidationMessage('Gaps are required'));
		}
	}
	public void validateOverhang(ValidationCategory validations, Thermal_Envelope_Type__c overhang,ValidationCategory otherValidations){
		if(overhang.Exterior_Area__c == null || String.valueOf(overhang.Exterior_Area__c).trim() == '' || overhang.Exterior_Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(overhang.Exterior_Area__c != null && overhang.Exterior_Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if((overhang.Insul_Amount__c == null || String.valueOf(overhang.Insul_Amount__c).trim() == '' || overhang.Insul_Amount__c.equalsIgnoreCase('none')) 
			&& (overhang.Gaps__c == null || String.valueof(overhang.Gaps__c).trim() == '')){
			validations.errors.add(new ValidationMessage('Gaps are required'));
		}
	}
	public void validateFloorOverOtherDwelling(ValidationCategory validations, Thermal_Envelope_Type__c food,ValidationCategory otherValidations){
		if(food.Exterior_Area__c == null || String.valueOf(food.Exterior_Area__c).trim() == '' || food.Exterior_Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(food.Exterior_Area__c != null && food.Exterior_Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if((food.Insul_Amount__c == null || String.valueOf(food.Insul_Amount__c).trim() == '' || food.Insul_Amount__c.equalsIgnoreCase('none')) 
			&& (food.Gaps__c == null || String.valueof(food.Gaps__c).trim() == '')){
			validations.errors.add(new ValidationMessage('Gaps are required'));
		}
	}
	public void validateSlab(ValidationCategory validations, Thermal_Envelope_Type__c slab,ValidationCategory otherValidations){
		if(slab.Exterior_Area__c == null || String.valueOf(slab.Exterior_Area__c).trim() == '' || slab.Exterior_Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(slab.Exterior_Area__c != null && slab.Exterior_Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if(slab.Perimeter__c != null && slab.Perimeter__c > 600){
			validations.errors.add(new ValidationMessage('Perimeter must be less than equal to 600'));
		}
		if(slab.Edge_R_Value__c != null && slab.Edge_R_Value__c > 20){
			validations.errors.add(new ValidationMessage('Edge R value must be less than 20'));
		}
		if((slab.Insul_Amount__c == null || String.valueOf(slab.Insul_Amount__c).trim() == '' || slab.Insul_Amount__c.equalsIgnoreCase('none')) 
			&& (slab.Gaps__c == null || String.valueof(slab.Gaps__c).trim() == '')){
			validations.errors.add(new ValidationMessage('Gaps are required'));
		}
		if(slab.SlabInsulationDepth__c != null && slab.SlabInsulationDepth__c > 24){
			validations.errors.add(new ValidationMessage('Insulation Dept value must be less than 24'));
		}
	}
	public void validateVaultedRoof(ValidationCategory validations, Thermal_Envelope_Type__c vaultedRoof,ValidationCategory otherValidations){
		if(vaultedRoof.Exterior_Area__c == null || String.valueOf(vaultedRoof.Exterior_Area__c).trim() == '' || vaultedRoof.Exterior_Area__c < 1){
			validations.errors.add(new ValidationMessage('Area must be greater than equal to 1.'));
		}
		if(vaultedRoof.Exterior_Area__c != null && vaultedRoof.Exterior_Area__c > 15000){
			validations.errors.add(new ValidationMessage('Area Must be less than equal to 15000.'));
		}
		if(vaultedRoof.Slope__c != null && vaultedRoof.Slope__c > 30){
			validations.errors.add(new ValidationMessage('Slope must be less than equal to 30.'));
		}
		if((vaultedRoof.Insul_Amount__c == null || String.valueOf(vaultedRoof.Insul_Amount__c).trim() == '' || vaultedRoof.Insul_Amount__c.equalsIgnoreCase('none')) 
			&& (vaultedRoof.Gaps__c == null || String.valueof(vaultedRoof.Gaps__c).trim() == '')){
			validations.errors.add(new ValidationMessage('Gaps are required'));
		}
		if(vaultedRoof.Reflectivity__c == null && String.valueOf(vaultedRoof.Reflectivity__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Reflectivity is required.'));
		}
	}
	public void validateWindows(ValidationCategory validations, Thermal_Envelope_Type__c window, ValidationCategory otherValidations){
		if(window.Single_Pane_Windows__c == null || String.valueOf(window.Single_Pane_Windows__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Count of Single Pane Windows is required.'));
		}
		if(window.Window_Type__c == null || String.valueOf(window.Window_Type__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Type of primary window must be defined.'));
		}
	}
}