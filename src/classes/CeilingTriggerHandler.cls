public class CeilingTriggerHandler extends TriggerHandler implements ITrigger{
    
    public void bulkBefore() 
    {
        if (trigger.isInsert) 
        {
            calculateDefaultValues(trigger.new, (Map<Id, Ceiling__c>) trigger.oldmap, trigger.isInsert);
            //DSST-6924: Changes made by DeveloperName – Begin
            dsmtRecursiveTriggerHandler.preventCeilingUpdateAfterInsert = true;
        }
        else if (trigger.isUpdate && dsmtRecursiveTriggerHandler.preventCeilingUpdateAfterInsert == false) 
        {
            //DSST-6924: Changes made by DeveloperName – End
            calculateDefaultValues(trigger.new, (Map<Id, Ceiling__c>) trigger.oldmap, trigger.isInsert);
        } 
        else if (trigger.isDelete) 
        {
            deleteLayers(trigger.oldmap.keyset());
        } 
        else if (trigger.isUndelete) 
        {
            
        }
    }

    public void bulkAfter() 
    {
        if (trigger.isInsert) 
        {
            CreateDefaultLayer(trigger.new);
        } 
        else if (trigger.isUpdate) 
        {
            
        } 
        else if (trigger.isDelete) 
        {
            
        } 
        else if (trigger.isUndelete) 
        {
            
        }
    }

    private static void ManageActuals(Ceiling__c ceiling)
    {
        if(ceiling.ActualSlope__c <=0) ceiling.ActualSlope__c=null;
        if(ceiling.ActualArea__c <=0) ceiling.ActualArea__c=null;
        if(ceiling.ActualPerimeter__c <=0) ceiling.ActualPerimeter__c=null;
        if(ceiling.ActualProjectedArea__c<=0)ceiling.ActualProjectedArea__c=null;
        if(ceiling.ActualInsulationAmount__c == '') ceiling.ActualInsulationAmount__c = null;
    }
    private static void CreateDefaultLayer(List<Ceiling__c> ceilList)
    {
        Map<Id,string> recTypeMap = dsmtHelperClass.GetRecordType();

        dsmtEAModel.Surface bp = new dsmtEAModel.Surface();        
        bp.recordTypeMap = recTypeMap;

        Set<Id> setCeilId = new Set<Id>();
        Set<Id> EaId = new Set<Id>();
        Set<Id> setTETId = new Set<Id>();

        for(Ceiling__c te : ceilList)
        {
            if(te.IsFromFieldTool__c) continue;

            if(te.Thermal_Envelope_Type__c !=null)
                setTETId.add(te.Thermal_Envelope_Type__c);
        }

        if(setTETId ==null || setTETId.size() ==0) return;

        String query = 'select ' + dsmtHelperClass.sObjectFields('Thermal_Envelope_Type__c') +' Id from Thermal_Envelope_Type__c where Id In :setTETId order by CreatedDate Asc ';

        Map<Id,Thermal_Envelope_Type__c> tetMap = new Map<Id,Thermal_Envelope_Type__c> ((List<Thermal_Envelope_Type__c>)database.query(query));
        
        if(tetMap ==null || tetMap.size()==0)return;

        for(Ceiling__c te : ceilList)
        {
            if(te.IsFromFieldTool__c) continue;
            if(te.Thermal_Envelope_Type__c ==null) continue;

            bp.teType = tetMap.get(te.Thermal_Envelope_Type__c);
            bp.ceiling = te;

            dsmtSurfaceHelper.CreateNewLayers(bp);
        }        

    }
    
    private static void calculateDefaultValues(list<Ceiling__c> newList, Map<Id, Ceiling__c> oldMap, boolean isInsert)
    {
        try
        {            
            Set<Id> setCeilId = new Set<Id>();
            Set<Id> EaId = new Set<Id>();
            Set<Id> setTETId = new Set<Id>();

            for(Ceiling__c te : newList)
            {
                if(te.Id!=null)
                    setCeilId.add(te.Id);

                if(te.Thermal_Envelope_Type__c !=null)
                    setTETId.add(te.Thermal_Envelope_Type__c);
            }

            if(setTETId ==null || setTETId.size() ==0) return;

            Map<Id,Thermal_Envelope_Type__c> tetMap = new Map<Id,Thermal_Envelope_Type__c> ([Select Id, Energy_Assessment__c From 
            Thermal_Envelope_Type__c Where Id in :setTETId]);

            if(tetMap ==null || tetMap.size() ==0) return;

            For(Id key : setTETId)
            {
                if(tetMap.get(key)==null)continue;
                if(tetMap.get(key).Energy_Assessment__c ==null)continue;
                EaId.add(tetMap.get(key).Energy_Assessment__c);
            }

            dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(EaId);

            if(bp==null) return;
                
            
            if(bp.teList !=null && bp.teList.size() >0)
            {
                bp.AtticList  = new List<Thermal_Envelope_Type__c>();
                for(Thermal_Envelope_Type__c te : bp.teList)
                {   
                    if(bp.recordTypeMap.get(te.RecordTypeID) == 'Attic')
                    {
                        bp.AtticList.add(te);
                    }
                }    
            }

             // consutruct temprature and building model
            bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
            bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

            /// update base model first
           // bp = dsmtBuildingModelHelper.UpdateBaseBuildingThermal(bp);
            bp = dsmtBuildingModelHelper.UpdateThermalAreaModel(bp);
            bp.ai = dsmtBuildingModelHelper.UpdateAirFlowModel(bp.ai, bp).clone(true,true,true,true);

            bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
            bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

            integer ceilingCount = newList.size();
            if(isInsert)
            {
                if(bp.ceilList ==null) bp.ceilList = new List<Ceiling__c>();

                For(integer i=0 ;i< ceilingCount; i++)
                {
                    bp.ceilList.add(newList[i]);
                }
            }
            else 
            {
                For(integer i=0 ;i< ceilingCount; i++)
                {
                    if(bp.ceilList ==null) bp.ceilList = new List<Ceiling__c>();

                    if(bp.ceilList.size() ==0)
                    {
                        bp.ceilList.add(newList[i]);
                    }
                    else 
                    {
                        boolean isFound=false;
                        for(integer j=0; j<bp.ceilList.size();j++)
                        {
                            if(bp.ceilList[j].Id==newList[i].Id)
                            {
                                bp.ceilList[j] = newList[i];
                                isFound=true;
                                break;
                            }
                        }

                        if(!isFound)
                        {
                            bp.ceilList.add(newList[i]);
                        }
                    }
                }    
            }

            tetMap = new Map<Id,Thermal_Envelope_Type__c>();

            if(bp.teList !=null && bp.teList.size()>0)
            {
                for(integer i=0; i<bp.teList.size();i++)
                {
                    tetMap.put(bp.teList[i].Id,bp.teList[i]);
                }
            }
                
            for(Ceiling__c tet: newList)
            {
                if(tet.IsFromFieldTool__c) continue;
                if(tet.Thermal_Envelope_Type__c ==null) continue;

                if(tetMap !=null && tetMap.size()>0)
                {
                    bp.teType = tetMap.get(tet.Thermal_Envelope_Type__c);
                }

                if(bp.teType ==null)continue;

                
                if(tet.Id !=null)
                {
                    if(bp.allLayerMap !=null && bp.allLayerMap.size()>0)
                    {
                        bp.layers = bp.allLayerMap.get(tet.Id);
                    }
                }

                bp.layers = dsmtBuildingModelHelper.UpdateLayerModel(bp.layers,bp);

                Id parentId = tet.Thermal_Envelope_Type__c;
                string recTypeName = bp.RecordTypeMap.get(bp.teType.RecordTypeId);
                    
                bp.Ceiling = tet;

                ManageActuals(tet);

                tet.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);

                bp.Location = tet.Location__c;
                tet.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
                bp.LocationSpace = tet.LocationSpace__c;
                bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
                bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
                bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

                tet.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
                bp.LocationType = tet.LocationType__c;
                
                tet.Space_Type__c = dsmtSurfaceHelper.GetSurfaceSpaceType().get(recTypeName + ' Ceiling');

                tet.FloorType__c = dsmtSurfaceHelper.ComputeFloorTypeCommon(bp);
                tet.CeilingType__c = dsmtSurfaceHelper.ComputeCeilingTypeCommon(bp);                
                tet.FloorCeilingType__c = dsmtSurfaceHelper.ComputeFloorCeilingTypeCommon(bp);
                tet.Type__c = tet.FloorCeilingType__c;
                tet.Has_Trusses__c = bp.teType.Trusses__c;
                    
                //boolean isInsert = isInsert;
                if(bp.teType.ChangeType__c)
                {
                    isInsert = true;
                    tet.Name = '';
                }
                    
                if(tet.Name == '' || tet.Name == null)
                {
                    tet.Name = tet.Type__c;
                    if(recTypeName == 'Crawlspace')
                    {
                        tet.Name = 'Crawlspace Ceiling';
                    }
                }

                // DSST-6348 - Cavity Insulation Depth error
                // #Code Block Start
                if(bp.layers !=null && bp.layers.size() > 0)
                {
                    Layer__c wfLayer = dsmtSurfaceHelper.GetFirstFramingLayer(bp.layers,1);
                    if(wfLayer !=null)
                    {
                        if(wfLayer.ActualCavityInsulationDepth__c !=null && wfLayer.ActualCavityInsulationDepth__c !=0)
                        {
                            tet.ActualInsulationAmount__c = null;
                        }
                    }
                }

                if(tet.ActualInsulationAmount__c == null)
                {
                    tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
                }    
                // #Code Block End

                if(isInsert)
                {
                    if(tet.Add__c==null || tet.Add__c == '') 
                    {
                        if(recTypeName == 'Attic')
                        {
                            tet.Add__c = 'Unfinished';
                        }
                    }
    
                    if(tet.Roof_To__c == null || tet.Roof_To__c == '') tet.Roof_To__c = 'Exterior';
                    
                    if(recTypeName == 'Basement' || recTypeName == 'Crawlspace')
                    {
                        tet.Roof_To__c = 'Living Space';
                    }

                    if(tet.Exposed_To__c == null || tet.Exposed_To__c == '')
                        tet.Exposed_To__c = tet.Roof_To__c;

                    //tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
                    tet.Reflectivity__c = dsmtSurfaceHelper.ComputeFloorCeilingReflectivityCommon(tet.FloorType__c);
                }

                tet.Width__c = bp.EaObj.FrontLength__c;

                if(tet.ActualSlope__c ==null)
                    tet.Slope__c = dsmtSurfaceHelper.ComputeFloorCeilingSlopeCommon(bp);

                tet.Length__c = dsmtSurfaceHelper.ComputeFloorCeilingLengthCommon(bp);
    
                if(tet.ActualArea__c ==null)
                    tet.Area__c = dsmtSurfaceHelper.ComputeSurfaceAreaCommon(bp);

                 if(tet.ActualPerimeter__c == null)
                    tet.Perimeter__c = dsmtSurfaceHelper.ComputeFloorCeilingPerimeterCommon(bp);
                    
                if(tet.ActualProjectedArea__c==null)
                    tet.Projected_Area__c = dsmtSurfaceHelper.ComputeFloorCeilingProjectedAreaCommon(bp);

                //if(tet.ActualInsulationAmount__c ==null)
                    //tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
                
                if(tet.Gaps__c ==null)
                    tet.Gaps__c = Saving_Constant__c.getOrgDefaults().DefaultInsulationGaps__c;

                tet.ReflectivityFactor__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofReflectivityFactorCommon(tet.Reflectivity__c);
                tet.RoofVentCFMNat__c =dsmtSurfaceHelper.ComputeFloorCeilingRoofVentCFMNatCommon(bp);
                tet.RoofVentCFMH__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofVentCFMHCommon(bp);
                tet.SolarGainTempAddH__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofSolarGainTempAddCommon(bp,'H');
                tet.SolarGainTempAddC__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofSolarGainTempAddCommon(bp,'C');
                tet.TSolairH__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofTSolairCommon(bp,'H');
                tet.TSolairC__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofTSolairCommon(bp,'C');
                tet.TSolairDesignC__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofTSolairDesignCCommon(bp);
                tet.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'H');
                tet.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'C');
                
                tet.Net_Area__c = dsmtSurfaceHelper.ComputeAreaNet(bp);
                tet.RValueConst__c = dsmtSurfaceHelper.ComputeRValueConst(bp);
                tet.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(bp);  
                tet.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveRValueCommon(bp);   
                tet.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveNominalRValueCommon(bp); 
                tet.UA__c = dsmtSurfaceHelper.ComputeFloorCeilingUACommon(bp); 
                tet.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(tet.R_Value__c);
               
                tet.LoadH__c = dsmtSurfaceHelper.ComputeFloorCeilingLoadCommon(bp, 'H');
                tet.LoadC__c = dsmtSurfaceHelper.ComputeFloorCeilingLoadCommon(bp, 'C');
            }            
            
        }
        catch(Exception ex)
        {
            dsmtHelperClass.WriteMessage(ex);
        }
    }
    
    public static void deleteLayers(Set<Id> ceilingIdSet)
    {
        List<Layer__c> layerList = [select id from Layer__c where Ceiling__c in : ceilingIdSet];
        
        if(layerList.size() > 0)
        {
            delete layerList;
        }
    }
}