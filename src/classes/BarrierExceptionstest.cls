@isTest
   public class BarrierExceptionstest{
     public static testmethod void test(){
     
          Exception_Template__c et = new Exception_Template__c ();
          et.Automatic__c = true;
          et.Active__c = true;
          et.Barrier_Level_Message__c = true;
          et.Reference_Id__c ='BAR-00001';
          insert et;
          
          //ID rectypeid = Schema.SObjectType.Barrier__c.getRecordTypeInfosByName().get('Asbestos').getRecordTypeId();
          
          Barrier__c rs = new Barrier__c();
          //rs.Energy_Assessment__c =ea.id;
          //rs.Status__c='Approved';
          rs.Knob__c = false;
          //rs.RecordTypeId = rectypeid;
          insert rs;
          
          List<Barrier__c> lstRev = new List<Barrier__c>();
          lstRev.add(rs);
          
          Map<Id,Barrier__c> mapRev = new Map<Id,Barrier__c>();
          mapRev.put(rs.Id,rs);
          
          BarrierExceptions.CheckUpdateExceptions(lstRev,mapRev);
         
          
             
         }
 }