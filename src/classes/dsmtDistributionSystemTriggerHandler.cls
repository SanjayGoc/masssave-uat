public class dsmtDistributionSystemTriggerHandler
{
    public static boolean recursiveLoop =false;
    public static boolean forRecommendation=false;
    public static boolean forDuctRecommendation=false;

    public static void calculateDefaultValues(list<Mechanical_Sub_Type__c> newList, Map<Id, Mechanical_Sub_Type__c> oldMap, dsmtEAModel.Surface bp)
    {       
        forRecommendation=false;
        for(Mechanical_Sub_Type__c a : newList)
        {
           bp.mstobj = a;
           UpdateDistributionSystem(bp);
        }
    }

    private static void ManageActuals(Mechanical_Sub_Type__c a)
    {
        if(a.ActualNumSupplyRegisters__c <=0) a.ActualNumSupplyRegisters__c=null;
        if(a.ActualNumReturnRegisters__c <=0) a.ActualNumReturnRegisters__c=null;
        if(a.ActualTotalNumRegisters__c <=0) a.ActualTotalNumRegisters__c=null;
        if(a.ActualLeakageTestMethod__c =='' ) a.ActualLeakageTestMethod__c=null;
        if(a.ActualReturnLeakage__c <=0) a.ActualReturnLeakage__c =null;
        if(a.ActualSupplyLeakage__c <=0) a.ActualSupplyLeakage__c =null;
        if(a.ActualTotalLeakage__c <=0) a.ActualTotalLeakage__c =null;

        if(a.ActualSupplyDuctsArea__c <= 0) 
        {
            if(a.ActualSupplyDuctsArea__c !=-11.111)
            {
                a.ActualSupplyDuctsArea__c = null;
            }
            //else {
                //a.ActualSupplyDuctsArea__c = 0;   
            //}
        }

        if(a.ActualReturnDuctsArea__c <= 0) 
        {
            if(a.ActualReturnDuctsArea__c !=-11.111)
            {
                a.ActualReturnDuctsArea__c = null;
            }
            //else {
                //a.ActualReturnDuctsArea__c = 0;   
            //}
        }
        
        if(a.ActualPipeInsulationAmountValue__c <= 0)  a.ActualPipeInsulationAmountValue__c=null;
        if(a.ActualPipeTotalLengthValue__c <=0) a.ActualPipeTotalLengthValue__c =null;
        if(a.ActualDistributionFractionH__c <=0) a.ActualDistributionFractionH__c=null;
        if(a.ActualDistributionFractionC__c <=0) a.ActualDistributionFractionC__c=null;
        if(a.ActualSupplyDuctsMaterialType__c =='') a.ActualSupplyDuctsMaterialType__c=null;
        if(a.ActualBTUsPerHour__c <=0) a.ActualBTUsPerHour__c =null;                
        
    }

    public static void UpdateDistributionSystem(dsmtEAModel.Surface bp)
    {
        Mechanical_Sub_Type__c a = bp.mstobj;

        Mechanical_Sub_Type__c hs = dsmtMechanicalHelper.GetMechanicalHeatingSystem(bp);
        Mechanical_Sub_Type__c cs = dsmtMechanicalHelper.GetMechanicalCoolingSystem(bp);
            
        if(a.RecordTypeId != null && bp.recordTypeMap.get(a.RecordTypeId) == 'Duct Distribution System')
            a.IsDuctDistributionSystem__c = true;
        
        a.DistributionSystemType__c = dsmtDistributionSystemHelper.ComputeDistributionSystemTypeCommon(bp);

        ManageActuals(a);

        if(cs==null)
        {
           a.ActualDistributionFractionC__c = null;
        }
        if(hs==null)
        {
           a.ActualDistributionFractionH__c = null;
        }
        
        a.Efficiency__c = 1;
        bp.Efficiency = a.Efficiency__c;
        
        if(a.Year__c == null)
            a.Year__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectYearCommon(bp);
        
        if(a.Fuel_Type__c == null)
            a.Fuel_Type__c = dsmtEnergyConsumptionHelper.ComputeFuelTypeCommon(bp);
            
        if(a.Fuel_Type__c != null)
            a.FuelUnits__c = SurfaceConstants.FuelUnitsTable.get(a.Fuel_Type__c).get(0);
            
        if(a.Location__c == null)
            a.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);
        
        bp.Location = a.Location__c;
        
        a.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
        a.EquipmentEfficiency__c = dsmtDistributionSystemHelper.ComputeDistributionSystemEfficiencyCommon(bp);
        
        bp.FuelType = a.Fuel_Type__c;
        bp.FuelUnits = a.FuelUnits__c;
        bp.Efficiency = a.Efficiency__c;
        bp.LocationSpace = a.LocationSpace__c;
        bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
        bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
        bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);
        
        a.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
        bp.LocationType = a.LocationType__c;
        
        a.RegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');
        a.RegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');
        
        a.IsWaterDistributionSystem__c = SurfaceConstants.CommonWaterDistributionSystemTypes.contains(bp.recordTypeMap.get(a.RecordTypeId));
        a.IsElectricDistributionSystem__c = SurfaceConstants.CommonElectricDistributionSystemTypes.contains(bp.recordTypeMap.get(a.RecordTypeId));
        a.IsAirDistributionSystem__c = SurfaceConstants.CommonAirDistributionSystemTypes.contains(bp.recordTypeMap.get(a.RecordTypeId));
        a.IsAirDistributionSystemWithFan__c = SurfaceConstants.CommonAirDistributionSystemWithFanTypes.contains(bp.recordTypeMap.get(a.RecordTypeId));
        a.IsDuctDistributionSystem__c = SurfaceConstants.CommonDuctDistributionSystemTypes.contains(bp.recordTypeMap.get(a.RecordTypeId));
        
        if(a.ActualNumSupplyRegisters__c == null)
            a.NumSupplyRegisters__c = 1;

        if(a.ActualNumReturnRegisters__c == null)
            a.NumReturnRegisters__c = 1;

        if(a.ActualTotalNumRegisters__c == null)
            a.TotalNumRegisters__c = 2;

        if(a.Voltage__c == null) a.Voltage__c = 'Low';

       
        if(a.ActualLeakageTestMethod__c ==null) a.Leakage_Test_Method__c = 'Not Tested';
        
        a.Airflow__c = dsmtDistributionSystemHelper.ComputeDistributionSystemAirflowCommon(bp);

        /// Duct Segment Code

        /// Duct Locatons
        a.DuctLocation__c = a.LocationSpace__c; // 'Living Space';
        bp.Location = a.DuctLocation__c;        
        a.DuctLocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
        bp.LocationSpace = a.DuctLocationSpace__c;
        bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
        bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
        bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

        a.DuctLocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);

        a.BufferZoneTempH__c = dsmtDistributionSystemHelper.ComputeDuctSegmentBufferZoneTempCommon(bp, 'H');
        a.BufferZoneTempC__c = dsmtDistributionSystemHelper.ComputeDuctSegmentBufferZoneTempCommon(bp, 'C');
        ////////////

        /// Reset to distribution 
        bp.LocationSpace = a.LocationSpace__c;
        bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
        bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
        bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);
        bp.LocationType = a.LocationType__c;
        //////////////////

        /*if(cs !=null)
        {
            UpdateDSCoolingSystem(bp, cs);
        }
        else if(hs !=null)
        {
            UpdateDSHeatingSystem(bp, hs);
        }*/

        if(bp.mst !=null && bp.mst.size()>1 && recursiveLoop==false)
        {
            if(forRecommendation==false)            
                UpdateOtherDSSystem(bp);
        }

        /// Return duct segment
        a.IsSupplyDuctSegment__c = false;

        if(a.ActualReturnDuctsArea__c == null)
        {
            a.Return_Ducts_Area__c = dsmtDistributionSystemHelper.ComputeDuctSegmentAreaCommon(bp);
        }
        else 
        {
            if(a.ActualReturnDuctsArea__c ==-11.111)
            {
                a.Return_Ducts_Area__c = 0;
            }
            else if(a.ActualReturnDuctsArea__c ==0)
            {
                a.Return_Ducts_Area__c = dsmtDistributionSystemHelper.ComputeDuctSegmentAreaCommon(bp);
            }
            
        }
        
        a.Return_Ducts_Area_Fraction__c = dsmtDistributionSystemHelper.ComputeDuctSegmentAreaFractionCommon(bp);
        a.TotalReturnDuctsArea__c = a.Return_Ducts_Area__c;

        if(!forDuctRecommendation)
            a.ReturnNominalRValue__c = dsmtDistributionSystemHelper.ComputeDuctSegmentNominalRValueCommon(bp); 
            
        a.Return_Ducts_R_Value__c = dsmtDistributionSystemHelper.ComputeDuctSegmentRValueCommon(bp);
        a.ReturnRegainC__c = dsmtDistributionSystemHelper.ComputeDuctSegmentRegainCCommon(bp);
        a.ReturnRegainH__c = dsmtDistributionSystemHelper.ComputeDuctSegmentRegainHCommon(bp);

        if(a.ActualSupplyDuctsMaterialType__c ==null || a.ActualSupplyDuctsMaterialType__c =='')
            a.Supply_Ducts_Material_Type__c = dsmtDistributionSystemHelper.ComputeDuctSegmentMaterialTypeCommon(bp);
        ////////////////////

        /// Supply duct segment        
        a.IsSupplyDuctSegment__c = true;

        if(a.ActualSupplyDuctsArea__c == null)
        {
            a.Supply_Ducts_Area__c = dsmtDistributionSystemHelper.ComputeDuctSegmentAreaCommon(bp);
        }
        else {
            if(a.ActualSupplyDuctsArea__c ==-11.111)
            {
                a.Supply_Ducts_Area__c = 0;
            }
            else if(a.ActualSupplyDuctsArea__c ==0)
            {
                a.Supply_Ducts_Area__c = dsmtDistributionSystemHelper.ComputeDuctSegmentAreaCommon(bp);
            }
        }

        a.Supply_Ducts_Area_Fraction__c = dsmtDistributionSystemHelper.ComputeDuctSegmentAreaFractionCommon(bp);
        a.TotalSupplyDuctsArea__c = a.Supply_Ducts_Area__c;

        if(!forDuctRecommendation)
            a.SupplyNominalRValue__c = dsmtDistributionSystemHelper.ComputeDuctSegmentNominalRValueCommon(bp); 

        a.Supply_Ducts_R_Value__c = dsmtDistributionSystemHelper.ComputeDuctSegmentRValueCommon(bp);
        a.SupplyRegainC__c = dsmtDistributionSystemHelper.ComputeDuctSegmentRegainCCommon(bp);
        a.SupplyRegainH__c = dsmtDistributionSystemHelper.ComputeDuctSegmentRegainHCommon(bp);
        ////////////////////

        //// Pipe System        

        if(a.ActualPipeTotalLengthValue__c ==null)
            a.PipeTotalLengthValue__c = dsmtDistributionSystemHelper.ComputeDistributionSystemPipeTotalLengthValueCommon(bp);

        if(a.ActualPipeInsulationAmountValue__c ==null)
            a.PipeInsulationAmountValue__c = dsmtDistributionSystemHelper.ComputeDistributionSystemPipeInsulationAmountValueCommon(bp);

        a.PipeInsulationRatio__c = dsmtDistributionSystemHelper.ComputeDistributionSystemPipeInsulationRatioCommon(bp);
        
        if(a.Pipe_Insulation_Amount__c ==null)
            a.Pipe_Insulation_Amount__c = dsmtDistributionSystemHelper.ComputeDistributionSystemPipeInsulationAmountCommon(bp);

        /// Code End
        
        a.ReturnLeakFraction__c = dsmtDistributionSystemHelper.ComputeDistributionSystemReturnLeakFractionCommon(bp);
        a.SupplyLeakFraction__c = dsmtDistributionSystemHelper.ComputeDistributionSystemSupplyLeakFractionCommon(bp);
        
        a.LeakageGUISupplyConversionFactor__c = dsmtDistributionSystemHelper.ComputeDistributionSystemLeakageGUISupplyConversionFactorCommon(bp);
        a.LeakageGUIReturnConversionFactor__c = dsmtDistributionSystemHelper.ComputeDistributionSystemLeakageGUIReturnConversionFactorCommon(bp);
        a.LeakageGUITotalConversionFactor__c  = dsmtDistributionSystemHelper.ComputeDistributionSystemLeakageGUITotalConversionFactorCommon(bp);
        
        a.DuctSubtractionNetCFM50__c = dsmtDistributionSystemHelper.ComputeDistributionSystemDuctSubtractionNetCFM50Common(bp);

        if(a.Leakiness__c ==null || a.Leakiness__c =='')
            a.Leakiness__c = dsmtDistributionSystemHelper.ComputeDistributionSystemLeakinessCommon(bp);
        
        a.DefaultCFM25Qual__c   = dsmtDistributionSystemHelper.ComputeDistributionSystemDefaultCFM25QualCommon(bp); 

        a.DefaultCFM25R__c      = dsmtDistributionSystemHelper.ComputeDistributionSystemDefaultCFM25RCommon(bp); 
        a.DefaultCFM25ROut__c   = dsmtDistributionSystemHelper.ComputeDistributionSystemDefaultCFM25ROutCommon(bp); 

        
        if(a.ActualReturnLeakage__c ==null)
            a.Return_Leakage__c = dsmtDistributionSystemHelper.ComputeDistributionSystemReturnLeakageCommon(bp);
        
        a.ReturnLeakage25__c = dsmtEAModel.SetScale(a.Return_Leakage__c);

        a.DefaultCFM25S__c      = dsmtDistributionSystemHelper.ComputeDistributionSystemDefaultCFM25SCommon(bp); 
        a.DefaultCFM25SOut__c   = dsmtDistributionSystemHelper.ComputeDistributionSystemDefaultCFM25SOutCommon(bp);
                
        if(a.ActualSupplyLeakage__c ==null)
            a.Supply_Leakage__c = dsmtDistributionSystemHelper.ComputeDistributionSystemSupplyLeakageCommon(bp);        

        a.SupplyLeakage25__c = dsmtEAModel.SetScale(a.Supply_Leakage__c);

        if(a.ActualTotalLeakage__c ==null)
            a.Total_Leakage__c = dsmtDistributionSystemHelper.ComputeDistributionSystemTotalLeakageCommon(bp);

        a.TotalLeakage25__c = dsmtEAModel.SetScale(a.Total_Leakage__c);

        a.DuctSystemLeakageImbalance__c = dsmtDistributionSystemHelper.ComputeDuctSystemLeakageImbalanceCommon(bp);
        
        a.DisplaySupplyLeakage__c = (a.Supply_Leakage__c != null ? a.Supply_Leakage__c : 0) *
                                   (a.LeakageGUISupplyConversionFactor__c != null ? a.LeakageGUISupplyConversionFactor__c : 0);
                                   
        a.DisplayReturnLeakage__c = (a.Return_Leakage__c != null ? a.Return_Leakage__c : 0) *
                                   (a.LeakageGUIReturnConversionFactor__c != null ? a.LeakageGUIReturnConversionFactor__c : 0);
        
        a.DisplayTotalLeakage__c = (a.Total_Leakage__c != null ? a.Total_Leakage__c : 0) *
                                   (a.LeakageGUITotalConversionFactor__c  != null ? a.LeakageGUITotalConversionFactor__c : 0);       

        a.CFM25ROut__c = dsmtDistributionSystemHelper.ComputeDistributionSystemCFM25ROutCommon(bp); 
        a.CFM25SOut__c = dsmtDistributionSystemHelper.ComputeDistributionSystemCFM25SOutCommon(bp);
        
        
        a.HeatingFanOn__c = dsmtDistributionSystemHelper.ComputeHeatingFanOnCommon(bp);
        a.LoadFromHeatingCycle__c = dsmtDistributionSystemHelper.ComputeDistributionSystemLoadFromHeatingCycleCommon(bp);
        
        a.DistributionCoolingFanOn__c = dsmtDistributionSystemHelper.ComputeCoolingFanOnCommon(bp);
        a.LoadFromCoolingCycle__c = dsmtDistributionSystemHelper.ComputeDistributionSystemLoadFromCoolingCycleCommon(bp);
        
        a.ShoulderSeasonFanOn__c = dsmtDistributionSystemHelper.ComputeShoulderSeasonFanOnKWHCommon(bp);
        a.LoadForShoulderSeason__c = dsmtDistributionSystemHelper.ComputeDistributionSystemLoadForShoulderSeasonCommon(bp);
        
        a.DuctSystemTempRiseH__c = dsmtDistributionSystemHelper.ComputeDuctSystemTempRiseHCommon(bp);
        a.DuctSystemTempRiseC__c = dsmtDistributionSystemHelper.ComputeDuctSystemTempRiseCCommon(bp);
        
        a.DuctOperatingPressureH__c = dsmtDistributionSystemHelper.ComputeDistributionSystemDuctOperatingPressureHCommon(bp);
        a.SupplyLeakageAdjustedH__c = dsmtDistributionSystemHelper.ComputeDistributionSystemSupplyLeakageAdjustedHCommon(bp);
        a.ReturnLeakageAdjustedH__c = dsmtDistributionSystemHelper.ComputeDistributionSystemReturnLeakageAdjustedHCommon(bp);

        a.DuctSystemLeakageImbalanceH__c  = dsmtDistributionSystemHelper.ComputeDuctSystemLeakageImbalanceHCommon(bp);
        a.DuctSystemLeakageImbalanceC__c  = dsmtDistributionSystemHelper.ComputeDuctSystemLeakageImbalanceCCommon(bp);
        
        a.DuctOperatingPressureC__c = dsmtDistributionSystemHelper.ComputeDistributionSystemDuctOperatingPressureCCommon(bp);
        a.SupplyLeakageAdjustedC__c = dsmtDistributionSystemHelper.ComputeDistributionSystemSupplyLeakageAdjustedCCommon(bp);
        a.ReturnLeakageAdjustedC__c = dsmtDistributionSystemHelper.ComputeDistributionSystemReturnLeakageAdjustedCCommon(bp);

        a.DuctSystemDeliveryEffectivenessH__c = dsmtDistributionSystemHelper.ComputeDuctSystemDeliveryEffectivenessHCommon(bp);
        a.DuctSystemDeliveryEffectivenessC__c = dsmtDistributionSystemHelper.ComputeDuctSystemDeliveryEffectivenessCCommon(bp);
        
        a.DSDeliveryEffectivenessCorrectedH__c = dsmtDistributionSystemHelper.ComputeDuctSystemDeliveryEffectivenessCorrectedHCommon(bp);
        a.DSDeliveryEffectivenessCorrectedC__c = dsmtDistributionSystemHelper.ComputeDuctSystemDeliveryEffectivenessCorrectedCCommon(bp);
        
        a.DuctSystemEquipCycleLossH__c = dsmtDistributionSystemHelper.ComputeDuctSystemEquipCycleLossHCommon(bp);
        a.DuctSystemEquipCycleLossC__c = dsmtDistributionSystemHelper.ComputeDuctSystemEquipCycleLossCCommon(bp);
        a.DuctSystemAirInfiltrationFactorH__c = dsmtDistributionSystemHelper.ComputeDuctSystemAirInfiltrationFactorHCommon(bp);
        a.DuctSystemAirInfiltrationFactorC__c = dsmtDistributionSystemHelper.ComputeDuctSystemAirInfiltrationFactorCCommon(bp);
        a.PercentInUnconditioned__c = dsmtDistributionSystemHelper.ComputeDistributionSystemPercentInUnconditionedCommon(bp);
        
        a.DuctSystemEfficiencyH__c = dsmtDistributionSystemHelper.ComputeDistributionSystemDuctSystemEfficiencyHCommon(bp);
        a.Heating_Efficiency__c = dsmtEAmodel.SetScale(dsmtEAModel.ISNULL(a.DuctSystemEfficiencyH__c)) * 100;

        a.DuctSystemEfficiencyC__c = dsmtDistributionSystemHelper.ComputeDistributionSystemDuctSystemEfficiencyCCommon(bp);
        a.Cooling_Efficiency__c = dsmtEAmodel.SetScale(dsmtEAModel.ISNULL(a.DuctSystemEfficiencyC__c)) * 100;
        
        if(a.ActualDistributionFractionH__c ==null)
        {
            a.DistributionFractionH__c = dsmtDistributionSystemHelper.ComputeDistributionSystemDistributionFractionHCommon(bp); 
        }
        else {
            a.DistributionFractionH__c   = dsmtEAModel.SetScale(a.ActualDistributionFractionH__c);
        }
        
        if(a.ActualDistributionFractionC__c ==null)
        {
            a.DistributionFractionC__c = dsmtDistributionSystemHelper.ComputeDistributionSystemDistributionFractionCCommon(bp);     
        }

        if(cs==null)
        {
           a.Distribution_Fraction__c = dsmtEAmodel.SetScale(dsmtEAModel.ISNULL(a.DistributionFractionH__c)) * 100; 
        }
        if(hs==null)
        {
           a.Distribution_Fraction__c = dsmtEAmodel.SetScale(dsmtEAModel.ISNULL(a.DistributionFractionC__c)) * 100; 
        }
        
        a.PipeTotalLengthValue__c = dsmtDistributionSystemHelper.ComputeDistributionSystemPipeTotalLengthValueCommon(bp);
        
        a.NormalizedDistributionFractionC__c = dsmtDistributionSystemHelper.ComputeDistributionSystemNormalizedDistributionFractionCCommon(bp); 
        a.NormalizedDistributionFractionH__c = dsmtDistributionSystemHelper.ComputeDistributionSystemNormalizedDistributionFractionHCommon(bp); 
        //a.NormalizedDistributionFractionSh__c = dsmtDistributionSystemHelper.ComputeDistributionSystemNormalizedDistributionFractionShCommon(bp); 

        if(a.ActualPipeInsulationAmountValue__c<=0)a.ActualPipeInsulationAmountValue__c =null;
        if(a.ActualPipeInsulationAmountValue__c ==null)
        {
            a.PipeInsulationAmountValue__c = dsmtDistributionSystemHelper.ComputeDistributionSystemPipeInsulationAmountValueCommon(bp); 
        }

        a.Load__c = dsmtDistributionSystemHelper.ComputeDistributionSystemLoadCommon(bp);
        bp.Load = a.Load__c;
        
        a.EnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'H');
        a.EnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'C');
        a.EnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'Sh');
        
        a.FuelConsumptionH__c = dsmtDistributionSystemHelper.ComputeBuildingProfileObjectFuelConsumptionHCommon(a);
        a.FuelConsumptionC__c = dsmtDistributionSystemHelper.ComputeBuildingProfileObjectFuelConsumptionCCommon(a);
        a.FuelConsumptionSh__c = dsmtDistributionSystemHelper.ComputeBuildingProfileObjectFuelConsumptionShCommon(a);
             
        bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
        bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
        bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;
        
        
        a.TotalLeakageCFM50__c = dsmtDistributionSystemHelper.ComputeDistributionSystemTotalLeakageCFM50Common(bp);
        a.DuctSystemInfiltAdjH__c = dsmtDistributionSystemHelper.ComputeDuctSystemInfiltAdjHCommon(bp);
        a.DuctSystemInfiltAdjC__c = dsmtDistributionSystemHelper.ComputeDuctSystemInfiltAdjCCommon(bp);

        /*a.Net_Leakage__c = a.TotalLeakage25__c;
        a.Net_Supply_Leakage__c = a.SupplyLeakage25__c;
        a.Net_Return_Leakage__c = a.ReturnLeakage25__c;*/
        
        a.AnnualEnergyConsumption__c = dsmtDistributionSystemHelper.ComputeDistributionSystemAnnualEnergyConsumptionCommon(bp);
        
        a.AnnualFuelConsumption__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualFuelConsumptionCommon(bp);
            
        bp.AnnualFuelConsumption = a.AnnualFuelConsumption__c;
        
        //Date dt = Date.today();
        //a.AnnualOperatingCost__c = dsmtEnergyConsumptionHelper.GetEffectiveFuelPrice(bp,dt) * bp.AnnualFuelConsumption;
    }

    private static void UpdateOtherDSSystem(dsmtEAModel.Surface bp)
    {
        dsmtEAModel.Surface heatingBP = dsmtEAModel.CloneSurfaceModel(bp);
        heatingBP.temp = dsmtTempratureHelper.ComputeTemprature(heatingBP);
        heatingBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(heatingBP);
        
        recursiveLoop=true;

        heatingBP.mechType = bp.mechType;

        if(bp.mst !=null)
        {
            List<Mechanical_Sub_Type__c> mstList = bp.mst;
            For(integer i=0, j=mstList.size(); i<j ; i++)
            {
                if(mstList[i].Id ==null || mstList[i].Id == bp.mstObj.id)
                {
                    heatingBP.mst[i]=bp.mstObj;
                }

                if(mstList[i].Id ==null) continue;
                if(mstList[i].Id == bp.mstObj.id) continue;

                if(dsmtEAModel.GetDistributionSystemRecordTypes().contains(bp.RecordTypeMap.get(bp.mst[i].RecordTypeId)))
                {
                    system.debug('updating other distribution system : ' + mstList[i].DistributionSystemType__c);

                    heatingBP.mstObj = mstList[i].clone(true,true,true,true);
                    heatingBP.mst[i]= heatingBP.mstObj;
                    
                    UpdateDistributionSystem(heatingBP);

                    bp.mst[i] = heatingBP.mstObj.clone(true,true,true,true);                    
                }
            }
        }

        recursiveLoop=false;
    }

    @testvisible
    private static void UpdateDSCoolingSystem(dsmtEAModel.Surface bp, Mechanical_Sub_Type__c cs)
    {
        dsmtEAModel.Surface coolingBP = dsmtEAModel.CloneSurfaceModel(bp);
        coolingBP.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        coolingBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        coolingBP.mechType = bp.mechType;
        
        coolingBP.mstObj = cs.clone(true,true,true,true);                    
        MechanicalSubTypeTriggerHandler.UpdateCoolingSystemRecord(coolingBP);

        if(bp.mst !=null)
        {
            For(integer i=0; i< bp.mst.size(); i++)
            {
                if(bp.mst[i].Id ==null) continue;
                if(bp.mst[i].Id != coolingBP.mstObj.id) continue;
                bp.mst[i] = coolingBP.mstObj.clone(true,true,true,true);
                break;
            }
        }
    }

    @testvisible
    private static void UpdateDSHeatingSystem(dsmtEAModel.Surface bp, Mechanical_Sub_Type__c cs)
    {
        dsmtEAModel.Surface heatingBP = dsmtEAModel.CloneSurfaceModel(bp);
        heatingBP.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        heatingBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        heatingBP.mechType = bp.mechType;
        
        heatingBP.mstObj = cs.clone(true,true,true,true);                    
        MechanicalSubTypeTriggerHandler.UpdateCoolingSystemRecord(heatingBP);

        if(bp.mst !=null)
        {
            For(integer i=0; i< bp.mst.size(); i++)
            {
                if(bp.mst[i].Id ==null) continue;
                if(bp.mst[i].Id != heatingBP.mstObj.id) continue;
                bp.mst[i] = heatingBP.mstObj.clone(true,true,true,true);
                break;
            }
        }
    }
}