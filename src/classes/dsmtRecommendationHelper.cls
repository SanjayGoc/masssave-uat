public class dsmtRecommendationHelper{
    
    public static boolean disableRolluptriggeronRec = false;
    public static boolean RollupOnProject = false;
    public static boolean StopRecTrigger = false;
    public static boolean StopRecExcTrigger = false;
    public static boolean ChangeorderLineTrigger  = false;
    public static boolean stopProjectException = false;
    public static boolean stopRecPayInvLinetrigger = false;
    
    public static void setInvoiceType(List<Recommendation__c> recList){
        
        Set<Id> eaId = new Set<Id>();
        Set<Id> prodId = new Set<Id>();
        
        for(Recommendation__c rec : recList){
            if(rec.Energy_Assessment__c != null){
                eaId.add(rec.Energy_Assessment__c);
            }
            
            if(rec.Dsmtracker_Product__c != null){
                prodId.add(rec.Dsmtracker_Product__c);
            }
        }
        
        if(eaId.size() > 0){
            List<Energy_Assessment__c> eaList = [select id,Assessment_Type__c,Primary_Provider__c,Secondary_Provider__c
                                                        from Energy_Assessment__c where id in : eaId];
            
            Map<Id,Energy_Assessment__c> eaMap = new Map<Id,Energy_Assessment__c>();
            
            for(Energy_Assessment__c ea : eaList){
                eaMap.put(ea.Id,ea);
            }
            
            if(prodId.size() > 0){
                List<Dsmtracker_Product__c> dsmtProdList = [select id,name,Category__c,RecordType.Name,Sub_Category__c,Surface__c
                                                                 from Dsmtracker_Product__c where id in : prodId];
                
                Map<Id,Dsmtracker_Product__c> dsmtProdMap = new Map<Id,Dsmtracker_Product__c>();
                
                for(Dsmtracker_Product__c dsmt : dsmtProdList){
                    dsmtProdMap.put(dsmt.Id,dsmt);
                }
                
                String Type = null;
                String PrimaryProvider = null;
                String SecondaryProvider = null;
                String RecTypeName = null;
                
                for(Recommendation__c rec : recList){
                    Type = null;
                    PrimaryProvider = null;
                    SecondaryProvider = null;
                    RecTypeName = null;
                    String SubCategory = dsmtProdMap.get(rec.Dsmtracker_Product__c).Sub_Category__c;
                    String dsmtProdName = dsmtProdMap.get(rec.Dsmtracker_Product__c).Name;
                    
                    if(rec.Dsmtracker_Product__c != null){
                        
                        system.debug('--dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c---'+dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c);
                        
                        RecTypeName = dsmtProdMap.get(rec.Dsmtracker_Product__c).RecordType.Name;
                        String Category = dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c;
                       // if(dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c == 'Direct Install'){
                            
                            if(rec.Energy_Assessment__c != null){
                                if(eaMap.get(rec.Energy_Assessment__c) != null){
                                    Type = eaMap.get(rec.Energy_Assessment__c).Assessment_Type__c;
                                    PrimaryProvider = eaMap.get(rec.Energy_Assessment__c).Primary_Provider__c;
                                    SecondaryProvider = eaMap.get(rec.Energy_Assessment__c).Secondary_Provider__c;
                                    
                                    system.debug('--Type ---'+Type );
                                    
                                    if(Type == 'HEA (Home Energy Assessment)' || Type == 'Landlord Visit' || Type == 'Expanded HEA'){
                                        
                                        if(PrimaryProvider == 'Eversource East Electric'){
                                             //if(SecondaryProvider == 'Eversource East Gas' || SecondaryProvider == null || SecondaryProvider == ''){
                                                if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTAR416';
                                                }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTAR416';
                                                }else if(Category != 'Direct Install'){
                                                    rec.Invoice_Type__c = 'NSTAR413';
                                                }
                                            //}
                                        }else if(PrimaryProvider == 'Eversource West Electric'){
                                           // if(SecondaryProvider == null || SecondaryProvider == ''){
                                                if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                    rec.Invoice_Type__c = 'WMECO416';
                                                }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                    rec.Invoice_Type__c = 'WMECO416';
                                                }else if(Category != 'Direct Install'){
                                                    rec.Invoice_Type__c = 'WMECO413';
                                                }  
                                           // }
                                        }else if(PrimaryProvider == 'Eversource East Gas'){
                                            if(SecondaryProvider == 'Eversource East Electric'){
                                                if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTAR418';
                                                }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                }else if(Category != 'Direct Install'){
                                                    rec.Invoice_Type__c = 'NSTARGAS413';
                                                }
                                            }else if(SecondaryProvider == 'National Grid Electric'){
                                                if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NATGRID418';
                                                }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                }else if(Category != 'Direct Install'){
                                                    rec.Invoice_Type__c = 'NSTARGAS413';
                                                }
                                            }else if(SecondaryProvider == 'Eversource West Electric'){
                                                 System.debug('step5 '+SecondaryProvider);
                                                
                                                if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                    rec.Invoice_Type__c = 'WMECO418';
                                                    System.debug('step6 '+SecondaryProvider);
                                                }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                }else{
                                                    rec.Invoice_Type__c = 'NSTARGAS413';
                                                }// DSMT  9731 changes Athiq changes ends
                                            }else { //if(SecondaryProvider == null || SecondaryProvider == ''){
                                                if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                                }else if(Category != 'Direct Install'){
                                                    rec.Invoice_Type__c = 'NSTARGAS413';
                                                }
                                            }
                                        }
                                    } else if(Type == 'SHV (Special Home Visit)' || Type == 'Renter Visit'){
                                            
                                            if(PrimaryProvider == 'Eversource East Electric'){
                                               // if(SecondaryProvider == 'Eversource East Gas' || SecondaryProvider == null || SecondaryProvider == ''){
                                                    if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTAR416S';
                                                    }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTAR416S';
                                                    }else if(Category != 'Direct Install'){
                                                        rec.Invoice_Type__c = 'NSTAR413S';
                                                    }
                                               // }
                                            }else if(PrimaryProvider == 'Eversource West Electric'){
                                                // if(SecondaryProvider == null || SecondaryProvider == ''){
                                                    if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                        rec.Invoice_Type__c = 'WMECO416S';
                                                    }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                        rec.Invoice_Type__c = 'WMECO416S';
                                                    }else if(Category != 'Direct Install'){
                                                        rec.Invoice_Type__c = 'WMECO413S';
                                                    }  
                                                // }
                                            }else if(PrimaryProvider == 'Eversource East Gas'){
                                                if(SecondaryProvider == 'Eversource East Electric'){
                                                    if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTAR418S';
                                                    }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    }else if(Category != 'Direct Install'){
                                                        rec.Invoice_Type__c = 'NSTARGAS413S';
                                                    }
                                                }else if(SecondaryProvider == 'National Grid Electric'){
                                                    if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NATGRID418S';
                                                    }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    }else if(Category != 'Direct Install'){
                                                        rec.Invoice_Type__c = 'NSTARGAS413S';
                                                    }
                                                }else{ // if(SecondaryProvider == null || SecondaryProvider == ''){
                                                    if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    }else if(Category != 'Direct Install'){
                                                        rec.Invoice_Type__c = 'NSTARGAS413S';
                                                    }
                                                }
                                            }
                                    }
                                }
                            }
                      
                        
                    }
                    
                    if(dsmtProdName  == 'Install an Aube Line Voltage Thermostat' ||
                       dsmtProdName  == 'Install an Aube Low Voltage Thermostat'){
                        
                        if(PrimaryProvider == 'Eversource East Electric' || SecondaryProvider == 'Eversource East Electric'){
                            rec.Invoice_Type__c = 'NSTAR413';
                        }else if(PrimaryProvider == 'Eversource West Electric' || SecondaryProvider == 'Eversource West Electric'){
                            rec.Invoice_Type__c = 'WMECO413';
                        }
                           
                    } 
                    if(rec.Invoice_Type__c == null){
                        rec.Invoice_Type__c = 'Not Found';
                    }
                }
            }
            
        }
    } 
    
    @future
    public static void setInvoiceTypeFuture(Set<Id> eaIds){
        
        
        System.debug('Check Entered');
        
        List<Recommendation__c> recList = [select id,Energy_Assessment__c,Dsmtracker_Product__c,Invoice_Type__c,Dsmtracker_Product__r.Name,
                                            Dsmtracker_Product__r.Sub_Category__c,Dsmtracker_Product__r.Category__c from Recommendation__c where
                                            Energy_Assessment__c in : eaIds];
        Set<Id> eaId = new Set<Id>();
        Set<Id> prodId = new Set<Id>();
        
        if(recList != null && recList.size() > 0){
        for(Recommendation__c rec : recList){
            if(rec.Energy_Assessment__c != null){
                eaId.add(rec.Energy_Assessment__c);
            }
            
            if(rec.Dsmtracker_Product__c != null){
                prodId.add(rec.Dsmtracker_Product__c);
            }
        }
        
        if(eaId.size() > 0){
            List<Energy_Assessment__c> eaList = [select id,Assessment_Type__c,Primary_Provider__c,Secondary_Provider__c
                                                        from Energy_Assessment__c where id in : eaId];
            
            Map<Id,Energy_Assessment__c> eaMap = new Map<Id,Energy_Assessment__c>();
            
            for(Energy_Assessment__c ea : eaList){
                eaMap.put(ea.Id,ea);
            }
            
            if(prodId.size() > 0){
                List<Dsmtracker_Product__c> dsmtProdList = [select id,name,Category__c,RecordType.Name,Sub_Category__c,Surface__c
                                                                 from Dsmtracker_Product__c where id in : prodId];
                
                Map<Id,Dsmtracker_Product__c> dsmtProdMap = new Map<Id,Dsmtracker_Product__c>();
                
                for(Dsmtracker_Product__c dsmt : dsmtProdList){
                    dsmtProdMap.put(dsmt.Id,dsmt);
                }
                
                String Type = null;
                String PrimaryProvider = null;
                String SecondaryProvider = null;
                String RecTypeName = null;
                  System.debug('Check Entered');
                boolean InvoiceTypeFound = false;
                for(Recommendation__c rec : recList){
                    Type = null;
                    InvoiceTypeFound = false;
                    PrimaryProvider = null;
                    SecondaryProvider = null;
                    RecTypeName = null;
                    String SubCategory = dsmtProdMap.get(rec.Dsmtracker_Product__c).Sub_Category__c;
                    if(rec.Dsmtracker_Product__c != null){
                        
                        system.debug('--dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c---'+dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c);
                        
                        RecTypeName = dsmtProdMap.get(rec.Dsmtracker_Product__c).RecordType.Name;
                        String Category = dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c;
                       // if(dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c == 'Direct Install'){
                            
                        if(rec.Energy_Assessment__c != null){
                                if(eaMap.get(rec.Energy_Assessment__c) != null){
                                    Type = eaMap.get(rec.Energy_Assessment__c).Assessment_Type__c;
                                    PrimaryProvider = eaMap.get(rec.Energy_Assessment__c).Primary_Provider__c;
                                    SecondaryProvider = eaMap.get(rec.Energy_Assessment__c).Secondary_Provider__c;
                                    
                                    system.debug('--Type ---'+Type );
                                 //DSMT-12444 changes start  Athiq -----------------------
                                   if(Type == 'HEA (Home Energy Assessment)' || Type == 'Landlord Visit' || Type == 'Expanded HEA'){
                                    
                                    if(PrimaryProvider == 'Eversource East Electric'){
                                        if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                            rec.Invoice_Type__c = 'NSTAR416';
                                        }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                            rec.Invoice_Type__c = 'NSTAR416';
                                        }else if(Category != 'Direct Install'){
                                            rec.Invoice_Type__c = 'NSTAR413';
                                        }

                                    }else if(PrimaryProvider == 'Eversource West Electric'){
                                        if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                            rec.Invoice_Type__c = 'WMECO416';
                                        }else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                            rec.Invoice_Type__c = 'WMECO416';
                                        }else if(Category != 'Direct Install'){
                                            rec.Invoice_Type__c = 'WMECO413';
                                        }  
                                    }else if(PrimaryProvider == 'Eversource East Gas'){
                                           System.debug('step3 '+PrimaryProvider);
                                        if(SecondaryProvider == 'Eversource East Electric'){
                                            // DSMT  9731 changes Athiq
                                            //if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                            if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Lighting' || rec.Dsmtracker_Product__r.Sub_Category__c == 'Power Strip'){
                                                rec.Invoice_Type__c = 'NSTAR418';
                                                System.debug('step4 '+SecondaryProvider);
                                            }else if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Direct Install'){
                                                rec.Invoice_Type__c = 'NSTARGAS416';
                                                System.debug('step4 '+SecondaryProvider);
                                            }else{
                                                rec.Invoice_Type__c = 'NSTARGAS413';
                                            }
                                        }else if(SecondaryProvider == 'Eversource West Electric'){
                                             System.debug('step5 '+SecondaryProvider);
                                            
                                            if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Lighting' || rec.Dsmtracker_Product__r.Sub_Category__c == 'Power Strip'){
                                                rec.Invoice_Type__c = 'WMECO418';
                                                System.debug('step6 '+SecondaryProvider);
                                            }else if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Direct Install'){
                                                rec.Invoice_Type__c = 'NSTARGAS416';
                                                System.debug('step4 '+SecondaryProvider);
                                            }else{
                                                rec.Invoice_Type__c = 'NSTARGAS413';
                                            }// DSMT  9731 changes Athiq changes ends
                                        }else if(SecondaryProvider == 'National Grid Electric'){
                                           // if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                           if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Lighting' || rec.Dsmtracker_Product__r.Sub_Category__c == 'Power Strip'){
                                                rec.Invoice_Type__c = 'NATGRID418';
                                                System.debug('step7 '+SecondaryProvider);
                                            }else{
                                                rec.Invoice_Type__c = 'NSTARGAS416';
                                                 System.debug('step8 '+SecondaryProvider);
                                            }
                                        }else{
                                            
                                            /*if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                            }
                                            else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                            }else if(Category != 'Direct Install'){
                                                    rec.Invoice_Type__c = 'NSTARGAS413';
                                            }*/
                                            rec.Invoice_Type__c = 'NSTARGAS416';
                                              System.debug('stepfinal--NSTARGAS416 ');
                                        }
                                    }
                                } else if(Type == 'SHV (Special Home Visit)' || Type == 'Renter Visit'){
                                        
                                        if(PrimaryProvider == 'Eversource East Electric'){
                                            rec.Invoice_Type__c = 'NSTAR416S';
                                        }else if(PrimaryProvider == 'Eversource West Electric'){
                                            rec.Invoice_Type__c = 'WMECO416S';
                                        }else if(PrimaryProvider == 'Eversource East Gas'){
                                            if(SecondaryProvider == 'Eversource East Electric'){
                                               // if(RecTypeName != null && RecTypeName.contains('Lighting')){
                                                if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Lighting' || rec.Dsmtracker_Product__r.Sub_Category__c == 'Power Strip'){
                                                    rec.Invoice_Type__c = 'NSTAR418S';
                                                    System.debug('step4 '+SecondaryProvider);
                                                }else if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Direct Install'){
                                                    rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    System.debug('step4 '+SecondaryProvider);
                                                }else{
                                                    rec.Invoice_Type__c = 'NSTARGAS413S';
                                                }
                                            }else if(SecondaryProvider == 'Eversource West Electric'){
                                                if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Lighting' || rec.Dsmtracker_Product__r.Sub_Category__c == 'Power Strip'){
                                                    rec.Invoice_Type__c = 'WMECO418S';
                                                    System.debug('step6 '+SecondaryProvider);
                                                }else if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Direct Install'){
                                                    rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    System.debug('step4 '+SecondaryProvider);
                                                }else{
                                                    rec.Invoice_Type__c = 'NSTARGAS413S';
                                                }
                                            }else if(SecondaryProvider == 'National Grid Electric'){
                                              
                                               if(rec.Dsmtracker_Product__r.Sub_Category__c == 'Lighting' || rec.Dsmtracker_Product__r.Sub_Category__c == 'Power Strip'){
                                                    rec.Invoice_Type__c = 'NATGRID418S';
                                                }else{
                                                    rec.Invoice_Type__c = 'NSTARGAS416S';
                                                }
                                            }
                                            else{
                                                /* if(Category == 'Direct Install' && (Subcategory == 'Lighting' || Subcategory == 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                }
                                                else if(Category == 'Direct Install' && (Subcategory != 'Lighting' && Subcategory != 'Power Strip')){
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                }else if(Category != 'Direct Install'){
                                                        rec.Invoice_Type__c = 'NSTARGAS413S';
                                                }*/
                                                rec.Invoice_Type__c = 'NSTARGAS416S';
                                                  System.debug('stepfinal--NSTARGAS416');
                                            }
                                        }
                                
                            }//DSMT-12444 changes ends
                                }
                            }
                       
                        
                    }
                    
                    if(rec.Dsmtracker_Product__r.Name == 'Install an Aube Line Voltage Thermostat' ||
                       rec.Dsmtracker_Product__r.Name == 'Install an Aube Low Voltage Thermostat'){
                        
                        if(PrimaryProvider == 'Eversource East Electric' || SecondaryProvider == 'Eversource East Electric'){
                            rec.Invoice_Type__c = 'NSTAR413';
                        }else if(PrimaryProvider == 'Eversource West Electric' || SecondaryProvider == 'Eversource West Electric'){
                            rec.Invoice_Type__c = 'WMECO413';
                        }
                           
                    } 
                   
                }
            }
            
        }
        update recList;
        }
    }    
}