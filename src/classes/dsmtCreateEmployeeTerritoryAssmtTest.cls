@isTest
public class dsmtCreateEmployeeTerritoryAssmtTest {
    @isTest
    public static void runTest(){
        Location__c loc= Datagenerator.createLocation();
        insert loc;
        
        
        Schedules__c sc = new Schedules__c(Name='test',Location__c=loc.Id);
        insert sc;
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c();
        //sli.Name = 'TestLI';
        sli.Schedules__c = sc.Id;
        
        Employee__c em = Datagenerator.createEmployee(loc.id);
        em.Employee_Id__c = 'test123';
        em.Status__c='Active';
        
        insert em;
        
        ApexPages.currentPage().getParameters().put('Id',em.Id);
        dsmtCreateEmployeeTerritoryAssmtCntrl etCnt = new dsmtCreateEmployeeTerritoryAssmtCntrl();
        em.Schedule__c = sc.Id;
        update em;
        etCnt.autoRun();
        
    }
}