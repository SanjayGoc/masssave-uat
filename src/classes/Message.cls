public class Message{
        
        /**
        * @author Hemanshu Patel - GTES
        * @date 05/17/2016
        *
        * @group Ticket#-000560
        * @group-content https://na16.salesforce.com/a0vj0000003NIb4
        *
        * @description Send inline image in email, Populated Message HTML Text with Email body
       */
        public static void CreateMessage(String FromAdd,String ToAdd,String CCadd,String BCCAdd,Id EAId,String subject,String Message,String MsgType){
        
            Messages__c msg = new Messages__c();
            msg.From__c = FromAdd;
            msg.To__c = ToAdd;
            msg.CC__c = CCadd;
            msg.BCC__c = BCCAdd;
            msg.Invoice__c = EAId;
            msg.Subject__c = Subject;
            msg.Message__c = Message;
            msg.Message_Html_Text__c = Message;
            msg.Mesage_Text_Only__c = htmlToText(Message);
            msg.Message_Type__c = msgType;
            msg.Message_Direction__c  = 'Outbound';
            msg.Status__c = 'Sent';
            msg.Sent__c = Datetime.Now();
            insert msg;
            

        }
        
        
        public static Id CreateEmailMessage(String FromAdd,String ToAdd,String CCadd,String BCCAdd,Id EAId,String subject,String Message,String MsgType,String ToId,String MsgId){
            
            List<Invoice__c> eaList = [select id from Invoice__c where id =: EAId];
            List<User> UserList = [select Id from User where id =: UserInfo.getUserId()];
            
            Messages__c msg = new Messages__c();
            msg.From__c = FromAdd;
            msg.To__c = ToAdd;
            msg.CC__c = CCadd;
            msg.BCC__c = BCCAdd;
            msg.Invoice__c = EAId;
            if(MsgId != '' && MsgId != null){
                msg.Parent_Message__c = MsgId;
            }
            system.debug('--subject.contains---'+subject.contains('Sandbox:'));
            system.debug('--Message.isSandbox()---'+isSandbox());
            if(isSandbox() && !subject.contains('Sandbox:')){
                subject = 'Sandbox: '+ subject;
            }
            msg.Subject__c = Subject;
            String ActualMessage = Message;
            if(Message.length() < 131072){
                msg.Message__c = Message;
                msg.Message_Html_Text__c = Message;
                msg.Mesage_Text_Only__c = htmlToText(Message);
            }else{
            
            }
            if(Message.length() > 131072){
                
                Integer Start = -1;
                
                Start  = Message.indexOf('<img src=');
                
                system.debug('---Start --'+Start);
                if(Start  != -1){
                    
                    Integer count = Message.countMatches('<img src=');
                    
                    system.debug('---count--'+count);
                    
                    for(Integer i = 0; i < count;i++){
                        Start = Message.indexOf('<img src=');
                        
                        /**
                        * @author Hemanshu Patel - GTES
                        * @date 10/07/2016
                        *
                        * @group Ticket#-000648
                        * @group-content https://na16.salesforce.com/a0vj0000004vjJl
                        *
                        * @description Attachment and RFI issues
                       */
                        
                        
                        if(Start != -1){
                            Integer EndLine = Message.indexOf('/>',Start+8);
                            system.debug('--EndLine ---'+EndLine);
                            Message = Message.replace(Message.substring(Start,EndLine),'');
                            system.debug('--Message ---'+Message );
                            msg.Mesage_Text_Only__c = htmlToText(Message);
                        }
                    }
                    count = Message.countMatches('/>');
                    for(Integer i = 0; i < count;i++){
                        //Message = Message.replace('/>','');
                    }
                    system.debug('--Message ---'+Message );
                    
                    msg.Mesage_Text_Only__c = htmlToText(Message);
                    
                }else{
                    msg.Mesage_Text_Only__c = htmlToText(Message);
                }
                
            }
            msg.Message_Type__c = msgType;
            msg.Message_Direction__c  = 'Outbound';
            msg.Status__c = 'Sent';
            msg.Sent__c = Datetime.Now();
            msg.Contact__c = ToId;
            //msg.Do_Not_Create_FeedItem__c = true;
            insert msg;
            
            
           // if(Message.length() > 131072){
                
                
                
          //  }
            return msg.Id;
        }
        
        
        public static String htmlToText(String htmlString) {
            String RegEx = '(</{0,1}[^>]+>)';
            if (htmlString == null)
                htmlString = '';
            return htmlString.replaceAll('&nbsp', '').replaceAll(';', '').replaceAll(RegEx, '');
       }
       
       public static Boolean isSandbox() {
            //return URL.getSalesforceBaseUrl().getHost().left(2).equalsignorecase('cs');
            if(URL.getSalesforceBaseUrl().getHost().left(2).equalsignorecase('cs') || URL.getSalesforceBaseUrl().getHost().left(4).equalsignorecase('c.cs')){
                return true;  
            }else{
                return false;
            }
        }

}