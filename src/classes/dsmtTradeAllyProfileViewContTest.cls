@isTest
Public class dsmtTradeAllyProfileViewContTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
       //  dsmt.Contact__c= cont.id;
         //update dsmt;
        Review__c rev= Datagenerator.createReview(Tacc.Id);
        Attachment__c att= Datagenerator.createAttachment(Tacc.Id);
        ApexPages.currentPage().getParameters().put('taid',Tacc.Id);
        ApexPages.currentPage().getParameters().put('dcid',dsmt.Id);
        ApexPages.currentPage().getParameters().put('id',rev.Id);
        dsmtTradeAllyProfileViewCont controller = new dsmtTradeAllyProfileViewCont();
       // controller.TAId=Tacc.Id;
        controller.changeEditMode();
        controller.updateProfile();
        controller.getStateList();
        controller.getQuestions();
        

    }
      static testMethod void passwordresettest(){
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test3@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        Billing_Account_Number__c= 'Gas-~~~1234',
        OwnerId = portalAccountOwner1.Id
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile Limit 1];
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
       
        System.runAs ( user1) {
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
         dsmt.Contact__c= user1.contactID;
         dsmt.Trade_Ally_Account__c=Tacc.Id;
         update dsmt;
        ApexPages.currentPage().getParameters().put('uid',user1.id);
        dsmtTradeAllyProfileViewCont controller = new dsmtTradeAllyProfileViewCont();
       // controller.TAId=Tacc.Id;
        controller.changeEditMode();
        controller.updateProfile();
        controller.getStateList();
        controller.getQuestions();
        controller.updatePassword();
        //controller.fetchdsmtContactAndTradeAllyAccount();
        }
        }
       // controller2.gridpage();
    }
}