public with sharing class dsmtTakeOwnershipExtn {
    String id = '';
    String objecttype  = '';
    
    public dsmtTakeOwnershipExtn() {
        id = ApexPages.currentPage().getParameters().get('id');
        objecttype = ApexPages.currentPage().getParameters().get('type');
    }

    public PageReference assigneOwnerToCurrentLoggedInUser(){
        try{
        
            if(objecttype != null){
                if(objecttype == 'Trade_Ally_Account__c'){ 
                    List<Trade_Ally_Account__c> talist = [select id,OwnerId,Account__c from Trade_Ally_Account__c WHERE id=:id];
                    if(talist.size() > 0){
                        Trade_Ally_Account__c taacc = talist.get(0);
                        taacc.ownerId = UserInfo.getUserId();
                        //taacc.Account_Manager__c = UserInfo.getUserId();
                        update taacc;
                        List<Account> acclist = [select id,OwnerId from Account Where id =: taacc.Account__c];
                        if(acclist.size() > 0){
                            Account acc = acclist.get(0);
                            acc.OwnerId = Userinfo.getUserid();
                            update acc;
                        }
                        return new PageReference ('/'+taacc.id);
                    }
                }else if(objecttype == 'Review__c'){
                   List<Review__C> rlist = [select id,OwnerId from Review__c WHERE id=:id];
                   if(rlist.size() > 0){
                      Review__c r = rlist.get(0);
                      r.OwnerId = Userinfo.getuserid();
                      update r;
                      return new PageReference('/'+r.id);
                   }
                }else if(objecttype == 'Registration_Request__c'){
                   List<Registration_Request__c> rlist = [select id,OwnerId from Registration_Request__c WHERE id=:id];
                   if(rlist.size() > 0){
                      Registration_Request__c r = rlist.get(0);
                      r.OwnerId = Userinfo.getuserid();
                      //r.Account_Manager__c = UserInfo.getUserId();
                      update r;
                      return new PageReference('/'+r.id);
                   }
                }else if(objecttype == 'Service_Request__c'){
                   List<Service_Request__c> rlist = [select id,OwnerId from Service_Request__c WHERE id=:id];
                   if(rlist.size() > 0){
                      Service_Request__c r = rlist.get(0);
                      r.OwnerId = Userinfo.getuserid();
                      update r;
                      return new PageReference('/'+r.id);
                   }
                }
           } 
            
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getMessage()));
            return null;
        }
        return null;
    }
    
    
}