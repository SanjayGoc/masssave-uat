public class dsmtAvailableAppointment{
    
   /* public static Boolean getAvailableAppoit(dsmtCallOut.AppointmentOption aoption){
         string strdt = aoption.serviceDate+' '+aoption.serviceTimeSlot.split('-')[0];
         string stredt = aoption.serviceDate+' '+aoption.serviceTimeSlot.split('-')[1];
         DateTime dt = DateTime.parse(strdt);
         DateTime edt = DateTime.parse(stredt);
         
         List<Workorder__c> lstWorkOrd = [select Id,Scheduled_Start_Date__c,Scheduled_End_Date__c from Workorder__c where Work_Team__c = :aoption.workTeamId AND Scheduled_Start_Date__c >= :dt AND Scheduled_End_Date__c <= :edt AND (Status__c = 'Scheduled' OR Status__c = 'Requires Review' OR Status__c = 'Requires Rescheduling')];
         if(lstWorkOrd.size()>0){
             for(Workorder__c w: lstWorkOrd ){
                 if(dt >= w.Scheduled_Start_Date__c &&
                    dt <= w.Scheduled_End_Date__c ){
                        return false;
                }
                
                if(edt >= w.Scheduled_Start_Date__c &&
                    edt <= w.Scheduled_End_Date__c ){
                        return false;
                }
                if(dt <= w.Scheduled_Start_Date__c &&
                    edt  >= w.Scheduled_End_Date__c){
                    
                    return false; 
                }
             }
             //return false;
         }         
         return true;
    }*/
    
    public static string getAvailableAppoit1(dsmtCallOut.AppointmentOption aoption){
         string strdt = aoption.serviceDate+' '+aoption.serviceTimeSlot.split('-')[0];
         string stredt = aoption.serviceDate+' '+aoption.serviceTimeSlot.split('-')[1];
         DateTime dt = DateTime.parse(strdt);
         DateTime edt = DateTime.parse(stredt);
         
         List<Workorder__c> lstWorkOrd = [select Id,Name,Scheduled_Start_Date__c,Scheduled_End_Date__c from Workorder__c where Work_Team__c = :aoption.workTeamId AND Scheduled_Start_Date__c >= :dt AND Scheduled_End_Date__c <= :edt AND (Status__c = 'Scheduled' OR Status__c = 'Requires Review' OR Status__c = 'Requires Rescheduling')];
         if(lstWorkOrd.size()>0){
             for(Workorder__c w: lstWorkOrd ){
                 if(dt >= w.Scheduled_Start_Date__c &&
                    dt <= w.Scheduled_End_Date__c ){
                        //return false;
                        return '<tr><td>'+w.Name+'</td><td>'+w.Scheduled_Start_Date__c+'</td><td>'+w.Scheduled_End_Date__c+'</td></tr>';
                }
                
                if(edt >= w.Scheduled_Start_Date__c &&
                    edt <= w.Scheduled_End_Date__c ){
                        //return false;
                        return '<tr><td>'+w.Name+'</td><td>'+w.Scheduled_Start_Date__c+'</td><td>'+w.Scheduled_End_Date__c+'</td></tr>';
                }
                if(dt <= w.Scheduled_Start_Date__c &&
                    edt  >= w.Scheduled_End_Date__c){
                    
                    //return false; 
                    return '<tr><td>'+w.Name+'</td><td>'+w.Scheduled_Start_Date__c+'</td><td>'+w.Scheduled_End_Date__c+'</td></tr>';
                }
             }
             //return false;
         }         
         return 'success';
    }
}