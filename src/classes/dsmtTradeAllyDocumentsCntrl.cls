public class dsmtTradeAllyDocumentsCntrl {
    public String documentsForManagerJSON{get;set;} 
    public DSMTracker_Contact__c thisContact{get;set;} 
    public string taAccId{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
    
    public dsmtTradeAllyDocumentsCntrl(){
        isPortal = true;
        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
        List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
            if(lstUser.size()>0){
                if(lstUser[0].IsPortalEnabled){
                    headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                }
                else{
                    headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                    isPortal = false;
                }
            }
        loadLoggedInUserContactInfo();
        if(thisContact != null && thisContact.Trade_Ally_Account__c != null){
            taAccId = thisContact.Trade_Ally_Account__c;
        }
    }
    
     public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    
    public void loadDocumentsForManager(){
        List<Object> docRows = new List<Object>();
        if(thisContact != null && thisContact.Trade_Ally_Account__c != null){
           Date nxt30Date = Date.today()+30;
           taAccId = thisContact.Trade_Ally_Account__c;
           List<Attachment__c> lstAttchments = [select Id,Name,Attachment_Name__c,Attachment_Type__c,Expires_On__c,File_Url__c,DSMTracker_Contact__r.First_Name__c,DSMTracker_Contact__r.Last_Name__c,Status__c
                                              // from Attachment__c where Expires_On__c!=null AND Expires_On__c <= :nxt30Date AND Trade_Ally_Account__c =: thisContact.Trade_Ally_Account__c];
                                               from Attachment__c where Expires_On__c!=null  AND Trade_Ally_Account__c =: thisContact.Trade_Ally_Account__c];
            
            if(lstAttchments != null){
                for(Attachment__c doc :lstAttchments ){
                    string name = doc.DSMTracker_Contact__r.First_Name__c!=null?doc.DSMTracker_Contact__r.First_Name__c:'';
                    name += doc.DSMTracker_Contact__r.Last_Name__c!=null?doc.DSMTracker_Contact__r.Last_Name__c:'';
                    string isRed = doc.Expires_On__c < Date.today()?'True':'False';
                    docRows.add(new Map<String,Object>{
                          'Id'=>doc.Id,
                            'AttachmentName'=>doc.Attachment_Name__c,
                            'AttachmentNumber'=>doc.Name,
                            'Type'=>doc.Attachment_Type__c,
                            'ExpiryDate'=>string.valueOf(doc.Expires_On__c.Month())+'/'+string.valueOf(doc.Expires_On__c.Day())+'/'+string.valueOf(doc.Expires_On__c.Year()),
                            'Employee'=>name ,
                            'Status'=>doc.Status__c,
                            'DownloadURL'=> doc.File_Url__c,
                            'IsRed'=> isRed 
                    });
                }
            }
        }
        this.documentsForManagerJSON = JSON.serialize(docRows);
    }
    
    public void loadLoggedInUserContactInfo(){
        User loggedInUser = [SELECT Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name FROM User WHERE Id =: UserInfo.getUserId()];
        if(loggedInUser != null && loggedInUser.ContactId != null){
            thisContact = [
                SELECT Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Trade_Ally_Account__r.Id,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c 
                FROM DSMTracker_Contact__c 
                WHERE Contact__r.Id = : loggedInUser.ContactId AND Trade_Ally_Account__c != null 
                LIMIT 1
            ];
        }
    }
}