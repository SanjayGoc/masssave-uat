@isTest
Public class dsmtConsoleHomePageCntrlTest{

    @isTest
    public static void test_method_one(){
        dsmtConsoleHomePageCntrl cntrl = new dsmtConsoleHomePageCntrl();
        List<Object> properties = new List<Object>{
            cntrl.imgURL,
            cntrl.installJSON,
            cntrl.PostReviewjSON,
            cntrl.selectedAppointmentId,
            cntrl.selectedAssessId,
            cntrl.newEnergyAssesmentURL,
            cntrl.getHESReviews(),
            cntrl.getHESServiceRequests(),
            cntrl.getHESOpenContracts(),
            cntrl.getHESProjectsReadyForReview(),
            cntrl.getHESProjectsReadyToInstall(),
            cntrl.getHESProjectsInstalled(),
            cntrl.getPendingAcceptance(),
            cntrl.getPendingScheduling(),
            cntrl.getPastDueInstalls(),
            cntrl.getFailedInvoiceReview(),
            cntrl.getWorkScopes(),
            cntrl.getWorkScopesRejected()
        };
        cntrl.generateNewEnergyAssesmentURL();
        cntrl.initInstalls();
        cntrl.initpostInspectionGird();
    }

    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        insert obj;
        
       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c = Tacc.Id;
        dsmt.Portal_User__c = UserInfo.getUserId();
        update dsmt;
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        update app;
        //  dsmt.Contact__c= cont.id;
        //update dsmt;
        Service_Request__c sr = new Service_Request__c();
        sr.DSMTracker_Contact__c = dsmt.Id;
        insert sr;
        
        dsmtConsoleHomePageCntrl controller = new dsmtConsoleHomePageCntrl();
        controller.GetContactInfo();
        controller.TilesCounting();
        controller.initScheduledAppointments();
        controller.initEnergySpecTickets();
        //  controller.cloneCallScriptLineItem(obj.Id);
        
    }

    static testMethod void case2(){
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);

        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = hexDigest + 'test2@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
            //Create account
            Account portalAccount1 = new Account(
                Name = 'TestAccount',
                OwnerId = portalAccountOwner1.Id
            );
            insert portalAccount1;
            
                
            User user1 = Datagenerator.CreatePortalUser();
           
            System.runAs ( user1) {
                Eligibility_Check__c EL= Datagenerator.createELCheck();
                Trade_Ally_Account__c Tacc= new Trade_Ally_Account__c();
                Tacc.Name = 'test';
                insert Tacc;
                DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
                dsmt.Contact__c= user1.contactID;
                dsmt.Trade_Ally_Account__c=Tacc.id;
                // dsmt.Super_User__c=true;
                update dsmt;
                Appointment__c app= Datagenerator.createAppointment(EL.Id);
                app.DSMTracker_Contact__c=dsmt.id;
                app.Appointment_Status__c='Scheduled';
                update app;
                ApexPages.currentPage().getParameters().put('uid',user1.id);
                dsmtConsoleHomePageCntrl controller = new dsmtConsoleHomePageCntrl();
                dsmtConsoleHomePageCntrl.AppointmentModal appoint = new dsmtConsoleHomePageCntrl.AppointmentModal();
                controller.GetContactInfo();
                controller.initScheduledAppointments();
                controller.TilesCounting();
            
            }
        }
        // controller2.gridpage();
    }
}