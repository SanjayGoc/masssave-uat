public class dsmtRecommendationValidationHelper extends dsmtEnergyAssessmentValidationHelper{

    private static String AIR_FLOW_RECOMMENDATION = 'Air Sealing Recommendation';
    private static String LIGHTING_RECOMMENDATION = 'Light Bulb Recommendation';
    private static String APPLIANCE_RECOMMENDATIONS = 'Appliance Recommendation';

    public ValidationCategory validate(Id eaId,ValidationCategory otherValidations){
        ValidationCategory validations = new ValidationCategory('Recommendation Validations','RecommendationValidations');
        if(eaId != null){
            //String partQuery = 'SELECT ' + getSObjectFields('');
            String query = 'SELECT ' + getSObjectFields('Recommendation__c') + ',RecordType.Name FROM Recommendation__c WHERE Energy_Assessment__c=:eaId';
            List<Recommendation__c> recs = Database.query(query);
            for(Recommendation__c rec : recs){
                if(rec.RecordType.Name == AIR_FLOW_RECOMMENDATION && rec.Air_Flow_and_Air_Leakage__c != null){
                    validations.addCategory(validateAirflowRecommendation(rec,otherValidations));
                }else if(rec.RecordType.Name == APPLIANCE_RECOMMENDATIONS){
                    validations.addCategory(validateApplianceRecommendation(rec,otherValidations));
                }else if(rec.RecordType.Name == LIGHTING_RECOMMENDATION){
                    validations.addCategory(validateLightingRecommendations(rec,otherValidations));
                }
            }
        }
        return validations;
    }
    
    private ValidationCategory validateWaterFixtureRecommendation(Recommendation__c rec,ValidationCategory otherValidations){
        ValidationCategory validations = new ValidationCategory(rec.RecordType.Name + '(' + rec.Name + ')' ,rec.Id);
        if(rec.DSMTracker_Product__c == null){
            validations.errors.add(new ValidationMessage('Part is required'));
        }
        if(rec.Quantity__c == null || rec.Quantity__c < 1){
            validations.errors.add(new ValidationMessage('Quantity must be greater than or equal to 1'));
        }
        return validations;
    }
    
    private ValidationCategory validateAirflowRecommendation(Recommendation__c rec,ValidationCategory otherValidations){
        ValidationCategory validations = new ValidationCategory(rec.RecordType.Name + '(' + rec.Name + ')' ,rec.Id);
        
        if(rec.Hours__c == null || rec.Hours__c == 0){
            validations.errors.add(new ValidationMessage('Recommendation is invalid because Hours is blank or zero.'));
        }
        if(rec.Hours__c != null && rec.Hours__c < 1){
            validations.errors.add(new ValidationMessage('Hours must be greater than or equal to 1.'));
        //}else if(rec.){
        }
        return validations;
    }
    private ValidationCategory validateLightingRecommendations(Recommendation__c rec,ValidationCategory otherValidations){
        ValidationCategory validations = new ValidationCategory(rec.RecordType.Name + '(' + rec.Name + ')' ,rec.Id);
        try{
            if(rec.DSMTracker_Product__c == null){
                validations.errors.add(new ValidationMessage('Part is required'));
            }
            if(rec.Quantity__c == null || String.valueOf(rec.Quantity__c).trim() == '' || rec.Quantity__c < 1){
                validations.errors.add(new ValidationMessage('Quantity must be greater than or equal to 1'));
            }
            if(rec.Usage__c == null || rec.Usage__c == 0){
                validations.errors.add(new ValidationMessage('Usage is required'));
            }else if(rec.Usage__c > 24){
                validations.errors.add(new ValidationMessage('Usage must be less than equal to 24'));
            }
            if(rec.Lighting_Location_Picklist__c == null || rec.Lighting_Location_Picklist__c.trim() == ''){
                validations.errors.add(new ValidationMessage('Location is required'));   
            }
        }catch(Exception ex){
            validations.systemExceptions.add(new ValidationMessage(ex));
        }
        return validations;
    }
    
    private ValidationCategory validateApplianceRecommendation(Recommendation__c rec,ValidationCategory otherValidations){
        if(rec.Category__c == 'Appliance' && rec.ApplianceType__c == 'Refrigeration'){
            return validateRefrigeratorAppliance(rec,otherValidations);
        }else if(rec.Category__c == 'Appliance' && rec.ApplianceType__c == 'Washer'){
            return validateWasherAppliance(rec,otherValidations);
        }
        return new ValidationCategory('Appliance Validations','applianceValidations');
    }
    private ValidationCategory validateRefrigeratorAppliance(Recommendation__c rec,ValidationCategory otherValidations){
        ValidationCategory validations = new ValidationCategory(rec.RecordType.Name + '(' + rec.Name + ')' ,rec.Id);
        try{
            if(rec.Refrigerator_Location__c == null || rec.Refrigerator_Location__c.trim() == ''){
                validations.errors.add(new ValidationMessage('Refrigerator Location is required'));
            }
            if(rec.Rated_kWh__c < 1175){
                validations.errors.add(new ValidationMessage('Rated kWh does not meet minimum requirements'));
            }
        }catch(Exception ex){
            validations.systemExceptions.add(new ValidationMessage(ex));
        }
        return validations;
    }
    private ValidationCategory validateWasherAppliance(Recommendation__c rec,ValidationCategory otherValidations){
        ValidationCategory validations = new ValidationCategory(rec.RecordType.Name + '(' + rec.Name + ')' ,rec.Id);
        try{
            if(rec.Appliance__c == null){
                validations.errors.add(new ValidationMessage('Washer is required'));
            }
        }catch(Exception ex){
            validations.systemExceptions.add(new ValidationMessage(ex));
        }
        return validations;
    }
}