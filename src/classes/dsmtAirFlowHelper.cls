public class dsmtAirFlowHelper 
{
    // ComputeShellLeakinessCommon
    //Line #3601
    public static string ComputeShellLeakinessCommon( dsmtEAModel.Surface bp )
    {
      string retval = SurfaceConstants.DefaultShellLeakiness;
      Air_Flow_and_Air_Leakage__c af = bp.ai; 

      // if have actual CFM50, then categorize leakiness according to ShellLeakinessTable
      if ( af.ActualCFM50__c != null && af.ActualCFM50__c != 0 ) {
        retval = 'ExtremelyTight';  // in case we fall through the table scan
        decimal shellAreaGross = bp.eaObj.ShellAreaGross__c;
        if ( shellAreaGross != null && shellAreaGross != 0 ) {
          decimal CFM50PerSSF = af.CFM50__c / shellAreaGross;

          Set<String> keySet = SurfaceConstants.CommonShellLeakinessTable.keySet();
          for ( string key : keySet ) 
          {
              if ( CFM50PerSSF >= SurfaceConstants.CommonShellLeakinessTable.get(key) ) 
              {
                  retval = key;
                  break;
              }
          }
        }
      }

      return retval;
    }


    // ComputeShellCFM50Common
    //Line #3631
    public static decimal ComputeShellCFM50Common( dsmtEAModel.Surface bp )
    {

      decimal retval = null;
      Air_Flow_and_Air_Leakage__c af = bp.ai;

      if ( bp.eaObj != null ) {
        //// look for a BlowerDoorPair first
         if ( bp.blowDoorReadingList != null && bp.blowDoorReadingList.size() > 0) {
             retval  =  bp.blowDoorReadingList[0].Starting_CFM50__c;
         }

        //// if didn't find a BlowerDoorPair with a StartingCFM50, then compute it from ShellAreaGross
        if ( retval == null 
          && bp.eaObj.ShellAreaGross__c != null 
          && af.Leakiness__c !=null && af.Leakiness__c !='') 
        {
            decimal shellAreaGross = dsmtEAModel.ISNULL(bp.eaObj.ShellAreaGross__c);
            if(shellAreaGross !=0)  shellAreaGross = shellAreaGross.setScale(7,roundingmode.HALF_UP);

            system.debug('af.Leakiness__c ' + af.Leakiness__c);

            retval = dsmtEAModel.ISNULL(SurfaceConstants.CommonShellLeakinessTable.get(af.Leakiness__c)) * shellAreaGross ;
        }
      }

      return retval;
    }


    // ComputeAirInfiltrationACHHCommon
    //Line #3657
    public static decimal ComputeAirInfiltrationACHHCommon( dsmtEAModel.Surface bp )
    {
      return SurfaceConstants.DefaultAirInfiltrationACHH;
    }


    // ComputeAirInfiltrationACHCCommon
    //Line #3671
    public static decimal ComputeAirInfiltrationACHCCommon( dsmtEAModel.Surface bp )
    {
      return SurfaceConstants.DefaultAirInfiltrationACHC;
    }


    // ComputeAirInfiltrationACHCommon
    //Line #3681
    public static decimal ComputeAirInfiltrationACHCommon( dsmtEAModel.Surface bp)
    {
      decimal retval = null;

      Air_Flow_and_Air_Leakage__c af = bp.ai; 

      if ( bp.eaObj != null && af.CFM50__c != null && bp.eaObj.Total_Volume__c != null && bp.eaObj.Total_Volume__c  > 0 )
        retval = ( af.CFM50__c * 60 ) / ( bp.eaObj.Total_Volume__c );

      return retval;
    }

    public static decimal GetNewACH( dsmtEAModel.Surface bp )
    {
      decimal retval = null;
      Recommendation__c asr = bp.rcmd;

      if (asr != null && bp.eaObj != null
        && bp.eaObj.Total_Volume__c != null && bp.eaObj.Total_Volume__c > 0
        && bp.eaObj.Nfactor136__c != null)
      {
        retval = (asr.ProposedCFM50__c * 60) / (bp.eaObj.Total_Volume__c * bp.eaObj.Nfactor136__c);
      }

      return retval;
    }

    public static decimal GetNewCFM50( dsmtEAModel.Surface bp )
    {

      decimal retval = (decimal)bp.ai.CFM50__c - GetCFM50Delta(bp);

      return Math.Max( 0, retval );
    }

    public static decimal GetCFM50Delta(dsmtEAModel.Surface bp)
    {
      decimal retval = 0;

      if(bp==null)return retval;
      if(bp.ProductPartData ==null)return retval;

      AirSealing_Part__c ap =null;

      try 
      {
        ap = (AirSealing_Part__c)bp.ProductPartData;
      } catch(Exception ex) 
      {
        
      }

      if(ap==null) return retval;
      retval = dsmtEAModel.ISNULL(ap.CFM50PerHour__c) * bp.rcmd.Hours__c;      
      return retval;
    }

}