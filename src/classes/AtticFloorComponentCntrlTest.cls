@isTest 
private class AtticFloorComponentCntrlTest
{
    static testMethod void test1() {
         
        Floor__c af = new Floor__c();
        af.FloorType__c='test';
        insert af;
      
        Layer__c l = new Layer__c();
        l.Name='test';
        l.HideLayer__c = false;
        l.Floor__c=af.id;
        insert l;
          
        AtticFloorComponentCntrl cntrl = new AtticFloorComponentCntrl();
        String newLayer = cntrl.newLayerType;
        cntrl.atticFloorId=af.Id;
        cntrl.getLayerTypeOptions();
        cntrl.getFloorLayersMap();
        cntrl.saveFloor();
        
        ApexPages.currentPage().getParameters().put('layerType','Flooring');
        cntrl.addNewFloorLayer();
        
        ApexPages.currentPage().getParameters().put('index','0');
        cntrl.deleteFloorLayer();
        //cntrl.saveFloorLayers();
        
    }
 }