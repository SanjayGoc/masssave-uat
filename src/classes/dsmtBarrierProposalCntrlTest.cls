@isTest
public class dsmtBarrierProposalCntrlTest {
 	
    
    @isTest(SeeAllData=true)
    static void dsmtBarrierProposalCntrlTest1(){
        
        Energy_Assessment__c enrObj = new Energy_Assessment__c();   
        enrObj = [select Id from Energy_Assessment__c limit 1];
        
        
        
        Barrier__c barObj = new Barrier__c();
        barObj.Type__c='Type1';
        barObj.Energy_Assessment__c=enrObj.Id;
        insert barObj;
        
        Proposal__c proposalObj = new Proposal__c();
        proposalObj.Name='Test';
		proposalObj.Type__c = 'Insulation';
        proposalObj.Energy_Assessment__c=enrObj.Id;
		insert proposalObj;
        
       
		Recommendation_Scenario__c recSceObj = new Recommendation_Scenario__c();
		recSceObj.Proposal__c = proposalObj.Id;
        insert recSceObj;
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(barObj);
        
        PageReference pageRef = Page.dsmtBarrierProposal; // Add your VF page Name here
  		pageRef.getParameters().put('selectedProposalId', String.valueOf(proposalObj.Id));
  		Test.setCurrentPage(pageRef);
        
        dsmtBarrierProposalController dsmtB = new dsmtBarrierProposalController(sc);  
        dsmtB.RecId = '';
        dsmtB.barrier= new Barrier__c();
        //dsmtB.ProposalWrapperList = new List<dsmtB.ProposalWrapper>();
        
        dsmtB.updateBarrier();

        
    }
    
    @isTest(SeeAllData=true)
    static void dsmtBarrierProposalCntrlTest2(){
        
        Energy_Assessment__c enrObj = new Energy_Assessment__c();   
        enrObj = [select Id from Energy_Assessment__c limit 1];
        
        
        
        Barrier__c barObj = new Barrier__c();
        barObj.Type__c='Type1';
        barObj.Energy_Assessment__c=enrObj.Id;
        insert barObj;
        
        Proposal__c proposalObj = new Proposal__c();
        proposalObj.Name='Test';
		proposalObj.Type__c = 'Insulation';
        proposalObj.Energy_Assessment__c=enrObj.Id;
		insert proposalObj;
        
       
		//Recommendation_Scenario__c recSceObj = new Recommendation_Scenario__c();
		//recSceObj.Proposal__c = proposalObj.Id;
        //insert recSceObj;
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(barObj);
        
        PageReference pageRef = Page.dsmtBarrierProposal; // Add your VF page Name here
  		pageRef.getParameters().put('selectedProposalId', String.valueOf(proposalObj.Id));
  		Test.setCurrentPage(pageRef);
        
        dsmtBarrierProposalController dsmtB = new dsmtBarrierProposalController(sc);  
        dsmtB.RecId = '';
        dsmtB.barrier= new Barrier__c();
        //dsmtB.ProposalWrapperList = new List<dsmtB.ProposalWrapper>();
        
        dsmtB.updateBarrier();
        
    }
}