public class dsmtWorkTeamHelper{
    
    @future(callOut=true)
    public static void syncworkTeam(){
        
            dsmtCallOut.loginDetail ldObj = new dsmtCallOut.loginDetail();
            
            ldObj.userName = Login_Detail__c.getInstance().UserName__c;
            ldObj.password = Login_Detail__c.getInstance().Password__c;
            ldObj.orgID = Login_Detail__c.getInstance().Org_Id__c;
            ldObj.securityToken = Login_Detail__c.getInstance().Security_Token__c;
            ldObj.serverURL = Login_Detail__c.getInstance().Server_URL__c;
            
            dsmtCallOut.Body bodyobj = new dsmtCallOut.Body();
            bodyObj.syncFromDate = Date.today();
            bodyObj.loginDetail  = ldObj;
            
            
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            req.setEndpoint(System_Config__c.getInstance().URL__c+'SyncWorkTeam?type=json');
            req.setMethod('POST');
            String body = JSON.serialize(bodyObj);
            
            system.debug('--body ---'+body);
            req.setBody(body);
            req.setHeader('content-type', 'application/json');
            http = new Http();
            if(!Test.IsrunningTest()){
                res = http.send(req);
                System.debug(res.getBody());
            }
        }
    
}