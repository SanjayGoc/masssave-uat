public class dsmtLightingValidationHelper extends dsmtEnergyAssessmentValidationHelper{
    public ValidationCategory validate(String categoryTitle,String uniqueName,Id lightingId,ValidationCategory otherValidations){
        ValidationCategory lightingValidations = new ValidationCategory(categoryTitle,uniqueName); 
        List<Lighting__c> lightings = Database.query('SELECT ' + getSObjectFields('Lighting__c') + ',(SELECT '+ getSObjectFields('Lighting_Location__c') +' FROM Lighting_Locations__r) FROM Lighting__c WHERE Id=:lightingId');
        if(lightings.size() > 0){
            Lighting__c lighting = lightings.get(0);
            
            validateRequiredFields(lighting ,new List<Validation>{
                new Validation('General_Indoor_Lighting__c','General Indoor Lighting','General Indoor Lighting requires an explicit value'),
                new Validation('General_Outdoor_Lighting__c','General Outdoor Lighting','General Outdoor Lighting requires an explicit value'),
                new Validation('General_Indoor_Lighting_kWh_yr__c','General kWh/yr Indoor','General kWh/yr Indoor requires an explicit value'),
                new Validation('General_Outdoor_Lighting_kWh_yr__c','General kWh/yr Uutdoor','General kWh/yr Uutdoor requires an explicit value')
            },lightingValidations );
            
            if(Lighting.Lighting_Locations__r != null && Lighting.Lighting_Locations__r.size() > 0){
                Integer i = 0;
                for(Lighting_Location__c location : Lighting.Lighting_Locations__r){
                    i++;
                    ValidationCategory locationValidations = new ValidationCategory(location.Name,location.Id);
                    
                    validateRequiredFields(location,new List<Validation>{
                        new Validation('Location__c','Location','Location Requires an explicit value'),
                        new Validation('Total_Watts__c','Total Watts','Total Watss requires an explicit value'),
                        new Validation('Quantity__c','Quantity','Quantity requires an explicit value'),
                        new Validation('Usage__c','Usage','Usage requires an explicit value')
                    },locationValidations);
                    
                    if(location.Total_Watts__c != null && location.Total_Watts__c <= 0){
                        locationValidations.errors.add(new ValidationMessage('Total Watts must be greater than or equal to 1'));
                    }
                    if(location.Quantity__c != null && location.Quantity__c <= 0){
                        locationValidations.errors.add(new ValidationMessage('Quantity must be greater than or equal to 1'));
                    }else if(location.Quantity__c != null && location.Quantity__c > 10){
                        locationValidations.errors.add(new ValidationMessage('Quantity must be less than or equal to 10'));                    
                    }
                    if(location.Usage__c != null && location.Usage__c <= 0){
                        locationValidations.errors.add(new ValidationMessage('Usage must be greater than or equal to 1'));
                    }else if(location.Usage__c != null && location.Usage__c > 24){
                        locationValidations.errors.add(new ValidationMessage('Usage must be less than or equal to 24'));
                    } 
                    
                    lightingValidations.addCategory(locationValidations);
                }
            }
        }    
        return lightingValidations;
    }
}