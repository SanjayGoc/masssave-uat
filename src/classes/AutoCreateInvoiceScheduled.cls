global class AutoCreateInvoiceScheduled Implements Schedulable {
    
    List<Invoice__c> extInvoice = new List<Invoice__c>();
    Map<String,Invoice__c> invMap = new Map<String,Invoice__c>();
    
    global void execute(SchedulableContext sc) {
        invoicetype();
    }

    public Invoice__c createInvoice(InvoiceType__c invSetting) {
        Date startDate = Date.today().toStartOfMonth();
        Date endDate = startDate.addMonths(1).addDays(-1);
        System.debug('--endDate---'+endDate );
        
        
        /*String target = '/'; 
        String replacement = '';
        String formatedDate= endDate.format().replace(target, replacement); */
        
        String target = '/'; 
        String replacement = '';                      
        Datetime output = endDate;                       
        String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);


       
        Invoice__c inv = new Invoice__c();
        inv.Invoice_Type__c = invSetting.TypeName__c;
        inv.Invoice_Date__c = endDate;
        inv.Invoice_Number__c = formatedDate + invSetting.TypeName__c;
        
        if(invMap.get(inv.Invoice_Number__c) != null){
            for(integer i = 1; i < 1000; i++){
                
                if(invMap.get(inv.Invoice_Number__c+'-'+i) == null){
                    inv.Invoice_Number__c = inv.Invoice_Number__c+'-'+i;
                    invMap.put(inv.Invoice_Number__c,inv);
                    break;
                }
            }
        }
        
        inv.Work_Type__c = invSetting.Work_Type__c;
        inv.Provider__c = invSetting.Provider__c;
        inv.Status__c = 'In Process';
        return inv;
    }
    
  
    public void invoicetype() {
    
        Date startDate = Date.today().toStartOfMonth();
        Date endDate = startDate.addMonths(1).addDays(-1);
        System.debug('--endDate---'+endDate );
        /*String target = '/'; 
        String replacement = '';
        String formatedDate= endDate.format().replace(target, replacement); */
        
        String target = '/'; 
        String replacement = '';                      
        Datetime output = endDate;                       
        String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
        
        extInvoice  = [select id,Invoice_Number__c from invoice__c where Invoice_Number__c  like : formatedDate + '%'];
        
        for(Invoice__c inv : extInvoice){
            invMap.put(inv.Invoice_Number__c,inv);
        }
        // Create a set of invoice types
        List<InvoiceType__c> invoiceTypes = InvoiceType__c.getall().values();
        
        List<Invoice__c> invList1 = new List<Invoice__c>();
        
        for (InvoiceType__c invType : invoiceTypes) {
            if(invType.Isactive__c){
                invList1.add(createInvoice(invType));
            }
        }
        
        if(invList1.size() > 0)
            insert invList1;
    }
}