public with sharing class dsmtRelatedListController {
    public String getParam(String key){
        return ApexPages.currentPage().getParameters().get(key);
    }
    public boolean isNotNullOrEmpty(String value){
        return value != null || !String.isBlank(value) || !String.isEmpty(value);
    }
    public boolean getHasPageError(){
        return ApexPages.hasMessages();
    }

    /* dsmtRelatedList.page **************************************************************************************************/
    public String childRecordPluralName {get;private set;}
    public SObject parentRecord {get;private set;}
    public List<SObject> childRecords {get;private set;}
    public String parentRecordId {get;private set;}
    public String parentRecordType {get;private set;}
    public String childRecordType {get;private set;}
    public String childRecordParentFieldName {get;private set;}
    public String childRecordFieldsetName {get;private set;}
    public String childRecordRecordTypeId {get;private set;}
    public String recommendationProjects {get;private set;}

    public dsmtRelatedListController() { }
    
    public PageReference initRelatedList(){
        parentRecordId = getParam('prId');
        parentRecordType = getParam('pRType');
        childRecordType = getParam('cRType');
        childRecordParentFieldName = getParam('cRPFName');
        childRecordFieldsetName = getParam('cRFName');
        childRecordRecordTypeId = getParam('cRRTId');
        recommendationProjects = getParam('recommendationProjects');


        if(isNotNullOrEmpty(parentRecordId) && isNotNullOrEmpty(parentRecordType) && isNotNullOrEmpty(childRecordType) && isNotNullOrEmpty(childRecordParentFieldName) && isNotNullOrEmpty(childRecordFieldsetName)){
            List<SObject> parentRecords = Database.query('SELECT Id,Name FROM ' + parentRecordType + ' WHERE Id= :parentRecordId');
            if(parentRecords.size() > 0){
                parentRecord = parentRecords.get(0);
            } 

            // FIELDSET
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
            Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(childRecordType); 
            Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
            childRecordPluralName = DescribeSObjectResultObj.getLabelPlural();
            Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(childRecordFieldsetName);
            List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
            String childFields = 'Id,Name';
            Set<String> childFieldsSet = new Set<String>{'Id','Name'};
            for(Schema.FieldSetMember field : fieldSetMemberList){
                if(childFieldsSet.add(field.getFieldPath())){
                    childFields += ',' + field.getFieldPath();
                }
            }

            if(isNotNullOrEmpty(recommendationProjects) && recommendationProjects.toLowerCase().trim() == 'true' && childRecordType.equalsIgnoreCase('Recommendation_Scenario__c') ){
                List<Recommendation__c> recommendations = [SELECT Id,Name,Recommendation_Scenario__c FROM Recommendation__c WHERE Energy_Assessment__c =:parentRecordId];
                if(Test.isRunningTest()){
                    recommendations = new List<Recommendation__c>{
                        new Recommendation__c()
                    };
                }
                if(recommendations.size() > 0){
                    Set<Id> projectIdSet = new Set<Id>();
                    for(Recommendation__c rec : recommendations){
                        if(rec.Recommendation_Scenario__c != null){ projectIdSet.add(rec.Recommendation_Scenario__c); }
                    }
                    String soql = 'SELECT ' + childFields + ' FROM ' + childRecordType + ' WHERE Id IN :projectIdSet';
                    if(isNotNullOrEmpty(childRecordRecordTypeId)){
                        soql += ' AND RecordTypeId= :childRecordRecordTypeId';
                    }
                    childRecords = Database.query(soql);
                }
            }else if(childRecordParentFieldName.toLowerCase().trim() != 'none'){
                String soql = 'SELECT ' + childFields + ' FROM ' + childRecordType + ' WHERE ' + childRecordParentFieldName + ' =:parentRecordId';
                if(isNotNullOrEmpty(childRecordRecordTypeId)){
                    soql += ' AND RecordTypeId= :childRecordRecordTypeId';
                }
                childRecords = Database.query(soql);
            }
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please provide valid required parameters.'));
        }

        if(childRecordPluralName == null){
            childRecordPluralName = 'Related List';
        }
        return null;
    }


    /* dsmtCLRESTileDetails.page ******************************************************************************************/
    public String pageHeaderTitle{get;private set;}
    public String pageHeaderSubTitle{get;private set;}
    public String tileId{get;private set;}
    public transient List<SObject> records{get;private set;}

    public PageReference loadCLRESTilesDetails(){
        records = new List<SObject>();
        pageHeaderTitle = UserInfo.getName();
        Id loggedInUserId = UserInfo.getUserId();
        tileId = getParam('tileId');
        if(isNotNullOrEmpty(tileId)){
            Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
            if(tileId.equalsIgnoreCase('HESReviews')){
                pageHeaderSubTitle = 'My Reviews';
                records = [
                    SELECT Id,Name,Type__c,Status__c,Failure_Notes__c,Energy_Assessment__c,Energy_Assessment__r.Name,Customer__c,Customer__r.Name,Address__c,City__c
                    FROM Review__c 
                    WHERE CreatedById =:loggedInUserId
                    ORDER BY LastModifiedDate DESC
                ];
            }else if(tileId.equalsIgnoreCase('HESServiceRequests')){
                pageHeaderSubTitle = 'My Service Requests';
                records = [
                    SELECT Id,Name,Type__c,Sub_Type__c,Status__c,Subject__c,Description__c,Customer__c,Customer_Address__c
                    FROM Service_Request__c 
                    WHERE OwnerId =:loggedInUserId AND CreatedById != :loggedInUserId
                    ORDER BY LastModifiedDate DESC
                ];
            }else if(tileId.equalsIgnoreCase('HESOpenContracts')){
                pageHeaderSubTitle = 'My Open Contracts';
                String proposalStatus = 'Proposed';
                Set<String> proposalTypes = new Set<String>{'Air Sealing','Electric Thermostats','Weatherization'};
                records = [
                    SELECT Id,Name,Energy_Assessment__c,Status__c,Customer_Name__c,Total_Cost__c,Total_Annual_Energy_Savings__c
                    FROM Proposal__c 
                    WHERE CreatedById = :loggedInUserId AND Status__c =:proposalStatus AND Name_Type__c IN :proposalTypes
                    ORDER BY LastModifiedDate DESC
                ];
            }else if(tileId.equalsIgnoreCase('HESProjectsReadyForReview')){
                pageHeaderSubTitle = 'My Projects Ready for Review';
                String status = 'Contracted';
                records = [
                    SELECT Id,Name,Project_ID__c,Energy_Assessment__c,Type__c,Status__c,Customer_Name__c,Address__c,City__c,Total_Cost__c,Installed_Date__c
                    FROM Recommendation_Scenario__c 
                    WHERE CreatedById = :loggedInUserId AND Status__c =:status
                    ORDER BY LastModifiedDate DESC
                ];
            }else if(tileId.equalsIgnoreCase('HESProjectsReadyToInstall')){
                pageHeaderSubTitle = 'My Projects Ready to Install';
                Set<String> reviewRecordTypes = new Set<String>{'Work Scope Review Manual','Work Scope Review'};
                Set<String> reviewStatuses = new Set<String>{'Accepted','Scheduled','Pending Contractor Acceptance'};
                List<Project_Review__c> recordsList = [
                    SELECT Id,Project__c,
                            Project__r.Id,Project__r.Name,Project__r.Project_ID__c,Project__r.Energy_Assessment__c,Project__r.Type__c,
                            Project__r.Status__c,Project__r.Customer_Name__c,Project__r.Address__c,Project__r.City__c,
                            Project__r.Total_Cost__c,Project__r.Installed_Date__c
                    FROM Project_Review__c
                    WHERE Review__c != NULL AND Project__c != NULL 
                        AND Review_Record_Type__c IN :reviewRecordTypes 
                        AND Project__r.CreatedById = :loggedInUserId 
                        AND Review__r.Status__c IN :reviewStatuses
                    ORDER BY Project__r.LastModifiedDate DESC
                ];
                for(Project_Review__c pr : recordsList){ records.add(pr.Project__r); }
            }else if(tileId.equalsIgnoreCase('HESProjectsInstalled')){
                pageHeaderSubTitle = 'My Projects Installed';
                Set<String> reviewRecordTypes = new Set<String>{'Work Scope Review Manual','Work Scope Review'};
                Set<String> reviewStatuses = new Set<String>{'Complete Work Order'};
                List<Project_Review__c> recordsList = [
                    SELECT Id,Project__c,
                            Project__r.Id,Project__r.Name,Project__r.Project_ID__c,Project__r.Energy_Assessment__c,Project__r.Type__c,
                            Project__r.Status__c,Project__r.Customer_Name__c,Project__r.Address__c,Project__r.City__c,
                            Project__r.Total_Cost__c,Project__r.Installed_Date__c
                    FROM Project_Review__c
                    WHERE Review__c != NULL AND Project__c != NULL 
                        AND Review_Record_Type__c IN :reviewRecordTypes 
                        AND Project__r.CreatedById = :loggedInUserId 
                        AND Review__r.Status__c IN :reviewStatuses
                    ORDER BY Project__r.LastModifiedDate DESC
                ];
                for(Project_Review__c pr : recordsList){ records.add(pr.Project__r); }
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please provide valid tile id.'));  
            }
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please provide valid tile id.'));      
        }
        return null;
    }
}