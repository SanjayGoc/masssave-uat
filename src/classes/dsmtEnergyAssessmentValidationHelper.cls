public virtual class dsmtEnergyAssessmentValidationHelper{
    public Enum ValidationMessageType{
        ERROR,WARNING,INFO,SYSTEM_EXCEPTION
    }
    public class ValidationMessage{
        public String sObjectApiName{get;set;}
        public Id sObjectId{get;set;}
        public String fieldApiName{get;set;}
        public Object fieldValue{get;set;}
        public Schema.DescribeFieldResult fieldMetadata{get;set;}

        public String actualFieldApiName{get;set;}
        public Object actualFieldValue{get;set;}
        public Schema.DescribeFieldResult actualFieldMetadata{get;set;}

        public List<Schema.PicklistEntry> picklistValues{get;set;}


        public ValidationMessageType type{get;set;}
        public String message{get;set;}
        public String fieldName{get;set;}
        public Integer lineNumber{get;set;}
        public String stackTraceString{get;set;}
        public String typeName{get;set;}
        public ValidationMessage(String message){
            this.message = message;
        }
        public ValidationMessage(String message,String sObjectApiName,String fieldApiName, String actualFieldApiName){
            this.message = message;
            this.sObjectApiName = sObjectApiName;
            this.fieldApiName = fieldApiName;
            this.actualFieldApiName = actualFieldApiName;   
        }
        public ValidationMessage(String message,String sObjectApiName,String fieldApiName,Object fieldValue, String actualFieldApiName,Object actualFieldValue){
            this.message = message;
            this.sObjectApiName = sObjectApiName;
            this.fieldApiName = fieldApiName;this.fieldValue = fieldValue;
            this.actualFieldApiName = actualFieldApiName;this.actualFieldValue = actualFieldValue;
        }
        public ValidationMessage(String message,String sObjectApiName,Id sObjectId,String fieldApiName,Object fieldValue, String actualFieldApiName,Object actualFieldValue){
            this.message = message;
            this.sObjectApiName = sObjectApiName;this.sObjectId = sObjectId;
            this.fieldApiName = fieldApiName;this.fieldValue = fieldValue;
            this.actualFieldApiName = actualFieldApiName;this.actualFieldValue = actualFieldValue;
        }
        public ValidationMessage(String message,String sObjectApiName,Id sObjectId,String fieldApiName,Object fieldValue, String actualFieldApiName,Object actualFieldValue,List<Schema.PicklistEntry> picklistValues){
            this.message = message;
            this.sObjectApiName = sObjectApiName;this.sObjectId = sObjectId;
            this.fieldApiName = fieldApiName;this.fieldValue = fieldValue;
            this.actualFieldApiName = actualFieldApiName;this.actualFieldValue = actualFieldValue;
            this.picklistValues = picklistValues;
        }
        public ValidationMessage(String message,String sObjectApiName,Id sObjectId,String fieldApiName,Object fieldValue, String actualFieldApiName,Object actualFieldValue,Schema.DescribeFieldResult fieldMetadata,Schema.DescribeFieldResult actualFieldMetadata){
            this.message = message;
            this.sObjectApiName = sObjectApiName;this.sObjectId = sObjectId;
            this.fieldApiName = fieldApiName;this.fieldValue = fieldValue;
            this.actualFieldApiName = actualFieldApiName;this.actualFieldValue = actualFieldValue;
            this.fieldMetadata = fieldMetadata;
            this.actualFieldMetadata = actualFieldMetadata;
            if(String.valueOf(fieldMetadata.getType()).equalsIgnoreCase('picklist')){
                this.picklistValues = fieldMetadata.getPicklistValues();
            }
        }
        
        public ValidationMessage(Exception ex){
            this.message = ex.getMessage();
            this.lineNumber = ex.getLineNumber();
            this.stackTraceString = ex.getStackTraceString();
            this.typeName = ex.getTypeName();
            this.type = ValidationMessageType.SYSTEM_EXCEPTION;
        }
    }
    
    public class ValidationCategory{
        public String title{get;set;}
        public String uniqueName{get;set;}
        public List<ValidationMessage> errors{get;set;}
        public List<ValidationMessage> warnings{get;set;}
        public List<ValidationMessage> infos{get;set;}
        public Integer messagesCount{get{ messagesCount = errors.size() + warnings.size() + infos.size(); return messagesCount; }set;}
        public List<ValidationMessage> systemExceptions{get;set;}
        public ValidationCategory(String title,String uniqueName){
            this.title = title;
            this.uniqueName = uniqueName;
            this.errors = new List<ValidationMessage>();
            this.warnings = new List<ValidationMessage>();
            this.infos = new List<ValidationMessage>();
            this.systemExceptions = new List<ValidationMessage>();
        }
        private List<ValidationCategory> categories{get;set;}
        public void addCategory(ValidationCategory category){
            if(this.categories == null){ this.categories = new List<ValidationCategory>();}
            this.categories.add(category);
        }
        public List<ValidationCategory> subCategories(){
            if(this.categories == null) return new List<ValidationCategory>();
            return this.categories;
        }
    }
    public class ValidationResponse{
        public List<ValidationCategory> categories{get;private set;}
        public Map<String,List<ValidationMessage>> systemExceptions{get;set;}
        public ValidationResponse(){
            this.categories = new List<ValidationCategory>();
            this.systemExceptions = new Map<String,List<ValidationMessage>>();
        }
        public void addCategory(ValidationCategory category){
            if(category.systemExceptions.size() > 0){
                this.systemExceptions.put(category.uniqueName,category.systemExceptions);
            }
            if(category.messagesCount > 0){
                this.categories.add(category);
            }
            for(ValidationCategory subCategory :category.subCategories()){
                addCategory(subCategory);
            }
        }
    }
    public class Validation{
        public String fieldName{get;set;}
        public String fieldLabel{get;set;}
        public String message{get;set;}
        public Validation(String fieldName,String fieldLabel,String message){
            this.fieldName = fieldName;
            this.fieldLabel = fieldLabel;
            this.message = message;
        }
        public Validation(String fieldName,String fieldLabel){
            this(fieldName,fieldLabel,null);
        }
    }

    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
    public static void validateRequiredFields(SObject sObj,Map<String,String> reqFieldsMap,ValidationCategory category){
        try{
            for(String fieldName :reqFieldsMap.keySet()){
                if(sObj.get(fieldName) == null || String.valueOf(sObj.get(fieldName)).trim() == ''){
                    category.errors.add(new ValidationMessage(reqFieldsMap.get(fieldName) + ' is required.'));
                }
            }
        }catch(Exception ex){
            category.systemExceptions.add(new ValidationMessage(ex));
        }
    }
    public static void validateRequiredFields(SObject sObj,List<Validation> validations,ValidationCategory category){
        try{
            for(Validation v :validations){
                if(sObj.get(v.fieldName) == null || String.valueOf(sObj.get(v.fieldName)).trim() == ''){
                    if(v.message == null || v.message.trim() == ''){
                        category.errors.add(new ValidationMessage(v.fieldLabel + ' is required.'));
                    }else{
                        category.errors.add(new ValidationMessage(v.message));
                    }
                }
            }
        }catch(Exception ex){
            category.systemExceptions.add(new ValidationMessage(ex));
        }
    }

    /* CUSTOM OBJECT MODELS */
    public virtual class SObjectModel{
        public Map<Id,SObject> recordsMap{
            get{
                if(recordsMap == null){
                    recordsMap = new Map<Id,SObject>();
                }
                return recordsMap;
            }
            set;
        }
    }
    public class ThermalModel extends SObjectModel{
        public String recordTypeName{get;set;}
        public Map<Id,AtticModel> atticsMap{
            get{
                if(atticsMap == null){
                    atticsMap = new Map<Id,AtticModel>();
                }
                return atticsMap;
            }
            set;
        }
    }
    
    public class AtticModel extends SObjectModel{
         public Map<Id,CeilingModel> atticRoofsMap{
            get{
                if(atticRoofsMap == null){
                    atticRoofsMap = new Map<Id,CeilingModel>();
                }
                return atticRoofsMap;
            }
            set;
        }
        public Map<Id,WallModel> atticWallsMap{
            get{
                if(atticWallsMap == null){
                    atticWallsMap = new Map<Id,WallModel>();
                }
                return atticWallsMap;
            }
            set;
        }
        public Map<Id,FloorModel> atticFloorsMap{
            get{
                if(atticFloorsMap == null){
                    atticFloorsMap = new Map<Id,FloorModel>();
                }
                return atticFloorsMap;
            }
            set;
        }
    }
    public class CeilingModel extends SObjectModel{}
    public class WallModel extends SObjectModel{}
    public class FloorModel extends SObjectModel{}
}