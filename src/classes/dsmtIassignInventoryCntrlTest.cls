@isTest
Public class dsmtIassignInventoryCntrlTest {
    @isTest
    public static void runTest(){
        
        Location__c  loc = Datagenerator.createLocation();
        insert loc;
        
        Region__c  reg = Datagenerator.CreateRegion(loc.Id);
        insert reg;
        
        Employee__c emp = Datagenerator.createEmployee(loc.Id);
        insert emp;
        
        Warehouse__c ware = Datagenerator.createWarehouse();
        insert ware;
        
        Product__c prod = Datagenerator.createProduct();
        insert prod;
        
        ApexPages.currentPage().getParameters().put('id',emp.Id);
        dsmtassignInventoryCntrl cntrl =new dsmtassignInventoryCntrl();
        cntrl.selectedInvIds = prod.Id;
        cntrl.selectedEmp = emp.Id;
        cntrl.assignInventory();
        cntrl.removeAssignInventory();
        
    }
}