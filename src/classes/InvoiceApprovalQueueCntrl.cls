public with sharing class InvoiceApprovalQueueCntrl {

    public String comments {get;set;}
    public Boolean hide_rejected {get;set;}
    public Boolean hide_more_info {get;set;}
    public String selected_program {get;set;}
    public String selected_program_name {get;set;}

    public List<SelectOption> program_list {get;set;}
    public List<InvoiceWrapperClass> invoiceWrappers {get;set;}
    
    public String enr_app_id{get;set;}
    public String ea_id {get;set;}
    public String stage {get;set;}
    public String exception_str{get;set;}
    public String excep_msg{get;set;}
    public String rejectionReasons{get;set;}
    public Boolean err_msg{get;set;}
    public String ea_with_oexp{get;set;}
    public String pay_id{get;set;}
    public String commentsss{get;set;}
    public Static String invoicetype;
    
   // public List<Enrollment_Application__c> ea_list {get;set;}
    public List<Exception__c> exceptions{get;set;}
    
    public Boolean payProcess{get;set;}
    
    private Id batchId;
    public Boolean pollerBool {get;set;}
    
    public static String rejectionReason;
    
    public Map<string,string> mapProgramCustom;
    public boolean TANameDisplay{get;set;}

    public InvoiceApprovalQueueCntrl() {
        try {
            mapProgramCustom =new Map<string,string>();
            program_list = new List<SelectOption>();
            exceptions = new List<Exception__c>();
        //    ea_list = new List<Enrollment_Application__c>();
            payProcess = false;
            pollerBool = false;
            
            program_list.add(new SelectOption('None', '--Select Invoice Type--'));
            //program_list.add(new SelectOption('All', 'All'));
           /* for (Program__c program: [SELECT Id, Name from Program__c ORDER BY Program__c.Name ASC]) {
                program_list.add(new SelectOption(program.Id, program.Name));
            } */
            
            
            Set<String> invoiceNumber = new Set<String>();
            List<Invoice_Line_Item__c> lstili = queryInvoice();
            for(Invoice_Line_Item__c ili : queryInvoice()){
                 invoiceNumber.add(ili.Invoice__r.invoice_number__c);
             }
            for(String str : invoiceNumber){
                 program_list.add(new SelectOption(str,str));
              }
            hide_rejected = false;
            hide_more_info = false;
            //selected_program = 'None';
            invoiceWrappers = new List<InvoiceWrapperClass>();
            buildPaymentWrapperList();
        } catch (Exception ex) {
            String message = ex.getStackTraceString() + ' -- ' + ex.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, message));
           // LogUtil.write('ApplicationExceptionsController.ApplicationExceptionsController()', ex);
        }
    }

    public final static List<String> paymentFieldsNeeded = new List<String>{
        'Id',
        'Name',
        'First_Name__c',
        'Last_Name__c',
        'Invoice__r.Invoice_Amount__c',
        'Review__r.Customer__r.Name',
        'Workorder__r.Customer__r.Name',
        'Recommendation__r.Energy_Assessment__r.Customer__r.Name',
        'Invoice__r.Account__r.Name',
        'Invoice__r.Provider__c',
        'PRIM_HEAT_FUEL__c',
        'MEA_ID__c',
        'PART_ID__c',
        'QTY__c',
        'INSTALLED_DATE__c',
        'Unit_Cost__c',
        'SAVINGS_ELE__c',
        'SAVINGS_GAS__c',
        'SAVINGS_PROPANE__c',
        'SAVINGS_OIL__c',
        'SAVINGS_OTHER__c',
        'Invoice__r.Billing_city__c',
        'Invoice__r.Invoice_Type__c',
        'Invoice__r.Invoice_Number__c',
        'Review__r.Customer__r.Electric_Provider_Name__c',
        'Review__r.Customer__r.Electric_Account_Number__c',
        'Review__r.Customer__r.Gas_Account_Number__c',
        'Review__r.Customer__r.Gas_Provider_Name__c',
        'Review__r.Customer__r.Service_City__c',
        'Workorder__r.Customer__r.Service_City__c',
        'Workorder__r.Customer__r.Electric_Provider_Name__c',
        'Workorder__r.Customer__r.Electric_Account_Number__c',
        'Workorder__r.Customer__r.Gas_Account_Number__c',
        'Workorder__r.Customer__r.Gas_Provider_Name__c',
        'Recommendation__r.Energy_Assessment__r.Customer__r.Service_City__c',
        'Recommendation__r.Energy_Assessment__r.Customer__r.Electric_Provider_Name__c',
        'Recommendation__r.Energy_Assessment__r.Customer__r.Electric_Account_Number__c',
        'Recommendation__r.Energy_Assessment__r.Customer__r.Gas_Account_Number__c',
        'Recommendation__r.Energy_Assessment__r.Customer__r.Gas_Provider_Name__c',
        'Calculated_Incentive_Amount__c',
        'Review__c','Workorder__c','Recommendation__c',
        'Voided__c',
        'Review__r.Name'
    };

    public static String getNeededPaymentFields(){
        return String.join(paymentFieldsNeeded, ', ');
    }
//Scenario 1:
//Payment is associated with an EA
// Payment.Status = 'Approval Pending'
// EA.Stage = 'Complete'
// EA.Status = 'Awaiting Review'
// EA.Program = selected Program

//Scenario 2:
//Payment is associate with a File Request
// Payment.Status = 'PM Approval Pending'
// Payment.FileRequest not null
// FileRequest.MOU.Program = selected Program

    public List<Invoice_Line_Item__c> queryInvoice(){
        
            
       String query = 'SELECT Recommendation__r.Recommendation_Scenario__r.Type__c,Project_Type__c, ' + getNeededPaymentFields() + ' FROM Invoice_Line_Item__c';
       String whereClauseForNonFileRequest = ' Status__c != \'Approved\' And Status__c != \'Submitted\' And ((Voided__c = false and Voided_from_Submitted__c = false) or (Voided__c = true and Voided_from_Submitted__c = true)) And ';
       if(selected_program != null){
          whereClauseForNonFileRequest += 'MEA_Id__c != null and Invoice__r.Invoice_Number__c ='+'\''+selected_program+'\'';
        }else{
          whereClauseForNonFileRequest += 'MEA_Id__c != null and (Review__r.Customer__c != null or Workorder__r.Customer__c != null) ';
        }
        
        query += ' WHERE (' + whereClauseForNonFileRequest + ')';
        query += ' ORDER BY Review__c, Part_Id__c,Name LIMIT 999';
        system.debug('--query--' +query);
        
        if(Test.isRunningTest()){
            query = 'select '+getNeededPaymentFields() +' From Invoice_Line_Item__c';
        }
        
        List<Invoice_Line_Item__c> result = Database.query(query);
        
        return result;
    }

    

    public static InvoiceWrapperClass buildWrapperForNonFileRequest(Invoice_Line_Item__c invli){
        System.debug(LoggingLevel.ERROR, 'payment: '+invli);
        
        InvoiceWrapperClass invoiceWrapper = new InvoiceWrapperClass();
        if(invli.Review__c != null){
            invoiceWrapper.customerName= invli.Review__r.Customer__r.Name;
        }else if(invli.Workorder__c != null){
            invoiceWrapper.customerName= invli.Workorder__r.Customer__r.Name;
        }else if(invli.Recommendation__c != null){
            invoiceWrapper.customerName= invli.Recommendation__r.Energy_Assessment__r.Customer__r.Name;
        }
        //invoiceWrapper.accountnumber = invli.Invoice__r.Account__r.Name;
        invoiceWrapper.billedprovider = invli.Invoice__r.Provider__c;
        invoiceWrapper.primaryheat = invli.PRIM_HEAT_FUEL__c;
        invoiceWrapper.meaid= invli.MEA_ID__c;
        invoiceWrapper.partid = invli.PART_ID__c;
        invoiceWrapper.quantity = String.ValueOf(invli.QTY__c);
        invoiceWrapper.invoiceLineItem = invli;
        
        if(invli.Calculated_Incentive_Amount__c != null){
            invoiceWrapper.amount= String.ValueOf(invli.Calculated_Incentive_Amount__c);
            if(invli.QTY__c != null){
                invoiceWrapper.totalamt = String.ValueOf(invli.QTY__c * invli.Calculated_Incentive_Amount__c);
            }else{
                invoiceWrapper.totalamt = String.ValueOf(invli.Calculated_Incentive_Amount__c);
            }
        }else{
            if(invli.Unit_Cost__c != null){
            invoiceWrapper.amount= String.ValueOf(invli.Unit_Cost__c);
            if(invli.QTY__c != null){
                invoiceWrapper.totalamt = String.ValueOf(invli.QTY__c * invli.Unit_Cost__c);
            }else{
                invoiceWrapper.totalamt = String.ValueOf(invli.Unit_Cost__c);
            }
            }
        }
        
        if(invli.INSTALLED_DATE__c != null){
          invoiceWrapper.installdate= String.ValueOf(invli.INSTALLED_DATE__c.Month())+'/'+String.ValueOf(invli.INSTALLED_DATE__c.Day())+'/'+String.ValueOf(invli.INSTALLED_DATE__c.Year());
        }
        invoiceWrapper.city = invli.Review__r.Customer__r.Service_City__c;
        Decimal energysaving = 0;
        invoiceWrapper.energysaving = '';
        if(invli.SAVINGS_ELE__c != null)
            invoiceWrapper.energysaving += decimal.valueOf(invli.SAVINGS_ELE__c);
        else if(invli.SAVINGS_GAS__c != null)
            invoiceWrapper.energysaving += decimal.valueOf(invli.SAVINGS_GAS__c);
        else if(invli.SAVINGS_PROPANE__c != null)
            invoiceWrapper.energysaving += decimal.valueOf(invli.SAVINGS_PROPANE__c);
        else if(invli.SAVINGS_OIL__c != null)
            invoiceWrapper.energysaving += decimal.valueOf(invli.SAVINGS_OIL__c);
        else if(invli.SAVINGS_OTHER__c != null)
            invoiceWrapper.energysaving += decimal.valueOf(invli.SAVINGS_OTHER__c);
            
        //invoiceWrapper.accountnumber = invli.Review__r.Customer__r.Gas_Account_Number__c;
        
        if(invli.invoice__r.Invoice_Type__c != null){
        
        
        if(invli.Invoice__r.Invoice_Type__c == 'NSTAR416'){          
            
            if(invli.Review__c != null){
                if(invli.Review__r.Customer__r.Electric_Provider_Name__c == 'Eversource East Electric'){
                        invoiceWrapper.accountnumber = invli.Review__r.Customer__r.Electric_Account_Number__c;
                }else{
                        invoiceWrapper.accountnumber = invli.Review__r.Customer__r.Gas_Account_Number__c;
                }         
            }else if(invli.Workorder__c != null){
                if(invli.Workorder__r.Customer__r.Electric_Provider_Name__c == 'Eversource East Electric'){
                        invoiceWrapper.accountnumber = invli.Workorder__r.Customer__r.Electric_Account_Number__c;
                }else{
                        invoiceWrapper.accountnumber = invli.Workorder__r.Customer__r.Gas_Account_Number__c;
                }
            }else if(invli.Recommendation__c != null){
                if(invli.Recommendation__r.Energy_Assessment__r.Customer__r.Electric_Provider_Name__c == 'Eversource East Electric'){
                        invoiceWrapper.accountnumber = invli.Recommendation__r.Energy_Assessment__r.Customer__r.Electric_Account_Number__c;
                }else{
                        invoiceWrapper.accountnumber = invli.Recommendation__r.Energy_Assessment__r.Customer__r.Gas_Account_Number__c;
                }
            }
            
        }else if(invli.Invoice__r.Invoice_Type__c == 'WMECO416'){          
            if(invli.Review__c != null){
                if(invli.Review__r.Customer__r.Electric_Provider_Name__c == 'Eversource West Electric'){
                        invoiceWrapper.accountnumber = invli.Review__r.Customer__r.Electric_Account_Number__c;
                }else{
                        invoiceWrapper.accountnumber = invli.Review__r.Customer__r.Gas_Account_Number__c;
                }         
            }else if(invli.Workorder__c != null){
                if(invli.Workorder__r.Customer__r.Electric_Provider_Name__c == 'Eversource West Electric'){
                        invoiceWrapper.accountnumber = invli.Workorder__r.Customer__r.Electric_Account_Number__c;
                }else{
                        invoiceWrapper.accountnumber = invli.Workorder__r.Customer__r.Gas_Account_Number__c;
                } 
            }else if(invli.Recommendation__c != null){
                if(invli.Recommendation__r.Energy_Assessment__r.Customer__r.Electric_Provider_Name__c == 'Eversource West Electric'){
                        invoiceWrapper.accountnumber = invli.Recommendation__r.Energy_Assessment__r.Customer__r.Electric_Account_Number__c;
                }else{
                        invoiceWrapper.accountnumber = invli.Recommendation__r.Energy_Assessment__r.Customer__r.Gas_Account_Number__c;
                } 
            }
            
        }else if(invli.Invoice__r.Invoice_Type__c == 'NSTARGAS413'){
            /*if(invli.Review__r.Customer__r.Electric_Provider_Name__c == 'Eversource West Electric'){
                    invoiceWrapper.accountnumber = invli.Review__r.Customer__r.Electric_Account_Number__c;
            }else{*/
                if(invli.Review__c != null){
                    invoiceWrapper.accountnumber = invli.Review__r.Customer__r.Gas_Account_Number__c;
                }else if(invli.Workorder__c != null){
                    invoiceWrapper.accountnumber = invli.Workorder__r.Customer__r.Gas_Account_Number__c;
                }else if(invli.Recommendation__c != null){
                    invoiceWrapper.accountnumber = invli.Recommendation__r.Energy_Assessment__r.Customer__r.Gas_Account_Number__c;
                }
            //}
        }
        
        } 
        return invoiceWrapper; 
    }

   
   
    public void buildPaymentWrapperList() {
        invoiceWrappers = new List<InvoiceWrapperClass> ();
      //  ea_list = new List<Enrollment_Application__c>();
        
        boolean foundEAWithOpenException = false;
        integer wrapperCounter = 0;
        
        selected_program_name = '';
        TANameDisplay = false;
        
        
        for(Invoice_Line_Item__c ili : queryInvoice()){
            
            
            wrapperCounter++;
            // if (wrapperCounter > 200) break;
            InvoiceWrapperClass invoiceWrapper = buildWrapperForNonFileRequest(ili);
            invoiceWrapper.projectName = ili.Recommendation__r.Recommendation_Scenario__r.Type__c;
            if(invoiceWrapper.projectName == null || invoiceWrapper.projectName == ''){
                invoiceWrapper.projectName = ili.Project_Type__c;
            }
            /*if(payment.Enrollment_Application__c != null && payment.Enrollment_Application_Program__c.contains('Lighting') ){
                if((payment.Enrollment_Application__r.Mailing_Contact_Last_Name__c != null || 
                    payment.Enrollment_Application__r.Mailing_Contact_First_Name__c != null )&&
                    payment.Enrollment_Application__r.Payment_Mailing_Email_Address__c!= null &&
                    payment.Enrollment_Application__r.Mailing_City__c != null && payment.Enrollment_Application__r.Mailing_State__c != null &&
                    payment.Enrollment_Application__r.Mailing_Zip_Code__c != null ){
                     
                    paymentWrappers.add(paymentWrapper);   
                }
            }*/
            //else{
                invoiceWrappers.add(invoiceWrapper);
            //}
         //   selected_program_name = payment.Enrollment_Application_Program__c;
            if(selected_program_name != null && mapProgramCustom.get(selected_program_name) != null)
                TANameDisplay = true;
        }
        System.debug(LoggingLevel.ERROR, 'invoiceWrappers: '+invoiceWrappers);
        
        
       
    }

    public void set_EnrlApps_stage() {
        
    }
    @TestVisible private void processEnrollmentApplication(){
        
    } 


    @TestVisible private void rejectPayment(){
        
    }
    
    
    
  /*  public void processPayments(String PayIdstr){
        system.debug('--PayIDstr--'+PayIDstr );
        if(PayIDstr != null){
            List<String> PayIds = PayIDstr.split(',');
            
            List<payment__c> lstLightPay = [select id,Name,Payee_Name__c,Payment_Mailing_Address_1__c,Payment_City__c,Payment_State__c,Payment_Postal_Code__c from payment__c where id in : PayIds limit 1];
            system.debug('--lstLightPay--'+lstLightPay.size());
            if(lstLightPay.size() > 0){
                Payment__c pp = lstLightPay[0];
                string blankFields = '';
                if(pp.Payee_Name__c == null)
                    blankFields += 'Name, ';
                if(pp.Payment_Mailing_Address_1__c == null)
                    blankFields += 'Address, ';
                if(pp.Payment_City__c == null)
                    blankFields += 'City, ';
                if(pp.Payment_State__c == null)
                    blankFields += 'State, ';
                if(pp.Payment_Postal_Code__c == null)
                    blankFields += 'Postal Code, ';
                    
                blankFields = blankFields.subString(0,blankFields.Length()-2);
                    
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please fill Payee '+blankFields+' Details for this Payment :- '+lstLightPay[0].Name));    
            }else{
                pollerBool = true; 
              //  dsmtBatchProcessPayments dBp = new dsmtBatchProcessPayments(PayIds);
                //batchId = database.executebatch(dBp,1);
                //checkBatchStatus();
            }
        }
    }
    */
    
    public void checkBatchStatus() {
        
    }
    
    public boolean pageReload{get;set;}
    
    public void check_print_collateral_status(){
       
    }
    
    public Pagereference CloseInvoiceRecord(){
        
        String InvType = ApexPages.currentPage().getParameters().get('InvType');
        List<Invoice__c> invList = [select id,Status__c from Invoice__c where Invoice_Number__c =: InvType and Status__c != 'Submitted'];
        
        if(invList != null && invList.size() > 0){
            for(Invoice__c inv : invList){
                inv.Status__c = 'Submitted';
            }
            update invList;
        }
        return null;
    }
    
    public Pagereference applyPayments(){
        String payIds = ApexPages.currentPage().getParameters().get('payIds'); // split by ~~~
        String voidcomment = ApexPages.currentPage().getParameters().get('voidcomment');
        
        Set<Id> payId = new Set<Id>();
        for(Id pId : payIds.split('~~~')){
            payId.add(pId);
        }
        
      //  List<Payment__c> payList = [select id,Review__c from Payment__c where id in : payId];
        
        /*List<Invoice_Line_Item__c> listOfInvLines = [select id,Review__c from Invoice_Line_Item__c where id in : payId];
        
        if(!listOfInvLines.isEmpty()){
            Set<Id> setOfReviewIds = new Set<Id>();
            for(Invoice_Line_Item__c objLine : listOfInvLines){
                setOfReviewIds.add(objLine.Review__c);
            }
        }*/
        
        //List<Invoice_Line_Item__c> invLineList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Invoice_Line_Item__c','id in : payId and Voided__c = false','')); 
        List<Invoice_Line_Item__c> invLineList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Invoice_Line_Item__c','id in : payId ','')); 
                
        Invoice_Line_Item__c newinvli = null;
        
        List<Invoice_Line_Item__c> newLineList = new List<Invoice_Line_Item__c> ();
        
        if(invLineList != null && invLineList.size() > 0){
            for(Invoice_Line_Item__c invli : invLineList ){
                invli.Status__c = 'Approved';
            }
        }

        if(invLineList != null && invLineList.size() >0) {
            update invLineList ;
        }
        return new Pagereference('/apex/dsmtInvoiceApprovalQueue');
         
    }
    
      public static map<id, ProcessInstance> processInsMap(){
        map<id, ProcessInstance> pi_map = new map<id, ProcessInstance>();
        list<ProcessInstance> list_pi = [SELECT Id, TargetObjectId, IsDeleted, Status,
                                         (SELECT Id, ProcessInstanceId, ActorId, Actor.Name, StepStatus, Comments
                                          FROM StepsAndWorkItems
                                          WHERE StepStatus = 'Pending' AND IsDeleted = false
                                          ORDER BY CreatedDate ASC LIMIT 1)
                                          FROM ProcessInstance
                                          WHERE IsDeleted = false AND Status = 'Pending'
                                          ORDER BY CreatedDate Asc];
        if(list_pi.size() > 0){
            for(ProcessInstance pi : list_pi){
                pi_map.put(pi.TargetObjectId, pi);
            }
        }
        return pi_map;
    }
        
    public void fetchActiveExceptions(){
       
    }  
    
    public Pagereference voidPayments()
    {
        String payIds = ApexPages.currentPage().getParameters().get('payIds'); // split by ~~~
        String voidcomment = ApexPages.currentPage().getParameters().get('voidcomment');
        
        Set<Id> payId = new Set<Id>();
        for(Id pId : payIds.split('~~~')){
            payId.add(pId);
        }
        
      //  List<Payment__c> payList = [select id,Review__c from Payment__c where id in : payId];
        
        List<Invoice_Line_Item__c> listOfInvLines = [select id,Review__c from Invoice_Line_Item__c where id in : payId];
        
        if(!listOfInvLines.isEmpty()){
            Set<Id> setOfReviewIds = new Set<Id>();
            for(Invoice_Line_Item__c objLine : listOfInvLines){
                setOfReviewIds.add(objLine.Review__c);
            }
        }
        
        List<Invoice_Line_Item__c> invLineList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Invoice_Line_Item__c','Review__c in : setOfReviewIds and Voided__c = false','')); 
                
        Invoice_Line_Item__c newinvli = null;
        
        List<Invoice_Line_Item__c> newLineList = new List<Invoice_Line_Item__c> ();
        
        if(invLineList != null && invLineList.size() > 0){
            for(Invoice_Line_Item__c invli : invLineList ){
                invli.Voided__c = true;
                newinvli = new Invoice_Line_Item__c ();
                newinvli = invli.clone(false,false);
                if(invli.Qty__c != null){
                    newinvli.QTY__c = invli.Qty__c * -1;
                }
                if(invli.Calculated_Incentive_Amount__c != null){
                    newinvli.Calculated_Incentive_Amount__c = invli.Calculated_Incentive_Amount__c* -1;
                }
                newinvli.Voided__c = true;
                newLineList.add(newinvli);
                newLineList.add(invli);
            }
        }

        if(newLineList != null && newLineList.size() >0) {
            upsert newLineList;
        }
        return new Pagereference('/apex/dsmtInvoiceApprovalQueue');
    }  

    public class InvoiceWrapperClass{
        public Boolean is_selected {get;set;}
        public String customerName{get;set;}
        public String accountnumber{get;set;}
        public String billedprovider{get;set;}
        public String primaryheat{get;set;}
        public String meaid{get;set;}
        public String partid{get;set;}
        public String quantity{get;set;}
        public String amount{get;set;}
        public String installdate{get;set;}
        public String energysaving{get;set;}
        public String city{get;set;}
        public String totalamt{get;set;}
        public Invoice_Line_Item__c invoicelineitem{get;set;}
        public string projectName{get;set;}
        public InvoiceWrapperClass() {
            is_selected = false;
        }
    }
}