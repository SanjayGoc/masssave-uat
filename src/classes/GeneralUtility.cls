global without sharing class GeneralUtility {
    
    static String EA_EXCEPTION_TRIGGER = 'Enrollment_Application__C Exception Trigger';
    static String MLI_EXCEPTION_TRIGGER = 'Measure_Line_Item__C Exception Trigger';
    static String DMS_TRACKER_CONFIG_CATEGORY = 'APEX TEMPLATES';
    
    public static string getCreatableFieldsSOQL(String objectName, String whereClause,String additionalFields){
 
        String selects = '';
        
       
        if (whereClause == null || whereClause == ''){ return null; }
      
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
 
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
              //  if (fd.isCreateable()){ // field is creatable                    
                    selectFields.add(fd.getName());
              //  }
            }
        }
        
       if(!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
           if(additionalFields != null)
                selects += additionalFields;
           
                if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
            
        }
         System.debug('--created query -- ' + 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause);
        return 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause;
    } 
    
     public static string getObjectFieldsSOQL(String objectName, String whereClause,String additionalFields, Map<String,String> referenceObjectMap){
 
        String selects = '';
        String SelectReference = '';
       
        if (whereClause == null || whereClause == ''){ return null; }
      
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
 
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                    selectFields.add(fd.getName());
            }
        }
        
        //for Refence Object Fields 
        System.debug('--referenceObjectMap--'+referenceObjectMap);
        if(referenceObjectMap != null){
            
            for(String refObjApiName : referenceObjectMap.keySet()){
               String objApi = referenceObjectMap.get(refObjApiName);
               Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objApi.toLowerCase()).getDescribe().Fields.getMap();
               if(fieldMap != null){
                   for (Schema.SObjectField ft : fieldMap.values()){ // loop through all field tokens (ft)
                        Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                        String fieldname = refObjApiName.replace('__c','__r')+'.'+fd.getName();
                        selectFields.add(fieldname);
                    }
               }
            }
        }
         
       if(!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            
            if(additionalFields != null){
               selects += additionalFields;
            }
            
            if (selects.endsWith(',')){
                selects = selects.substring(0,selects.lastIndexOf(','));
            }
        }
         System.debug('--created query -- ' + 'SELECT ' + selects + ' FROM ' + objectName + ' WHERE ' + whereClause);
         
         return 'SELECT ' + selects +' FROM ' + objectName + ' WHERE ' + whereClause;
    } 
    
   
}