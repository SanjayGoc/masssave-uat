@isTest
public class AIScheduleCreateItemsTest {
    testmethod static  void testmethod1(){
        
        Location__c loc = new Location__c(Name='Loc');
        insert loc;
        
        Schedules__c sc = new Schedules__c(Location__c=loc.Id,Name='test',Start_Time__c='08:00 AM',End_Time__c='05:00 AM');
        insert sc;
        
        Territory__c t = new Territory__c(Location__c=loc.id,Territory_Name__c='FSSSSSSs');
        insert t;
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c(Location__c=loc.id,Schedules__c=sc.Id,Schedule_Week__c='WEEK B',End_Time__c='05:00 AM',Start_Time__c='08:00 AM');
        insert sli;
    }
        
}