@isTest
public class dsmtAirFlowInfoValidationHelperTest{
    public testmethod static void test1(){
        
         Building_Specification__c bs = new Building_Specification__c();
         bs.Bedromm__c=2;
         insert bs;
         Air_Flow_and_Air_Leakage__c af = new Air_Flow_and_Air_Leakage__c();
         af.Building_Specifications__c = bs.Id;
         af.CFM50__c = 0;
         insert af;
         Blower_Door_Reading__c bdr = new Blower_Door_Reading__c();
         bdr.Air_Flow_and_Air_Leakage__c = af.Id;
         bdr.Building_Specification__c = bs.Id;
         Insert bdr;
         dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
         dsmtAirFlowInfoValidationHelper cont = new dsmtAirFlowInfoValidationHelper(); 
         cont.validate('test','test',bs,testWrap);
         
         af.CFM50__c = 2;
         update af;
         dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap2=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
         dsmtAirFlowInfoValidationHelper cont2 = new dsmtAirFlowInfoValidationHelper(); 
         cont2.validate('test','test',bs,testWrap2);
         
         af.CFM50__c = 1550;
         update af;
         dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap1=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
         dsmtAirFlowInfoValidationHelper cont1 = new dsmtAirFlowInfoValidationHelper(); 
         cont1.validate('test','test',bs,testWrap1);         
     }
}