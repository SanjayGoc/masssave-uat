public class dsmtCreateEmployeeTerritoryAssmtCntrl {
    
   
    String empIdNew{get;set;}
    
    public dsmtCreateEmployeeTerritoryAssmtCntrl(){
        empIdNew = ApexPages.currentPage().getParameters().get('Id');
        
    }
    
     public PageReference autoRun() {
        
        String EmpIdNew = ApexPages.currentPage().getParameters().get('Id');
            
                        
        List<Employee__c> empList = [select id,Name,Schedule__c,Territory__c from Employee__c where Status__c = 'Active'
                                                and Id =: EmpIdNew];
        
        Set<Id> empId = new set<Id>();
        
        for(Employee__c emp : empList){
            empId.add(emp.Id);
        }
        
        List<Employee_Territory_Assignment__c> extetaList = [select id,employee__c from Employee_Territory_Assignment__c where employee__c in : empId];
        
        Set<Id> etaExistId = new set<Id>();
        
        for(Employee_Territory_Assignment__c eta : extetaList ){
            etaExistId.add(eta.employee__c);
        }
        
        Set<Id> ScheduleId = new Set<Id>();
        Set<Id> territoryId = new Set<Id>();
        
        for(Employee__c emp : empList){
            if(etaExistId.add(emp.Id)){
                if(emp.Schedule__c != null){
                    ScheduleId.add(emp.Schedule__c);
                }
                if(emp.Territory__c != null){
                    territoryId.add(emp.Territory__c);
                }
            }
            
        }
        
        if(ScheduleId.size() > 0){
            List<Schedule_Line_Item__c> sclList = [select id,Schedule_Week__c,Schedules__c from Schedule_Line_Item__c where Schedules__c in : ScheduleId];
            
            List<Schedule_Line_Item__c> tempScl = new List<Schedule_Line_Item__c>();
            Map<Id,List<Schedule_Line_Item__c>> ScheduleMap = new Map<Id,List<Schedule_Line_Item__c>>();
            
            for(Schedule_Line_Item__c scl : sclList){
                
                if(ScheduleMap.get(scl.Schedules__c) != null){
                    tempScl = ScheduleMap.get(scl.Schedules__c);
                    tempScl.add(scl);
                    ScheduleMap.put(scl.Schedules__c,tempScl);
                }else{
                    tempScl = new List<Schedule_Line_Item__c>();
                    tempScl.add(scl);
                    ScheduleMap.put(scl.Schedules__c,tempScl);
                }
            }
            
            Map<Id,Territory__c> terMap = new Map<Id,Territory__c>();
            
            if(territoryId.size() > 0){
                List<Territory__c> terList = [select id,Territory_Name__c from Territory__c where id in : territoryId];
                
                for(Territory__c ter : terList){
                    terMap.put(ter.Id,ter);
                }
            }
            
            List<Employee_Territory_Assignment__c> etaList = new List<Employee_Territory_Assignment__c>();
            Employee_Territory_Assignment__c eta = null;
            
            String str = '';
            
            for(Employee__c emp : empList){
                
                if(emp.Schedule__c != null && ScheduleMap.get(emp.Schedule__c) != null){
                    
                    for(Schedule_Line_Item__c scl : ScheduleMap.get(emp.Schedule__c)){
                        
                        eta = new Employee_Territory_Assignment__c();
                        eta.Schedule_Line_Item__c = scl.Id;
                        eta.Territory__c = emp.Territory__c;
                        eta.Schedules__c = emp.Schedule__c;
                        eta.Employee__c = emp.Id;
                        eta.Assignment_Type__c = 'Recurring';
                        eta.Assignment_Start_Date__c = Date.Today();
                        
                        if(terMap.get(emp.Territory__c) != null){
                            str = terMap.get(emp.Territory__c).Territory_Name__c;
                        }
                        str += scl.Schedule_Week__c;
                        str += emp.Name;
                        
                        if(str.length() > 80){
                            str = str.Substring(0,80);
                        }
                        eta.Name = str;
                        etaList.add(eta);
                    }
                }
            }
            
            system.debug('--etaList--'+etaList.size());
            if(etaList.size() > 0){
                insert etaList;
            }
        }
        
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/' + empIdNew);
        pageRef.setRedirect(true);
        return pageRef;
        
    }
    
}