@isTest public class WorkTeamMakerTest {

    @isTest static void itShouldPickCorrectAlternateWeekOutOfSingle(){
        Set<String> weekDesignations = new Set<String>{'WEEK A'};
        Test.startTest();
        String actual = WorkTeamMaker.getWeekDesignation(Date.newInstance(2018, 3, 16), weekDesignations);
        Test.stopTest();
        System.assertEquals('WEEK A', actual,'');
    }

    @isTest static void itShouldPickCorrectAlternateWeekOutOfTwo(){
        Set<String> weekDesignations = new Set<String>{'WEEK A', 'WEEK B'};
        Test.startTest();
        String actual1 = WorkTeamMaker.getWeekDesignation(Date.newInstance(2018, 1, 1), weekDesignations);
        String actual2 = WorkTeamMaker.getWeekDesignation(Date.newInstance(2018, 1, 7), weekDesignations);
        String actual3 = WorkTeamMaker.getWeekDesignation(Date.newInstance(2018, 3, 16), weekDesignations);
        String actual4 = WorkTeamMaker.getWeekDesignation(Date.newInstance(2018, 8, 4), weekDesignations);
        String actual5 = WorkTeamMaker.getWeekDesignation(Date.newInstance(2019, 2, 8), weekDesignations);
        String actual6 = WorkTeamMaker.getWeekDesignation(Date.newInstance(2025, 1, 1), weekDesignations);
        Test.stopTest();
        System.assertEquals('WEEK A', actual1, '');
        System.assertEquals('WEEK B', actual2, '');
        System.assertEquals('WEEK A', actual3, '');
        System.assertEquals('WEEK A', actual4, '');
        System.assertEquals('WEEK B', actual5, '');
        System.assertEquals('WEEK B', actual6, '');
    }

    @isTest static void itShouldPickCorrectAlternateWeekOutOfFour(){
        Set<String> weekDesignations = new Set<String>{'WEEK A', 'WEEK B', 'WEEK C', 'WEEK D'};
        Test.startTest();
        String actual = WorkTeamMaker.getWeekDesignation(Date.newInstance(2018, 3, 16), weekDesignations);
        Test.stopTest();
        System.assertEquals('WEEK C', actual,'');
    }

    @isTest static void itShouldPickCorrectScheduleLineItem(){
        WorkTeamMaker maker = new WorkTeamMaker(Date.newInstance(2018, 3, 13), 0);
        Location__c location = new Location__c();
        insert location;
        Schedules__c schedule = new Schedules__c();
        insert schedule;
        List<Schedule_Line_Item__c> scheduleLineItems = new List<Schedule_Line_Item__c>{
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule.Id, WeekDay__c = 'Monday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule.Id, WeekDay__c = 'Tuesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule.Id, WeekDay__c = 'Wednesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule.Id, WeekDay__c = 'Monday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule.Id, WeekDay__c = 'Tuesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
            new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule.Id, WeekDay__c = 'Wednesday', Start_Time__c='08:00 AM', End_time__c='06:15 PM')
        };
        insert scheduleLineItems;
        Employee__c employee = new Employee__c(
                Schedule__c = schedule.Id,
                Name = 'Test Employee',
                Location__c = location.Id,
                Employee_Id__c = '09875',
                Status__c = 'Active');
        insert employee;
        delete [SELECT Id FROM Work_Team__c];
        Test.startTest();
        maker.createWorkTeamsForEmployees(new List<Employee__c>{employee});
        Test.stopTest();
        Schedule_Line_Item__c selectedScheduleLineItem = [SELECT Schedule_Week__c, WeekDay__c 
                FROM Schedule_Line_Item__c 
                WHERE Id IN (SELECT Schedule_Line_Item__c FROM Work_Team__c) LIMIT 1];
        System.assertEquals('WEEK A', selectedScheduleLineItem.Schedule_Week__c, '');
        System.assertEquals('Tuesday', selectedScheduleLineItem.WeekDay__c, '');
    }

    private static Employee__c createEmployee( Id scheduleId, String employeeId, Id locationId){
        return new Employee__c(
                Schedule__c = scheduleId,
                Name = 'Test Employee ' + employeeId,
                Employee_Id__c = employeeId,
                Location__c = locationId,
                Status__c = 'Active'
            );
    }

    @isTest static void itShouldHandle2EmployeeRecords(){
        Location__c location = new Location__c();
        insert location;
        Schedules__c schedule1 = new Schedules__c();
        Schedules__c schedule2 = new Schedules__c();
        insert new List<Schedules__c>{schedule1, schedule2};
        List<String> weekDayNames = new List<String>{'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'};
        List<Schedule_Line_Item__c> scheduleLineItems = new List<Schedule_Line_Item__c>();
        for (String weekDayname : weekDayNames){
            scheduleLineItems.addAll(new List<Schedule_Line_Item__c>{
                new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule1.Id, WeekDay__c = weekDayName, Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
                new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule1.Id, WeekDay__c = weekDayName, Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
                new Schedule_Line_Item__c(Schedule_Week__c='WEEK A', Schedules__c = schedule2.Id, WeekDay__c = weekDayName, Start_Time__c='08:00 AM', End_time__c='06:15 PM'),
                new Schedule_Line_Item__c(Schedule_Week__c='WEEK B', Schedules__c = schedule2.Id, WeekDay__c = weekDayName, Start_Time__c='08:00 AM', End_time__c='06:15 PM')
           });
        }
        insert scheduleLineItems;
        Employee__c employee0 = createEmployee(schedule1.Id, '0000', location.Id);
        insert employee0;
        Employee__c employee1 = createEmployee(schedule1.Id, '0001', location.Id);
        insert employee1;
        List<Employee__c> employees = new List<Employee__c>{ employee0, employee1 };
        delete [SELECT Id FROM Work_Team__c];
        WorkTeamMaker maker = new WorkTeamMaker(Date.newInstance(2018, 3, 13), 180);
        Test.startTest();
        maker.createWorkTeamsForEmployees(employees);
        Test.stopTest();
        Integer numberOfWorkTeamsCreated = [SELECT count() FROM Work_Team__c];
        System.assertEquals(362, numberOfWorkTeamsCreated,'');
    }
}