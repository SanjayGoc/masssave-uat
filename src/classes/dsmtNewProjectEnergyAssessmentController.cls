public with sharing class dsmtNewProjectEnergyAssessmentController {

    public string eaId{get;set;}
    public static final Double E;
    
    public List<SelectOption> projectSOList {get;set;}
    public List<Project_and_Energy_Assessment__c> peaList {get;set;}

    public dsmtNewProjectEnergyAssessmentController(){
        
    }
    
    public dsmtNewProjectEnergyAssessmentController(ApexPages.StandardController controller) {
        fetchProject();
    }
    
    public void fetchProject(){
        eaId = ApexPages.currentPage().getParameters().get('Id');
        
        peaList = new List<Project_and_Energy_Assessment__c>(); 
        
        projectSOList = new List<SelectOption>();
        
        map<String, List<Project_and_Energy_Assessment__c>> prjEAMap = new map<String, List<Project_and_Energy_Assessment__c>>();
        
        List<Recommendation_Scenario__c> projectList = [select id,name,Type__c,Status__c
                                                        from Recommendation_Scenario__c 
                                                        where Energy_Assessment__c =: eaId];
        
        for(Project_and_Energy_Assessment__c projEA : [select id, Project__c, Special_Incentive__c, Energy_Assessment__c
                                                         from Project_and_Energy_Assessment__c
                                                         where Energy_Assessment__c =: eaId
                                                         and Energy_Assessment__c != null
                                                         and Project__c != null])
        {
            peaList.add(projEA);
        }
        
        projectSOList.add(new SelectOption('', '--Select Project--'));
        
        for(Recommendation_Scenario__c r : projectList)
        {
            projectSOList.add(new SelectOption(r.Id, r.Name));
        }
    }
    
    public void addNewRecord(){
        peaList.add(new Project_and_Energy_Assessment__c(Energy_Assessment__c = eaId));
    }
    
    public PageReference saveRecords()
    {
        upsert peaList;
        
        return new PageReference('/'+eaId).setRedirect(true);
    }
    
    //Decimal x = 10;
    //Decimal y = 15; // mo.Elevation
    
    //Decimal val1 = (decimal)Math.Exp( (-3.71781196*(x.pow(-5))) * (double)(y) );
        
    // Decimal val = (decimal)Math.Exp( (-3.71781196*(x.pow(-5))) * (decimal)(1.5) );
    
    //Decimal val = Math.exp((decimal).0000371781196 * 1.5); 
}