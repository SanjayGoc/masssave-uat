@isTest
Public class dsmtMoreInfoExtnTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        acc.Billing_Account_Number__c= 'Gas-~~~1234445';
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
         /*Customer__c cust = Datagenerator.createCustomer(acc.id,null);
         insert cust;*/
           DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
       
        //  dsmt.Contact__c= cont.id;
        //update dsmt;
        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.HIC_Attached__c=true;
        update reg;
        
         Messages__c msg= Datagenerator.createMessage();
         Attachment__c att= Datagenerator.createAttachment(Tacc.ID);
         att.Message__c= msg.id;
         update att;
         Exception_Template__c emailTemp= new Exception_Template__c(Object_Sub_Type__c='HPC', Reference_ID__c='ETN-0000003', Automatic__c = true, Active__c = true,
                                            Registration_Request_Level_Message__c= true);
        insert emailTemp;
        Exception__c ex= new Exception__c(Exception_Message__c='test',Registration_Request__c=reg.ID,Exception_Template__c=emailTemp.id, Automated__c=true,Hidden__c=false);
        insert ex;
        Email_Custom_Setting__c emailset= Datagenerator.createEmailCustSetting();
        emailset.RR_Submitted_Owner_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Submitted_TA_ThankYou_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_TA_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_DSMTContact_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        update emailset;
         ApexPages.currentPage().getParameters().put('id',reg.id);
         Apexpages.currentpage().getparameters().put('isMIR2','true');
        dsmtMoreInfoExtn controller = new dsmtMoreInfoExtn();
        controller.expId=ex.id;
       // dsmtMoreInfoExtn.MessageModal wrap= new dsmtMoreInfoExtn.MessageModal();
        controller.saveExp();
      //  controller.fetchRegistrationRequest(reg.id);
        
        controller.saveOutboundConcate();
        controller.getTemplateList();
        
        
       
        
    }
         static testMethod void case2(){
        Account  acc= Datagenerator.createAccount();
        acc.Billing_Account_Number__c= 'Gas-~~~1234445';
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
         /*Customer__c cust = Datagenerator.createCustomer(acc.id,null);
         insert cust;*/
           DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
       
        //  dsmt.Contact__c= cont.id;
        //update dsmt;
        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.HIC_Attached__c=true;
        update reg;
        
         Messages__c msg= Datagenerator.createMessage();
         Attachment__c att= Datagenerator.createAttachment(Tacc.ID);
         att.Message__c= msg.id;
         update att;
         Exception_Template__c emailTemp= new Exception_Template__c(Object_Sub_Type__c='HPC', Reference_ID__c='ETN-0000003', Automatic__c = true, Active__c = true,
                                            Registration_Request_Level_Message__c= true);
        insert emailTemp;
        Exception__c ex= new Exception__c(Exception_Message__c='test',Registration_Request__c=reg.ID,Exception_Template__c=emailTemp.id, Automated__c=true,Hidden__c=false);
        insert ex;
        Email_Custom_Setting__c emailset= Datagenerator.createEmailCustSetting();
        emailset.RR_Submitted_Owner_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Submitted_TA_ThankYou_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_TA_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_DSMTContact_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        update emailset;
         ApexPages.currentPage().getParameters().put('id',reg.id);
         //Apexpages.currentpage().getparameters().put('isMIR2','true');
        dsmtMoreInfoExtn controller = new dsmtMoreInfoExtn();
        controller.expId=ex.id;
        controller.type='Override';
       // dsmtMoreInfoExtn.MessageModal wrap= new dsmtMoreInfoExtn.MessageModal();
        controller.saveExp();
      //  controller.fetchRegistrationRequest(reg.id);
        controller.saveOutboundConcate();
        controller.getTemplateList();
             
        ApexPages.currentPage().getParameters().put('id',reg.id);
        Apexpages.currentpage().getparameters().put('isMIR2','true');
        dsmtMoreInfoExtn controller1 = new dsmtMoreInfoExtn();
        controller1.saveOutboundConcate();
        
        
       
        
    }
 }