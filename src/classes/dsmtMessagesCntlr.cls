public class dsmtMessagesCntlr{
    public String conName {get;set;}
    public String conId   {get;set;}
    
    public String result1;
    public User u{get;set;}
    public String accid;
    public Boolean isTac;
    public String message_Filter {set;get;}
    public List<MessagesModel> listMessage{set;get;}
    public Boolean isMessageInfoOpen{set;get;}
    public String selectedMessageId{set;get;}
    public List<Attachment__c> listAtt{set;get;}
    public Messages__c openMsg {set;get;}
    public String paymentResult1;
    public boolean displayPopUp{get;set;}
    string conTanentId = '';
    // -------------- PAGINATION -----------//
    // Json Result For Ajax Responder 
    public JSONObject jsonx {get;set;}
    public Integer TotalRecordsCount {get;set;}
    public String selectedRowIds{get;set;}
    public JSONObject jsonxPayment {get;set;}
    public Integer TotalRecordsCountPayment {get;set;}
    public Boolean showColorBox{get;set;}
    public string messageJSON{get;set;}
    public string msgType{get;set;}
    public Integer UnreadMessage{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}

      
    public dsmtMessagesCntlr(){
        isPortal = true;
        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
        List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
            if(lstUser.size()>0){
                if(lstUser[0].IsPortalEnabled){
                    headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                }
                else{
                    headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                    isPortal = false;
                }
            }
        init();
        fetchMessages();
        //initPaginationSorting();
        
    
    }
    
    public void init(){
        selectedRowIds = '';
        openMsg = new Messages__c();
        //openMessages = new Messages__c();
        displayPopUp = false;
        listAtt = new List<Attachment__c>();
        conName = conId = '';
        message_Filter = 'Inbox';
        selectedMessageId = '';
        //cloneEAId = '';
        isMessageInfoOpen = false;
        //eaIds = new Set<Id>();
        listMessage = new List<MessagesModel>();
        msgType = ApexPages.currentPage().getParameters().get('msgType');
    }
    
     public void fetchMessages(){
        String userId = userinfo.getUserId();                 
        String queryString = 'Select Id,Message_Date__c,Sent__c,From__c,CC__c,BCC__c,To__c,Offer_Letter_Message_Title__c,Name,Subject__c,Deleted__c,Message__c,Accepted_Rejected__c,Message_Type__c,Opened__c,Archive__c,Message_Direction__c,CreatedById from Messages__c where OwnerId!=null';
        
       
        UnreadMessage = 0;
        List<Messages__c> messageLst = [select id,name from Messages__c where Opened__c = null];
        if(messageLst != null && messageLst.size() > 0){
            UnreadMessage  = messageLst.size();
        }
        system.debug('--message_Filter --'+message_Filter );
        if(message_Filter != null && message_Filter.trim().length() > 0){
            if(message_Filter == 'Inbox'){
                queryString += ' And Archive__c = false  Order By createddate desc';
            }else if(message_Filter == 'Sent'){
                queryString += ' And Archive__c = false And Message_Direction__c = \'Outbound\' Order By createddate desc';
            }else if(message_Filter == 'Archive'){
                queryString += ' And  Archive__c = true And Message_Direction__c = \'Outbound\' Order By createddate desc';
            }else if(message_Filter == 'Offers'){
                queryString += ' And Archive__c = false And Message_Direction__c = \'Inbound\' And Message_Type__c = \'Offer Letter\' Order By createddate desc';
            }
        }
        queryString += ' LIMIT 50';
        system.debug('--queryString---'+queryString);
        listMessage = new List<MessagesModel>();
        for(Messages__c msg : Database.query(queryString)){
           
            MessagesModel mod = new MessagesModel();
            mod.msg = msg;
            mod.msgno = msg.Name;
            if(mod.msg.Opened__c == null){
                mod.isOpenedBlank = true;
            }
            if(/*msg.Message_Direction__c == 'Inbound' &&*/ msg.To__c == (userinfo.getUserEmail()))
                listMessage.add(mod);
            else if(/*msg.Message_Direction__c == 'Outbound' && */msg.CreatedById == userinfo.getUserId())
                listMessage.add(mod);
        }
        system.debug('--listMessage---'+listMessage);
    }
    
    public void initMessages(){
        
        messageJSON = '';
        UnreadMessage = 0;
        String userId = userinfo.getUserId();
        String queryString = 'Select Id,From__c,To__c,From_Formula__c,CreatedDate, Sent__c,Offer_Letter_Message_Title__c,Name,Subject__c,Deleted__c,Message__c,'+
                             'Accepted_Rejected__c,Message_Type__c,Opened__c,Archive__c,Message_Direction__c,OwnerId,Owner.Email,CreatedById'+
                             ' from Messages__c where OwnerId!=null ';
        if(msgType == 'new'){
            queryString += ' AND  Message_Direction__c = \'Inbound\' AND Status__c != \'Opened\' AND Opened__c = null';
        }
        
        
        /*List<Messages__c> messageLst = [select id,name from Messages__c 
                                        where CreatedById =: userId
                                        and Opened__c = null];
                                        
        if(messageLst != null && messageLst.size() > 0){
            UnreadMessage  = messageLst.size();
        }*/
        
        string message_Filter = ApexPages.currentPage().getParameters().get('message_Filter');
        
            system.debug('--message_Filter ---'+message_Filter);
        
            if(message_Filter != null && message_Filter.trim().length() > 0){
               // String UserId = userinfo.getUserId();
                if(message_Filter == 'Inbox'){
                    
                   queryString += ' Order By createddate desc';
                    
                }else if(message_Filter == 'Sent'){
                    queryString += ' And Archive__c = false And Message_Direction__c = \'Outbound\' and Status__c != \'Draft\' Order By createddate desc';
                }else if(message_Filter == 'Archive'){
                    queryString += ' And  Archive__c = true And Message_Direction__c = \'Outbound\' Order By createddate desc';
                }else if(message_Filter == 'Offers'){
                    queryString += ' And Archive__c = false And Message_Direction__c = \'Inbound\' And Message_Type__c = \'Offer Letter\' Order By createddate desc';
                }
            }
            
            queryString += ' LIMIT 50000';
            
            list<Messages__c> listMsg = Database.query(queryString);
            set<string> fromEmails = new set<String>();
            map<string, string> emailMap = new map<string, string>();
            for(Messages__c msg : listMsg){
                fromEmails.add(msg.From__c);
            }
            
            for(User users : [select id, Name, Email from User where Email in : fromEmails and Profile.UserLicense.Name != 'Guest User License']){
                emailMap.put(users.Email, users.Name);
            }
            
            list<MessageModal> listMessage = new List<MessageModal>();
            for(Messages__c msg : Database.query(queryString)){
                 
                MessageModal mod = new MessageModal();
                mod.Id = msg.Id;
                if(msg.Sent__c != null)
                    mod.Sent = msg.Sent__c.format('MMM d,  yyyy hh:mm a');
                else{
                    mod.Sent = msg.CreatedDate.format('MMM d,  yyyy hh:mm a');
                }
                mod.OfferLetterMessageTitle = msg.Offer_Letter_Message_Title__c;
                mod.Name = msg.Name;
                if(msg.Opened__c == null){
                    mod.Subject = msg.Subject__c +'<br/><span style="color:#444;">'+(msg.Message__c != null ? msg.Message__c.replaceAll('\\<.*?\\>', '') : '')+'</span>';
                }else{
                    mod.Subject = msg.Subject__c;
                }
                if(mod.Subject.length() > 250)
                    mod.Subject = mod.Subject.substring(0, 250);
                
                mod.Deleted = msg.Deleted__c;
                mod.Message = msg.Message__c;
                mod.AcceptedRejected = msg.Accepted_Rejected__c;
                mod.MessageType = msg.Message_Type__c;
                mod.Opened = msg.Opened__c;
                mod.Archive = msg.Archive__c;
                mod.MessageDirection = msg.Message_Direction__c;
                mod.OwnerId = msg.OwnerId;
                mod.OwnerEmail = msg.Owner.Email;
                
                if(emailMap.containsKey(msg.From__c))
                    mod.fromAdd = emailMap.get(msg.From__c)+'('+msg.From__c+')';
                else
                    mod.fromAdd = msg.From__c;
                
                mod.IsDraft = false;
                if(msg.Opened__c == null)
                    mod.isOpenedBlank = true;
                else
                    mod.isOpenedBlank = false;
                
                if(mod.Subject.length() > 250)
                    mod.Subject = mod.Subject.substring(0, 250);
                
                mod.toAdd = msg.To__c; 
                
                system.debug('--msg.To__c---'+msg.To__c);
                if(message_Filter == 'Inbox'){
                    if( msg.To__c == userinfo.getUserEmail()){
                        listMessage.add(mod);
                    }
                }else{
                    if(msg.To__c != userinfo.getUserEmail() && msg.Message_Direction__c == 'Outbound' && msg.CreatedById == userinfo.getUserId())
                        listMessage.add(mod); 
                    }  
                //listMessage.add(mod);
            }
            system.debug('--listMessage---'+listMessage);
            messageJSON = JSON.serialize(listMessage);
        
        system.debug('--listMessage---'+listMessage.size());
    }
    
    public void openMessage(){
        openMsg = new Messages__c();
        listAtt = new List<Attachment__c>();
        isMessageInfoOpen = true;
        //system.debug('###### selectedMessageId = '+selectedMessageId);
        if(selectedMessageId != null && selectedMessageId.trim().length() >= 15){
            for(MessagesModel mod : listMessage){
                if(mod.msg.id == selectedMessageId){
                    mod.isOpenedBlank = false;
                    openMsg = mod.msg;
                    break;
                }
            }
        }
        if(openMsg != null && openMsg.id != null){
            openMsg.Opened__c = system.now();
            update openMsg;
            
            listAtt = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate from Attachment__c where Message__c = :openMsg.Id];
        }
        
        selectedMessageId = '';
    }
    
    public void backToMessage(){
        isMessageInfoOpen = false;
        openMsg = new Messages__c();
        listAtt = new List<Attachment__c>();
    }
    
    public class MessageModal{
        public string Id{get;set;}
        public string Sent{get;set;}
        public string OfferLetterMessageTitle{get;set;}
        public string Name{get;set;}
        public string Subject{get;set;}
        public datetime Deleted{get;set;}
        public string Message{get;set;}
        public string AcceptedRejected{get;set;}
        public string MessageType{get;set;}
        public datetime Opened{get;set;}
        public boolean Archive{get;set;}
        public string MessageDirection{get;set;}
        public string EnrollmentApplicationId{get;set;}
        public string OwnerId{get;set;}
        public string OwnerEmail{get;set;}
        public boolean isOpenedBlank{get;set;}
        public String fromAdd{get;set;}
        public String toAdd{get;set;}
        public boolean IsDraft{get;set;}
    }
    
    public class MessagesModel{
        public Messages__c msg{set;get;}
        public Boolean isOpenedBlank{set;get;}
        public Boolean IsDraft{get;set;}
        public String msgno{get;set;}
        public MessagesModel(){
            msg = new Messages__c();
            isOpenedBlank = false;
            
        }
    }
    
}