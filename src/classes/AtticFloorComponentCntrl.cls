public class AtticFloorComponentCntrl {
    public Id atticFloorId{get;set;}
    public String newLayerType{get;set;}
    public Floor__c atticFloor{get{
        if(atticFloor == null){
            List<Floor__c> floors = Database.query('SELECT ' + getSObjectFields('Floor__c') + ' Id FROM Floor__c WHERE Id=:atticFloorId'); 
            if(floors.size() > 0){
                atticFloor = floors.get(0);
            }
        }
        return atticFloor;
    }set;}
    public void saveFloor()
    {
        saveFloorLayers();
        if(atticFloor != null && atticFloor.Id != null)
        {
            UPDATE atticFloor;
            atticFloor = null;
        }
    }
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    public List<Layer__c> floorLayers{get;set;}
    public Map<Id,Layer__c> getFloorLayersMap(){
        Id floorId = atticFloor.Id;
        System.debug('floorId :: ' + floorId);
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ' Id ,RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Floor__c =:floorId And HideLayer__c = false ';
        System.debug('@@QUERY' + query);
        floorLayers = Database.query(query);
        return new Map<Id,Layer__c>(floorLayers);
    }
    public void saveFloorLayers(){
        if(floorLayers != null && floorLayers.size() > 0){
            UPSERT floorLayers;
        }
    }
    public void addNewFloorLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Floor__c =:atticFloor.Id AND RecordTypeId =:recordTypeId];
        if(layerType != null && m.containsKey(layerType)){
            Decimal nextNumber=layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Floor__c = atticFloor.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            if(newLayer.Id != null){
                atticFloor.Next_Attic_Floor_Layer_Number__c = nextNumber + 1;
                UPDATE atticFloor;
            }
            floorLayers.add(newLayer);
        }
    }
    public void deleteFloorLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = floorLayers.get(index).Id);
        floorLayers.remove(index);
        saveFloor();
    }
    
    private static String getSObjectFields(String sObjectApiName)
    {
       return dsmtHelperClass.sObjectFields(sObjectApiName);
    }
}