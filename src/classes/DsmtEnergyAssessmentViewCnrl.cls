Public Class DsmtEnergyAssessmentViewCnrl{
    transient Public String EnergyAssessmentJSON{get;set;}
    Public String EnAsId{get;set;}
    Public Trade_Ally_Account__c  ta{get;set;}
    public Dsmtracker_contact__c dsmtCon{get;set;}
    Set<Id> TAId = new Set<Id>();
    public String url{get;set;}    
    
    public boolean showNewEnergyAssessment{get;set;}
    public Appointment__c filterApp {get;set;}

    Public DsmtEnergyAssessmentViewCnrl(){
        filterApp = new Appointment__c();
        filterApp.Appointment_Start_Time__c = System.now().addMonths(-3);

        showNewEnergyAssessment = true;
        List<User> userList =  [select id,Profile.Name from User where id =: userinfo.getUserId()];
        if(userList != null && userList.get(0).Profile.Name.contains('HPC')){
            showNewEnergyAssessment  = false;
        }
        initEnergyAssessment();
    }
    Public Void initEnergyAssessment(){
        
        url = Energy_Assessment__c.SObjectType.getDescribe().getKeyPrefix()+'/e';
        system.debug('--URL--'+url);
        EnergyAssessmentJSON='';
        String userId = userinfo.getUserId();
        String uid = null;
        List<User> usrList  = [select Id,Name from user Where Id =: userId];
        if(usrList.size()>0){
            uid = usrList.get(0).id;
        }
        List <DSMTracker_Contact__c> dsmtList = [Select Id,Portal_Role__c, Trade_Ally_Account__c, Trade_Ally_Account__r.name from DSMTracker_Contact__c where Portal_User__c = : uId and Trade_Ally_Account__c != null];
        if(dsmtList != null && dsmtList.size() > 0){
            dsmtCon = dsmtList.get(0);
            List<Trade_Ally_Account__c> taList = [Select id,Name,(select id from Energy_Assessment__r)from Trade_Ally_Account__c where Id =: dsmtCon.Trade_Ally_Account__c];
            
            New_Energy_Assesment__c setting = New_Energy_Assesment__c.getOrgDefaults();
            IF(dsmtCon.Trade_Ally_Account__c != null && dsmtCon.Trade_Ally_Account__r.name != null){
                url += '?'+setting.Trade_Ally_Account__c+'='+ dsmtCon.Trade_Ally_Account__r.name +'&'+setting.Trade_Ally_Account__c+'_lkid=' + dsmtCon.Trade_Ally_Account__c;
            }
            Date dt = Date.Today().addMonths(-3);

            if(taList != null && taList.size() > 0){
                ta = taList.get(0);
                TAId.add(ta.id);
                
                String queryString = 'Select id,Customer__r.Name,Name,Address__c,City__c,State__c,Zip_Code__c,Status__c,Assessment_Type__c,Appointment__c,Appointment__r.Name from Energy_Assessment__c ';
                
              //  queryString += ' and Appointment_Date__c >=: dt '; 
                
                String filterquery =  ' where Trade_Ally_Account__c in: TAId ';
                
                string firstNameF = ApexPages.currentPage().getParameters().get('firstNameF');
                string lastNameF = ApexPages.currentPage().getParameters().get('lastNameF');
                string addressF = ApexPages.currentPage().getParameters().get('addressF');
                string sdF = ApexPages.currentPage().getParameters().get('sdF');
                string edF = ApexPages.currentPage().getParameters().get('edF');
                
                //FilterQuery = '';
                if(firstNameF != null && firstNameF.trim().length() > 0){
                    firstNameF = '%'+firstNameF+'%';
                    filterquery += ' and Customer__r.First_Name__c like : firstNameF '; 
                }
                
                if(lastNameF != null && lastNameF.trim().length() > 0){
                    lastNameF = '%'+lastNameF+'%';
                    filterquery += ' and Customer__r.Last_Name__c like : lastNameF '; 
                }
                
                if(addressF != null && addressF.trim().length() > 0){
                    addressF = '%'+addressF+'%';
                    filterquery += ' and Customer__r.Service_Address__c like : addressF '; 
                }
                
                datetime newdt, edt;
                if(sdF != null && sdF.trim().length() > 0){
                    newdt = datetime.parse(sdF);
                    filterquery += ' and Appointment__r.Appointment_Start_Time__c >=: newdt '; 
                }
                
                if(edF != null && edF.trim().length() > 0){
                    edt = datetime.parse(edF);
                    filterquery += ' and Appointment__r.Appointment_End_Time__c <=: edt '; 
                }
                
                if(filterquery.endswith('And')){
                    filterquery = filterquery.substring(0,filterquery.length() - 3);
                }
                
                   if(filterquery.endswith('And')){
                    filterquery = filterquery.substring(0,filterquery.length() - 3);
                }
                
                List<EnergyAssmentModel> ListEnAss = new List<EnergyAssmentModel>();
                
                queryString += filterquery ;
                queryString += ' order by Appointment__r.Appointment_Start_Time__c desc limit 18000';
                
                system.debug('--queryString---'+queryString);
                List<Energy_Assessment__c> listEA = Database.query(queryString);
                
                for(Energy_Assessment__c EnAsses : listEA){
                    EnergyAssmentModel mod = new EnergyAssmentModel();
                    mod.Id = EnAsses.id;
                    mod.Name = EnAsses.Name;
                    mod.Address = EnAsses.Address__c;
                    mod.City = EnAsses.City__c;
                    mod.state = EnAsses.State__c;
                    mod.ZipCode = EnAsses.Zip_Code__c;
                    mod.Status = EnAsses.Status__c;
                    mod.Appointmnt = EnAsses.Appointment__r.Name;
                    mod.AssessmentType = EnAsses.Assessment_Type__c;
                    mod.CustomerName = EnAsses.Customer__r.Name;
                    ListEnAss.add(mod);
                    
                    //EnAsId = ListEnAss.get(0).id;
                    
                }
                if(ListEnAss.size()>0){
                    EnergyAssessmentJSON = JSON.serialize(ListEnAss);   
                }
                system.debug('--EnergyAssessmentJSON---'+EnergyAssessmentJSON);
            }
        }
    }
    Public class EnergyAssmentModel{
        Public String Id{get;set;}
        Public String Name{get;set;}
        Public String Address{get;set;}
        Public String City{get;set;}
        Public String State{get;set;}
        Public String ZipCode{get;set;}
        Public String Status{get;set;}
        Public String Appointmnt{get;set;}
        Public String AssessmentType{get;set;}
        Public String CustomerName{get;set;}
    }
}