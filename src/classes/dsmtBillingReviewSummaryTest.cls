@istest
public class dsmtBillingReviewSummaryTest
{
    @istest
    static void test()
    {
        User u = Datagenerator.CreatePortalUser();
        system.runAs(u)
        {  

            
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Email__c='test@gmail.com';
            ta.Name = 'test';
            ta.First_Name__c='test';
            ta.Last_Name__c='test';
            ta.Phone__c='1111';
            insert ta;
            
            Dsmtracker_Product__c dp =new Dsmtracker_Product__c();
            dp.EMHome_EMHub_PartID__c='test';
            dp.Name='test';
            insert dp;
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c= u.contactId;
            dsmtc.Portal_User__c=userinfo.getUserId();
            insert dsmtc; 

            Recommendation_Scenario__c pro = new Recommendation_Scenario__c();
            pro.Name='test';
            pro.Total_CustomerIncentive__c=5;
            pro.Type__c='';
            pro.Status__c='';
            pro.Installed_Date__c=date.today();
            insert pro;
                       
            Review__c rv = new Review__c();
            rv.Project__c=pro.id;
            rv.Status__c='New';
            insert rv;
            
            Project_Review__c  pr = new Project_Review__c();
            pr.Review__c=rv.id;
            pr.Project__c=pro.id;           
            insert pr;
                        
            Recommendation__c rc =new Recommendation__c ();
            rc.Recommendation_Scenario__c=pro.Id;
            insert rc;

            Change_Order_Line_Item__c colt =new Change_Order_Line_Item__c();
            colt.Change_Order_Quantity__c=5;
            colt.Recommendation__c=rc.id;
            colt.Change_Order_Review__c=rv.id;
            insert colt;
            
            Proposal__c proposal =new Proposal__c();
            proposal.Name='test';
            insert proposal;
            
            Proposal_Recommendation__c prp =new Proposal_Recommendation__c();
            prp.Proposal__c=proposal.id;
            prp.Project__c=pro.id;
            prp.Recommendation__c=rc.id;
            insert prp;
            
            ApexPages.currentPage().getParameters().put('prjid',pr.id);
            ApexPages.currentPage().getParameters().put('id',rv.id);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(rv);
            
            dsmtBillingReviewSummary test = new dsmtBillingReviewSummary(sc);
            
            ApexPages.currentPage().getParameters().put('prjid',pr.id);
            ApexPages.currentPage().getParameters().put('id',rv.id);           
            ApexPages.currentPage().getParameters().put('projIds',rc.Id);
            Apexpages.currentPage().getParameters().put('projId',pro.Id);
            ApexPages.currentPage().getParameters().put('projects','All');
            
            dsmtBillingReviewSummary dbrs= new dsmtBillingReviewSummary();
            dbrs.CreateReviewRecord();
            dbrs.loadRecommendations();
        }
    }
    @istest
    static void runtest1()
    {
        User u = Datagenerator.CreatePortalUser();
        system.runAs(u)
        {  
            
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Email__c='test@gmail.com';
            ta.Name = 'test';
            ta.First_Name__c='test';
            ta.Last_Name__c='test';
            ta.Phone__c='1111';
            insert ta;
            
            Dsmtracker_Product__c dp =new Dsmtracker_Product__c();
            dp.EMHome_EMHub_PartID__c='test';
            dp.Name='test';
            insert dp;
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c= u.contactId;
            dsmtc.Portal_User__c=userinfo.getUserId();
            insert dsmtc;

            Recommendation_Scenario__c pro = new Recommendation_Scenario__c();
            pro.Name='test';
            pro.Total_CustomerIncentive__c=5;
            pro.Type__c='';
            pro.Status__c='';
            pro.Installed_Date__c=date.today();
            pro.Barrier_Incentive_Trigger__c=10;
            insert pro;
                       
            Review__c rv = new Review__c();
            rv.Project__c=pro.id;
            rv.Status__c='New';
            insert rv;
            
            Project_Review__c  pr = new Project_Review__c();
            pr.Review__c=rv.id;
            pr.Project__c=pro.id;          
            insert pr;
                        
            Recommendation__c rc =new Recommendation__c ();
            rc.Recommendation_Scenario__c=pro.Id;
            insert rc;

            Change_Order_Line_Item__c colt =new Change_Order_Line_Item__c();
            colt.Change_Order_Quantity__c=5;
            colt.Recommendation__c=rc.id;
            colt.Change_Order_Review__c=rv.id;
            insert colt;
            
            Proposal__c proposal =new Proposal__c();
            proposal.Name='test';
            insert proposal;
            
            Proposal_Recommendation__c prp =new Proposal_Recommendation__c();
            prp.Proposal__c=proposal.id;
            prp.Project__c=pro.id;
            prp.Recommendation__c=rc.id;
            insert prp;
            
            ApexPages.currentPage().getParameters().put('prjid',pr.id);
            ApexPages.currentPage().getParameters().put('id',rv.id);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(rv);
            
            dsmtBillingReviewSummary test = new dsmtBillingReviewSummary(sc);
            
            ApexPages.currentPage().getParameters().put('id',rv.id);           
            Apexpages.currentPage().getParameters().put('projid',pro.Id);
            
            dsmtBillingReviewSummary dbrs= new dsmtBillingReviewSummary();
            dbrs.CreateReviewRecord();
            dbrs.loadRecommendations();
            
            ApexPages.currentPage().getParameters().put('id', 'a1A1g000000LFu1EAH');
            ApexPages.currentPage().getParameters().put('revid', rv.id);
            Apexpages.currentPage().getParameters().put('projid',pro.Id);
            
            dbrs= new dsmtBillingReviewSummary();
            dbrs.loadRecommendations();
        }
    }
}