@isTest
public class dsmtTradeAllyDocumentsCntrlTest {
    @isTest
    Public Static Void RunTest(){
        
        test.startTest();
        
        User u = Datagenerator.CreatePortalUser();
        
        system.runAs(u) // normally you want to verify the list has data first as this could cause null exception
        {
             Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
             ta.Name = 'test';
             insert ta;
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c=u.contactid;
            insert dsmtc;
            
             Attachment__c atch = new Attachment__c();
            atch.Attachment_Name__c='test attechment';
            atch.Attachment_Type__c='Waiver';
            atch.Expires_On__c=Date.today()+30;
            atch.DSMTracker_Contact__c=dsmtc.id;
            atch.status__c = 'Approved';
            atch.Trade_Ally_Account__c=ta.id;
            insert atch;
            
            dsmtTradeAllyDocumentsCntrl dsmtdoc = new dsmtTradeAllyDocumentsCntrl();
            dsmtdoc.loadDocumentsForManager();
            String purl = dsmtdoc.PortalURL;
            String orgid = dsmtdoc.orgId;

        }
        test.stopTest();
        
    }
}