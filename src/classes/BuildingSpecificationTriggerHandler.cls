public class BuildingSpecificationTriggerHandler extends TriggerHandler implements ITrigger{
    public static boolean stopProcessForBuildingModal = false;
    
    public void bulkBefore() 
    {
        if (trigger.isInsert) 
        {
            calculateBuildingDefaults(trigger.new, (Map<Id, Building_Specification__c>) trigger.oldmap, trigger.isInsert);
            calculateDefaults(trigger.new, (Map<Id, Building_Specification__c>) trigger.oldmap, trigger.isInsert);
        } 
        else if (trigger.isUpdate) 
        {
            calculateBuildingDefaults(trigger.new, (Map<Id, Building_Specification__c>) trigger.oldmap, trigger.isInsert);
            calculateDefaults(trigger.new, (Map<Id, Building_Specification__c>) trigger.oldmap, trigger.isInsert);
        } 
        else if (trigger.isDelete) 
        {
            
        } 
        else if (trigger.isUndelete) 
        {
            
        }
    }

    public void bulkAfter() 
    {
        if (trigger.isInsert) 
        {
            
        } 
        else if (trigger.isUpdate) 
        {
            //UpdateAllApplianceOnBuildingYearChange(trigger.new, (Map<Id, Building_Specification__c>) trigger.oldmap);
            dsmtBldgspecBAScfm50AU(trigger.new, (Map<Id, Building_Specification__c>) trigger.oldmap);
        } 
        else if (trigger.isDelete) 
        {
            
        } 
        else if (trigger.isUndelete) 
        {
            
        }
    }
    
    public static void calculateDefaults(list<Building_Specification__c> newList, Map<Id, Building_Specification__c> oldMap, boolean isInsert)
    {
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'EnclosedCavityAreasContent' LIMIT 1];
        String body = sr.Body.toString();
        Map<String, String> content = (Map<String,String>) JSON.deserialize(body, Map<String,String>.class);
        
        Map<String, String> valueToApiNameMap = new Map<String, String>();
        valueToApiNameMap.put('Wood Shingle & Clapboard Content', 'Wood_Shingle_Clapboard_Content__c');
        valueToApiNameMap.put('Vinyl Siding Content', 'Vinyl_Siding_Content__c');
        valueToApiNameMap.put('Asphalt Siding Content', 'Asphalt_Siding_Content__c');
        valueToApiNameMap.put('T-111 Content', 'T_111_Content__c');
        valueToApiNameMap.put('Asbestos Siding Content', 'Asbestos_Siding_Content__c');
        valueToApiNameMap.put('Aluminum Siding Content', 'Aluminum_Siding_Content__c');
        valueToApiNameMap.put('Interior Drill & Blow Content', 'Interior_Drill_Blow_Content__c');
        valueToApiNameMap.put('Interior Paneling Content', 'Interior_Paneling_Content__c');
        valueToApiNameMap.put('Overhang Content', 'Overhang_Content__c');
        valueToApiNameMap.put('Garage Ceiling Content', 'Garage_Ceiling_Content__c');
        valueToApiNameMap.put('Temporary Access Content', 'Temporary_Access_Content__c');
        valueToApiNameMap.put('Sheathing Access Content', 'Sheathing_Access_Content__c');
        
        for(Building_Specification__c newSpec: newList) 
        {
            String eca = newSpec.Enclosed_Cavity_Areas__c;
            String[] arr = new String[]{};
            if(eca != null) {
                arr = eca.split(';');
            }
            
            for(String picklistValue: valueToApiNameMap.keySet()) {
                String key = picklistValue.replace(' Content', '');
                if(arr.contains(key)) {
                    newSpec.put(valueToApiNameMap.get(picklistValue), True); // content.get(picklistValue)
                } else {
                    newSpec.put(valueToApiNameMap.get(picklistValue), False); //
                }
            }
        }
    }
    
    public static void calculateBuildingDefaults(list<Building_Specification__c> newList, Map<Id, Building_Specification__c> oldMap, boolean isInsert)
    {
        Set<Id> appId = new Set<Id>();
        
        for(Building_Specification__c bs : newList) {
            if(bs.Appointment__c != null){
                appId.add(bs.Appointment__c);
            }
        }
        
        Map<Id,Id> elcIdMap = new Map<Id,Id>();
        Map<Id,Eligibility_Check__c> ecMap = new Map<Id,Eligibility_Check__c>();
        
        if(appId.size() > 0)
        {
            List<Appointment__c> appList = [select id,Workorder__r.Eligibility_Check__c from Appointment__c where id in : appId];
            
            system.debug('appList '+appList );
            
            if(appList != null && appList.size() > 0)
            {   
                for(Appointment__c app : appList){
                    elcIdMap.put(app.Id,app.Workorder__r.Eligibility_Check__c);
                }
                
                if(elcIdMap.keyset().size() > 0){
                    List<Eligibility_Check__c> ecList = [select id,Home_Size__c,Year_built_in__c 
                    From Eligibility_Check__c where id in : elcIdMap.values()];
                    for(Eligibility_Check__c ec : ecList){
                        ecMap.put(ec.Id,ec);
                    }
                }
            }
        }
    
        for(Building_Specification__c newSpec: newList) 
        {
            if(newSpec.IsFromFieldTool__c) continue;

            if(elcIdMap.get(newSpec.Appointment__c) != null)
            {
                if(ecMap.get(elcIdMap.get(newSpec.Appointment__c)) != null)
                {              
                    newSpec.HomeSize__c =  ecMap.get(elcIdMap.get(newSpec.Appointment__c)).Home_Size__c;              
                    if(newSpec.HomeSize__c != null)
                        newSpec.HomeSize__c = SurfaceConstants.CommonBuildingSize.get(newSpec.HomeSize__c);
                }
            }

            if(newSpec.homeSize__c  == null)
                newSpec.homeSize__c  = 'Medium';
            
            if((elcIdMap !=null 
                && newSpec !=null 
                && ecMap !=null 
                && newSpec.Appointment__c !=null) &&                  
                    (newSpec.Actual_Year_Built__c == null 
                        || newSpec.Actual_Year_Built__c == ''))
                {
                    if(ecMap.get(elcIdMap.get(newSpec.Appointment__c))!=null)
                        newSpec.Year_Built__c = ecMap.get(elcIdMap.get(newSpec.Appointment__c)).Year_built_in__c;    
                }
                
            /// DSST-15797
            /// Rohit Sharma Added extra condition block for value not to be null
            /// #Code Block start

            if(newSpec.Year_Built__c ==null && newSpec.Actual_Year_Built__c !=null)
            {
                newSpec.Actual_Year_Built__c =null;    
            }
            
            if(newSpec.Actual_Bedrooms__c == null 
                || newSpec.Actual_Bedrooms__c ==0
                || newSpec.Bedromm__c==null)
            {
                newSpec.Actual_Bedrooms__c = null;
                newSpec.Bedromm__c = SurfaceConstants.CommonDefaultBedrooms.get(newSpec.homeSize__c);
            }            

            if(newSpec.Actual_Occupants__c ==null 
                || newSpec.Actual_Occupants__c==0 
                || newSpec.Occupants__c == null)
            {
                newSpec.Actual_Occupants__c = null;
                newSpec.Occupants__c = newSpec.Bedromm__c;
            }            
            
            if(newSpec.Actual_Orientation__c == null || newSpec.Actual_Orientation__c =='')
                newSpec.Orientation__c = SurfaceConstants.DefaultOrientation;//'South';
            
            if(newSpec.Actual_Floors__c == null || newSpec.Actual_Floors__c == 0 || newSpec.Floors__c == null)
            {
                newSpec.Actual_Floors__c = null;
                newSpec.Floors__c = SurfaceConstants.CommonDefaultNumFloors.get(newSpec.homeSize__c);
            }

            if(newSpec.Actual_Floor_Area__c == null ||
                newSpec.Actual_Floor_Area__c ==0 || newSpec.Floor_Area__c ==null)
            {
                newSpec.Actual_Floor_Area__c = null;
                integer tbr = integer.ValueOf(newSpec.Bedromm__c);
                if(tbr > 4) tbr =0;
                newSpec.Floor_Area__c = SurfaceConstants.CommonFloorAreaTable.get(newSpec.homeSize__c).get(tbr);
            }            
            
            if(newSpec.Actual_Ceiling_Heights__c == null 
                || newSpec.Actual_Ceiling_Heights__c ==0
                || newSpec.Ceiling_Heights__c==null)
            {
                newSpec.Actual_Ceiling_Heights__c = null;
                newSpec.Ceiling_Heights__c = SurfaceConstants.DefaultCeilingHeight;//8;
            }
            /// #Code Block End
            
            if(newSpec.Finished_Basement_Area__c == null)
                newSpec.Finished_Basement_Area__c = 0;
            
            decimal ceilingHeight = newSpec.Ceiling_Heights__c;
            decimal totalFlorArea = DSMTEAModel.ISNULL(newSpec.Floor_Area__c) + newSpec.Finished_Basement_Area__c;
            newSpec.Total_Volume__c = ceilingHeight * totalFlorArea;
            newSpec.Total_Floor_Area__c = totalFlorArea;
        }
    }
    
    public static void dsmtBldgspecBAScfm50AU(list<Building_Specification__c> newList, Map<Id, Building_Specification__c> oldMap)
    {
        set<Id> builndingIds = new Set<Id>();
        Final String EMHomePartId = 'AirSealHoursAt_625_CFM50';
            
        Map<Id, Recommendation__c> recommendationEAMap = new Map<Id,Recommendation__c>();
        Map<Id, Thermal_Envelope_Type__c> thermalEnvTypeEAMap = new Map<Id,Thermal_Envelope_Type__c>();
        Map<Id, Floor__c> floorEAMap = new Map<Id,Floor__c>();
    
        for(Building_Specification__c b : newList)
        {
            if(b.Floor_Area__c != oldmap.get(b.Id).Floor_Area__c)
                builndingIds.add(b.Id);
        }
    
        if(!builndingIds.isEmpty())
        {
    
            Map<Id,Energy_Assessment__c> energyAssessments = new Map<Id,Energy_Assessment__c>([SELECT Id,Building_Specification__c,
                                                                     Building_Specification__r.Floor_Area__c
                                                                     FROM Energy_Assessment__c
                                                                     WHERE Building_Specification__c In: builndingIds]); 
            
    
            List<Thermal_Envelope_Type__c> thermalEnvTypes = [SELECT SpaceConditioning__c,Energy_Assessment__c                                                              
                                                                     FROM Thermal_Envelope_Type__c
                                                                     WHERE RecordType.Name = 'Basement' 
                                                                     AND Energy_Assessment__c In: energyAssessments.KeySet()];
            for(Thermal_Envelope_Type__c t : thermalEnvTypes)
            {
                thermalEnvTypeEAMap.put(t.Energy_Assessment__c,t);
            }
    
            List<Floor__c> floors = [SELECT Id,Thermal_Envelope_Type__c,Area__c,Thermal_Envelope_Type__r.Energy_Assessment__c
                                                                     FROM Floor__c
                                                                     WHERE Thermal_Envelope_Type__r.Energy_Assessment__c In: energyAssessments.keyset()
                                                                     AND Type__c = 'Basement Floor'];
            for(Floor__c f : floors)
            {
                floorEAMap.put(f.Thermal_Envelope_Type__r.Energy_Assessment__c,f);    
            }  
    
            List<Recommendation__c> recomms = [Select Recommendation_Scenario__c, 
                                                Recommendation_Scenario__r.Energy_Assessment__c,
                                                Seal_Penetrations__c,
                                                Seal_Rim_Joist_Area__c
                                                FROM Recommendation__c
                                                WHERE Recommendation_Scenario__r.Energy_Assessment__c In: energyAssessments.keyset() 
                                                AND DSMTracker_Product__r.EMHome_EMHub_PartID__c =: EMHomePartId];
        
    
            for( Recommendation__c recObj : recomms) 
            {  
                recommendationEAMap.put(recObj.Recommendation_Scenario__r.Energy_Assessment__c, recObj);
            }
    
            List<Building_Specification__c> buildingSpecsToUpdate = new List<Building_Specification__c>();
            Decimal BAScfm50 = 0.0;
            Floor__c floor;
            Thermal_Envelope_Type__c thermalType;
            Recommendation__c recommendation;
                                                                 
            for(Energy_Assessment__c ea : energyAssessments.values())
            {
                if(floorEAMap.containskey(ea.Id) && thermalEnvTypeEAMap.containskey(ea.Id) && recommendationEAMap.containskey(ea.Id)){
                    floor = floorEAMap.get(ea.Id);
                    thermalType = thermalEnvTypeEAMap.get(ea.Id);
                    recommendation = recommendationEAMap.get(ea.Id);
                    
                    BAScfm50 = calculateBAScf50(recommendation.Seal_Penetrations__c, thermalType.SpaceConditioning__c,recommendation.Seal_Rim_Joist_Area__c,ea.Building_Specification__r.Floor_Area__c,floor.Area__c);
                    buildingSpecsToUpdate.add(
                                                new Building_Specification__c(
                                                                                Id = ea.Building_Specification__c,
                                                                                BAS_cfm50_Floor_Calculation__c = BAScfm50));
                    
                            
                }
            }
            
            if(!buildingSpecsToUpdate.isEmpty())
                update buildingSpecsToUpdate;
        }
    }
    
    @testvisible
    private static void UpdateAllApplianceOnBuildingYearChange(list<Building_Specification__c> newList, Map<Id, Building_Specification__c> oldMap)
    {
        Set<String> bsIdSet = new Set<String>();
        
        for(Building_Specification__c a : newList)
        {
            if((a.Year_Built__c != null && a.Year_Built__c != oldMap.get(a.Id).Year_Built__c) ||
               (a.Bedromm__c != null && a.Bedromm__c != oldMap.get(a.Id).Bedromm__c) ||
               (a.Occupants__c != null && a.Occupants__c != oldMap.get(a.Id).Occupants__c))
                bsIdSet.add(a.Id);
        }
        
        if(bsIdSet.size() > 0)
            dsmtApplianceHelper.UpdateAllApplianceOnBuildingYearChange(bsIdSet);
    }
    
    @testvisible
    private static decimal calculateBAScf50(String seal_Pen, String space, String seal_Rim, Decimal FA, Decimal A) {
        Decimal bas = 0.0;
        if(space == 'Conditioned')
           return calculateSub(FA, A);
           
        if(space == 'Vented')
           return FA;
           
        if(space == 'Unconditioned'){
            if(seal_Pen == 'Ceiling' && (seal_Rim == null || seal_Rim == 'Wall'))  
                return FA;
            if(seal_Rim == 'Wall' && (seal_pen == null || seal_Pen == 'Wall'))
                return calculateAdd(FA ,A);
            
            if(seal_Pen == null)
                return FA;      
        
        }       
        
        return bas;
    }  
    
    @testvisible
    private static Decimal calculateAdd(Decimal num1, Decimal num2){
        if(num1 != null && num2 != null){
            return (num1 + num2);
        }
        
        return 0;
    }  
    
    @testvisible
    private static Decimal calculateSub(Decimal num1, Decimal num2){
        if(num1 != null && num2 != null){
            return (num1 - num2);
        }
        
        return 0;
    } 

    public static string getSObjectFields(string objName)
    {
        return dsmtHelperClass.sObjectFields(objName);
    }
}