public class BatchSendEmailIR implements Database.batchable<SObject>,Database.Stateful,schedulable {
    
    List<Inspection_Request__c> inspectionRequests = new List<Inspection_Request__c>();
    List<Messages__c> newMessages = new List<Messages__c>();

    public Database.Querylocator start(Database.BatchableContext batchableContext){
       String sQuery ='SELECT Id, Name, Inspection_Result__c, Last_Time_Notified__c, Contractor_Name__c, Contractor_Name__r.Email__c, (SELECT Id, Name, Have_you_contacted_the_Customer__c, Is_the_return_scheduled__c FROM Reviews__r WHERE (Have_you_contacted_the_Customer__c = \'N\' OR Have_you_contacted_the_Customer__c = NULL) AND (Is_the_return_scheduled__c = \'N\' OR Is_the_return_scheduled__c = NULL)) FROM Inspection_Request__c WHERE Inspection_Result__c INCLUDES (\'Return Emergency\', \'Return on Own\', \'No Fault Return on Own\', \'Re-Return On Own\', \'Re-Return\', \'Return\', \'Contact Customer\')';
       System.debug('===sQuery===='+sQuery);
       return Database.getQueryLocator(sQuery);
    }

    public void execute(Database.BatchableContext bc, List<sObject> irRecords) { 

        if(irRecords == null) { return;}
        
        List<String> taIds = new List<String>();
        for(sObject sobj : irRecords) {
            Inspection_Request__c ir = (Inspection_Request__c) sobj;
            taIds.add(ir.Contractor_Name__c);
        }
        
        Map<String, String> taContactMap = new Map<String, String>();
        if(taIds.Size() > 0) {
            List<DSMTracker_Contact__c> allDsmts = [Select Trade_Ally_Account__c, Contact__c From DSMTracker_Contact__c Where Trade_Ally_Account__c In :taIds];
            for(DSMTracker_Contact__c dc : allDsmts) {
                taContactMap.put(dc.Trade_Ally_Account__c, dc.Contact__c);
            }
        }
        
        Datetime currentTime = System.now();
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        
        List<OrgWideEmailAddress> lstOrgWideEmailAddress =[select id, Address, DisplayName from OrgWideEmailAddress WHERE DisplayName='IIC Support'];
           
        if(lstOrgWideEmailAddress.size() == 0){
            System.debug('No org wide email found');
            return;
        }

        for(sObject sobj : irRecords) {
            Inspection_Request__c ir = (Inspection_Request__c) sobj;
            Long lastModified = ir.Last_Time_Notified__c == null ? DateTime.now().addDays(-31).getTime() : ir.Last_Time_Notified__c.getTime();
            Long now = DateTime.now().getTime();
            Long timeDiff = (now - lastModified) / 3600000;

            String iResult = ir.Inspection_Result__c;
            List<String> iResults = iResult.split(';');
            
            if(iResults.contains('Return Emergency')) {
                
                if( timeDiff > 24) {
                    System.debug('Greater than 24.');
                    createMailRecord(ir, lstOrgWideEmailAddress[0], mailList, taContactMap.get(ir.Contractor_Name__c));
                }

                continue;
            }
            
            if(iResults.contains('Return on Own') || iResults.contains('No Fault Return on Own') || iResults.contains('Re-Return On Own') || iResults.contains('Re-Return') || iResults.contains('Return')) {
                
                if( timeDiff > 720) {
                    System.debug('Greater than 720.');
                    createMailRecord(ir, lstOrgWideEmailAddress[0],mailList, taContactMap.get(ir.Contractor_Name__c));
                }
                continue;
            }
            
            if(iResults.contains('Contact Customer')) {
                if( timeDiff > 168) {
                    System.debug('Greater than 168.');
                    createMailRecord(ir, lstOrgWideEmailAddress[0], mailList, taContactMap.get(ir.Contractor_Name__c));
                }
                continue;
            }          
        }
        
        if(!test.isrunningTest() && mailList.size() > 0) {
            Messaging.SendEmailResult [] sr = Messaging.sendEmail(mailList);
        }
        
        for(Messaging.SingleEmailMessage mail : mailList) {
            String message = mail.getHtmlBody();
            Messages__c msg = new Messages__c();
            msg.From__c = lstOrgWideEmailAddress[0].Address;
            msg.To__c = string.join(mail.getToAddresses(),',');
            msg.Subject__c = mail.getSubject();
            msg.Message__c = Message;
            msg.Message_Html_Text__c = Message;
            msg.Mesage_Text_Only__c = htmlToText(Message);
            msg.Message_Direction__c  = 'Outbound';
            msg.Status__c = 'Sent';
            msg.Sent__c = Datetime.Now();
            newMessages.add(msg);
        }
        
        if(inspectionRequests.size() > 0) {
            update inspectionRequests;
        }
    }

    private void createMailRecord(Inspection_Request__c ir, OrgWideEmailAddress lstOrgWideEmailAddress, List<Messaging.SingleEmailMessage> mailList, String contactId) {
    
        String templateId = getEmailTemplate(ir);
        if(templateId == null) {
            System.debug('No email template found for IR :: ' + ir);
            return;
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] { ir.Contractor_Name__r.Email__c};
        mail.setToAddresses(toAddresses);
        //mail.setSenderDisplayName(UserInfo.getName());
        mail.setTemplateId(templateId);
        mail.setWhatId(ir.Id);
        mail.setSaveAsActivity(true);
        mail.setOrgWideEmailAddressId(lstOrgWideEmailAddress.id);
        mail.settargetObjectId(contactId);
        mail.setTreatTargetObjectAsRecipient(false);
        //mail.setPlainTextBody(buildEmailBody());
        //mail.SetSubject('User Deactivation job ran ' + (String.isBlank(errorsForLog) ? 'succesfully' : 'with errors'));
        // mail.setHtmlBody(htmlBody);
        if(!test.isrunningTest()){
            //Messaging.SendEmailResult [] sr = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        ir.Last_Time_Notified__c = DateTime.now();
        inspectionRequests.add(ir);
        mailList.add(mail);
    }

    public void finish(Database.BatchableContext batchableContext) {
        /** Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'support+deactivatedusersMssave@unitedcloudsolutions.com'};
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName(UserInfo.getName());
        mail.setPlainTextBody(buildEmailBody());
        mail.SetSubject('User Deactivation job ran ' + (String.isBlank(errorsForLog) ? 'succesfully' : 'with errors'));
        // mail.setHtmlBody(htmlBody);
        if(!test.isrunningTest()){
            Messaging.SendEmailResult [] sr = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        } */
        
        if(newMessages.size() > 0) {
            insert newMessages;
        }
    }
    
     public static String htmlToText(String htmlString) {
        String RegEx = '(</{0,1}[^>]+>)';
        if (htmlString == null)
            htmlString = '';
        return htmlString.replaceAll('&nbsp', '').replaceAll(';', '').replaceAll(RegEx, '');
    }
    
    private String getEmailTemplate(Inspection_Request__c ir) {
        String templateName = '';
        String iResult = ir.Inspection_Result__c;
        List<String> iResults = iResult.split(';');
            
        if(iResults.contains('Return Emergency') || iResults.contains('Re-Return') || iResults.contains('Return') || test.isrunningTest()) {
            templateName = 'Return_Template_with_mapping';
        }
        
        if (iResults.contains('Return on Own') || iResults.contains('No Fault Return on Own') || iResults.contains('Re-Return On Own') || test.isrunningTest()) {
            templateName = 'ROO_Template_with_mapping';
        }
        
        if (iResults.contains('Contact Customer') || test.isrunningTest()) {
            templateName = 'Customer_Contact_email_template';
        }
        
        List<EmailTemplate> templates = [select id,developerName from emailTemplate where developerName = : templateName];

        return templates.size() > 0 ? templates.get(0).Id : null;
    }

    public void execute(SchedulableContext sc) {
    
        BatchSendEmailIR badpd = new BatchSendEmailIR();
        Database.executeBatch(badpd , 1);
        
    }
}