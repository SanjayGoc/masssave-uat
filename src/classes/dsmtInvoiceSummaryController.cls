public with Sharing class  dsmtInvoiceSummaryController {
   
    public String invoiceId {get;set;}
    Public Date mydate{get;set;}
    public String sdate {get;set;}
    public String lastnInvType {get;set;}
    public double programPriceTotal {get;set;}
     public double customerPriceTotal {get;set;}
     public double btuTotal {get;set;}
    public List<TransactionWrapper> transactionList {get;set;}
    
    public dsmtInvoiceSummaryController(ApexPages.StandardController controller) {
        init();
    }
    
    private void init()
    {        
        programPriceTotal = 0;
        customerPriceTotal =0;
        btuTotal =0;
        invoiceId = ApexPages.currentPage().getParameters().get('id');
          
        List<Invoice__c> inv=[Select Invoice_Type__c,Invoice_Date__c FROM Invoice__c where Id=:invoiceId];
       // DateTime dT=inv[0].Invoice_Date__c;
        //myDate = date.newinstance(dT.year(),dT.month(),dT.day());
        sdate = String.valueOf(inv[0].Invoice_Date__c);
        sdate=sdate.remove('-');
        lastnInvType=inv[0].Invoice_Type__c.right(3);
        transactionList = new List<TransactionWrapper>();
          Map<String,Double> qtyMap = new Map<String,Double>();
          Map<String,Double> priceMap = new Map<String,Double>();
          Map<String,Double> customerPrice= new Map<String,Double>();
           Map<String,Double> btuPrice= new Map<String,Double>();
         
                                                            
           
            
            List<Invoice_Line_Item__c> invLineLst = [select id,MEA_Id__c,Recommendation_Description__c,QTY__c,Program_Price__c,
                                                    Fuel_Type__c,SAVINGS_ELE__c ,SAVINGS_GAS__c,SAVINGS_PROPANE__c,
                                                    SAVINGS_OIL__c,SAVINGS_OTHER__c,CUSTOMER_COST__c  
                                                    FROM Invoice_Line_Item__c where invoice__c =: invoiceId];
           
        
        for(Invoice_Line_Item__c invl :  invLineLst){
        
        if(invl.QTY__c!=null){
            if(qtyMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null){
                qtyMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,invl.QTY__c);
            }
            else{
                qtyMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,qtyMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+invl.QTY__c);
            }
           } 
           else{
            qtyMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,0);
           }
            if(priceMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.Program_Price__c != null){
                priceMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,invl.Program_Price__c );
                
            }
            else{
                if(invl.Program_Price__c != null){
                    priceMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,priceMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+invl.Program_Price__c );
                }
            }
            
            
             if(customerPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.CUSTOMER_COST__c!= null){
                customerPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,invl.CUSTOMER_COST__c);
                
            }
            else{
                if(invl.CUSTOMER_COST__c!= null){
                    customerPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,customerPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+invl.CUSTOMER_COST__c);
                }
            }
            
            
      
             
              if(invl.Fuel_Type__c == 'Electric'){
                     if(btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.SAVINGS_ELE__c != null){
                btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c, decimal.valueOf(invl.SAVINGS_ELE__c));
                
                }
               else{
                if(invl.SAVINGS_ELE__c != null){
                    btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+ decimal.valueOf(invl.SAVINGS_ELE__c));
                   }
                 }
                }///////////////////////////
       if(invl.Fuel_Type__c == 'Gas'){
            
         if(btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.SAVINGS_GAS__c!= null){
                btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c, decimal.valueOf(invl.SAVINGS_GAS__c));
                
                }
               else{
                if(invl.SAVINGS_GAS__c!= null){
                    btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+ decimal.valueOf(invl.SAVINGS_GAS__c));
                   }
                 }
                   
                }//////
    if(invl.Fuel_Type__c == 'Propane'){
                 
                   
                    
           if(btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.SAVINGS_PROPANE__c!= null){
                btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c, decimal.valueOf(invl.SAVINGS_PROPANE__c));
                
                }
               else{
                if(invl.SAVINGS_PROPANE__c!= null){
                    btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+ decimal.valueOf(invl.SAVINGS_PROPANE__c));
                   }
                 }
             }
                
        if(invl.Fuel_Type__c == 'Oil'){
                                     
           if(btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.SAVINGS_OIL__c!= null){
                btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c, decimal.valueOf(invl.SAVINGS_OIL__c));
                
                }
               else{
                if(invl.SAVINGS_OIL__c!= null){
                    btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+ decimal.valueOf(invl.SAVINGS_OIL__c));
                   }
                 }
           }
                
         if(invl.Fuel_Type__c == 'Other'){
                   
            if(btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.SAVINGS_OTHER__c != null){
                btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c, decimal.valueOf(invl.SAVINGS_OTHER__c ));
                
                }
               else{
                if(invl.SAVINGS_OTHER__c != null){
                    btuPrice.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,btuPrice.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+ decimal.valueOf(invl.SAVINGS_OTHER__c ));
                   }
                 }
                   
                }
       }
        
      
        
 for(String str : qtyMap.keyset()){
        
transactionList.add(new TransactionWrapper(str.split('~~~').get(0), str.split('~~~').get(1), decimal.valueOf(qtyMap.get(str)),
                                            priceMap.get(str),customerPrice.get(str),btuPrice.get(str)));
       
          if(priceMap.get(str)!=null){
           programPriceTotal =programPriceTotal + decimal.valueOf(priceMap.get(str));
           }
           
           if(customerPrice.get(str)!=null){
           customerPriceTotal =customerPriceTotal + decimal.valueOf(customerPrice.get(str));
           }
           
           if(btuPrice.get(str)!=null){
           btuTotal =btuTotal + decimal.valueOf(btuPrice.get(str));
           }
           
           
        }
        
    }
    
    public class TransactionWrapper
    {
        public String title {get;set;}
        public String description {get;set;}
        public Decimal quantity {get;set;}
        public decimal programPrice {get;set;}
        public decimal customerPrice {get;set;}
        public decimal btuSavings {get;set;}
        
        public TransactionWrapper(String title,
                                  String description,
                                  Decimal quantity,
                                   decimal programPrice,
                                  decimal customerPrice,
                                  decimal btuSavings)
        {
            this.title = title;
            this.description = description;
            this.quantity = quantity; 
            this.programPrice = programPrice;
            this.customerPrice = customerPrice;
            this.btuSavings = btuSavings;
        }
    }
   
}