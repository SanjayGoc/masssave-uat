@isTest
public class dsmtCheckWorkorderonWoTest {

    static TestMethod void test(){
        
        Account a = new Account();
        a.Name = 'Test';
        a.Billing_Account_Number__c = '11470390235';
        insert a;
        
        
        Location__c loc = new Location__c();
        loc.Name = 'Test Loc';
        insert loc;
        
        Customer__c cust = new Customer__c();
        cust.Account__c = a.Id;
        insert cust;
        
        Employee__c e = new Employee__c();
        e.Location__c = loc.id;
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        e.Employee_Id__c = hexDigest.substring(0,11);
        e.Active__c = true;
        e.Status__c ='Active';   
        insert e;
        
        DSMTracker_Contact__c dc = new DSMTracker_Contact__c();
        insert dc;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.Customer__c = cust.Id;
        insert ec;
        
       
        
        Appointment__c apt = new Appointment__c();
        apt.DSMTracker_Contact__c = dc.id;
        apt.Appointment_Start_Time__c=date.today();
        apt.Appointment_End_Time__c= date.today();
        apt.Eligibility_Check__c = ec.Id;
        apt.Customer_Reference__c = cust.Id;
        try{
            
        insert apt;
        apt.Workorder__c = null;
        apt.Employee__c = null;
                
        update apt;
        }        
        catch(Exception EX) {
            
        }
        
   
        
        dsmtCheckWorkorderonWo obj = new dsmtCheckWorkorderonWo();
        obj.execute(null);
        
              
        
        
        
    }
}