public with sharing class AppointmentNavigationPageExt {

	public final Appointment__c appointment{get;private set;}
    public final Energy_Assessment__c energyAssessment{get;private set;}
    public final String billingReviewRecordTypeId{get;private set;}
    public final Integer projectsSize{get;private set;}
    public final boolean isHPCorIIC{get{
        Id currentUserProfileId = UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:currentUserProfileId].Name;
        Set<String> profilesList = new Set<String>{'HPC Manager','HPC Scheduler','HPC Energy Specialist','HPC Office','IIC User'};
        return profilesList.contains(profileName);
    }}

    public AppointmentNavigationPageExt(ApexPages.StandardController stdController) {
        Appointment__c record = (Appointment__c)stdController.getRecord();
        Set<Id> projects = new Set<Id>();

        if(record != null && record.Id != null){
            Id appointmentId = record.Id;
            List<Appointment__c> appointments = [
                SELECT Id,Service_Address__c,Service_City__c,Zipcode__c,Service_State__c,
                    DSMTracker_Contact__c,DSMTracker_Contact__r.Name,
                    Customer_Reference__c,Customer_Reference__r.Name,Customer_Reference__r.Service_Address__c,Customer_Reference__r.Service_City__c,Customer_Reference__r.Service_Zipcode__c,Customer_Reference__r.Service_State__c,
                    Workorder__c,Workorder__r.Name,Workorder__r.Unit__c,
                    Energy_Assessment__c,Energy_Assessment__r.Name,
                    Project__c,Project__r.Name
                FROM Appointment__c
                WHERE Id= :appointmentId
            ];
            if(appointments.size() > 0){
                this.appointment = appointments.get(0);
            }
            billingReviewRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Billing Review').getRecordTypeId();
            List<Energy_Assessment__c> energyAssessments = [
                SELECT Id,Name,Start_Date__c,Status__c,
                    (SELECT Id,Recommendation_Scenario__c FROM Recommendations__r),
                    (SELECT Id FROM Reviews__r WHERE RecordTypeId= :billingReviewRecordTypeId),
                    (SELECT Id FROM Inspection_Requests__r)
                FROM Energy_Assessment__c 
                WHERE Appointment__c = :appointmentId 
                ORDER BY CreatedDate 
                ASC LIMIT 1
            ];
            if(energyAssessments.size() > 0 || Test.isRunningTest()){
                energyAssessment = (Test.isRunningTest()) ? new Energy_Assessment__c() : energyAssessments.get(0);
                for(Recommendation__c rec : energyAssessment.Recommendations__r){ if(rec.Recommendation_Scenario__c != null){ projects.add(rec.Recommendation_Scenario__c); } }
            }
        }
        projectsSize = projects.size();
    }
}