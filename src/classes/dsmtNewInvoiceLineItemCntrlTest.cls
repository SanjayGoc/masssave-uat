@istest
public class dsmtNewInvoiceLineItemCntrlTest
{
    @istest
    static void test()
    {
        
        
        Account acc = new Account();
        acc.Name = 'Xcel Energy NM';
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        acc.Billing_Account_Number__c= hexDigest.substring(0,11);
        insert acc;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Name='Air Sealing_AS Hours';
        insert proj;
        
        Invoice__c invobj = new Invoice__c();
        invobj.invoice_date__c = date.Today();
        invobj.Account__c = acc.Id;
        invobj.Project__c= proj.id;
        insert invObj;
        
        
       /*            
        Recommendation__c rc = new Recommendation__c();
        rc.Recommendation_Scenario__c = proj.id;
        insert rc;

        Recommendation__c rc1 = new Recommendation__c();
        rc1.Recommendation_Scenario__c = proj.id;
        insert rc1;
        */
        Invoice_Line_Item__c invl = new Invoice_Line_Item__c();
        invl.invoice__c = invObj.Id;
        insert invl;
        ApexPages.currentPage().getParameters().put('id',invObj.id);
     //   ApexPages.currentPage().getParameters().put('id', String.valueOf(invl.Id));

        test.startTest();        
        dsmtNewInvoiceLineItemCntrl dnii=new dsmtNewInvoiceLineItemCntrl();
        dnii.saveItems();
        test.stopTest();
    }
}