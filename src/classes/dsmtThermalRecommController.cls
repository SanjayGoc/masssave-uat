public class dsmtThermalRecommController {
    public string  eassid {get;set;}
    public string mechanicalId {get;set;}
    public string mechanicalTypeId {get;set;}
    public string parentId {get;set;}
    public string parentField {get;set;}
    public String RecommendationId{get;set;}
    public String recordTypeName {get;set;}
    public String recordType {get;set;}
    public String parentRecordTypeName {get;set;}
    public Recommendation__c recommendation {get;set;}
    public String addOn {get;set;}
    public list<Recommendation__c> thermalEnvRecom {get;set;}
    
    public dsmtThermalRecommController() {
        recommendation = new Recommendation__c();
        recommendation.Status_Code__c = 'Recommended';
        thermalEnvRecom = new list<Recommendation__c>();
        eassId = Apexpages.currentPage().getParameters().get('assessId');
        
        thermalEnvRecom = fetchThermalEnvRecom();
    }
    
    public String getAssignDefaultValues()
    {
        system.debug('recordType :::::' + recordType);
        
        if(Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().containskey(recordType))
            recommendation.RecordTypeId = Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        
        return null;
    }
    
    public list<Recommendation__c> fetchThermalEnvRecom(){
        return [select id, Status__c, Operation__c, Part__c, Description__c, CreatedDate
                     from Recommendation__c
                     where Thermal_Envelope_Type__c =: parentId
                     and Thermal_Envelope_Type__c != null];
    }
    
    public void saveNewReccom() {
        
        recommendation.put(parentField, parentId);
        insert recommendation;   
        
        recommendation = new Recommendation__c();
        thermalEnvRecom = fetchThermalEnvRecom();
    }
    
    public Pagereference DeleteRecommendation(){
        List<Recommendation__c> recList = [select id from Recommendation__c where id =: RecommendationId];
        
        if(recList != null && recList.size() > 0){
            delete recList;
        }
        return null;
    }
}