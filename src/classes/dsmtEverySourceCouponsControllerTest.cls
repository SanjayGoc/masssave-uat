@isTest
public class dsmtEverySourceCouponsControllerTest {
	@isTest
    public static void runTest(){  
        EverSourceIR_Site__c cr = new EverSourceIR_Site__c();
        cr.Account_Name__c='EverSource Instant Rebate';
        cr.Allowed_Attempt__c=2;
        cr.Checklist_Name__c='EverSouce_IR_Coupons_Portal';
        cr.Eligible_Measure_Name__c='Smart Thermostat';
        INSERT cr;
         
        Account utlAcc = new Account();
        utlAcc.Name = cr.Account_Name__c; 
        utlAcc.Utility_Service_Type__c = 'Electric';
        INSERT utlAcc;
		 
        Account retAcc = new Account();
        retAcc.Name = 'Test Retailer'; 
        INSERT retAcc;
		
        Portfolio__c p=new Portfolio__c();
        p.Account__c = utlAcc.Id;
        INSERT p;
        
		Program__c prog = new Program__c();
        prog.Name = 'ComEd Instant Rebate';
        prog.GL_String__c='test';
        prog.Portfolio__c = p.Id;
        prog.Account__c=utlAcc.Id;
        INSERT prog;
        
        MOU__c mou= new MOU__c();
        mou.Retailer__c = retAcc.Id;
        mou.Account__c = utlAcc.Id; 
        INSERT mou;
		        
        Qualified_Product_List_Master__c Obj1 = new Qualified_Product_List_Master__c(Name = 'test', MOU__c = mou.Id, Program__c = prog.Id);
        insert Obj1;
        
        DSMTracker_Product__c Obj2 = new DSMTracker_Product__c();
        insert Obj2;
        
        Measure__c mesure = new Measure__c();
        mesure.Measure_Name__c = cr.Eligible_Measure_Name__c;
        INSERT mesure;
		
		Eligible_Measure__c em = new Eligible_Measure__c(Measure__c = mesure.Id,Program__c=prog.Id, Portfolio__c=p.Id);
        INSERT em;
        
        Qualified_Product_List__c Obj = new Qualified_Product_List__c(Qualified_Product_List_Master__c = Obj1.id, DSMTracker_Product__c = Obj2.id);
        insert Obj;
        
        Checklist__c Obj3 = new Checklist__c(Unique_Name__c = cr.Checklist_Name__c,Reference_ID__c = 'test');
        insert Obj3;
        
        Checklist_Items__c Obj4 = new Checklist_Items__c(Parent_Checklist__c = Obj3.id, Checklist__c = 'test',Reference_ID__c = 'Confirmation_Message', 
                                                         Retailer_Name__c=retAcc.Name, Section__c=retAcc.Name,
                                                        Checklist_Information__c='{!mdl.coupon.Amount__c} test {!mdl.coupon.Name} test {!mdl.coupon.Expiration_Date__c} test href=""');
        insert Obj4;
        
        Account custAcc = new Account();
        custAcc.Name = 'Test Customer'; 
        INSERT custAcc;
         
        Date sDate = Date.newInstance(2018, 3, 31);
        Date eDate = Date.newInstance(2019, 1, 1);   
        
        Coupon__c cop = new Coupon__c();
        cop.Name = '32132162132';
        cop.Retailer_Name__c = retAcc.Name;
        cop.Status__c = 'New';
        cop.Qualified_Product_List__c = Obj.Id;
        cop.Start_Date__c = sDate;
        cop.Amount__c=100;
        cop.Expiration_Date__c = eDate;
        INSERT cop;
        
        Customer__c cust = new Customer__c();
        cust.Account__c = custAcc.Id;
        cust.DSMTracker_Product__c=Obj2.Id;
        cop.Retailer_Name__c = retAcc.Name;
        INSERT cust;
        
        Coupon__c cop1 = new Coupon__c();
        cop1.Name = '32132162asd132';
        cop1.Retailer_Name__c = retAcc.Name;
        cop1.Status__c = 'New';
        cop1.Qualified_Product_List__c = Obj.Id;
        cop1.Start_Date__c = sDate;
        cop1.Amount__c=100;
        cop1.Expiration_Date__c = eDate;
        INSERT cop1;
        
        Coupon__c cop2 = new Coupon__c();
        cop2.Name = '32132162asd134';
        cop2.Retailer_Name__c = retAcc.Name;
        cop2.Status__c = 'New';
        cop2.Qualified_Product_List__c = Obj.Id;
        cop2.Start_Date__c = sDate;
        cop2.Amount__c=100;
        cop2.Expiration_Date__c = eDate;
        INSERT cop2;
        
        Coupon__c cop3 = new Coupon__c();
        cop3.Name = '32132162asd135';
        cop3.Retailer_Name__c = retAcc.Name;
        cop3.Status__c = 'New';
        cop3.Qualified_Product_List__c = Obj.Id;
        cop3.Start_Date__c = sDate;
        cop3.Amount__c=100;
        cop3.Expiration_Date__c = eDate;
        INSERT cop3;
        
        Coupon__c cop4 = new Coupon__c();
        cop4.Name = '32132162asd136';
        cop4.Retailer_Name__c = retAcc.Name;
        cop4.Status__c = 'New';
        cop4.Qualified_Product_List__c = Obj.Id;
        cop4.Start_Date__c = sDate;
        cop4.Amount__c=100;
        cop4.Expiration_Date__c = eDate;
        INSERT cop4;
        ApexPages.currentPage().getParameters().put('id',Obj2.id);
        
        dsmtEverySourceCouponsController ctr = new dsmtEverySourceCouponsController(); 
        List<Account> lst = new List<Account>();
        lst.add(custAcc);
        dsmtEverySourceCouponsUtil.testAcc = lst;
        List<Object> getters = new List<Object>{
        		ctr.mdl.currentPage,
                ctr.mdl.previousPage,
                ctr.mdl.stepErrors,
                ctr.mdl.hasError,
                    ctr.mdl.attempt,
                ctr.mdl.account,
                ctr.mdl.coupon,
                ctr.mdl.checkListConfigMap,
                ctr.mdl.cs,
                ctr.mdl.stateList,
                ctr.mdl.propertType,
                ctr.mdl.selectedPropType,
                    ctr.mdl.thermoReplaList,
                    ctr.mdl.retailerList,
                ctr.mdl.thermoInstList,
                ctr.mdl.selectedThemoInst,
                ctr.mdl.selectedThemoRepl,
                ctr.mdl.selectedRetailer,
                ctr.mdl.isEligible,
                ctr.mdl.confirmMessage,
                ctr.mdl.expirationDate,
                ctr.mdl.rating,
                ctr.mdl.allowedToContinue,
                ctr.mdl.refId, 
                ctr.mdl.procodeUrlMap,
                ctr.mdl.confMsgMap,
                    ctr.mdl.dsmtProduct
        };
		//ctr.init();
        ctr.mdl.selectedRetailer = retAcc.Name;
        ApexPages.currentPage().getParameters().put('stepToMove', '2');
        ctr.mdl.moveToStep();
        ctr.searchCustomer();
        ctr.fillInvalidCustomer();
        String msg = ctr.mdl.getErrorMessage(Obj4.Name, true, true);
        dsmtEverySourceCouponsController.saveBarcodeImage(String.valueOf(cop.Id),'test');
        dsmtEverySourceCouponsController.getUrlParameterValue('id');
        ctr.getDayOfMonthSuffix(3);
        ApexPages.currentPage().getParameters().put('copId',cop.id); 
        ApexPages.currentPage().getParameters().put('custId',cust.Id);
        dsmtEverySourceCouponsController ctr1 = new dsmtEverySourceCouponsController(); 
        ApexPages.currentPage().getParameters().put('quickC','true');
        ctr1.mdl.coupon = cop4;
        ctr1.custId = cust.Id;
        ctr1.updateCouponCreateCustomer(); 
        ctr1.createConfirmationMessage();
        ctr1.quickC = 'true';
        ctr1.updateQuickCoupon();
        ctr1.init();
        /*ctr.createCustomer();
        ctr.convertDateToReadableFormat(Date.today());*/
    }
}