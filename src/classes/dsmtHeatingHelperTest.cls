@isTest
private class dsmtHeatingHelperTest {
    
    @isTest static void test1()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       
        
        Test.startTest();

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        
        insert mstList;

        Set<Id> eaId = new Set<Id>();
        eaId.add(testEA.Id);

        //system.debug('mstList[0].Energy_Assessment__c>>>'+mstList[0].Energy_Assessment__c);
        //system.debug('eaId>>>'+eaId);

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

        bp.mstObj = bp.mst[0];
        bp.mechType = mechType;

        dsmtHeatingHelper.ComputeHeatingSystemTypeCommon();
        dsmtHeatingHelper.ComputeTotalHeatingSystemCapacity(bp);
        dsmtHeatingHelper.ComputeHeatingSystemFanOnKWHCommon(bp);
        dsmtHeatingHelper.ComputeHeatingSystemRunHrsCommon(bp);
        dsmtHeatingHelper.computeLoadBin(55.45,510);
        dsmtHeatingHelper.computeBinFrac(bp,true,32);
        dsmtHeatingHelper.computeBinFrac(bp,false,32);
        dsmtHeatingHelper.computeBinCompressorCap(65,88,56);
        dsmtHeatingHelper.ComputeHeatingSystemAirFlowChangedCommon(bp);
        dsmtHeatingHelper.percentToDecimal(null);
        dsmtHeatingHelper.ComputeHeatingSystemIsBackupHeatCommon(bp);
        dsmtHeatingHelper.ComputeHeatingSystemHasBackupHeatCommon(bp);
        dsmtHeatingHelper.findDFHPPrimaryHeat(bp);
        dsmtHeatingHelper.ComputeDFHPCutoffTempCommon(bp);
        dsmtHeatingHelper.ComputeDFHPPctLoadPrimaryHeatCommon(bp);
        dsmtHeatingHelper.ComputeDFHPPctLoadPrimaryHeatCommon(bp);
        dsmtHeatingHelper.ComputeDFHPPctLoadBackupHeatCommon(bp);
        dsmtHeatingHelper.ComputeHeatingSystemVentDamperCommon(bp);
        dsmtHeatingHelper.ComputeHeatingSystemHighSpeedBurnerCommon(bp);
        dsmtHeatingHelper.ComputeDFHPLoadFractionCommon(bp);

        bp.mstObj.IsBackupHeat__c=true;
        bp.mstObj.ActualEquipEffInEffUnits__c=250;      
        dsmtHeatingHelper.ComputeHeatingSystemDistributionEfficiencyCommon(bp);
        dsmtHeatingHelper.ComputeHeatingSystemCOPRatedCommon(bp);
        dsmtHeatingHelper.ComputeDFHPCutoffTempCommon(bp);
        dsmtHeatingHelper.findDFHPPrimaryHeat(bp);

        List<Decimal> binNetCOP = new List<decimal>();
        binNetCOP.add(43);
        binNetCOP.add(48);
        binNetCOP.add(50);
        binNetCOP.add(43);
        binNetCOP.add(48);
        binNetCOP.add(50);
        binNetCOP.add(43);
        binNetCOP.add(48);
        binNetCOP.add(50);
        binNetCOP.add(43);
        binNetCOP.add(48);
        binNetCOP.add(50);
        binNetCOP.add(50);

        List<Decimal> binFrac = new List<decimal>();
        binFrac.add(.56);
        binFrac.add(1);
        binFrac.add(.75);
        binFrac.add(.56);
        binFrac.add(1);
        binFrac.add(.75);
        binFrac.add(.56);
        binFrac.add(1);
        binFrac.add(.75);
        binFrac.add(.56);
        binFrac.add(1);
        binFrac.add(.75);
        binFrac.add(.75);

        dsmtHeatingHelper.computeBinCopFrac(binNetCOP, binFrac, false);
        dsmtHeatingHelper.computeBinNetCOP(binNetCOP,binFrac,0.88);

        Test.stopTest();
    }

    @isTest static void test2()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Boiler').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Boiler').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Baseboard Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Gravity Hot Air Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Gravity Hot Water Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Hydro Air Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Test.stopTest();
    }

    @isTest static void test3()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Electric Resistance').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Electric Resistance').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Electric Baseboard Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Test.stopTest();
    }

    @isTest static void test4()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Wood Stove').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Wood Stove').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Gravity Hot Air Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Test.stopTest();
    }

    @isTest static void test5()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Wall Furnace').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Wall Furnace').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Gravity Hot Air Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Test.stopTest();
    }


    @isTest static void test6()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating+Cooling').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('GSHP').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('GSHP').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('GSHP Cooling').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Test.stopTest();
    }

    @isTest static void test7()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating+Cooling').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('ASHP').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('ASHP').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('ASHP Cooling').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Set<Id> eaId = new Set<Id>();
        eaId.add(testEA.Id);

        //system.debug('mstList[0].Energy_Assessment__c>>>'+mstList[0].Energy_Assessment__c);
        //system.debug('eaId>>>'+eaId);

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

        bp.mstObj = bp.mst[0];
        bp.mechType = mechType;

        dsmtHeatingHelper.ComputeHeatingSytemASHPHasDemandDefrostControlCommon(bp);

        bp.mstObj.ASHPHasEHtOnBelow__c  = true;
        bp.mstObj.ASHPHasStripOutdoorLockout__c  = true;
        bp.mstObj.ASHPHasStripOn__c  = true;
        bp.mstObj.Capacity__c = 24000;
        bp.mstObj.LoadFraction__c = 1;
        bp.mstObj.HeatingSystemType__c = 'ASHP';
        bp.mstObj.ASHPAdjCap47__c  = 56;
        bp.mstObj.ASHPCap17__c  = 36;
        bp.mstObj.ASHPInput47__c  = 6;
        bp.mstObj.ASHPInput17__c  = 22;
        bp.mstObj.DistributionEfficiency__c = .85;

        dsmtHeatingHelper.ComputeHeatingSystemASHPCOPNetCommon(bp);
        dsmtDistributionSystemTriggerHandler.UpdateDSHeatingSystem(bp,bp.mstObj);
        dsmtDistributionSystemTriggerHandler.UpdateDSCoolingSystem(bp,bp.mstObj);

        bp.mstObj = bp.mst[3];
        bp.mechType = mechType;     
        dsmtDistributionSystemHelper.ComputeIsWaterDistributionSystemCommon(bp);
        dsmtDistributionSystemHelper.ComputeIsElectricDistributionSystemCommon(bp);
        dsmtDistributionSystemHelper.ComputeIsAirDistributionSystemCommon(bp);
        dsmtDistributionSystemHelper.ComputeIsAirDistributionSystemWithFanCommon(bp);
        dsmtDistributionSystemHelper.ComputeIsDuctDistributionSystemCommon(bp);
        dsmtDistributionSystemHelper.ComputeTotalSupplyDuctsAreaCommon(bp);
        dsmtDistributionSystemHelper.ComputeTotalReturnDuctsAreaCommon(bp);
        dsmtDistributionSystemHelper.ComputeDistributionSystemUnitBlowerCommon(bp);

        dsmtDHWHelper.ComputeCAECommon(bp);
        dsmtDHWHelper.ComputeDHWTankWrapCommon(bp);
        //dsmtDHWHelper.LookupDHWFuelTypesCommon(bp);
        dsmtDHWHelper.ComputeDHWSolarSolarTankVolumeCommon(bp.mstObj);

        bp.Location = 'UnderSlab';
        dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);

        bp.Location = 'ExteriorWalls';
        dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);

        bp.Location = 'ManufacturedHomeBellyLeakyAverageInsulation';
        dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);

        bp.Location = 'ManufacturedHomeBellyWellInsulated';
        dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);

        bp.Location = 'RoofDeck';
        dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);

        dsmtEnergyConsumptionHelper.getFractionDays(bp,'IntensH');
        dsmtEnergyConsumptionHelper.getFractionDays(bp,'IntensC');
        dsmtEnergyConsumptionHelper.getFractionDays(bp,'IntensSh');
        dsmtEnergyConsumptionHelper.AddFuelUseForCurrentBuildingProfileObject(bp);

        /*bp.applObj = bp.aplList[4];
        //dsmtEnergyConsumptionHelper.findCorrespondingDryer(bp);
        //dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualFuelConsumptionCommon(bp);

        bp.waterObj = bp.waterList[0];
        bp.waterObj.Fuel_Type__c = 'Electricity';
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainPlugCommon(bp, 'C');
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainPlugCommon(bp, 'H');
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainPlugCommon(bp, 'CLat');

        bp.waterObj.Fuel_Type__c = 'Natural Gas';
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainGasCommon(bp, 'C');
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainGasCommon(bp, 'H');
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainGasCommon(bp, 'CLat');

        bp.waterObj.Fuel_Type__c = 'Oil';
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainOtherCommon(bp, 'C');
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainOtherCommon(bp, 'H');
        dsmtEnergyConsumptionHelper.ComputeWaterFixtureIndoorGainOtherCommon(bp, 'CLat');*/

        Test.stopTest();
    }

    @isTest static void test9()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Hot Water').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
        mechSubType.Fuel_Type__c = 'Oil';
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
        mechSubType.Fuel_Type__c = 'Electricity';
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Set<Id> eaId = new Set<Id>();
        eaId.add(testEA.Id);

        //system.debug('mstList[0].Energy_Assessment__c>>>'+mstList[0].Energy_Assessment__c);
        //system.debug('eaId>>>'+eaId);

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

        bp.mstObj = bp.mst[0];
        bp.mechType = mechType;

        Test.stopTest();
    }

    @isTest static void test10()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Hot Water').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('On Demand').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('On Demand').getRecordTypeId();
        mstList.add(mechSubType);
        
        Test.startTest();

        insert mstList;

        Set<Id> eaId = new Set<Id>();
        eaId.add(testEA.Id);

        //system.debug('mstList[0].Energy_Assessment__c>>>'+mstList[0].Energy_Assessment__c);
        //system.debug('eaId>>>'+eaId);

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

        bp.mstObj = bp.mst[0];
        bp.mechType = mechType;

        Test.stopTest();
    }

    @isTest static void test11()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
        Mechanical__c mech = new Mechanical__c();       
        mech.Energy_Assessment__c = testEA.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating+DHW').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = testEA.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Boiler + Indirect Storage Tank').getRecordTypeId();
        insert mechType;

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Boiler').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Indirect Storage Tank').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = testEA.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Baseboard Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;

        Test.stopTest();
    }

    @isTest static void test12()
    {
        Test.startTest();
        
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtEAModel.setupAssessment();

        Test.stopTest();
    }   
}