public class ReturnEmergencyScheduled implements Database.batchable<SObject>, schedulable{  
     
     public ReturnEmergencyScheduled (){
         
     }
     
   public database.querylocator start(Database.batchableContext batchableContext){
         String sQuery = 'select id,Trade_Ally_Email__c,Inspection_Request__r.inspection_result__c from Review__c where (Have_you_contacted_the_Customer__c !=  \'Y\' AND Is_the_return_scheduled__c !=  \'Y\' AND Inspection_Reminder_Sent__c != null AND Inspection_Request__r.inspection_result__c == \'Return Emergency\')  ';
         System.debug('===sQuery===='+sQuery);
         return Database.getQueryLocator(sQuery);
    }
    public void execute(Database.BatchableContext bc, List<Review__c> sObjs){ 
        
            for(Review__c rev : sObjs){
                system.debug('--rev.Id---'+rev.Id);
                
                   List<emailtemplate > etList = [Select id from emailtemplate where developername = 'Automated_Mail_On_Review'];
                        
                    List<OrgWideEmailAddress >lstOrgWideEmailAddress =[select id, Address, DisplayName from OrgWideEmailAddress
                                                          limit 1]; 
                                                          
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    email.setTargetObjectId(rev.Trade_Ally_Email__c);
                    if(Test.isrunningTest()){
                        etList = [Select id from emailtemplate limit 1];
                    }
                    email.setTemplateId(etList.get(0).Id);
                    if(!test.isrunningtest()){
                        email.setOrgWideEmailAddressId(lstOrgWideEmailAddress[0].id);

                    Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {email};
                    Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                
                    Message.CreateMessage(lstOrgWideEmailAddress[0].Address,rev.Trade_Ally_Email__c,'','',rev.Id,email.getSubject(),email.getHtmlBody(),'Automated Email');
                    }
                  }
            }
        
    public void finish(Database.BatchableContext batchableContext) {

    }
     
     public void execute(SchedulableContext sc) {
         ReturnEmergencyScheduled cntrl = new ReturnEmergencyScheduled();
         database.executebatch(cntrl,1);
         
         
     }
 
 }