public class dsmtNewInvoiceLineItemCntrl {
    
    public String invid{get;set;}
    public String invoiceId{get;set;}
    public Map<Id,Recommendation__c> projectMap{get;set;}
    public List<Invoice_Line_Item__c> invoiceLineItem{get;set;}
    public Invoice__c invoice{get;set;}
    
    public dsmtNewInvoiceLineItemCntrl() {
        invid = ApexPages.currentPage().getParameters().get('id');
        
        List<Invoice__c> invoicelist  = [Select id,Name, Project__c, Contact_First_Name__c, Contact_Last_Name__c, Billing_Street__c, Billing_City__c, Billing_Country__c, Billing_State_Province__c, Billing_Zip_Postal_Code__c From Invoice__c Where Id= :invid]; 
        if(invoicelist.size() > 0) {
                Invoice__c inv = invoicelist.get(0);
                invoice = inv;
                invoiceId = inv.id;
                String projectId = inv.Project__c;
            
                List<Recommendation__c> recomlist = [Select id, Name, Description__c,Recommendation_Description__c, Quantity__c, Total_Project_Cost__c, Status__c, Units__c, Unit_Cost__c, Location__c, (Select id from Invoice_Line_Items__r) From Recommendation__c Where Recommendation_Scenario__c = :projectId]; 
                invoiceLineItem = new List<Invoice_Line_Item__c>();
                projectMap = new Map<Id,Recommendation__c>();
                for(Recommendation__c pl : recomlist) {
                    if(pl.Invoice_Line_Items__r != null && pl.Invoice_Line_Items__r.size() > 0) {
                        continue;
                    }
                    projectMap.put(pl.id,pl);
                    Invoice_Line_Item__c il = new Invoice_Line_Item__c();
                  //  il.Name = inv.Name + '-' + pl.Name;  //Invoice Line Item Name changed to Auto Number
                    il.Recommendation__c = pl.id;
                    il.Invoice__c = invoiceId;
                    il.Unit_Cost__c = pl.Unit_Cost__c;
                    il.Units__c = pl.Units__c;
                    // il.Actual_Project_Cost__c = pl.Total_Project_Cost__c;
                    il.Quantity_Used__c = pl.Quantity__c;
                    
                    invoiceLineItem.add(il);
                }
        }
        
        
    }   

    public PageReference saveItems() {
        System.debug('Save Items Called');
        List<Invoice_Line_Item__c> saveItems = new List<Invoice_Line_Item__c>();
        for(Invoice_Line_Item__c il : invoiceLineItem) {
            if(il.Unit_Cost__c !=null && il.Units__c != null && il.Units__c != '') {
                il.Actual_Project_Cost__c = il.Unit_Cost__c * Decimal.valueOf(il.Units__c);
            }
            
            if(il.Actual_Project_Cost__c  != null && il.Actual_Project_Cost__c>0) {
                System.debug('Invoked to add.');
                saveItems.add(il);
            }
        }
        if(saveItems.size() > 0){
            insert saveItems;
        }
        PageReference p = new PageReference('/'+invid);
        p.setRedirect(true);
        return p;
    }
}