public with sharing class AddReviewRecommendationExtn {

    public string recommXml {get;set;}
    public string prjId{get;set;}
    public Review__c rev{get;set;}
    public string dataSave{get;set;}
    public String revId{get;set;}
    public String reviewType {get;set;}
    public decimal subTotal {get;set;}
    public decimal utilityIncentive {get;set;}
    public decimal customerContribution {get;set;}
    public decimal customerDeposit {get;set;}
    public decimal remainingCustomerContribution {get;set;}
    public string EAName{get;set;}
    public string TAName{get;set;}
    public boolean showApproved{get;set;}
    public string assessId{get;set;}
    public boolean fromController;
    public String reviewId;
    public boolean disabledProjectBtn {get;set;}
    public String proposalType {get;set;}
    
    List<Recommendation__c> recomList;
    
    List<Recommendation__c> recommList;
    List<Change_Order_Line_Item__c> cliList;
    
    public List<SelectOption> projList {get;set;}
    
    public AddReviewRecommendationExtn (){
        fromController = true;
        init();
    }
    
    public AddReviewRecommendationExtn(ApexPages.StandardController controller) {
        fromController = true;
        init();
    }
    
    private void init()
    {
        showApproved = false;
        rev = new Review__c();
        prjId = ApexPages.currentPage().getParameters().get('prjid');
        revId = ApexPages.currentPage().getParameters().get('id');
        assessId = ApexPages.currentPage().getParameters().get('assessId');
        
        checkReviewType();
        loadRecommendations();
        
        List<User> userList = [select id,profile.Name from user where id =: userinfo.getuserId()];
        
        if(userList != null && userList.get(0).Profile.Name == 'DSMTracker Back Office'){
            showApproved  = true;
        }
        
        set<String> projectIds = new set<String>();
        
        for(Project_Review__c pr : [select id, Project__r.Name, Project__c, Project__r.Category__c, Project__r.Type__c
                                         from Project_Review__c 
                                         where Review__c =: revId 
                                         and Review__c != null
                                         and Project__c != null])
        {
            projectIds.add(pr.Project__c);
            if(pr.Project__r.Type__c == 'Insulation')
                proposalType = 'Air Sealing';
            else if(pr.Project__r.Type__c == 'Air Sealing')
                proposalType = 'Insulation';
        }
    
        if(projectIds.size() > 1)
            disabledProjectBtn = true;
        else
            disabledProjectBtn = false;
    }
    
    private void checkReviewType()
    {
        List<Review__C> reviewList = [select Id, RecordType.Name,Trade_Ally_Account__r.Name, Energy_Assessment__r.Name,Status__c
                                      from Review__C 
                                      where Id =: revId 
                                      limit 1];
        if(reviewList.size() > 0){
            reviewType = reviewList[0].RecordType.Name; 
            if(reviewList[0].Status__c == 'Failed Completion Review'){
                reviewType = 'Failed Billing Review';
            }
            EAName = reviewList[0].Energy_Assessment__r.Name;
            TAName = reviewList[0].Trade_Ally_Account__r.Name;
        }
    }
    
    public Pagereference createReviewRec(){
        
        String projId = ApexPages.currentPage().getParameters().get('projid');
        
        if(revId != null)
        {
            loadChangeOrderReview();  
        }
        else if(projId != null)
        {    
            List<Project_Review__c> prList = [select id, Review__c, Project__c 
                                              from Project_Review__c 
                                              where Project__c =: projId
                                              and Review__c != null
                                              and (Review__r.RecordType.Name = 'Work Scope Review'
                                              or Review__r.RecordType.Name = 'Work Scope Review Manual')];
            
            if(prList.size() > 0)
                return new PageReference('/apex/AddRemoveReviewRecommendation?id='+prList[0].Review__c+'&projid='+projId).setRedirect(true);
        }
        
        return null;
    }
    
    private set<String> fetchProjectIdSet(Id reviewId)
    {
        set<String> projectIdSet = new Set<String>();
        
        List<Project_Review__c> revList = [select id, Name, Review__c, Project__c 
                                           from Project_Review__c 
                                           where Review__c =: reviewId
                                           and Review__c != null];
        for(Project_Review__c pr : revList)
        {
            projectIdSet.add(pr.Project__c);
        }
        
        return projectIdSet;
    }
    
    public void CreateReviewRecord(){
        if(revId != null)
        {
            List<Review__c> reviewList = [select Id,Name,Project__c,Status__c,Work_Scope_Review__c,Trade_Ally_Account__r.Name,
                                          Energy_Assessment__r.Name 
                                          from Review__c 
                                          where (Work_Scope_Review__c = :revId  or Billing_Review__c =: revId)
                                          and Type__c = 'Change Order Review'
                                          and (Status__c = 'New' or Status__c = 'Rejected')
                                          limit 1];
            
            if(reviewList.size() > 0)
            {
                rev = reviewList[0];
            }
            else
            {
                loadChangeOrderReview();
                upsert rev; 
                if(fromController)
                    reviewId = rev.Id;
            }
        }
    }
    
    private void loadChangeOrderReview()
    {
        List<Review__C> lstPrj = [select Id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                    Energy_Assessment__r.Trade_Ally_Account__r.Email__c,First_Name__c,
                                    Last_Name__c,Phone__c,Email__c,Address__c,City__c,State__c,
                                    Zipcode__c,Trade_Ally_Account__c,Type__c  from Review__C 
                                    where Id =: revId limit 1]; 
        rev.Status__c = 'New';
        Id devRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();

        List<Dsmtracker_Contact__c> dsmtList = [select id,Dsmtracker_Contact__c,Trade_Ally_Account__c from Dsmtracker_Contact__c where Portal_User__c =: userinfo.getUSerID()
                                                    and Trade_Ally_Account__c  != null];
        if(dsmtList.size() > 0){
            rev.Trade_Ally_Account__c = dsmtList.get(0).Trade_Ally_Account__c;
        } 
        if(lstPrj.size() > 0){                                  
            if(lstPrj.get(0).Energy_Assessment__c != null)    
                rev.Energy_Assessment__c = lstPrj.get(0).Energy_Assessment__c;
            
            rev.Type__c = 'Change Order Review';
            rev.First_Name__c = lstPrj.get(0).First_Name__c;
            rev.Last_Name__c = lstPrj.get(0).Last_Name__c;
            rev.Phone__c = lstPrj.get(0).Phone__c;
            rev.Email__c = lstPrj.get(0).Email__c;
            rev.Address__c = lstPrj.get(0).Address__c;
            rev.City__c = lstPrj.get(0).City__c;
            rev.State__c = lstPrj.get(0).State__c;
            rev.Zipcode__c = lstPrj.get(0).Zipcode__c;
            if(lstPrj.get(0).Type__c == 'Billing Review'){
                rev.Billing_Review__c = lstPrj.get(0).Id;
            }else{
                rev.Work_Scope_Review__c = lstPrj.get(0).Id;
            }
        }
        if(lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c != null)
            rev.Trade_Ally_Email__c = lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c;
        
        rev.RecordTypeId =  devRecordTypeId;
    }
    
    public void loadRecommendations()
    {
        projList = new List<SelectOption>();
        projList.add(new SelectOption('All', 'All Projects'));
        
        remainingCustomerContribution = customerDeposit = utilityIncentive = subTotal = customerContribution = 0;
        
        recommXml = '<Recommendations>';
        
        Set<String> projIds = new Set<String>();
        
        string revId1 = ApexPages.currentPage().getParameters().get('id');
        
        if(revId1  != null){
            projIds = fetchProjectIdSet(revId1);
        }else{
            string projId = ApexPages.currentPage().getParameters().get('projid');
            projIds.add(projId);
        }
        
        Set<Id> prjIds = new Set<Id>();
        map<string, decimal> projCollectedMap = new map<string, decimal>();
        
        String projects = ApexPages.currentPage().getParameters().get('projects');
        
        if(projects != null && projects != 'All'){
            projIds = new Set<string>();
            projIds.add(projects);
        }
        
        system.debug('projIds :::::' + projIds);
        
        Map<Id,Decimal> changeOrderMap = new Map<Id,Decimal>();
        
        if(projIds != null && projIds.size() > 0){
            String query = 'select ' + sObjectFields('Recommendation__c') +' Recommendation_Scenario__r.Total_Collected__c, Recommendation_Scenario__r.Total_CustomerIncentive__c, Recommendation_Scenario__r.Customer_Cost__c, Id,DSMTracker_Product__r.Name,Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Type__c,Recommendation_Scenario__r.Status__c,Recommendation_Scenario__r.Installed_Date__c,Energy_Assessment__r.Primary_Provider__c,Dsmtracker_Product__r.EMHome_EMHub_PartID__c,(Select id,Project__c,Recommendation__c,Utility_Incentive_Share__c, Final_Recommendation_Incentive__c,Proposal__r.Current_Incentive__c,proposal__r.Final_Proposal_Incentive__c,proposal__r.Barrier_Incentive_Amount__c from Proposal_Recommendations__r where Project__c =:projIds), (select ' + sObjectFields('Change_Order_Line_Item__c') +' Id from Change_Order_Line_Items__r) from Recommendation__c where Recommendation_Scenario__c in :projIds and Recommendation_Scenario__c  != null order by CreatedDate desc limit 300 ';
            
            Set<Id> recId = new Set<Id>();
            
            recomList = database.query(query);
            
            system.debug('recomList :::::' + recomList);
            
            if(fromController)
            {
                recommList = new List<Recommendation__c>();
                cliList = new List<Change_Order_Line_Item__c>();
            }
                                
            for (Recommendation__c ir: recomList) {
               // recId.add(ir.Id);
                changeOrderMap.put(ir.id,ir.Change_Order_Quantity__c);
                
                if(fromController)
                {
                    Recommendation__c cloneRecom = ir.clone();
                    cloneRecom.id = ir.Id;
                    recommList.add(cloneRecom);
                    
                    if(ir.Change_Order_Line_Items__r.size() > 0)
                        cliList.addAll(ir.Change_Order_Line_Items__r);
                }
            }
            
            Set<String> proposalIds = new Set<String>();
            for (Recommendation__c ir: recomList) {
                
                if(ir.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(ir.Recommendation_Scenario__c + '~~~' + ir.Recommendation_Scenario__r.Name, ir.Recommendation_Scenario__r.Total_Collected__c);
                
                recommXml += '<Recommendation>';
                recommXml += '<Checked>' + false + '</Checked>';
                recommXml += '<ProjectId>' + ir.Recommendation_Scenario__c + '</ProjectId>';
                recommXml += '<ProjectName><![CDATA[<a href="/'+ir.Recommendation_Scenario__c+'" target="_blank">' + checkStringNull(ir.Recommendation_Scenario__r.Name) + '</a>]]></ProjectName>';
                recommXml += '<RecommendationId>' + ir.Id + '</RecommendationId>';
                recommXml += '<RecommendationName><![CDATA[' + checkStringNull(ir.DSMTracker_Product__r.Name) + ']]></RecommendationName>';
                
                if(ir.Quantity__c == null || ir.Quantity__c == 0)
                    recommXml += '<Quantity>0</Quantity>';
                else
                    recommXml += '<Quantity><![CDATA[' + ir.Quantity__c + ']]></Quantity>';
                
                if(changeOrderMap.containsKey(ir.Id) && changeOrderMap.get(ir.Id) != null)
                    recommXml += '<ChangeOrderQuantity><![CDATA[' + changeOrderMap.get(ir.Id)+ ']]></ChangeOrderQuantity>';
                else if(ir.IsChangeOrder__c && ir.Change_Order_Quantity__c != null){
                    recommXml += '<ChangeOrderQuantity><![CDATA[' + ir.Change_Order_Quantity__c + ']]></ChangeOrderQuantity>';
                }else     
                    recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
                
                if(changeOrderMap.get(ir.Id) != null  && ir.Unit_Cost__c != null){
                    recommXml += '<PartCost><![CDATA[' + changeOrderMap.get(ir.Id) * ir.Unit_Cost__c+ ']]></PartCost>';
                }else if(ir.Change_Order_Quantity__c != null && ir.Unit_Cost__c != null){
                    recommXml += '<PartCost><![CDATA[' + ir.Change_Order_Quantity__c * ir.Unit_Cost__c+ ']]></PartCost>';
                }else{ 
                    if(ir.Part_Cost__c != null)
                        recommXml += '<PartCost><![CDATA[' + ir.Part_Cost__c+ ']]></PartCost>';
                    else
                        recommXml += '<PartCost></PartCost>';
                }
                recommXml += '<Reason><![CDATA[' + checkStringNull(ir.Change_Order_Reason__c)+ ']]></Reason>';
                
                recommXml += '<UtilityIncentive><![CDATA[' + ir.Total_Recommended_Incentive__c+ ']]></UtilityIncentive>';
                recommXml += '<PartId><![CDATA[' + checkStringNull(ir.Dsmtracker_Product__r.EMHome_EMHub_PartID__c)+ ']]></PartId>';
                recommXml += '<UtilityBeingCharged><![CDATA[' + checkStringNull(ir.Energy_Assessment__r.Primary_Provider__c)+ ']]></UtilityBeingCharged>';
                
                if(ir.Installed_Date__c == null)
                    recommXml += '<InstallDate></InstallDate>';
                else
                    recommXml += '<InstallDate><![CDATA[' + ir.Installed_Date__c + ']]></InstallDate>';
                
                if(ir.Invoice_Type__c == null)
                    recommXml += '<InvoiceType></InvoiceType>';
                else
                    recommXml += '<InvoiceType><![CDATA[' + ir.Invoice_Type__c + ']]></InvoiceType>';
                
                if(ir.Installed_Quantity__c == null)
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                else
                    recommXml += '<InstalledQuantity><![CDATA[' + ir.Installed_Quantity__c + ']]></InstalledQuantity>';
                
                if(ir.Status__c != null)
                    recommXml += '<Status><![CDATA[' + ir.Status__c + ']]></Status>';
                else
                    recommXml += '<Status></Status>';
                
                recommXml += '</Recommendation>';
                
                system.debug('--subTotal --'+subTotal );
                system.debug('--ir.Part_Cost__c---'+ir.Part_Cost__c);
                /*if(changeOrderMap.get(ir.Id) != null && changeOrderMap.get(ir.Id).Change_Order_Quantity__c != null && ir.Unit_Cost__c != null){
                    subTotal += changeOrderMap.get(ir.Id).Change_Order_Quantity__c * ir.Unit_Cost__c;
                }else{
                    if(ir.Part_Cost__c != null)
                        subTotal += ir.Part_Cost__c;
                }*/
                 if(ir.Unit_Cost__c != null && changeOrderMap.get(ir.id) != null){
                    subTotal += ir.Unit_Cost__c * changeOrderMap.get(ir.id);
                 } else if(ir.Unit_Cost__c != null && ir.Quantity__c != null){
                    subTotal += ir.Unit_Cost__c * ir.Quantity__c;
                 }
                  Proposal_Recommendation__c pr = null;

                    if(!ir.Proposal_Recommendations__r.isEmpty()) { 
                        pr = ir.Proposal_Recommendations__r.get(0);
                    }
                    
                     if(pr != null){
                        if(proposalIds.add(pr.proposal__c)){
                            System.debug('--Final_Proposal_Incentive'+pr.Proposal__r.Final_Proposal_Incentive__c);
                            utilityIncentive += pr.Proposal__r.Final_Proposal_Incentive__c ;    
                        }
                    }
                
                system.debug('--subTotal --'+subTotal );
            }
        }
        
        
        customerContribution = subTotal - utilityIncentive;
        
        if(customerContribution != null){
            customerContribution.setScale(2);
        }
        
        for(String s : projCollectedMap.Keyset())
        {
            customerDeposit += projCollectedMap.get(s);
            
            if(s.split('~~~').size() > 1){
                projList.add(new SelectOption(s.split('~~~')[0], s.split('~~~')[1]));
            }
        }
        
        remainingCustomerContribution = (customerContribution - customerDeposit);
        
        recommXml += '</Recommendations>';
        
        system.debug('--recommXml ---'+recommXml );
    }
    
    public Proposal__c saveProposal()
    {
        Proposal__c newProposal = new Proposal__c(Name = (proposalType == 'Insulation' ? 'Weatherization' : proposalType),
                                                  Name_Type__c = (proposalType == 'Insulation' ? 'Weatherization' : proposalType),
                                                  Type__c = proposalType,
                                                  Status__c = 'Signed',
                                                  Signature_Date__c = date.today());
                                                  
        newProposal.Energy_Assessment__c = assessId;
            
        List<Energy_Assessment__c> assList = [select id, No_Cap_Insulation__c from Energy_Assessment__c
                                              where id =: assessId
                                              limit 1];
        
        if(assList.size() > 0)
            newProposal.No_Cap__c = assList[0].No_Cap_Insulation__c;
            
        upsert newProposal;
        
        return newProposal;
    }
    
    public void addRecommendations()
    {   
        Proposal__c newProposal = saveProposal();
        system.debug('newProposal :::::' + newProposal.Id);
        
        list<Proposal_Recommendation__c> newPropRecommList = new list<Proposal_Recommendation__c>();
        Map<id, Recommendation__c> rmap = new map<id,Recommendation__c>([select id,Cost__c, 
            Incentive__c, Annual_Energy_Savings__c,DSMTracker_Product__C,Units__c
            from Recommendation__c
            where id in: recomList]);
            
        for(id recomId : rmap.keyset())
        {
            Recommendation__c r = rmap.get(recomId);
            newPropRecommList.add(new Proposal_Recommendation__c(Recommendation__c = r.Id,
                                                                 Proposal__c = newProposal.Id,
                                                                 Cost__c = r.Cost__c,
                                                                 Incentive__c = r.Incentive__c,
                                                                 Annual_Energy_Savings__c = r.Annual_Energy_Savings__c));
        }
        
        system.debug('newPropRecommList :::::' + newPropRecommList.size());
        //if(newPropRecommList.size() > 0)
        //    insert newPropRecommList;

        if(newProposal.Status__c == 'Signed'){
            newProposal.Created_Signed_Proposal__c = true;    
        }
        
        upsert newProposal;
        
        List<Recommendation_Scenario__c> projectList = [select id from Recommendation_Scenario__c where Proposal__c =: newProposal.Id];
        
        if(projectList.size() > 0)
        {
            List<Project_Review__c> prList = new List<Project_Review__c>();
            
            for(Recommendation_Scenario__c rs : projectList)
            {
                prList.add(new Project_Review__c(Project__c = rs.Id,
                                                 Review__c = revId));    
            }
            
            insert prList;
        }
        
        //if(newPropRecommList.size() > 0){
        //   update newPropRecommList;
        //}
    }
    
    public string checkStringNull(string str){
        return str == null?'':str;
    }
    
    public void saveProjects()
    {
        saveProjId();
    }
    
    public void cancelProjects()
    {
        Set<String> projIds = new Set<String>();
        
        string revId1 = ApexPages.currentPage().getParameters().get('id');
        
        if(revId  != null){
            projIds = fetchProjectIdSet(revId1);
        }else{
            string projId = ApexPages.currentPage().getParameters().get('projid');
            projIds.add(projId);
        }
        
        Set<Id> prjIds = new Set<Id>();
        map<string, decimal> projCollectedMap = new map<string, decimal>();
        
        String projects = ApexPages.currentPage().getParameters().get('projects');
        
        if(projects != null && projects != 'All'){
            projIds = new Set<string>();
            projIds.add(projects);
        }
        
        Map<Id,Decimal> changeOrderMap = new Map<Id,Decimal>();
    
        if(projIds != null && projIds.size() > 0){
            List<Recommendation__c> deleteList = [select Id, 
                                                 (select id from Change_Order_Line_Items__r) from Recommendation__c 
                                                  where Recommendation_Scenario__c in :projIds 
                                                  and Recommendation_Scenario__c  != null
                                                  and id not in : recommList
                                                  order by CreatedDate desc limit 300];
            
            List<Change_Order_Line_Item__c> cliDeleteList = new List<Change_Order_Line_Item__c>();
            
            if(deleteList.size() > 0){
                for(Recommendation__c r : deleteList)
                {
                    if(r.Change_Order_Line_Items__r.size() > 0)
                        cliDeleteList.addAll(r.Change_Order_Line_Items__r);
                }
                
                if(cliDeleteList.size() > 0)
                    delete cliDeleteList;
                
                delete deleteList;
            }
        }
        
        if(reviewId != null)
            delete new Review__c(id = reviewId);
        
        dsmtRecommendationHelper.disableRolluptriggeronRec = true;
        if(cliList.size() > 0)
        {
            undelete cliList;
            update cliList;
        }
        
        List<Change_Order_Line_Item__c> cliDeleteList = [select id from Change_Order_Line_Item__c where id not in : cliList
                                                         and Recommendation__c in : recommList];
                                                         
        if(cliDeleteList.size() > 0)
            delete cliDeleteList;
        
        RecommendationTriggerHandler.preventRecursion = true;
        
        if(recommList.size() > 0){
            Map<id, Recommendation__c> recUpdateMap  = new Map<id, Recommendation__c>([select Id, 
                                                                                      (select id from Change_Order_Line_Items__r) from Recommendation__c 
                                                                                       where id in : recommList
                                                                                       order by CreatedDate desc limit 300]);
            for(Recommendation__c r : recommList)
            {
                if(recUpdateMap.containskey(r.id) && recUpdateMap.get(r.Id).Change_Order_Line_Items__r.size() == 0)
                {
                    r.Change_Order_Quantity__c = null;
                    r.Change_Order_Reason__c  = null;
                }
            }
            update recommList;
        }
    }
    
    public void saveProjId(){
        
        CreateReviewRecord();
        
        List<Recommendation__c> lstRec = new List<Recommendation__c>();
        List<Change_Order_Line_Item__c> lstChangeOrderItems = new List<Change_Order_Line_Item__c >();
        
        
        if(dataSave != null && dataSave != ''){
            map<String, String> recQMap = new map<String, String>();
            list<String> splitedData = dataSave.split(';');
            
            for(string s: splitedData){
                //Ticket DSST-10067 Fix
                //Added by Prasanjeet Sharma
                if(s.split('-').size() == 2)
                    recQMap.put(s.split('-')[0], s.split('-')[1]);
            }
            
            List<Recommendation__c> recomList = [select id, Name, Recommendation_Scenario__c, Recommendation_Description__c, Quantity__c, Units__c,
                                                 isChecked__c, Recommendation_Scenario__r.Name, Inspected_Quantity__c, Change_Order_Quantity__c,
                                                 Change_Order_Reason__c,IsChangeOrder__c 
                                                 from Recommendation__c 
                                                 where id in : recQMap.keyset()];
                                                 
            Set<Id> recId = new Set<Id>();
            Map<Id, Change_Order_Line_Item__c> changeOrderMap = new Map<Id, Change_Order_Line_Item__c>();
            
            if(recId.size() > 0){
                List<Review__c> changeRevList = [select id 
                                                 from Review__c 
                                                 where (Billing_Review__c =: revId
                                                 OR Work_Scope_Review__c =: revId) 
                                                 and RecordType.Name = 'Change Order Review'
                                                 and Status__c = 'New'
                                                 order by CreatedDate Desc];
                
                if(changeRevList  != null && changeRevList.size() > 0){
                    
                    Set<Id> crId = new Set<Id>();
                    
                    for( Review__c c : changeRevList){
                        crId.add(c.Id);
                    }
                
                    List<Change_Order_Line_Item__c> chageorderList = [select id,Change_Order_Quantity__c,Recommendation__c from Change_Order_Line_Item__c
                                                                      where Recommendation__c in : recId 
                                                                      and (Billing_Review__c =: revId
                                                                      OR Review__c =: revId)
                                                                      and Change_Order_Review__c =: changeRevList.get(0).Id
                                                                      and Status__c = 'New'
                                                                      order by CreatedDate Desc];
                                                                      
                    for(Change_Order_Line_Item__c c : chageorderList){
                        if(changeOrderMap.get(c.Recommendation__c) == null && c.Change_Order_Quantity__c != null){
                            changeOrderMap.put(c.Recommendation__c, c);
                        }
                    }
                }
            }
            
            List<Change_Order_Line_Item__c> colList = [ select id from Change_Order_Line_Item__c where Change_Order_Review__c =: rev.Id];
            
            if(colList != null && colList.size() > 0){
                delete colList;
            }
                                                 
            for(Recommendation__c r : recomList){
                
                system.debug('--r.IsChangeOrder__c---'+r.IsChangeOrder__c);
                Change_Order_Line_Item__c rec = new Change_Order_Line_Item__c();
                
                List<String> splitedQnty = recQMap.get(r.Id).split(',');
                
                decimal changeOrderQn = r.Change_Order_Quantity__c;
                
                if(changeOrderMap.containsKey(r.Id))
                    changeOrderQn = changeOrderMap.get(r.Id).Change_Order_Quantity__c;
                
                try{
                    if(decimal.valueof(splitedQnty[0]) != decimal.valueof(splitedQnty[2])){
                       
                        rec.Recommendation__c = r.Id;
                        
                        if(changeOrderMap.containsKey(r.Id))
                            rec.Id = changeOrderMap.get(r.Id).Id;
                        
                        if(splitedQnty != null && splitedQnty.size() > 0 && splitedQnty[0] != null){
                            rec.Change_Order_Quantity__c = splitedQnty[0] == null ? null: decimal.valueof(splitedQnty[0]);
                            r.Change_Order_Quantity__c = splitedQnty[0] == null ? null: decimal.valueof(splitedQnty[0]);
                        }
                
                        if(splitedQnty != null && splitedQnty.size() > 2 && splitedQnty[2] != null){
                            rec.Original_Quantity__c     = splitedQnty[2] == null ? null: decimal.valueof(splitedQnty[2]);
                        }
                
                        if(splitedQnty != null && splitedQnty.size() > 1 && splitedQnty[1] != null){
                            rec.Change_Order_Reason__c   = splitedQnty[1];
                            r.Change_Order_Reason__c   = splitedQnty[1];
                        }
                                            
                        rec.Review__c = revId;
                        rec.Change_Order_Review__c = rev.Id;
                        rec.Project__c = r.Recommendation_Scenario__c;
                        rec.Status__c = 'New';
                        
                        lstChangeOrderItems.add(rec);
                    }
                }
                catch(Exception ex){
                    system.debug('Exception :::::' + ex.getMessage());
                    
                    if(splitedQnty[0].trim() == ''){
                        r.Change_Order_Quantity__c = null;
                        r.Change_Order_Reason__c   = splitedQnty[1];
                        if(changeOrderMap.containsKey(r.Id)){
                            rec.Change_Order_Quantity__c = null;  
                            rec.Id = changeOrderMap.get(r.Id).Id;  
                            rec.Change_Order_Reason__c   = splitedQnty[1];
                            lstChangeOrderItems.add(rec);
                        }
                    }
                    continue;
                } 
            }
            
            if(lstChangeOrderItems.size()>0){
                upsert lstChangeOrderItems;
            }
            
            dsmtRecommendationHelper.disableRolluptriggeronRec = true;
            //update recomList;
        }
        
        try{
            fromController = false;
            loadRecommendations();
        }catch(Exception e){
        
        }
    }
    
     private static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
}