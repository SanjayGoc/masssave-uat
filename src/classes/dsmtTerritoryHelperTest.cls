@isTest
Public class dsmtTerritoryHelperTest{
    @isTest
    public static void runTest(){
        
        Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        insert wt;
        
        Territory__c ter = Datagenerator.createTerritory(regObj.Id,emp.Id,wt.Id);
        ter.Location__c = locObj.Id;
        ter.Address__c = 'sdf';
        insert ter;
        
        Territory_Detail__c td = Datagenerator.createTerritoryDetail(ter.Id);
        insert td;
        
        dsmtTerritoryHelper.CreateTerritoryDetails(new Set<Id>{ter.Id});
        
    }
}