@isTest
public class dsmtEnergyAssessmentValidationCntlrTest{
    public testmethod static void test1(){
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        Account acc = Datagenerator.createAccount();
        Premise__c prem = Datagenerator.createPremise();
        Customer__c cust = Datagenerator.createCustomer(acc.Id,prem.Id);
        Building_Specification__c bs = new Building_Specification__c();
        bs.Bedromm__c=2;
        insert bs;
        Lighting__c light = new Lighting__c();
        light.Name = 'test';
        insert light;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Trade_Ally_Account__c = ta.Id;
        ea.Dsmtracker_contact__c = dsmt.Id;
        ea.Building_Specification__c = bs.Id;
        ea.Lighting__c = light.Id;
        insert ea;
        //dsmtEnergyAssessmentValidationController.validateEnergyAssessment(ea.Id,cust.Id);
    }
}