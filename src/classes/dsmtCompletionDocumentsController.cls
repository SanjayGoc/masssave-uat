public class dsmtCompletionDocumentsController {
    
    public List<DocumentName> documentList{get;set;}
    public String reviewId{get;set;}
    public string selectedDocuments {get;set;}
    public String templateID{get;set;}
    public String queryID{get;set;}
    public String newCustomAttachmentId{get;set;}
    public String[] documents{get;set;}
    public Integer counter{get;set;}
    public String docName{get;set;}
    public String attachments{get;set;}
    public String query{get;set;}
    //Public Integer innerCounter{get;set;}
    Public String stdAttachmentId{get;set;}
    Public String OFN{get;set;}
    Public Review__c review{get;set;}
    
    public dsmtCompletionDocumentsController() {
        
        documentList = new List<DocumentName>();
        reviewId = apexpages.currentpage().getparameters().get('Id');
        
        DocumentName document = new DocumentName();
        document.name = 'Contractor Work Order';
        documentList.add(document);
        
        document = new DocumentName();
        document.name = 'Certificate of Completion';
        
        documentList.add(document);

        attachments='';
        
        List<Review__c> rlist = [select id,name,RecordType.Name from Review__c where id =: reviewId];
        if(rlist.size() > 0){
           review = rlist.get(0);
        }
    }
    
    public void saveOutboundConcate() {
        if(selectedDocuments != null && selectedDocuments.length() > 0 ) {
           selectedDocuments = selectedDocuments.substring(0, selectedDocuments.length() - 1);
           documents = selectedDocuments.split(':');
        }
    }
    
    public void generateCongaDocument() {
        System.debug('documents === '+documents +' '+ counter);
        String documentName = documents[counter];
        
        if(documentName == 'Contractor Work Order') {
            query = '';
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Contractor Work Order'];
            templateID = templates.get(0).ID;
            
            List<String> qList = new List<String>();
            qList.add('Contractor_Work_Order1');
            qList.add('Contractor_Work_Order2');
            qList.add('Contractor_Work_Order3');
            qList.add('Contractor_Work_Order4');
            qList.add('Contractor_Work_Order5');
            qList.add('Contractor_Work_Order6');
            qList.add('Contractor_Work_Order7');
            qList.add('Contractor_Work_Order8');
            qList.add('Contractor_Work_Order9');
            qList.add('Contractor_Work_Order91');
            qList.add('Contractor_Work_Order92');
            qList.add('Contractor_Work_Order93');
            qList.add('Contractor_Work_Order94');
            qList.add('Contractor_Work_Order95');
            qList.add('Contractor_Work_Order96');
            qList.add('Contractor_Work_Order97');
            qList.add('Contractor_Work_Order98');
            qList.add('Contractor_Work_Order99');
            
            List<Project_Review__c> prList = [Select Project__r.Energy_Assessment__c From Project_review__c Where Review__c =: reviewId And Project__r.Energy_Assessment__c != null limit 1]; //DSST-15260
            
            //DSST-10994--START
            List<Floor__c> floorlist =new List<floor__c>();
            //DSST-10994--END
            
            Set<Id> mechanicalIds = new Set<Id>();
            String mStr = '';
            if(prList != null && !prList.isempty() && prList.get(0).Project__r.Energy_Assessment__c != null)  //DSST-15260
            {
                floorlist = [Select Id From Floor__c Where Thermal_Envelope_Type__r.Energy_Assessment__c =: prList.get(0).Project__r.Energy_Assessment__c and Thermal_Envelope_Type__r.RecordType.Name = 'Attic' limit 1];
                Set<String> recordTypes = New Set<String>();
                recordTypes.add('Heating');
                recordTypes.add('Heating+Cooling');
                recordTypes.add('Heating+DHW');

                For(Mechanical__c m : [SELECT Id From Mechanical__c WHERE RecordType.Name In: recordTypes AND Energy_Assessment__c =: prList.get(0).Project__r.Energy_Assessment__c])
                {
                    mechanicalIds.add(m.Id);
                }
                
                
                for(Id s : mechanicalIds)
                {
                    mStr += '\'' +s + '\'|';
                }
                if(mStr.length() > 0){
                    mStr = mStr.subString(0, mStr.length() -1);
                }
                
            }
            //get Conga Query                 
            List<APXTConga4__Conga_Merge_Query__c> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In: qList ORDER BY Unique_Conga_Query_Name__c ASC    ]; 
            for(APXTConga4__Conga_Merge_Query__c queryObj: queries) {
                queryID = queryObj.ID;
                if(queryObj.Unique_Conga_Query_Name__c == 'Contractor_Work_Order3' && !floorlist.isEmpty())
                {
                    query += queryID+'?pv0='+floorlist.get(0).Id+',';
                }
                else if(!mechanicalIds.isempty() && (queryObj.Unique_Conga_Query_Name__c == 'Contractor_Work_Order98' || queryObj.Unique_Conga_Query_Name__c == 'Contractor_Work_Order99')){

                    query += queryID+'?pv0='+ EncodingUtil.urlEncode(mStr , 'UTF-8') +',';
                }
                else
                {
                    if(queryObj.Unique_Conga_Query_Name__c == 'Contractor_Work_Order98' || queryObj.Unique_Conga_Query_Name__c == 'Contractor_Work_Order99')
                        query += queryID+'?pv0=' + EncodingUtil.urlEncode('\'' + reviewId + '\'', 'UTF-8') + ',';
                    else
                        query += queryID+'?pv0='+reviewId+',';

                }
            }
            query = query.subString(0, query.length() -1);
            
        } else if(documentName == 'Certificate of Completion') {
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='CERTIFICATE_OF_COMPLETION'];
            templateID = templates.get(0).ID;
            
            //get Conga Query                 
            List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Certificate_of_completionform_Query','Certificate_of_completionform_Sub_Query') ORDER BY Unique_Conga_Query_Name__c ASC]; 
            query = '';
            for(sObject queryObj: queries) {
                queryID = queryObj.ID;
                query += queryID+'?pv0='+reviewId+',';
            }
            query = query.subString(0, query.length() -1);
        }

        OFN = documentName;
        system.debug('query --' + query); 
        
 
        Attachment__c newCustomAttachment = new Attachment__c();
        newCustomAttachment.Status__c = 'New';
        newCustomAttachment.Attachment_Name__c = documentName +'.pdf';
        if(review.RecordType.Name == 'Work Scope Review' || review.RecordType.Name == 'Work Scope Review Manual'){
            newCustomAttachment.Work_Scope_Review__c  = reviewId ;
        }else if(review.RecordType.Name == 'Billing Review'){
            newCustomAttachment.Billing_Review__c  = reviewId ;
        }
        

        insert newCustomAttachment;
        newCustomAttachmentId = newCustomAttachment.ID;
        attachments += newCustomAttachmentId+',';
        
        System.debug('templateID === '+ templateID + ' & '+query + ' & '+newCustomAttachmentId);
    }   
    
    public boolean enablePollar{get;set;}
    public Id congaAttachmentId {get;set;}
    public boolean pollarSuccess{get;set;}
    public void checkCongaStatus(){
        try{
            pollarSuccess = false;
            enablePollar = true;
            Attachment__c customAttachment = [SELECT Id,Name, (SELECT Id,Name FROM Attachments) FROM Attachment__c WHERE Id =: newCustomAttachmentId];
            system.debug('customAttachment --'+customAttachment);
            system.debug('customAttachment.Attachments  '+customAttachment.Attachments);
            if(customAttachment != null && customAttachment.Attachments.size() > 0) {
                pollarSuccess = true;
                enablePollar = false;
                
                DocumentServiceWSFile.clsSalesForceLoginDetail loginDetail = new DocumentServiceWSFile.clsSalesForceLoginDetail();
                loginDetail.SessionID = UserInfo.getSessionId();
                loginDetail.serverURL = Login_Detail__c.getInstance().Server_URL__c;
                loginDetail.orgID = Login_Detail__c.getInstance().Org_Id__c;
                loginDetail.userName = Login_Detail__c.getInstance().Attachment_User_Name__c;
                loginDetail.password = Login_Detail__c.getInstance().Attachment_User_Password__c;
                loginDetail.securityToken = Login_Detail__c.getInstance().Attachment_User_Security_Token__c;
                
                DocumentServiceWSFile.clsQueryStringParameters clsQueryStringParametersObj = new DocumentServiceWSFile.clsQueryStringParameters();
                clsQueryStringParametersObj.SessionID = UserInfo.getSessionId();
                clsQueryStringParametersObj.ServerURL = Login_Detail__c.getInstance().Server_URL__c;
                clsQueryStringParametersObj.OrgId  = Login_Detail__c.getInstance().Org_Id__c;
                clsQueryStringParametersObj.AttachmentId = newCustomAttachmentId;
                clsQueryStringParametersObj.SecurityKey = 'intelcore2duo';
                clsQueryStringParametersObj.AccountKey = 'nokia710lumia';
                clsQueryStringParametersObj.Replace = 'YES';
                clsQueryStringParametersObj.RecordId = reviewId;
                clsQueryStringParametersObj.AttachmentLookupCol = 'Review__c';
                clsQueryStringParametersObj.AttachmentType = documents[counter];

                stdAttachmentId = customAttachment.Attachments.get(0).Id;
                DocumentServiceWSFile.DocumentServiceWSFileSoap documentServiceWSFile = new DocumentServiceWSFile.DocumentServiceWSFileSoap();
                DocumentServiceWSFile.ArrayOfClsUploadProcessResult result = documentServiceWSFile.MigrateAttachment(clsQueryStringParametersObj, stdAttachmentId, 'attachment', loginDetail);
            }
        }catch(Exception ex) {
            enablePollar = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
    
    public PageReference cancel() {
        return new pagereference('/'+reviewId);     
    }

    public class DocumentName {
        public String name{get;set;}
    }
}