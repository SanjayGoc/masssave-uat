@isTest
public class dsmtTicketsCntlrTest {
	@isTest
    Public Static Void RunTest(){
         
        /*UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test2@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;*/
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        insert ta;
        
        Contact cn = new Contact();
        cn.LastName = 'test last name';
        insert cn;
        
		DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
		dsmtc.Name='test';
        dsmtc.Super_User__c=true;
        dsmtc.Trade_Ally_Account__c=ta.id;
        dsmtc.contact__c=cn.id;
        insert dsmtc;
        
        Service_Request__c sr = new Service_Request__c();
        sr.Type__c='API';
        sr.Subject__c='test subject';
        sr.Status__c='New';
        sr.Priority__c='High';
        sr.Description__c='test';
        sr.DSMTracker_Contact__c=dsmtc.id;
        
        insert sr;
            
        dsmtTicketsCntlr dsmttkt = new dsmtTicketsCntlr();
        dsmttkt.listOwnerId();
        
        dsmtTicketsCntlr.queryMessage(sr.id);
        dsmtTicketsCntlr.saveRespond(sr.Subject__c, sr.Type__c, sr.Status__c, sr.Priority__c,sr.Description__c,ta.ID, dsmtc.Id);
        dsmttkt.getTicketType();
        dsmttkt.getTicketPriority();
        dsmttkt.getTicketStatus();
        dsmttkt.initMessages();
        dsmtTicketsCntlr.MessageModal inclass1 = new dsmtTicketsCntlr.MessageModal();
        inclass1.name='test';
        inclass1.Subject='test subject';
        inclass1.Type='API';
        inclass1.subType='test sub type';
        inclass1.Status='New';
        inclass1.Priority='High';
        inclass1.Description='Test dec';
        inclass1.CreatedDate='Todat';
        dsmtTicketsCntlr.MessagesModel inclass2 = new dsmtTicketsCntlr.MessagesModel();
    }
}