public with sharing class CrawlspaceThermalEnvelopeTypeModel{
    public String visiblePanel{get;set;}
    public Thermal_Envelope_Type__c envelopeType{get;private set;}
    public String fieldUniqueDataId{get;set;}
    
    public CrawlspaceThermalEnvelopeTypeModel(Thermal_Envelope_Type__c envelopeType){
        setEnvelopeType(envelopeType);
        updateCrawlspaceChildComponents();
        refreshCrawlspacePanel();
    }
    public void setEnvelopeType(Thermal_Envelope_Type__c envelopeType){
        this.envelopeType = envelopeType.clone(true);
    }
    
    public void refreshCrawlspacePanel(){
        System.debug('VISIBLE PANEL : ' + visiblePanel);
        queryCrawlspaceWalls();
        queryCrawlspaceCeilings();
        queryCrawlspaceFloors();
    }
    
    public void updateCrawlspaceChildComponents(){
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeFloors(envelopeType.Id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeWalls(envelopeType.Id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeRoofs(envelopeType.Id);
    }
    
    /* CRAWLSPACE CEILINGS -------------------------------------------------------------------------------------------------------*/
    public Ceiling__c editCrawlspaceCeiling{get;set;}
    public transient Ceiling__c newCrawlspaceCeiling{get;set;}
    public transient List<Ceiling__c> crawlspaceCeilings{get;set;}
    public void queryCrawlspaceCeilings(){
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'CrawlspaceHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            crawlspaceCeilings = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            crawlspaceCeilings = new List<Ceiling__c>();
        }
    }
    public void addNewCrawlspaceCeiling(String ceilingType ,String exposedTo){
        if(envelopeType != null && envelopeType.Id != null){
            Decimal nextNumber = (envelopeType.Next_Crawlspace_Ceiling_Number__c != null) ? envelopeType.Next_Crawlspace_Ceiling_Number__c : 1;
            
            newCrawlspaceCeiling= new Ceiling__c();
            newCrawlspaceCeiling.Thermal_Envelope_Type__c = envelopeType.Id;
            newCrawlspaceCeiling.Name = 'Crawlspace Ceiling ' + nextNumber;
            newCrawlspaceCeiling.Exposed_To__c = exposedTo;
            newCrawlspaceCeiling.Add__c = ceilingType; 
               
            INSERT newCrawlspaceCeiling;
            if(newCrawlspaceCeiling.Id != null){
                envelopeType.Next_Crawlspace_Ceiling_Number__c = nextNumber + 1;
                UPDATE envelopeType;
                if(updateCrawlspaceCeilingById(newCrawlspaceCeiling.Id) != null){
                    queryCrawlspaceCeilingById(newCrawlspaceCeiling.Id);
                }
            }
        }
        refreshCrawlspacePanel();
    }
    public Ceiling__c updateCrawlspaceCeilingById(String ceilingId){
        if(ceilingId != null){
            Ceiling__c ceiling= new Ceiling__c(Id = ceilingId);
            UPDATE [SELECT Id FROM Layer__c WHERE Ceiling__c =:ceiling.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE ceiling;
            return ceiling;
        }
        return null;
    }
    public void queryCrawlspaceCeilingById(String ceilingId){
        if(ceilingId != null){
            List<Ceiling__c> ceilings = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Id=:ceilingId');
            if(ceilings.size() > 0){
                editCrawlspaceCeiling = ceilings.get(0);
                visiblePanel = 'CrawlspaceCeilingPanel';
            }
        }
    }
    public void deleteCrawlspaceCeilingById(String ceilingId){
        if(ceilingId != null){
            DELETE new Ceiling__c(Id = ceilingId);
        }
        refreshCrawlspacePanel();
    }
    
    /* CRAWLSPACE WALLS -------------------------------------------------------------------------------------------------------*/
    public Wall__c editCrawlspaceWall{get;set;}
    public transient Wall__c newCrawlspaceWall{get;set;}
    public transient List<Wall__c> crawlspaceWalls{get;set;}
    public void queryCrawlspaceWalls(){
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'CrawlspaceHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            crawlspaceWalls = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            crawlspaceWalls = new List<Wall__c>();
        }
    }
    public void addNewCrawlspaceWall(String wallType, String exposedTo){
        if(envelopeType != null && envelopeType.Id != null){
            Decimal nextNumber = (envelopeType.Next_Crawlspace_Wall_Number__c != null) ? envelopeType.Next_Crawlspace_Wall_Number__c : 1;
            
            newCrawlspaceWall= new Wall__c();
            newCrawlspaceWall.Thermal_Envelope_Type__c = envelopeType.Id;
            newCrawlspaceWall.Name = 'Crawlspace Wall ' + nextNumber;
            newCrawlspaceWall.Exposed_To__c = exposedTo;
            newCrawlspaceWall.Add__c = wallType; 
            newCrawlspaceWall.Insul_Amount__c = 'Standard';
               
            INSERT newCrawlspaceWall;
            if(newCrawlspaceWall.Id != null){
                envelopeType.Next_Crawlspace_Wall_Number__c = nextNumber + 1;
                UPDATE envelopeType;
                if(updateCrawlspaceWallById(newCrawlspaceWall.Id) != null){
                    queryCrawlspaceWallById(newCrawlspaceWall.Id);
                }
            }
        }
        refreshCrawlspacePanel();
    }
    public Wall__c updateCrawlspaceWallById(String wallId){
        if(wallId != null){
            Wall__c wall = new Wall__c(Id = wallId);
            UPDATE [SELECT Id FROM Layer__c WHERE Wall__c =:wall.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE wall;
            return wall;
        }
        return null;
    }
    public void queryCrawlspaceWallById(String wallId){
        if(wallId != null){
            List<Wall__c> walls = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Id=:wallId');
            if(walls.size() > 0){
                editCrawlspaceWall = walls.get(0);
                visiblePanel = 'CrawlspaceWallPanel';
            }
        }
    }
    public void deleteCrawlspaceWallById(String wallId){
        if(wallId != null){
            DELETE new Wall__c(Id = wallId);
        }
        refreshCrawlspacePanel();
    }
    
    /* CRAWLSPACE FLOORS -------------------------------------------------------------------------------------------------------*/
    public Floor__c editCrawlspaceFloor{get;set;}
    public transient Floor__c newCrawlspaceFloor{get;set;}
    public transient List<Floor__c> crawlspaceFloors{get;set;}
    public void queryCrawlspaceFloors(){
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'CrawlspaceHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            crawlspaceFloors = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Floor__c') + ' FROM Floor__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            crawlspaceFloors = new List<Floor__c>();
        }
    }
    public void addNewCrawlspaceFloor(String floorType ,String exposedTo){
        if(envelopeType != null && envelopeType.Id != null){
            Decimal nextNumber = (envelopeType.Next_Crawlspace_Floor_Number__c != null) ? envelopeType.Next_Crawlspace_Floor_Number__c : 1;
            
            newCrawlspaceFloor= new Floor__c();
            newCrawlspaceFloor.Thermal_Envelope_Type__c = envelopeType.Id;
            newCrawlspaceFloor.Name = 'Crawlspace Floor ' + nextNumber;
            newCrawlspaceFloor.Exposed_To__c = exposedTo;
            newCrawlspaceFloor.Add__c = floorType; 
               
            INSERT newCrawlspaceFloor;
            if(newCrawlspaceFloor.Id != null){
                envelopeType.Next_Crawlspace_Floor_Number__c = nextNumber + 1;
                UPDATE envelopeType;
                if(updateCrawlspaceFloorById(newCrawlspaceFloor.Id) != null){
                    queryCrawlspaceFloorById(newCrawlspaceFloor.Id);
                }
            }
        }
        refreshCrawlspacePanel();
    }
    public Floor__c updateCrawlspaceFloorById(String floorId){
        if(floorId != null){
            Floor__c floor = new Floor__c(Id = floorId);
            UPDATE [SELECT Id FROM Layer__c WHERE Floor__c =:floor.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE floor;
            return floor;
        }
        return null;
    }
    public void queryCrawlspaceFloorById(String floorId){
        if(floorId != null){
            List<Floor__c> floors = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Floor__c') + ' FROM Floor__c WHERE Id=:floorId');
            if(floors.size() > 0){
                editCrawlspaceFloor = floors.get(0);
                visiblePanel = 'CrawlspaceFloorPanel';
            }
        }
    }
    public void deleteCrawlspaceFloorById(String floorId){
        if(floorId != null){
            DELETE new Floor__c(Id = floorId);
        }
        refreshCrawlspacePanel();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    public Component.Apex.OutputPanel getDynamicComponent(){
        Component.Apex.OutputPanel panel = new Component.Apex.OutputPanel();
        panel.layout = 'block';
        if(visiblePanel == 'CrawlspaceWallPanel'){
            Component.c.CrawlspaceWallComponent wall = new Component.c.CrawlspaceWallComponent();
            wall.crawlspaceWallObjId = editCrawlspaceWall.Id;
            wall.onSave = 'showCrawlspacePanel("CrawlspaceHomePanel");';
            panel.childComponents.add(wall);
        }else if(visiblePanel == 'CrawlspaceCeilingPanel'){
            Component.c.CrawlspaceCeilingComponent ceiling = new Component.c.CrawlspaceCeilingComponent();
            ceiling.crawlspaceCeilingObjId = editCrawlspaceCeiling.Id;
            ceiling.onSave = 'showCrawlspacePanel("CrawlspaceHomePanel");';
            panel.childComponents.add(ceiling);
        }else if(visiblePanel == 'CrawlspaceFloorPanel'){
            Component.c.CrawlspaceFloorComponent floor = new Component.c.CrawlspaceFloorComponent();
            floor.crawlspaceFloorObjId = editCrawlspaceFloor.Id;
            floor.onSave = 'showCrawlspacePanel("CrawlspaceHomePanel");';
            panel.childComponents.add(floor);
        }else{
            panel.rendered=false;
        }
        return panel;
    }
    
    public void saveDynamicComponent(){
        if(visiblePanel == 'CrawlspaceWallPanel'){
            UPSERT editCrawlspaceWall;
        }else if(visiblePanel == 'CrawlspaceCeilingPanel'){
            UPSERT editCrawlspaceCeiling;
        }else if(visiblePanel == 'CrawlspaceFloorPanel'){
            UPSERT editCrawlspaceFloor;
        }
    }
}