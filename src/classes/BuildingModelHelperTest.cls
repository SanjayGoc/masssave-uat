@isTest
private class BuildingModelHelperTest 
{  
  @testsetup static void SetupEnergyAssessment()
    {
    	dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();     
        Air_Flow_and_Air_Leakage__c ai = new Air_Flow_and_Air_Leakage__c();
        ai.Energy_Assessment__c = testEA.Id;
        insert ai;

        List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:testEA.Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = testEA.Id;
            insert te;
        }

        List<Thermal_Envelope_Type__c> tetList = new List<Thermal_Envelope_Type__c>();

        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        tet.Thermal_Envelope__c = te.Id;
        tet.Energy_Assessment__c = testEA.Id;
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Attic').getRecordTypeId();
        tetList.add(tet);

        tet = new Thermal_Envelope_Type__c();
        tet.Thermal_Envelope__c = te.Id;
        tet.Energy_Assessment__c = testEA.Id;
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Exterior Wall').getRecordTypeId();
        tetList.add(tet);

        tet = new Thermal_Envelope_Type__c();
        tet.Thermal_Envelope__c = te.Id;
        tet.Energy_Assessment__c = testEA.Id;
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Door').getRecordTypeId();
        tetList.add(tet);

        tet = new Thermal_Envelope_Type__c();
        tet.Thermal_Envelope__c = te.Id;
        tet.Energy_Assessment__c = testEA.Id;
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Door').getRecordTypeId();
        tetList.add(tet);

        tet = new Thermal_Envelope_Type__c();
        tet.Thermal_Envelope__c = te.Id;
        tet.Energy_Assessment__c = testEA.Id;
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Door').getRecordTypeId();
        tetList.add(tet);

        insert tetList;

        dsmtFuture.CreateThermalEnvelopType(testEA.Id, te.Id, 'Windows & Skylights');

        Lighting__c genLighting = new Lighting__c();
	    genLighting.Energy_Assessment__c = testEA.Id;
	    genLighting.Name = 'General Lighting';
	    insert genLighting;
        
    }

    @istest static void BuildingModelHelperTest1()
    {
		Set<Id> eaId = new Set<Id>();
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
    	eaId.add(eaList[0].Id);

    	Test.startTest();

        Attic_Venting__c av = new Attic_Venting__c();

        av.Energy_Assessment__c = eaList[0].Id;
        av.Name = 'Attic Venting';
        insert av;

     	dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
      	bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
      	bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);
		dsmtBuildingModelHelper.UpdateBaseBuildingThermal(bp);

		bp.months = 'January;February;March;April;May;June;July;August;September;October;November;December';
		dsmtBuildingModelHelper.ComputeMonthsSeasonFractionCommon(bp);

		dsmtBuildingModelHelper.IsLocationInThermalSpace(bp, 'AtticSpace');
		dsmtBuildingModelHelper.ComputeHasHeatingCommon(bp);
		dsmtBuildingModelHelper.UpdateBuildingThermalLoadModel(bp);
		dsmtBuildingModelHelper.UpdateSurfaceModelForAirFlowRecommendation(bp);
		dsmtBuildingModelHelper.UpdateLightLocations(new Lighting_Location__c(),bp);

	    Test.stopTest();
    }

    @istest static void BuildingModelHelperTest2()
    {
		Set<Id> eaId = new Set<Id>();
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
		eaId.add(eaList[0].Id);

	      /// create mechnical records
	    Mechanical__c mech = new Mechanical__c();    
	    mech.Energy_Assessment__c = eaList[0].Id;
	    mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
	    insert mech;

	    Mechanical_Type__c mechType = new Mechanical_Type__c();
	    mechType.Energy_Assessment__c = eaList[0].Id;
	    mechType.Mechanical__c = mech.Id;
	    mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
	    insert mechType;

	    Mechanical__c mechHotWater = new Mechanical__c();    
	    mechHotWater.Energy_Assessment__c = eaList[0].Id;
	    mechHotWater.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Hot Water').getRecordTypeId();
	    insert mechHotWater;

	    Mechanical_Type__c mechHotWaterType = new Mechanical_Type__c();
	    mechHotWaterType.Energy_Assessment__c = eaList[0].Id;
	    mechHotWaterType.Mechanical__c = mechHotWater.Id;
	    mechHotWaterType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
	    insert mechHotWaterType;

	    List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

	    Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
	    mechSubType.Energy_Assessment__c = eaList[0].Id;
	    mechSubType.Mechanical__c = mech.Id;
	    mechSubType.Mechanical_Type__c = mechType.Id;
	    mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
	    mstList.add(mechSubType);

	    mechSubType = new Mechanical_Sub_Type__c();
	    mechSubType.Energy_Assessment__c = eaList[0].Id;
	    mechSubType.Mechanical__c = mech.Id;
	    mechSubType.Mechanical_Type__c = mechType.Id;
	    mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
	    mstList.add(mechSubType);

	    mechSubType = new Mechanical_Sub_Type__c();
	    mechSubType.Energy_Assessment__c = eaList[0].Id;
	    mechSubType.Mechanical__c = mech.Id;
	    mechSubType.Mechanical_Type__c = mechType.Id;
	    mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
	    mstList.add(mechSubType);

	    mechSubType = new Mechanical_Sub_Type__c();
	    mechSubType.Energy_Assessment__c = eaList[0].Id;
	    mechSubType.Mechanical__c = mechHotWater.Id;
	    mechSubType.Mechanical_Type__c = mechHotWaterType.Id;
	    mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
	    mstList.add(mechSubType);

     	List<Appliance__c> aplList = new List<Appliance__c>();

	    Appliance__c apl = New Appliance__c();
	    apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Refrigeration').getRecordTypeId();
	    apl.Type__c = 'Refrigeration';
	    apl.Name = 'Refrigeration 1';
	    apl.Energy_Assessment__c = eaList[0].Id;

	    aplList.add(apl);

	    apl = New Appliance__c();

	    apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Washer').getRecordTypeId();
	    apl.Type__c = 'Washer';
	    apl.Name = 'Washer 1';
	    apl.Energy_Assessment__c = eaList[0].Id;

	    aplList.add(apl);

	    apl = New Appliance__c();
	    apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Dryer').getRecordTypeId();
	    apl.Type__c = 'Dryer';
	    apl.Name = 'Dryer 1';
	    apl.Energy_Assessment__c = eaList[0].Id;
	    aplList.add(apl);

	    apl = New Appliance__c();
	    apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Electronics TV').getRecordTypeId();
	    apl.Type__c = 'TV';
	    apl.Name = 'TV 1';
	    apl.Energy_Assessment__c = eaList[0].Id;
	    aplList.add(apl);

	    apl = New Appliance__c();
	    apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Dishwasher').getRecordTypeId();
	    apl.Type__c = 'Dishwasher';
	    apl.Name = 'Dishwasher 1';
	    apl.Energy_Assessment__c = eaList[0].Id;

	    aplList.add(apl);

	    apl = New Appliance__c();
	    apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Range').getRecordTypeId();
	    apl.Type__c = 'Range';
	    apl.Name = 'Range 1';
	    apl.Energy_Assessment__c = eaList[0].Id;

	    aplList.add(apl);      

	    Test.startTest();

	    insert mstList;
	    insert aplList;

		dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		dsmtBuildingModelHelper.UpdateBaseBuildingThermal(bp);
		dsmtBuildingModelHelper.UpdateBuildingModelForLighting(bp);
		dsmtBuildingModelHelper.UpdateBuildingModelForApplianceAndWaterFixtureRecommendation(bp);
		Test.stopTest();
    }

    @isTest static void test3()
    {
		Set<Id> eaId = new Set<Id>();
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
		eaId.add(eaList[0].Id);

		Test.startTest();

      /// create mechnical records
		Mechanical__c mech = new Mechanical__c();    
		mech.Energy_Assessment__c = eaList[0].Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Hot Water').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = eaList[0].Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		insert mechType;    

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		mstList.add(mechSubType);

		insert mstList;

		dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
		dsmtTempratureHelper.ComputeTemprature(bp);
		dsmtBuildingModelHelper.ComputeBuildingModel(bp);

		bp.mstobj = mstList[0];
		bp.mechtype = mechType;

		dsmtBuildingModelHelper.UpdateBuildingProfileForDHWInsert(bp);
		dsmtBuildingModelHelper.UpdateMechanicalEquipmentModel(mstList[0],bp);
		dsmtBuildingModelHelper.UpdateAtticVentingModel(new Attic_Venting__c(),bp);
		dsmtBuildingModelHelper.UpdateFloorCeilingModel(bp);

		Test.stopTest();
    }

     @isTest static void test4()
    {
		Set<Id> eaId = new Set<Id>();
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
		eaId.add(eaList[0].Id);

		Test.startTest();

		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(1);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(2);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(3);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(4);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(5);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(6);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(7);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(8);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(9);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(10);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(11);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(12);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(13);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(14);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(15);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(16);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(17);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(18);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(19);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(20);
		dsmtBuildingModelHelper.GetDHWSystemRecommendationOperation(21);

		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(1);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(2);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(3);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(4);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(5);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(6);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(7);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(8);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(9);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(10);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(11);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(12);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(13);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(14);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(15);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(16);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(17);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(18);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(19);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(20);
		dsmtBuildingModelHelper.GetMechanicalSystemRecommendationOperation(21);

		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(1);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(2);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(3);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(4);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(5);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(6);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(7);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(8);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(9);
		dsmtBuildingModelHelper.GetHeatingSystemRecommendationOperation(10);

		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(1);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(2);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(3);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(4);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(5);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(6);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(7);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(8);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(9);
		dsmtBuildingModelHelper.GetCoolingSystemTypeFromNum(10);

		dsmtBuildingModelHelper.GetHeatingSystemTypeFromNum(1);
		dsmtBuildingModelHelper.GetHeatingSystemTypeFromNum(2);
		dsmtBuildingModelHelper.GetHeatingSystemTypeFromNum(3);
		dsmtBuildingModelHelper.GetHeatingSystemTypeFromNum(4);

		dsmtBuildingModelHelper.GetAtticVentType(1);
		dsmtBuildingModelHelper.GetAtticVentType(2);
		dsmtBuildingModelHelper.GetAtticVentType(3);
		dsmtBuildingModelHelper.GetAtticVentType(4);
		dsmtBuildingModelHelper.GetAtticVentType(5);
		dsmtBuildingModelHelper.GetAtticVentType(6);
		dsmtBuildingModelHelper.GetAtticVentType(7);
		dsmtBuildingModelHelper.GetAtticVentType(8);
		dsmtBuildingModelHelper.GetAtticVentType(9);
		dsmtBuildingModelHelper.GetAtticVentType(100);

		dsmtBuildingModelHelper.GetWaterFixtureRecommendationOperation(1);
		dsmtBuildingModelHelper.GetWaterFixtureRecommendationOperation(2);
		dsmtBuildingModelHelper.GetWaterFixtureRecommendationOperation(3);
		dsmtBuildingModelHelper.GetWaterFixtureRecommendationOperation(4);
		dsmtBuildingModelHelper.GetWaterFixtureRecommendationOperation(5);
		dsmtBuildingModelHelper.GetWaterFixtureRecommendationOperation(6);

		Recommendation__c avr = new Recommendation__c();
		avr.Energy_Assessment__c = eaList[0].Id;
		avr.AtticVentType__c = dsmtBuildingModelHelper.GetAtticVentType(5);
		avr.VentIsContinuous__c = false;

		dsmtBuildingModelHelper.ComputeAtticVentingRecommendationCanBeAtticAccessCommon(avr);
      

		Test.stopTest();
    }  

    @isTest static void test5()
    {
		Set<Id> eaId = new Set<Id>();
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
		eaId.add(eaList[0].Id);

		Energy_Assessment__c ea = eaList[0];

		Test.startTest();

		List<Appliance__c> aplList = new List<Appliance__c>();

		Appliance__c apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other Dehumidifier').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Dehumidifier';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other Other').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Other';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other General').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'General';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Refrigeration').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Refrigeration';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Electronics TV').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'TV';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Dishwasher').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Dishwasher';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Range').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Range';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Dryer').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Dryer';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Washer').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Washer';
		aplList.add(apl);


		insert aplList;


		Test.stopTest();
    }

	@isTest
	static void test6()
	{
		Set<Id> eaId = new Set<Id>();
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
		eaId.add(eaList[0].Id);

		Energy_Assessment__c ea = eaList[0];

		Test.startTest();

		List<Appliance__c> aplList = new List<Appliance__c>();

		Appliance__c apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other Dehumidifier').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Dehumidifier';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other Other').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Other';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other General').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'General';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Refrigeration').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Refrigeration';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Electronics TV').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'TV';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Dishwasher').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Dishwasher';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Range').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Range';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Dryer').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Dryer';
		aplList.add(apl);

		apl = new Appliance__c();
		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Washer').getRecordTypeId();
		apl.Energy_Assessment__c = ea.Id;
		apl.Type__c = 'Washer';
		aplList.add(apl);


		insert aplList;
		
		eaId.add(ea.Id);

		dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
		bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
		bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);


		//system.debug('bp.aplList.size()>>>'+bp.aplList.size());

		For(integer i=0, j = bp.aplList.size(); i <j; i++)
		{
			bp.applObj = bp.aplList[i];
			//ApplianceTriggerHandler.updateApplianceSystem(bp);
			break;
		}

		//dsmtApplianceHelper.ComputeRefrigeratorMeteringMinutesCommon(bp.aplList[0]);
		dsmtApplianceHelper.GetGameConsoleTV(bp);
		dsmtEnergyConsumptionHelper.ComputeHandDishwashingIndoorGainHCommon(bp);
		dsmtEnergyConsumptionHelper.ComputeHandDishwashingIndoorGainCCommon(bp);
		dsmtEnergyConsumptionHelper.findCorrespondingDryer(bp);

		Test.stopTest();
	}

	@isTest
    static void test7()
    {
        Set<Id> eaId = new Set<Id>();
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
		eaId.add(eaList[0].Id);

		Energy_Assessment__c ea = eaList[0];

        Test.startTest();

        List<Water_Fixture__c> waterList = new List<Water_Fixture__c>();

        Water_Fixture__c waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Pool').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterobj.Type__c = 'Pool';
        waterobj.NumberOperatingMonths__c =10;
        waterobj.TimerHours__c = 4;
        waterobj.WinterHours__c = 5;
        waterobj.PoolPumpWatts__c = 20;
        waterList.add(waterobj);

        dsmtWaterFixtureHelper.ComputePoolHasOffSeasonUseCommon(waterobj);

        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Sink').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);

        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Bath').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);

        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Shower').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);

        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Spa').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);

        insert waterList;

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
		bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
		bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);
		
		For(integer i =0, j = bp.waterList.size(); i < j; i++)
		{
			bp.waterObj = waterList[1];
			break;
		}

		dsmtEnergyConsumptionHelper.ComputeWaterFixtureLoad2Common(bp);
		dsmtEnergyConsumptionHelper.ComputeWaterFixtureEfficiency2Common(bp);
		dsmtEnergyConsumptionHelper.ComputeWaterFixtureFuel2IndoorGainOther(bp,'GainUseH',1);
		dsmtEnergyConsumptionHelper.computeWaterFixtureFuel2IndoorGainGas(bp,'GainUseC',1);
		dsmtEnergyConsumptionHelper.computeWaterFixtureFuel2IndoorGainHW(bp,'GainUseH',1);
		dsmtEnergyConsumptionHelper.computeWaterFixtureFuel2IndoorGain(bp,'GainUseH',1);
		dsmtEnergyConsumptionHelper.computeWaterFixtureFuelIndoorGain(bp,'GainUseH',1);
		dsmtEnergyConsumptionHelper.ComputeAnnualOperatingCostCommon(bp);
		dsmtEnergyConsumptionHelper.ComputeFuel2TypeCommon(bp);
		dsmtEnergyConsumptionHelper.getFractionDays(bp,'IntensC');
		dsmtEnergyConsumptionHelper.AddFuelUseForCurrentBuildingProfileObject(bp);

        Test.stopTest();
    }
}