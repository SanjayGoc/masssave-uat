@isTest
public class RecommendationHelperTest{
    public testmethod static void test1(){
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        DSMTracker_Product__c dp = new DSMTracker_Product__c();
        dp.name ='test';
        dp.Eversource_Program_Price__c =2;
        insert dp;
        
        Lighting_Part_Detail__c lpd = new Lighting_Part_Detail__c();
        lpd.name='test';
        lpd.DSMTracker_Product__c =dp.id;
        lpd.Wattage__c =5;
        insert lpd;
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Occupants__c =2;
        insert bs;
        
        /*Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Building_Specification__c =bs.id;
        insert ea;*/
        
        
        Lighting__c lig = new Lighting__c();
        lig.name ='test';
        //lig.Energy_Assessment__c =ea.id;
        insert lig;
        
        Saving_Constant__c sc = new Saving_Constant__c();
        sc.name='test';
        sc.LightingLocation_DaysPerYear__c=5;
        sc.Kwh_To_BTUH__c=5;
        insert sc;
                      
        Lighting_Location__c ll = new Lighting_Location__c();
        ll.name='test';
        ll.Lighting__c =lig.id;
        ll.Total_Watts__c =2;
        ll.Usage__c=4;
        ll.LightBulbSetLoadKWH__c  =5;
        insert ll;
        
        Id RecordTypeIdappliance = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Electronics TV').getRecordTypeId();
        
        Appliance__c ap = new Appliance__c();
        ap.name ='test';
        ap.Smart_Power_Strip__c =true;
       // ap.recordtypeid = RecordTypeIdappliance;
        ap.Computed_Room_Temperature__c =4;
        insert ap;
        
        Water_Fixture__c wf = new Water_Fixture__c();
        wf.name ='test';
        insert wf;
        
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        rc.DSMTracker_Product__c=dp.id;
        rc.Lighting_Location__c=ll.id;
        rc.Appliance__c=ap.id;
        rc.Water_Fixture__c =wf.id;
        rc.Total_Recommended_KW_Savings__c =5;
        //rc.Energy_Assessment__c =ea.id;
        rc.Kwh__c =5;
        rc.Primary_Refrigerator__c =true;
        rc.Anti_Sweat_Switch__c =true;
        rc.Ice_in_Door__c =true;
        rc.Door_Seal_Gaps__c=true;
        rc.Kw__c =45;
        insert rc;
        
       /* Energy_Assessment__c ea1 =new Energy_Assessment__c();
        ea1.id =;
        insert ea1;*/
        
        Recommendation__c rc1 = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc1;

        Review__c rev = new Review__c(Project__c = proj.id);
        insert rev;
        
        customer__c cus = new customer__c();
        cus.name = 'test';
        insert cus;
        
        
        
        list<recommendation__c> rclist = new list<recommendation__c>();
        rclist.add(rc);
        list<recommendation__c> rclist1 = new list<recommendation__c>();
        RecommendationHelper rh = new RecommendationHelper();
        RecommendationHelper.CalculateCostAndSavings(rclist);
    }
   
}