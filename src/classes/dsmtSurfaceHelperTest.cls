@isTest
private class dsmtSurfaceHelperTest {
    
    @testsetup static void SetupEnergyAssessment()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();

        Air_Flow_and_Air_Leakage__c ai = new Air_Flow_and_Air_Leakage__c();
        ai.Energy_Assessment__c = testEA.Id;
        insert ai;

        List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:testEA.Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = testEA.Id;
            insert te;        
        }

        Thermal_Envelope_Type__c AtticObj = new Thermal_Envelope_Type__c();
        AtticObj.Energy_Assessment__c = testEA.Id;
        AtticObj.Name = 'Attic';
        AtticObj.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Attic').getRecordTypeId();
        AtticObj.Thermal_Envelope__c = te.Id;
        insert atticObj;

    }
    
    @isTest static void SurfaceHelperTest1() 
    {
        Set<Id> eaId = new Set<Id>();
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
        eaId.add(eaList[0].Id);

        Test.startTest();

        List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:eaList[0].Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = eaList[0].Id;
            insert te;        
        }

        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Exterior Wall');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Garage Wall');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Garage Ceiling');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Band Joist');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Door');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Door');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Basement');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Attic');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Slab');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Flat Roof');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Vaulted Roof');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Overhang');        
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Floor Over Other Dwelling');
        dsmtFuture.CreateThermalEnvelopType(eaList[0].Id, te.Id, 'Windows & Skylights');

        dsmtSurfaceHelper.GetOrientationNumericEnumValue('Southeast');
        dsmtSurfaceHelper.GetOrientationNumericEnumValue('East');
        dsmtSurfaceHelper.GetOrientationNumericEnumValue('Northeast');
        dsmtSurfaceHelper.GetOrientationNumericEnumValue('North');
        dsmtSurfaceHelper.GetOrientationNumericEnumValue('Northwest');
        dsmtSurfaceHelper.GetOrientationNumericEnumValue('West');
        dsmtSurfaceHelper.GetOrientationNumericEnumValue('Southwest');

        dsmtSurfaceHelper.GetFaceNumericEnumValue('Front');
        dsmtSurfaceHelper.GetFaceNumericEnumValue('Right');
        dsmtSurfaceHelper.GetFaceNumericEnumValue('Left');
        dsmtSurfaceHelper.GetFaceNumericEnumValue('Back');
        dsmtSurfaceHelper.GetFaceNumericEnumValue('Right Back');
        dsmtSurfaceHelper.GetFaceNumericEnumValue('Left Back');
        dsmtSurfaceHelper.GetFaceNumericEnumValue('Front Left');
        

        Attic_Venting__c av = new Attic_Venting__c();

        av.Energy_Assessment__c = eaList[0].Id;
        av.Name = 'Attic Venting';
        insert av;
        
        
        Test.stopTest();
    }

    @isTest static void SurfaceHelperTest2() 
    {
        Set<Id> eaId = new Set<Id>();
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
        eaId.add(eaList[0].Id);

        Test.startTest();

        List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:eaList[0].Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = eaList[0].Id;
            insert te;        
        }

        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        tet.Energy_Assessment__c = eaList[0].Id;
        tet.Name = 'Attic';
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Attic').getRecordTypeId();
        tet.Thermal_Envelope__c = te.Id;
        insert tet;

        Wall__c wall = new Wall__c();
        wall.Add__c = 'Unfinished Exterior';
        wall.Exposed_To__c = 'Living Space';
        wall.Name = 'Knee Wall';
        wall.Thermal_Envelope_Type__c = tet.Id;
        insert wall;

        Attic_Access_and_Whole_House_Fan__c aa = new Attic_Access_and_Whole_House_Fan__c();
        aa.Thermal_Envelope_Type__c = tet.Id;
        aa.Access_Type__c = SurfaceConstants.DefaultAtticAccessType;
        aa.Insulation_Type__c = SurfaceConstants.DefaultInsulationType;            
        aa.Insulation_Thickness_inch__c =0;
        insert aa;
        
        Test.stopTest();
    }

    @isTest static void SurfaceHelperTest3() 
    {
        Set<Id> eaId = new Set<Id>();
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
        eaId.add(eaList[0].Id);

        Test.startTest();

        List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:eaList[0].Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = eaList[0].Id;
            insert te;        
        }

        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        tet.Energy_Assessment__c = eaList[0].Id;
        tet.Name = 'Basement';
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Basement').getRecordTypeId();
        tet.Thermal_Envelope__c = te.Id;
        insert tet;

        Wall__c wall = new Wall__c();

        wall.Add__c = 'Rim Joist';
        wall.Thermal_Envelope_Type__c = tet.Id;
        wall.Exposed_To__c = 'Exterior';
        wall.Name = 'Baesment Rim Joist';
        insert wall;

        List<Floor__c> floorList = [Select Id From Floor__c Where Thermal_Envelope_Type__c=:tet.Id];
        if(floorList !=null && floorList.size() > 0) upsert floorList[0];
        
        Test.stopTest();
    }

    @isTest static void SurfaceHelperTest4() 
    {
        Set<Id> eaId = new Set<Id>();
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
        eaId.add(eaList[0].Id);

        Test.startTest();

        List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:eaList[0].Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = eaList[0].Id;
            insert te;        
        }

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        tet.Energy_Assessment__c = eaList[0].Id;
        tet.Name = 'Exterior Wall';
        tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Exterior Wall').getRecordTypeId();
        tet.Thermal_Envelope__c = te.Id;

        insert tet;

        List<Layer__c> layers = new List<Layer__c>();

        Layer__c layer = new Layer__c();
        layer.RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get('Wood Frame').getRecordTypeId();
        layer.Thermal_Envelope_Type__c = tet.Id;
        layer.Type__c = 'Wood Frame';
        layer.Name = 'Wood Frame';

        layers.add(layer);

        layer = new Layer__c();
        layer.RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get('Masonry').getRecordTypeId();
        layer.Thermal_Envelope_Type__c = tet.Id;
        layer.Type__c = 'Masonry';
        layer.Name = 'Masonry';

        layers.add(layer);

        layer = new Layer__c();
        layer.RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get('Wood Frame').getRecordTypeId();
        layer.Thermal_Envelope_Type__c = tet.Id;
        layer.Type__c = 'Wallboard';
        layer.Name = 'Wallboard';

        layers.add(layer);

        insert layers;

        bp.tetype = tet;
        bp.layers = layers;

        Mechanical_Type__c mechType = new Mechanical_Type__c();        
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();

        bp.mechList = new List<Mechanical_Type__c>();
        bp.mechList.add(mechType);

        dsmtSurfaceHelper.ComputeHasCoolingCommon(bp);
        bp.tetype.RValueConst__c = dsmtSurfaceHelper.ComputeRValueConst(bp);

        dsmtSurfaceHelper.ComputeSurfaceLayersRValue(bp);
        dsmtSurfaceHelper.ComputeWallInsulationAmountCommon2(bp);
        dsmtSurfaceHelper.ComputeSurfaceDesiredContinuousInsulationInsertionPointCommon(bp);
        dsmtSurfaceHelper.ComputeOtherHLFCLF(bp, 'Exterior Wall', 'H');
        dsmtSurfaceHelper.ComputeOtherHLFCLF(bp, 'Exterior Wall', 'C');
        dsmtSurfaceHelper.GetIndexOfFirstFramingLayer(layers);
        dsmtSurfaceHelper.ComputeSurfaceLayersRValue(bp,0,layers.size()-1,false);
        dsmtSurfaceHelper.LayerContainMasnory(layers);
        dsmtSurfaceHelper.ComputeBasementWallLinEffCommon(bp,'H');
        dsmtSurfaceHelper.ComputeBasementWallLinEffCommon(bp,'C');
        dsmtSurfaceHelper.AdjustCoolingIndoorTemp(65,'Fully Used');

        Test.stopTest();
    }

    @isTest static void test5() 
    {
        Set<Id> eaId = new Set<Id>();
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
        eaId.add(eaList[0].Id);

        Test.startTest();

        List<Thermal_Envelope_Type__c> tetList = [Select Id From Thermal_Envelope_Type__c Where RecordType.Name = 'Attic'];
        Attic_Access_and_Whole_House_Fan__c aw = new Attic_Access_and_Whole_House_Fan__c();
        aw.Thermal_Envelope_Type__c = tetList[0].Id;
        insert aw;

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);


        dsmtSurfaceHelper.AllFloorsProjectedArea(bp);
        dsmtSurfaceHelper.AllCeilingProjectedArea(bp);
        dsmtSurfaceHelper.ComputeSurfaceAtticAccessArea(bp,tetList[0].Id);

        Test.stopTest();
    }

    @isTest static void test6() 
    {
        Set<Id> eaId = new Set<Id>();
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
        eaId.add(eaList[0].Id);

        List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:eaList[0].Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = eaList[0].Id;
            insert te;        
        }

        Thermal_Envelope_Type__c AtticObj = new Thermal_Envelope_Type__c();
        AtticObj.Energy_Assessment__c = eaList[0].Id;
        AtticObj.Name = 'Windows & Skylights';
        AtticObj.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Windows & Skylights').getRecordTypeId();
        AtticObj.Thermal_Envelope__c = te.Id;
        insert atticObj;

        Test.startTest();
        

        dsmtFuture.Createwindowset(atticObj.Thermal_Envelope__c,atticObj.Id);
        
        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
        dsmtSurfaceHelper.ComputeWindowAreaByFace(bp, 'Front');
        dsmtSurfaceHelper.ComputeWindowAreaByFace(bp, 'Left');

        
        Test.stopTest();
    }
    
}