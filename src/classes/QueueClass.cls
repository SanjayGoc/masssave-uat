public class QueueClass implements Queueable {
   
    Id roIdNew;
  
    public QueueClass(Id roId) {
        roIdNew= roID;
    }
    public void execute(QueueableContext context) {
        //insert newetaList;
        Route_Optimization__c ro = [select id,Service_Date__c from Route_Optimization__c where id =: roIdNew];
            
        dsmtCallOut.loginDetail ldObj = new dsmtCallOut.loginDetail();
           
            ldObj.userName = Login_Detail__c.getInstance().UserName__c;
            ldObj.password = Login_Detail__c.getInstance().Password__c;
            ldObj.orgID = Login_Detail__c.getInstance().Org_Id__c;
            ldObj.securityToken = Login_Detail__c.getInstance().Security_Token__c;
            ldObj.serverURL = Login_Detail__c.getInstance().Server_URL__c;
            
            dsmtCallOut.Body bodyobj = new dsmtCallOut.Body();
            bodyobj.optimizerId = ro.Id;
            bodyobj.fromDate = ro.Service_Date__c;
            bodyobj.toDate = ro.Service_Date__c.Adddays(3);
            bodyobj.currentTime = '';
            bodyobj.TimeZone = '-7';
            bodyobj.BatchMode = true;
            bodyObj.loginDetail  = ldObj;
            
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            req.setEndpoint(System_Config__c.getInstance().URL__c+'OptimizeSchedule?type=json');
            req.setMethod('POST');
            String body = JSON.serialize(bodyObj);
            
            system.debug('--body ---'+body);
            req.setBody(body);
            req.setHeader('content-type', 'application/json');
            http = new Http();
            if(!Test.isrunningTest()){
                res = http.send(req);
                System.debug(res.getBody());
            }
            
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'support+optimizeRoute@unitedcloudsolutions.com'};
            mail.setToAddresses(toAddresses);
            mail.setSenderDisplayName(UserInfo.getName());
            mail.setPlainTextBody('Route Optimization run for '+ro.Service_Date__c);
            mail.SetSubject('Route Optimization run for '+ro.Service_Date__c);
            // mail.setHtmlBody(htmlBody);
            if(!Test.isrunningTest()){
                Messaging.SendEmailResult [] sr = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
            
    }
}