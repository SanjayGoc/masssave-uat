public with sharing class Resi_SendApplicationToQueueExtn {
    public Review__c Revw{get;set;}
    public Review__c newRev{get;set;}
    
    public string selectedAction{get;set;}
    public string noteTitle{get;set;}
    public string noteBody{get;set;}
    public String selectedExcptions{set;get;}
    public string selectedStatus{get;set;}
    public string conts{get;set;}
    public string notesStr{get;set;}
    public boolean isDisplayForResubmit{get;set;}
    public List<Attachment__c> attchList{get;set;}
    private string stage;
    public boolean billAdjOnly{get;set;}
    public boolean onSite{get;set;}
    public boolean ContactAndSched{get;set;}
    public Boolean reSubmitted{get;set;}
    public Boolean schdPost{get;set;}
    public boolean ResubmitAndSched{get;set;}
    public Boolean schedOnly{get;set;}
        
    public string profileName = '';
    
    private final ApexPages.standardController theController;
    Date Schdate = null;
    
    
    public Resi_SendApplicationToQueueExtn(ApexPages.StandardController controller){
        theController = controller;
        selectedAction = '';
        selectedExcptions = '';
        notesStr = '';
        billAdjOnly = false;
        onSite = false;
        ContactAndSched = false;
        reSubmitted = false;
        schdPost = false;
        Revw = new Review__c();
        newRev = new Review__c();
        String ReId = controller.getId();
        List<Review__c> lstRev = [select id,Name,RecordType.Name,Refund_Status__c, IIC_Next_Status__c,HPC_Next_Status__c,IIc_Status__c,HPC_Status__c,Status__c,Type__c ,Notes__c,Fail_Reason__c,Failure_Notes__c,Schedule_Date__c,Schedule_End_Date__c,Inspection_Request__c,Is_the_return_scheduled__c,Return_Scheduled_Date__c , 
                                    Return_Scheduled_Duration__c,Photos_provided__c,Return_Schedule_Window__c,Outcome__c,Schedule_Duration__c,Schedule_Window__c,Installed_DAte__c,Permit_documentation_provided__c,Trade_Ally_Account__c,Assignment_Type__c,
                                    Estimated_Resume_Date__c,Have_you_contacted_the_Customer__c,inspection_Request__r.Inspection_Result__c, inspection_Request__r.Billing_Adjustment_Only__c,inspection_Request__r.Inspector_required_on_site__c from Review__c WHERE Id=:ReId ];
                                    
        system.debug('-- Record Size -- '+lstRev.size());
        if(lstRev.size()>0){
            Revw = lstRev[0];
            
            List<User> userList = [select id,profile.Name from User where id =: userinfo.getUserId()];
            
            profileName = userlist.get(0).Profile.Name;
            
            if(profileName != null &&  profileName == 'IIC User'){
                revW.IIc_Status__c = revW.Status__c;
                revW.IIC_Next_Status__c = revW.Status__c;
            }else if(profileName  != null && profileName.contains('HPC')){
                revW.HPC_Status__c = revW.Status__c;
                revW.HPC_Next_Status__c = revW.Status__c;
            }
            
            schDate = Revw.Schedule_Date__c;
            isDisplayForResubmit = true;
            
            if(profileName == 'IIC User' || profileName == 'HPC Office' || profileName == 'DSMTracker Call Center Agent' || profileName == 'DSMTracker Call Center Supervisor'){
                billAdjOnly = Revw.inspection_Request__r.Billing_Adjustment_Only__c;
                onSite = Revw.inspection_Request__r.Inspector_required_on_site__c;
                
                if(Revw.RecordType.Name == 'Post-Inspection Result Review'){
                     /*if(billAdjOnly == false && onSite == false && Revw.Status__c != 'Scheduled' && Revw.Status__c != 'Manager Fail' && Revw.Status__c == 'Customer contacted'){
                          ContactAndSched = true; 
                     }*/
                       
                       if(Revw.Status__c == 'Scheduled' ||Revw.Status__c == 'Manager Fail'){
                          reSubmitted = true;
                          onSite = false;
                          billAdjOnly = false;
                       }else if(Revw.Status__c == 'Customer contacted') {
                          ResubmitAndSched = true;
                          onSite = false;
                          billAdjOnly = false;
                       }else if(Revw.Status__c == 'Availability provided'){
                             schedOnly = true;
                             onSite = false;
                             billAdjOnly = false;
                        }else if(billAdjOnly == false && onSite == false){
                            ContactAndSched = true; 
                        }
                       
                    }
            }
            
            if(Revw.RecordType.Name == 'Post-Inspection Result Review'){
                schdPost =true;
            }
            
            if(Revw.inspection_Request__c != null && Revw.inspection_Request__r.Inspection_Result__c != null){
                
                String result = Revw.inspection_Request__r.Inspection_Result__c;
                
                List<String> resultlist = result.split(';');
                
                Set<String> checkingResultlist = new Set<String>();
                checkingResultlist.add('Return Emergency');
                checkingResultlist.add('Return On Own');
                checkingResultlist.add('No Fault Return On Own');
                checkingResultlist.add('Re-Return On Own');
                checkingResultlist.add('Return');
                checkingResultlist.add('Re-Return');
                checkingResultlist.add('Contact Customer');
                
                for(String checkresult : checkingResultlist){
                    if(resultlist.contains(checkresult)){
                         isDisplayForResubmit = true;
                         break;
                    }
                }
            }
            stage = lstRev.get(0).Status__c== null ? '' : lstRev.get(0).Status__c;
        }
        
        attchList = [select id,Attachment_Type__c,Post_Inspection_Review__c from Attachment__c where Attachment_Type__c = 'Printed Photos' AND Post_Inspection_Review__c =: ReId];
        
       
        }
    
  /*  public List<SelectOption> getCountries()
{
  List<SelectOption> options = new List<SelectOption>();
        
   Schema.DescribeFieldResult fieldResult =Review__c.Status__c.getDescribe();
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
   for( Schema.PicklistEntry f : ple)
   {
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   }       
   return options;
}*/
    
    public string FailReason{get;set;}
    public String errorMessage{get;set;}
    public PageReference saveAndUpdate(){
        RecommendationTriggerHandler.preventRecursion = true;
      try{  
        system.debug('--revw.Fail_Reason__c---'+revw.Fail_Reason__c);
        system.debug('--revw.Failure_Notes__c---'+revw.Failure_Notes__c);
        system.debug('--newrev.Failure_Notes__c---'+newrev.Failure_Notes__c);
        
         revw.Fail_Reason__c = revw.Fail_Reason__c.replace('~~~',';');
         if(newrev.Failure_Notes__c != null && newrev.Failure_Notes__c != ''){
             notesStr +=  '\n---------------------------------\n';
           //  notesStr +=  newrev.Failure_Notes__c+'\n';
             notesStr += 'User: '+UserInfo.getName()+'\n';
             notesStr += 'Date: '+Date.today().day()+'-'+Date.today().month()+'-'+Date.today().year()+' '+DateTime.now().time()+'\n';//+DateTime.now().hour()+':'+DateTime.now().minute()+'\n';
             notesStr += 'Status: '+revw.Status__c+'\n';
             notesStr += 'Failed Reason: '+revw.Fail_Reason__c+'\n';
             notesStr += 'Failure Notes: '+newrev.Failure_Notes__c+'\n';
             
             if(revw.Failure_Notes__c != null){
                 revw.Failure_Notes__c = notesStr +'\n\n'+revw.Failure_Notes__c;
             }else{
                 revw.Failure_Notes__c = notesStr;
             }
         }
         if(newrev.Notes__c != null && newrev.Notes__c != ''){
             notesStr =  '\n---------------------------------\n';
           //  notesStr +=  newrev.Failure_Notes__c+'\n';
             notesStr += 'User: '+UserInfo.getName()+'\n';
             notesStr += 'Date: '+Date.today().day()+'-'+Date.today().month()+'-'+Date.today().year()+' '+DateTime.now().time()+'\n';//+DateTime.now().hour()+':'+DateTime.now().minute()+'\n';
             notesStr += 'Status: '+revw.Status__c+'\n';
           //  notesStr += 'Failed Reason: '+revw.Fail_Reason__c+'\n';
             notesStr += 'Notes: '+newrev.Notes__c+'\n';
             
             if(revw.Notes__c != null){
                 revw.Notes__c = notesStr +'\n\n'+revw.Notes__c;
             }else{
                 revw.Notes__c = notesStr;
             }
         }
         String RevId = revw.Id;
         if(schDate != null && revw.Schedule_Date__c == null){
             revw.Schedule_Date__c = schDate;
         }
         
         if(revw.Status__c == 'Batched' /*|| revw.Status__c == 'Failed Completion Review'*/){
             dsmtCreateEAssessRevisionController.StopEATrigger = true;
             dsmtCreateEAssessRevisionController.stopTrigger = true;
           //  dsmtCreateEAssessRevisionController.stopExcReviewtrigger = true;
             InvoiceExceptions.isStopValidateException = true;
             ReviewTriggerHandler.ReviewBatched = true;
             dsmtRecommendationHelper.stopRecPayInvLinetrigger = true;
         }
         update revw;
         //theController.save();
         
         List<Review__c> revList = [select id from Review__c where id =: RevId ];
         
         if(revList != null && revList.size() > 0){
             return new PageReference('/'+revw.id);
         }else{
             return new PageReference('/home/home.jsp');
         }
       }catch(Exception e){
           errorMessage = e.getMessage();
           if(errorMessage.contains('Do_you_dispute_the_Billing_Adjustment__c')){
               errorMessage = 'Do you dispute the Billing Adjustment? field is required if Inspection Result contains value of Billing Adjustment.';
           }
           return null;
       }  
    }
    
    public PageReference Cancel(){
        return new PageReference('/'+revw.id);
    }
    
}