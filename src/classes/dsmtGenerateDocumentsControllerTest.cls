@istest
public class dsmtGenerateDocumentsControllerTest
{
  @testSetup static void setup() {
        List<APXTConga4__Conga_Template__c> templates = new List<APXTConga4__Conga_Template__c>();
        APXTConga4__Conga_Template__c template = new APXTConga4__Conga_Template__c();
        template.Unique_Template_Name__c = 'Enclosed Cavity Insulation Template';
        templates.add(template);
        
        APXTConga4__Conga_Template__c template1 = new APXTConga4__Conga_Template__c();
        template1.Unique_Template_Name__c = 'Master Disclosure Form';
        templates.add(template1);
        
        APXTConga4__Conga_Template__c template2 = new APXTConga4__Conga_Template__c();
        template2.Unique_Template_Name__c = 'Clothes Washer Rebate Form Template';
        templates.add(template2);
        
        APXTConga4__Conga_Template__c template3 = new APXTConga4__Conga_Template__c();
        template3.Unique_Template_Name__c = 'Early Refrigerator Rebate Form Template';
        templates.add(template3);
        
        APXTConga4__Conga_Template__c template4 = new APXTConga4__Conga_Template__c();
        template4.Unique_Template_Name__c = 'Audit Receipt Template';
        templates.add(template4);
        
        APXTConga4__Conga_Template__c template5 = new APXTConga4__Conga_Template__c();
        template5.Unique_Template_Name__c = '1216-MS-Statewide-607806-IC Rated Recessed Lights Form-CR-R3b';
        templates.add(template5);
        
        APXTConga4__Conga_Template__c template6 = new APXTConga4__Conga_Template__c();
        template6.Unique_Template_Name__c = '1216-MS-Statewide-607806-IC Rated Recessed Lights Form-HPC-R3b';
        templates.add(template6);
        
        APXTConga4__Conga_Template__c template7 = new APXTConga4__Conga_Template__c();
        template7.Unique_Template_Name__c = 'Early CAC and CHP Rebate Form Template';
        templates.add(template7);
        
        APXTConga4__Conga_Template__c template8 = new APXTConga4__Conga_Template__c();
        template8.Unique_Template_Name__c = 'Permit Authorization Customer Signoff Template';
        templates.add(template8);
        
        APXTConga4__Conga_Template__c template9 = new APXTConga4__Conga_Template__c();
        template9.Unique_Template_Name__c = 'Early Boiler Replacement Rebate Form Template';
        templates.add(template9);
        
        APXTConga4__Conga_Template__c template10 = new APXTConga4__Conga_Template__c();
        template10.Unique_Template_Name__c = 'Early Furnace Replacement Rebate Form';
        templates.add(template10);
        
        APXTConga4__Conga_Template__c template11 = new APXTConga4__Conga_Template__c();
        template11.Unique_Template_Name__c = 'Customer Receipt SHV Form';
        templates.add(template11);
        
        APXTConga4__Conga_Template__c template12 = new APXTConga4__Conga_Template__c();
        template12.Unique_Template_Name__c = 'Specified Measures Agreement Form Template';
        templates.add(template12);
        
        APXTConga4__Conga_Template__c template13 = new APXTConga4__Conga_Template__c();
        template13.Unique_Template_Name__c = 'Eversource HEAT Loan Intake Form Template';
        templates.add(template13);
      
        APXTConga4__Conga_Template__c template14 = new APXTConga4__Conga_Template__c();
        template14.Unique_Template_Name__c = 'Barrier Incentive Form';
        templates.add(template14);

        APXTConga4__Conga_Template__c template15 = new APXTConga4__Conga_Template__c();
        template15.Unique_Template_Name__c = 'Duct Seal Verification Form';
        templates.add(template15);
        
        insert templates;
        
        List<APXTConga4__Conga_Merge_Query__c> queries = new List<APXTConga4__Conga_Merge_Query__c>();
        APXTConga4__Conga_Merge_Query__c query1 = new APXTConga4__Conga_Merge_Query__c();
        query1.Unique_Conga_Query_Name__c = 'Enclosed Cavity Insulation Query';
        queries.add(query1);
        
        APXTConga4__Conga_Merge_Query__c query2 = new APXTConga4__Conga_Merge_Query__c();
        query2.Unique_Conga_Query_Name__c = 'Master Disclosure Query';
        queries.add(query2);
        
        APXTConga4__Conga_Merge_Query__c query3 = new APXTConga4__Conga_Merge_Query__c();
        query3.Unique_Conga_Query_Name__c = 'Early Refrigerator Rebate Form Query';
        queries.add(query3);
        
        APXTConga4__Conga_Merge_Query__c query4 = new APXTConga4__Conga_Merge_Query__c();
        query4.Unique_Conga_Query_Name__c = 'Audit Receipt SHV Query';
        queries.add(query4);
        
        APXTConga4__Conga_Merge_Query__c query5 = new APXTConga4__Conga_Merge_Query__c();
        query5.Unique_Conga_Query_Name__c = 'Clothes Washer Rebate Form Query';
        queries.add(query5);

        APXTConga4__Conga_Merge_Query__c query6 = new APXTConga4__Conga_Merge_Query__c();
        query6.Unique_Conga_Query_Name__c = 'IC Recessed Lights Form (CR Or HPC) 1';
        queries.add(query6);
        
        APXTConga4__Conga_Merge_Query__c query7 = new APXTConga4__Conga_Merge_Query__c();
        query7.Unique_Conga_Query_Name__c = 'Early CAC and CHP Rebate Form Query';
        queries.add(query7);
        
        APXTConga4__Conga_Merge_Query__c query8 = new APXTConga4__Conga_Merge_Query__c();
        query8.Unique_Conga_Query_Name__c = 'Permit Authorization Customer Signoff Query';
        queries.add(query8);
    
        APXTConga4__Conga_Merge_Query__c query9 = new APXTConga4__Conga_Merge_Query__c();
        query9.Unique_Conga_Query_Name__c = 'Early Boiler Replacement Rebate Form Query';
        queries.add(query9);
        
        APXTConga4__Conga_Merge_Query__c query10 = new APXTConga4__Conga_Merge_Query__c();
        query10.Unique_Conga_Query_Name__c = 'Customer Receipt SHV Query';
        queries.add(query10);
        
        APXTConga4__Conga_Merge_Query__c query11 = new APXTConga4__Conga_Merge_Query__c();
        query11.Unique_Conga_Query_Name__c = 'Specified Measures Agreement Form Query';
        queries.add(query11);

        APXTConga4__Conga_Merge_Query__c query12 = new APXTConga4__Conga_Merge_Query__c();
        query12.Unique_Conga_Query_Name__c = 'Early Furnace Replacement Rebate Form Query';
        queries.add(query12);
        
        APXTConga4__Conga_Merge_Query__c query13 = new APXTConga4__Conga_Merge_Query__c();
        query13.Unique_Conga_Query_Name__c = 'Barrier Detail For Incentive Form Query';
        queries.add(query13);

        APXTConga4__Conga_Merge_Query__c query14 = new APXTConga4__Conga_Merge_Query__c();
        query13.Unique_Conga_Query_Name__c = 'Duct Seal Verification Form';
        queries.add(query14);
        
        insert queries;  
    }

    public static testmethod void runtest1()
    {
        dsmtCreateEAssessRevisionController.stopTrigger = true;
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        
        Test.setCurrentPageReference(new PageReference('Page.dsmtGenerateDocuments')); 
        System.currentPageReference().getParameters().put('id', ea.id);
    
        Attachment__c attach=new Attachment__c();        
        attach.Attachment_Name__c='test With Signature.pdf';
        attach.Energy_Assessment__c = ea.Id;
        attach.Status__c='Completed';
        //attach.Attachment_Type__c='attachType';
        attach.Attachment_Type__c='Contract';
        insert attach;
        
        Attachment__c attach2=new Attachment__c();        
        attach2.Attachment_Name__c='test With Signature.pdf';
        attach2.Energy_Assessment__c = ea.Id;
        attach2.Status__c='Completed';
        //attach.Attachment_Type__c='attachType';
        attach2.Attachment_Type__c='Audit Report';
        insert attach2;
        
        
        Attachment att1 =new Attachment();
        att1.Name='test';
        
        att1.ParentId=attach.Id;
        att1.body=blob.valueOf('test');
        insert att1;
        
        dsmtGenerateDocumentsController dgdc = new dsmtGenerateDocumentsController();
        dgdc.newCustomAttachmentId=attach.id;
        dgdc.checkCongaStatus();
        dgdc.clearMap = false;
        dgdc.documents=new List<String>();
        dgdc.documents.add('Clothes Washer Rebate Form');
        dgdc.documents.add('Enclosed Cavity Sheet');
        dgdc.documents.add('Early CAC and CHP Rebate Form');
        dgdc.documents.add('Early Boiler Replacement');
        dgdc.documents.add('Early Furnace Replacement Rebate Form');
        dgdc.documents.add('Barrier Incentive Form');
        dgdc.documents.add('Duct Seal Verification Form');
        dgdc.counter=1;
        
        test.startTest();
        dgdc.documents.add(1,'Clothes Washer Rebate Form');
        dgdc.generateCongaDocument();
        dgdc.documents.add(1,'Early Boiler Replacement');
        dgdc.generateCongaDocument();
        dgdc.documents.add(1,'Early Furnace Replacement Rebate Form');
        dgdc.generateCongaDocument();
        dgdc.documents.add(1,'Early Refrigerator Rebate Form');
        dgdc.generateCongaDocument();
        dgdc.documents.add(1,'Audit Receipt Form');
        dgdc.generateCongaDocument();
        dgdc.documents.add(1,'Master Disclosure Form');
        dgdc.generateCongaDocument();
        dgdc.documents.add(1,'HEAT Loan Intake Form');
        dgdc.generateCongaDocument();
        dgdc.documents.add(1,'Barrier Incentive Form');
        dgdc.generateCongaDocument();
        
        dgdc.validateSubtypes();
        dgdc.saveOutboundConcate();
        dgdc.getInvalidDocsJson();
        dgdc.getDocsJson();
        dgdc.cancel();
       
        dgdc.documents.add(1,'Enclosed Cavity Sheet');
        dgdc.generateCongaDocument();
        
        dgdc.documents.add(1,'IC Rated Recessed Rated lights Sign off');
        dgdc.generateCongaDocument();

        dgdc.documents.add(1,'Early CAC and CHP Rebate Form');
        dgdc.generateCongaDocument();
        
        dgdc.documents.add(1,'Permit Authorization Form');
        dgdc.generateCongaDocument();
        
        dgdc.documents.add(1,'Customer Receipt SHV Form');
        dgdc.generateCongaDocument();
        
        dgdc.documents.add(1,'Specified Measures Agreement');
        dgdc.generateCongaDocument();
        dgdc.checkCongaStatus();
        
        dgdc.documents.add(1,'Duct Seal Verification Form');
        dgdc.generateCongaDocument();
        dgdc.checkCongaStatus();
        
        Attachment__c att = new Attachment__c();
        att.Attachment_Type__c = 'Specified Measures Agreement';
        insert att;
        
        test.stopTest();
    }
}