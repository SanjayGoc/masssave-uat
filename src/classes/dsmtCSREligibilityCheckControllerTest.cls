@isTest
Public class dsmtCSREligibilityCheckControllerTest{
    @isTest
    public static void runTest(){
    test.starttest();
            Account  acc= Datagenerator.createAccount();
            acc.Billing_Account_Number__c= 'Gas-~~~123445';
            insert acc;
            
            Account acc2 = new Account();
            acc2.Name = 'Xcel Energy NM';
            acc2.Billing_Account_Number__c= 'Gas-~~~1234';
            acc2.Utility_Service_Type__c= 'Gas';
            acc2.Account_Status__c= 'Active';
            insert acc2;
            
            Account acc3 = new Account();
            acc3.Name = 'Xcel Energy NM';
            acc3.Billing_Account_Number__c= 'Ele-~~~1234';
            acc3.Utility_Service_Type__c= 'Electric';
            acc3.Account_Status__c= 'Active';
            insert acc3;
            Premise__c pre= Datagenerator.createPremise();
            insert pre;
            Customer__c cust = Datagenerator.createCustomer(acc2.id,pre.id);
            //cust.Electric_Account__c=acc3.id;
            cust.Gas_Account__c=acc2.id;
            insert cust;
            
            Call_List__c callListObj = new Call_List__c();
            callListObj.Status__c = 'Active';
            insert callListObj;
            
            Lead l = new Lead();
            l.LastName = 'test';
            l.Company = 'test';
            l.Read_date__c = Date.Today()-2;
            insert l;
            
            Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
            obj.Call_List__c = callListObj.Id;
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            obj.Followup_Date__c = date.Today();
            obj.Customer__c= cust.id;
            insert obj;
            
            
            /* Task tsk = new Task();    
            tsk.WhatId = obj.Id;
            // tsk.WhoId = l.Id;
            tsk.CallDisposition = 'Outbound';
            tsk.Status = 'Completed';
            tsk.Subject = ' Call To ';
            tsk.Description = 'test';
            tsk.ActivityDate = Date.Today();
            tsk.Type = 'Call';
            insert tsk;
            
            Note n = new Note();
            n.ParentId = obj.Id;
            n.Title = 'test';
            n.Body = 'test';
            insert n;*/
            Skill__c sk= Datagenerator.createSkill();
            insert sk;
            
            
            Location__c loc= Datagenerator.createLocation();
            insert loc;
            Employee__c em = Datagenerator.createEmployee(loc.id);
            em.Status__c='Approved';
            insert em;
            Employee__c em2 = Datagenerator.createEmployee(loc.id);
            em2.Status__c='Approved';
            insert em2;
            
            /*Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
            insert wt;*/
            Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
            emskill.Survey_Score__c=89;
            insert emskill;
            Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
            Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
            insert woTypeList;
            Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
            insert rsk;
            Eligibility_Check__c EL= Datagenerator.createELCheck();
           /* EL.Workorder_Type__c=woTypeList.Id;
            EL.Service_Address__c='test';
            EL.City__c='test';
            EL.Start_Date__c=date.today().addDays(-5);
            EL.End_Date__c=date.today().addDays(5);
            update EL;*/
            EL.How_many_units__c='Single family';
            EL.How_many_do_you_own__c  = '1';
          
            update EL;
            Program_Eligibility__c PE= Datagenerator.createProgramEL();
            
            Customer__c cust2 = Datagenerator.createCustomer(acc3.Id, pre.id);
            cust2.Electric_Account__c=acc3.id;
           // cust.Gas_Account__c=acc2.id;
           // update cust;
            insert cust2;
            Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
             customer_Eligibility__c  cEL= new Customer_Eligibility__c (Eligibility_Check__c =EL.id,Electric_Account__c='Electric',Gas_Account__c='Gas');
           cEL.Workorder_Type__c=woTypeList.Id;
            cEL.Gas_Customer__c= cust.id;
            //cEL.Electric_Customer__c=cust2.Id;
            cEL.Interested_in_receiving_a_visit__c = false;
            cEL.Excluded_From_Scheduling__c = false;
            insert cEL;
           
           // customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
           /* cEL.Workorder_Type__c=woTypeList.Id;
            cEL.Gas_Customer__c= cust.id;
            cEL.Interested_in_receiving_a_visit__c = false;
            cEL.Excluded_From_Scheduling__c = false;
            update cEL;*/
            DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
            dsmt.Trade_Ally_Account__c =Tacc.id; 
            update dsmt;
            Appointment__c app= Datagenerator.createAppointment(EL.Id);
            app.DSMTracker_Contact__c=dsmt.id;
            update app;
            
            customer_Eligibility__c cEL2= Datagenerator.createCustomerEL(EL.id);
            cEL2.Eligibility_Check__c = EL.id;
            cEL2.Workorder_Type__c=woTypeList.Id;
            //cEL2.Gas_Customer__c= cust.id;
            cEL2.Electric_Customer__c=cust2.Id;
            cEL2.Interested_in_receiving_a_visit__c = false;
            cEL2.Excluded_From_Scheduling__c = false;
            // cEL2.Gas_Customer__c= cust.id;
            update cEL2;
            
            ApexPages.currentPage().getParameters().put('clId',obj.id);
            
            
            dsmtCSREligibilityCheckController controller = new dsmtCSREligibilityCheckController();
            controller.CreateEligibilityCheckRecord();
            controller.callListLineItemId =obj.id;
            controller.customerId=cust.id;
            
            dsmtCSREligibilityCheckController.GECheckWrapper wrap= new dsmtCSREligibilityCheckController.GECheckWrapper();
            controller.customerId= cust.Id;
            controller.eleCheckId=EL.ID;
            controller.WoTypeId=woTypeList.Id;
            // controller.SelectedWorkTeam=wt.id;
            controller.SelectedEmployee=em.id+'~~~'+em2.Id;
            controller.custInteraction =EL;
            controller.custInteraction.Start_Date__c = Date.Today();
            controller.custInteraction.End_Date__c = Date.Today().Adddays(32);
            controller.custInteraction.Gas_Account_Holder_First_Name__c = 'testF';
          controller.custInteraction.Gas_Account_Holder_Last_Name__c = 'testL';
            controller.saveEligibility();
            controller.getAppt();
            //controller.CreateWorkOrder();
            //controller.getApartmentList();
            dsmtCSREligibilityCheckController.getContractors();
            dsmtCSREligibilityCheckController.fetchCustomer(obj.Id);
            dsmtCSREligibilityCheckController.LetsGo(false,false);
            controller.SaveRecord();
            
            dsmtCSREligibilityCheckController.getContactValues(EL.Id);
            dsmtCSREligibilityCheckController.checkZipEligibility('1234','test','2','1','test',false,false);
            dsmtCSREligibilityCheckController.GetCity('1234');
            dsmtCSREligibilityCheckController.AccountCheck('1Gas-1234~~~1234','test','1234','Motown','');
            dsmtCSREligibilityCheckController.checkGEUtility('Gas');
            controller.getEmployee();
            dsmtCSREligibilityCheckController.CheckCustomer('test','test','test','test');
           //controller.getAppt();
            controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
            controller.CreateWorkOrder();
            EL.How_many_units__c='2 units';
            EL.How_many_do_you_own__c  = '1';
            update EL;
            controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
            
            controller.GetWoType();
            controller.eleCheckId=EL.ID;
           // controller.CreateWorkOrder();
            
            controller.upsertCustomerEligibility();
            controller.EligibilityFaliRecord();
           // controller.VerifyMultiFamily();
            controller.getWeeks();
            controller.getDays();
            controller.callListLineItemId=obj.id;
            
            dsmtCSREligibilityCheckController.checkonlyZipEleigibility('1234');
            controller.getTimes();
           // dsmtCSREligibilityCheckController.accountsearch('1Gas-1234~~~1234','test','test','Gas','test','test','1234');
            dsmtCSREligibilityCheckController.CreateAccount('1Gas-1234~~~1234','test','test','Gas','test','test','1234');
            dsmtCSREligibilityCheckController.ValidateAddress('test','test','1234');
             controller.callListLineItemId =obj.id;
           //  controller.updateRecord();
           // controller.updateDraft();
            
            
            
            //  controller.dsmtVerifyMultiFamily();
           // controller.getApartmentList();
         test.stoptest();
      //  controller.cloneCallScriptLineItem(obj.Id);
        
    }
    @isTest
    public static void runTest1(){
    test.starttest();
            Account  acc= Datagenerator.createAccount();
            acc.Billing_Account_Number__c= 'Gas-~~~123445';
            insert acc;
            
            Account acc2 = new Account();
            acc2.Name = 'Xcel Energy NM';
            acc2.Billing_Account_Number__c= 'Gas-~~~1234';
            acc2.Utility_Service_Type__c= 'Gas';
            acc2.Account_Status__c= 'Active';
            insert acc2;
            
            Account acc3 = new Account();
            acc3.Name = 'Xcel Energy NM';
            acc3.Billing_Account_Number__c= 'Ele-~~~1234';
            acc3.Utility_Service_Type__c= 'Electric';
            acc3.Account_Status__c= 'Active';
            insert acc3;
            Premise__c pre= Datagenerator.createPremise();
            insert pre;
            Customer__c cust = Datagenerator.createCustomer(acc2.id,pre.id);
            //cust.Electric_Account__c=acc3.id;
            cust.Gas_Account__c=acc2.id;
            insert cust;
            
            Call_List__c callListObj = new Call_List__c();
            callListObj.Status__c = 'Active';
            insert callListObj;
            
            Lead l = new Lead();
            l.LastName = 'test';
            l.Company = 'test';
            l.Read_date__c = Date.Today()-2;
            insert l;
            
            Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
            obj.Call_List__c = callListObj.Id;
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            obj.Followup_Date__c = date.Today();
            obj.Customer__c= cust.id;
            insert obj;
            
            
            /* Task tsk = new Task();    
            tsk.WhatId = obj.Id;
            // tsk.WhoId = l.Id;
            tsk.CallDisposition = 'Outbound';
            tsk.Status = 'Completed';
            tsk.Subject = ' Call To ';
            tsk.Description = 'test';
            tsk.ActivityDate = Date.Today();
            tsk.Type = 'Call';
            insert tsk;
            
            Note n = new Note();
            n.ParentId = obj.Id;
            n.Title = 'test';
            n.Body = 'test';
            insert n;*/
            Skill__c sk= Datagenerator.createSkill();
            insert sk;
            
            
            Location__c loc= Datagenerator.createLocation();
            insert loc;
            Employee__c em = Datagenerator.createEmployee(loc.id);
            em.Status__c='Approved';
            insert em;
            Employee__c em2 = Datagenerator.createEmployee(loc.id);
            em2.Status__c='Approved';
            insert em2;
            
            /*Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
            insert wt;*/
            Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
            emskill.Survey_Score__c=89;
            insert emskill;
            Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
            Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
            insert woTypeList;
            Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
            insert rsk;
            Eligibility_Check__c EL= Datagenerator.createELCheck();
           /* EL.Workorder_Type__c=woTypeList.Id;
            EL.Service_Address__c='test';
            EL.City__c='test';
            EL.Start_Date__c=date.today().addDays(-5);
            EL.End_Date__c=date.today().addDays(5);
            update EL;*/
            EL.How_many_units__c='Single family';
            EL.How_many_do_you_own__c  = '1';
            update EL;
            Program_Eligibility__c PE= Datagenerator.createProgramEL();
            
            Customer__c cust2 = Datagenerator.createCustomer(acc3.Id, pre.id);
            cust2.Electric_Account__c=acc3.id;
           // cust.Gas_Account__c=acc2.id;
           // update cust;
            insert cust2;
            Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
             customer_Eligibility__c  cEL= new Customer_Eligibility__c (Eligibility_Check__c =EL.id,Electric_Account__c='Electric',Gas_Account__c='Gas');
           cEL.Workorder_Type__c=woTypeList.Id;
            cEL.Gas_Customer__c= cust.id;
            cEL.Electric_Customer__c=cust2.Id;
            cEL.Interested_in_receiving_a_visit__c = false;
            cEL.Excluded_From_Scheduling__c = false;
            insert cEL;
           
           // customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
           /* cEL.Workorder_Type__c=woTypeList.Id;
            cEL.Gas_Customer__c= cust.id;
            cEL.Interested_in_receiving_a_visit__c = false;
            cEL.Excluded_From_Scheduling__c = false;
            update cEL;*/
            DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
            dsmt.Trade_Ally_Account__c =Tacc.id; 
            update dsmt;
            Appointment__c app= Datagenerator.createAppointment(EL.Id);
            app.DSMTracker_Contact__c=dsmt.id;
            update app;
            
            customer_Eligibility__c cEL2= Datagenerator.createCustomerEL(EL.id);
            cEL2.Eligibility_Check__c = EL.id;
            cEL2.Workorder_Type__c=woTypeList.Id;
            cEL2.Gas_Customer__c= cust.id;
            cEL2.Electric_Customer__c=cust2.Id;
            cEL2.Interested_in_receiving_a_visit__c = false;
            cEL2.Excluded_From_Scheduling__c = false;
            // cEL2.Gas_Customer__c= cust.id;
            update cEL2;
            
            ApexPages.currentPage().getParameters().put('clId',obj.id);
            
            
            dsmtCSREligibilityCheckController controller = new dsmtCSREligibilityCheckController();
            controller.CreateEligibilityCheckRecord();
            controller.callListLineItemId =obj.id;
            controller.customerId=cust.id;
            controller.saveEligibility();
            dsmtCSREligibilityCheckController.GECheckWrapper wrap= new dsmtCSREligibilityCheckController.GECheckWrapper();
            controller.customerId= cust.Id;
            controller.eleCheckId=EL.ID;
            controller.WoTypeId=woTypeList.Id;
            // controller.SelectedWorkTeam=wt.id;
            controller.SelectedEmployee=em.id+'~~~'+em2.Id;
            controller.custInteraction =EL;
            controller.custInteraction.Start_Date__c = Date.Today();
            controller.custInteraction.End_Date__c = Date.Today().Adddays(32);
            controller.getAppt();
            //controller.CreateWorkOrder();
            //controller.getApartmentList();
            dsmtCSREligibilityCheckController.getContractors();
            dsmtCSREligibilityCheckController.fetchCustomer(obj.Id);
            dsmtCSREligibilityCheckController.LetsGo(false,false);
            controller.SaveRecord();
            
            dsmtCSREligibilityCheckController.getContactValues(EL.Id);
            dsmtCSREligibilityCheckController.checkZipEligibility('1234','test','2','1','test',false,false);
            dsmtCSREligibilityCheckController.GetCity('1234');
            dsmtCSREligibilityCheckController.AccountCheck('1Gas-1234~~~1234','test','1234','Motown', '');
            dsmtCSREligibilityCheckController.checkGEUtility('Gas');
            //controller.getEmployee();
            dsmtCSREligibilityCheckController.CheckCustomer('test','test','test','test');
           //controller.getAppt();
            controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
          //  controller.CreateWorkOrder();
            EL.How_many_units__c='2 units';
            EL.How_many_do_you_own__c  = '1';
            update EL;
            controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
            
            controller.GetWoType();
            controller.eleCheckId=EL.ID;
            controller.CreateWorkOrder();
            
            //controller.upsertCustomerEligibility();
            controller.EligibilityFaliRecord();
           // controller.VerifyMultiFamily();
            controller.getWeeks();
            controller.getDays();
            controller.callListLineItemId=obj.id;
            
            //dsmtCSREligibilityCheckController.checkonlyZipEleigibility('1234');
            controller.getTimes();
           // dsmtCSREligibilityCheckController.accountsearch('1Gas-1234~~~1234','test','test','Gas','test','test','1234');
            //dsmtCSREligibilityCheckController.CreateAccount('1Gas-1234~~~1234','test','test','Gas','test','test','1234');
            //dsmtCSREligibilityCheckController.ValidateAddress('test','test','1234');
             controller.callListLineItemId =obj.id;
           //  controller.updateRecord();
           // controller.updateDraft();
            
            
            
            //  controller.dsmtVerifyMultiFamily();
           // controller.getApartmentList();
         test.stoptest();
      //  controller.cloneCallScriptLineItem(obj.Id);
        
    }
    
    @isTest
     public static void runTest2(){
    test.starttest();
            Account  acc= Datagenerator.createAccount();
            acc.Billing_Account_Number__c= 'Gas-~~~123445';
            insert acc;
            
            Account acc2 = new Account();
            acc2.Name = 'Xcel Energy NM';
            acc2.Billing_Account_Number__c= 'Gas-~~~1234';
            acc2.Utility_Service_Type__c= 'Gas';
            acc2.Account_Status__c= 'Active';
            insert acc2;
            
            Account acc3 = new Account();
            acc3.Name = 'Xcel Energy NM';
            acc3.Billing_Account_Number__c= 'Ele-~~~1234';
            acc3.Utility_Service_Type__c= 'Electric';
            acc3.Account_Status__c= 'Active';
            insert acc3;
            Premise__c pre= Datagenerator.createPremise();
            insert pre;
            Customer__c cust = Datagenerator.createCustomer(acc2.id,pre.id);
           
           insert cust;
            
            Call_List__c callListObj = new Call_List__c();
            callListObj.Status__c = 'Active';
            insert callListObj;
            
            Lead l = new Lead();
            l.LastName = 'test';
            l.Company = 'test';
            l.Read_date__c = Date.Today()-2;
            insert l;
            
            Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
            obj.Call_List__c = callListObj.Id;
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            obj.Followup_Date__c = date.Today();
            obj.Customer__c= cust.id;
            insert obj;
            
            
            /* Task tsk = new Task();    
            tsk.WhatId = obj.Id;
            // tsk.WhoId = l.Id;
            tsk.CallDisposition = 'Outbound';
            tsk.Status = 'Completed';
            tsk.Subject = ' Call To ';
            tsk.Description = 'test';
            tsk.ActivityDate = Date.Today();
            tsk.Type = 'Call';
            insert tsk;
            
            Note n = new Note();
            n.ParentId = obj.Id;
            n.Title = 'test';
            n.Body = 'test';
            insert n;*/
            Skill__c sk= Datagenerator.createSkill();
            insert sk;
            
            
            Location__c loc= Datagenerator.createLocation();
            insert loc;
            Employee__c em = Datagenerator.createEmployee(loc.id);
            em.Status__c='Approved';
            insert em;
            Employee__c em2 = Datagenerator.createEmployee(loc.id);
            em2.Status__c='Approved';
            insert em2;
            /*Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
            insert wt;*/
            Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
            emskill.Survey_Score__c=89;
            insert emskill;
            Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
            Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
            insert woTypeList;
            Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
            insert rsk;
            Eligibility_Check__c EL= Datagenerator.createELCheck();
           /* EL.Workorder_Type__c=woTypeList.Id;
            EL.Service_Address__c='test';
            EL.City__c='test';
            EL.Start_Date__c=date.today().addDays(-5);
            EL.End_Date__c=date.today().addDays(5);
            update EL;*/
            EL.How_many_units__c='2 units';
            EL.How_many_do_you_own__c  = '2';
            El.Do_you_own_or_rent__c = 'Own';
            update EL;
            Program_Eligibility__c PE= Datagenerator.createProgramEL();
            
            Customer__c cust2 = Datagenerator.createCustomer(acc3.Id, pre.id);
            insert cust2;
            Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
             customer_Eligibility__c  cEL= new Customer_Eligibility__c (Eligibility_Check__c =EL.id,Electric_Account__c='Electric',Gas_Account__c='Gas');
           cEL.Workorder_Type__c=woTypeList.Id;
            //cEL.Gas_Customer__c= cust.Id;
           // cEL.Electric_Customer__c='';
            cEL.Interested_in_receiving_a_visit__c = false;
            cEL.Excluded_From_Scheduling__c = false;
            //cEL.Electric_Customer__c = 
            insert cEL;
           
           // customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
           /* cEL.Workorder_Type__c=woTypeList.Id;
            cEL.Gas_Customer__c= cust.id;
            cEL.Interested_in_receiving_a_visit__c = false;
            cEL.Excluded_From_Scheduling__c = false;
            update cEL;*/
            DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
            dsmt.Trade_Ally_Account__c =Tacc.id; 
            update dsmt;
            Appointment__c app= Datagenerator.createAppointment(EL.Id);
            app.DSMTracker_Contact__c=dsmt.id;
            update app;
            
            customer_Eligibility__c cEL2= Datagenerator.createCustomerEL(EL.id);
            cEL2.Eligibility_Check__c = EL.id;
            cEL2.Workorder_Type__c=woTypeList.Id;
            //cEL2.Electric_Customer__c=cust2.Id;
            cEL2.Interested_in_receiving_a_visit__c = false;
            cEL2.Excluded_From_Scheduling__c = false;
            // cEL2.Gas_Customer__c= cust.id;
            update cEL2;
            
            ApexPages.currentPage().getParameters().put('clId',obj.id);
            
            dsmtCSREligibilityCheckController controller = new dsmtCSREligibilityCheckController();
            controller.callListLineItemId =obj.id;
            controller.customerId=cust.id;
           
            controller.GetWoType();
            controller.eleCheckId=EL.ID;
            
            controller.SelectedEmployee=em.id+'~~~'+em2.Id;
            controller.custInteraction =EL;
            controller.custInteraction.Start_Date__c = Date.Today();
            controller.custInteraction.End_Date__c = Date.Today().Adddays(32);
            controller.getAppt();
            cEL2.Gas_Customer__c= cust.id;
            update cEL2;
            controller.CreateWorkOrder();
            
            controller.EligibilityFaliRecord();
            dsmtCSREligibilityCheckController.accountsearch('1Gas-1234~~~1234','test','test','Gas','test','test','1234');
           // controller.VerifyMultiFamily();
            
           //  controller.updateRecord();
           // controller.updateDraft();
            
          //  controller.getAppt();
            controller.callListLineItemId = obj.id;
              controller.dsmtVerifyMultiFamily();
            controller.getApartmentList();
            try{controller.updateRecord();}catch(Exception ex){}
            try{controller.updateDraft();}catch(Exception ex){}
            controller.VerifyMultiFamily();
         test.stoptest();
      //  controller.cloneCallScriptLineItem(obj.Id);
        
    }
    
}