@istest
public class dsmtSyncAppointmentCntrlTest
{
	@istest
    static void runtest()
    {
        Location__c Lc = new Location__c();
        Lc.name = 'test';
        //Lc.Use_default_address_for_routing__c = true;
        insert Lc; 
        
        //List<Employee__c> lstEmp = new List<Employee__c>();
        
        Employee__c Em = new Employee__c( location__c = Lc.Id);
        Em.Employee_Id__c = 'Test';
        Em.name = 'Test';
        Em.Active__c = true;
        Em.Status__c ='Active';    
      	insert em;
        
        Appointment__c App = new Appointment__c();
        App.Appointment_Status__c = 'Scheduled';
        App.Appointment_Start_Time__c = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        App.Appointment_End_Time__c = datetime.newInstance(2015, 9, 15, 12, 30, 0);
        insert App;
        
        apexpages.currentpage().getparameters().put('empid',em.id);
        apexpages.currentpage().getparameters().put('empid',lc.id);
        
        dsmtSyncAppointmentCntrl cntrl =new dsmtSyncAppointmentCntrl();
        cntrl.saveAndUpdate();
    }
}