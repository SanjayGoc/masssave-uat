public with sharing class InvoiceTriggerHandler implements ITrigger {
    public InvoiceTriggerHandler() {}

    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            
            set<string> invoiceTypeSet = new set<string>{'NSTAR413', 'NSTARGAS413', 'WMECO413'};
            set<string> processSet = new set<string>();
        
            for(Sobject Sobj : trigger.New)
            {
                Invoice__c inv = (Invoice__c)Sobj;
                if(invoiceTypeSet.contains(inv.Invoice_Type__c))
                {
                    processSet.add(inv.Invoice_Type__c);
                }
            }
            
            updateInvoicesUnsubmitted(trigger.New,processSet);
            populateInvocieTypeandProvider(trigger.New,null);
            UpdateAccountOnInvoice(trigger.New);
            updateBillingInfoonInvoice(trigger.New);
            
        } else if (trigger.isUpdate) {
            system.debug('--trigger.OldMap--'+trigger.OldMap);
            
            populateInvocieTypeandProvider(trigger.New,trigger.OldMap);
            //Here we will call before update actions
        } else if (trigger.isDelete) {
            //Here we will call before delete actions
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) {
            //Here we will call after insert actions
            createDefaultInvoices(trigger.new);
        } else if (trigger.isUpdate) {
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }
    
    private static void UpdateAccountOnInvoice(List<Invoice__c> newList){
        Set<String> utilityName = new Set<String>();
        
        for(Invoice__c inv : newList){
            if(inv.Provider__c != null){
                utilityName.add(inv.Provider__c);
            }
        }
        
        if(utilityName.size() > 0){
            List<Account> accList = [select id,name from Account where Name in : utilityName];
            
            if(accList != null && accList.size() > 0){
                
                Map<String,Account> accMap = new Map<String,Account>();
                
                for(Account acc : accList){
                    accMap.put(acc.Name,acc);
                }
                
                for(Invoice__c inv : newList){
                    if(inv.Provider__c != null && accMap.get(inv.Provider__c) != null){
                        inv.Account__c = accMap.get(inv.Provider__c).Id;
                    }
                }
            }
        }    
    }
    
    private static void updateBillingInfoonInvoice(List<Invoice__c> newList){
        set<Id> accID = new Set<Id>();
        
        for(Invoice__c inv : newList){
            if(inv.Account__c != null){
                accID.add(inv.Account__c);
            }
        }
        
        if(accID.size() > 0){
            List<Account> accList = [select BillingStreet,BillingCity,BillingState,BillingPostalCode,Bill_To_Email__c,Bill_To_Phone__c,
                                        Remit_Address__c,Remit_City__c,Remit_State__c,Remit_Postal_Code__c,Contract__c,Bill_to_Attn__c
                                        from Account where id in : accId];
           
           Map<Id,Account> accMap = new Map<Id,Account>();
           
           for(Account acc : accList){
               accMap.put(acc.Id,acc);
           }   
           Account newAcc = null;
           
           for(Invoice__c inv : newList){
                if(inv.Account__c != null && accMap.get(inv.Account__c) != null){
                    
                    newAcc = accMap.get(inv.Account__c);   
                    inv.Billing_Street__c = newAcc.BillingStreet;      
                    inv.Billing_City__c = newAcc.BillingCity;
                    inv.Billing_State_Province__c = newAcc.BillingState;
                    inv.Billing_Zip_Postal_Code__c = newAcc.BillingPostalCode;
                    inv.Bill_To_Email__c = newAcc.Bill_To_Email__c;
                    inv.Bill_To_Phone__c = newAcc.Bill_To_Phone__c;
                    inv.Remit_Address__c = newAcc.Remit_Address__c;
                    inv.Remit_City__c = newAcc.Remit_City__c;
                    inv.Remit_City__c = newAcc.Remit_City__c;
                    inv.Remit_State__c = newAcc.Remit_State__c;
                    inv.Remit_Postal_Code__c = newAcc.Remit_Postal_Code__c;
                    inv.Contract__c = newAcc.Contract__c;
                    inv.Bill_to_Attn__c = newAcc.Bill_to_Attn__c;
                }
            }   
        }
    }
    
    public static void populateInvocieTypeandProvider(list<Invoice__c> newList,Map<Id,SObject> oldMap){
        
        system.debug('--oldMap---'+oldMap);
        
        Date startDate = Date.today().toStartOfMonth();
        Date endDate = startDate.addMonths(1).addDays(-1);
        
        /*String target = '/'; 
        String replacement = '';
        String formatedDate= endDate.format().replace(target, replacement); */
        
            String target = '/'; 
            String replacement = '';                      
            Datetime output = endDate;                       
            String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
            
            
        
        if(oldMap == null){
            for(Invoice__c inv : newList){
                
                if(inv.Invoice_Date__c == null){
                    inv.Invoice_Date__C = endDate;
                }
                if(inv.Invoice_Number__c == null){
                    if(inv.Invoice_Type__c == 'Master (default)'){
                        inv.Invoice_Number__c = formatedDate+'Master';
                    }else{
                        inv.Invoice_Number__c = formatedDate+inv.Invoice_Type__c;
                    }
                }
                for(InvoiceType__c invType : InvoiceType__c.getAll().values()){
                    if(invType.IsActive__c == true && invType.TypeName__c == inv.Invoice_Type__c){
                            inv.Work_Type__c = invType.Work_Type__c;
                            inv.Provider__c = invType.Provider__c ;
                    }
                }
            }
        }else{
            for(Invoice__c inv : newList){
                system.debug('--((invoice__c)OldMap.get(inv.Id)).Invoice_Type__c---'+((invoice__c)OldMap.get(inv.Id)).Invoice_Type__c);
                if(inv.Invoice_Type__c != ((invoice__c)OldMap.get(inv.Id)).Invoice_Type__c){
                    for(InvoiceType__c invType : InvoiceType__c.getAll().values()){
                        if(invType.IsActive__c == true && invType.TypeName__c == inv.Invoice_Type__c){
                                inv.Work_Type__c = invType.Work_Type__c;
                                inv.Provider__c = invType.Provider__c ;
                        }
                    }
                }
            }
        }
    }
   // @Future
    private static void updateInvoicesUnsubmitted(List<SObject> invObj,set<string> processSet)
    {
        
        set<string> statusSet = new set<string>{'New', 'In Process'};
       
        
        list<Invoice__c> invoiceList = invObj;
        
        if(invoiceList.size() > 0)
        {
            for(Invoice__c inv : invoiceList)
            {
                inv.Status__c = 'Unsubmitted';
            }
            
            //update invoiceList;
        }
    }
    
    private static void createDefaultInvoices(list<Invoice__c> newList)
    {
        Id invClientInvRtId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Client Invoice').getRecordTypeId();
        Id invRtId = Schema.SObjectType.Invoice__c.getRecordTypeInfosByName().get('Invoice').getRecordTypeId();
        
        list<Invoice__c> invList = new List<Invoice__c>();
        map<id,Invoice__c> invmap = new map<id,Invoice__c>();
        
        for(Invoice__c inv : newList){
            if(inv.RecordTypeId == invClientInvRtId && inv.Invoice_Type__c == 'Master (default)'){
                list<String> invTypeList = new list<String>();
                
               /* if(inv.Work_Type__c == 'ISM' && inv.Provider__c != null)
                {
                    if(inv.Provider__c.contains('Eversource East Gas'))
                        invTypeList.addAll(new list<String>{'NSTARGAS416', 'NSTARGAS416S', 'NSTARGAS418'});
                    if(inv.Provider__c.contains('Eversource East Electric'))
                        invTypeList.addAll(new list<String>{'NSTAR416', 'NSTAR416S', 'NSTAR418'});
                    if(inv.Provider__c.contains('Eversource West Electric'))
                        invTypeList.addAll(new list<String>{'WMECO416', 'WMECO416S'});
                    if(inv.Provider__c.contains('National Grid Electric'))
                        invTypeList.addAll(new list<String>{'NSTAR418O'});
                }
                else if(inv.Work_Type__c == 'Subwork' && inv.Provider__c != null)
                {
                    if(inv.Provider__c.contains('Eversource East Gas'))
                        invTypeList.addAll(new list<String>{'NSTARGAS413'});
                    if(inv.Provider__c.contains('Eversource East Electric'))
                        invTypeList.addAll(new list<String>{'NSTAR413'});
                    if(inv.Provider__c.contains('Eversource West Electric'))
                        invTypeList.addAll(new list<String>{'WMECO413'});
                }*/
                for(InvoiceType__c invType : InvoiceType__c.getAll().values()){
                    if(invType.IsActive__c == true && invType.Work_Type__c == inv.Work_Type__c && 
                        invType.Provider__c == inv.Provider__c){
                            invTypeList.add(invType.TypeName__c);
                    }
                }
                
                for(String s : invTypeList){
                    Invoice__c invInvoiceRt = new Invoice__c();
                    
                    invInvoiceRt.Invoice__c = inv.Id;
                    invInvoiceRt.RecordTypeId = invRtId;
                    invInvoiceRt.Status__c = inv.Status__c;
                    invInvoiceRt.Invoice_Type__c = s;
                    invInvoiceRt.Invoice_Date__c = inv.Invoice_Date__c;
                    invInvoiceRt.Work_Type__c = inv.Work_Type__c;
                    invInvoiceRt.Provider__c = inv.Provider__c;
                    if(invInvoiceRt.Invoice_Date__c != null){
                        
                        /*String target = '/'; 
                        String replacement = '';
                        String formatedDate= invInvoiceRt.Invoice_Date__c.format().replace(target, replacement); */
                        
                        String target = '/'; 
                        String replacement = '';                      
                        Datetime output = invInvoiceRt.Invoice_Date__c;                       
                        String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);


                        
                        invInvoiceRt.Invoice_Number__c = formatedDate+s;
                    }
                    else
                        invInvoiceRt.Invoice_Number__c = s;
                    
                    invList.add(invInvoiceRt); 
                }
            }
        }
        
        if(invList.size() > 0)
            insert invList;
    }
}