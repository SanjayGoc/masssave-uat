Public Class dsmtFieldToolCallout{

    @Future(callout=true)
    public static void updatePINDetails(String oldPin, String newPin){
        List<User> userlist = [select id,Email,Mobile_PIN__c,Name,Battery_Geo_Sync_Time_Sec__c,User_s_Company__c from User where Mobile_Pin__c =: newPin];
        
         List<MobilePINNewWrapper> pinNewlist = new list<MobilePINNewWrapper>();

         For(User u: userlist){
             MobilePINNewWrapper NewWrapper = new MobilePINNewWrapper();
             NewWrapper.Pin = u.User_s_Company__c + '-'+ u.Mobile_PIN__c;
             NewWrapper.Name = u.Name;
             NewWrapper.Email = u.Email;
             NewWrapper.ImageURL = Mobile_Setting__c.getInstance().PIN_Image_URL__c; 
             NewWrapper.orgId = userinfo.getOrganizationId();
             NewWrapper.BatGeoSyncTime= u.Battery_Geo_Sync_Time_Sec__c!=null ? u.Battery_Geo_Sync_Time_Sec__c.intValue() : 0;
             NewWrapper.role = Mobile_Setting__c.getInstance().Role__c;
             NewWrapper.companyType = Mobile_Setting__c.getInstance().Company_Type__c;
             NewWrapper.fieldType= Mobile_Setting__c.getInstance().Field_Type__c;
             NewWrapper.subFieldType=Mobile_Setting__c.getInstance().Sub_Field_Type__c;
             NewWrapper.timeZone= Mobile_Setting__c.getInstance().Time_Zone__c;
             NewWrapper.oldpin= oldPin != null && oldPin != '' ? u.User_s_Company__c + '-'+ oldPin : '';
             
             pinNewlist.add(NewWrapper);
         }
         if(pinNewlist.Size() > 0){
                HttpRequest req = new HttpRequest();
                Http http = new Http();
                HTTPResponse res = null;
                
                String url = Mobile_Setting__c.getInstance().Update_PIN_Details_URL__c;
                if(url != null){
                   req.setEndpoint(url);
                }
                
                req.setMethod('POST');
                req.setHeader('AuthorizeToken', Mobile_Setting__c.getInstance().Authorize_Token__c);
                req.setTimeOut(120000);
                
                String jsonOut = JSON.serialize(pinNewlist);
                system.debug('--Json jsonOut---'+jsonOut);
               
                req.setBody(jsonOut);
                req.setHeader('content-type', 'application/json');
                http = new Http();
                if(!Test.isRunningTest()){
                    res = http.send(req);
                    System.debug(res.getBody());
                }
         }
    }
    
     @Future(callout=true)
    public static void SyncAppointmentSFDCToMobile(Set<String> appointmentIds,boolean isDelete){
       
        List<Appointment__c> AppList = [Select id,Workorder__c,Workorder__r.Address_Formula__c,Workorder__r.Address__c,Workorder__r.Name,Workorder__r.City__c,Workorder__r.State__c,Workorder__r.Zipcode__c,Workorder__r.Phone__c,Workorder__r.Mobile__c,
                           Customer_Reference__r.Name,Customer_Reference__r.First_Name__c,Customer_Reference__r.Last_Name__c,Customer_Reference__r.Email__c,Customer_Reference__r.Electric_Rate_Code__c,Customer_Reference__r.Gas_Rate_Code__c,
                           Customer_Reference__r.Gas_Provider_Name__c,Customer_Reference__r.Gas_Account_Number__c,Customer_Reference__r.Electric_Provider_Name__c,Customer_Reference__r.Electric_Account_Number__c,
                           Appointment_Start_Date__c,Workorder__r.Program__r.Name,Workorder__r.Emergency__c,Workorder__r.Preferred_Contact_Method__c,Workorder__r.Preferred_Contact_Number_Email__c,Workorder__r.Alert__c,Workorder__r.Priority__c,
                           Workorder__r.GeoCoordinates__Latitude__s,Workorder__r.GeoCoordinates__Longitude__s,Workorder__r.Primary_Provider__c,
                           Workorder__r.Work_Order_Notes__c,workorder__r.Send_Receipt__c,Description__c,owner.Name,Appointment_Status__c,Appointment_Type__c,Appointment_Start_Time__c,Appointment_End_Time__c,Notes__c,
                           DSMTracker_Contact__c,DSMTracker_Contact__r.Portal_user__r.User_s_Company__c, DSMTracker_Contact__r.Portal_user__r.Mobile_pin__c,Workorder__r.Heating_Climate_Zone__c,Workorder__r.Cooling_Climate_Zone__c,
                           Workorder__r.Eligibility_Check__r.Do_you_heat_with_natural_Gas__c,Workorder__r.Eligibility_Check__r.How_many_units__c,Workorder__r.Eligibility_Check__r.Do_you_own_or_rent__c,Workorder__r.Eligibility_Check__r.Do_you_live_there__c,
                           Workorder__r.Eligibility_Check__r.How_did_you_hear_about_this_program__c,Workorder__r.Eligibility_Check__r.Promo_Code__c,Workorder__r.Eligibility_Check__r.Home_Size__c,Workorder__r.Eligibility_Check__r.Home_Style__c,Workorder__r.Eligibility_Check__r.I_heat_my_home_with__c,
                           Workorder__r.Eligibility_Check__r.Year_built_in__c,Workorder__r.Eligibility_Check__r.Attic__c,Workorder__r.Eligibility_Check__r.Thermostats__c,
                           WeatherStation__c,Trade_Ally_Account__r.Internal_Account__c,Trade_Ally_Account__r.FieldTool_TA_name__c  
                           from Appointment__c where DSMTracker_Contact__r.Portal_user__r.Mobile_pin__c != null and Id IN : appointmentIds];
        
        Map<String,List<Appointment__c>> PinToAppMap = new Map<String,List<Appointment__c>>();
        List<String> weatherStationIds = new List<String>();
        for(Appointment__c app : Applist){
            if(app.DSMTracker_Contact__r.Portal_user__r.Mobile_pin__c == null){
               continue;
            }
            
            List<Appointment__c> appmapList = PinToAppMap.get(app.DSMTracker_Contact__r.Portal_user__r.Mobile_pin__c);
            if(appmapList == null){
               appmapList = new List<Appointment__c>(); 
            }
            appmapList.add(app);
            
            if(app.WeatherStation__c != null){
               weatherStationIds.add(app.WeatherStation__c);
            }
            PinToAppMap.put(app.DSMTracker_Contact__r.Portal_user__r.User_s_Company__c +'-'+ app.DSMTracker_Contact__r.Portal_user__r.Mobile_pin__c,appmapList);
        }
        
        Map<String,WeatherStation__c> WeatherStationMap = new Map<String,WeatherStation__c>();
        
        if(weatherStationIds.size() > 0){
              
         List<WeatherStation__c> wlist = [select id,Name,owner.name,CDLatentMult__c,CDMult__c,CDMultPCFM__c,CExposeMult__c,CHeightExp__c,Clg1PctDesignTempDryBulb__c,
                    Clg1PctDesignTempWetBulb__c,ClgGrDiff__c,CNFactor__c,CSolarH__c,CZ1_2__c,Default__c,Elevation__c,HDMult__c,
                    HDMultPCFM__c,HExposeMult__c, HHeightExp__c,HistoricalTempStationName__c,HNFactor__c,HSolarH__c,Htg99PctDesignTemp__c,
                    Latitude__c,LightAverage__c,LightC__c,LightH__c,LightSh__c,Longitude__c,NormalNFactor__c,PostalCode__c,TGround__c,
                    TGroundC__c,TGroundH__c,TMYType__c,WeatherStationName__c,Zip_Code_5__c,
                    (select id,Name,owner.name,TempIncr__c,TempStart__c,TempValues__c,TMYType__c,WeatherStation__c,WeatherStation__r.Name,WeatherStationName__c,WeatherStationPostalCode__c,WSProperty__c from WeatherStationTempratures__r),
                    (Select id,Name,owner.name,Orientation__c,SolarGain__c,TMYType__c,WeatherStation__c,WeatherStation__r.Name,WeatherStationName__c,WeatherStationPostalCode__c from SolarGainByOperations__r)
                    from WeatherStation__c where id in: weatherStationIds];
            for(WeatherStation__c w : wlist){
                WeatherStationMap.put(w.id,w);
            }
        }
        
        if(PinToAppMap.size() > 0){
              
             List<WorkOrderSyncWrapper> wosyncwraplist = new List<WorkOrderSyncWrapper>();
              
             for(String pin : PinToAppMap.keyset()){
                 
                 WorkOrderSyncWrapper womainWrapper = new WorkOrderSyncWrapper();
                 womainwrapper.PIN = pin;
                 womainwrapper.isDelete = isDelete;
                 womainwrapper.CompanyType = Mobile_Setting__c.getInstance().Company_Type__c;
                 womainwrapper.fieldType= Mobile_Setting__c.getInstance().Field_Type__c;
                 womainwrapper.OrgID = userinfo.getOrganizationId();
                 List<Appointment__c> appmtlist = PinToAppMap.get(pin);
                 
                 List<WorkOrderWrapper> wowraplist = new List<WorkOrderWrapper>();
                 
                 for(Appointment__c app : appmtlist){
                      WorkOrderWrapper wowrap = new WorkOrderWrapper();
                      wowrap.customerAndAddressInformation = CheckNull(app.Workorder__r.Address_Formula__c);
                      wowrap.customer = CheckNull(app.Customer_Reference__r.Name);
                      wowrap.customerLastName = CheckNull(app.Customer_Reference__r.Last_Name__c);
                      wowrap.customerFirstName = CheckNull(app.Customer_Reference__r.First_Name__c);
                      wowrap.email = CheckNull(app.Customer_Reference__r.Email__c);
                      wowrap.workorder = CheckNull(app.Workorder__r.Name);
                      wowrap.street = CheckNull(app.Workorder__r.Address__c);
                      wowrap.city = CheckNull(app.Workorder__r.City__c);
                      wowrap.state = CheckNull(app.Workorder__r.State__c);
                      wowrap.zipCode = CheckNull(app.Workorder__r.Zipcode__c);
                      wowrap.mobilePhone = CheckNull(app.Workorder__r.Mobile__c);
                      wowrap.homePhone = CheckNull(app.Workorder__r.Phone__c);
                      wowrap.officePhone = CheckNull(app.Workorder__r.Mobile__c);
                      wowrap.sendReceipt = (app.workorder__r.Send_Receipt__c) ? 'Yes' : 'No';
                      wowrap.serviceDate = app.Appointment_Start_Date__c.year()+'-'+app.Appointment_Start_Date__c.month()+'-'+app.Appointment_Start_Date__c.day();
                      wowrap.program = CheckNull(app.Workorder__r.Program__r.Name);
                      wowrap.emergency = (app.Workorder__r.Emergency__c) ? 'Yes' : 'No';
                      wowrap.preferredContactMethod = CheckNull(app.Workorder__r.Preferred_Contact_Method__c);
                      wowrap.preferredContactPhone = CheckNull(app.Workorder__r.Preferred_Contact_Number_Email__c);
                      wowrap.alert = CheckNull(app.Workorder__r.Alert__c);
                      wowrap.comments = CheckNull(app.Description__c);
                      wowrap.owner = CheckNull(app.owner.name);
                      wowrap.status = CheckNull(app.Appointment_Status__c);
                      wowrap.recordType = CheckNull(app.Appointment_Type__c);
                      wowrap.priority = CheckNull(app.Workorder__r.Priority__c);
                      wowrap.start = app.Appointment_Start_Time__c.format('yyyy-MM-dd hh:mm a');
                      wowrap.endtime = app.Appointment_End_Time__c.format('yyyy-MM-dd hh:mm a');
                      wowrap.note = CheckNull(app.Notes__c);
                      wowrap.workorderNotes = CheckNull(app.workorder__r.Work_Order_Notes__c);
                      wowrap.electricUtility = CheckNull(app.Customer_Reference__r.Electric_Provider_Name__c);
                      wowrap.electricUtilityAccountNumber = CheckNull(app.Customer_Reference__r.Electric_Account_Number__c);
                      wowrap.gasUtilityName = CheckNull(app.Customer_Reference__r.Gas_Provider_Name__c);
                      wowrap.gasUtilityAccountNumber = CheckNull(app.Customer_Reference__r.Gas_Account_Number__c);
                      wowrap.coolingclimatezone = app.Workorder__r.Cooling_Climate_Zone__c != null ? String.valueof(app.Workorder__r.Cooling_Climate_Zone__c) : '';
                      wowrap.heatingclimatezone = app.Workorder__r.Heating_Climate_Zone__c != null ? String.Valueof(app.Workorder__r.Heating_Climate_Zone__c) : '';
                      wowrap.geoCoordinates = app.Workorder__r.GeoCoordinates__Latitude__s + ','+ app.Workorder__r.GeoCoordinates__Longitude__s;
                      //wowrap.geoCoordinates = '34.0723164,-84.29635979999999';
                      
                      wowrap.agreementSigned = 'No'; 
                      wowrap.country = 'USA';
                      wowrap.schedulingInformation = 'None';
                      wowrap.version = '000';
                      wowrap.references = '';
                      wowrap.orgId = userinfo.getOrganizationId();
                      wowrap.CompanyType = Mobile_Setting__c.getInstance().Company_Type__c;
                      wowrap.fieldType= Mobile_Setting__c.getInstance().Field_Type__c;
                      
                      //Intake Information
                      IntakeInformationWrapper intakeinfo = new IntakeInformationWrapper();
                      intakeinfo.WorkOrderId = app.Workorder__c;
                      intakeinfo.WorkOrderNumber = app.Workorder__r.Name;
                      intakeinfo.Pin = pin;
                      intakeinfo.OrgID = userinfo.getOrganizationId();
                      intakeinfo.GasRateCode = app.Customer_Reference__r.Gas_Rate_Code__c;
                      intakeinfo.ElectricRateCode = app.Customer_Reference__r.Electric_Rate_Code__c;
                      intakeinfo.AppointmentType = app.Appointment_Type__c;
                      intakeinfo.HomeHeatingSource = app.Workorder__r.Eligibility_Check__r.Do_you_heat_with_natural_Gas__c;
                      intakeinfo.Units = app.Workorder__r.Eligibility_Check__r.How_many_units__c;
                      intakeinfo.OwnOrRent = app.Workorder__r.Eligibility_Check__r.Do_you_own_or_rent__c;
                      intakeinfo.LiveThere = app.Workorder__r.Eligibility_Check__r.Do_you_live_there__c;
                      intakeinfo.Hear = app.Workorder__r.Eligibility_Check__r.How_did_you_hear_about_this_program__c;
                      intakeinfo.PromoCode = app.Workorder__r.Eligibility_Check__r.Promo_Code__c;
                      intakeinfo.HomeSize = app.Workorder__r.Eligibility_Check__r.Home_Size__c;
                      intakeinfo.HomeStyle = app.Workorder__r.Eligibility_Check__r.Home_Style__c;
                      intakeinfo.BuiltNo = app.Workorder__r.Eligibility_Check__r.Year_built_in__c;
                      intakeinfo.AtticDucts = app.Workorder__r.Eligibility_Check__r.Attic__c;
                      intakeinfo.Thermostats = app.Workorder__r.Eligibility_Check__r.Thermostats__c;
                      intakeinfo.IsClrEs = String.valueof(app.Trade_Ally_Account__r.Internal_Account__c);
                      intakeinfo.TradeAllyAccountName = app.Trade_Ally_Account__r.FieldTool_TA_name__c;
                      intakeinfo.HomeHeatingSourceType = app.Workorder__r.Eligibility_Check__r.I_heat_my_home_with__c;
                      intakeinfo.PrimaryProvider = app.Workorder__r.Primary_Provider__c;
                      
                      wowrap.IntakeInformation = intakeinfo;
                      
                      //Weather Station Information 
                      WeatherStation__c ws = WeatherStationMap.get(app.WeatherStation__c);
                      if(ws != null){
                          
                         WorkOrderWeatherStationDetailWrapper wsdetail = new WorkOrderWeatherStationDetailWrapper();
                         //fill WeatherStation Detail
                            
                            wsdetail.Pin = pin;
                            wsdetail.OrgID = userinfo.getOrganizationId();
                            wsdetail.WeatherStationSfId = ws.id;
                            wsdetail.WeatherStationNumber = ws.name;
                            wsdetail.WeatherStationName = ws.WeatherStationName__c;
                            wsdetail.Owner = ws.owner.name;
                            wsdetail.CDLatentMult = String.ValueOf(ws.CDLatentMult__c);
                            wsdetail.CZ1_2 = String.ValueOf(ws.CZ1_2__c); 
                            wsdetail.CDMult = String.ValueOf(ws.CDMult__c);
                            wsdetail.NormalNFactor = String.ValueOf(ws.NormalNFactor__c);
                            wsdetail.CDMultPCFM = String.ValueOf(ws.CDMultPCFM__c);
                            wsdetail.ClgGrDiff = String.ValueOf(ws.ClgGrDiff__c);
                            wsdetail.CExposeMult = String.ValueOf(ws.CExposeMult__c);
                            wsdetail.Clg1PctDesignTempWetBulb = String.ValueOf(ws.Clg1PctDesignTempWetBulb__c);
                            wsdetail.CHeightExp = String.ValueOf(ws.CHeightExp__c);
                            wsdetail.Clg1PctDesignTempDryBulb = String.ValueOf(ws.Clg1PctDesignTempDryBulb__c);
                            wsdetail.CNFactor = String.ValueOf(ws.CNFactor__c);
                            wsdetail.Htg99PctDesignTemp = String.ValueOf(ws.Htg99PctDesignTemp__c);
                            wsdetail.CSolarH = String.ValueOf(ws.CSolarH__c);
                            wsdetail.HistoricalTempStationName = ws.HistoricalTempStationName__c;
                            wsdetail.Elevation = String.ValueOf(ws.Elevation__c);
                            wsdetail.LightSh = String.ValueOf(ws.HDMult__c);
                            wsdetail.HDMult = String.ValueOf(ws.HDMult__c);
                            wsdetail.Lightc = String.ValueOf(ws.Lightc__c);
                            wsdetail.HDMultPcFM = String.ValueOf(ws.HDMultPcFM__c);
                            wsdetail.LightH = String.ValueOf(ws.LightH__c);
                            wsdetail.HExposeMult = String.ValueOf(ws.HExposeMult__c);
                            wsdetail.HHeightExp = String.ValueOf(ws.HHeightExp__c);
                            wsdetail.LightAverage = String.ValueOf(ws.LightAverage__c);
                            wsdetail.HNFactor = String.ValueOf(ws.HNFactor__c);
                            wsdetail.Postalcode = ws.Postalcode__c;
                            wsdetail.HSolarH = String.ValueOf(ws.HSolarH__c);
                            wsdetail.Longitude = String.ValueOf(ws.Longitude__c);
                            wsdetail.TGround = String.ValueOf(ws.TGround__c);
                            wsdetail.Latitude = String.ValueOf(ws.Latitude__c);
                            wsdetail.TMYType = ws.TMYType__c;
                            wsdetail.stDefault = String.ValueOf(ws.Default__c);
                            wsdetail.Zipcode5 = String.ValueOf(ws.Zip_Code_5__c);
                            wsdetail.TGroundH = String.ValueOf(ws.TGroundH__c);
                            wsdetail.TGroundc = String.ValueOf(ws.TGroundc__c);
                            
                            List<WeatherStationTempratureWrapper> WsTempratureList = new List<WeatherStationTempratureWrapper>();   
                            //fill WSTempraturelist 
                            
                            for(WeatherStationTemprature__c wst: ws.WeatherStationTempratures__r){
                               WeatherStationTempratureWrapper tempwrapper = new WeatherStationTempratureWrapper();
                               
                               tempwrapper.WeatherStationSfId = wst.WeatherStation__c;
                               tempwrapper.WeatherStationNumber = wst.WeatherStation__r.Name;
                               tempwrapper.WeatherStationName = wst.WeatherStationName__c;
                               tempwrapper.WeatherStationTempratureName = wst.Name;
                               tempwrapper.WeatherStationPostalCode =wst.WeatherStationPostalCode__c;
                               tempwrapper.WSProperty = wst.WSProperty__c;
                               tempwrapper.TempStart = String.ValueOF(wst.TempStart__c);
                               tempwrapper.TMYType = wst.TMYType__c;
                               tempwrapper.TempIncr = String.ValueOF(wst.TempIncr__c);
                               tempwrapper.Owner =  wst.owner.name;
                               tempwrapper.TempValues = wst.TempValues__c;
                              
                                WsTempratureList.add(tempwrapper);
                            } 
                             
                            List<SolarGainByOperationDetailWrapper> SolarGainByOpDetailList = new List<SolarGainByOperationDetailWrapper>(); 
                            //fill solarGainByOperation List
                            for(SolarGainByOperation__c sgo: ws.SolarGainByOperations__r){
                                SolarGainByOperationDetailWrapper tempsolarwrapper =new SolarGainByOperationDetailWrapper();
                               
                                tempsolarwrapper.WeatherStationSfId = sgo.WeatherStation__c;
                                tempsolarwrapper.WeatherStationNumber = sgo.WeatherStation__r.Name;
                                tempsolarwrapper.WeatherStationName = sgo.WeatherStationName__c;
                                tempsolarwrapper.SolarGainByOperationName = sgo.Name;
                                tempsolarwrapper.Owner = sgo.owner.name;
                                tempsolarwrapper.Orientation = sgo.Orientation__c;
                                tempsolarwrapper.SolarGain = String.ValueOf(sgo.SolarGain__c);
                                tempsolarwrapper.WeatherStationPostalCode = sgo.WeatherStationPostalCode__c;
                                tempsolarwrapper.TMYType = sgo.TMYType__c;
                                
                                SolarGainByOpDetailList.add(tempsolarwrapper);
                            }
                             
                             
                            wsdetail.WeatherStationTempratureList = WsTempratureList;
                            wsdetail.SolarGainByOperationDetailList = SolarGainByOpDetailList;
                             
                            wowrap.WorkOrderWeatherStationDetail = wsdetail;
                            wowraplist.add(wowrap);
                      }
                 }
                 
                 womainwrapper.AllWorkOrders = wowraplist;
                 wosyncwraplist.add(womainwrapper);
             }
             
             if(wosyncwraplist.size() > 0){
                 
                    HttpRequest req = new HttpRequest();
                    Http http = new Http();
                    HTTPResponse res = null;
                    
                    String url = Mobile_Setting__c.getInstance().SFDC_To_Mobile_Appointment_Sync_URL__c;
                    if(url != null){
                       req.setEndpoint(url);
                    }
                    
                    req.setMethod('POST');
                    
                    req.setTimeOut(120000);
                    req.setHeader('AuthorizeToken', Mobile_Setting__c.getInstance().Authorize_Token__c);


                    String jsonOut = JSON.serialize(wosyncwraplist);
                    system.debug('--Json jsonOut---'+jsonOut);
                    
                    jsonOut = jsonOut.replaceAll('"endtime":','"end":');
                    
                    system.debug('--after replace jsonOut---'+jsonOut);
                    req.setBody(jsonOut);
                    req.setHeader('content-type', 'application/json');
                    http = new Http();
                    if(!Test.isRunningTest()){
                        res = http.send(req);
                        System.debug(res.getBody());
                    }
             }
        }
        
    }
    
    public static string CheckNull(String value){
    
         if(value == null){
            return '';
         }
         
         return value;
    }
    
    @Future(callout=true)
    public static void ProcessSyncWoToSFDC(){
        
        
            dsmtCallOut.loginDetail ldObj = new dsmtCallOut.loginDetail();
            
            ldObj.userName = Mobile_Setting__c.getInstance().UserName__c;
            ldObj.password = Mobile_Setting__c.getInstance().Password__c;
            ldObj.orgID = Login_Detail__c.getInstance().Org_Id__c;
            ldObj.securityToken = Mobile_Setting__c.getInstance().Security_Token__c;
            ldObj.serverURL = Login_Detail__c.getInstance().Server_URL__c;
            
            
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            
            String url = Mobile_Setting__c.getInstance().Sync_Mobile_To_SFDC_URL__c;
            if(url != null){
               req.setEndpoint(url);
            }
            
           
            
            req.setMethod('POST');
            req.setTimeOut(120000);
            String body = JSON.serialize(ldObj);
            system.debug('--body ---'+body);
            req.setBody(body);
            req.setHeader('content-type', 'application/json');
            http = new Http();
         if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
        }
        
    }
    
    
    @Future(callout=true)
    public static void ProcessSyncBatteryGeoInfoToSFDC(){
        
        
            dsmtCallOut.loginDetail ldObj = new dsmtCallOut.loginDetail();
            
            ldObj.userName = Mobile_Setting__c.getInstance().UserName__c;
            ldObj.password = Mobile_Setting__c.getInstance().Password__c;
            ldObj.orgID = Login_Detail__c.getInstance().Org_Id__c;
            ldObj.securityToken = Mobile_Setting__c.getInstance().Security_Token__c;
            ldObj.serverURL = Login_Detail__c.getInstance().Server_URL__c;
            
            
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            
            String url = Mobile_Setting__c.getInstance().Sync_Battery_Geo_Info_To_SFDC__c;
            if(url != null){
               req.setEndpoint(url);
            }
            
            req.setMethod('POST');
            req.setTimeOut(120000);
            String body = JSON.serialize(ldObj);
            system.debug('--body ---'+body);
            req.setBody(body);
            req.setHeader('content-type', 'application/json');
            http = new Http();
         if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
        }
        
    }
    
    Public class DeviceDetailList {

       public Integer WorkOrderId;
       public String WorkOrderNumber;
       public String Pin;
       public String DeviceNumber;
       public String OrgID;
       public String TurndownServiceReason;
       public String Timer;
       public String SystemType;
       public String SystemSize;
       public String SystemAge;
       public String Status;
       public String SignalTest;
       public String Shunt;
       public String ShedRestore;
       public String SerialNumber;
       public String RLA;
       public String Result;
       public String RemovedSerialNumber2;
       public String RemovedSerialNumber1;
       public String RemovedModel2;
       public String RemovedModel1;
       public String RemovedLocation2;
       public String RemovedLocation1;
       public String RemovedDeviceType2;
       public String RemovedDeviceType1;
       public String RemovalReason2;
       public String RemovalReason1;
       public String Programmed2;
       public String Programmed1;
       public String Programmed;
       public String Programmable2;
       public String Programmable1;
       public String Programmable;
       public String PreOperatingTest;
       public String PreMilliAmps;
       public String PostOperatingTest;
       public String PostMilliAmps;
       public String CyclingLevel;
       public String Model;
       public String Measure;
       public String Manufacturer;
       public String MACAddress;
       public String ISO;
       public String InstalledDate;
       public String Efficiency;
       public String DeviceLocation;
       public String DeviceAppInstalled;
       public String ConnectedtoWiFi;
       public String CompletionDate;
       public String Barcode;
    }
    
    Public class MobilePINNewWrapper{
        Public String Pin;
        Public String Name;
        Public String Email;
        Public String role;
        Public String companyType;
        Public String fieldType;
        Public String subFieldType;
        Public String ImageURL;
        Public String timeZone;
        Public String orgId;
        Public String oldpin;
        Public Integer BatGeoSyncTime;
    }  
    
      public class WorkOrderSyncWrapper{
    
        public String PIN;
        public Boolean isDelete;
        public String CompanyType;
        public String fieldType;
        public String OrgID;
        public List<WorkOrderWrapper> AllWorkOrders;     
    }
    
    public class IntakeInformationWrapper{
    
         public String WorkOrderId;
         public String WorkOrderNumber;
         public String Pin;
         public String OrgID;
         public String GasRateCode;
         public String ElectricRateCode;
         public String AppointmentType;
         public String HomeHeatingSource;
         public String Units;
         public String OwnOrRent;
         public String LiveThere;
         public String Hear;
         public String PromoCode;
         public String HomeSize;
         public String HomeStyle;
         public String BuiltNo;
         public String AtticDucts;
         public String Thermostats;
         public String IsClrEs;
         public String TradeAllyAccountName;
         public String HomeHeatingSourceType;
         public String PrimaryProvider;
    }
    
    public class WorkOrderWeatherStationDetailWrapper{
    
        public String WorkOrderId;
        public String WorkOrderNumber;
        public String Pin;
        public String OrgID;
        public String WeatherStationSfId;
        public String WeatherStationNumber;
        public String WeatherStationName;
        public String Owner;
        public String CDLatentMult;
        public String CZ1_2;
        public String CDMult;
        public String NormalNFactor;
        public String CDMultPCFM;
        public String ClgGrDiff;
        public String CExposeMult;
        public String Clg1PctDesignTempWetBulb;
        public String CHeightExp;
        public String Clg1PctDesignTempDryBulb;
        public String CNFactor;
        public String Htg99PctDesignTemp;
        public String CSolarH;
        public String HistoricalTempStationName;
        public String Elevation;
        public String LightSh;
        public String HDMult;
        public String LightC;
        public String HDMultPCFM;
        public String LightH;
        public String HExposeMult;
        public String HHeightExp;
        public String LightAverage;
        public String HNFactor;
        public String  PostalCode;
        public String HSolarH;
        public String Longitude;
        public String TGround;
        public String Latitude;
        public String TMYType;
        public String stDefault;
        public String ZipCode5;
        public String TGroundH;
        public String TGroundC;
       
       public List<WeatherStationTempratureWrapper> WeatherStationTempratureList;   
       public List<SolarGainByOperationDetailWrapper> SolarGainByOperationDetailList;   
    }
    
    public class WeatherStationTempratureWrapper{
    
         public String WeatherStationId;
         public String WeatherStationSfId;
         public String WeatherStationNumber;
         public String WeatherStationName;
         public String WeatherStationTempratureName;
         public String WeatherStationPostalCode;
         public String WSProperty;
         public String TempStart;
         public String TMYType;
         public String TempIncr;
         public String Owner;
         public String TempValues;
    }
    
    public class SolarGainByOperationDetailWrapper{
        
        public String WeatherStationId;
        public String WeatherStationSfId;
        public String WeatherStationNumber;
        public String WeatherStationName;
        public String SolarGainByOperationName;
        public String Owner;
        public String Orientation;
        public String SolarGain;
        public String WeatherStationPostalCode;
        public String TMYType;
    }
    
    public class WorkOrderWrapper{
    
        public String customerAndAddressInformation;
        public String customer;
        public String customerLastName;
        public String customerFirstName;
        public String email;
        public String workorder;
        public String agreementSigned;
        public String street;
        public String city;
        public String state;
        public String zipCode;
        public String country;
        public String mobilePhone;
        public String homePhone;
        public String officePhone;
        public String geoCoordinates;
        public String sendReceipt;
        public String serviceDate;
        public String program;
        public String emergency;
        public String preferredContactMethod;
        public String preferredContactPhone;
        public String alert;
        public String comments;
        public String owner;
        public String status;
        public String recordType;
        public String priority;
        public String schedulingInformation;
        public String start;
        public String endtime;
        public String note;
        public String workorderNotes;
        public String version;
        public String references;
        public String electricUtility;
        public String electricUtilityAccountNumber;
        public String gasUtilityName;
        public String gasUtilityAccountNumber;
        public String heatingclimatezone;
        public String coolingclimatezone;
        public String orgId;
        public String CompanyType;
        public String fieldType;
        public List<DeviceDetailList> deviceDetailList;
        
        public IntakeInformationWrapper IntakeInformation;
        
        public WorkOrderWeatherStationDetailWrapper WorkOrderWeatherStationDetail;
        
        
    }
      
}