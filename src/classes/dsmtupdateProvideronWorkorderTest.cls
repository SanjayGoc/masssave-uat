@isTest
Public class dsmtupdateProvideronWorkorderTest{
    @isTest
    public static void runTest(){
         
        test.starttest();
        
        Customer__c cust = new Customer__c();
        cust.Name = 'test';
        cust.First_Name__c='hemanshu';
        cust.Last_Name__c='patel';
        cust.Service_Zipcode__c='1234';
        insert cust;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.How_many_units__c  = 'Single family';
        insert ec;
        
        Workorder__c wo = new Workorder__c();
        wo.Customer__c  = cust.Id;
        
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();
        insert wo;
        test.stoptest();
      //  controller.cloneCallScriptLineItem(obj.Id);
        
    }
    
    @isTest
    public static void runTest1(){
         
        test.starttest();
        
        Customer__c cust = new Customer__c();
        cust.Name = 'test';
        cust.First_Name__c='hemanshu';
        cust.Last_Name__c='patel';
        cust.Service_Zipcode__c='1234';
        insert cust;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.How_many_units__c  = '2 family';
        insert ec;
        
        customer_Eligibility__c  CustEl = new Customer_Eligibility__c (Eligibility_Check__c =ec.Id,Electric_Account__c='Electric',Gas_Account__c='Gas');
        insert CustEl;
        
        Workorder__c wo = new Workorder__c();
        wo.Customer__c  = cust.Id;
        wo.customer_Eligibility__c   = CustEl.Id;
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();
        insert wo;
        
        Trade_Ally_Account__c TAcc = new Trade_Ally_Account__c(name='test');
        insert TAcc;
        
        wo.Preferred_Contractor__c = TAcc.Id;
        update wo;
       
        test.stoptest();
      //  controller.cloneCallScriptLineItem(obj.Id);
        
    }
}