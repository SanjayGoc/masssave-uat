public class dsmtServiceRequestsCntrl{
    public String conName {get;set;}
    public String conId   {get;set;}
    
    public String result1;
    public User u{get;set;}
    public String accid;
    public Boolean isTac;
    public String message_Filter {set;get;}
    public List<MessagesModel> listMessage{set;get;}
    public Boolean isMessageInfoOpen{set;get;}
    public String selectedMessageId{set;get;}
    public List<Attachment__c> listAtt{set;get;}
    public Service_Request__c openTickets{set;get;}
    public String cloneEAId{set;get;}
    public String paymentResult1;
    public Service_Request__c paymentMsg{set;get;}
    public boolean displayPopUp{get;set;}
    public Set<Id> eaIds = new Set<Id>();
    string conTanentId = '';
    // -------------- PAGINATION -----------//
    // Json Result For Ajax Responder 
    public JSONObject jsonx {get;set;}
    public Integer TotalRecordsCount {get;set;}
    public String selectedRowIds{get;set;}
    public JSONObject jsonxPayment {get;set;}
    public Integer TotalRecordsCountPayment {get;set;}
    public Boolean showColorBox{get;set;}
    public string messageJSON{get;set;}
    
    public string tdareAllyAccId;
    public string dsmtTrackerConId;
    
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
    
     public Integer UnreadMessage{get;set;}
    public dsmtServiceRequestsCntrl(){
    
        isPortal = true;
        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
        List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
            if(lstUser.size()>0){
                if(lstUser[0].IsPortalEnabled){
                    headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                }
                else{
                    headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                    isPortal = false;
                }
            }
        
        init();
        listOwnerId();
        fetchMessages();
        //initPaginationSorting();
        
    
    }
    
    public void init(){
        selectedRowIds = '';
        tdareAllyAccId = '';
        dsmtTrackerConId = '';
        openTickets = new Service_Request__c();
        paymentMsg = new Service_Request__c();
        displayPopUp = false;
        listAtt = new List<Attachment__c>();
        conName = conId = '';
        message_Filter = 'Inbox';
        selectedMessageId = '';
        //cloneEAId = '';
        isMessageInfoOpen = false;
        //eaIds = new Set<Id>();
        listMessage = new List<MessagesModel>();
        
        isTac   = false;
        //fetchContact();
        if(accid != null && accid.length() >14){
        }
        
    }
    
    public Set<Id> listOwnerId(){
        List<User> userList = [select id,contactId from user where id =: userinfo.getUserId()];
        
        Set<Id> dsmtcId = new Set<Id>();
        
        Set<Id> userId = new Set<Id>();
        List<DSMTracker_Contact__c> dsmtconList = null;
        if(userList != null && userList.size() > 0 && userList.get(0).ContactId != null){
            
            system.debug('--userList.get(0).ContactId---'+userList.get(0).ContactId);
            
                dsmtconList = [select id,Name,Super_User__c,Trade_Ally_Account__c,contact__c from DSMTracker_Contact__c where 
                                                            contact__c =: userList.get(0).ContactId];
            }else{
                dsmtconList = [select id,Name,Super_User__c,Trade_Ally_Account__c,contact__c from DSMTracker_Contact__c where 
                                                            portal_user__c  =: userList.get(0).id];
            }
            if(dsmtConList.size() > 0){
               
                tdareAllyAccId = dsmtConList.get(0).Trade_Ally_Account__c;
                dsmtTrackerConId = dsmtConList.get(0).Id;
                
                system.debug('--dsmtTrackerConId--'+dsmtTrackerConId);
                dsmtconList = [select id,Super_User__c,Name,contact__c from DSMTracker_Contact__c where 
                                                            Trade_Ally_Account__c  =: dsmtConList.get(0).Trade_Ally_Account__c and Super_User__c = false];
                for(DSMTracker_Contact__c dsmtc : dsmtConList){
                    dsmtcId.add(dsmtc.contact__c);
                    //dsmtContactOption.add(new SelectOption(dsmtc.Id, dsmtc.Name));
                }
               // dsmtcId.add(dsmtConList.get(0).contact__c);
            
        }
        
        userId.add(Userinfo.getUserId());
        system.debug('--dsmtcId---'+dsmtcId);
        userList = [select id from user where contactid in : dsmtcId];
        
        
        
        for(User u : userList){
            userId.add(u.Id);
        }
        return userId;
    }
    
    public void backToMessage(){
        isMessageInfoOpen = false;
        //openMsg = new Messages__c();
        listAtt = new List<Attachment__c>();
    }
    
    public void fetchMessages(){
        //Set<String> eaIdSet = portalUtility.getEnrollAppIdsforLandingPages(); 
        String userId = userinfo.getUserId();
        Set<Id> setUserId = new Set<Id>();
        setUserId = listOwnerId();                 
        String queryString = 'Select Id,Name,Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate  from Service_Request__c where Trade_Ally_Account__c = :tdareAllyAccId';
        
       
        UnreadMessage = 0;
        List<Service_Request__c> messageLst = [select id,name from Service_Request__c];
        if(messageLst != null && messageLst.size() > 0){
            UnreadMessage  = messageLst.size();
        }
        
        queryString += ' LIMIT 50';
        system.debug('--queryString---'+queryString);
        listMessage = new List<MessagesModel>();
        for(Service_Request__c msg : Database.query(queryString)){
            MessagesModel mod = new MessagesModel();
            mod.msg = msg;
            mod.msgno = msg.Name;
            
            listMessage.add(mod);
        }
        system.debug('--listMessage---'+listMessage);
    }
    public void initMessages(){
        
        Id srRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
         
        system.debug('--dsmtTrackerConId--'+dsmtTrackerConId);  
        
        messageJSON = '';
        String userId = userinfo.getUserId();
        Set<Id> setUserId = new Set<Id>();
        setUserId = listOwnerId();
        String queryString = 
            'SELECT '+
                'Id,Name,Type__c,Sub_Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate,' +
                'Workorder__c,Workorder__r.Name,Customer__c,Customer_Name__c,Program_Utility_Provider__c,Primary_Provider__c,Rebate_Status__c,Boiler_Type__c,Service_Request_Panel_Title__c  '+
            'FROM Service_Request__c ' + 
            'WHERE RecordTypeId != :srRecordTypeId AND Dsmtracker_Contact__c = :dsmtTrackerConId ';
        
            queryString += ' order by name desc LIMIT 50000';
            
            //list<Service_Request__c> listMsg = Database.query(queryString);

            Id srRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Phase I Ticket Detail').getRecordTypeId();
            Id ptdRecordId = Schema.SObjectType.Phase_I_Ticket_Detail__c.getRecordTypeInfosByName().get('Early Boiler & Furnace Request').getRecordTypeId();
            Set<String> srSubTypeSet = new Set<String>{
                'EBR/EFR Needs Office Review',
                'EBR/EFR Rejected in Office',
                'EBR/EFR Rejected On-Site',
                'EBR Verified On-Site',
                'EFR Verified On-Site'
            };            
            List<Service_Request__c> srs = [
                SELECT 
                    Id,Name,Type__c,Sub_Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate,
                    Workorder__c,Workorder__r.Name,Customer__c,Customer_Name__c,Program_Utility_Provider__c,Primary_Provider__c,Rebate_Status__c,Boiler_Type__c,
                    Service_Request_Panel_Title__c,
                    (
                        SELECT 
                            Id,Name,Service_Request__c,Rebate_Status__c,Number_of_Heating_Systems__c,Manufacturer__c,Model__c,Serial_Number__c,MFG_Year__c,
                            Boiler_Type__c,Furnace_Type__c,Fuel_Type__c,Original_Audit_Date__c,Original_Audit_Type__c,Owner_Occupied__c,
                            Program_Utility_Provider__c,Provider__c,Provider__r.Name
                        FROM Phase_I_Ticket_Details__r
                        WHERE RecordTypeId =:ptdRecordId 
                    ) 
                FROM Service_Request__c 
                WHERE Status__c != 'Closed' AND Sub_Type__c NOT IN:srSubTypeSet AND RecordTypeId !=:srRecordTypeId AND Dsmtracker_Contact__c =:dsmtTrackerConId ORDER BY Name DESC LIMIT 50000
            ];
        
            System.debug('--srs.size()-- ' + srs.size());
            list<MessageModal> listMessage = new List<MessageModal>();
            for(Service_Request__c sr : srs){
                MessageModal mod = new MessageModal();
                mod.Id = sr.Id;
                mod.Name = sr.Name;
                mod.Type= sr.Type__c;
                mod.subType = sr.Sub_Type__c;
                mod.Subject = sr.Subject__c;
                mod.Status = sr.Status__c;
                mod.Priority = sr.Priority__c;
                mod.Description = sr.Description__c ;
                mod.CreatedDate = sr.CreatedDate.format();
                mod.WorkOrderId=sr.Workorder__c;
                mod.WorkOrderName=sr.Workorder__r.Name;
                mod.CustomerId=sr.Customer__c;
                mod.CustomerName=sr.Customer_Name__c;
                mod.PanelTitle=sr.Service_Request_Panel_Title__c;
                
                System.debug(sr.Name + ' --sr.Phase_I_Ticket_Details__r.size()-- ' + sr.Phase_I_Ticket_Details__r.size());
                if(sr.Phase_I_Ticket_Details__r != null && sr.Phase_I_Ticket_Details__r.size() > 0){
                    Phase_I_Ticket_Detail__c ptd = sr.Phase_I_Ticket_Details__r.get(0);
                    mod.ProgramUtilityProvider=ptd.Program_Utility_Provider__c;
                    if(mod.ProgramUtilityProvider != null){
                        mod.ProgramUtilityProvider = mod.ProgramUtilityProvider.replace('CSG','CLR');
                    }
                    mod.Provider=ptd.Provider__r.Name;
                    mod.RebateStatus=ptd.Rebate_Status__c;
                    mod.BoilerType=ptd.Boiler_Type__c;
                }
                
                listMessage.add(mod);
            }
            
            /*list<MessageModal> listMessage = new List<MessageModal>();
            for(Service_Request__c msg : Database.query(queryString)){
                 
                
                MessageModal mod = new MessageModal();
                mod.Id = msg.Id;
                mod.Name = msg.Name;
                mod.Type= msg.Type__c;
                mod.subType = msg.Sub_Type__c;
                mod.Subject = msg.Subject__c;
                mod.Status = msg.Status__c;
                mod.Priority = msg.Priority__c;
                mod.Description = msg.Description__c ;
                mod.CreatedDate = msg.CreatedDate.format();
                
                mod.WorkOrderId=msg.Workorder__c;
                mod.WorkOrderName=msg.Workorder__r.Name;
                mod.CustomerId=msg.Customer__c;
                mod.CustomerName=msg.Customer_Name__c;
                mod.ProgramUtilityProvider=msg.Program_Utility_Provider__c;
                mod.Provider=msg.Primary_Provider__c;
                mod.RebateStatus=msg.Rebate_Status__c;
                mod.BoilerType=msg.Boiler_Type__c;
                    
                listMessage.add(mod);
            }*/
            system.debug('--listMessage---'+listMessage);
            messageJSON = JSON.serialize(listMessage);
        
        system.debug('--listMessage---'+listMessage);
    }
    
    public class MessageModal{
        public string Id{get;set;}
        public string Name{get;set;}
        public string Subject{get;set;}
        public string Type{get;set;}
        public String subType{get;set;}
        public string Status{get;set;}
        public string Priority{get;set;}
        public string Description{get;set;}
        public String CreatedDate{get;set;}
        public String WorkOrderId{get;set;}
        public String WorkOrderName{get;set;}
        public String CustomerId{get;set;}
        public String CustomerName{get;set;}
        public String ProgramUtilityProvider{get;set;}
        public String Provider{get;set;}
        public String RebateStatus{get;set;}
        public String BoilerType{get;set;}
        public String PortalRequestType{get{return (this.Type == null ? '' :this.type + ', ') + (this.subType == null ? '' :this.subType);}}
        public String PanelTitle{get;set;}
      //public String PortalRequestType{get{return this.Type + ', ' + this.subType;}}
    }
    
    
     
  
    public class MessagesModel{
        public Service_Request__c msg{set;get;}
        public Boolean isOpenedBlank{set;get;}
        public Boolean IsDraft{get;set;}
        public String msgno{get;set;}
        public MessagesModel(){
            msg = new Service_Request__c();
            isOpenedBlank = false;
            
        }
    }
    
    
    
    @RemoteAction
    public static Service_Request__c queryMessage(string mid) {
        return [Select Id,Name,Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate  from Service_Request__c where Id = :mid];
    }
    
    @RemoteAction
    public static  String saveRespond(String subject,String type,String status,String priority,String body,string taID,string dsmtConId) {
        
        
        Service_Request__c dm = new Service_Request__c();
        
        dm.Subject__c = subject;
        dm.Type__c = type;
        dm.Status__c = status;
        dm.Priority__c = priority;
        dm.Description__c = body;
        if(taId!=null&&taId!=''){
            dm.Trade_Ally_Account__c = taId;
        }
        if(dsmtConId!=null && dsmtConId!='')
            dm.DSMTracker_Contact__c = dsmtConId;
        
        upsert dm;
        
        return 'success';
    }
    
    public List<SelectOption> getTicketType()    
    {    
        List<SelectOption> options =  new List<SelectOption>();    
        options.add(new selectOption('','--- None ---'));    
        Schema.DescribeFieldResult fieldResult = Service_Request__c.Type__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }
    
    public List<SelectOption> getTicketStatus()    
    {    
        List<SelectOption> options =  new List<SelectOption>();    
        options.add(new selectOption('','--- None ---'));    
        Schema.DescribeFieldResult fieldResult = Service_Request__c.Status__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }
    
    public List<SelectOption> getTicketPriority()    
    {    
        List<SelectOption> options =  new List<SelectOption>();    
        options.add(new selectOption('','--- None ---'));    
        Schema.DescribeFieldResult fieldResult = Service_Request__c.Priority__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }  

}