@isTest
Public class dsmtInsertproviderDetailsTest{
    @isTest
    public static void runTest(){
        
        Eligibility_Check__c el = Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        
        Workorder_Type__c woType = new Workorder_Type__c(Name='Wifi');
        woType.Qualifying_Audit__c  = true;
        insert woType;
        
        Workorder__c wo = new Workorder__c(Status__c='Unscheduled');
        wo.Requested_Start_Date__c  = Datetime.now();
        wo.Requested_End_Date__c  = Datetime.now().Addhours(2);
        wo.Scheduled_Start_Date__c = Datetime.now();
        wo.Scheduled_End_Date__c= Datetime.now().Addhours(2);
        wo.workorder_Type__c = woType.Id;
        wo.Eligibility_Check__c = el.id;
        
        insert wo;
    }
}