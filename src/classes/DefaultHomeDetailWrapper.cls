public class DefaultHomeDetailWrapper{
    public BuildingProfile BuildingProfile;
    public WaterFixtures[] WaterFixtures;
    public Lighting Lighting;
    public Appliance[] Appliance;
    public AirFlow AirFlow;
    public ThermalEnvelope ThermalEnvelope;
    public String Error;    //
    public boolean Success;
    public class BuildingProfile {
        public String Name;
        public Decimal YearBuilt;   //0
        public Decimal NumBedrooms; //3
        public Decimal NumOccupants;    //3
        public String Orientation;  //South
        public String PrimaryHeatingFuel;   //Natural Gas
        public Decimal CeilingHeight;
        public Decimal NumFloors;   //1
        public Decimal FloorArea;
        public Decimal TotalFloorArea;
        public Decimal TotalVolume;
        public boolean IsManufacturedHome;
        public BuildingNotes BuildingNotes;
        public boolean IsAttached;
        public Decimal NumUnitAttached; //0
        public OtherDewllingOn OtherDewllingOn;
        public String BuildingSize; //Medium
        public String BuildingShape;    //Rectangular
        public Decimal FinishedBasementArea;
        public Decimal ShellAreaGross;
        public Decimal FootprintArea;
    }
    class BuildingNotes {
    }
    class OtherDewllingOn {
    }
    public class WaterFixtures {
        public String Name;
        public String WaterFixtureType; //Sink
        public Decimal TotalWaterGallonsPerDay;
        public Decimal HotWaterGallonsPerDay;
        public Decimal ColdWaterGallonsPerDay;
        public Decimal TotalWaterGallonsPerDayH;
        public Decimal HotWaterGallonsPerDayH;
        public Decimal ColdWaterGallonsPerDayH;
        public Decimal TotalWaterGallonsPerDayC;
        public Decimal HotWaterGallonsPerDayC;
        public Decimal ColdWaterGallonsPerDayC;
        public Decimal TotalWaterGallonsPerDaySH;
        public Decimal HotWaterGallonsPerDaySH;
        public Decimal ColdWaterGallonsPerDaySH;
        public Decimal HotWaterGallonsPerDayMixAdj;
        public Decimal HotWaterGallonsPerDayMixAdjH;
        public Decimal HotWaterGallonsPerDayMixAdjC;
        public Decimal HotWaterGallonsPerDayMixAdjSH;
        public Decimal HotWaterMixTemp;
        public Decimal Quantity;    //3
        public String Location; //Living Space
        public String AdditionalNotes;
        public String DHW;  //
        public Decimal ShowersPerDay;
        public Decimal ShowerExGPM;
        public ShowerInternalExGPM ShowerInternalExGPM;
        public Decimal ShowerNewGPM;
        public ShowerInternalNewGPM ShowerInternalNewGPM;
        public Decimal LowFlowGPM;
        public Decimal LowFlowQuantity; //0
        public SavedHotWaterGallonsPerDay SavedHotWaterGallonsPerDay;
        public SpaInsulation SpaInsulation;
        public SpaUsage SpaUsage;
        public OperatingMonths OperatingMonths;
        public Decimal NumberOperatingMonths;   //0
        public CoveredAmount CoveredAmount;
        public PoolHeatMonths PoolHeatMonths;
        public Decimal NumberPoolHeatMonths;    //0
        public TimerHours TimerHours;
        public boolean HasOffSeasonUse;
        public WinterHours WinterHours;
        public boolean HasSmartTimer;
        public Decimal PumpHP;
        public PoolLength PoolLength;
        public PoolWidth PoolWidth;
        public PoolPumpType PoolPumpType;
        public String PoolHeaterType;
        public String PoolSize;
        public Decimal PoolHeatTemp;
        public Decimal PoolArea;
        public PoolVolume PoolVolume;
        public PoolPumpWatts PoolPumpWatts;
        public PoolHeatUp PoolHeatUp;
        public PoolHeatLoad PoolHeatLoad;
        public SpaUseWatts SpaUseWatts;
        public SpaIdleWatts SpaIdleWatts;
        public SpaUseHours SpaUseHours;
        public String HeaterType;
    }
    class HotWaterMixTemp {
    }
    class ShowerInternalExGPM {
    }
    class ShowerInternalNewGPM {
    }
    class SavedHotWaterGallonsPerDay {
    }
    class SpaInsulation {
    }
    class SpaUsage {
    }
    class OperatingMonths {
    }
    class CoveredAmount {
    }
    class PoolHeatMonths {
    }
    class TimerHours {
    }
    class WinterHours {
    }
    class PoolLength {
    }
    class PoolWidth {
    }
    class PoolPumpType {
    }
    class PoolVolume {
    }
    class PoolPumpWatts {
    }
    class PoolHeatUp {
    }
    class PoolHeatLoad {
    }
    class SpaUseWatts {
    }
    class SpaIdleWatts {
    }
    class SpaUseHours {
    }
    public class Lighting {
        public String Name;
        public String General_IndoorLighting;   //Medium
        public Double KWH_Year_Indoor;  //1532.6
        public String EfficientLights;  //Some
        public String General_OutdoorLighting;  //Medium
        public Double KWH_Year_Outdoor; //117.3
        public boolean Has_Outdoor_Fixtures;
        public String AdditionalNotes;
        public LightBulbSet LightBulbSet;
    }
    class LightBulbSet {
    }
    public class Appliance {
        public String Name;
        public String Category;
        public String Location; //Kitchen
        public Decimal ActualAnnualFuelConsumption;
        public String ApplianceFuelTypes;
        public Decimal ApplianceNumberOperatingMonths;  //0
        public ApplianceOperatingMonths ApplianceOperatingMonths;
        public ApplianceQuantity ApplianceQuantity;
        public String ApplianceType;    //Dishwasher
        public String ApplianceUsage;
        public String CalibratableBPOType;
        public Decimal ColdWaterGallonsPerDay;
        public Decimal ColdWaterGallonsPerDayC;
        public Decimal ColdWaterGallonsPerDayH;
        public Decimal ColdWaterGallonsPerDaySh;
        public String DehumidifierType;
        public String DHW;  //
        public Decimal DishwasherEnergyFactor;  //0
        public Decimal DishwasherLoadsPerWeek;  //0
        public String DishwasherType;   //Standard
        public boolean DryerHasIndoorVent;
        public Decimal DryerIndoorHangLoadsPerWeek;
        public Decimal DryerLoadsPerWeek;
        public DryerNominalKWH DryerNominalKWH;
        public DryerUseFactor DryerUseFactor;
        public boolean HasSmartPowerStrip;
        public boolean HasTimer;
        public HolidayLightsFractionLED HolidayLightsFractionLED;
        public HolidayLightsUsage HolidayLightsUsage;
        public Decimal HomeOfficeDesktopPCQuantity;
        public Decimal HomeOfficeNotebookPCQuantity;
        public Decimal HomeOfficeOtherQuantity;
        public Decimal HomeOfficeRouterQuantity;
        public Decimal HotWaterGallonsPerDay;
        public Decimal HotWaterGallonsPerDayC;
        public Decimal HotWaterGallonsPerDayH;
        public Decimal HotWaterGallonsPerDaySh;
        public String Pilot;
        public Decimal RefrigeratorAdjustedVolume;
        public String RefrigeratorDefrostType;
        public boolean RefrigeratorHasAntiSweatSwitch;
        public boolean RefrigeratorHasDoorSealGaps;
        public boolean RefrigeratorHasThroughDoorIce;
        public boolean RefrigeratorIsPrimary;
        public RefrigeratorkWhComputedFromMetering RefrigeratorkWhComputedFromMetering;
        public RefrigeratorkWhModeled RefrigeratorkWhModeled;
        public RefrigeratorMeteredKWH RefrigeratorMeteredKWH;
        public RefrigeratorMeteredMinutes RefrigeratorMeteredMinutes;
        public RefrigeratorMeteredPeakKW RefrigeratorMeteredPeakKW;
        public RefrigeratorMeteredRoomTemp RefrigeratorMeteredRoomTemp;
        public String RefrigeratorMeteredStartTime; //0001-01-01T00:00:00
        public String RefrigeratorMeteredStopTime;  //0001-01-01T00:00:00
        public boolean RefrigeratorPurchasedUsed;
        public Decimal RefrigeratorRatedKWH;
        public RefrigeratorRoomTemp RefrigeratorRoomTemp;
        public RefrigeratorRoomTempH RefrigeratorRoomTempH;
        public RefrigeratorRoomTempSh RefrigeratorRoomTempSh;
        public String RefrigeratorStyle;
        public String RefrigeratorType;
        public RefrigeratorVintageRatio RefrigeratorVintageRatio;
        public Decimal RefrigeratorVolume;
        public Decimal TotalWaterGallonsPerDay;
        public Decimal TotalWaterGallonsPerDayC;
        public Decimal TotalWaterGallonsPerDayH;
        public Decimal TotalWaterGallonsPerDaySh;
        public Decimal TVDiagonal;
        public Decimal TVDVRQuantity;   //0
        public Decimal TVOtherQuantity; //0
        public Decimal TVQuantity;
        public Decimal TVSetTopBoxQuantity; //0
        public WasherAnnualLoads WasherAnnualLoads;
        public Decimal WasherCapacity;
        public WasherColdLoadFraction WasherColdLoadFraction;
        public Decimal WasherColdLoadsPerWeek;
        public WasherColdWaterGallonsPerLoad WasherColdWaterGallonsPerLoad;
        public WasherHotLoadFraction WasherHotLoadFraction;
        public Decimal WasherHotLoadsPerWeek;
        public WasherHotWaterGallonsPerLoad WasherHotWaterGallonsPerLoad;
        public WasherLbsCapacity WasherLbsCapacity;
        public WasherLoadsPerWeek WasherLoadsPerWeek;
        public WasherMixAdjustC WasherMixAdjustC;
        public WasherMixAdjustH WasherMixAdjustH;
        public WasherMixAdjustSh WasherMixAdjustSh;
        public WasherMixTemp WasherMixTemp;
        public Decimal WasherModifiedEnergyFactor;
        public double WasherRemainingMoistureContent; // changed by HP
        public WasherTotalWaterGallonsPerLoad WasherTotalWaterGallonsPerLoad;
        public WasherTotalWaterGallonsPerRinse WasherTotalWaterGallonsPerRinse;
        public WasherTotalWaterGallonsPerWash WasherTotalWaterGallonsPerWash;
        public String WasherType;
        public WasherWarmLoadFraction WasherWarmLoadFraction;
        public Decimal WasherWarmLoadsPerWeek;
        public WasherWarmRinseHotFraction WasherWarmRinseHotFraction;
        public WasherWarmWashHotFraction WasherWarmWashHotFraction;
        public WasherWashRinse WasherWashRinse;
        public decimal WasherWaterFactor; // change by HP
        public boolean WellPumpHasIrrigationSystem;
        public Decimal Year;    //0
        public String Make; //
        public String Model;    //
        public String SerialNumber; //
    }
    class ApplianceOperatingMonths {
    }
    class ApplianceQuantity {
    }
    class CalibratableBPOType {
    }
    class DehumidifierType {
    }
    class DryerNominalKWH {
    }
    class DryerUseFactor {
    }
    class HolidayLightsFractionLED {
    }
    class HolidayLightsUsage {
    }
    class RefrigeratorkWhComputedFromMetering {
    }
    class RefrigeratorkWhModeled {
    }
    class RefrigeratorMeteredKWH {
    }
    class RefrigeratorMeteredMinutes {
    }
    class RefrigeratorMeteredPeakKW {
    }
    class RefrigeratorMeteredRoomTemp {
    }
    class RefrigeratorRoomTemp {
    }
    class RefrigeratorRoomTempH {
    }
    class RefrigeratorRoomTempSh {
    }
    class RefrigeratorVintageRatio {
    }
    class WasherAnnualLoads {
    }
    class WasherColdLoadFraction {
    }
    class WasherColdWaterGallonsPerLoad {
    }
    class WasherHotLoadFraction {
    }
    class WasherHotWaterGallonsPerLoad {
    }
    class WasherLbsCapacity {
    }
    class WasherLoadsPerWeek {
    }
    class WasherMixAdjustC {
    }
    class WasherMixAdjustH {
    }
    class WasherMixAdjustSh {
    }
    class WasherMixTemp {
    }
    class WasherTotalWaterGallonsPerLoad {
    }
    class WasherTotalWaterGallonsPerRinse {
    }
    class WasherTotalWaterGallonsPerWash {
    }
    class WasherWarmLoadFraction {
    }
    class WasherWarmRinseHotFraction {
    }
    class WasherWarmWashHotFraction {
    }
    class WasherWashRinse {
    }
    public class AirFlow {
        public String Name;
        public String Leakiness;    //Average
        public Double CFM50;    //2991.4
        public BuildingTightnessLimit BuildingTightnessLimit;
        public MechanicalValidationRequiredCFM50 MechanicalValidationRequiredCFM50;
        public BlowerDoorReadings BlowerDoorReadings;
        public PressureDifferentialReadings PressureDifferentialReadings;
        public AirflowVolMin AirflowVolMin;
        public AirflowOccMin AirflowOccMin;
        public Nfactor136 Nfactor136;
    }
    class BuildingTightnessLimit {
    }
    class MechanicalValidationRequiredCFM50 {
    }
    class BlowerDoorReadings {
    }
    class PressureDifferentialReadings {
    }
    class AirflowVolMin {
    }
    class AirflowOccMin {
    }
    class Nfactor136 {
    }
    public class ThermalEnvelope {
        public String Name;
        public Attic[] Attic;
        public VaultFlat VaultFlat;
        public Wall[] Wall;
        public BandJoist BandJoist;
        public FloorCeiling Floor;
        public Slab Slab;
        public Foundation[] Foundation;
        public WindowsAndSkylights[] WindowsAndSkylights;
    }
    public class Attic {
        public String Name;
        public Decimal AreaJoist;
        public Decimal AreaRafter;
        public AtticAccesses AtticAccesses;
        public AtticConditioning AtticConditioning;
        public AtticSpaceWorkOrderData AtticSpaceWorkOrderData;
        public AtticVenting AtticVenting;
        public boolean HasTrusses;
        public boolean IsKneeslope;
        public RValJoistNet RValJoistNet;
        public RValRaftGableNet RValRaftGableNet;
        public SolarAddDesignC SolarAddDesignC;
        public TDesignC TDesignC;
        public TempCDuct TempCDuct;
        public TSolairC TSolairC;
        public TSolairDesignC TSolairDesignC;
        public TSolairH TSolairH;
        public VentCFMH VentCFMH;
        public VentCFMNat VentCFMNat;
        public Notes Notes;
        public String SpaceConditioning;
        public SpaceConditioning_Internal SpaceConditioning_Internal;
        public Decimal CeilingHeight;
        public FloorCeiling[] Ceilings;
        public FloorCeiling[] Floors;
        public boolean IsDisconnected;
        public SpaceType SpaceType;
        public TempC TempC;
        public TempH TempH;
        public Wall[] Walls;
        public boolean WallSpacesIncludesOther;
    }
    class AtticAccesses {
    }
    class AtticConditioning {
    }
    public class AtticSpaceWorkOrderData {
        public String Name;
        public String AtticAreaType;    //
        public String AtticHeightType;  //
        public Decimal NumChimneys; //0
        public Decimal NumRecessedLights;   //0
        public Decimal StorageArea; //0
    }
    public class AtticVenting {
        public String Name; //Attic Venting
        public String VentingAmount;    //Unknown
        public VentingSet VentingSet;
    }
    class VentingSet {
    }
    class RValJoistNet {
    }
    class RValRaftGableNet {
    }
    class SolarAddDesignC {
    }
    class TDesignC {
    }
    class TempCDuct {
    }
    class TSolairC {
    }
    class TSolairDesignC {
    }
    class TSolairH {
    }
    class VentCFMH {
    }
    class VentCFMNat {
    }
    class Notes {
    }
    class SpaceConditioning {
    }
    class SpaceConditioning_Internal {
    }
   /* public class Ceilings {
        public String CeilingType;  //Basement Ceiling
        public Decimal EffectiveRValue; //0
        public FloorCeilingType FloorCeilingType;
        public FloorType FloorType;
        public boolean HasRadiantBarrier;
        public boolean HasTrusses;
        public Decimal InsulatedArea;
        public Length Length;
        public Decimal NominalRValue;   //0
        public Decimal Perimeter;   //173
        public String ProjectedArea;
        public String Reflectivity;
        public ReflectivityFactor ReflectivityFactor;
        public ReradiationFactor ReradiationFactor;
        public RoofVentCFMH RoofVentCFMH;
        public RoofVentCFMNat RoofVentCFMNat;
        public Decimal RValue;  //5
        public Decimal SkylightArea;
        public Decimal SlabEdgeRValue;
        public boolean SlabHasThermalBreak;
        public SlabInsulationDepth SlabInsulationDepth;
        public SlabInsulationDepthFactor SlabInsulationDepthFactor;
        public SlabInsulationToTop SlabInsulationToTop;
        public SlabIsCarpeted SlabIsCarpeted;
        public SlabRValNetLin SlabRValNetLin;
        public SlabUALin SlabUALin;
        public SlabUALinEffC SlabUALinEffC;
        public SlabUALinEffH SlabUALinEffH;
        public String Slope;
        public SolarGainTempAddC SolarGainTempAddC;
        public SolarGainTempAddH SolarGainTempAddH;
        public TSolairC TSolairC;
        public TSolairDesignC TSolairDesignC;
        public String UA;
        public UAEffC UAEffC;
        public UAEffH UAEffH;
        public Double UValue;   //0.207
        public VentingAmount VentingAmount;
        public Width Width;
        public String Name;
        public SpaceType SpaceType;
        public Decimal Area;    //1800
        public Decimal AreaNet; //1800
        public BareWallRVal BareWallRVal;
        public Decimal BasementWallExposedArea;
        public BasementWallExposedHeight BasementWallExposedHeight;
        public CLF CLF;
        public DepthRatio DepthRatio;
        public HLF HLF;
        public String InsulationAmount; //None
        public InsulationGaps InsulationGaps;
        public InsulationMisaligned InsulationMisaligned;
        public boolean IsDirtFloor;
        public boolean IsKneeslope;
        public NetRAdded NetRAdded;
        public PerimeterFactor PerimeterFactor;
        public RigidInsulationCutAtGrade RigidInsulationCutAtGrade;
        public RigidInsulationExtent RigidInsulationExtent;
        public RigidInsulationExtentUsed RigidInsulationExtentUsed;
        public Decimal RValueConst;
        public SurfaceType SurfaceType;
        public Layers[] Layers;
    } */
    class CeilingType {
    }
    class FloorCeilingType {
    }
    class FloorType {
    }
    class Length {
    }
    class Perimeter {
    }
    class ReflectivityFactor {
    }
    class ReradiationFactor {
    }
    class RoofVentCFMH {
    }
    class RoofVentCFMNat {
    }
    class SlabInsulationDepth {
    }
    class SlabInsulationDepthFactor {
    }
    class SlabInsulationToTop {
    }
    class SlabIsCarpeted {
    }
    class SlabRValNetLin {
    }
    class SlabUALin {
    }
    class SlabUALinEffC {
    }
    class SlabUALinEffH {
    }
    class SolarGainTempAddC {
    }
    class SolarGainTempAddH {
    }
    class UAEffC {
    }
    class UAEffH {
    }
    class VentingAmount {
    }
    class Width {
    }
    class SpaceType {
    }
    class BareWallRVal {
    }
    class BasementWallExposedHeight {
    }
    class CLF {
    }
    class DepthRatio {
    }
    class HLF {
    }
    class InsulationGaps {
    }
    class NetRAdded {
    }
    class PerimeterFactor {
    }
    class RigidInsulationExtent {
    }
    class RigidInsulationExtentUsed {
    }
    class SurfaceType {
    }
    public class Layers {
        public String Name;
        public Notes Notes;
        public String LayerType;    //Masonry
        public String MaterialType; //Concrete
        public Decimal Thickness;   //10
        public String CavityInsulationAmount;
        public boolean CavityInsulationCoversFrame;
        public Decimal CavityInsulationDepth;
        public CavityInsulationGaps CavityInsulationGaps;
        public String CavityInsulationType;
        public boolean CavityInsulationMisaligned;
        public Decimal CavityRValue;
        public Decimal CavityRValuePerInch;
        public Decimal ExtraCoveringCavityRValue;
        public Decimal ExtraNotCoveringCavityRValue;
        public Decimal FrameDepth;
        public boolean FrameDimensionsAreS4S;
        public FrameFactor FrameFactor;
        public Decimal FrameSpacing;
        public Decimal FrameWidth;
        public String FramingType;
        public Decimal NominalFrameDepth;
        public Decimal NominalFrameWidth;
        public Decimal EffectiveRValue;
        public Decimal NominalRValue;
        public Decimal RValue;
        public Decimal RValuePerInch;
        public boolean CavityEnclosed;
        public String CavityOverflowInsulationType;
        public boolean OverFlowCoverFrame;
        public Decimal Cavity_Insulation_R_value_PerInch;
    }
    class Thickness {
    }
    class CavityInsulationGaps {
    }
    class FrameFactor {
    }
    public class FloorCeiling{
        public String Name;
       /* public CeilingType CeilingType;
        public Decimal EffectiveRValue;
        public FloorCeilingType FloorCeilingType;
        public String FloorType;    //Basement Floor
        public boolean HasRadiantBarrier;
        public boolean HasTrusses;
        public Decimal InsulatedArea;
        public Length Length;
        public Decimal NominalRValue;
        public Decimal Perimeter;   //173
        public String ProjectedArea;
        public String Reflectivity;
        public ReflectivityFactor ReflectivityFactor;
        public ReradiationFactor ReradiationFactor;
        public RoofVentCFMH RoofVentCFMH;
        public RoofVentCFMNat RoofVentCFMNat;
        public Decimal RValue;
        public Decimal SkylightArea;
        public Decimal SlabEdgeRValue;
        public boolean SlabHasThermalBreak;
        public SlabInsulationDepth SlabInsulationDepth;
        public SlabInsulationDepthFactor SlabInsulationDepthFactor;
        public SlabInsulationToTop SlabInsulationToTop;
        public SlabIsCarpeted SlabIsCarpeted;
        public SlabRValNetLin SlabRValNetLin;
        public SlabUALin SlabUALin;
        public SlabUALinEffC SlabUALinEffC;
        public SlabUALinEffH SlabUALinEffH;
        public String Slope;
        public SolarGainTempAddC SolarGainTempAddC;
        public SolarGainTempAddH SolarGainTempAddH;
        public TSolairC TSolairC;
        public TSolairDesignC TSolairDesignC;
        public String UA;
        public UAEffC UAEffC;
        public UAEffH UAEffH;
        public Decimal UValue;
        public VentingAmount VentingAmount;
        public Width Width;
        public String Name;
        public SpaceType SpaceType;
        public Decimal Area;    //1800
        public Decimal AreaNet; //1800
        public Decimal BareWallRVal;
        public Decimal BasementWallExposedArea;
        public Decimal BasementWallExposedHeight;
        public CLF CLF;
        public DepthRatio DepthRatio;
        public HLF HLF;
        public String InsulationAmount;
        public InsulationGaps InsulationGaps;
        public InsulationMisaligned InsulationMisaligned;
        public boolean IsDirtFloor;
        public boolean IsKneeslope;
        public NetRAdded NetRAdded;
        public PerimeterFactor PerimeterFactor;
        public RigidInsulationCutAtGrade RigidInsulationCutAtGrade;
        public RigidInsulationExtent RigidInsulationExtent;
        public RigidInsulationExtentUsed RigidInsulationExtentUsed;
        public Decimal RValueConst;
        public SurfaceType SurfaceType;
        public Layers[] Layers;*/
        public string CeilingType ;
        public decimal EffectiveRValue ;
        public string FloorCeilingType ;
        public string FloorType ;
        public boolean HasRadiantBarrier ;
        public boolean HasTrusses ;
        public  decimal InsulatedArea ;
        public  decimal Length ;
        public  decimal NominalRValue ;
        public  decimal Perimeter ;
        public  decimal ProjectedArea ;
        public  string Reflectivity ;
        public  decimal ReflectivityFactor ;
        public  decimal ReradiationFactor ;
        public  decimal RoofVentCFMH ;
        public  decimal RoofVentCFMNat ;
        public  decimal RValue ;
        public  decimal SkylightArea ;
        public  decimal SlabEdgeRValue ;
        public  boolean SlabHasThermalBreak ;
        public  decimal SlabInsulationDepth ;
        public  decimal SlabInsulationDepthFactor ;
        public  boolean SlabInsulationToTop ;
        public  boolean SlabIsCarpeted ;
        public  decimal SlabRValNetLin ;
        public  decimal SlabUALin ;
        public  decimal SlabUALinEffC ;
        public  decimal SlabUALinEffH ;
        public  decimal Slope ;
        public  decimal SolarGainTempAddC ;
        public  decimal SolarGainTempAddH ;
        public  decimal TSolairC ;
        public  decimal TSolairDesignC ;
        public  decimal UA ;
        public  decimal UAEffC ;
        public  decimal UAEffH ;
        public  decimal UValue ;
        public string VentingAmount ;
        public  decimal Width ;
       public string SpaceType ;
       public decimal Area ;
       public decimal AreaNet ;
       public decimal BareWallRVal ;
       public decimal BasementWallExposedArea ;
       public decimal BasementWallExposedHeight ;
       public decimal CLF ;
       public decimal DepthRatio ;
       public decimal HLF ;
       public string InsulationAmount ;
       public string InsulationGaps ;
       public boolean InsulationMisaligned ;
       public boolean IsDirtFloor ;
       public boolean IsKneeslope ;
       public decimal NetRAdded ;
       public decimal PerimeterFactor ;
       public boolean RigidInsulationCutAtGrade ;
       public decimal RigidInsulationExtent ;
       public decimal RigidInsulationExtentUsed ;
       public decimal RValueConst ;
       public string SurfaceType ;
       public Layers[] Layers ;
       public String ExposedTo;
    }
    class TempC {
    }
    class TempH {
    }
    public class Walls {
        public String Name;
        public WallType WallType;
        public Decimal GrossWallArea;
        public Decimal NetWallArea;
        public Decimal UValue;
        public String ExposedTo;
        public String UA;
        public UAEffH UAEffH;
        public Decimal RValue;  //0
        public Decimal NominalRValue;
        public Width Width;
        public Decimal WindowArea;
        public Decimal WindowAreaBack;
        public Decimal WindowAreaFront;
        public Decimal WindowAreaLeft;
        public Decimal WindowAreaRight;
        public Decimal AreaBack;
        public Decimal AreaFront;
        public Decimal AreaLeft;
        public Decimal AreaRight;
        public boolean IsRim;
        public BasementWallLinEffC BasementWallLinEffC;
        public BasementWallLinEffH BasementWallLinEffH;
        public Decimal DoorArea;
        public Decimal EffectiveRValue; //0
        public Decimal ExposedRValue;
        public FractionBack FractionBack;
        public FractionFront FractionFront;
        public FractionLeft FractionLeft;
        public FractionRight FractionRight;
        public Decimal Height;  //8
        public boolean IsAttachedOnBack;
        public boolean IsAttachedOnFront;
        public boolean IsAttachedOnLeft;
        public boolean IsAttachedOnRight;
        public boolean IsWallInteresting;
        public SpaceType SpaceType;
        public Decimal Area;
        public Decimal AreaNet;
        public BareWallRVal BareWallRVal;
        public Decimal BasementWallExposedArea;
        public Decimal BasementWallExposedHeight;   //1
        public CLF CLF;
        public DepthRatio DepthRatio;
        public HLF HLF;
        public String InsulationAmount;
        public InsulationGaps InsulationGaps;
        public boolean InsulationMisaligned;
        public boolean IsDirtFloor;
        public boolean IsKneeslope;
        public NetRAdded NetRAdded;
        public PerimeterFactor PerimeterFactor;
        public boolean RigidInsulationCutAtGrade;
        public RigidInsulationExtent RigidInsulationExtent;
        public RigidInsulationExtentUsed RigidInsulationExtentUsed;
        public Decimal RValueConst;
        public SurfaceType SurfaceType;
        public Layers[] Layers;
    }
    class WallType {
    }
    class BasementWallLinEffC {
    }
    class BasementWallLinEffH {
    }
    class FractionBack {
    }
    class FractionFront {
    }
    class FractionLeft {
    }
    class FractionRight {
    }
    class Height {
    }
    class MaterialType {
    }
    class VaultFlat {
    }
    public class Wall {
        public String Name;
        public WallType WallType;
        public Decimal GrossWallArea;
        public Decimal NetWallArea;
        public Double UValue;   //0.088
        public Decimal UA;
        public String ExposedTo;
        public UAEffH UAEffH;
        public Decimal RValue;  //11
        public Decimal NominalRValue;   //11
        public Width Width;
        public Decimal WindowArea;
        public Decimal WindowAreaBack;
        public Decimal WindowAreaFront;
        public Decimal WindowAreaLeft;
        public Decimal WindowAreaRight;
        public Decimal AreaBack;
        public Decimal AreaFront;
        public Decimal AreaLeft;
        public Decimal AreaRight;
        public boolean IsRim;
        public BasementWallLinEffC BasementWallLinEffC;
        public BasementWallLinEffH BasementWallLinEffH;
        public Decimal DoorArea;
        public Decimal EffectiveRValue; //8
        public Decimal ExposedRValue;
        public FractionBack FractionBack;
        public FractionFront FractionFront;
        public FractionLeft FractionLeft;
        public FractionRight FractionRight;
        public Decimal Height;  //8
        public boolean IsAttachedOnBack;
        public boolean IsAttachedOnFront;
        public boolean IsAttachedOnLeft;
        public boolean IsAttachedOnRight;
        public boolean IsWallInteresting;
        public SpaceType SpaceType;
        public Decimal Area;
        public Decimal AreaNet;
        public BareWallRVal BareWallRVal;
        public Decimal BasementWallExposedArea;
        public Decimal BasementWallExposedHeight;   //1
        public CLF CLF;
        public DepthRatio DepthRatio;
        public HLF HLF;
        public String InsulationAmount;
        public InsulationGaps InsulationGaps;
        public boolean InsulationMisaligned;
        public boolean IsDirtFloor;
        public boolean IsKneeslope;
        public NetRAdded NetRAdded;
        public PerimeterFactor PerimeterFactor;
        public boolean RigidInsulationCutAtGrade;
        public RigidInsulationExtent RigidInsulationExtent;
        public RigidInsulationExtentUsed RigidInsulationExtentUsed;
        public Decimal RValueConst;
        public SurfaceType SurfaceType;
        public Layers[] Layers;
    }
    class BandJoist {
    }
    
    class Slab {
    }
    public class Foundation {
        public String FoundationType;   //Basement
        public boolean BasementIsFinished;
        public String Name; //Basement
        public Notes Notes;
        public String SpaceConditioning;    //Unconditioned
        public SpaceConditioning_Internal SpaceConditioning_Internal;
        public Decimal CeilingHeight;   //8
        public FloorCeiling[] Ceilings;
        public FloorCeiling[] Floors;
        public boolean IsDisconnected;
        public SpaceType SpaceType;
        public TempC TempC;
        public TempH TempH;
        public Wall[] Walls;
        public boolean WallSpacesIncludesOther;
    }
    public class WindowsAndSkylights {
        public String Name;
        public String FrontWindowQualitativeAmount; //Average
        public String BackWindowQualitativeAmount;  //Average
        public String LeftWindowQualitativeAmount;  //Average
        public String RightWindowQualitativeAmount; //Average
        public Decimal FrontWindowFraction; //21
        public Double FrontWindowAreaSQFT;  //86.79
        public Decimal BackWindowFraction;  //21
        public Double BackWindowAreaSQFT;   //86.79
        public Decimal LeftWindowFraction;  //17
        public Double LeftWindowAreaSQFT;   //48.21
        public Decimal RightWindowFraction; //17
        public Double RightWindowAreaSQFT;  //48.21
        //public Windows[] Windows;
        public WindowsAndSkylightsSet[] WindowsSets;
        public WindowsAndSkylightsSet[] SkylightsSets;
    }
    public class WindowsAndSkylightsSet{
        public String Name; //Window Set
        public String Notes;    //
        public String Type; //Double, Vinyl
        public String SashOprationType; //Double Hung
        public Double UFactor;  //0.46
        public Double SHGC; //0.57
        public boolean HasStromWindow;
        public Windows[] Windows;
        public Skylights[] Skylights;
    }
    public class Windows {
        public String AttchedToWall;    //Exterior Wall
        public String BuildingFace; //Front
        public String Orientation;  //South
        public decimal Qty;
        public decimal Each_Window_Width_Inch;
        public decimal Each_Window_Height_Inch;
        public Decimal Each_Window_Area_Sqft;
        public Double Total_Area_Sqft;  //86.8
        public String Notes;
    }
    class Qty {
    }
    class Each_Window_Width_Inch {
    }
    class Each_Window_Height_Inch {
    }
    class Skylights {
    }
    class SkylightsSet {
    }
    public static DefaultHomeDetailWrapper parse(String json){
        return (DefaultHomeDetailWrapper) System.JSON.deserialize(json, DefaultHomeDetailWrapper.class);
    }
}