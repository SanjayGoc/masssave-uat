@isTest
     public class dsmtBIBUReviewTest {
       static testMethod void test()
       {
       Id revred = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Work Scope Review Manual').getRecordTypeId();
      
        Review__c r1= new Review__c();
        r1.Status__c='Failed Admin Review - Send To Energy Specialist';
        r1.RecordtypeId= revred;
        insert r1;
    
        
        
    
        Project_Review__c pr = new Project_Review__c();
        pr.Review__c=r1.id;
        insert pr;
        
        Review__c r= new Review__c();
        r.Status__c='Failed Admin Review - Send To Energy Specialist';
        r.RecordtypeId= revred;
        r.Pre_Work_Review__c=r1.id; 
        insert r;
        
        
        update r;
     }
}