public class dsmtCreatePortalUserForEmployeeCntrl{
 
    // Constructor - this only really matters if the autoRun function doesn't work right
    public dsmtCreatePortalUserForEmployeeCntrl() {
    }
     
    // Code we will invoke on page load.
    public PageReference autoRun() {
        
        String TaId = ApexPages.currentPage().getParameters().get('Id');
        
        List<DSMTracker_Contact__c> dsmtList = [select id,email__c from DSMTracker_Contact__c where Trade_Ally_Account__c =: TaId];
        
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/' + TaId);
        pageRef.setRedirect(true);
        return pageRef;
 
    }
    
}