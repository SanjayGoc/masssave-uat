public class dsmtGetAppointmentCall{
    
    public static void getAppointments(){
        dsmtScheduleRequestWrapper reqObj = new dsmtScheduleRequestWrapper();
        reqObj.loginDetail = new dsmtScheduleRequestWrapper.LoginWrapper();
        reqObj.loginDetail.sessionID = UserInfo.getSessionId();
        
        reqObj.workOrderDetail = new dsmtScheduleRequestWrapper.WorkOrderDetailWrapper();
        reqObj.workOrderDetail.workOrderId = '906F000001TSEIbIAO';
        reqObj.workOrderDetail.city = 'Udaipur';
        reqObj.workOrderDetail.country = 'India';
        reqObj.workOrderDetail.postalCode = '313001';
        reqObj.workOrderDetail.serviceAddress = 'Plot No: 1, Goravpadh Rd, Pula, R.K. Circle, Goravpadh, Pulla Bhuwana, Rupsagar';
        reqObj.workOrderDetail.serviceDuration = '120';
        reqObj.workOrderDetail.territoryId = '50130000000014c';
        reqObj.workOrderDetail.state = 'Rajasthan';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(System_Config__c.getInstance().URL__c+'GetAppointment?type=json');
        req.setMethod('POST');
        req.setBody(JSON.serialize(reqObj));
        req.setHeader('content-type', 'application/json');
        
        if(!Test.IsrunningTest()){
        HttpResponse res = h.send(req);
        
        String str = res.getBody();
        system.debug('str '+ str);
        dsmtScheduleRequestWrapper.Address obj = (dsmtScheduleRequestWrapper.Address) System.JSON.deserialize(str, dsmtScheduleRequestWrapper.Address.class);
        system.debug('--obj---'+obj);
        }
    }
}