public class dsmtCreateTerritoryController{
    public list<Employee__c> allLocationEmps{get;set;}
    public Location__c location {get;set;}
    public list<SelectOption> empOptions {get;set;}
    public list<SelectOption> empDeleteOptions {get;set;}
    public list<Territory__c> territoryList {get;set;}
    
    public string empId {get;set;}
    Transient public string locId;
    public string empDeleteId {get;set;}
    public string coordinates {get;set;}
    public string terId {get;set;}
    public string shapeType {get;set;}
    public string territoryName {get;set;}
    public decimal radius {get;set;}
    public list<Location__c> locationList {get;set;}
        
    public dsmtCreateTerritoryController(ApexPages.StandardController controller) {
        init();
    }

    public dsmtCreateTerritoryController()
    {
        init();
    }
    
    private void init()
    {
        
        allLocationEmps = new list<Employee__c>();
        location        = new Location__c();
        empOptions      = new list<SelectOption>();
        empDeleteOptions= new list<SelectOption>();
        territoryList   = new list<Territory__c>();
        
        locId = ApexPages.currentPage().getParameters().get('id');
        territoryList   = QueryTerritories();
        
        allLocationEmps = QueryAllEmployees(locId);
        allLocationEmps.sort();
        
        location        = FetchLocation(locId);
        
        set<string> empIdSet = new set<string>();
        empOptions.add(new SelectOption('', '--None--'));
        empDeleteOptions.add(new SelectOption('', '--None--'));
        for(Territory__c t : [select id, Name, Territory_Name__c, Employee__c from Territory__c where Location__c =: locId]){
            empIdSet.add(t.Employee__c);
            if(t.Territory_Name__c != null){
                empDeleteOptions.add(new SelectOption(t.Id, t.Territory_Name__c));
            }
        }
        
        for(Employee__c emp : allLocationEmps)
        {
            if(!empIdSet.contains(emp.Id))
                empOptions.add(new SelectOption(emp.Id, emp.Name));
            else{
                //empDeleteOptions.add(new SelectOption(emp.Id, emp.Name));
            }
        }
        
        locationList = new list<Location__c>();
        locationList = [select id, Name from Location__c];
    } 
    
    //
    private list<Territory__c> QueryTerritories(){
        return [select id, Name, Employee__c, ShapeType__c,  Territory_Name__c, Address__c, 
               (select id, Sequence__c, Coordinates__latitude__s, Coordinates__longitude__s, Radius__c
                from Territory_Details__r
                where Coordinates__latitude__s != null
                and Coordinates__longitude__s != null
                order by Sequence__c)
                from Territory__c 
                //where Location__c =: locId
                where ShapeType__c != null
                order by Name
                limit 1000];
    }
    
    //Get current Location record
    private Location__c FetchLocation(string locationId) {
        return [select id, Name, GoogleMapIcon__c, Address__c, City__c, Country__c, 
                Zip__c from Location__c where id =: locationId
                order by name limit 1];
    }
    
    private static Region__c QueryRegion(string regionId)
    {
        system.debug('--regionId ---'+regionId);
        return [select id, Name, Location__c from Region__c where id =: regionId limit 1];
    }
    
    private static list<Employee__c> QueryAllEmployees(string selectedLocation)
    {
        
        string employeeString = ' Select Id, Name, Team_Color__c, Active__c, Address__c, City__c, Date_of_Birth__c, Department_ID__c, Email__c, '+
                                ' Employee_Id__c, Home_Phone__c, Job_Code__c, Location__c, Mobile_Phone__c, Start_Date__c, '+
                                ' State__c, Status__c, Work_Teams__c, Zip__c'+
                                ' FROM Employee__c e '+
                                ' WHERE Location__c = \'' + selectedLocation +'\''+
                                ' AND Status__c != \'' + 'I' + '\'';
        
        return database.query(employeeString);
    }
    
    @RemoteAction
    public static boolean checkTerritory(string terrName, string locId)
    {
        list<Territory__c> territoryList = [select id 
                                            from Territory__c 
                                            where Territory_Name__c =: terrName
                                            and Territory_Name__c != null
                                            and Location__c =: locId];
        
        if(territoryList.size() > 0)
            return true;
        
        return false;
    }
    
    @RemoteAction
    public static string saveTerritory(string terrName, string adrs, decimal lat, decimal lng, string locId)
    {
        Territory__c newTerr = new Territory__c(Territory_Name__c = terrName,
                                                Address__c = adrs,
                                                Location__c = locId,
                                                Geo_Coordinates__Latitude__s = lat,
                                                Geo_Coordinates__Longitude__s = lng);
        insert newTerr;
        
        return newTerr.Id;
    }
    
    public PageReference SaveTerritory()
    {
        Territory__c territory = new Territory__c(Id = terId,
                                                  Employee__c = empId,
                                                  ShapeType__c = shapeType);
        
        update territory;
        
        list<Territory_Detail__c> territoryDetailList = new list<Territory_Detail__c>();
        
        list<string> splitedCoordinates = coordinates.replace('(','').replace(')','').split('~~~');
        
        integer sequence = 0;
        for(string coordinate : splitedCoordinates)
        {
            sequence++;
            string cordinate = coordinate.replace(',','~~~');
            list<string> lanLongList = cordinate.split('~~~');
            
            Territory_Detail__c territoryDetail = new Territory_Detail__c(Coordinates__latitude__s = decimal.valueOf(lanLongList[0].trim()),
                                                                          Coordinates__longitude__s = decimal.valueOf(lanLongList[1].trim()),
                                                                          Territory__c = territory.Id,
                                                                          Sequence__c = sequence,
                                                                          Radius__c = radius);
        
            territoryDetailList.add(territoryDetail);
        }
        
        if(territoryDetailList.size() > 0)
            insert territoryDetailList;
        
        return new PageReference('/'+locId);
    }
    
    public void DeleteEmpTerritory()
    {
        list<Territory__c> territoryDeleteList = [select id from Territory__c where id =: empDeleteId];
        
        if(territoryDeleteList.size() > 0)
            delete territoryDeleteList;
    }
}