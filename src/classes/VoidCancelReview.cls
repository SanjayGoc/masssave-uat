global class VoidCancelReview{
    
    webservice static String voidPayments(string ReviewId){
        try{
         
         String year = String.valueOf(Date.today().Year());
         String Month = String.valueOf(Date.today().Month());
         String Day = String.valueOf(Date.today().Day());
         
         if(Month != null && Month.length() == 1){
             Month = '0'+Month;
         }
         
         if(Day != null && Day.length() == 1){
             Day = '0'+Day;
         }
         
        String BatchStr = year+Month+Day;
        
        List<Payment__c> payLineList = [select id,Batch__c from Payment__c where Batch__c like : batchStr + '%' and Status__c != 'Approved'];
        
        String newBatchStr = BatchStr + 'A';
        
        Map<Integer,String> BatchMap = new Map<Integer,String>();
        BatchMap.put(0,'A');BatchMap.put(1,'B');BatchMap.put(2,'C');BatchMap.put(3,'D');BatchMap.put(4,'E');
        BatchMap.put(5,'F');BatchMap.put(6,'G');BatchMap.put(7,'H');BatchMap.put(8,'I');BatchMap.put(9,'J');
        BatchMap.put(10,'K');BatchMap.put(11,'L');BatchMap.put(12,'M');BatchMap.put(13,'N');BatchMap.put(14,'O');
        BatchMap.put(15,'P');BatchMap.put(16,'Q');BatchMap.put(17,'R');BatchMap.put(18,'S');BatchMap.put(19,'T');
        BatchMap.put(20,'U');BatchMap.put(21,'V');BatchMap.put(22,'W');BatchMap.put(23,'X');BatchMap.put(24,'Y');
        BatchMap.put(25,'Z');
        
        
        integer j = 0;
        
        Set<String> paybatch = new Set<String>();
        
        for(Payment__c pay : payLineList){
            paybatch.add(pay.Batch__c);
            //if(BatchStr+BatchMap.get(i) != pay.Batch__c){
              //  i++;
            //}
        }
        for(integer i = 0; i < 26; i++){
            if(paybatch.add(BatchStr+BatchMap.get(i))){
                break;
            }else{
                j++;
            }
        }
        
        system.debug('--BatchStr+BatchMap.get(j)---'+BatchStr+BatchMap.get(j));
         
         List<Payment__c> paymentList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Payment__c','Review__c = : ReviewId and Voided_or_Cancelled__c = false','')); 
         
         List<Payment_Line_Item__c> newpliList = new List<Payment_Line_Item__c>();
         List<Payment__c> newpayment = new List<Payment__c>();
         Set<Id> payId = new Set<Id>();
         if(paymentList!= null && paymentList.size() > 0){
             
           
           
           for(Payment__c pay :paymentList){
               PayId.add(pay.Id);
               
               Payment__c objpay = pay.clone(false,false);
               objpay.Amount__c = pay.Amount__c *-1 ;
               objPay.Voided_Payment__c = pay.Id;
               objPay.Itemcode__c = 'CONTR_ADJ';
               objpay.Voided_or_Cancelled__c = true;
               newpayment.add(objpay);
               pay.Voided_or_Cancelled__c = true;
               pay.Batch__c = BatchStr+BatchMap.get(j);
               objPay.Batch__c = BatchStr+BatchMap.get(j);
               //pay.Notes__c = voidcomment;
              
               
               
           }
           if(newpayment.size() > 0){
                insert newpayment ;
                
                List<Payment_Line_Item__c> paymentLineList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Payment_Line_Item__c','Payment__c in : payId','')); 
                
                Map<Id,Id> newpayMap = new Map<Id,Id>();
                Payment_Line_Item__c newpli = null;
                
                for(Payment__c p : newpayment){
                    newPayMap.put(p.Voided_Payment__c,p.Id);
                }
                
                if(paymentLineList != null && paymentLineList.size() > 0){
                    for(Payment_Line_Item__c pli : paymentLineList){
                        pli.Void_Payment__c = true;
                        newpli = new Payment_Line_Item__c();
                        newpli = pli.clone(false,false);
                        newpli.Itemcode__c = 'CONTR_ADJ';
                        newpli.Void_Payment__c = true;
                        if(pli.Amount__c != null){
                            newpli.Amount__c = pli.Amount__c * -1;
                        }
                        newpli.Payment__c = newPayMap.get(pli.Payment__c);
                        pli.Batch__c = BatchStr+BatchMap.get(j);
                        newpli.Batch__c = BatchStr+BatchMap.get(j);
                        newpliList.add(newpli);
                    }
                    update paymentLineList;
                }
           }
           if(newpliList != null && newpliList.size() >0) {
               insert newpliList ;
            }
            update paymentList;
            return 'success';
          }
        }
        catch(Exception ex){
            return ex.getMessage();
        }
        
        return 'All payments are already voided';
    }
    
    webservice static String voidInvoiceLineItems(string ReviewId){
        try{
        
        List<Invoice_Line_Item__c> invLineList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Invoice_Line_Item__c','Review__c =: ReviewId and Voided__c = false','')); 
                
        Invoice_Line_Item__c newinvli = null;
        
        List<Invoice_Line_Item__c> newLineList = new List<Invoice_Line_Item__c> ();
        
        if(invLineList != null && invLineList.size() > 0){
            for(Invoice_Line_Item__c invli : invLineList ){
                invli.Voided__c = true;
                newinvli = new Invoice_Line_Item__c ();
                newinvli = invli.clone(false,false);
                if(invli.Qty__c != null){
                    newinvli.QTY__c = invli.Qty__c * -1;
                }
                if(invli.Calculated_Incentive_Amount__c != null){
                    newinvli.Calculated_Incentive_Amount__c = invli.Calculated_Incentive_Amount__c* -1;
                }
                newinvli.Voided__c = true;
                newLineList.add(newinvli);
                newLineList.add(invli);
            }
        }

        if(newLineList != null && newLineList.size() >0) {
            upsert newLineList;
            return 'success';
        }
         
         }catch(Exception e){
         
         }
         return null;
    }
    
    webservice static String voidAppointmentInvoiceLineItems(string workOrderId){
        try{
        
        // List<Appointment__c> listOfAppointments = Database.query(dsmtFuture.getCreatableFieldsSOQL('Appointment','Id =: appointmentId',''));
        
        List<Invoice_Line_Item__c> invLineList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Invoice_Line_Item__c','Workorder__c =: workOrderId',''));
                
        Invoice_Line_Item__c newinvli = null;
        
        List<Invoice_Line_Item__c> newLineList = new List<Invoice_Line_Item__c> ();
        
        if(invLineList != null && invLineList.size() > 0){
            for(Invoice_Line_Item__c invli : invLineList ){
                invli.Voided__c = true;
                newinvli = new Invoice_Line_Item__c ();
                newinvli = invli.clone(false,false);
                newinvli.QTY__c = invli.Qty__c * -1;
                newinvli.Voided__c = true;
                newLineList.add(newinvli);
                newLineList.add(invli);
            }
        }

        if(newLineList != null && newLineList.size() >0) {
            upsert newLineList;
            return 'success';
        }
         
         }catch(Exception e){
         
         }
         return null;
    }
    
    webservice static String InsertInvoiceLineItemWhenStatusBatched(string revId)
    {
         list<Review__c> newlist = [select Id,Status__c,Installed_Date__c from Review__c where Id = :revId];
            map<id, Review__c> oldmap = new map<id, Review__c>();
            Set<Id> revids = new Set<Id>();
    
            Set<Id> projId = new Set<Id>();
            
            Set<String>  invoiceIds=new Set<String>();
            
            DAte installedDate = Date.Today();
            
            List<Invoice_Line_Item__c> iLItem = new List<Invoice_Line_Item__c>();
            List<Invoice__c> invData= new List<Invoice__c>();
         try{       
            for (Review__c rev: newlist) {
                if (rev.Status__c == 'Batched'){
                    revids.add(rev.Id);
                    if(rev.Installed_Date__c != null)
                        installedDate = rev.Installed_Date__c;
                }
            }
            
            Date startDate = installedDate.toStartOfMonth();
            Date endDate = startDate.addMonths(1).addDays(-1);
            
            if (revids != null) {
                List<Project_Review__c> revList = [select id, name, Review__c, Project__c from Project_Review__c where Review__c in: revids];
            
                for (Project_Review__c rev: revList) {
                    projId.add(rev.Project__c);
                }
            
                if (projId.size() > 0) {
                 // DSST-16299 by HP on 1st Dec 2018
                 
                List<Recommendation__c> recomList = [select id,EXT_JOB_ID__c,Energy_Assessment__R.Name, name,Invoice_Type__c,Isinvoiced__c, Quantity__c,Installed_Date__c,Unit_Cost__c,
                                                        Energy_Assessment__r.Primary_Provider__c,DSMTracker_Product__r.EMHome_EMHub_PartID__c,Category__c,
                                                        Recommendation_Scenario__r.Type__c,Calculated_Incentive__c,DSMTracker_Product__r.Do_Not_Create_Invoice_Line__c,
                                                        (select Id,Voided__c from invoice_Line_Items__r where Voided__c = false),
                                                      (select Id,Proposal__c,Proposal__r.Final_Proposal_Incentive__c,Proposal__r.Barrier_Incentive_Amount__c from Proposal_Recommendations__r where  Proposal__r.Status__c !=: 'Inactive' )   
                                                      from Recommendation__c where Recommendation_Scenario__c in: projId
                                                     /* and IsInvoiced__c = false */ and Invoice_Type__c  != null
                                                      and Invoice_Type__c != 'Not Found' and Installed_Date__c != null];
                
                Map<String,List<Recommendation__c>> RecTypeMap = new Map<String,List<Recommendation__c>>();
                List<Recommendation__c> temp = null;
                                        
                if (recomList.size() > 0) {
                  for (Recommendation__c r: recomList) {
                      system.debug('---r.Invoice_Type__c---'+r.Invoice_Type__c);
                   //  r.Isinvoiced__c = true;
                    if(r.Invoice_Type__c  != null && r.Invoice_Type__c != 'Not Found'){
                        invoiceIds.add(r.Invoice_Type__c);
                    }
                    
                    if(r.Category__c != 'Direct Install' && r.Recommendation_Scenario__r.Type__c != null){
                        if(RecTypeMap.get(r.Recommendation_Scenario__r.Type__c) != null){
                            
                            temp = RecTypeMap.get(r.Recommendation_Scenario__r.Type__c);
                            temp.add(r);
                            RecTypeMap.put(r.Recommendation_Scenario__r.Type__c,temp);
                        }else{
                            temp = new List<Recommendation__c>();
                            temp.add(r);
                            RecTypeMap.put(r.Recommendation_Scenario__r.Type__c,temp);
                        }
                    }
                  } 
                 // update recomList;
                }
             
             Map<String,Invoice__c> invTypeMap = new Map<String,Invoice__c>();
             
             List<Invoice__c> invList = [Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:invoiceIds 
                                         and Status__c = 'UnSubmitted' and Invoice_Date__c != null 
                                         And Invoice_Date__c >=: installedDate ORDER BY Invoice_Date__c  ASC];
                
                for(Invoice__c inv : invList){
                    invTypeMap.put(inv.Invoice_Type__c,inv);
                }
                
                Set<Id> recId = new set<Id>();
                Set<Id> CreatedrecId = new set<Id>();
                
                List<Recommendation__c> InvToCreate = new List<Recommendation__c>();
                
                Set<String> ExtInvoiceType = new Set<String>();
                    
                if(invList.size()>0) {
                    
                     for( Invoice__c  inv:invList){
                         ExtInvoiceType.add(inv.Invoice_Type__c);
                     }
                         
                     for( Invoice__c  inv:invList){
                         
                       
                        /*String target = '/'; 
                        String replacement = '';
                        
                        String formatedDate= inv.Invoice_Date__c.format().replace(target, replacement); */
                        
                        String target = '/'; 
                        String replacement = '';                      
                        Datetime output = inv.Invoice_Date__c;                       
                        String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);


                        
                        for(Recommendation__c r: recomList) {
                            
                           /* system.debug('--r.Invoice_Type__c---'+r.Invoice_Type__c);
                            system.debug('--inv.Invoice_Type__c---'+inv.Invoice_Type__c);
                            system.debug('--r.Isinvoiced__c---'+r.Isinvoiced__c);*/
                            
                           if(inv.Invoice_Type__c  == r.Invoice_Type__c && (r.Isinvoiced__c == false || r.invoice_Line_Items__r.size() == 0) ){
                              
                              Invoice_Line_Item__c createILItemData;
                               
                               //system.debug('--r.Installed_Date__c---'+r.Installed_Date__c);
                           // system.debug('--inv.Invoice_Date__c---'+inv.Invoice_Date__c);
                            
                                if(r.Installed_Date__c <= inv.Invoice_Date__c){
                                    recId.add(r.Id);
                                    if(CreatedrecId.add(r.Id)){
                                     createILItemData = new Invoice_Line_Item__c(
                                         Invoice__c = inv.Id,
                                         Recommendation__c = r.Id,
                                         Status__c=inv.Status__c,
                                         QTY__c= r.Quantity__c,
                                         Unit_Cost__c = r.Unit_Cost__c,
                                         INSTALLED_DATE__c = r.INSTALLED_DATE__c,
                                         Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                         //Review__c = newlist.get(0).Id
                                     );
                                     if(r.Category__c != 'Direct Install'){
                                         createILItemData.Review__c = newlist.get(0).Id;
                                     }
                                     if(r.Installed_Quantity__c != null){
                                         createILItemData.QTY__c = r.Installed_Quantity__c;
                                     }
                                     system.debug('--createILItemData---'+createILItemData);
                                   iLItem.add(createILItemData);
                                   
                                   r.Isinvoiced__c = true;
                                   r.EXT_JOB_ID__c = r.Energy_Assessment__R.Name+formatedDate+inv.Invoice_Type__c ;
                                   }
                                }else{
                                    if(recId.add(r.Id)){
                                        ExtInvoiceType.add(inv.Invoice_Type__c);
                                        InvToCreate.add(r);
                                    }
                                }
                           }else{
                               system.debug('---ExtInvoiceType---'+ExtInvoiceType);
                               system.debug('---inv.Invoice_Type__c---'+inv.Invoice_Type__c);
                               
                               if(recId.add(r.Id) && ExtInvoiceType.add(r.Invoice_Type__c)){
                                    system.debug('--r.Invoice_Type__c123---'+r.Invoice_Type__c);
                                    InvToCreate.add(r);
                                }
                           }
                           
                        }
                        system.debug('--InvToCreate--'+InvToCreate);
                    }
                }
                else  {
                    /*String target = '/'; 
                    String replacement = '';
                    String formatedDate= endDate.format().replace(target, replacement); */
                    
                    String target = '/'; 
                    String replacement = '';                      
                    Datetime output = endDate;                       
                    String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                    

                      for(Recommendation__c r: recomList) {
                          // DSST-14968 by HP on 1st Dec 2018
                          if(r.Invoice_Type__c != null && r.Invoice_Type__c != ''){
                              if(!r.DSMTracker_Product__r.Do_Not_Create_Invoice_Line__c){
                                  if(ExtInvoiceType.add(r.Invoice_Type__c)){
                                      Invoice__c invRecord;
                                      invRecord=new Invoice__c(
                                      Invoice_Type__c = r.Invoice_Type__c,
                                      Invoice_Date__c=endDate,
                                      Status__c = 'In Process',
                                      Provider__c = r.Energy_Assessment__r.Primary_Provider__c
                                      );
                                      invRecord.Invoice_Number__c = formatedDate + r.Invoice_Type__c;
            
                                      invData.add(invRecord);
                                      
                                  }
                              }
                          }
                      }
                          
                  }
        
        system.debug('--invDate---'+invData);
        Set<String> invType = new Set<String>();
            
        for(Recommendation__c r : invToCreate){
            // DSST-14968 by HP on 1st Dec 2018
            if(r.Invoice_Type__c != null && r.Invoice_Type__c != ''){
                if(!r.DSMTracker_Product__r.Do_Not_Create_Invoice_Line__c){
                    if(invType.Add(r.Invoice_Type__c)){
                        /*String target = '/'; 
                        String replacement = '';
                        String formatedDate= endDate.format().replace(target, replacement);*/
                        
                        String target = '/'; 
                            String replacement = '';
                            
                            Datetime output = endDate;
                            
                            String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                            
                        Invoice__c invRecord;
                        invRecord=new Invoice__c(
                        Invoice_Type__c = r.Invoice_Type__c,
                        Invoice_Date__c=endDate,
                        Status__c = 'In Process',
                        Provider__c = r.Energy_Assessment__r.Primary_Provider__c
                        );
                        invRecord.Invoice_Number__c = formatedDate + r.Invoice_Type__c;
                        
                        invData.add(invRecord);
                    }       
                    
                }
            }                  
                          
        }       
        
        if(invData.size()>0){
           insert invData;
           
           for(Invoice__c inv : invData){
                invTypeMap.put(inv.Invoice_Type__c,inv);
            }
           system.debug('--invoiceIds---'+invoiceIds);
           
           invList=[Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:invoiceIds 
                                         and Status__c = 'Unsubmitted' and Invoice_Date__c != null And Invoice_Date__c >=: installedDate
                                          ORDER BY Invoice_Date__c  ASC];
            
            Set<Id> NewRecId = new Set<Id>();
            
            if(invList.size()>0) {
                 for( Invoice__c  inv:invList){
                     if(inv.Invoice_Date__c != null){
                        for(Recommendation__c r: recomList) {
                        
                        /*String target = '/'; 
                        String replacement = '';
                        String formatedDate= inv.Invoice_Date__c.format().replace(target, replacement); */
                        
                        String target = '/'; 
                        String replacement = '';                      
                        Datetime output = inv.Invoice_Date__c;                       
                        String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);


                        
                        system.debug('--r.Invoice_Type__c---'+r.Invoice_Type__c);
                        system.debug('--inv.Invoice_Type__c---'+inv.Invoice_Type__c);
                        system.debug('--r.Isinvoiced__c---'+r.Isinvoiced__c);
                          
                          if(inv.Invoice_Type__c  ==r.Invoice_Type__c  ){
                                  Invoice_Line_Item__c createILItemData;
                                     
                                    system.debug('--r.Installed_Date__c---'+r.Installed_Date__c);
                                    system.debug('--inv.Invoice_Date__c---'+inv.Invoice_Date__c);
                            
                                    if(r.Installed_Date__c <= inv.Invoice_Date__c){
                                        if(CreatedrecId.add(r.Id)){
                                         system.debug('---inv.Id---'+inv.Id);
                                         createILItemData = new Invoice_Line_Item__c(
                                         Invoice__c = inv.Id,
                                         Recommendation__c = r.Id,
                                         Status__c=inv.Status__c,
                                         QTY__c= r.Quantity__c,
                                         Unit_Cost__c = r.Unit_Cost__c,
                                         INSTALLED_DATE__c = r.INSTALLED_DATE__c,
                                         Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                         
                                         //Review__c = newlist.get(0).Id
                                     );
                                     if(r.Category__c != 'Direct Install'){
                                         createILItemData.Review__c = newlist.get(0).Id;
                                     }
                                          iLItem.add(createILItemData);
                                    r.Isinvoiced__c = true;
                                      r.EXT_JOB_ID__c = r.Energy_Assessment__R.Name+formatedDate+inv.Invoice_Type__c ;
                                        }
                                    system.debug('--createILItemData---'+createILItemData);
                                    
                               }
                           }
                        }
                    }
                    }
                }
        }
        if(RecTypeMap != null && RecTypeMap.keyset().size() > 0){
            
            Map<String,Decimal> RecTypeTotal = new Map<String,Decimal>();
            
            for(String str : RecTypeMap.keyset()){
                RecTypeTotal = new Map<String,Decimal>();
                
                system.debug('--str---'+str);
                
                for(Recommendation__c rec : RecTypeMap.get(str)){
                    if(rec.invoice_Type__c != null && rec.Invoice_Type__c.contains('413')){
                        if(RecTypeTotal.get(rec.invoice_Type__c) != null){
                            RecTypeTotal.put(rec.invoice_Type__c,RecTypeTotal.get(rec.invoice_Type__c)+rec.Calculated_Incentive__c);
                        }else{
                            RecTypeTotal.put(rec.invoice_Type__c,rec.Calculated_Incentive__c);
                        }
                    }
                }
                
                for(String s : RecTypeTotal.keyset()){
                    for( Invoice__c  inv:invList){
                        if(inv.Invoice_Type__c  == s && InstalledDate <= inv.Invoice_Date__c && Enddate >= inv.Invoice_Date__c){
                            system.debug('--s--'+s);
                            Invoice_Line_Item__c createILItemData = new Invoice_Line_Item__c(
                                 Invoice__c = invTypeMap.get(s).Id,
                                 Status__c=invTypeMap.get(s).Status__c,
                                 Calculated_Incentive_Amount__c = RecTypeTotal.get(s),
                                 Project_Type__c = str,
                                 MEA_Id__c = 'Incentive',
                                 //QTY__c= r.Quantity__c,
                                 //Unit_Cost__c = r.Unit_Cost__c,
                                 INSTALLED_DATE__c = INSTALLEDDATE,
                              //   Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                 
                                 Review__c = newlist.get(0).Id
                             );
                             iLItem.add(createILItemData);
                             break;
                         }
                     }
                }
            }
        }
        
        system.debug('--iLItem---'+iLItem);
        if(iLItem.size()>0){
        dsmtRecommendationHelper.StopRecTrigger = true;
        ReviewTriggerHandler.Preventinsertinvoice  = true;
        insert iLItem;
        }
        dsmtRecommendationHelper.RollupOnProject = true;
        dsmtRecommendationHelper.disableRolluptriggeronRec = true;
        update recomList;
        }
        return 'success';
            }
          }
          catch(Exception ex){
            return ex.getMessage();
          }
        
        return null;
      }
      
      
      webservice static String InsertInvoiceLineItemWhenStatusBatchedWO(string woId1)
    {
            Set<Id> wtypeId = new Set<Id>();
            Set<Id> woId= new Set<Id>();
            woId.add(woId1);
         
             List<Workorder__c> lstWO = [select Id,Workorder_Type__c,Scheduled_Date__c from Workorder__c where Id = :woId1];
             wtypeId.add(lstWO[0].Workorder_Type__c);
             Date ScheduleDAte = lstWO[0].Scheduled_Date__c ;
            
         try{       
            List<Appointment__c> appList = [select id,Workorder__c,Trade_Ally_Account__c,Trade_Ally_Account__r.Internal_Account__c from Appointment__c
                                                    where workorder__c in  : woId];
                                                    
        Map<Id,boolean> appMap = new Map<Id,Boolean>();
        Map<Id,Appointment__c> appMapNew = new Map<Id,Appointment__c>();
        
        for( Appointment__c app : appList){
            appMap.put(app.Workorder__c,app.Trade_Ally_Account__r.Internal_Account__c);
            appMapNew.put(app.Workorder__c,app);
        }
        
        List<Workorder_Type__c> wtypeList = [select id,name from Workorder_Type__c where id in : wtypeId];
        
        Map<Id,String> wtypeMap = new Map<Id,String>(); 
        
        for(Workorder_Type__c wtype : wtypeList){
            wtypeMap.put(wtype.Id,wtype.Name);
        }
        
        List<Workorder_Type_Invoice_Item_Setting__c> wtypeSetting = [select id,Name__c,name,Workorder_Type__c,part_Id__c,Is_not_CLEAResult__c
                                                                      from Workorder_Type_Invoice_Item_Setting__c where Workorder_Type__c in : wtypeMap.values()];
        Map<String,String> partIdMap = new Map<String,String>();
        
        for(Workorder_Type_Invoice_Item_Setting__c wsetting : wtypeSetting){
            partIdMap.put(wsetting.Name__c,wsetting.Part_Id__c);
        }
        
        if(partIdMap.keyset().size() > 0){
            List<Dsmtracker_Product__c> dsmtList = [select id,name,Eversource_Labor_Price__c,EMHome_EMHub_PartID__c 
                                                             from dsmtracker_Product__c where EMHome_EMHub_PartID__c in : partIdMap.values()];
            
            if(dsmtList != null && dsmtList.size() > 0){
                Map<String,Dsmtracker_Product__c> dsmtMap = new Map<String,Dsmtracker_Product__c>();
                
                for(Dsmtracker_Product__c dsmt : dsmtList){
                    dsmtMap.put(dsmt.EMHome_EMHub_PartID__c,dsmt);
                }
                
                List<Invoice_Line_Item__c> invLineList = new List<Invoice_Line_Item__c>();
                Invoice_Line_Item__c newinvLine = null;
                
                MAp<String,String> invoiceIds = new Map<String,String>();
                
                List<Workorder__c> woList = [select id,Invoice_Type__c,Primary_Provider__c,Status__c,Workorder_Type__c,Scheduled_Date__c from Workorder__c where id in : woId];
                
                for(Workorder__c  wo : woList){
                    invoiceIds.put(wo.Invoice_Type__c,wo.Primary_Provider__c);
                }
                Set<String> newInvId = invoiceIds.keyset();
                
                List<Invoice__c > invList=[Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in: newInvId 
                                         and Status__c = 'Unsubmitted' and Invoice_Date__c >=: ScheduleDAte   ORDER BY Invoice_Date__c  ASC];
                
                if(invList != null && invList.size() > 0){
                
                }else{
                    Date startDate = ScheduleDAte.toStartOfMonth();
                    Date endDate = startDate.addMonths(1).addDays(-1);
        
                    /*String target = '/'; 
                    String replacement = '';
                    String formatedDate= endDate.format().replace(target, replacement); */
                    
                    String target = '/'; 
                    String replacement = '';                      
                    Datetime output = endDate;                       
                    String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                    
                    
                      
                      List<Invoice__c> invData= new List<Invoice__c>();

                      for(String s : invoiceIds.keyset()) {
                          // DSST-14968 by HP on 1st Dec 2018
                          if(s != null && s != ''){
                              Invoice__c invRecord;
                              invRecord=new Invoice__c(
                              Invoice_Type__c = s,
                              Invoice_Date__c=endDate,
                              Status__c = 'In Process',
                              Provider__c = invoiceIds.get(s)
                              );
                              invRecord.Invoice_Number__c = formatedDate + s;
    
                              invData.add(invRecord);
                          }
                          
                      }
                      if(invData.size() > 0){
                          insert invData;
                      }
                      invList=[Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:invoiceIds.keyset()
                                         and Status__c = 'Unsubmitted' ORDER BY Invoice_Date__c  ASC];
                }
                
                
                    List<Payment__c> payToinsert = new List<Payment__c>();
                    List<Payment_Line_Item__c> payLinetoInsert = new List<Payment_Line_Item__c>();
                    
                    for(Workorder__c  wo : woList){
                        
                        if(wo.Status__c == 'Completed' && wo.Workorder_Type__c != null){
                                
                            for( Invoice__c  inv:invList){
                                    
                                if(wo.Scheduled_Date__c <= inv.Invoice_Date__c){
                                 
                                    String woType = wtypeMap.get(wo.Workorder_Type__c);
                                    
                                    if(appMap.get(wo.Id) == false){
                                          woType += '_HPC';  
                                    }
                                    
                                    system.debug('--woType ---'+woType);
                                    
                                    if(woType  != null && partIdMap.get(woType) != null ){
                                        
                                        if(dsmtMap.get(partIdMap.get(woType)) != null){
                                        
                                            Dsmtracker_Product__c dsmt = dsmtMap.get(partIdMap.get(woType ));
                                            
                                            if(dsmt != null){
                                                newinvLine = new Invoice_Line_Item__c();
                                                newinvLine.Workorder__c = wo.Id;
                                                newinvLine.Invoice__c = inv.Id;
                                                newinvLine.Qty__c = 1;
                                                newinvLine.Unit_cost__c = dsmt.Eversource_Labor_Price__c;
                                                newinvLine.DSMTracker_Product__c = dsmt.Id;
                                                invLineList.add(newinvLine);
                                                break;
                                            }
                                        
                                        } 
                                    }
                                }
                            }
                            
                            String woType = wtypeMap.get(wo.Workorder_Type__c);
                                    
                            if(appMap.get(wo.Id) == false){
                                  woType += '_HPC';  
                            }
                            
                            system.debug('--woType ---'+woType);
                            
                            if(woType  != null && partIdMap.get(woType) != null ){
                                
                                if(dsmtMap.get(partIdMap.get(woType)) != null){
                                
                                    Dsmtracker_Product__c dsmt = dsmtMap.get(partIdMap.get(woType ));
                                    
                                    if(dsmt != null){
                                        Payment__c pay = new Payment__c();
                                        pay.Workorder__c = wo.Id;
                                        pay.Amount__c = dsmt.Eversource_Labor_Price__c;
                                        pay.Trade_Ally_Account__c = appMapNew.get(wo.Id).Trade_Ally_Account__c;
                                        pay.Customer__c = wo.Customer__c;
                                        payToinsert.add(pay);
                                        
                                        Payment_Line_Item__c payLine = new Payment_Line_Item__c ();
                                        payLine.Workorder__c = wo.Id;
                                        payLine.Amount__c = dsmt.Eversource_Labor_Price__c;
                                        payLine.DSMTracker_Product__c = dsmt.Id;
                                        payLinetoInsert.add(payLine);
                                        //invLineList.add(newinvLine);
                                       // break;
                                    }
                                
                                } 
                            }
                        }
                    }
                    
                    if(PayToinsert.size() > 0){
                        insert PayToinsert;
                        
                        Map<Id,Id> payMap = new Map<Id,Id>();
                        
                        for(Payment__c pay : PayToinsert){
                            payMap.put(pay.Workorder__c,Pay.Id);
                        }
                        
                        if(PayLineToinsert.size() > 0){
                            for(Payment_Line_Item__c pl : PayLineToinsert){
                                pl.Payment__c = payMap.get(pl.Workorder__c);
                            }
                            insert PayLineToinsert;
                        }
                    }
                    if(invLineList.size() > 0){
                    insert invLineList;
                }
                }
                
            }
          }
          catch(Exception ex){
            return ex.getMessage();
          }
        
        return null;
      } 
}