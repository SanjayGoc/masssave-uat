Public Class dsmtProposalViewCntrl{

    Public String propId{get;set;}
    public Boolean isWarningMessage{get;set;}
    public Proposal__c Prop{get;set;}
    
    public dsmtProposalViewCntrl(ApexPages.StandardController controller) {
        
        isWarningMessage = false;
        propId = controller.getId();
        
        String Userid =  userInfo.getUserId();
        list<User> userlist = [Select id,Profile.Name from User where id =: Userid];                      
        String ProfileName = userlist.get(0).Profile.Name;
        
        List<Proposal__c> proplist = [select id,Name_Type__c,Energy_Assessment__c from Proposal__c where id =: propId];
        if(proplist.size() > 0 && !ProfileName.contains('HPC')){
            Prop = proplist.get(0);
            
            String assessId = Prop.Energy_Assessment__c;
            
            List<Energy_Assessment__c> EAlist = [Select id,Safety_Aspect__c,Safety_Aspect__r.Knob_Tube_Wiring_Present__c,Safety_Aspect__r.Knob_Tube_Wiring_Addressed__c,
                                             Safety_Aspect__r.Dryer_Vent_Needed__c,Safety_Aspect__r.Dryer_Vent_Addressed__c,Safety_Aspect__r.High_CO_Addressed__c,
                                             Safety_Aspect__r.High_CO_present__c from Energy_Assessment__c where id =: assessId];
             system.debug('EAlist-----'+EAlist);
             
             if(EAlist.Size() > 0){
                    Energy_Assessment__c ea = EAlist.get(0);
                    if((ea.Safety_Aspect__r.Knob_Tube_Wiring_Present__c == true && ea.Safety_Aspect__r.Knob_Tube_Wiring_Addressed__c == false)
                        ||(ea.Safety_Aspect__r.Dryer_Vent_Needed__c == true &&  ea.Safety_Aspect__r.Dryer_Vent_Addressed__c == false)
                        ||(ea.Safety_Aspect__r.High_CO_present__c == true  && ea.Safety_Aspect__r.High_CO_Addressed__c == false))
                    {
                        if(Prop.Name_Type__c == 'Air Sealing' || Prop.Name_Type__c == 'Weatherization'){
                             isWarningMessage = true;
                        }
                    }
              }
        }
    }

    
}