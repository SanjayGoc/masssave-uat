public class dsmtChangePasswordCont {
    
    public boolean isShowError{get;set;}
    public string currentPassword {get;set;}
    public string password        {get;set;}
    public string confirmPassword {get;set;}
    public boolean isPasswordChanged {get;set;}
    public Dsmtracker_Contact__c dsmtContact{get;set;}
    public Trade_Ally_Account__c tradeallyAccount{get;set;}
    public String accid{get;set;}
    public String conid{get;set;}
    
    public dsmtChangePasswordCont(){
       fetchdsmtContactAndTradeAllyAccount();
    }
    
    private void fetchdsmtContactAndTradeAllyAccount() {
        String usrid = UserInfo.getUserId();

        List < User > usrList = [select Id, SecurityQuestion__c, Ans_SecurityQuestion__c, ContactId, Contact.AccountId, Contact.Account.Name, Contact.Account.RecordType.Name, Contact.Name from user Where Id = : usrid];
        if (usrList != null && usrList.size() > 0) {
           
            if (usrList.get(0).ContactId != null) {
                accid = usrList.get(0).Contact.AccountId;
                conId = usrList.get(0).ContactId;

                
                List < DSMTracker_Contact__c > dsmtContLst = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c where Contact__r.Id = : conId and Trade_Ally_Account__c != null limit 1];
                if (dsmtContLst != null && dsmtContLst.size() > 0) {
                     
                     dsmtcontact = dsmtContLst.get(0);
                     
                     
                     List<Trade_Ally_Account__c> taList = [select id,name,Account__c, Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,Website__c,Phone__c,Email__c,Registration_Renewal_Date__c,
                                                                Outreach_rep__r.Phone,Outreach_rep__r.Email,Outreach_rep__r.Name from Trade_Ally_Account__c where Id =: dsmtcontact.Trade_Ally_Account__c];
                        if(taList != null && taList.size() > 0){
                            tradeallyAccount = taList.get(0);
                         }
                }
            }
        }
    }
    
    public void updatePassword(){
        
        PageReference pg = Site.changePassword(password, confirmPassword, currentPassword);
        
        if(pg == null)
            isPasswordChanged = false;
        else
            isPasswordChanged = true;
        password = confirmPassword = currentPassword = null;
        isShowError = true;
    }
}