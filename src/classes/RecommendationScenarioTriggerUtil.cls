public class RecommendationScenarioTriggerUtil {
    public static List<Enrollment_Application__c> fillEnrollmentApplication(RecommendationScenarioTriggerHandler.RSModel model){
        List<Enrollment_Application__c> retLst = new List<Enrollment_Application__c>();
        List<Recommendation_Scenario__c> lstRS = model.lstRs;
        Map<String, String> mapEAToWO = model.mapEAToWO;
        Map<String, String> mapEAToAppt = model.mapEAToAppt;
        Map<String, Appointment__c> mapWOToApptObj = model.mapWOToApptObj;
        Map<Id, Appointment__c> mapApptIdAppt = model.mapApptIdAppt;
        
        for(Recommendation_Scenario__c rs: lstRS){
            String energyAssesmentId = rs.Energy_Assessment__c;
            Appointment__c appt = null;
            if(mapEAToAppt.containsKey(energyAssesmentId)){
                String apptId = mapEAToAppt.get(energyAssesmentId);
                if(mapApptIdAppt.containsKey(apptId)){
                    appt = mapApptIdAppt.get(apptId);
                }
            }else if(mapEAToWO.containsKey(energyAssesmentId)){
                String woId = mapEAToWO.get(energyAssesmentId);
                if(mapWOToApptObj.containsKey(woId)){
                    appt = mapWOToApptObj.get(woId);
                }
            }
            if(appt!=null){
                String custFirstname = appt.Customer_Reference__r.First_Name__c;
                String custLastName = appt.Customer_Reference__r.Last_Name__c;
                String custPhone = appt.Customer_Reference__r.Phone__c;
                String custEmail = appt.Customer_Reference__r.Email__c;
                
                String custServiceAddress = appt.Customer_Reference__r.Service_Address__c; 
                String custServiceState = appt.Customer_Reference__r.Service_State__c;
                String custServiceCity = appt.Customer_Reference__r.Service_City__c;
                String custServiceZipcode = appt.Customer_Reference__r.Service_Zipcode__c;
                String custElecProviderName = appt.Customer_Reference__r.Electric_Provider_Name__c;
                String custGasProviderName = appt.Customer_Reference__r.Gas_Provider_Name__c;
                String custGasAccNo = appt.Customer_Reference__r.Gas_Account_Number__c;
                String custElecAccNo = appt.Customer_Reference__r.Electric_Account_Number__c;
                
                
                String taFirstName=appt.Trade_Ally_Account__r.First_Name__c;
                String taLastName=appt.Trade_Ally_Account__r.Last_Name__c;
                String taPhone = appt.Trade_Ally_Account__r.Phone__c;
                String taEmail = appt.Trade_Ally_Account__r.Email__c;
                
                String taStreetAddress = appt.Trade_Ally_Account__r.Street_Address__c; 
                String taStreetState = appt.Trade_Ally_Account__r.Street_State__c;
                String taStreetCity = appt.Trade_Ally_Account__r.Street_City__c;
                String taStreetZipcode = appt.Trade_Ally_Account__r.Street_Zip__c;
                
                String taMailAddress = appt.Trade_Ally_Account__r.Mailing_Address__c; 
                String taMailState = appt.Trade_Ally_Account__r.Mailing_City__c;
                String taMailCity = appt.Trade_Ally_Account__r.Mailing_City__c;
                String taMailZipcode = appt.Trade_Ally_Account__r.Mailing_Zip__c; 
                
                Enrollment_Application__c ea = new Enrollment_Application__c();
                
                /* From Customer */
                ea.Customer__c = appt.Customer_Reference__c;
                ea.Service_Address__c = custServiceAddress;
                ea.Service_State__c = custServiceState;
                ea.Service_City__c = custServiceCity;
                ea.Service_Zip_Code__c = custServiceZipcode;
                ea.Electric_account_number__c = custElecAccNo;
                ea.Gas_account_number__c = custGasAccNo;
                
                /* From Customer DSMTContact */
                Map<String, DSMTracker_Contact__c> mapCustIdToDC = model.mapCustIdToDC;
                if(mapCustIdToDC.containsKey(appt.Customer_Reference__c)){
                    ea.Contact_First_Name__c = mapCustIdToDC.get(appt.Customer_Reference__c).First_Name__c;
                    ea.Contact_Last_Name__c = mapCustIdToDC.get(appt.Customer_Reference__c).Last_Name__c;
                    ea.Applicant_Email_Address__c = mapCustIdToDC.get(appt.Customer_Reference__c).Email__c;
                    ea.Applicant_Phone_Number__c = mapCustIdToDC.get(appt.Customer_Reference__c).Phone__c;
                    ea.Contact_Address__c = mapCustIdToDC.get(appt.Customer_Reference__c).Address__c; 
                    ea.Contact_State__c = mapCustIdToDC.get(appt.Customer_Reference__c).State__c; 
                    ea.Contact_City__c = mapCustIdToDC.get(appt.Customer_Reference__c).City__c; 
                    ea.Contact_Zip_Code__c = mapCustIdToDC.get(appt.Customer_Reference__c).Zip__c; 
                    ea.Customer_Contact_DSMT__c = mapCustIdToDC.get(appt.Customer_Reference__c).Id; 
                }
                
                /* From TradAlly Account */
                ea.Trade_Ally_Street_Address__c = taStreetAddress;
                ea.Trade_Ally_State__c = taStreetState;
                ea.Trade_Ally_City__c = taStreetCity;
                ea.Trade_Ally_Zip_Code__c = taStreetZipcode; 
                ea.Trade_Ally_Account__c = appt.Trade_Ally_Account__c;
                
                /* From TradAlly DSMTContact */
                Map<String, DSMTracker_Contact__c> mapTAIdToDC = model.mapTAIdToDC;
                if(mapTAIdToDC.containsKey(appt.Trade_Ally_Account__c)){
                    ea.Trade_Ally_Contact_First_Name__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).First_Name__c;
                    ea.Trade_Ally_Contact_Last_Name__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Last_Name__c;
                    ea.Trade_Ally_Contact_DSMT__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Id;
                    ea.Email_Address__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Email__c;
                    ea.Phone_Number__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Phone__c;
                    if(mapTAIdToDC.get(appt.Trade_Ally_Account__c).Contact__c!=null && String.valueOf(mapTAIdToDC.get(appt.Trade_Ally_Account__c).Contact__c)!=''){
                        ea.Trade_Ally_Contact__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Contact__c;     
                    }
                }
                retLst.add(ea);    
            } 
        }
        return retLst;
    }
}