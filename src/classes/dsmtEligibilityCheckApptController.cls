public class dsmtEligibilityCheckApptController
{
    public Eligibility_Check__c custInteraction {get;set;}
    public string customerId {get;set;}
    public List<Customer_Eligibility__c> ecaList{get;set;}
    public integer numberOfApartment{get;set;}
    public List<SelectOption> AppointmentType{get;set;}
    public String SelectedAppointmentType{get;set;}


    public boolean ShowEligibleMessage{get;set;}
    
    public Appointment__c newApp{get;set;}
    
    public List<SelectOption> dsmtContactOption{get;set;}
    public String Selectedcontacts{get;set;}
    Set<Id> dsmtcId = new Set<Id>();

    public dsmtEligibilityCheckApptController()
    {
        init();
        
        newApp = new Appointment__c();
    }
    
    private void init()
    {
        
        numberOfApartment = 1;
        custInteraction = new Eligibility_Check__c();
        GetContactInfo();
        Selectedcontacts = '';
        string dsmtConId = ApexPages.currentPage().getParameters().get('id');
        if(dsmtConId != null && dsmtConId != ''){
            Selectedcontacts = dsmtConId ;
        }
        ecaList = new List<Customer_Eligibility__c>();
        ecaList.add(new Customer_Eligibility__c());
        ShowEligibleMessage = Customer_Eligibility_Config__c.getOrgDefaults().Show_Eligible_Message__c;
        
         AppointmentType = new List<SelectOption>();
        
        
        AppointmentType.add(new SelectOption('', '--Select Type--'));  
        
        Schema.DescribeFieldResult fieldResult = Appointment__c.Appointment_Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
        for( Schema.PicklistEntry f : ple)
        {
          AppointmentType.add(new SelectOption(f.getLabel(), f.getValue()));
        }
    }
    
    public void getApartmentList(){
        Customer_Eligibility__c eca = null;
        ecaList = new list<Customer_Eligibility__c>();
        
        for(Integer i = 0; i < numberOfApartment ; i++){
            eca = new Customer_Eligibility__c();
            ecaList.add(eca);
        }
    }
    
    public void GetContactInfo(){
        dsmtContactOption = new List<SelectOption>(); 
        dsmtContactOption.add(new SelectOption('', '--Select Team Member--'));      
        
        List<User> userList = [select id,contactId from user where id =: userinfo.getUserId()];
        
        if(userList != null && userList.size() > 0 && userList.get(0).ContactId != null){
            
            system.debug('--userList.get(0).ContactId---'+userList.get(0).ContactId);
            
            List<DSMTracker_Contact__c> dsmtconList = [select id,Name,Super_User__c,Trade_Ally_Account__c from DSMTracker_Contact__c where 
                                                            contact__c =: userList.get(0).ContactId];
            
            if(dsmtConList != null && dsmtConList.get(0).Super_User__c){
                
                //IsSuperUser  = true;
                
                dsmtconList = [select id,Super_User__c,Name from DSMTracker_Contact__c where 
                                                            Trade_Ally_Account__c  =: dsmtConList.get(0).Trade_Ally_Account__c and Super_User__c = false];
                for(DSMTracker_Contact__c dsmtc : dsmtConList){
                    dsmtcId.add(dsmtc.Id);
                    dsmtContactOption.add(new SelectOption(dsmtc.Id, dsmtc.Name));
                }
            }else if(dsmtConList != null){
                //IsSuperUser = false;
                dsmtcId.add(dsmtConList.get(0).Id);
                dsmtContactOption.add(new SelectOption(dsmtConList.get(0).Id, dsmtConList.get(0).Name));        
            }
            
        }
    }
    
    @RemoteAction
    public static List<Trade_Ally_Account__c> getContractors(){
        List<Trade_Ally_Account__c> tacList = [select id, Name from Trade_Ally_Account__c];
            
        return tacList;
    }
    
    
    @RemoteAction
    public static string LetsGo(boolean  therm, boolean hes){
        Eligibility_Check__c custInteraction = new Eligibility_Check__c();
        custInteraction.Purchase_a_discounted_smart_thermostat__c = therm;
        custInteraction.No_cost_home_energy_assessment__c = hes;
        //if(Selectedcontacts!= null && Selectedcontacts!='')
            //custinteraction.DSMTracker_Contact__c = Selectedcontacts;
        upsert custInteraction;
        return custInteraction.Id;
    }
    
    public void SaveRecord(){
        custInteraction.Customer__c = customerId;
        if(Selectedcontacts!= null && Selectedcontacts!='')
            custinteraction.DSMTracker_Contact__c = Selectedcontacts;
        upsert custInteraction;
        
        if(ecaList != null && ecaList.size() > 0){
            for(Customer_Eligibility__c ec : ecaList){
                ec.Eligibility_Check__c = custInteraction.Id;
            }
            insert ecaList;
        }
        
    }
    
    
    
    public pageReference saveEligibility()
    {
        try{
             system.debug('--customerId---'+customerId);
            if(customerId != null && customerId != ''){
                custInteraction.Customer__c = customerId;
                List<Customer__c> custList = [select id,First_Name__c,Last_Name__c,Phone__c,Email__c,Service_ZipCode__c from customer__c
                                                    where id =: customerId];

                custInteraction.First_Name__c = custList.get(0).First_Name__c;
                custInteraction.Last_Name__c = custList.get(0).Last_Name__c;
                custInteraction.Email__c = custList.get(0).Email__c;
                custInteraction.Phone__c = custList.get(0).Phone__c;
                custInteraction.Zip__c = custList.get(0).Service_ZipCode__c ;
                
                if(custInteraction.Gas_Account_Holder_First_Name__c != null && custInteraction.Gas_Account_Holder_Last_Name__c != null){
                    DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
                    dsmtc.First_Name__c = custInteraction.Gas_Account_Holder_First_Name__c;
                    dsmtc.Last_Name__c = custInteraction.Gas_Account_Holder_Last_Name__c;
                    dsmtc.customer__c = customerId;
                    insert dsmtc;  
                }
            }
            upsert custInteraction;
            
            if(NewApp != null){
                newApp.Eligibility_Check__c = custInteraction.Id;
                //newApp.DSMTracker_Contact__c = apexpages.currentpage().getparameters().get('id');
                newApp.Customer_reference__c = customerId;
                newApp.DSMTracker_Contact__c = Selectedcontacts;
                newApp.Appointment_Status__c = 'Scheduled';
                newApp.Appointment_Type__c = SelectedAppointmentType;
                insert newApp;
            }
            if(ecaList != null && ecaList.size() > 0){
                
                for(Customer_Eligibility__c ec : ecaList){
                    ec.Eligibility_Check__c = custInteraction.Id;
                }
                upsert ecaList;
            }
            return null;//new Pagereference('/apex/ucsProspector');
        }catch(Exception ex){
            return null;
        }
    }
    
    @RemoteAction
    public static Customer__c getContactValues(string custId){
        
        system.debug('--custId---'+custId);
        
        List<Customer__c > custList = [select First_Name__c,Last_Name__c from Customer__c where Id =: custId];
                                                
        
        if(custList.size() > 0)
            return custList[0];
        
        return new Customer__c ();
    }
    
    @RemoteAction
    public static Customer__c CheckCustomer(String fName,String lName,String Email,String Zip,String Phn){
        
        
        List<Customer__c> custList = [select id from customer__c where First_Name__c =: fName and Last_Name__c =: lname
                                                 and Service_Zipcode__c =: zip
                                                order by CreatedDate ASC];
        
        if(custList != null && custList.size() > 0){
            return custList.get(0);
        }else{
            Customer__c cust = new Customer__c();
            cust.First_Name__c = fName ;
            cust.Last_Name__c = lName;
            cust.Email__c = Email;
            cust.Phone__c = Phn;
            cust.Service_ZipCode__c = Zip ;
            cust.Created_From_Online_Portal__c = true;
            insert cust;
            return cust;
        }
        return new customer__c();
    }
    
    @RemoteAction
    public static string checkZipEligibility(String zipCode,String fuel,String unit,String condo,boolean IsWifi){
        fuel = (fuel == 'Yes' ? 'Gas' : 'ELE');
        return EligibilityCheckUtility.checkZipEligibility(zipCode, fuel, unit, condo,IsWifi);
    }
    
    @RemoteAction
    public static GECheckWrapper checkGEUtility(string pcode){
        GECheckWrapper GE = new GECheckWrapper();
        GE.isGasChecked = EligibilityCheckUtility.checkGasutility(pcode);
        GE.isElectricChecked = EligibilityCheckUtility.checkElectricutility(pcode);
        GE.isLandloard = EligibilityCheckUtility.checkLandloardEligibility(pcode);
        return GE;
    }
    
    public class GECheckWrapper{
        public boolean isGasChecked;
        public boolean isElectricChecked;
        public boolean isLandloard;
    }
}