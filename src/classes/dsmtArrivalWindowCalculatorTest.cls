@istest
public class dsmtArrivalWindowCalculatorTest
{
	@istest
    static void runtest()
    {

        
        string timeString='0:0';
       	datetime d = DateTime.newInstance(2018, 1, 1, 01, 17, 0);
        system.debug('d: '+ d);
        
        date dt= date.newInstance(2018, 1, 1);
        
        string inputDateString='2018-01-01';
        string inputTimeString='1:1';
       
        /*System_Config__c.getOrgDefaults().Arrival_Window_min_before_Appointment__c=0;
        System_Config__c.getOrgDefaults().Arrival_Window_min_after_Appointment__c=0;*/
       
        
        System_Config__c config = new System_Config__c();
        config.Arrival_Window_min_after_Appointment__c = 30;
        config.Arrival_Window_min_before_Appointment__c = 10;
        insert config;
        
        dsmtArrivalWindowCalculator.calculateArrivalWindow(timeString);
        dsmtArrivalWindowCalculator.calculateArrivalWindow(d);
        dsmtArrivalWindowCalculator.buildTimeslotDescription(inputDateString, inputTimeString);
        
    }
}