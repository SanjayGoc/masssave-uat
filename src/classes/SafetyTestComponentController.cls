public class SafetyTestComponentController {
    public Safety_Test__c newSafetyTest{get;set;}
    public String safetyTestFieldSetName{get;set;}
    public List<Safety_Test__c> combustionDeviceTestsList{get;set;}
    public static final Id COMBUSTION_DEVICE_TEST_RECORD_TYPE_ID = Schema.SObjectType.Safety_Test__c.getRecordTypeInfosByName().get('Combustion Test Device').getRecordTypeId();
    
    
}