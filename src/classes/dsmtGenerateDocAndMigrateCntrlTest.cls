@isTest(seeAllData=true)
public class dsmtGenerateDocAndMigrateCntrlTest{
    public testmethod static void test1(){
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea=[select id,name from Energy_Assessment__c where Status__c='Assessment Complete' order by LastmodifiedDate desc limit 1];
        //ea.Trade_Ally_Account__c = ta.Id;
        //ea.Dsmtracker_contact__c = dsmt.Id;
        //insert ea;
        Barrier__c proj1 = new Barrier__c();
        proj1.Energy_Assessment__c = ea.Id;
        insert proj1;
        
        Recommendation_Scenario__c prj = new Recommendation_Scenario__c();
        prj.Energy_Assessment__c = ea.Id;
        insert prj;
        
        
        Attachment__c attach=new Attachment__c(); 
        attach.Attachment_Name__c='Unit Test Attachment'; 
        attach.Attachment_Type__c = 'Customer Signature';
        attach.Barrier__c = proj1.Id;
        insert attach;
        
        Attachment attach2=new Attachment();     
        attach2.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach2.body=bodyBlob;
        attach2.parentId=attach.id;
        insert attach2;
        
        Attachment__c attach1=new Attachment__c(); 
        attach1.Attachment_Name__c='Unit Test Attachment'; 
        attach1.Attachment_Type__c = 'Customer Signature';
        attach1.Project__c= prj.Id;
        insert attach1;
        
        string obj = 'Barrier__c';
        String TemplateId= 'test';
        String QueryId = 'test';
        String pv0 = 'test';
        String previewStr = 'false';
        ApexPages.currentPage().getParameters().put('Id',proj1.id);
        ApexPages.currentPage().getParameters().put('QueryId',QueryId);
        ApexPages.currentPage().getParameters().put('TemplateId',TemplateId);
        ApexPages.currentPage().getParameters().put('pv0',pv0);
        ApexPages.currentPage().getParameters().put('MasterId',attach.id);
        ApexPages.currentPage().getParameters().put('ObjectType ',obj);
        ApexPages.currentPage().getParameters().put('ispreview',previewStr);
        dsmtGenerateDocAndMigrateCntrl cont = new dsmtGenerateDocAndMigrateCntrl();
        cont.checkCongaStatus();
        
        attach1.Status__c = 'Completed';
        update attach1;
        attach.Status__c = 'Completed';
        update attach;
        cont.checkCongaStatus();
        cont.redirectToCompleted();
    }
}