/**
 * <h1>Code Change History</h1>
 * 
 *  2014-10-28 - greenteamsoftware.com
 *               Initial creation
 * 
 * 
 */


/**
 * <h1>MailTemplatePageControllerTest </h1>
 * Test class of MailTemplatePageController
 * 
 * @author  greenteamsoftware.com
 * @version 1.0
 * @since   2014-10-28
 */

 
@isTest
public class dsmtMailTemplatePageControllerTest{
    @isTest
    private static void test(){
       
       Account acc = DAtagenerator.createAccount();
       insert acc;
       
       Contact con = new contact();
       con.AccountId = acc.Id;
       con.LastName = 'Do Not Delete';
       insert con;
       
       Registration_Request__c reg = DAtagenerator.createRegistration();
       
       Attachment__c att = new Attachment__c(Registration_Request__c =reg.id);
        insert att;
        
        ApexPages.Currentpage().getParameters().put('objId',reg.Id);
       dsmtMailTemplatePageController cntrl = new dsmtMailTemplatePageController();
       cntrl.cancel();
       cntrl.ReloadPage();
       cntrl.selTemplateId = '00X4D000000QGdK';
        cntrl.subject = 'subject';
        cntrl.toAddress = 'test@test.com';
        cntrl.toAddressName = 'Test';
        cntrl.ccAddress = 'tsset@test.com';
        cntrl.bccAddress = 'tsset3@test.com';
        cntrl.isMIR = true;
        cntrl.sendEmail();
        cntrl.fillHtmlBody();
        cntrl.addMessage('');
        cntrl.call();
        
    }
}