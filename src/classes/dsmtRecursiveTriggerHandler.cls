public class dsmtRecursiveTriggerHandler{
    public static Boolean isFirstTimeRecommendation_Scenario = true;
    public static Boolean isFirstEnergy_Assessment= true;
    public static Boolean isFirstTimeWorkorder = true;
    public static Boolean isFirstTime = true;
    public static Boolean StopCustomerEligibilityUpdateOnWoTrigger = false;
    public static Boolean isFirstEnergyAssessment = true;
    public static Set<Id> updatedProposalIds = new Set<Id>();
    //DSST-6924: Changes made by DeveloperName – Begin
    public static Boolean preventFloorUpdateAfterInsert = false;
    public static Boolean preventCeilingUpdateAfterInsert = false;
    public static Boolean preventWallUpdateAfterInsert = false;
    //DSST-6924: Changes made by DeveloperName – End
    
    //Added by Prasanjeet
    //Too many soql when editing Attic - 19/08/2018
    public static boolean preventAtticVentingRecursion = false;
    public static boolean preventThermalEnvelopeRecursion = false;
    
    
    //Added by Prasanjeet
    //Too many soql - DSST-8810 - 30/08/2018
    public static boolean preventEligiblityCheckTrigger = false;
    
    public static boolean preventAppointmentTrigger = false;
    public static boolean preventApplianceTrigger = false;
    public static boolean preventAirFlowAndAirLeakageTrigger = false;
}