@istest(seeAllData=true)
public class dsmtAddBillingAdjustmentControllerTest
{
	@istest
    static void runtest()
    {
        Building_Specification__c bs =new Building_Specification__c ();
        bs.Floor_Area__c=1800;
        //bs.Previous_Floor_Area__c=5;
        //bs.Appointment__c=app.id;
        bs.Year_Built__c='1950';
        bs.Bedromm__c=3;
        bs.Occupants__c=3;
        bs.Natural_Gas__c='Gas';
        bs.Orientation__c='South';
        bs.Ceiling_Heights__c=8.00;
        bs.Floors__c=1;
        //bs.Floor_Area__c=1800.00;
        bs.Finished_Basement_Area__c=0;
        bs.Total_Floor_Area__c=1800;
        bs.Total_Volume__c=14400;
        
        insert bs;
        
        WeatherStation__c ws = new WeatherStation__c();
        ws.CDLatentMult__c=3.08;
        ws.CZ1_2__c=0;
        ws.CDMult__c=1.062;         
        ws.NormalNFactor__c=15.30;
        ws.CDMultPCFM__c=0.7155161;
        ws.ClgGrDiff__c=26.00;
        ws.CExposeMult__c=1.885;
        ws.Clg1PctDesignTempWetBulb__c=71.70;
        ws.CHeightExp__c=0.319;
        ws.CNFactor__c=34.83;
        ws.Htg99PctDesignTemp__c=12.30;
        ws.CSolarH__c=109461.00;
        ws.Elevation__c=16.40;
        
        ws.LightSh__c=0.90000;
        ws.HDMult__c=1.168;
        ws.LightC__c=0.8700;
        ws.HDMultPCFM__c=1.088053;
        ws.LightH__c=1.06000;
        ws.HExposeMult__c=1.485;
        ws.HHeightExp__c=0.417;
        
        ws.LightAverage__c=3.3300000;
        ws.HNFactor__c=22.47;
        ws.PostalCode__c='1460';
        ws.WeatherStationName__c='MABOSTON';
        ws.HSolarH__c=206276;
        ws.TGround__c  =51.0000;
        ws.Latitude__c= 42.3700;
        ws.TMYType__c    = 'TMY2';
        
        insert ws;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        
        ea.WeatherStation__c=ws.id;
        ea.Building_Specification__c=bs.id;
        
        
        ea.BelowGradeSpaceTempH__c = 23;
        ea.BelowGradeSpaceTempC__c = 24;
        ea.ExteriorSpaceTempH60__c = 45;
        ea.ExteriorSpaceTempC74__c = 46;
        ea.LivingSpaceTempH__c = 30;
        ea.LivingSpaceTempC__c = 31;
        
        insert ea; 
        
        Review__c rv =new Review__c();
        insert rv;
        ApexPages.currentPage().getParameters().put('id',rv.id);
        
        
        Recommendation_Scenario__c rcs =new Recommendation_Scenario__c ();
        insert rcs;
        
        Project_Review__c pr =new Project_Review__c();
        pr.Project__c=rcs.Id;
        pr.Review__c=rv.Id;
        insert pr;
        
        Recommendation__c rc =new Recommendation__c();
        rc.Recommendation_Scenario__c=rcs.Id;
        rc.Energy_Assessment__c=ea.id;
        insert rc;

        Change_Order_Line_Item__c col =new Change_Order_Line_Item__c();
        insert col;
        
        dsmtAddBillingAdjustmentController daac = new dsmtAddBillingAdjustmentController();  
        daac.initJSON();
        daac.createReviewRec();
        daac.reviewId=rv.Id;
        daac.dataSave='test;test';
        daac.CreateChagneOrderItems();
        
        //daac.init();
    }
}