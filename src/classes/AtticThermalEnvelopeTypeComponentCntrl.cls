public class AtticThermalEnvelopeTypeComponentCntrl {
    public Id thermalEnvelopeTypeId{get;set;}
    public Attic_Space_Work_Order_Data__c aswod{get;set;}
    public Attic_Venting__c atticVenting{get;set;}
    public String fieldUniqueDataId{get;set;}
    public Integer atticAWHFIndex{get;set;}
    public String visiblePanel{get;set;}
    
    public AtticThermalEnvelopeTypeComponentCntrl(){
        System.debug('thermalEnvelopeTypeId :: ' + thermalEnvelopeTypeId);
    } 
    
    public void showVisiblePanel(){
        System.debug('visiblePanel : ' + visiblePanel);
        newAtticWall = null;
    }
    public void saveAtticVisiblePanel(){
        
    }
    public void saveWorkorderData(){
        aswod = aswod.clone(true);
        UPDATE aswod;
    }
    public void saveAtticVentingData(){
        atticVenting = atticVenting.clone(true);
        UPDATE atticVenting;
        saveAtticVentSets();
    }
    
    public List<Attic_Access_and_Whole_House_Fan__c> atticAWHFs{get{
        if(atticAWHFs == null){
            queryAtticAWHFs();
        }
        return atticAWHFs;
    }set;}
    public void queryAtticAWHFs(){
        System.debug('--queryAtticAWHFs :: thermalEnvelopeTypeId ::' + thermalEnvelopeTypeId);
        atticAWHFs = new List<Attic_Access_and_Whole_House_Fan__c>();
        if(thermalEnvelopeTypeId != null){
            String atticId = thermalEnvelopeTypeId;
            String SOQL = 'SELECT '+getSObjectFields('Attic_Access_and_Whole_House_Fan__c') + ' FROM Attic_Access_and_Whole_House_Fan__c WHERE Thermal_Envelope_Type__c =:atticId';        
            System.debug('@@@ SOQL : ' + SOQL);
            atticAWHFs = Database.query(SOQL);
        }
    }
    public void addNewAtticAWHF(){
        if(thermalEnvelopeTypeId != null){
            saveAtticAWHFs();
            Attic_Access_and_Whole_House_Fan__c newAtticAWHF = new Attic_Access_and_Whole_House_Fan__c(
                Thermal_Envelope_Type__c = thermalEnvelopeTypeId,
                Name = 'Access and Whole House Fan ' + (atticAWHFs.size() + 1),
                Access_Type__c = 'Ceiling Hatch',
                Location__c = 'Living Space'
            );
            INSERT newAtticAWHF;
            queryAtticAWHFs();
        }
    }
    public void deleteAtticAWHF(){
        String AtticAWHFIndexStr = ApexPages.currentPage().getParameters().get('AtticAWHFIndex');
        if(AtticAWHFIndexStr != null){
            saveAtticAWHFs();
            Integer AtticAWHFIndex = Integer.valueOf(AtticAWHFIndexStr);
            DELETE new Attic_Access_and_Whole_House_Fan__c(Id = atticAWHFs.get(AtticAWHFIndex).Id);
            atticAWHFs.remove(AtticAWHFIndex);
        }
    }
    public void saveAtticAWHFs(){
        if(atticAWHFs != null && atticAWHFs.size() > 0){
            UPSERT atticAWHFs;
            queryAtticAWHFs();
        }
    }
    
    public List<Attic_Vent_Set__c> atticVentSets{get{
        if(atticVentSets == null){
            queryAtticVentingSets();
        }
        return atticVentSets;
    }set;}
    public void saveAtticVentSets(){
        if(atticVentSets.size() > 0){
            UPSERT atticVentSets;
            queryAtticVentingSets();
        }
    }
    public void addNewAtticVentSet(){
        saveAtticVentSets();
        if(atticVenting != null && atticVenting.Id != null){
            Attic_Vent_Set__c newAtticVentSet = new Attic_Vent_Set__c(
                Attic_Venting__c = atticVenting.Id,
                Name = 'Attic Vent Set ' + (atticVentSets.size() + 1)
            );
            INSERT newAtticVentSet;
            if(atticVentSets == null){
                atticVentSets = new List<Attic_Vent_Set__c>();
            }
            atticVentSets.add(newAtticVentSet);
        }
    }
    public void deleteAtticVentingSet(){
        saveAtticVentSets();
        String atticVentSetIndexStr = ApexPages.currentPage().getParameters().get('atticVentSetIndex');
        if(atticVentSetIndexStr != null && atticVentSetIndexStr.trim() != ''){
            Attic_Vent_Set__c atticVentSet = atticVentSets.get(Integer.valueOf(atticVentSetIndexStr));
            if(atticVentSet != null){
                DELETE new Attic_Vent_Set__c(Id = atticVentSet.Id);
                atticVentSets.remove(Integer.valueOf(atticVentSetIndexStr));
            }
        }
    }
    public void queryAtticVentingSets(){
        atticVentSets = new List<Attic_Vent_Set__c>();
        if(this.atticVenting != null && this.atticVenting.Id != null){
            Id atticVentingId = atticVenting.Id;
            atticVentSets = Database.query('SELECT '+getSObjectFields('Attic_Vent_Set__c') + ' FROM Attic_Vent_Set__c WHERE Attic_Venting__c=:atticVentingId');
        }
    }    
    
    public Ceiling__c newAtticRoof{get{
        if(newAtticRoof == null){
            newAtticRoof = new Ceiling__c();
        }
        return newAtticRoof;
    }set;}
    public Wall__c newAtticWall{get{
        if(newAtticWall == null){
            newAtticWall = new Wall__c();
        }
        return newAtticWall;
    }set;}
    public Floor__c newAtticFloor{get{
        if(newAtticFloor == null){
            newAtticFloor = new Floor__c();
        }
        return newAtticFloor;
    }set;}
    
    public void addNewAtticRoof(){
        System.debug('@@...addNewAtticRoof...');
        try{
            if(newAtticRoof != null && thermalEnvelopeTypeId != null){
                Thermal_Envelope_Type__c tet = [SELECT Id,Name,Next_Attic_Roof_Number__c FROM Thermal_Envelope_Type__c WHERE Id=:thermalEnvelopeTypeId LIMIT 1];
                Decimal nextNumber = (tet.Next_Attic_Roof_Number__c != null) ? tet.Next_Attic_Roof_Number__c : 1;
                newAtticRoof = newAtticRoof.clone(false);
                newAtticRoof.Thermal_Envelope_Type__c = thermalEnvelopeTypeId;
                newAtticRoof.Name = 'Attic Roof ' + nextNumber;
                newAtticRoof.Exposed_To__c = newAtticRoof.Roof_To__c;
                
                INSERT newAtticRoof;
                if(newAtticRoof.Id != null){
                    tet.Next_Attic_Roof_Number__c = nextNumber + 1;
                    UPDATE tet;
                }
                newAtticRoof = null;
            }
        }catch(Exception ex){
            System.debug('@@ EXCEPTION :: ' + ex.getMessage());
        }
    }
    public void editAtticWall(){
        String wallId = ApexPages.currentPage().getParameters().get('wallId');
        if(wallId != null && wallId.trim() != ''){
            List<Wall__c> walls = Database.query('SELECT ' + getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Id=:wallId');
            if(walls.size() > 0){
                newAtticWall = walls.get(0);
                visiblePanel = 'AtticWallComponentPanel';
            }
        }
    }
    public void addNewAtticWall(){
        System.debug('@@...addNewArricWall...');
        try{
            if(newAtticWall != null && thermalEnvelopeTypeId != null){
                Thermal_Envelope_Type__c tet = [SELECT Id,Name,Next_Attic_Wall_Number__c FROM Thermal_Envelope_Type__c WHERE Id=:thermalEnvelopeTypeId LIMIT 1];
                Decimal nextNumber = (tet.Next_Attic_Wall_Number__c != null) ? tet.Next_Attic_Wall_Number__c : 1;
                
                newAtticWall = newAtticWall.clone(false);
                newAtticWall.Thermal_Envelope_Type__c = thermalEnvelopeTypeId;
                newAtticWall.Name = 'Attic Wall ' + nextNumber;
                newAtticWall.Exposed_To__c = newAtticWall.Wall_To__c;
                
                INSERT newAtticWall;
                if(newAtticWall.Id != null){
                    tet.Next_Attic_Wall_Number__c = nextNumber + 1;
                    UPDATE tet;
                }
                //newAtticWall = null;
            }
        }catch(Exception ex){
            System.debug('@@ EXCEPTION :: ' + ex.getMessage());
        }
    }
    public void addNewAtticFloor(){
        System.debug('@@...addNewAtticFloor...');
        try{
            if(newAtticFloor != null && thermalEnvelopeTypeId != null){
                Thermal_Envelope_Type__c tet = [SELECT Id,Name,Next_Attic_Floor_Number__c FROM Thermal_Envelope_Type__c WHERE Id=:thermalEnvelopeTypeId LIMIT 1];
                Decimal nextNumber = (tet.Next_Attic_Floor_Number__c != null) ? tet.Next_Attic_Floor_Number__c : 1;
                
                newAtticFloor = newAtticFloor.clone(false);
                newAtticFloor.Thermal_Envelope_Type__c = thermalEnvelopeTypeId;
                newAtticFloor.Name = 'Attic Floor ' + nextNumber;
                newAtticFloor.Exposed_To__c = newAtticFloor.Floor_To__c;
                
                INSERT newAtticFloor;
                if(newAtticFloor.Id != null){
                    tet.Next_Attic_Floor_Number__c = nextNumber + 1;
                    UPDATE tet;
                }
                newAtticFloor = null;
            }
        }catch(Exception ex){
            System.debug('@@ EXCEPTION :: ' + ex.getMessage());
        }
    }
    
    public void deleteRoof(){
        String roofId = ApexPages.currentPage().getParameters().get('roofId');
        if(roofId != null){
            DELETE new Ceiling__c(Id = roofId);
        }
    }
    public void deleteWall(){
        String wallId = ApexPages.currentPage().getParameters().get('wallId');
        if(wallId != null){
            DELETE new Wall__c(Id = wallId);
        }
    }
    public void deleteFloor(){
        String floorId = ApexPages.currentPage().getParameters().get('floorId');
        if(floorId != null){
            DELETE new Floor__c(Id = floorId);
        }
    }
    
    public List<Ceiling__c> getAtticRoofs(){
        System.debug('thermalEnvelopeTypeId :: ' + thermalEnvelopeTypeId);
        if(thermalEnvelopeTypeId != null){
            return Database.query('SELECT '+ getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            return new List<Ceiling__c>();
        }
    }
    
    transient List<Wall__c> AtticWalls;
    public List<Wall__c> getAtticWalls(){
        System.debug('thermalEnvelopeTypeId :: ' + thermalEnvelopeTypeId);
        if(thermalEnvelopeTypeId != null){
            AtticWalls = Database.query('SELECT '+ getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            AtticWalls = new List<Wall__c>();
        }
        return AtticWalls;
    }
    public List<Floor__c> getAtticFloors(){
        System.debug('thermalEnvelopeTypeId :: ' + thermalEnvelopeTypeId);
        if(thermalEnvelopeTypeId != null){
            return Database.query('SELECT '+ getSObjectFields('Floor__c') + ' FROM Floor__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            return new List<Floor__c>();
        }
    }
    
    
    
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}