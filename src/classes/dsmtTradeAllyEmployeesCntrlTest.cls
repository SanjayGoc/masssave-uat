@isTest
public class dsmtTradeAllyEmployeesCntrlTest {
    @isTest
    Public Static Void RunTest(){
        
        test.startTest();
        
        User u = Datagenerator.CreatePortalUser();
        
        system.runAs(u) // normally you want to verify the list has data first as this could cause null exception
        {
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            insert ta;
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.First_Name__c='Test FN';
            dsmtc.Last_Name__c='Test LN';
            dsmtc.Email__c='test@pliant.com';
            dsmtc.Phone__c='9638527410';
            dsmtc.Title__c='Crew Chief';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c= u.ContactId;
            insert dsmtc;
            
            dsmtTradeAllyEmployeesCntrl dsmtEMP = new dsmtTradeAllyEmployeesCntrl();
            dsmtEMP.loadEmployeesForManager();
        }
         
        test.stopTest();
        
    }
}