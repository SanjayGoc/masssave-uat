public class AppointmentTriggerHandler
{
    
    public static void updateWorkorderCancelled(list<Appointment__c> newlist, map<id, Appointment__c> oldmap)
    {
        Map<id,String> woIdSet = new Map<id,String>();
        for(Appointment__c a : newList){
            if(a.Workorder__c != null && 
              ((oldmap == null &&  (a.Appointment_Status__c == 'Cancelled' || a.Appointment_Status__c  == 'Rescheduled')) || 
              (oldmap != null &&  (a.Appointment_Status__c == 'Cancelled' || a.Appointment_Status__c  == 'Rescheduled') && a.Appointment_Status__c != oldmap.get(a.Id).Appointment_Status__c)))
            {
                woIdSet.put(a.Workorder__c,a.Appointment_Status__c );
            }
        }
        
        list<Workorder__c> woUpdateList = new list<Workorder__c>();
        
        List<Workorder__c> woList = [select id,Status__c from workorder__c where id in : woIdSet.keyset()];
        
        if(woList.size() > 0)
        {
            for(Workorder__c wo : woList )
            {
                
                system.debug('--woIdSet.get(wo.Id)---'+woIdSet.get(wo.Id));
                system.debug('--wo.Status__c---'+wo.Status__c);
                
                if(wo.Status__c  != woIdSet.get(wo.Id)){
                    woUpdateList.add(new Workorder__c(id = wo.Id,
                                                  Status__c = woIdSet.get(wo.Id)));
                }
            }
            
            if(woUpdateList.size() > 0){
            
                update woUpdateList;
            }
        }
    }
}