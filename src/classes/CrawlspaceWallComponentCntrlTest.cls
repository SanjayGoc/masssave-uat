@istest
public class CrawlspaceWallComponentCntrlTest
{
    @istest
    static void runtest()
    {
        Wall__c wall = new Wall__c();
        insert wall;
        
        Layer__c ly = new Layer__c();
        insert ly;
        
        CrawlspaceWallComponentCntrl cwc = new CrawlspaceWallComponentCntrl();
        cwc.crawlspaceWallId =wall.Id;
        cwc.getWallLayersMap();
        
        cwc.saveWall();
        ApexPages.currentPage().getParameters().put('layerType','Flooring');
        //cwc.addNewWallLayer();
    }
}