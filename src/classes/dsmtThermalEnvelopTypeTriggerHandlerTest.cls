@isTest
private class dsmtThermalEnvelopTypeTriggerHandlerTest {
	
	@isTest static void test_method_one() 
	{
		Test.startTest();

		dsmtEAModel.setupAssessment();

		Test.stopTest();
	}

	@isTest static void test1() 
	{
		Energy_Assessment__c ea =	dsmtEAModel.setupAssessment();
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		Test.startTest();

		dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		update bp.telist;

		Test.stopTest();
	}

	@isTest static void test2() 
	{
		Energy_Assessment__c ea = dsmtEAModel.setupAssessment();

		List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:ea.Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        Set<Id> eaId = new Set<Id>();
        eaId.add(ea.Id);

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = ea.Id;
            insert te;        
        }

		Test.startTest();

		List<Thermal_Envelope_Type__c> tetList = new List<Thermal_Envelope_Type__c>();


		Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Exterior Wall').getRecordTypeId();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tetList.add(tet);

		tet = new Thermal_Envelope_Type__c();
		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Door').getRecordTypeId();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tetList.add(tet);

		tet = new Thermal_Envelope_Type__c();
		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Slab').getRecordTypeId();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tetList.add(tet);

		tet = new Thermal_Envelope_Type__c();
		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Crawlspace').getRecordTypeId();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tetList.add(tet);

		tet = new Thermal_Envelope_Type__c();
		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Basement').getRecordTypeId();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tetList.add(tet);

		tet = new Thermal_Envelope_Type__c();
		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Attic').getRecordTypeId();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tetList.add(tet);

		tet = new Thermal_Envelope_Type__c();
		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Windows & Skylights').getRecordTypeId();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tetList.add(tet);

		insert tetList;

		//string soql = 'SELECT ' + dsmtHelperClass.sObjectFields('Thermal_Envelope_Type__c') + ' Id FROM Thermal_Envelope_Type__c';
		//tetList = database.query(soql);

		
		dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
		//system.debug('bp.telist.count>>>'+bp.telist.size());
		//system.debug('telist.count>>>'+tetList.size());

		//dsmtThermalEnvelopTypeTriggerHandler.CreateDefaultLayerAndChild(bp.telist);
		dsmtThermalEnvelopTypeTriggerHandler.UpdateThermostateForThermalLoad(bp);
		dsmtThermalEnvelopTypeTriggerHandler.deleteLayers(null);

		For(Thermal_Envelope_Type__c t : bp.telist)
		{
			//system.debug('bp.recordTypeMap.get(t.RecordTypeId)>>>'+bp.recordTypeMap.get(t.RecordTypeId));

			if(bp.recordTypeMap.get(t.RecordTypeId) =='Basement')
			{
				bp.teType = t;
				dsmtThermalEnvelopTypeTriggerHandler.UpdateFoundationRecord(bp,false);
			}

			if(bp.recordTypeMap.get(t.RecordTypeId) =='Crawlspace')
			{
				bp.teType = t;
				dsmtThermalEnvelopTypeTriggerHandler.UpdateFoundationRecord(bp,false);
			}

			if(bp.recordTypeMap.get(t.RecordTypeId) =='Exterior Wall')
			{
				bp.teType = t;
				dsmtThermalEnvelopTypeTriggerHandler.UpdateWallForRecommendation(bp);
			}

			if(bp.recordTypeMap.get(t.RecordTypeId) =='Slab')
			{
				bp.teType = t;
				dsmtThermalEnvelopTypeTriggerHandler.UpdateFloorForRecommendation(bp);
				dsmtThermalEnvelopTypeTriggerHandler.UpdateCeilingForRecommendation(bp);
			}
		}

		/*bp.tetype = 
		dsmtThermalEnvelopTypeTriggerHandler.UpdateFloorForRecommendation(bp);
		dsmtThermalEnvelopTypeTriggerHandler.UpdateWallForRecommendation(bp);
		dsmtThermalEnvelopTypeTriggerHandler.UpdateCeilingForRecommendation(bp);
		dsmtThermalEnvelopTypeTriggerHandler.UpdateThermostateForThermalLoad(bp);*/
		

		Test.stopTest();
	}


	
}