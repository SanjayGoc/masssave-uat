public class dsmtIncentiveHelper
{
    public static decimal MAXIMUM_BERKSHIRE_HOURS = 12;
    public static decimal MAX_INSULATION_CAP = 2000;
    public static decimal MAX_DUCT_INSULATION_CAP = 1000;
    
    /*
    //// 100 % of installed cost but in case of berkshire gas only 12 hours of recommendation allowed
    public static decimal ComputeAirFlowIncentiveCommon(dsmtEAModel.Surface bp)
    {
        decimal result =0;
        Try
        {
            string gasProvider = '';
            boolean isBerkShire=false;
            
            if(bp.eaObj !=null)
            {
                gasProvider  = bp.eaObj.Primary_Provider__c;
            }
            
            if(gasProvider==null)gasProvider='';

            if(gasProvider.containsIgnoreCase('Berkshire')) isBerkshire = true;
            
            decimal currentCost = bp.rcmd.Cost__c;
            result = currentCost;
            
            if(isBerkShire)
            {
                decimal eligibleHours = bp.rcmd.Hours__c > 12 ? 12 : bp.rcmd.Hours__c;
                decimal eligibleCost =  eligibleHours * bp.rcmd.Unit_Cost__c;
                
                result = eligibleCost ;
            }
        }
        Catch(Exception ex)
        {
            WriteMessage(ex);         
        }
        
        return result;
    }
    
    public static decimal ComputeInsulationIncentive(dsmtEAModel.Surface bp)
    {
        decimal result =0;
        Try
        {
            decimal currentCost = bp.rcmd.Cost__c;
            if(CurrentCost ==null) currentCost =0;
            
            if(currentCost >0)
            {
                result = currentCost * 0.75;
                
                //if(result > MAX_INSULATION_CAP ) result = MAX_INSULATION_CAP ;
            }
           
        }
        Catch(Exception ex)
        {
            WriteMessage(ex);         
        }
        
        return result;            
    }*/

    public static decimal CalculateIncentive(dsmtEAModel.Surface bp, string category)
    {
        decimal result =0;
        
        if(category =='Airflow')
        {
            /// For Chagne Order Ticket Get to know working with Kavita and prasanjeet DSST-14768
            // comment out this method and use the generic method
            //result = ComputeAirFlowIncentiveCommon(bp);
            result = ComputeOtherIncentive(bp);
        }
        else if(category=='Insulation')
        {
            /// DSST-15099
            /// Change method to ComputeOtherIncentive from ComputeInsulationIncentive(bp);
            /// Rohit Sharma
            result = ComputeOtherIncentive(bp);
        }
        else if(category=='Duct Measures')
        {
            result = ComputeDuctInsulationIncentive(bp);
        }
        else if(category=='Thermostat')
        {
            result = ComputeThermostatIncentive(bp);
        }
        else if(category=='Direct Install')
        {
            result = ComputeISMIncentive(bp);
        }
        else if(category =='Refrigerator' || category == 'Washing Machine')
        {
            result = ComputeRefrigeratorIncentive(bp);
        }
        else if(category =='Ventilation')
        {
            result = ComputeVentilationIncentive(bp);
        }
        else {
            result = ComputeOtherIncentive(bp);
        }

        return result;
    }

    private static decimal ComputeRefrigeratorIncentive(dsmtEAModel.Surface bp)
    {
         return GetIncentiveFromProduct(bp);
    }

    public static decimal ComputeVentilationIncentive(dsmtEAModel.Surface bp)
    {
         decimal result =0;
        Try
        {
            decimal currentCost = bp.rcmd.Cost__c;
            if(CurrentCost ==null) currentCost =0;

            result = dsmtEAModel.ISNULL(GetIncentiveFromProduct(bp));

            if(currentCost > 0 && result ==0)
            {
                result = currentCost * 0.75;                
                //if(result > MAX_DUCT_INSULATION_CAP ) result = MAX_DUCT_INSULATION_CAP ;
            }
           
        }
        Catch(Exception ex)
        {
            WriteMessage(ex);         
        }
        
        return result;     
    }

    public static decimal ComputeDuctInsulationIncentive(dsmtEAModel.Surface bp)
    {
         decimal result =0;
        Try
        {
            decimal currentCost = bp.rcmd.Cost__c;
            if(CurrentCost ==null) currentCost =0;
            
            if(currentCost >0)
            {
                if(bp.product !=null)
                {
                    string primaryIncentiveString = bp.product.Primary_Incentive__c;
                    if(primaryIncentiveString==null) primaryIncentiveString='';

                    decimal primaryIncentive=0;

                    string incentiveType = 'percent';                    
                    if(primaryIncentiveString.contains('%'))
                    {
                        primaryIncentiveString = primaryIncentiveString.replace('%','');
                    }
                    else if(primaryIncentiveString.contains('$'))
                    {
                        incentiveType = 'dollar';
                        primaryIncentiveString = primaryIncentiveString.replace('$','');
                    }

                    try 
                    {
                        primaryIncentive = decimal.valueOf(primaryIncentiveString.trim());
                    } 
                    catch(Exception ex) 
                    {
                        dsmtHelperClass.WriteMessage(ex);
                    }

                    if(incentiveType=='percent')
                    {
                        result = currentCost * (primaryIncentive * .01);
                    }
                    else {
                        result = primaryIncentive;
                    }

                }
                else 
                {
                    result = currentCost * 1;    
                    if(result > MAX_DUCT_INSULATION_CAP ) result = MAX_DUCT_INSULATION_CAP ;
                }
            }
           
        }
        Catch(Exception ex)
        {
            WriteMessage(ex);         
        }
        
        return result;     
    }

    public static decimal ComputeThermostatIncentive(dsmtEAModel.Surface bp)
    {
         decimal result =0;
        Try
        {
            decimal currentCost = bp.rcmd.Cost__c;
            if(CurrentCost ==null) currentCost =0;
            
            if(currentCost >0)
            {
                result = currentCost * 1;                
            }
           
        }
        Catch(Exception ex)
        {
            WriteMessage(ex);         
        }
        
        return result;     
    }

    public static decimal ComputeISMIncentive(dsmtEAModel.Surface bp)
    {
         decimal result =0;
        Try
        {
            decimal currentCost = bp.rcmd.Cost__c;
            if(CurrentCost ==null) currentCost =0;
            
            result = dsmtEAModel.ISNULL(GetIncentiveFromProduct(bp));

            if(result ==0 && currentCost >0)
            {
                result = currentCost * 1;                
            }
           
        }
        Catch(Exception ex)
        {
            WriteMessage(ex);         
        }
        
        return result;     
    }

    public static decimal ComputeOtherIncentive(dsmtEAModel.Surface bp)
    {
         decimal result =0;
        Try
        {
            decimal currentCost = bp.rcmd.Cost__c;
            if(CurrentCost ==null) currentCost =0;
            
            result = GetIncentiveFromProduct(bp);

            //system.debug('incentive>>>'+result);

            //DSST-15099
            // Commend below code
            // Rohit Sharma
            /*
            if(currentCost >0 && result == 0 )
            {
                result = currentCost * 1;                
            }*/
           
        }
        Catch(Exception ex)
        {
            WriteMessage(ex);         
        }
        
        return result;     
    }
    
    
    public static void WriteMessage(Exception Ex)
    {
        system.debug('Error Message :- ' + ex.getMessage() + ' ' + ex.getStackTraceString());
    }

    private static decimal GetIncentiveFromProduct(dsmtEAModel.Surface bp)
    {
        decimal result =0;
        decimal currentCost = bp.rcmd.Cost__c;

        string primaryIncentiveString = bp.product.Primary_Incentive__c;
        if(primaryIncentiveString==null) primaryIncentiveString='';

        decimal primaryIncentive=0;

        if(primaryIncentiveString == '') return result;

        string incentiveType = 'percent';                    
        if(primaryIncentiveString.contains('%'))
        {
            primaryIncentiveString = primaryIncentiveString.replace('%','');
        }
        else if(primaryIncentiveString.contains('$'))
        {
            incentiveType = 'dollar';
            primaryIncentiveString = primaryIncentiveString.replace('$','');
        }

        try 
        {
            primaryIncentiveString = primaryIncentiveString.replace(',','');
            primaryIncentive = decimal.valueOf(primaryIncentiveString.trim());
        } 
        catch(Exception ex) 
        {
            dsmtHelperClass.WriteMessage(ex);
        }

        if(incentiveType=='percent')
        {
            result = currentCost * (primaryIncentive * .01);
        }
        else {
            result = primaryIncentive;
        }

        return result;
    }
}