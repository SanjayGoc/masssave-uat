public with sharing class dsmtCOLIGridController{
    
    public List<Change_Order_Line_Item__c> lstcoli{get;set;}
    String RecId = '';
    public Review__c rev{get;set;}
    
    Public List<ChangeOrderLIObj> attachObjlist{get;set;}
    
    public dsmtCOLIGridController(ApexPages.StandardController sc){
        
        lstcoli = new List<Change_Order_Line_Item__c>();
        
        String type = sc.getRecord().getSObjectType().getDescribe().getName();
        
        if(type  == 'Review__c'){
            RecId = sc.getId();    
            rev = new Review__c();
            rev = [select id,Name from Review__c where id =: RecId];   
        }
        
        lstcoli = [select Id,Name,Change_Order_Quantity__c,Inspected_Quantity__c,Original_Quantity__c from Change_Order_Line_Item__c where Review__c = : RecId ORDER BY CreatedDate desc limit 999]; 
        attachObjlist = new List<ChangeOrderLIObj>();
        
        for(Change_Order_Line_Item__c attach : lstcoli){
            ChangeOrderLIObj attachobj = new ChangeOrderLIObj();
            attachObj.attachId = attach.Id;
            attachobj.Name = attach.Name;
            //attachObj.Type = attach.RecordType.Name;
            attachObj.COQuantity = String.valueOf(attach.Change_Order_Quantity__c);
            attachObj.InsQuantity = string.valueOf(attach.Inspected_Quantity__c);
            attachObj.OrigQuantity = string.valueOf(attach.Original_Quantity__c);
            attachObjlist.add(attachObj);
        }
        
    }
    
    
    
    Public class ChangeOrderLIObj{
        public String attachId{get;set;}
        public String Name{get;set;}
        public String Type{get;set;}
        public String COQuantity{get;set;}
        public String InsQuantity{get;set;}
        public String OrigQuantity{get;set;}
    }
}