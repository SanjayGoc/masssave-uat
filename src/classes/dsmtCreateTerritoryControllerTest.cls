@isTest
public class dsmtCreateTerritoryControllerTest {

    @isTest
    public static void runTest(){
        
        Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        insert wt;
        
        Territory__c ter = Datagenerator.createTerritory(regObj.Id,emp.Id,wt.Id);
        ter.Location__c = locObj.Id;
        insert ter;
        
        Territory_Detail__c td = Datagenerator.createTerritoryDetail(ter.Id);
        insert td;
        
        ter = [select name,address__c,Geo_Coordinates__Latitude__s,Geo_Coordinates__Longitude__s from territory__c where id =: ter.Id];
        
          ApexPages.currentPage().getParameters().put('id',locobj.Id);
           dsmtCreateTerritoryController controller = new dsmtCreateTerritoryController();
          
           dsmtCreateTerritoryController.checkTerritory(ter.Name, locobj.Id);
           dsmtCreateTerritoryController.saveTerritory(ter.Name, ter.Address__c, ter.Geo_Coordinates__Latitude__s, ter.Geo_Coordinates__Longitude__s, locobj.Id);
           controller.TerId = ter.Id;  
           controller.coordinates = '32.010404958190534,-104.512939453125';
           controller.SaveTerritory();
           controller.DeleteEmpTerritory();
    
    
   } 
}