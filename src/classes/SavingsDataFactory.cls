@isTest
public class SavingsDataFactory
{
    
    /*public static testmethod void test1()
    {
        Building_Specification__c bs = createBuildingSpecification();
        
        createEnergyAssessment(bs);
    }
    
    public static testmethod void test2()
    {
        Building_Specification__c bs = createBuildingSpecification();
        
        createEnergyAssessment2(bs);
    }
    
    public static testmethod void test3()
    {
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        WallRatio__c wr = new WallRatio__c(Name = 'Rectangular', Value__c = 10);
        insert wr;
        
        System_Config__c systemConfig = new System_Config__c(Arrival_Window_min_before_Appointment__c = 40,
                                                             Arrival_Window_min_after_Appointment__c = 60);
        insert systemConfig;
        
        Saving_Constant__c sc = new Saving_Constant__c(DefaultDoorWidth__c = 10,
                                                       DefaultDoorHeight__c = 20,
                                                       FrontBackToLeftRightWindowAreaFactor__c = 23);
        insert sc;
        
        WallInsulationAmountRValue__c wiar = new WallInsulationAmountRValue__c(Name = 'Standard',
                                                                               Value__c = 12);
        insert wiar;
        
        LightingAndApplianceConstant__c lightApplianceConstant = new LightingAndApplianceConstant__c(DefaultIndoorLightingPerSqFt__c = 30,
                                                                                                     DefaultIndoorLightingUsage__c = '23',
                                                                                                     DefaultOutdoorLightingPerSqFt__c = 30,
                                                                                                     DefaultOutdoorLightingUsage__c = '23');
        insert lightApplianceConstant;
        
        Workorder__c wo = new Workorder__c();
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();         
        insert wo;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Floor_Area__c = 1;
        bs.Orientation__c = 'South';
        bs.Floors__C=1.0;
        bs.Bedromm__c = 1;
        bs.Ceiling_Heights__c = 10;
        bs.Appointment__c = appt.id;
        bs.Occupants__c = 10;
        bs.Year_Built__c='2018';
        
        insert bs;
        
        List<Building_Specification__c> eaList = new List<Building_Specification__c>();
        eaList.add(bs);
        
        Map<Id, Building_Specification__c> eaMap = new Map<Id, Building_Specification__c>();
        Building_Specification__c bsc = new Building_Specification__c(Floor_Area__c = 2);
        eaMap.put(bs.Id, bsc);
        
        BuildingSpecificationTriggerHandler.getSObjectFields('Air_Flow_and_Air_Leakage__c');
        BuildingSpecificationTriggerHandler.dsmtBldgspecBAScfm50AU(eaList, eaMap);
    }*/
    
    public static Building_Specification__c createBuildingSpecification()
    {
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        WallRatio__c wr = new WallRatio__c(Name = 'Rectangular', Value__c = 10);
        insert wr;
        
        System_Config__c systemConfig = new System_Config__c(Arrival_Window_min_before_Appointment__c = 40,
                                                             Arrival_Window_min_after_Appointment__c = 60);
        insert systemConfig;
        
        Saving_Constant__c sc = new Saving_Constant__c(DefaultDoorWidth__c = 10,
                                                       DefaultDoorHeight__c = 20,
                                                       FrontBackToLeftRightWindowAreaFactor__c = 23);
        insert sc;
        
        WallInsulationAmountRValue__c wiar = new WallInsulationAmountRValue__c(Name = 'Standard',
                                                                               Value__c = 12);
        insert wiar;
        
        LightingAndApplianceConstant__c lightApplianceConstant = new LightingAndApplianceConstant__c(DefaultIndoorLightingPerSqFt__c = 30,
                                                                                                     DefaultIndoorLightingUsage__c = '23',
                                                                                                     DefaultOutdoorLightingPerSqFt__c = 30,
                                                                                                     DefaultOutdoorLightingUsage__c = '23');
        insert lightApplianceConstant;
        
        Workorder__c wo = new Workorder__c();
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();         
        insert wo;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Floor_Area__c = 1;
        bs.Orientation__c = 'South';
        bs.Floors__C=1.0;
        bs.Bedromm__c = 1;
        bs.Ceiling_Heights__c = 10;
        bs.Appointment__c = appt.id;
        bs.Occupants__c = 10;
        bs.Year_Built__c='2018';
        
        insert bs;
        
        return bs;
    }
    
    public static Energy_Assessment__c createEnergyAssessment(Building_Specification__c bs)
    {
        Workorder__c wo = new Workorder__c();
        insert wo;
        
        Appointment__c app = new Appointment__c(Workorder__c = wo.Id);
        insert app;
        
        Energy_Assessment__c ea = new Energy_Assessment__c(Building_Specification__c = bs.Id,
                                                           Building_Shape__c = 'Rectangular',
                                                           Assessment_Type__c = 'HEA (Home Energy Assessment)',
                                                           Appointment__c = app.Id,
                                                           Workorder__c = wo.Id);
        
        insert ea;
        
        Thermal_Envelope__c tenv = new Thermal_Envelope__c(Energy_Assessment__c = ea.Id);
        insert tenv;
        
        Test.startTest();
        
        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c(Energy_Assessment__c = ea.Id,
                                                                    Thermal_Envelope__c = tenv.Id,
                                                                    RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Exterior Wall').getRecordTypeId());
        
        try{
            insert tet;
        }catch(Exception ex){}
        
        Dsmtracker_Product__c dp = new Dsmtracker_Product__c(Name = 'Test',
                                                             Category__c = 'Direct Install');
        insert dp;
        
        
        
        Recommendation__c recom = new Recommendation__c(Energy_Assessment__c = ea.Id,
                                                        Installed_Date__c = date.today(),
                                                        Dsmtracker_Product__c = dp.Id);
                                                        
        insert recom;
        
        Set<Id> eaIdSet = new Set<Id>();
        eaIdSet.add(ea.Id);
        
        List<Energy_Assessment__c> eaList = new List<Energy_Assessment__c>();
        eaList.add(ea);
        
        Map<Id, Energy_Assessment__c> eaMap = new Map<Id, Energy_Assessment__c>();
        eaMap.put(ea.Id, ea);
        
        dsmtEnergyAssessmentHelper.GetInvoiceType(eaIdSet);
        
        dsmtEnergyAssessmentHelper.PopulateContractor(eaList);
        
        try{
            dsmtEnergyAssessmentHelper.PopulateCustomerinfoOnAssessment(eaList, eaMap);
        }catch(Exception ex){}
        
        Test.stopTest();
        
        return ea;
    }
    
    public static Energy_Assessment__c createEnergyAssessment2(Building_Specification__c bs)
    {
        Workorder__c wo = new Workorder__c();
        insert wo;
        
        Appointment__c app = new Appointment__c(Workorder__c = wo.Id);
        insert app;
        
        Energy_Assessment__c ea = new Energy_Assessment__c(Building_Specification__c = bs.Id,
                                                           Building_Shape__c = 'Rectangular',
                                                           Assessment_Type__c = 'HEA (Home Energy Assessment)',
                                                           Appointment__c = app.Id,
                                                           Workorder__c = wo.Id);
        
        insert ea;
        
        Air_Flow_and_Air_Leakage__c af = new Air_Flow_and_Air_Leakage__c(Energy_Assessment__c = ea.Id);
        insert af;
        
        Test.startTest();
        
        update af;
        
        Dsmtracker_Product__c dp = new Dsmtracker_Product__c(Name = 'Test',
                                                             Category__c = 'Direct Install');
        insert dp;
        
        
        Recommendation__c recom = new Recommendation__c(Energy_Assessment__c = ea.Id,
                                                        Installed_Date__c = date.today(),
                                                        Dsmtracker_Product__c = dp.Id);
                                                        
        insert recom;
        
        Set<Id> eaIdSet = new Set<Id>();
        eaIdSet.add(ea.Id);
        
        List<Energy_Assessment__c> eaList = new List<Energy_Assessment__c>();
        eaList.add(ea);
        
        Map<Id, Energy_Assessment__c> eaMap = new Map<Id, Energy_Assessment__c>();
        eaMap.put(ea.Id, ea);
        
        dsmtEnergyAssessmentHelper.GetInvoiceType(eaIdSet);
        
        dsmtEnergyAssessmentHelper.PopulateContractor(eaList);
        
        try{
            dsmtEnergyAssessmentHelper.PopulateCustomerinfoOnAssessment(eaList, eaMap);
        }catch(Exception ex){}
        
        
        dsmtEAModel.Surface bp = new dsmtEAModel.Surface();
        
        List<Appliance__c> allApplianceList = new List<Appliance__c>{new Appliance__c(Type__c = 'Dishwasher'),
                                                                     new Appliance__c(Type__c = 'Washer'),
                                                                     new Appliance__c(Type__c = 'Refrigeration')};
        
        bp.aplList = allApplianceList;
        
        ApplianceTriggerHandler.updateApplianceSystem(bp);
        
        Test.stopTest();
        
        return ea;
    }
    
    public static testmethod void test4()
    {
        Building_Specification__c bs = createBuildingSpecification();
        
        Workorder__c wo = new Workorder__c();
        insert wo;
        
        Appointment__c app = new Appointment__c(Workorder__c = wo.Id);
        insert app;
        
        Energy_Assessment__c ea = new Energy_Assessment__c(Building_Specification__c = bs.Id,
                                                           Building_Shape__c = 'Rectangular',
                                                           Assessment_Type__c = 'HEA (Home Energy Assessment)',
                                                           Appointment__c = app.Id,
                                                           Workorder__c = wo.Id);
        
        insert ea;
        
        Air_Flow_and_Air_Leakage__c af = new Air_Flow_and_Air_Leakage__c(Energy_Assessment__c = ea.Id);
        insert af;
        
        Test.startTest();
        
        update af;
        
        Dsmtracker_Product__c dp = new Dsmtracker_Product__c(Name = 'Test',
                                                             Category__c = 'Direct Install');
        insert dp;
        
        
        Recommendation__c recom = new Recommendation__c(Energy_Assessment__c = ea.Id,
                                                        Installed_Date__c = date.today(),
                                                        Dsmtracker_Product__c = dp.Id,
                                                        Quantity__c = 1,
                                                        PipeInsulationPieceLength__c = 1,
                                                        Operation__c = 'Duct Sealing',
                                                        TestLeakageTestMethod__c = 'BD Subtraction @50Pa');
                                                        
        insert recom;
        
        Set<Id> eaIdSet = new Set<Id>();
        eaIdSet.add(ea.Id);
        
        Set<String> eaSet = new Set<String>();
        eaSet.add(ea.Id);
        
        List<Energy_Assessment__c> eaList = new List<Energy_Assessment__c>();
        eaList.add(ea);
        
        Map<Id, Energy_Assessment__c> eaMap = new Map<Id, Energy_Assessment__c>();
        eaMap.put(ea.Id, ea);
        
        DistributionSystemRecommendationHelper.GetDuctMeasureOperation(1);
        DistributionSystemRecommendationHelper.GetDuctMeasureOperation(2);
        DistributionSystemRecommendationHelper.GetDuctMeasureOperation(3);
        DistributionSystemRecommendationHelper.GetDuctMeasureOperation(4);
        DistributionSystemRecommendationHelper.GetDuctMeasureOperation(5);
        DistributionSystemRecommendationHelper.GetDuctMeasureOperation(6);
        DistributionSystemRecommendationHelper.GetDuctMeasureOperation(7);
        
        DistributionSystemRecommendationHelper.ComputePipeInsulationLength(recom);
        
        dsmtEAModel.Surface bp = new dsmtEAModel.Surface();
        bp.mstObj = new Mechanical_Sub_Type__c();
        bp.ProductPartData = new DistributionSystem_Part__c();
        
        try{
            DistributionSystemRecommendationHelper.ComputeFinalPipeInsulationLength(recom, bp);
        }catch(Exception ex){}
        
        try{
            DistributionSystemRecommendationHelper.ComputeRecommendationReductionCommon(recom, bp, bp);
            
            bp.ProductPartData = new DistributionSystem_Part__c(IsAeroseal__c = 1);
            DistributionSystemRecommendationHelper.ComputeRecommendationReductionCommon(recom, bp, bp);
        }catch(Exception ex){}
        
        DistributionSystemRecommendationHelper.ComputeTestHours(recom, bp, bp);
        
        try{
            DistributionSystemRecommendationHelper.ComputeTestInNetTotalLeakageCommon(recom, bp, bp);
        }catch(Exception ex){}
        
        
        DistributionSystemRecommendationHelper.ComputeTestInTotalLeakageCommon(recom, bp, bp);
        DistributionSystemRecommendationHelper.ComputeTestInSupplyLeakageCommon(recom, bp, bp);
        DistributionSystemRecommendationHelper.ComputeTestInReturnLeakageCommon(recom, bp, bp);
        
        try{
            DistributionSystemRecommendationHelper.ComputeTestOutNetTotalLeakageCommon(recom, bp, bp);
        }catch(Exception ex){}
        
        try{
            DistributionSystemRecommendationHelper.ComputeTestOutNetSupplyLeakageCommon(recom, bp, bp);
        }catch(Exception ex){}
        
        try{
            DistributionSystemRecommendationHelper.ComputeTestOutNetReturnLeakageCommon(recom, bp, bp);
        }catch(Exception ex){}
        
        DistributionSystemRecommendationHelper.ComputeTestOutTotalLeakageCommon(recom, bp, bp);
        DistributionSystemRecommendationHelper.ComputeTestOutSupplyLeakageCommon(recom, bp, bp);
        DistributionSystemRecommendationHelper.ComputeTestOutReturnLeakageCommon(recom, bp, bp);
        try{
            DistributionSystemRecommendationHelper.ComputeTestInNetSupplyLeakageCommon(recom, bp, bp);
        }catch(Exception ex){}
        
        try{
            DistributionSystemRecommendationHelper.ComputeTestInNetReturnLeakageCommon(recom, bp, bp);
        }catch(Exception ex){}
        
        bp.ai = new Air_Flow_and_Air_Leakage__c(ActualCFM50__c = 3, CFM50__c = 4);
        bp.eaObj = new Energy_Assessment__c(ShellAreaGross__c = 4);
        
        dsmtAirFlowHelper.ComputeShellLeakinessCommon(bp);
        dsmtAirFlowHelper.GetNewACH(bp);
        dsmtAirFlowHelper.GetNewCFM50(bp);
        dsmtAirFlowHelper.GetCFM50Delta(bp);
        
        dsmtApplianceHelper.queryBuildingSpecificationMap(eaSet);
        dsmtApplianceHelper.UpdateAllApplianceOnBuildingYearChange(eaSet);
        
        Appliance__c a = new Appliance__c(Metering_Minutes__c = 6,
                                            Peal_kW__c = 6,
                                            Room_Temperature__c = 6,
                                            Metered_kWh__c = 6);
        
        dsmtApplianceHelper.ComputeRefrigeratorkWhComputedFromMeteringCommon(a);
        dsmtApplianceHelper.ComputeRefrigeratorkWhModeledCommon(a, bs);
        dsmtApplianceHelper.ComputeRefrigeratorMeteringMinutesCommon(a);
        dsmtApplianceHelper.computeOtherAnnualFuelConsumption(a);
        dsmtApplianceHelper.ComputeHomeOfficeDesktopPCQuantityCommon(a);
        dsmtApplianceHelper.ComputeHomeOfficeNotebookPCQuantityCommon(a);
        dsmtApplianceHelper.ComputeHomeOfficeOtherQuantityCommon(a);
    }
}