global with sharing class dsmtTradeAllyHeaderController{
    
    public List<TradeAlly_Portal_Tab__c> tabList{get;set;}
    public Dsmtracker_Contact__c dsmtContact{get;set;}
    public Trade_Ally_Account__c tradeallyAccount{get;set;}
    
    public String accid{get;set;}
    public String conid{get;set;}
    
    public dsmtTradeAllyHeaderController(){
        
        fetchdsmtContactAndTradeAllyAccount();
        
        String portalRole = (dsmtcontact != null)?dsmtcontact.Portal_Role__c : '';
        
        tabList = new List<TradeAlly_Portal_Tab__c>();
        
        String whereclause = '';
        if(portalRole == 'Manager'){
           whereclause = ' Manager__c = true ';
        }else if(portalRole == 'Scheduler'){
           whereclause = ' Scheduler__c = true '; 
        }else if(portalRole == 'Energy Specialist'){
           whereclause = ' Energy_Specialist__c = true ';
        }
        
        if(tradeallyAccount.Trade_Ally_Type__c == 'IIC'){
           if(whereclause != ''){
              whereclause += ' and ';
           }
           whereclause += ' name not in (\'My Appointments\',\'Pending Intakes\')';
        }
        
        String query = 'select name,Tab_URL__c,Sequence__c,Manager__c,Scheduler__c,Energy_Specialist__c from TradeAlly_Portal_Tab__c ';
        if(whereclause.trim().length() > 0){
           query += 'where '+whereclause;
        }
        query += ' order by Sequence__c ASC';
        system.debug('--query--'+query);
        
        tabList = database.query(query);
        
        system.debug('--tablist--'+tablist);
    }
    
     private void fetchdsmtContactAndTradeAllyAccount() {
              
        tradeallyAccount  = new Trade_Ally_Account__c();
        String usrid = UserInfo.getUserId();

        List < User > usrList = [select Id, SecurityQuestion__c, Ans_SecurityQuestion__c, ContactId, Contact.AccountId, Contact.Account.Name, Contact.Account.RecordType.Name, Contact.Name from user Where Id = : usrid];
        if (usrList != null && usrList.size() > 0) {
            if (usrList.get(0).ContactId != null) {
                accid = usrList.get(0).Contact.AccountId;
                conId = usrList.get(0).ContactId;

                
                List < DSMTracker_Contact__c > dsmtContLst = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c where Contact__r.Id = : conId and Trade_Ally_Account__c != null limit 1];
                if (dsmtContLst != null && dsmtContLst.size() > 0) {
                     
                     dsmtcontact = dsmtContLst.get(0);
                     
                     List<Trade_Ally_Account__c> taList = [select id,Internal_Account__c,Trade_Ally_Type__c,name,Account__c, Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,Website__c,Phone__c,Email__c,Registration_Renewal_Date__c,
                                                                Outreach_rep__r.Phone,Outreach_rep__r.Email,Outreach_rep__r.Name from Trade_Ally_Account__c where Id =: dsmtcontact.Trade_Ally_Account__c];
                        if(taList != null && taList.size() > 0){
                            tradeallyAccount = taList.get(0);
                         }
                }
            }
        }
    }
        
    @RemoteAction
    global static boolean updateEULADate()
    {
        
        List<User> ulist =[Select Id,IsActive,EULA_Agreement_Date__c from User where Id =:UserInfo.getUserId()];// AND ProfileId IN: allowedProfileIdToLogin];       
        if(ulist.size() > 0 && ulist[0].EULA_Agreement_Date__c == null){
            ulist[0].EULA_Agreement_Date__c = System.now();
            update ulist[0];
        }
        
        return true;
    }
}