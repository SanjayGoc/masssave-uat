@isTest
private class dsmtWaterFixtureHelperTest
{
    @isTest
    static void test1()
    {
        dsmtEAModel.setupAssessment();

        Test.startTest();

        List<Water_Fixture__c> waterList = [Select Id, Type__c, RecordTypeId From Water_Fixture__c];

        update waterList;

        Test.stopTest();        
    }

    @isTest
    static void test2()
    {
        Test.startTest();

        dsmtEAModel.setupAssessment();

        Test.stopTest();        
    }

     @isTest
    static void test3()
    {
        Energy_Assessment__c ea = dsmtEAModel.setupAssessment();

        Test.startTest();

        List<Water_Fixture__c> waterList = new List<Water_Fixture__c>();

        Water_Fixture__c waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Pool').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterobj.Type__c = 'Pool';
        waterobj.NumberOperatingMonths__c =10;
        waterobj.TimerHours__c = 4;
        waterobj.WinterHours__c = 5;
        waterobj.PoolPumpWatts__c = 20;
        waterList.add(waterobj);

        dsmtWaterFixtureHelper.ComputePoolHasOffSeasonUseCommon(waterobj);

        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Sink').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);

        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Spa').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);


        insert waterList;

        Test.stopTest();        
    }
}