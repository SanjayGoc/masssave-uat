@isTest
public class dsmtTradeAllyManageContactsCntrlTest {
    @isTest
    public Static Void RunTest(){
       
        //User u = Datagenerator.CreatePortalUser();
        
        User u=createPortalUser();
        
        System.runAs(u) {
            
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c(name='test',Trade_Ally_Type__c = 'HPC');
            insert ta;
            DSMTracker_Contact__c dc = Datagenerator.createDSMTracker();
            
            Review__c review = new Review__c(Email__c='test@test.com',Trade_Ally_Account__c=ta.Id,Status__c = 'Submitted For Review', Requested_By__c = Userinfo.getuserid(),Review_Type__c='Employee');
            insert review;
            
            DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test1',email__c='test1@test.com',phone__c='1234115',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true, Status__c='Approved');
            dsmt.Trade_Ally_Account__c = ta.Id;
            //dsmt.Review__c = review.Id;
            insert dsmt;
            
            Exception__c exc = new Exception__c(DSMTracker_Contact__c = dsmt.Id,Required_Attachment_Type__c='BPI Building Analyst');
            insert exc;
            
            Exception__c exc1 = new Exception__c(DSMTracker_Contact__c = dsmt.Id,Required_Attachment_Type__c='BPI Building Analyst');
            exc1.Request_Date_Of_Hire__c = Date.today().addMonths(7);
            exc1.Expiry_date__c = Date.today().addMonths(-1);
            insert exc1;
            
            Attachment__c att = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='UnSubmitted',DSMTracker_Contact__c=dsmt.Id);
            insert att;
            
            Attachment__c att1 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='Approved',DSMTracker_Contact__c=dsmt.Id);
            insert att1;
            
            Attachment__c att3 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='Approved',DSMTracker_Contact__c=dc.Id);
            insert att3;
            
            Attachment__c att2 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='Approved');
            insert att2;
        
            ApexPages.currentPage().getParameters().put('taid',ta.Id);
            ApexPages.currentPage().getParameters().put('dcid', dc.Id);
            ApexPages.currentPage().getParameters().put('id', review.Id);
            Test.startTest();
            dsmtTradeAllyManageContactsController dsmtmng = new dsmtTradeAllyManageContactsController();
            boolean isEmailRequiredForNewEmployee = dsmtmng.isEmailRequiredForNewEmployee;
            String PortalURL = dsmtmng.PortalURL;
            String orgId = dsmtmng.orgId;
            dsmtmng.rerender();
            //dsmtmng.saveAsDraft();
            //dsmtmng.recallReview(review);
            dsmtmng.saveNewEmployee();
            dsmtmng.copyContacts(review.Id);
            dsmtmng.saveAndSubmit();
            
            if(dsmtmng.employees != null && dsmtmng.employees.size()>0)
            {
                dsmtmng.employeeid = dsmtmng.employees.get(0).Employee.id;
            }
            dsmtmng.updateDSMTExceptions();
            dsmtmng.toggleActiveInactiveEmployee();
            
            dsmtmng.isSubmitReviewexist = false;
            ApexPages.currentPage().getParameters().remove('id');
            dsmtmng.checkReview();
            ApexPages.currentPage().getParameters().remove('id');
            dsmtmng.taaccount = null;
            dsmtmng.submittedReview = null;
            dsmtmng.checkReview();
            dsmtTradeAllyManageContactsController.EmployeeModel emodel=new dsmtTradeAllyManageContactsController.EmployeeModel();
            emodel.employee=dsmt;
            emodel.checkFieldValidations();
            emodel.save();
            emodel.toggleActive();
            //dsmtmng.checkFieldValidations();
            //dsmtmng.save();
            //dsmtmng.toggleActive();
            Test.stopTest();
        }
        
    }
    public static user createPortalUser()
    {
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test21234@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        return portalAccountOwner1;
    }
    @isTest
    public Static Void RunTest2(){
       
        User u = createPortalUser();
        
        System.runAs(u) {
            
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c(name='test');
            insert ta;
            DSMTracker_Contact__c dc = Datagenerator.createDSMTracker();
            
            Review__c review = new Review__c(Email__c='test@test.com',Trade_Ally_Account__c=ta.Id,Status__c = 'Submitted For Review', Requested_By__c = Userinfo.getuserid(),Review_Type__c='Employee');
            insert review;
            
            DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test1',email__c='test1@test.com',phone__c='1234115',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true, Status__c='Approved',CSL_Attached__c=true);
            dsmt.Trade_Ally_Account__c = ta.Id;
            //dsmt.Review__c = review.Id;
            dsmt.BPI_Envelope_Attached__c = true;
            insert dsmt;
            /*
            Attachment__c att = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='UnSubmitted',DSMTracker_Contact__c=dsmt.Id);
            insert att;
            
            Attachment__c att1 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='Approved',DSMTracker_Contact__c=dsmt.Id);
            insert att1;
            
            Attachment__c att3 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='Approved',DSMTracker_Contact__c=dc.Id);
            insert att3;
            
            Attachment__c att2 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='Approved');
            insert att2;*/
        
            ApexPages.currentPage().getParameters().put('taid',ta.Id);
            ApexPages.currentPage().getParameters().put('dcid', dc.Id);
            ApexPages.currentPage().getParameters().put('id', review.Id);
            Test.startTest();
            dsmtTradeAllyManageContactsController dsmtmng = new dsmtTradeAllyManageContactsController();
            Map<String,dsmtTradeAllyManageContactsController.EmployeeModel> employeesMap = dsmtmng.employeesMap;
            boolean isEmailRequiredForNewEmployee = dsmtmng.isEmailRequiredForNewEmployee;
            String PortalURL = dsmtmng.PortalURL;
            String orgId = dsmtmng.orgId;
            dsmtmng.rerender();
            //dsmtmng.saveAsDraft();
            //dsmtmng.recallReview(review);
            dsmtmng.saveNewEmployee();
            dsmtmng.copyContacts(review.Id);
            dsmtmng.saveAndSubmit();
            
            
            if(dsmtmng.employees != null && dsmtmng.employees.size()>0)
            {
                dsmtmng.employeeid = dsmtmng.employees.get(0).Employee.id;
                dsmtmng.toggleEmployeeId = dsmtmng.employees.get(0).Employee.id;
            }
            dsmtmng.updateDSMTExceptions();
            dsmtmng.toggleActiveInactiveEmployee();
            Test.stopTest();
        }
        
    }

}