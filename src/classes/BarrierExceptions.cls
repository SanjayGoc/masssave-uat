public class BarrierExceptions
{
   public static boolean isStopValidateException = false;
   public static void InsertExceptionsOnRR(List<Barrier__c> RRlist){
       
        Map<String,Exception_Template__c> mapExt = new Map<String,Exception_Template__c>();     
        //Gets all expection with that check box checked.
        for(Exception_Template__c ext : [select id,name,Reference_ID__c,Type__c,Online_Portal_Title__c,Online_Portal_Text__c,Online_Application_Section__c,Required_Attachment_Type__c,Internal__c,Exception_Message__c,Outbound_Message__c,Short_Rejection_Reason__c,Attachment_Needed__c from Exception_template__c where Automatic__c = true and Active__c = true and Barrier_Level_Message__c = true ]) {
          mapExt.put(ext.Reference_Id__c,ext);
        } 
        
        Set<Id> rrids = new Set<id>();
        for(Barrier__c rr :RRlist){
             rrids.add(rr.id);   
        }
        
        Map<String,Exception__c> mapExceptions = new Map<String,Exception__c>();
        for(Exception__c Ex :[SELECT id,name, Disposition__c,Barrier__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Project__c, Project__r.id FROM Exception__c 
                                WHERE Barrier__r.id IN:rrids AND Disposition__c  != 'Resolved']){    
            mapExceptions.put(Ex.Barrier__c +'-'+Ex.Exception_Template__r.Reference_ID__c,Ex);     
        }      
        
        List<Exception__c> ExceptionsCreated = new List<Exception__c>();
          
        for(Barrier__c RRNew : RRlist) {
            
            List<string> AutoExceptionsList = new List<string>();
            
            /*for(String refId : mapExt.keyset()){
                Exception_Template__c temp = mapExt.get(refId);
               
                if(temp != null){
                      
                     if(refId == 'BAR-00001' && RRNew.Knob__c==false && RRNew.RecordType.NAme == 'Asbestos'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00001')==null) 
                            AutoExceptionsList.add('BAR-00001'); 
                     }
                     if(refId == 'BAR-00002' && RRNew.Asbestos_Addressed__c ==false  && RRNew.RecordType.NAme == 'Asbestos'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00002')==null) 
                            AutoExceptionsList.add('BAR-00002'); 
                     }
                     if(refId == 'BAR-00003' && RRNew.Knob_Tube_Wiring_Uploaded__c==false && RRNew.RecordType.NAme == 'Knob and Tube Wiring'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00003')==null) 
                            AutoExceptionsList.add('BAR-00003'); 
                     }
                     if(refId == 'BAR-00004' && RRNew.Knob_Tube_Wiring_Addressed__c==false && RRNew.RecordType.NAme == 'Knob and Tube Wiring'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00004')==null) 
                            AutoExceptionsList.add('BAR-00004'); 
                     }
                     if(refId == 'BAR-00005' && RRNew.Moisture_Uploaded__c==false && RRNew.RecordType.NAme == 'Moisture'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00005')==null) 
                            AutoExceptionsList.add('BAR-00005'); 
                     }
                     if(refId == 'BAR-00006' && RRNew.Moisture_Addressed__c==false && RRNew.RecordType.NAme == 'Moisture'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00006')==null) 
                            AutoExceptionsList.add('BAR-00006'); 
                     }
                     if(refId == 'BAR-00007' && RRNew.Structural_Uploaded__c==false && RRNew.RecordType.NAme == 'Structural'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00007')==null) 
                            AutoExceptionsList.add('BAR-00007'); 
                     }
                     if(refId == 'BAR-00008' && RRNew.Structural_Addressed__c==false && RRNew.RecordType.NAme == 'Structural'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00008')==null) 
                            AutoExceptionsList.add('BAR-00008'); 
                     }
                     if(refId == 'BAR-00009' && RRNew.Combustion_Safety_Uploaded__c ==false && RRNew.RecordType.NAme == 'Combustion Safety'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00009')==null) 
                            AutoExceptionsList.add('BAR-00009'); 
                     }
                     if(refId == 'BAR-00010' && RRNew.Combustion_Safety_Addressed__c==false && RRNew.RecordType.NAme == 'Combustion Safety'){
                        If(mapExceptions.get(RRNew.id+'-BAR-00010')==null) 
                            AutoExceptionsList.add('BAR-00010'); 
                     }
                     
                       
               }
                
           }
           
            system.debug('--AutoExceptionsList--'+AutoExceptionsList);
            for(string autolist :AutoExceptionsList){
                if(mapExt.get(autolist) !=null) {
                    Exception_Template__c Exceptiontemp = mapExt.get(autolist);
                    Exception__c ExceptionstoAdd = new Exception__c(); 
                    ExceptionstoAdd.Internal__c = Exceptiontemp.Internal__c; 
                    ExceptionstoAdd.Exception_Message__c= Exceptiontemp.Exception_Message__c; 
                    ExceptionstoAdd.Outbound_Message__c=Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Barrier__c =  RRNew.Id;
                    ExceptionstoAdd.Energy_Assessment__c=  RRNew.Energy_Assessment__c;
                    ExceptionstoAdd.Exception_Template__c = Exceptiontemp.id;
                    ExceptionstoAdd.FInal_Outbound_Message__c = Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Short_Rejection_Reason__c = Exceptiontemp.Short_Rejection_Reason__c ;
                    ExceptionstoAdd.Automated__c = true;
                    ExceptionstoAdd.OwnerId =RRNew.OwnerId;
                    ExceptionstoAdd.Attachment_Needed__c = Exceptiontemp.Attachment_Needed__c;
                    ExceptionstoAdd.Required_Attachment_Type__c = Exceptiontemp.Required_Attachment_Type__c;
                    ExceptionstoAdd.Online_Application_Section__c = Exceptiontemp.Online_Application_Section__c;
                    ExceptionstoAdd.Type__c = Exceptiontemp.Type__c;
                    //ExceptionstoAdd.Automatic_Communication__c = Exceptiontemp.Automatic_Communication__c;
                    //ExceptionstoAdd.Automatic_Rejection__c = Exceptiontemp.Automatic_Rejection__c;
                    //ExceptionstoAdd.Move_to_Incomplete_Queue__c = Exceptiontemp.Move_to_Incomplete_Queue__c;
                    ExceptionstoAdd.Online_Portal_Title__c  = Exceptiontemp.Online_Portal_Title__c;
                    ExceptionstoAdd.Online_Portal_Text__c = Exceptiontemp.Online_Portal_Text__c;
                    ExceptionsCreated.add(ExceptionstoAdd);    
                }
            } */    
       }
        
        system.debug('--ExceptionsCreated--'+ExceptionsCreated);
        BarrierExceptions.isStopValidateException = true;
        if(ExceptionsCreated.size()>0){
           insert ExceptionsCreated;  
        }   
   }
   
   public static void CheckUpdateExceptions(List<Barrier__c> RRlist , Map<Id,Barrier__c> oldmap){
        Map<string,List<Exception__c>> ExceptionsMap = new Map<string,List<Exception__c>>();
        for(Exception__c Exceptions :[SELECT id,name,Barrier__c, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Project__c, Project__r.id FROM Exception__c 
                                WHERE Barrier__r.id=:oldmap.keyset() AND Disposition__c  != 'Resolved']){
                List<Exception__c> lsExceptions = ExceptionsMap.get(Exceptions.Barrier__c+'-'+Exceptions.Exception_Template__r.Reference_ID__c);
                if(lsExceptions  == NULL)
                    lsExceptions  = new List<Exception__c>();
                lsExceptions.add(Exceptions );
                ExceptionsMap.put(Exceptions.Barrier__c+'-'+Exceptions.Exception_Template__r.Reference_ID__c,lsExceptions);        
        }
        
        system.debug('--ExceptionsMap---'+ExceptionsMap);
        
         List<Exception__c> ExceptionstoUpdate = new List<Exception__c>();
         Set<Id> RRId = new Set<Id>();
         for(Barrier__c RR : RRlist){
              RRId.add(RR.Id);        
         }
         
         for(Barrier__c rr : RRlist){
             
                //String keyValue = rr.id + '-BAR-00001';
                 /*if(ExceptionsMap.containskey(rr.id + '-BAR-00001')){  
                     if(rr.Knob__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00001')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                 if(ExceptionsMap.containskey(rr.id + '-BAR-00002')){  
                     if(rr.Asbestos_Addressed__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00002')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                 if(ExceptionsMap.containskey(rr.id + '-BAR-00003')){  
                     if(rr.Knob_Tube_Wiring_Uploaded__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00003')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                if(ExceptionsMap.containskey(rr.id + '-BAR-00004')){  
                     if(rr.Knob_Tube_Wiring_Addressed__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00004')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                 if(ExceptionsMap.containskey(rr.id + '-BAR-00005')){  
                     if(rr.Moisture_Uploaded__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00005')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                 if(ExceptionsMap.containskey(rr.id + '-BAR-00006')){  
                     if(rr.Moisture_Addressed__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00006')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                 if(ExceptionsMap.containskey(rr.id + '-BAR-00007')){  
                     if(rr.Structural_Uploaded__c== true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00007')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                if(ExceptionsMap.containskey(rr.id + '-BAR-00008')){  
                     if(rr.Structural_Addressed__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00008')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-BAR-00009')){  
                     if(rr.Combustion_Safety_Uploaded__c== true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00009')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }*/
                system.debug('----'+ExceptionsMap.containskey(rr.id + '-BAR-00010'));
                system.debug('--rr.Combustion_Safety_Addressed__c--'+rr.Combustion_Safety_Addressed__c);
                
                if(ExceptionsMap.containskey(rr.id + '-BAR-00010')){  
                     if(rr.Combustion_Safety_Addressed__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-BAR-00010')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
       
            }
         
          BarrierExceptions.isStopValidateException = true;
            system.debug('--ExceptionstoUpdate--'+ExceptionstoUpdate);
            if(ExceptionstoUpdate.size()>0){
                update ExceptionstoUpdate;
            }
   }
}