global interface dsmtIDispatchConsoleController{

    void queryWT();
    void queryWT2();
    void queryWO();
    void moveWO();
    void init();
    void scheduleWorder();
    void scheduleAllWorders();
    void saveWorkOdr();
    void CreateSchedule();
    void refetchSchedule();
    void fetchworkorder();
    void fetchSchedules();
    void OptimizeRoute(string workTeamWays, 
                       string woAfterRouts);
    
    boolean deleteWorkoder(String wrkId);
    boolean CheckAvailiability(string sel_date, 
                               string sel_time, 
                               string wrk_ordr_type);
                               
    boolean RA_UpdateSchedule(string wo_id, 
                              string resource_id, 
                              string location_id, 
                              string mon, 
                              string yr, 
                              string dy, 
                              string hr, 
                              string min, 
                              string wrk_ordr_type, 
                              string description);
    
    string getWorkordersJSON(string selected_date);
    
    pagereference refreshPage();
    
    Work_Team__c getWorkTeamForRouting(string wrkTeam, 
                                       string selected_date);
    Location__c getLocationByName(string locationName);
    
    list<Employee__c> getEmployees();
    list<MapPolylineColors__c> getPolylineColors();
    list<Work_Team__c> getWorkteams();
    list<Work_Team__c> getWorkteams2();
    list<Work_Team__c> getCompletedWOWorkteams();
    list<SelectOption> getWorkTypeslist();
    list<Workorder__c> getWorkorderForRouting(string wrkTeam, 
                                              string selected_date);
    list<Location__c> getLocations();
}