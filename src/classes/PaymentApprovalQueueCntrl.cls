public with sharing class PaymentApprovalQueueCntrl {

    public String comments {get;set;}
    public Boolean hide_rejected {get;set;}
    public Boolean hide_more_info {get;set;}
    public String selected_program {get;set;}
    public String selected_program_name {get;set;}

    public List<SelectOption> program_list {get;set;}
    public List<PaymentWrapperClass> paymentWrappers {get;set;}
    
    public String enr_app_id{get;set;}
    public String ea_id {get;set;}
    public String stage {get;set;}
    public String exception_str{get;set;}
    public String excep_msg{get;set;}
    public String rejectionReasons{get;set;}
    public Boolean err_msg{get;set;}
    public String ea_with_oexp{get;set;}
    public String pay_id{get;set;}
    public String commentsss{get;set;}
    
   // public List<Enrollment_Application__c> ea_list {get;set;}
    public List<Exception__c> exceptions{get;set;}
    
    public Boolean payProcess{get;set;}
    
    private Id batchId;
    public Boolean pollerBool {get;set;}
    
    public static String rejectionReason;
    
    public Map<string,string> mapProgramCustom ;
    public boolean TANameDisplay{get;set;}

    public PaymentApprovalQueueCntrl() {
        try {
            mapProgramCustom =new Map<string,string>();
            program_list = new List<SelectOption>();
            exceptions = new List<Exception__c>();
        //    ea_list = new List<Enrollment_Application__c>();
            payProcess = false;
            pollerBool = false;
            
            program_list.add(new SelectOption('None', '--Select a Program--'));
            program_list.add(new SelectOption('All', 'All'));
            for (Program__c program: [SELECT Id, Name from Program__c ORDER BY Program__c.Name ASC]) {
                program_list.add(new SelectOption(program.Id, program.Name));
            }
            hide_rejected = false;
            hide_more_info = false;
            selected_program = 'None';
            paymentWrappers = new List<PaymentWrapperClass>();
            buildPaymentWrapperList();
        } catch (Exception ex) {
            String message = ex.getStackTraceString() + ' -- ' + ex.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, message));
           // LogUtil.write('ApplicationExceptionsController.ApplicationExceptionsController()', ex);
        }
    }

    private final static List<String> paymentFieldsNeeded = new List<String>{
        'Amount__c',
        'Id',
        'Name',
        'Status__c',
        'Payment_Mailing_Address_1__c',
        'Payment_City__c',
        'Payment_State__c',
        'Payment_Postal_Code__c',
        'Customer__c',
        'Customer__r.Name',
        'Trade_Ally_Account__c',
        'Item_Code__c',
        'ItemCode__c',
        'Invoice__c',
        'Invoice__r.Name',
        'Contractor_Invoice_Date__c',
        'Site_ID__c',
        'Customer_Name__c',
        'Notes__c',
        'Trade_Ally_Account__r.Name',
        'Review__r.Contractor_Invoice__c',
        'Voided_or_Cancelled__c',
        'Review__c',
        'Review__r.Name',
        'Batch__c'
    };

    private static String getNeededPaymentFields(){
        return String.join(paymentFieldsNeeded, ', ');
    }
//Scenario 1:
//Payment is associated with an EA
// Payment.Status = 'Approval Pending'
// EA.Stage = 'Complete'
// EA.Status = 'Awaiting Review'
// EA.Program = selected Program

//Scenario 2:
//Payment is associate with a File Request
// Payment.Status = 'PM Approval Pending'
// Payment.FileRequest not null
// FileRequest.MOU.Program = selected Program

    private static List<Payment__c> queryPayments(){
        String query = 'SELECT ' + getNeededPaymentFields() + ' FROM Payment__c ';
        String whereClauseForNonFileRequest = 'Status__c = \'Approval Pending\' '; 
        query += ' WHERE (' + whereClauseForNonFileRequest + ')  ';
        query += ' ORDER BY Name DESC LIMIT 999';
        system.debug('--query--' +query);
        
        if(Test.isRunningTest()){
            query = 'select '+getNeededPaymentFields() +' From Payment__c';
        }
        
        List<Payment__c> result = Database.query(query);
        
        return result;
    }

    

    private static PaymentWrapperClass buildWrapperForNonFileRequest(Payment__c payment){
        System.debug(LoggingLevel.ERROR, 'payment: '+payment);
        
        PaymentWrapperClass paymentWrapper = new PaymentWrapperClass();
        paymentWrapper.ContractorName = payment.Trade_Ally_Account__r.Name;
        paymentWrapper.itemcode = payment.ItemCode__c;
        if(payment.Amount__c != null){
            paymentWrapper.DistributionAmount = String.valueof(payment.Amount__c);
        }
        paymentWrapper.ContractorInvoice = payment.Review__r.Contractor_Invoice__c;
        if(payment.Contractor_Invoice_Date__c != null){
            paymentWrapper.Cidate = payment.Contractor_Invoice_Date__c.Month()+'/'+payment.Contractor_Invoice_Date__c.Month()+'/'+payment.Contractor_Invoice_Date__c.Year();
        }
        paymentWrapper.Siteid = payment.Site_ID__c;
        paymentWrapper.CustomerName = payment.Customer_Name__c;
        paymentWrapper.notes = payment.Notes__c;
        //paymentWrapper.programId = enrollmentApplication.Program__r.Program_Id__c;
       // paymentWrapper.eanumber = enrollmentApplication.Name;
        paymentWrapper.payment = payment;
        paymentWrapper.VoidPayment = payment.Voided_or_Cancelled__c;
        paymentWrapper.mailing_contact = '';
        paymentWrapper.batchNumber = payment.Batch__c;
        paymentWrapper.ReviewId = payment.Review__c;
        paymentWrapper.ReviewNumber = payment.Review__r.Name;
        
        //paymentWrapper.mailing_contact += (enrollmentApplication.Mailing_Contact_First_Name__c != null ? enrollmentApplication.Mailing_Contact_First_Name__c : '') + ' ' + (enrollmentApplication.Mailing_Contact_Last_Name__c != null ? enrollmentApplication.Mailing_Contact_Last_Name__c : '');
       // paymentWrapper.pcall = enrollmentApplication.Comments__c != null ? enrollmentApplication.Comments__c.replace('.','').replace('\n','') : '';
       // paymentWrapper.enrollmentApplicationId = enrollmentApplication.Id;
       // paymentWrapper.customerName = enrollmentApplication.Customer__r.Name; 
        return paymentWrapper; 
    }

   
   
    public void buildPaymentWrapperList() {
        paymentWrappers = new List<PaymentWrapperClass> ();
      //  ea_list = new List<Enrollment_Application__c>();
        
        boolean foundEAWithOpenException = false;
        integer wrapperCounter = 0;
        
        selected_program_name = '';
        TANameDisplay = false;
        for(Payment__c payment : queryPayments()){
            wrapperCounter++;
            // if (wrapperCounter > 200) break;
            PaymentWrapperClass paymentWrapper = buildWrapperForNonFileRequest(payment);
            /*if(payment.Enrollment_Application__c != null && payment.Enrollment_Application_Program__c.contains('Lighting') ){
                if((payment.Enrollment_Application__r.Mailing_Contact_Last_Name__c != null || 
                    payment.Enrollment_Application__r.Mailing_Contact_First_Name__c != null )&&
                    payment.Enrollment_Application__r.Payment_Mailing_Email_Address__c!= null &&
                    payment.Enrollment_Application__r.Mailing_City__c != null && payment.Enrollment_Application__r.Mailing_State__c != null &&
                    payment.Enrollment_Application__r.Mailing_Zip_Code__c != null ){
                     
                    paymentWrappers.add(paymentWrapper);   
                }
            }*/
            //else{
                paymentWrappers.add(paymentWrapper);
            //}
         //   selected_program_name = payment.Enrollment_Application_Program__c;
            if(selected_program_name != null && mapProgramCustom.get(selected_program_name) != null)
                TANameDisplay = true;
        }
        System.debug(LoggingLevel.ERROR, 'paymentWrappers: '+paymentWrappers);
       
    }

    public void set_EnrlApps_stage() {
        
    }
    @TestVisible private void processEnrollmentApplication(){
        
    } 


    @TestVisible private void rejectPayment(){
        
    }
    
    
    
  /*  public void processPayments(String PayIdstr){
        system.debug('--PayIDstr--'+PayIDstr );
        if(PayIDstr != null){
            List<String> PayIds = PayIDstr.split(',');
            
            List<payment__c> lstLightPay = [select id,Name,Payee_Name__c,Payment_Mailing_Address_1__c,Payment_City__c,Payment_State__c,Payment_Postal_Code__c from payment__c where id in : PayIds limit 1];
            system.debug('--lstLightPay--'+lstLightPay.size());
            if(lstLightPay.size() > 0){
                Payment__c pp = lstLightPay[0];
                string blankFields = '';
                if(pp.Payee_Name__c == null)
                    blankFields += 'Name, ';
                if(pp.Payment_Mailing_Address_1__c == null)
                    blankFields += 'Address, ';
                if(pp.Payment_City__c == null)
                    blankFields += 'City, ';
                if(pp.Payment_State__c == null)
                    blankFields += 'State, ';
                if(pp.Payment_Postal_Code__c == null)
                    blankFields += 'Postal Code, ';
                    
                blankFields = blankFields.subString(0,blankFields.Length()-2);
                    
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please fill Payee '+blankFields+' Details for this Payment :- '+lstLightPay[0].Name));    
            }else{
                pollerBool = true; 
              //  dsmtBatchProcessPayments dBp = new dsmtBatchProcessPayments(PayIds);
                //batchId = database.executebatch(dBp,1);
                //checkBatchStatus();
            }
        }
    }
    */
    
    public void checkBatchStatus() {
        
    }
    
    public boolean pageReload{get;set;}
    
    public void check_print_collateral_status(){
       
    }
    
    public void applyPayments(){
        system.debug('--pay_id---'+pay_id);
        /*Boolean proceedWithPaymentApproval = True;
        Id paymentId = pay_id;
        List<Payment__c> paymentList = [select id,Amount__c,File_Request__c from Payment__c where id=: paymentId];
        
        if(paymentList != null && paymentList.size() > 0) {
            Set<Id> frId = new Set<Id>();
            Map<Id,Decimal> payAmtMap = new Map<Id,Decimal>();
            
            for(Payment__c pay : paymentList){
                frId.add(pay.File_Request__c);
                payAmtMap.put(pay.Id,pay.Amount__c);
            }
            
            if(frId != null && frId.size() > 0) {
                Decimal budgetPaidOther = 0.0;
                List<File_Request__c> frList = [select id,Mou__r.Budget_Paid_Appr_Other_Incentives__c from File_Request__c where id in : frId];
                for(File_Request__c fr : frList) {
                    budgetPaidOther = fr.Mou__r.Budget_Paid_Appr_Other_Incentives__c;
                    break;
                }
                if(payAmtMap.get(pay_id) > budgetPaidOther){
                    // Add Confirmation dialog Asking 'Cancel' or 'Approve'
                } else {
                    proceedWithPaymentApproval = True;
                }
            }
        
        }
        
        if(proceedWithPaymentApproval) {*/
         boolean approvepayment = false ;
           
         //String BatchStr = String.valueOf(Date.today().Year())+String.valueOf(Date.today().Month())+String.valueOf(Date.today().Day());
         
         String year = String.valueOf(Date.today().Year());
         String Month = String.valueOf(Date.today().Month());
         String Day = String.valueOf(Date.today().Day());
         
         if(Month != null && Month.length() == 1){
             Month = '0'+Month;
         }
         
         if(Day != null && Day.length() == 1){
             Day = '0'+Day;
         }
         
        String BatchStr = year+Month+Day;
        
        List<Payment__c> payLineList = [select id,Batch__c from Payment__c where Batch__c like : batchStr + '%' /*and Status__c != 'Approved'*/];
        
        String newBatchStr = BatchStr + 'A';
        
        Map<Integer,String> BatchMap = new Map<Integer,String>();
        BatchMap.put(0,'A');BatchMap.put(1,'B');BatchMap.put(2,'C');BatchMap.put(3,'D');BatchMap.put(4,'E');
        BatchMap.put(5,'F');BatchMap.put(6,'G');BatchMap.put(7,'H');BatchMap.put(8,'I');BatchMap.put(9,'J');
        BatchMap.put(10,'K');BatchMap.put(11,'L');BatchMap.put(12,'M');BatchMap.put(13,'N');BatchMap.put(14,'O');
        BatchMap.put(15,'P');BatchMap.put(16,'Q');BatchMap.put(17,'R');BatchMap.put(18,'S');BatchMap.put(19,'T');
        BatchMap.put(20,'U');BatchMap.put(21,'V');BatchMap.put(22,'W');BatchMap.put(23,'X');BatchMap.put(24,'Y');
        BatchMap.put(25,'Z');
        
        
        integer j = 0;
        
        Set<String> paybatch = new Set<String>();
        
        for(Payment__c pay : payLineList){
            paybatch.add(pay.Batch__c);
            //if(BatchStr+BatchMap.get(i) != pay.Batch__c){
              //  i++;
            //}
        }
        for(integer i = 0; i < 26; i++){
            if(paybatch.add(BatchStr+BatchMap.get(i))){
                break;
            }else{
                j++;
            }
        }
        
        system.debug('--BatchStr+BatchMap.get(j)---'+BatchStr+BatchMap.get(j));
        
         List<Approval.Processworkitemrequest> approval_requests = new List<Approval.Processworkitemrequest>();
        
         Set<Id> paymentIds = new Set<Id>();
         
         List<Payment__c> payToApprove = new List<Payment__c>();
         Payment__c newPay = null;
         
         for(String str : pay_id.split(',')){
             paymentIds.add(str);
             newpay = new Payment__c();
             newPay.Batch__c = BatchStr+BatchMap.get(j);
             newpay.Id = str;
             newpay.Status__c = 'Approved';
             payToApprove.add(newpay);
         }
         map<id, ProcessInstance> pimap = processInsMap();
         if(pay_id != null && pay_id != ''){
             for(String str : pay_id.split(',')){
                 // p.Approved_Payment__c = true;
                
                
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                ProcessInstance instList = pimap.get(str);
                if(instList != null){
                    if(instList.StepsAndWorkItems.size() > 0){
                        req.setWorkitemId(instList.StepsAndWorkItems[0].Id);
                        req.setComments('');
                        req.setAction('Approve');
                        approval_requests.add(req);
                    }
                }
            }
        }
        
        if(approval_requests !=null && approval_requests.size()>0){
            if(!Test.isRunningTest()){
                List<Approval.ProcessResult> result = Approval.process(approval_requests);
                system.debug('APPROVAL RESULT: ' + result);
            }
        }
        
        if(payToApprove.size() > 0){
            update payToApprove;
        }
         
           // dsmtHelper.isPaymentProcess = true;
            pageReload  = true;
           // processPayments(pay_id);
        //}
    }
    
      public static map<id, ProcessInstance> processInsMap(){
        map<id, ProcessInstance> pi_map = new map<id, ProcessInstance>();
        list<ProcessInstance> list_pi = [SELECT Id, TargetObjectId, IsDeleted, Status,
                                         (SELECT Id, ProcessInstanceId, ActorId, Actor.Name, StepStatus, Comments
                                          FROM StepsAndWorkItems
                                          WHERE StepStatus = 'Pending' AND IsDeleted = false
                                          ORDER BY CreatedDate ASC LIMIT 1)
                                          FROM ProcessInstance
                                          WHERE IsDeleted = false AND Status = 'Pending'
                                          ORDER BY CreatedDate Asc];
        if(list_pi.size() > 0){
            for(ProcessInstance pi : list_pi){
                pi_map.put(pi.TargetObjectId, pi);
            }
        }
        return pi_map;
    }
    
    public void applyAllPayments(){
         boolean approvepayment = false ;
           
         //String BatchStr = String.valueOf(Date.today().Year())+String.valueOf(Date.today().Month())+String.valueOf(Date.today().Day());
         
         String year = String.valueOf(Date.today().Year());
         String Month = String.valueOf(Date.today().Month());
         String Day = String.valueOf(Date.today().Day());
         
         if(Month != null && Month.length() == 1){
             Month = '0'+Month;
         }
         
         if(Day != null && Day.length() == 1){
             Day = '0'+Day;
         }
         
        String BatchStr = year+Month+Day;
        
        List<Payment__c> payLineList = [select id,Batch__c from Payment__c where Batch__c like : batchStr + '%' /*and Status__c != 'Approved'*/];
        
        String newBatchStr = BatchStr + 'A';
        
        Map<Integer,String> BatchMap = new Map<Integer,String>();
        BatchMap.put(0,'A');BatchMap.put(1,'B');BatchMap.put(2,'C');BatchMap.put(3,'D');BatchMap.put(4,'E');
        BatchMap.put(5,'F');BatchMap.put(6,'G');BatchMap.put(7,'H');BatchMap.put(8,'I');BatchMap.put(9,'J');
        BatchMap.put(10,'K');BatchMap.put(11,'L');BatchMap.put(12,'M');BatchMap.put(13,'N');BatchMap.put(14,'O');
        BatchMap.put(15,'P');BatchMap.put(16,'Q');BatchMap.put(17,'R');BatchMap.put(18,'S');BatchMap.put(19,'T');
        BatchMap.put(20,'U');BatchMap.put(21,'V');BatchMap.put(22,'W');BatchMap.put(23,'X');BatchMap.put(24,'Y');
        BatchMap.put(25,'Z');
        
        
        integer j = 0;
        
        Set<String> paybatch = new Set<String>();
        
        for(Payment__c pay : payLineList){
            paybatch.add(pay.Batch__c);
            //if(BatchStr+BatchMap.get(i) != pay.Batch__c){
              //  i++;
            //}
        }
        for(integer i = 0; i < 26; i++){
            if(paybatch.add(BatchStr+BatchMap.get(i))){
                break;
            }else{
                j++;
            }
        }
        
        system.debug('--BatchStr+BatchMap.get(j)---'+BatchStr+BatchMap.get(j));
        
         List<Approval.Processworkitemrequest> approval_requests = new List<Approval.Processworkitemrequest>();
        
         Set<Id> paymentIds = new Set<Id>();
         
         List<Payment__c> payToApprove = new List<Payment__c>();
         Payment__c newPay = null;
         
         
         for(Payment__c pay : queryPayments()){
             paymentIds.add(pay.Id);
             newpay = new Payment__c();
             newPay.Batch__c = BatchStr+BatchMap.get(j);
             newpay.Id = pay.Id;
             pay.Status__c = 'Approved';
             payToApprove.add(newpay);
         }
         map<id, ProcessInstance> pimap = processInsMap();
         if(paymentIds != null){
             for(String str : paymentIds){
                 // p.Approved_Payment__c = true;
                
                
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                ProcessInstance instList = pimap.get(str);
                if(instList != null){
                    if(instList.StepsAndWorkItems.size() > 0){
                        req.setWorkitemId(instList .StepsAndWorkItems[0].Id);
                        req.setComments('');
                        req.setAction('Approve');
                        approval_requests.add(req);
                    }
                    
                }
                
            }
        }
        
        if(approval_requests !=null && approval_requests.size()>0){
            if(!Test.isRunningTest()){
                List<Approval.ProcessResult> result = Approval.process(approval_requests);
                system.debug('APPROVAL RESULT: ' + result);
            }
        }
        
        if(payToApprove.size() > 0){
            update payToApprove;
        }
         
           // dsmtHelper.isPaymentProcess = true;
            pageReload  = true;
           // processPayments(pay_id);
        //}
    }
        
    public void fetchActiveExceptions(){
       
    }  
    
    public Pagereference voidPayments()
    {
        //String BatchStr = String.valueOf(Date.today().Year())+String.valueOf(Date.today().Month())+String.valueOf(Date.today().Day());
        
        String year = String.valueOf(Date.today().Year());
         String Month = String.valueOf(Date.today().Month());
         String Day = String.valueOf(Date.today().Day());
         
         if(Month != null && Month.length() == 1){
             Month = '0'+Month;
         }
         
         if(Day != null && Day.length() == 1){
             Day = '0'+Day;
         }
         
        String BatchStr = year+Month+Day;
        
        List<Payment__c> payLineList = [select id,Batch__c from Payment__c where Batch__c like : batchStr + '%' and Status__c != 'Approved'];
        
        String newBatchStr = BatchStr + 'A';
        
        Map<Integer,String> BatchMap = new Map<Integer,String>();
        BatchMap.put(0,'A');BatchMap.put(1,'B');BatchMap.put(2,'C');BatchMap.put(3,'D');BatchMap.put(4,'E');
        BatchMap.put(5,'F');BatchMap.put(6,'G');BatchMap.put(7,'H');BatchMap.put(8,'I');BatchMap.put(9,'J');
        BatchMap.put(10,'K');BatchMap.put(11,'L');BatchMap.put(12,'M');BatchMap.put(13,'N');BatchMap.put(14,'O');
        BatchMap.put(15,'P');BatchMap.put(16,'Q');BatchMap.put(17,'R');BatchMap.put(18,'S');BatchMap.put(19,'T');
        BatchMap.put(20,'U');BatchMap.put(21,'V');BatchMap.put(22,'W');BatchMap.put(23,'X');BatchMap.put(24,'Y');
        BatchMap.put(25,'Z');
        
        
        integer j = 0;
        
        Set<String> paybatch = new Set<String>();
        
        for(Payment__c pay : payLineList){
            paybatch.add(pay.Batch__c);
            //if(BatchStr+BatchMap.get(i) != pay.Batch__c){
              //  i++;
            //}
        }
        for(integer i = 0; i < 26; i++){
            if(paybatch.add(BatchStr+BatchMap.get(i))){
                break;
            }else{
                j++;
            }
        }
        
        system.debug('--BatchStr+BatchMap.get(j)---'+BatchStr+BatchMap.get(j));
        
        String payIds = ApexPages.currentPage().getParameters().get('payIds'); // split by ~~~
        String voidcomment = ApexPages.currentPage().getParameters().get('voidcomment');
        
        Set<Id> payId = new Set<Id>();
        for(Id pId : payIds.split('~~~')){
            payId.add(pId);
        }
        
        List<Payment__c> payList = [select id,Review__c from Payment__c where id in : payId];
        
        if(payList != null && payList.size() > 0){
            set<Id> ReviewId = new Set<Id>();
            
            for(Payment__c pay : payList){
                ReviewId.add(pay.Review__c);
            }
         //List<Payment__c> paymentList= [select id,Notes__c,Amount__c,(select id ,Void_Payment__c from Payment_Line_Items__r) From Payment__c where Review__c in :ReviewId];
         
         List<Payment__c> paymentList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Payment__c','Review__c in : ReviewId and Voided_or_Cancelled__c = false and Status__c = \'Approval Pending\'','')); 
         
         List<Payment_Line_Item__c> newpliList = new List<Payment_Line_Item__c>();
         List<Payment__c> newpayment = new List<Payment__c>();
         
         if(paymentList!= null && paymentList.size() > 0){
             
           payId = new Set<Id>();
           
           for(Payment__c pay :paymentList){
               PayId.add(pay.Id);
               
               Payment__c objpay = pay.clone(false,false);
               objpay.Amount__c = pay.Amount__c *-1 ;
               //objpay.Notes__c = voidcomment;
               objPay.Voided_Payment__c = pay.Id;
               objpay.Voided_or_Cancelled__c = true;
               objPay.Itemcode__c = 'CONTR_ADJ';
               newpayment.add(objpay);
               pay.Voided_or_Cancelled__c = true;
               pay.Notes__c = voidcomment;
               pay.Batch__c = BatchStr+BatchMap.get(j);
               objPay.Batch__c = BatchStr+BatchMap.get(j);
              /* for(Payment_Line_Item__c pli :pay.Payment_Line_Items__r){
                pli.Void_Payment__c = true;
                updatepli.add(pli);
               }*/
               
               
           }
           if(newpayment.size() > 0){
                insert newpayment ;
                
                List<Payment_Line_Item__c> paymentLineList = Database.query(dsmtFuture.getCreatableFieldsSOQL('Payment_Line_Item__c','Payment__c in : payId','')); 
                
                Map<Id,Id> newpayMap = new Map<Id,Id>();
                Payment_Line_Item__c newpli = null;
                
                for(Payment__c p : newpayment){
                    newPayMap.put(p.Voided_Payment__c,p.Id);
                }
                
                if(paymentLineList != null && paymentLineList.size() > 0){
                    for(Payment_Line_Item__c pli : paymentLineList){
                        pli.Void_Payment__c = true;
                        newpli = new Payment_Line_Item__c();
                        newpli = pli.clone(false,false);
                        newpli.Itemcode__c = 'CONTR_ADJ';
                        newpli.Void_Payment__c = true;
                        if(pli.Amount__c != null){
                            newpli.Amount__c = pli.Amount__c * -1;
                        }
                        newpli.Payment__c = newPayMap.get(pli.Payment__c);
                        pli.Batch__c = BatchStr+BatchMap.get(j);
                        newpli.Batch__c = BatchStr+BatchMap.get(j);
                        newpliList.add(newpli);
                    }
                    update paymentLineList;
                }
           }
           if(newpliList != null && newpliList.size() >0) {
               insert newpliList ;
            }
            update paymentList;
          }
        }
        return new Pagereference('/apex/dsmtPaymentApprovalQueue');
    }  

    public class PaymentWrapperClass {
        public Boolean is_selected {get;set;}
        public String customerName {get;set;}
        public String enrollmentApplicationId {get;set;}
        public String mailing_contact{get;set;}
        public String eanumber{get;set;}
        public String ContractorName{get;set;}
        public String itemcode{get;set;}
        public String DistributionAmount{get;set;}
        public String ContractorInvoice{get;set;}
        public String Cidate{get;set;}
        public String Siteid{get;set;}
        public String notes{get;set;}
        public String pcall{get;set;}
        public String programId {get; set;}
        public Payment__c payment {get;set;}
        public Integer numberOfExceptions{get;set;}
        public Decimal amount{get;set;}
        public Decimal budgetPaidApprOtherincentive{get;set;}
        public boolean VoidPayment{get;set;}
        public string batchNumber{get;set;}
        public string ReviewNumber{get;set;}
        public string ReviewId{get;set;}
        
        public PaymentWrapperClass() {
            is_selected = false;
        }
    }
}