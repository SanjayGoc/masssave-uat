public class RecommendationScenarioTriggerHandler extends TriggerHandler{
    public RSModel model{get;set;}
    public RecommendationScenarioTriggerHandler(){
        this.model = new RSModel();
    }
    
    public override void afterInsert(){
    	createAllRecords();    
    }
	public override void afterUpdate(){
    	createAllRecords();    
    }    
    
    private void createAllRecords(){
        Map<String, Enrollment_Application__c> mapRecSerIdToEA = new Map<String, Enrollment_Application__c>();
        for(Enrollment_Application__c ea:[SELECT Id, Name FROM Enrollment_Application__c WHERE Recommendation_Scenario__c IN: Trigger.NewMap.keySet()]){
            mapRecSerIdToEA.put(ea.Recommendation_Scenario__c, ea);
        }
        List<Recommendation_Scenario__c> lstRS = new List<Recommendation_Scenario__c>();
        List<String> lstEnegryAssesmentId = new List<String>();
        for(Recommendation_Scenario__c rs: (List<Recommendation_Scenario__c>)Trigger.New){
            if(rs.Status__c == 'Installed' && !mapRecSerIdToEA.containsKey(rs.Id)){
                lstRS.add(rs);
                lstEnegryAssesmentId.add(rs.Energy_Assessment__c);
            }
        } 
        this.model.lstRS = lstRS;
        List<String> woId = new List<String>();
        List<String> apptId = new List<String>();
        Map<String, String> mapEAToWO = new Map<String, String>();  //Recommendation Scenario ---> Energy Assessment ---> Workorder
        Map<String, String> mapEAToAppt = new Map<String, String>();  //Recommendation Scenario ---> Energy Assessment ---> Appointment
        for(Energy_Assessment__c ea : [SELECT Id, Name, Workorder__c, Appointment__c FROM Energy_Assessment__c WHERE Id IN:lstEnegryAssesmentId]){
            woId.add(ea.Workorder__c);
            apptId.add(ea.Appointment__c);
            mapEAToWO.put(ea.Id, ea.Workorder__c);
            mapEAToAppt.put(ea.Id, ea.Appointment__c);
        }
        this.model.mapEAToWO = mapEAToWO;
        this.model.mapEAToAppt = mapEAToAppt;
        Map<Id, Appointment__c> mapApptIdAppt = new Map<Id, Appointment__c>([
            SELECT Id, Name, Customer_Reference__c,Workorder__c,
            Customer_Reference__r.Service_Address__c, Customer_Reference__r.Service_State__c, Customer_Reference__r.Service_Zipcode__c, 
            Customer_Reference__r.Service_City__c, Customer_Reference__r.Electric_Account__c, Customer_Reference__r.Email__c, Customer_Reference__r.Phone__c,
            Customer_Reference__r.Gas_Account__c, Customer_Reference__r.Electric_Account_Number__c, Customer_Reference__r.Gas_Account_Number__c,
            Customer_Reference__r.First_Name__c, Customer_Reference__r.Last_Name__c,Customer_Reference__r.Electric_Provider_Name__c, Customer_Reference__r.Gas_Provider_Name__c,
            Trade_Ally_Account__c, Trade_Ally_Account__r.Street_Address__c, Trade_Ally_Account__r.Street_State__c, Trade_Ally_Account__r.Street_City__c, 
            Trade_Ally_Account__r.Street_Zip__c, Trade_Ally_Account__r.Mailing_Address__c, Trade_Ally_Account__r.Mailing_City__c, Trade_Ally_Account__r.Mailing_State__c, 
            Trade_Ally_Account__r.Mailing_Zip__c, Trade_Ally_Account__r.Phone__c, Trade_Ally_Account__r.Email__c,
            Trade_Ally_Account__r.First_Name__c, Trade_Ally_Account__r.Last_Name__c,Trade_Ally_Account__r.Account__c 
            FROM Appointment__c 
        	WHERE (Workorder__c IN:woId AND Workorder__c!='') OR Id IN:apptId  
            LIMIT 100
        ]);
        List<Appointment__c> lstAppts = mapApptIdAppt.values();
        
        System.debug('lstAppts--> '+JSON.serialize(lstAppts));
        Map<String, Appointment__c> mapWOToApptObj = new Map<String, Appointment__c>(); // mapEAToWO ---> Workorder ---> Appointment
        List<String> custIds = new List<String>();
        List<String> taIds = new List<String>();
        for(Appointment__c appt: lstAppts){
            mapWOToApptObj.put(appt.Workorder__c, appt);
            custIds.add(appt.Customer_Reference__c);
            taIds.add(appt.Trade_Ally_Account__c);
        }
        this.model.mapApptIdAppt = mapApptIdAppt;
        this.model.mapWOToApptObj = mapWOToApptObj;
        List<DSMTracker_Contact__c> lstDSMTracContact = [
            SELECT Id, Name, Account__c, Contact__c, Customer__c, Trade_Ally_Account__c, 
            First_Name__c, Last_Name__c, Address__c, City__c, State__c, Zip__c, Phone__c, Email__c
            FROM DSMTracker_Contact__c 
            WHERE (Trade_Ally_Account__c IN:taIds AND Trade_Ally_Account__c!='')
            OR (Customer__c IN:custIds AND Customer__c !='')
        	LIMIT 100
        ];
        System.debug('lstDSMTracContact--> '+JSON.serialize(lstDSMTracContact));
        Map<String, DSMTracker_Contact__c> mapCustIdToDC = new Map<String, DSMTracker_Contact__c>(); 
        Map<String, DSMTracker_Contact__c> mapTAIdToDC = new Map<String, DSMTracker_Contact__c>(); 
        
        for(DSMTracker_Contact__c dc: lstDSMTracContact){
            mapCustIdToDC.put(dc.Customer__c, dc);
            mapTAIdToDC.put(dc.Trade_Ally_Account__c, dc);
        }
        System.debug('lstDSMTracContact--> '+mapCustIdToDC);
        System.debug('lstDSMTracContact--> '+mapTAIdToDC);
        this.model.mapCustIdToDC = mapCustIdToDC;
        this.model.mapTAIdToDC = mapTAIdToDC;
        List<Enrollment_Application__c> lstEnrollmentApplication = RecommendationScenarioTriggerUtil.fillEnrollmentApplication(this.model);
        if(lstEnrollmentApplication.size()>0){
            INSERT lstEnrollmentApplication;
           
            System.debug('lstEnrollmentApplication---> '+JSON.serialize(lstEnrollmentApplication));
        }
    }
    
    public class RSModel{
        public List<Recommendation_Scenario__c> lstRS{get;set;}
        public Map<Id, Appointment__c> mapApptIdAppt{get;set;}
        public Map<String, String> mapEAToWO{get;set;}
        public Map<String, String> mapEAToAppt{get;set;}
        public Map<String, Appointment__c> mapWOToApptObj{get;set;}
        public Map<String, DSMTracker_Contact__c> mapCustIdToDC{get;set;}
        public Map<String, DSMTracker_Contact__c> mapTAIdToDC{get;set;}
        public RSModel(){
            this.lstRS = new List<Recommendation_Scenario__c>();
            this.mapApptIdAppt = new Map<Id, Appointment__c>(); 
            this.mapEAToWO = new Map<String, String>();
            this.mapEAToAppt = new Map<String, String>();
            this.mapWOToApptObj = new Map<String, Appointment__c>();
            this.mapCustIdToDC = new Map<String, DSMTracker_Contact__c>(); 
            this.mapTAIdToDC = new Map<String, DSMTracker_Contact__c>(); 
        }
    }
}