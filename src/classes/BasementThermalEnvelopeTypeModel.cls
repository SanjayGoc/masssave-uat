public with sharing class BasementThermalEnvelopeTypeModel{
    public String visiblePanel{get;set;}
    public Thermal_Envelope_Type__c envelopeType{get;private set;}
    public String fieldUniqueDataId{get;set;}
    
    public BasementThermalEnvelopeTypeModel(Thermal_Envelope_Type__c envelopeType){
        setEnvelopeType(envelopeType);
        updateBasementChildComponents();
        refreshBasementPanel();
    }
    public void setEnvelopeType(Thermal_Envelope_Type__c envelopeType){
        this.envelopeType = envelopeType.clone(true);
    }
    
    public void refreshBasementPanel(){
        System.debug('VISIBLE PANEL : ' + visiblePanel);
        queryBasementWalls();
        queryBasementCeilings();
        queryBasementFloors();
    }
    
    public void updateBasementChildComponents(){
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeFloors(envelopeType.Id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeWalls(envelopeType.Id);
        CalculationTriggerHelper.updateAndReCalculateThermalEnvelopeTypeRoofs(envelopeType.Id);
    }
    
    /* BASEMENT CEILINGS -------------------------------------------------------------------------------------------------------*/
    public Ceiling__c editBasementCeiling{get;set;}
    public transient Ceiling__c newBasementCeiling{get;set;}
    public transient List<Ceiling__c> basementCeilings{get;set;}
    public void queryBasementCeilings(){
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'BasementHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            basementCeilings = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            basementCeilings = new List<Ceiling__c>();
        }
    }
    public void addNewBasementCeiling(String ceilingType ,String exposedTo){
        if(envelopeType != null && envelopeType.Id != null){
            // DSST-12295 - START PP 
            Decimal nextNumber = (envelopeType.Next_Basement_Ceiling_Number__c != null) ? envelopeType.Next_Basement_Ceiling_Number__c : 2;
            // DSST-12295 - END PP
            
            newBasementCeiling= new Ceiling__c();
            newBasementCeiling.Thermal_Envelope_Type__c = envelopeType.Id;
            newBasementCeiling.Name = 'Basement Ceiling ' + nextNumber;
            newBasementCeiling.Exposed_To__c = exposedTo;
            newBasementCeiling.Add__c = ceilingType; 
               
            INSERT newBasementCeiling;
            if(newBasementCeiling.Id != null){
                envelopeType.Next_Basement_Ceiling_Number__c = nextNumber + 1;
                UPDATE envelopeType;
                if(updateBasementCeilingById(newBasementCeiling.Id) != null){
                    queryBasementCeilingById(newBasementCeiling.Id);
                }
            }
        }
        refreshBasementPanel();
    }
    public Ceiling__c updateBasementCeilingById(String ceilingId){
        if(ceilingId != null){
            Ceiling__c ceiling= new Ceiling__c(Id = ceilingId);
            UPDATE [SELECT Id FROM Layer__c WHERE Ceiling__c =:ceiling.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE ceiling;
            return ceiling;
        }
        return null;
    }
    public void queryBasementCeilingById(String ceilingId){
        if(ceilingId != null){
            List<Ceiling__c> ceilings = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Id=:ceilingId');
            if(ceilings.size() > 0){
                editBasementCeiling = ceilings.get(0);
                visiblePanel = 'BasementCeilingPanel';
            }
        }
    }
    public void deleteBasementCeilingById(String ceilingId){
        if(ceilingId != null){
            DELETE new Ceiling__c(Id = ceilingId);
        }
        refreshBasementPanel();
    }
    
    /* BASEMENT WALLS -------------------------------------------------------------------------------------------------------*/
    public Wall__c editBasementWall{get;set;}
    public transient Wall__c newBasementWall{get;set;}
    public transient List<Wall__c> basementWalls{get;set;}
    public void queryBasementWalls(){
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'BasementHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            basementWalls = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            basementWalls = new List<Wall__c>();
        }
    }
    public void addNewBasementWall(String wallType, String exposedTo){
        if(envelopeType != null && envelopeType.Id != null){
            // DSST-12295 -  STRAT PP
            Decimal nextNumber = (envelopeType.Next_Basement_Wall_Number__c != null) ? envelopeType.Next_Basement_Wall_Number__c : 2;
            // DSST-12295 - END PP
            newBasementWall= new Wall__c();
            newBasementWall.Thermal_Envelope_Type__c = envelopeType.Id;
            newBasementWall.Name = wallType + ' ' + nextNumber;
            newBasementWall.Exposed_To__c = exposedTo;
            newBasementWall.Add__c = wallType; 
            newBasementWall.Type__c = wallType; 
            newBasementWall.Insul_Amount__c = 'Standard';
               
            INSERT newBasementWall;
            if(newBasementWall.Id != null){
                envelopeType.Next_Basement_Wall_Number__c = nextNumber + 1;
                UPDATE envelopeType;
                if(updateBasementWallById(newBasementWall.Id) != null){
                    queryBasementWallById(newBasementWall.Id);
                }
            }
        }
        refreshBasementPanel();
    }
    public Wall__c updateBasementWallById(String wallId){
        if(wallId != null){
            Wall__c wall = new Wall__c(Id = wallId);
            UPDATE [SELECT Id FROM Layer__c WHERE Wall__c =:wall.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE wall;
            return wall;
        }
        return null;
    }
    public void queryBasementWallById(String wallId){
        if(wallId != null){
            List<Wall__c> walls = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Wall__c') + ' FROM Wall__c WHERE Id=:wallId');
            if(walls.size() > 0){
                editBasementWall = walls.get(0);
                visiblePanel = 'BasementWallPanel';
            }
        }
    }
    public void deleteBasementWallById(String wallId){
        if(wallId != null){
            DELETE new Wall__c(Id = wallId);
        }
        refreshBasementPanel();
    }
    
    /* BASEMENT FLOORS -------------------------------------------------------------------------------------------------------*/
    public Floor__c editBasementFloor{get;set;}
    public transient Floor__c newBasementFloor{get;set;}
    public transient List<Floor__c> basementFloors{get;set;}
    public void queryBasementFloors(){
        if(envelopeType != null && envelopeType.Id != null && (visiblePanel == null || visiblePanel == '' || visiblePanel == 'BasementHomePanel')){
            Id thermalEnvelopeTypeId = envelopeType.Id;
            basementFloors = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Floor__c') + ' FROM Floor__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            basementFloors = new List<Floor__c>();
        }
    }
    public void addNewBasementFloor(String floorType ,String exposedTo){
        if(envelopeType != null && envelopeType.Id != null){
            // DSST-12295 - START PP
            Decimal nextNumber = (envelopeType.Next_Basement_Floor_Number__c != null) ? envelopeType.Next_Basement_Floor_Number__c : 2;
            // DSST-12295 - END PP
            
            newBasementFloor= new Floor__c();
            newBasementFloor.Thermal_Envelope_Type__c = envelopeType.Id;
            newBasementFloor.Name = 'Basement Floor ' + nextNumber;
            newBasementFloor.Exposed_To__c = exposedTo;
            newBasementFloor.Add__c = floorType; 
               
            INSERT newBasementFloor;
            if(newBasementFloor.Id != null){
                envelopeType.Next_Basement_Floor_Number__c = nextNumber + 1;
                UPDATE envelopeType;
                if(updateBasementFloorById(newBasementFloor.Id) != null){
                    queryBasementFloorById(newBasementFloor.Id);
                }
            }
        }
        refreshBasementPanel();
    }
    public Floor__c updateBasementFloorById(String floorId){
        if(floorId != null){
            Floor__c floor = new Floor__c(Id = floorId);
            UPDATE [SELECT Id FROM Layer__c WHERE Floor__c =:floor.Id AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            UPDATE floor;
            return floor;
        }
        return null;
    }
    public void queryBasementFloorById(String floorId){
        if(floorId != null){
            List<Floor__c> floors = Database.query('SELECT '+ dsmtEnergyAssessmentBaseController.getSObjectFields('Floor__c') + ' FROM Floor__c WHERE Id=:floorId');
            if(floors.size() > 0){
                editBasementFloor = floors.get(0);
                visiblePanel = 'BasementFloorPanel';
            }
        }
    }
    public void deleteBasementFloorById(String floorId){
        if(floorId != null){
            DELETE new Floor__c(Id = floorId);
        }
        refreshBasementPanel();
    }
    
    /*-------------------------------------------------------------------------------------------------------------------------*/
    public Component.Apex.OutputPanel getDynamicComponent(){
        Component.Apex.OutputPanel panel = new Component.Apex.OutputPanel();
        panel.layout = 'block';
        if(visiblePanel == 'BasementWallPanel'){
            Component.c.BasementWallComponent wall = new Component.c.BasementWallComponent();
            wall.basementWallObjId = editBasementWall.Id;
            wall.onSave = 'showBasementPanel("BasementHomePanel");';
            panel.childComponents.add(wall);
        }else if(visiblePanel == 'BasementCeilingPanel'){
            Component.c.BasementCeilingComponent ceiling = new Component.c.BasementCeilingComponent();
            ceiling.basementCeilingObjId = editBasementCeiling.Id;
            ceiling.onSave = 'showBasementPanel("BasementHomePanel");';
            panel.childComponents.add(ceiling);
        }else if(visiblePanel == 'BasementFloorPanel'){
            Component.c.BasementFloorComponent floor = new Component.c.BasementFloorComponent();
            floor.basementFloorObjId = editBasementFloor.Id;
            floor.onSave = 'showBasementPanel("BasementHomePanel");';
            panel.childComponents.add(floor);
        }else{
            panel.rendered=false;
        }
        return panel;
    }
    
    public void saveDynamicComponent(){
        if(visiblePanel == 'BasementWallPanel'){
            UPSERT editBasementWall;
        }else if(visiblePanel == 'BasementCeilingPanel'){
            UPSERT editBasementCeiling;
        }else if(visiblePanel == 'BasementFloorPanel'){
            UPSERT editBasementFloor;
        }
    }
}