@isTest
public class VoidCancelReviewtest
{
    public testmethod static void test1()
    {        
        Review__c re = new Review__c();
        re.Status__c = 'Batched';
        re.Installed_Date__c=date.today()+1;
        insert re;
        
        Payment__c pt = new Payment__c();
        pt.Review__c =re.id;
        pt.Batch__c = 'test';
        pt.Status__c ='Batched';
        pt.Amount__c =1; 
        insert pt;
        
        Payment_Line_Item__c plt = new Payment_Line_Item__c();
        plt.Payment__c = pt.id;
        plt.Amount__c=10;
        insert plt;

        Workorder_Type__c wotp = new Workorder_Type__c();
        wotp.Name='test';
        insert wotp;
        
        Workorder__c wo = new Workorder__c();
        wo.Early_Arrival_Time__c= datetime.newInstance(2014, 9, 15, 12, 30, 0);
        wo.Late_Arrival_Time__c= datetime.newInstance(2014, 9, 15, 12, 30, 0);
        wo.Scheduled_Date__c=date.today();
        wo.Workorder_Type__c=wotp.id;
        insert wo;

        Invoice__c inv = new Invoice__c();
        inv.Status__c = 'UnSubmitted';
        inv.Invoice_Type__c ='NSTAR413';
        inv.Invoice_Date__c=date.today()+1;
        insert inv;
        
        Invoice_Line_Item__c ilt = new Invoice_Line_Item__c();
        ilt.Review__c =re.id;
        ilt.Invoice__c = inv.id;
        ilt.Qty__c = 1;
        ilt.Workorder__c = wo.id;
        ilt.Calculated_Incentive_Amount__c=5;
        insert ilt;
        
        Recommendation_Scenario__c pro = new Recommendation_Scenario__c();
        pro.Type__c = 'Billing Review';
        insert pro;       
        
        Project_Review__c  pr = new Project_Review__c();
        pr.Review__c = re.id;
        pr.Project__c = pro.id;
        insert pr;
        
        Recommendation__c rcm = new Recommendation__c();
        rcm.Review__c =re.id;
        rcm.Recommendation_Scenario__c = pro.id;
        rcm.Invoice_Type__c ='NSTAR413';
        rcm.Category__c ='test';
        rcm.Installed_Date__c=date.today()-2;
        insert rcm;
        
        Dsmtracker_Product__c dsmp = new Dsmtracker_Product__c();
        dsmp.Eversource_Material_Price__c=51;
        dsmp.EMHome_EMHub_PartID__c='';
        insert dsmp;
        test.startTest();
        Appointment__c apt = new Appointment__c();
        insert apt;
        
        Workorder_Type_Invoice_Item_Setting__c wtis =new Workorder_Type_Invoice_Item_Setting__c();
        wtis.Name__c='test';
        wtis.Workorder_Type__c='test';//wotp.id;
        wtis.Part_Id__c='test';
        wtis.Is_not_CLEAResult__c=true;
        wtis.Name='test';
        insert wtis;
        
        VoidCancelReview cntrl = new VoidCancelReview();
        VoidCancelReview.voidPayments(re.id);
        VoidCancelReview.voidInvoiceLineItems(re.id);
        VoidCancelReview.voidAppointmentInvoiceLineItems(wo.id);
        VoidCancelReview.InsertInvoiceLineItemWhenStatusBatched(re.id);
        VoidCancelReview.InsertInvoiceLineItemWhenStatusBatchedWO(wo.id);
        test.stopTest();
    }
    public testmethod static void runtest2()
    {        
        Review__c re = new Review__c();
        re.Status__c = 'Batched';
        re.Installed_Date__c=date.today()+1;
        insert re;
        
       /* Payment__c pt = new Payment__c();
        pt.Review__c =re.id;
        pt.Batch__c = 'test';
        pt.Status__c ='Batched';
        pt.Amount__c =1; 
        insert pt;
        
        Payment_Line_Item__c plt = new Payment_Line_Item__c();
        plt.Payment__c = pt.id;
        insert plt;*/

        /*Invoice__c inv = new Invoice__c();
        inv.Status__c = 'UnSubmitted';
        inv.Invoice_Type__c ='NSTAR418';
        inv.Invoice_Date__c=date.today()+1;
        insert inv;*/
        /**Invoice_Line_Item__c ilt = new Invoice_Line_Item__c();
        ilt.Review__c =re.id;
        ilt.Invoice__c = inv.id;
        ilt.Qty__c = 1;
        ilt.Workorder__c = wo.id;
        ilt.Calculated_Incentive_Amount__c=5;
        insert ilt;*/

        Recommendation_Scenario__c pro = new Recommendation_Scenario__c();
        pro.Type__c = 'Billing Review';
        insert pro;

        Project_Review__c  pr = new Project_Review__c();
        pr.Review__c = re.id;
        pr.Project__c = pro.id;
        insert pr;
        Dsmtracker_Product__c dsmp = new Dsmtracker_Product__c();
        dsmp.Eversource_Material_Price__c=51;
        dsmp.EMHome_EMHub_PartID__c='';
        insert dsmp;
        
        Recommendation__c rcm = new Recommendation__c();
        rcm.Review__c =re.id;
        rcm.Recommendation_Scenario__c = pro.id;
        rcm.Invoice_Type__c ='NSTAR413';
        rcm.Category__c ='test';
        rcm.Installed_Date__c=date.today()-2;
        rcm.DSMTracker_Product__c=dsmp.id;
        insert rcm;

        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        //ta.Trade_Ally_Type__c = 'HPC';
        ta.Internal_Account__c=true;
        insert ta;
        test.startTest();
        Workorder_Type__c wotp = new Workorder_Type__c();
        wotp.Name='test';
        insert wotp;
        
        Workorder__c wo = new Workorder__c();
        wo.Early_Arrival_Time__c= datetime.newInstance(2014, 9, 15, 12, 30, 0);
        wo.Late_Arrival_Time__c= datetime.newInstance(2014, 9, 15, 12, 30, 0);
        wo.Scheduled_Date__c=date.today();
        wo.Workorder_Type__c=wotp.id;
        wo.Primary_Provider__c = 'Eversource East Gas';
        wo.Invoice_Type__c = 'Master (default)';
        wo.Status__c = 'Completed';
        insert wo;
        
        Appointment__c apt = new Appointment__c();
        apt.Workorder__c=wo.id;
        apt.Workorder_Type__c=wotp.id;
        apt.Trade_Ally_Account__c=ta.id;
        insert apt;

        Workorder_Type_Invoice_Item_Setting__c wtis =new Workorder_Type_Invoice_Item_Setting__c();
        wtis.Name__c='test';
        wtis.Workorder_Type__c='test';//wotp.id;
        wtis.Part_Id__c='test';
        wtis.Is_not_CLEAResult__c=true;
        wtis.Name='test';
        wtis.Part_Id__c = 'TRGH';
        insert wtis;
        
        Invoice__c inv = new Invoice__c();
        inv.Status__c = 'Unsubmitted';
        inv.Invoice_Type__c ='NSTAR418';
        inv.Invoice_Date__c=date.today().addDays(-1);
        insert inv;
        
        Invoice_Line_Item__c ilt = new Invoice_Line_Item__c();
        ilt.Review__c =re.id;
        ilt.Invoice__c =inv.id;
        ilt.Qty__c = 1;
        ilt.Workorder__c = wo.id;
        ilt.Calculated_Incentive_Amount__c=5;
        insert ilt;
        
        Dsmtracker_Product__c p = new Dsmtracker_Product__c();
        p.name = 'test';
        p.Eversource_Labor_Price__c = 12;
        p.EMHome_EMHub_PartID__c = 'TRGH';
        insert p;
        
        VoidCancelReview cntrl = new VoidCancelReview();
        //VoidCancelReview.voidPayments(re.id);
        //VoidCancelReview.voidInvoiceLineItems(re.id);
        //VoidCancelReview.voidAppointmentInvoiceLineItems(wo.id);
        VoidCancelReview.InsertInvoiceLineItemWhenStatusBatched(re.id);
        VoidCancelReview.InsertInvoiceLineItemWhenStatusBatchedWO(wo.id);
        test.stopTest();
    }
    
}