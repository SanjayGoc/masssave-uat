@istest
public class TradeAllyAccountTriggerHandlerTest 
{
    @istest
    static void test()
    {
      
        Trade_Ally_Account__c taa=new Trade_Ally_Account__c();
        taa.Name='testing';
        insert taa;
        
        Group g =new Group();
        g.Name='testing Queue';
        g.DeveloperName='';
        g.Type='Queue';
        insert g;
        
        
        //Trade_Ally_Account__c taa1=new Trade_Ally_Account__c();
        taa.Name='testingnew';
        
        update taa;
        
        
        
        List<Trade_Ally_Account__c> newList = new List<Trade_Ally_Account__c>();
        Map<Id,Trade_Ally_Account__c> oldMap = new Map<Id,Trade_Ally_Account__c>();
        Set<Id> groupIds = new Set<Id>();
        groupIds.add(g.id);
        Map<String,String> trNamesMap = new Map<String,String>();
        trNamesMap.put('test','testing');
        TradeAllyAccountTriggerHandler tath=new TradeAllyAccountTriggerHandler();
        TradeAllyAccountTriggerHandler.AfterInsertUpdate(newList, true, oldMap);
        TradeAllyAccountTriggerHandler.createQueue(groupIds);
    }
    
    /*@istest
    static void test1()
    {
      
        Trade_Ally_Account__c taa=new Trade_Ally_Account__c();
        taa.Name='testing';
        insert taa;
        
        Group g =new Group();
        g.Name='test group';
        g.DeveloperName='';
        g.Type='Queue';
        insert g;
        
        
        //Trade_Ally_Account__c taa1=new Trade_Ally_Account__c();
        taa.Name='testingnew';
        
        update taa;
        
        
        
        List<Trade_Ally_Account__c> newList = new List<Trade_Ally_Account__c>();
        Map<Id,Trade_Ally_Account__c> oldMap = new Map<Id,Trade_Ally_Account__c>();
        Set<Id> groupIds = new Set<Id>();
        groupIds.add(g.id);
        Map<String,String> trNamesMap = new Map<String,String>();
        trNamesMap.put('test','testing');
        TradeAllyAccountTriggerHandler tath=new TradeAllyAccountTriggerHandler();
        TradeAllyAccountTriggerHandler.AfterInsertUpdate(newList, true, oldMap);
        TradeAllyAccountTriggerHandler.createQueue(groupIds);
    }
    */
    @istest
    static void test3()
    {
      
        Trade_Ally_Account__c taa=new Trade_Ally_Account__c();
        taa.Name='testing';
        insert taa;
        
        Group g =new Group();
        g.Name='testing';
        g.DeveloperName='';
        g.Type='Queue';
        insert g;
        
        
        //Trade_Ally_Account__c taa1=new Trade_Ally_Account__c();
        taa.Name='testingnew';
        
        update taa;
        
        
        
        List<Trade_Ally_Account__c> newList = new List<Trade_Ally_Account__c>();
        Map<Id,Trade_Ally_Account__c> oldMap = new Map<Id,Trade_Ally_Account__c>();
        Set<Id> groupIds = new Set<Id>();
        groupIds.add(g.id);
        Map<String,String> trNamesMap = new Map<String,String>();
        trNamesMap.put('test','testing');
        TradeAllyAccountTriggerHandler tath=new TradeAllyAccountTriggerHandler();
        TradeAllyAccountTriggerHandler.AfterInsertUpdate(newList, true, oldMap);
        TradeAllyAccountTriggerHandler.createQueue(groupIds);
    }
}