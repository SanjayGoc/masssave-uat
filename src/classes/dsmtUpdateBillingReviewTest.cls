@istest
public class dsmtUpdateBillingReviewTest 
{
	@istest
    static void test()
    {
        Inspection_Request__c ir= new Inspection_Request__c();
        insert ir;
        
        Review__c rv=new Review__c();
        rv.Billing_Adjustment_Requested_Date__c = date.today();
        rv.Billing_Adjustment_Requested__c= true;
        insert rv;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ir);
        dsmtUpdateBillingReview test = new dsmtUpdateBillingReview(sc);
        
        dsmtUpdateBillingReview dubr =new dsmtUpdateBillingReview(sc);
        dubr.updateReview();
        
    }
}