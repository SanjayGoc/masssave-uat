@isTest 
private class dsmtCustomerAcceptanceCntrlTest{
    static testMethod void test1() {
    
        Recommendation_Scenario__c recSce = new Recommendation_Scenario__c();
        recSce.Status__c = 'Pending Customer Acceptance';
        insert recSce;
        
        Proposal__c prop = new Proposal__c();
        insert prop;
        
        Checklist__c chk = new Checklist__c(Reference_ID__c = 'chk001');
        chk.Unique_Name__c = 'Customer_Acceptance_Form';
        insert chk;
        
        Checklist_Items__c chkItem = new Checklist_Items__c(Reference_ID__c = 'chkitem001', Checklist__c = chk.id, Help_text_for_public_portal__c = false, Special_instruction_for_public_portal__c = false);
        insert chkItem;
        
        Attachment__c att = new Attachment__c();
        att.Project__c = recSce.Id;
        att.attachment_Type__c='Contract Document';
        att.status__c = 'Completed';
        att.Proposal__c = prop.Id;
        insert att;
        
        Attachment__c att1 = new Attachment__c();
        att1.Project__c = recSce.Id;
        att1.attachment_Type__c='Contract Document';
        att1.status__c = 'Completed';
        att1.Proposal__c = prop.Id;
        insert att1;
        
        Login_Detail__c loginDetail = new Login_Detail__c();
        insert loginDetail;
        
        ApexPages.currentPage().getParameters().put('id',recSce.Id);
        ApexPages.currentPage().getParameters().put('propid',prop.Id);
        ApexPages.currentPage().getParameters().put('att1id',att1.Id);
        
        dsmtCustomerAcceptanceCntrl ctrl = new dsmtCustomerAcceptanceCntrl();
        ctrl.projectId = recSce.Id;
        dsmtCustomerAcceptanceCntrl.saveSign(String.valueOf(recSce.Id),'test','imgURI','Acceptance','Recommendation_Scenario__c');
        dsmtCustomerAcceptanceCntrl.saveSign(String.valueOf(prop.Id),'test','imgURI','Acceptance','Proposal__c');
        ctrl.getdataForContractDocument();
        ctrl.attachmentId = att.Id;
        ctrl.checkCongaStatus();
        ctrl.CreateReview();
        ctrl.getdataForInspectionReportDocument();
        ctrl.getdataForProposalContractDocument();
        
        
    }
   /*  static testMethod void test2() {
    
        Recommendation_Scenario__c re = new Recommendation_Scenario__c();
        re.Status__c = 'Pending Customer Acceptance';
        insert re;
        
        Proposal__c pro= new Proposal__c();
        insert pro;
        
        Checklist__c ch = new Checklist__c(Reference_ID__c = 'chk001');
        ch.Unique_Name__c = 'Customer_Acceptance_Form';
        
        insert ch;
        
        Checklist_Items__c chk = new Checklist_Items__c(Reference_ID__c = 'chkitem001', Checklist__c = ch.id, Help_text_for_public_portal__c = false, Special_instruction_for_public_portal__c = false);
        insert chk;
        
        Attachment__c at = new Attachment__c();
        at.Project__c = re.Id;
        at.attachment_Type__c='Contract Document';
        at.status__c = 'Completed';
        at.Proposal__c = pro.Id;
        insert at;
        
        Attachment__c att = new Attachment__c();
        att.Project__c = re.Id;
        att.attachment_Type__c='Contract Document';
        att.status__c = 'Completed';
        att.Proposal__c = pro.Id;
        insert att;
        
        Login_Detail__c log = new Login_Detail__c();
        insert log;
        
        ApexPages.currentPage().getParameters().put('id',re.Id);
        ApexPages.currentPage().getParameters().put('proid',pro.Id);
        
        dsmtCustomerAcceptanceCntrl ctrl = new dsmtCustomerAcceptanceCntrl();
        ctrl.projectId = re.Id;
        dsmtCustomerAcceptanceCntrl.saveSign(String.valueOf(re.Id),'test','imgURI','Acceptance','Recommendation_Scenario__c');
        dsmtCustomerAcceptanceCntrl.saveSign(String.valueOf(pro.Id),'test','imgURI','Acceptance','Proposal__c');
        ctrl.getdataForContractDocument();
        ctrl.attachmentId = at.Id;
        ctrl.checkCongaStatus();
        ctrl.CreateReview();
        ctrl.getdataForInspectionReportDocument();
}*/
}