public with sharing class dsmtUpdateBillingReview {
    public final Inspection_Request__c IR;
    public dsmtUpdateBillingReview(ApexPages.StandardController stdController) {
        this.IR = (Inspection_Request__c)stdController.getRecord();
    }
    
    public PageReference updateReview() {
    
        //check if the user belongs to Inspection Manager Review
      //  try{
            Inspection_Request__c irq = [select id, Review__c from Inspection_Request__c where id =: IR.id];
            List<Review__c> revList = [select id, Billing_Adjustment_Requested_Date__c, Billing_Adjustment_Requested__c from Review__c where id =: irq.Review__c];
            
            if(revList  != null && revList.size() > 0){
                revList.get(0).Billing_Adjustment_Requested_Date__c = Date.today();
                revList.get(0).Billing_Adjustment_Requested__c = true;
                update revList ;
            }
            // Redirect the user back to the original page
            PageReference pageRef = new PageReference('/' + IR.id);
            pageRef.setRedirect(true);
            return pageRef;
       /* }catch(Exception ex){
            //dev console was not workin so had this to check for errors will remove once complete
           
            return null;
            
        } */
        
    }
    
}