public with sharing class CrawlspaceFloorComponentCntrl{
    public Id crawlspaceFloorId{get;set;}
    public String newLayerType{get;set;}
    public Floor__c crawlspaceFloor{get{
        if(crawlspaceFloor == null){
            List<Floor__c> floors = Database.query('SELECT ' + getSObjectFields('Floor__c') + ',Thermal_Envelope_Type__r.SpaceConditioning__c FROM Floor__c WHERE Id=:crawlspaceFloorId'); 
            if(floors.size() > 0){
                crawlspaceFloor = floors.get(0);
            }
        }
        return crawlspaceFloor;
    }set;}
    public void saveFloor(){
        saveFloorLayers();
        if(crawlspaceFloor != null && crawlspaceFloor.Id != null){
            crawlspaceFloor.Exposed_To__c = crawlspaceFloor.Floor_To__c;
            UPDATE crawlspaceFloor;
            crawlspaceFloor = null;
        }
    }
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    public List<Layer__c> floorLayers{get;set;}
    public Map<Id,Layer__c> getFloorLayersMap(){
        Id floorId = crawlspaceFloor.Id;
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Floor__c =:floorId AND HideLayer__c = false';
        floorLayers = Database.query(query);
        return new Map<Id,Layer__c>(floorLayers);
    }
    public void saveFloorLayers(){
        if(floorLayers != null && floorLayers.size() > 0){
            UPSERT floorLayers;
        }
    }
    public void addNewFloorLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Floor__c =:crawlspaceFloor.Id AND RecordTypeId =:recordTypeId];
        if(layerType != null && m.containsKey(layerType)){
            Decimal nextNumber= layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Floor__c = crawlspaceFloor.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            if(newLayer.Id != null){
                crawlspaceFloor.Next_Crawlspace_Floor_Layer_Number__c = nextNumber + 1;
                UPDATE crawlspaceFloor;
            }
            floorLayers.add(newLayer);
        }
    }
    public void deleteFloorLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = floorLayers.get(index).Id);
        floorLayers.remove(index);
        saveFloor();
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}