global class dsmtAddEditTicketCntlr{
    public boolean editable{get;set;}
    public string ticketId{get;set;}
    public Service_Request__c objTicket{get;set;}
    public string body{get;set;}
    
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
    
    
    public dsmtAddEditTicketCntlr(){
       isPortal = false;
       headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
       List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
       if(lstUser.size()>0){
           if(lstUser[0].IsPortalEnabled){
               isPortal = true;
               headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
           }
           else{
               headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
           }
       }
       ticketId = ApexPages.currentPage().getParameters().get('id');
        if(ticketId == null || ticketId == ''){
            ticketId = ApexPages.currentPage().getParameters().get('tid');
            if(ticketId == null || ticketId == ''){
                editable = false;
            }
        }else{
            editable = true;
        }
       
       if(ticketId!=null){
           List<Service_Request__c> lstTickets = [select Id,Name,Subject__c,Sub_Type__c,Type__c,Status__c,Priority__c,Description__c,RecordTypeId from Service_Request__c where Id = :ticketId Limit 1];
           if(lstTickets.size()>0){
               objTicket = lstTickets[0];
               body = objTicket.Description__c;
           }
       }
        
        if(objTicket == null){
            objTicket = new Service_Request__c();
            objTicket.Status__c= 'Draft';
            editable = true;
        }
       objTicket.Type__c = 'Trade Ally Service Request';
       Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Trade Ally Service Request').getRecordTypeId();
       objTicket.RecordTypeId = devRecordTypeId; 
       
    }
    
    public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    public void saveAndSubmit(){
        objTicket.Status__c= 'Submitted';
        this.saveTicket();
    }
    public void saveTicket(){
        //if(objTicket.Name != null && objTicket.Name!=''){
        try{
        List<User> userList = [select id,contactId from user where id =: userinfo.getUserId()];
        
        List<DSMTracker_Contact__c> dsmtconList = null;
        if(userList != null && userList.size() > 0 && userList.get(0).ContactId != null){
            
            system.debug('--userList.get(0).ContactId---'+userList.get(0).ContactId);
            
            dsmtconList = [
                select 
                    id,Name,First_Name__c,Last_Name__c,Super_User__c,Trade_Ally_Account__c,Trade_Ally_Account__r.Email__c,
                    Trade_Ally_Account__r.First_Name__c,Trade_Ally_Account__r.Last_Name__c,
                    Trade_Ally_Account__r.OwnerId,Trade_Ally_Account__r.Owner.Email,contact__c,Trade_Ally_Account__r.Account_Manager__c,Trade_Ally_Account__r.Account_Manager__r.Email
                from 
                    DSMTracker_Contact__c 
                where 
                    contact__c =: userList.get(0).ContactId];
        }else{
            dsmtconList = [
                select 
                    id,Name,First_Name__c,Last_Name__c,Super_User__c,Trade_Ally_Account__c,Trade_Ally_Account__r.Email__c,
                    Trade_Ally_Account__r.First_Name__c,Trade_Ally_Account__r.Last_Name__c,Portal_User__c,
                    Trade_Ally_Account__r.OwnerId,Trade_Ally_Account__r.Owner.Email,contact__c,Trade_Ally_Account__r.Account_Manager__c,Trade_Ally_Account__r.Account_Manager__r.Email
                from 
                    DSMTracker_Contact__c 
                where 
                    Portal_User__c =: userList.get(0).id];
        }
        if(dsmtConList != null){
                objTicket.Trade_Ally_Account__c = dsmtConList.get(0).Trade_Ally_Account__c;
                objTicket.DSMTracker_Contact__c = dsmtConList.get(0).Id;
                objTicket.Trade_Ally_Name__c = dsmtConList[0].Trade_Ally_Account__r.First_Name__c+' '+dsmtConList[0].Trade_Ally_Account__r.Last_Name__c;
                objTicket.Trade_Ally_Main_Email__c = dsmtConList[0].Trade_Ally_Account__r.Email__c;
                objTicket.DSMT_Contact_Name__c = dsmtConList[0].First_Name__c+' '+dsmtConList[0].Last_Name__c;
                
                if(dsmtConList.get(0).Trade_Ally_Account__r.Account_Manager__c != null){
                    objTicket.Trade_Ally_Email__c = dsmtConList.get(0).Trade_Ally_Account__r.Account_Manager__r.Email; //lstUser[0].Email;
                    objTicket.Trade_Ally_Owner__c = dsmtConList.get(0).Trade_Ally_Account__r.Account_Manager__c; //lstUser[0].Id;    
                }else if(dsmtConList.get(0).Trade_Ally_Account__r.OwnerId!=null){
                    //objTicket.OwnerId = dsmtConList.get(0).Trade_Ally_Account__r.OwnerId;
                    //List<User> lstUser = [select Id,Email from User where Id =:dsmtConList.get(0).Trade_Ally_Account__r.OwnerId limit 1];
                    //if(lstUser.size() > 0){
                        objTicket.Trade_Ally_Email__c = dsmtConList.get(0).Trade_Ally_Account__r.Owner.Email; //lstUser[0].Email;
                        objTicket.Trade_Ally_Owner__c = dsmtConList.get(0).Trade_Ally_Account__r.OwnerId; //lstUser[0].Id;    
                    //}
                }
            }
            
            objTicket.Requested_By__c = Userinfo.getuserid();  
            objTicket.Description__c = body;
            upsert objTicket;
            ticketId = objTicket.Id;
            
        }
        catch(Exception ex){}
        //}
    }
    
    @RemoteAction
    global static String getAttachmentsByTicketId(String pid) {
            list<Attachment__c> listAtts;
            if(pid!=null && pid!=''){
                //Integer count = [Select COUNT() from Attachment__c where Attachment_Type__c = 'OfferLetter' and CreatedBy =: uid and Message__c = :pid];
                Integer count = [Select COUNT() from Attachment__c where Service_Request__c = :pid];
                if(count > 0){
                  listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Service_Request__c = :pid order by CreatedDate desc];
                } 
            }else{
                return null;
            }
            return JSON.serializePretty(listAtts);
          // return count;
            //return listAtts      
     }
     @RemoteAction
     global static String  delAttachmentsById(String aid, String tid) {
                
            Attachment__c a = [select id from Attachment__c where id=:aid];
            delete a;
            list<Attachment__c> listAtts;
            Integer count = [Select COUNT() from Attachment__c where  Ticket__c = :tid];
            if(count > 0){
              listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Service_Request__c = :tid order by CreatedDate desc];
            } 
            return JSON.serializePretty(listAtts);
            //return listAtts      
      }
      public List<SelectOption> getTicketTypes()
        {
          List<SelectOption> options = new List<SelectOption>();
                
           Schema.DescribeFieldResult fieldResult =Service_Request__c.Type__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
             if(f.getValue()=='Question' || f.getValue()=='Complaint' || f.getValue()=='Bug' || f.getValue()=='Enhancement')
                  options.add(new SelectOption(f.getLabel(), f.getValue()));
           }       
           return options;
        }
}