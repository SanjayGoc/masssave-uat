@istest
public class dsmtCreateProjectOnAUTest {
    testMethod static void test() 
    {
        
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        ea.Enclosed_Cavity_Uploaded__c=true;
        ea.Lead_Safety_Uploaded__c=true;
        ea.SMA_photo_Uploaded__c=true;
        ea.Status__c='Assessment Complete';
        update ea;
        
        test.startTest();
        
        List<Proposal__c> updateproposallist = new List<Proposal__c>();
        Set<String> propId = new Set<String>();
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        proposal.Current_Incentive__c = 10;
        proposal.Status__c='Inactive';
        insert proposal;
        updateproposallist.add(proposal);
        system.debug('current incentive ====='+proposal.Current_Incentive__c);
        proposal.Create_Project_Manually__c=true;
        proposal.Status__c='Signed';
        update proposal;
        updateproposallist.add(proposal);
        system.debug('current incentive ====='+proposal.Current_Incentive__c);
        
        DSMTracker_Product__c dsmtp1 = new DSMTracker_Product__c();
        insert dsmtp1;
        
        DSMTracker_Product__c dsmtp = new DSMTracker_Product__c();
        dsmtp.Air_Sealing_Proposal_Type_Dependency__c = dsmtp1.Id;
        dsmtp.Incentive_Type__c = 'Percentage';
        insert dsmtp;   
        
        Recommendation_Scenario__c recs = new Recommendation_Scenario__c();
        insert recs;
        
        Proposal__c pr  = new Proposal__c();
        pr.Current_Incentive__c = 1.00;
        insert pr;
        propId.add(pr.id);
        List<Recommendation__c> updaterecommendationlist = new List<Recommendation__c>();
        
        Recommendation__c rem = new Recommendation__c();
        rem.Energy_Assessment__c = ea.id;
        //rem.Dsmtracker_Product__c =dsmtp.id;
        rem.Recommendation_Scenario__c = recs.id;
        rem.Proposal__c=proposal.id;
        rem.Invoice_Type__c =  'NSTAR418';
        insert rem;
        updaterecommendationlist.add(rem);
        
        Proposal_Recommendation__c pro =new Proposal_Recommendation__c();
        pro.Proposal__c=pr.id;
        pro.Recommendation__c=rem.id;
        pro.Project__c = recs.id;
        insert pro; 
        update pro;
        test.stopTest();
        
    }
    testMethod static void dsmtCreateNotesOnWorkOrdertesting() 
    {
        
        Account acc = new Account();
        acc.Name = 'Energy NM';
        acc.Billing_Account_Number__c= 'Gas-~~~1234';
        acc.Utility_Service_Type__c= 'Gas';
        acc.Account_Status__c= 'Active';
        insert acc;
        
        Customer__c cust =new Customer__c();
        cust.Account__c=acc.Id;
        cust.Service_Address__c='test';
        cust.Service_City__c='test';
        cust.Service_State__c='CO';
        cust.Service_Postal_Code__c='test';
        insert cust;
        
        Location__c loc = new Location__c();
        loc.Name = 'Fresno';
        insert loc;
        
        Employee__c emp =new Employee__c();
        emp.Location__c=loc.id;
        emp.Status__c='Approved';
        emp.Employee_Id__c = 'test123';
        insert emp;
        test.startTest();
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, emp.Id);
        wt.Service_Date__c = system.today();
        wt.Captain__c = emp.Id;
        wt.Default_Region__c='Boston';
        wt.Unavailable__c=false;
        wt.Start_Date__c=date.today();
        wt.End_Date__c=date.today();
        wt.Lunch_Start_Date__c=date.today();
        wt.Lunch_End_Date__c=date.today();
        insert wt;
        
        Workorder_Type__c wty = new Workorder_Type__c();
        wty.name='test';
        wty.Est_PreWork_Time__c=2;
        wty.Est_PostWork_Time__c=34;
        wty.Est_Work_Time__c=4;
        wty.Visit_Size__c='S';
        wty.Est_Deliverable_Time__c=65;
        wty.Visit_Size__c='';       
        insert wty;
        
        List<Note> noteToInsert = new List<Note>();
        Note__c n = new Note__c();
        n.Title__c = 'WO Alert Added/Changed';
        n.Body__c = 'test';
        insert n;
        
        Workorder__c work =new Workorder__c();
        work.Cancelled_date__c=date.today();
        work.Notes__c='test';
        work.Customer__c=cust.id;
        work.Workorder_Type__c=wty.id;
        work.Early_Arrival_Time__c=date.today();
        work.Late_Arrival_Time__c=date.today();
        work.Scheduled_Date__c=date.today();
        work.Alert__c = 'testing';
        //work.Work_Team__c=wt.id;
        insert work;
        work.Alert__c = 'abcd';
        update work;
        test.stopTest();
    }
}