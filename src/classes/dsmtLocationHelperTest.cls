@isTest
public class dsmtLocationHelperTest{
    public testMethod static void test1(){
    
        WeatherStation__c ws = new WeatherStation__c();
        ws.WeatherStationName__c = 'station1';
        insert ws;
        
        WeatherStationTemprature__c wst = new WeatherStationTemprature__c();
        wst.TempIncr__c=5;
        wst.TempStart__c=5;
        wst.WeatherStation__c=ws.id;
        wst.WSProperty__c='WSProperty';
        wst.TempValues__c='000';
        insert wst;
       
        Mechanical_Type__c mt = new Mechanical_Type__c();
        insert mt;
        
        CoolingSystemUsage__c cls = new CoolingSystemUsage__c();
        cls.Value__c=22.30;
        cls.Name='test';
        insert cls;
        
        test.startTest();
        List <WeatherStationTemprature__c> wsTemp = new List <WeatherStationTemprature__c>();
        wsTemp.add(wst);
        
        dsmtLocationHelper cntrl = new dsmtLocationHelper();
        dsmtLocationHelper.findLocationSpace('Family');
        dsmtLocationHelper.getWeatherStTemp(ws.Id, 'WSProperty', 000);
        dsmtLocationHelper.getWeatherStTemp(wsTemp, 'WsProperty', 000);
        Decimal dc3 = cntrl.adjustCoolingIndoorTemp(22.30,'Never');
        
        //	Decimal dc = cntrl.getFractionDays('H',ws.id,ea.id,'Test');
        //Decimal dc1 = cntrl.computeTestH(ea.id);
        //Decimal dc2 = cntrl.computeRoomTemp('Interior','H',ea.id,ws.id);
        
        //Decimal dc4 = cntrl.computeMechRoomTempC(ea.id);
        test.stopTest();
    }
}