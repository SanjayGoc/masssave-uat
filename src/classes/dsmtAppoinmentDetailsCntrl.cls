public class dsmtAppoinmentDetailsCntrl{
    
    public string custId{get; set;}
    public Customer__c objCustomer{get; set;}
    public list<AppointmentWOModel> appList {get;set;}
    
    public dsmtAppoinmentDetailsCntrl(){
        Init();
    }
    
    public void Init(){
        objCustomer = new Customer__c();
        appList = new list<AppointmentWOModel>();
        
        custId = ApexPages.currentPage().getParameters().get('id');
        string userId = UserInfo.getUserId();
        
        if(custId!=null && custId!=''){
            List<Customer__c> lstCustomers = [select Id,First_Name__c,Last_Name__c,Phone__c,Email__c,Service_Address__c,Service_City__c,
                                              Service_State__c,Service_Zipcode__c,Electric_provider__r.Name,Electric_Account_Number__c,
                                              Gas_provider__r.Name,Gas_Account_Number__c,Landlord_Name__c,Landlord_Phone__c,
                                              Landlord_Email__c,LANDLORD_ADDRESS__c,LANDLORD_CITY__c,LANDLORD_STATE__c,LANDLORD_ZIP__c,
                                             (select id,Phone__c,Email__c, Workorder__c, Workorder__r.Name, Appointment_Status__c,
                                              Workorder__r.Scheduled_Date__c, Workorder__r.Scheduled_Start_Date__c, 
                                              Workorder__r.Scheduled_End_Date__c, Workorder__r.Duration__c,Workorder__r.Requested_Start_Date__c, 
                                              Workorder__r.Requested_End_Date__c,
                                              Workorder__r.Workorder_Type_Name__c,Workorder__r.Customer_Eligibility__c,Workorder__r.Customer_Eligibility__r.Home_Size__c,
                                              Workorder__r.Customer_Eligibility__r.Home_Style__c,Workorder__r.Customer_Eligibility__r.Year_built_in__c,Workorder__r.Customer_Eligibility__r.Attic__c,Workorder__r.Customer_Eligibility__r.Thermostats__c,
                                              Workorder__r.Customer_Eligibility__r.Primary_Provider__c,Workorder__r.Customer_Eligibility__r.Gas_Account_Holder_First_Name__c,Workorder__r.Customer_Eligibility__r.Gas_Account_Holder_Last_Name__c,
                                              Workorder__r.Customer_Eligibility__r.Electric_Account_Holder_First_Name__c,Workorder__r.Customer_Eligibility__r.Electric_Account_Holder_Last_Name__c,
                                              Eligibility_Check__r.First_Name__c, Eligibility_Check__r.Last_Name__c,
                                              Eligibility_Check__r.Status__c, Eligibility_Check__r.Phone__c,
                                              Eligibility_Check__r.Email__c, Eligibility_Check__r.Do_you_heat_with_natural_Gas__c,
                                              Eligibility_Check__r.I_heat_my_home_with__c, Eligibility_Check__r.How_many_units__c,
                                              Eligibility_Check__r.Do_you_own_or_rent__c, Eligibility_Check__r.Do_you_live_there__c,
                                              Eligibility_Check__r.How_many_do_you_own__c, Eligibility_Check__r.Home_Size__c,
                                              Eligibility_Check__r.Home_Style__c, Eligibility_Check__r.Year_built_in__c,
                                              Eligibility_Check__r.Attic__c, Eligibility_Check__r.Thermostats__c
                                              from Appointments__r
                                              //where (OwnerId = :userId or user__c =: UserId)
                                              where Workorder__r.Scheduled_Start_Date__c != null
                                              and Workorder__r.Scheduled_End_Date__c != null
                                              ) 
                                              from Customer__c 
                                              where Id = :custId 
                                              limit 1];
            
            if(lstCustomers.size()>0){
                objCustomer = lstCustomers[0];
                
                
                if(objCustomer.Electric_Account_Number__c !=null && objCustomer.Electric_Account_Number__c.length() > 4){
                    system.debug('---'+objCustomer.Electric_Account_Number__c.substring(objCustomer.Electric_Account_Number__c.length() -4));
                    objCustomer.Electric_Account_Number__c = '******'+ objCustomer.Electric_Account_Number__c.substring(objCustomer.Electric_Account_Number__c.length() -4);
                   // objCustomer.Electric_Account_Number__c = '*******'+objCustomer.Electric_Account_Number__c;
                }
                
                if(objCustomer.Gas_Account_Number__c !=null && objCustomer.Gas_Account_Number__c.length() > 4){
                    system.debug('---'+objCustomer.Gas_Account_Number__c.substring(objCustomer.Gas_Account_Number__c.length() -4));
                    objCustomer.Gas_Account_Number__c = '******'+ objCustomer.Gas_Account_Number__c.substring(objCustomer.Gas_Account_Number__c.length() -4);
                   // objCustomer.Electric_Account_Number__c = '*******'+objCustomer.Electric_Account_Number__c;
                }
                
                for(Appointment__c a : objCustomer.Appointments__r){
                    system.debug(a.Workorder__r.Name);
                    system.debug(a.Workorder__c);
                    system.debug(a.Workorder__r.Scheduled_Start_Date__c);
                    system.debug(a.Workorder__r.Scheduled_End_Date__c);
                    system.debug(a.Workorder__r.Requested_Start_Date__c);
                    system.debug(a.Workorder__r.Requested_End_Date__c);
                    if(a.Workorder__r.Name != null){
                    
                        String firstname = '';
                        String lastname = '';
                        if(a.Workorder__r.Customer_Eligibility__c != null){
                            if(a.Workorder__r.Customer_Eligibility__r.Primary_Provider__c == 'Gas'){
                               firstname = a.Workorder__r.Customer_Eligibility__r.Gas_Account_Holder_First_Name__c;
                               lastname = a.Workorder__r.Customer_Eligibility__r.Gas_Account_Holder_Last_Name__c;
                            }else{
                               firstname = a.Workorder__r.Customer_Eligibility__r.Electric_Account_Holder_First_Name__c;
                               lastname = a.Workorder__r.Customer_Eligibility__r.Electric_Account_Holder_Last_Name__c;
                            }
                        }else{
                           firstname = a.Eligibility_Check__r.First_Name__c;
                           lastname = a.Eligibility_Check__r.Last_Name__c;
                        }
                        
                        
                        appList.add(new AppointmentWOModel(a.Id,
                                                           a.Workorder__r.Name,
                                                           a.Appointment_Status__c,
                                                           a.Workorder__r.Workorder_Type_Name__c,
                                                           a.Workorder__r.Scheduled_Date__c,
                                                           a.Workorder__r.Scheduled_Start_Date__c.format('MM/dd/yyyy hh:mm a'),
                                                           a.Workorder__r.Scheduled_End_Date__c.format('MM/dd/yyyy hh:mm a'),
                                                           a.Workorder__r.Duration__c,
                                                           //a.Eligibility_Check__r.First_Name__c, 
                                                           //a.Eligibility_Check__r.Last_Name__c,
                                                           firstname,
                                                           lastname,
                                                           a.Eligibility_Check__r.Status__c, 
                                                           a.Phone__c,
                                                           a.Email__c, 
                                                           a.Eligibility_Check__r.Do_you_heat_with_natural_Gas__c,
                                                           a.Eligibility_Check__r.I_heat_my_home_with__c, 
                                                           a.Eligibility_Check__r.How_many_units__c,
                                                           a.Eligibility_Check__r.Do_you_own_or_rent__c, 
                                                           a.Eligibility_Check__r.Do_you_live_there__c,
                                                           a.Eligibility_Check__r.How_many_do_you_own__c, 
                                                           (a.Workorder__r.Customer_Eligibility__c != null)? a.Workorder__r.Customer_Eligibility__r.Home_Size__c :a.Eligibility_Check__r.Home_Size__c,
                                                           (a.Workorder__r.Customer_Eligibility__c != null)? a.Workorder__r.Customer_Eligibility__r.Home_Style__c: a.Eligibility_Check__r.Home_Style__c, 
                                                           (a.Workorder__r.Customer_Eligibility__c != null)? a.Workorder__r.Customer_Eligibility__r.Year_built_in__c : a.Eligibility_Check__r.Year_built_in__c,
                                                           (a.Workorder__r.Customer_Eligibility__c != null)? a.Workorder__r.Customer_Eligibility__r.Attic__c : a.Eligibility_Check__r.Attic__c, 
                                                           (a.Workorder__r.Customer_Eligibility__c != null)? a.Workorder__r.Customer_Eligibility__r.Thermostats__c : a.Eligibility_Check__r.Thermostats__c));
                    }
                }
            }          
            system.debug('--appList---'+appList);       
        }
    }
    
    public class AppointmentWOModel{
        public string appId {get;set;}
        public string woNumber{get;set;}
        public string status {get;set;}
        public string woType{get;set;}
        public date sDate{get;set;}
        public string startDate {get;set;}
        public string endDate {get;set;}
        public decimal duration {get;set;} 
        
        public string firstName {get;set;}
        public string lastName {get;set;}
        public string eStatus {get;set;}
        public string phone {get;set;}
        public string email {get;set;}
        public string heatWith {get;set;}
        public string myHomeHeat {get;set;}
        public string units {get;set;}
        public string rentOwn {get;set;}
        public string liveThere  {get;set;}
        public string howManyOwn {get;set;}
        public string homeSize {get;set;}
        public string homeStyle {get;set;}
        public string yearBuilt {get;set;}
        public string ducts {get;set;}
        public string thermostats {get;set;}
        
        public AppointmentWOModel(string appId,
                                  string woNumber, string status, string woType, date sDate,
                                  string startDate, string endDate, decimal duration,
                                  string firstName,
                                  string lastName,
                                  string eStatus,
                                  string phone,
                                  string email,
                                  string heatWith,
                                  string myHomeHeat,
                                  string units,
                                  string rentOwn,
                                  string liveThere ,
                                  string howManyOwn,
                                  string homeSize,
                                  string homeStyle,
                                  string yearBuilt,
                                  string ducts,
                                  string thermostats){
                                  
            this.appId = appId;
            this.woNumber = woNumber;
            this.status = status;
            this.woType = woType;
            this.sDate = sDate;
            this.startDate = startDate;
            this.endDate = endDate;
            this.duration = duration;
            
            this.firstName = firstName;
            this.lastName = lastName;
            this.eStatus = eStatus;
            this.phone = phone;
            this.email = email;
            this.heatWith = heatWith;
            this.myHomeHeat = myHomeHeat;
            this.units = units;
            this.rentOwn = rentOwn;
            this.liveThere = liveThere;
            this.howManyOwn = howManyOwn;
            this.homeSize = homeSize;
            this.homeStyle = homeStyle;
            this.yearBuilt = yearBuilt;
            this.ducts = ducts;
            this.thermostats = thermostats;
        }
    }
}