@istest
public class dsmtRejectInvoiceLineItemExtnTest
{
	@istest
    static void test()
    {
        Invoice__c iv=new Invoice__c();
        insert iv;
        
        Invoice_Line_Item__c ili =new Invoice_Line_Item__c();
        ili.Invoice__c=iv.id;
        ili.Status__c='Rejected';
        ili.Rejection_Notes__c='Rejected';
        ili.Unit_Cost__c=12;
        insert ili;
        ApexPages.StandardController sc = new ApexPages.StandardController(ili);
        dsmtRejectInvoiceLineItemExtn cntrl = new dsmtRejectInvoiceLineItemExtn(sc);
        cntrl.saveAndUpdate();
    }
}