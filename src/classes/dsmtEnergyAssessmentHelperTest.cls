@isTest
public class dsmtEnergyAssessmentHelperTest   {
    public testmethod static void test1(){
        List<Energy_Assessment__c> RecObj = new List<Energy_Assessment__c>();
        
        
        Energy_Assessment__c ea = datagenerator.setupAssessmentWithWorkOrder();       
        RecObj.add(ea);
        insert ea;   
        test.startTest();
        Eligibility_Check__c elc =new Eligibility_Check__c();
        elc.Do_you_heat_with_natural_Gas__c='Yes';
        elc.How_many_units__c='Single family';
        insert elc;
  
        Customer_Eligibility__c ce = new Customer_Eligibility__c();
        ce.Eligibility_Check__c=elc.Id;
        insert ce;
        
        Workorder__c wo = [select id from Workorder__c where id =:ea.Workorder__c];
        wo.Customer_Eligibility__c = ce.Id;
        wo.Eligibility_Check__c=elc.Id;
        update wo;
        
        Appointment__c app=[select id from Appointment__c where id = : ea.Appointment__c];
        
        dsmtEnergyAssessmentHelper.CalculateCostAndSavings(RecObj, true);
        dsmtEnergyAssessmentHelper.PopulateContractor(RecObj);
        test.stopTest();
    }
    
    public testmethod static void test2(){
        set<Id> eaId =new set<Id>(); 
        Trigger_Configuration__c tc=new Trigger_Configuration__c();
        tc.Deactivate_WorkOrder_Triggers__c=True;
        tc.Deactivate_Appointment_Triggers__c = True;
        insert tc;
        Energy_Assessment__c ea = datagenerator.setupAssessmentWithWorkOrder();       
        ea.Assessment_Type__c = 'Landlord Visit';
        ea.Primary_Provider__c='Eversource East Electric';
        insert ea;
        eaId.add(ea.Id);
        test.startTest();
        
        DSMTracker_Product__c dsmtPro = new DSMTracker_Product__c();
        dsmtPro.Category__c = 'Direct Install';
        insert dsmtPro;
        
        Recommendation__c reco = new Recommendation__c();
        reco.Energy_Assessment__c = ea.Id;
        reco.Installed_Date__c = date.today();
        //reco.DSMTracker_Product__c = dsmtPro.Id;
        insert reco;
        
        dsmtEnergyAssessmentHelper.GetInvoiceType(eaId);
        
        test.stopTest();
        
    }
    
    public testmethod static void test3(){
        List<Energy_Assessment__c> RecObj = new List<Energy_Assessment__c>();
        
        
        Energy_Assessment__c ea = datagenerator.setupAssessmentWithWorkOrder();       
        RecObj.add(ea);
        insert ea;       
        
        test.startTest();
        
        RecordType rc = [select id from RecordType where sobjecttype ='Thermal_Envelope_Type__c' and name ='Basement'];
        
        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        tet.RecordTypeId =rc.Id;
        tet.Finished__c = true;
        insert tet;
        
        Floor__c floor = new Floor__c();
        floor.Thermal_Envelope_Type__c = tet.Id;
        floor.Area__c = 1200;
        insert floor;
        
        
        Wall__c wall = new Wall__c();
        wall.Thermal_Envelope_Type__c = tet.Id;
        insert wall;
        
        dsmtEnergyAssessmentHelper.CalculateCostAndSavings(RecObj, false);
        
        test.stopTest();
        
    }
    
    public testmethod static void test4(){
        set<Id> eaId =new set<Id>(); 
        List<Energy_Assessment__c> eaList = new List<Energy_Assessment__c>();
        Map<Id,Energy_Assessment__c> oldMap = new Map<Id,Energy_Assessment__c>();
        
        LightingAndApplianceConstant__c laca = new LightingAndApplianceConstant__c();
        laca.DefaultIndoorLightingPerSqFt__c = 12;
        laca.DefaultOutdoorLightingUsage__c = 'Medium';
        laca.DefaultOutdoorLightingPerSqFt__c = 12;
        insert laca;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.Do_you_heat_with_natural_Gas__c='Yes';
        ec.How_many_units__c='Single family';
        insert ec;
        
        Customer_Eligibility__c ce = new Customer_Eligibility__c();
        ce.Eligibility_Check__c=ec.Id;
        insert ce;
        
        Workorder__c wo = new Workorder__c();
        wo.Customer_Eligibility__c = ce.Id;
        wo.Eligibility_Check__c=ec.Id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();         
        insert wo;
        checkRecursive.runOnce();
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;

        Building_Specification__c bs = new Building_Specification__c();
        bs.Floor_Area__c = 1;
        bs.Orientation__c = 'South';
        bs.Floors__C=1.0;
        bs.Bedromm__c = 1;
        bs.Ceiling_Heights__c = 10;
        bs.Appointment__c = appt.id;
        bs.Occupants__c = 10;
        bs.Year_Built__c='2018';
        insert bs;
        
         WallRatio__c wr = new WallRatio__c();
        wr.Name = 'Rectangular';
        wr.Value__c = 1;
        insert wr;
        
        FloorWallExp__c fw = new FloorWallExp__c();
        fw.Name = 'Rectangular';
        fw.Value__c = 1;
        insert fw;
        
        Saving_Constant__c sc = new Saving_Constant__c();
        sc.DefaultDoorWidth__c = 10;
        sc.DefaultDoorheight__c = 10;
        sc.FrontBackToLeftRightWindowAreaFactor__c = 10;
        sc.DefaultWindowFilmTotalSolarRejection__c = 10;
        sc.InsulationQuiltLengthOfNightFactor__c = 10;
        insert sc;
        
        WeatherStation__c ws = new WeatherStation__c();
        ws.NormalNFactor__c = 10;
        insert ws;
        
        Energy_Assessment__c ea =new Energy_Assessment__c();
  
        ea.Building_Specification__c = bs.id;   
        ea.Building_Shape__c = 'Rectangular';
        ea.WeatherStation__c = ws.id;                        
        ea.Workorder__c = wo.Id;      
        ea.Appointment__c = appt.Id;
              
        ea.Assessment_Type__c = 'Landlord Visit';
        insert ea;
        eaId.add(ea.Id);
        
        test.startTest();
     
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        dsmt.Status__c='Approved';
        insert dsmt;
        
        Customer__c cust = new Customer__c();
        cust.Customer_Phone__c = '9696969696';
        insert cust;
                
        checkRecursive.runOnce();
        Appointment__c app=[select id from Appointment__c where id = : ea.Appointment__c];
        app.DSMTracker_Contact__c=dsmt.Id;
        app.Energy_Assessment__c=ea.id;
        app.Workorder__c = wo.id;
        app.Appointment_Status__c = 'Cancelled';
        app.Appointment_Type__c = 'Combustion Safety Test';
        
        update app; 
        
        DSMTracker_Product__c dsmtPro = new DSMTracker_Product__c();
        dsmtPro.Category__c = 'Direct Install';
        dsmtPro.Sub_Category__c = 'Duct Measures';
        insert dsmtPro;
        
        Recommendation__c reco = new Recommendation__c();
        reco.Energy_Assessment__c = ea.Id;
        reco.Installed_Date__c = date.today();
        reco.DSMTracker_Product__c = dsmtPro.Id;
        insert reco;
        
        dsmtEnergyAssessmentHelper.GetInvoiceType(eaId);
        dsmtEnergyAssessmentHelper.PopulateCustomerinfoOnAssessment(eaList,oldMap);
        test.stopTest();
        
    }
   @istest
    static  void dsmtBIPopulateFieldOnAssessmenttesting()
    {
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        dsmt.Status__c='Approved';
        insert dsmt;
        
        test.startTest();
        checkRecursive.runOnce();
        Energy_Assessment__c ea = datagenerator.setupAssessmentWithWorkOrder();       
        ea.Assessment_Type__c = 'Landlord Visit';
        ea.DSMTracker_Contact__c=dsmt.id;
        insert ea;
        //ea.Status__c='Assessment Complete';
        //update ea;
        Customer__c cust = new Customer__c();
        cust.Customer_Phone__c = '9696969696';
        insert cust;
        test.stopTest();
       
        
        Appointment__c app = new Appointment__c(); //[select id from Appointment__c where id = : ea.Appointment__c];
        app.DSMTracker_Contact__c=dsmt.Id;
        app.Energy_Assessment__c=ea.id;
        app.customer_Reference__c = cust.id;
        app.Appointment_Status__c = 'Cancelled';
        insert app;
    }
}