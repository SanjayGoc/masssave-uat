public class UpdateAppointmentHelper{
    
    @future
    public static void updateAppointment(String mapString)
    {
        update (List<Appointment__c>) JSON.deserialize(mapString, List<Appointment__c>.class);
    }
}