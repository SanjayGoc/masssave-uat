global class dsmtPendingIntakesCntrl{
    
    public string apptJSON{get;set;}
    public Dsmtracker_contact__c dsmtCon{get;set;}
    public Trade_Ally_Account__c ta{get;set;}
    public String conId   {get;set;}
    public Set<Id> dsmtcIds{get;set;}
    public String taId{get;set;}
    
    global dsmtPendingIntakesCntrl(){
        loadContactAndTA();
        initScheduledAppointments();
        
    }
    
    public void loadContactAndTA(){
        dsmtcIds = new Set<Id>();
        String usrid = UserInfo.getUserId();
        List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
        if(usrList != null && usrList.size() > 0){
            if(usrList.get(0).ContactId != null){
                conId   = usrList.get(0).ContactId;
                List<DSMTracker_Contact__c> dsmtList = [select id,Trade_Ally_Account__c,Portal_Role__c from DSMTracker_Contact__c where Contact__c =: conId limit 1];
                if(dsmtList != null && dsmtList.size() > 0){
                    dsmtCon = dsmtList.get(0);
                    dsmtcIds.add(dsmtCon.id);
                    
                    List<Trade_Ally_Account__c> taList = [select id,Owner.Email from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                    if(taList != null && taList.size() > 0){
                        ta = taList.get(0);
                        taId = ta.id;
                        if(dsmtCon.Portal_Role__c == 'Manager'){
                            List<Dsmtracker_Contact__c> dsmtconList = [select id,Super_User__c,Name,contact__c from DSMTracker_Contact__c where Trade_Ally_Account__c  =: ta.Id];
                            for(Dsmtracker_Contact__c dsmt : dsmtConList){
                               dsmtcIds.add(dsmt.id);
                            }
                        }
                    }
                }
            }
        }
    }
    
    public void initScheduledAppointments(){
        
        apptJSON = '';
        
        String userId = userinfo.getUserId();
        String queryString = 'Select Id,Name,Customer_Reference__r.Name,Appointment_Type__c,Appointment_Start_Time__c,Appointment_End_Time__c,Employee__r.Name,Appointment_Status__c,CreatedDate ';
            queryString += ',Eligibility_Check__c, Notes__c,Eligibility_Check__r.Eligibility_Failure_Message__c,Eligibility_Check__r.LastModifiedDate,Customer_Reference__c,Eligibility_Check__r.How_many_units__c,Eligibility_Check__r.First_Name__c,Eligibility_Check__r.Last_Name__c,Eligibility_Check__r.Phone__c,Eligibility_Check__r.Email__c,Eligibility_Check__r.Zip__c ';
            queryString += 'from Appointment__c where Appointment_Status__c In (\'Pending\',\'Approved\',\'Denied\',\'More Information Required\',\'Eligible\',\'Ineligible\',\'Incomplete\') AND Trade_ally_Account__c =: taId';
            queryString += ' order by CreatedDate Desc LIMIT 50000 ';
            
            list<Appointment__c> listMsg=new list<Appointment__c>();
            for(Appointment__c Apt:Database.query(queryString)){
                listMsg.add(Apt);
            }
            
            
            list<AppointmentModal> listAppoint = new List<AppointmentModal>();
            for(Appointment__c apt : Database.query(queryString)){
                 
                
                AppointmentModal mod = new AppointmentModal();
                mod.Id = apt.Id;
                mod.AppointNumber = apt.Name;
                
                mod.CustomerName= apt.Customer_Reference__r.Name;
                mod.customerId = apt.Customer_Reference__c;
                mod.AppointmentType= apt.Appointment_Type__c;
                if(apt.Appointment_Start_Time__c!= null)
                    mod.StartTime = date.newinstance(apt.Appointment_Start_Time__c.year(), apt.Appointment_Start_Time__c.month(), apt.Appointment_Start_Time__c.day());
                if(apt.Appointment_End_Time__c!= null)
                    mod.EndTime = date.newinstance(apt.Appointment_End_Time__c.year(), apt.Appointment_End_Time__c.month(), apt.Appointment_End_Time__c.day());
                mod.Employee = apt.Employee__r.Name;
                mod.Status = apt.Appointment_Status__c;
                mod.CreatedDate = date.newinstance(apt.CreatedDate.year(), apt.CreatedDate.month(), apt.CreatedDate.day());
                mod.notes = apt.Notes__c;
                if(apt.Eligibility_Check__c!=null){
                    mod.FirstName = apt.Eligibility_Check__r.First_Name__c;
                    mod.LastName = apt.Eligibility_Check__r.Last_Name__c;
                    mod.Phone = apt.Eligibility_Check__r.Phone__c;
                    mod.Email = apt.Eligibility_Check__r.Email__c;
                    mod.Zip = apt.Eligibility_Check__r.Zip__c;
                    mod.Units = apt.Eligibility_check__r.How_many_units__c;
                    mod.FailureErrMsg = apt.Eligibility_check__r.Eligibility_Failure_Message__c; // UAT-525
                    mod.LastModifiedDate = date.newinstance(apt.Eligibility_Check__r.LastModifiedDate.year(), apt.Eligibility_Check__r.LastModifiedDate.month(), apt.Eligibility_Check__r.LastModifiedDate.day()); //UAT-525
                }
                system.debug('--mod--'+mod);
                    
                listAppoint.add(mod);
            }
            system.debug('--listMessage---'+listAppoint );
            apptJSON = JSON.serialize(listAppoint );
        
        
    }
    
    public class AppointmentModal{
        public string Id{get;set;}
        public String AppointNumber{get;set;}
        public string CustomerName{get;set;}
        public string AppointmentType{get;set;}
        public date StartTime{get;set;}
        public date EndTime{get;set;}
        public string Employee{get;set;}
        public string Status{get;set;}
        public date CreatedDate{get;set;}
        public string FirstName{get;set;}
        public string LastName{get;set;}
        public string Phone{get;set;}
        public string Email{get;set;}
        public string Zip{get;set;}
        public string notes {get;set;}
        public String Units{get;set;}
        public String customerId{get;set;}
        public date LastModifiedDate{get;set;} //UAT-525
        public String FailureErrMsg{get;set;} //UAT-525
        //public String Notes{get;set;}
    }
}