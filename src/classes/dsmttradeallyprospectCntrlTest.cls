@isTest
public class dsmttradeallyprospectCntrlTest {

    testMethod static void testsaveTAA(){ 
        Checklist__c parent = new Checklist__c();
        parent.Unique_Name__c = 'Trade_Ally_Prospect_Form';
        parent.Reference_ID__c='121221';
        insert parent;
        
        Checklist_Items__c item = new Checklist_Items__c();
        item.Parent_Checklist__c = parent.Id;
        item.Reference_ID__c='121221';
        item.Checklist__c='test';
        insert item;
        
        dsmttradeallyprospectCntrl dsmt = new dsmttradeallyprospectCntrl();
        dsmt.sectionTitle = 'Test';
        dsmt.saveTAA();
        Trade_Ally_Account__c ac = new Trade_Ally_Account__c();
        ac.Name='Test';
        dsmt.objTAC = ac;
        dsmt.saveTAA();
        Checklist__c pgCheckList = dsmt.pgCheckList;
        list<Checklist_Items__c> listItem = dsmt.getChecklists();
    }
    

    testMethod static void testsaveTAAList(){
        Checklist__c parent = new Checklist__c();
        parent.Unique_Name__c = 'Trade_Ally_Prospect_1';
        parent.Reference_ID__c='121221';        
        insert parent;
        
        Checklist_Items__c item = new Checklist_Items__c();
        item.Parent_Checklist__c = parent.Id;
        item.Reference_ID__c='121221';
        item.Checklist__c='test';
        insert item;
        
        
        dsmttradeallyprospectCntrl dsmt = new dsmttradeallyprospectCntrl();
        list<Checklist_Items__c> listItem = dsmt.getChecklists();
    }
}