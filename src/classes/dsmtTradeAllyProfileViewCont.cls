public class dsmtTradeAllyProfileViewCont {
  
    public Dsmtracker_Contact__c dsmtContact{get;set;}
    public Trade_Ally_Account__c tradeallyAccount{get;set;}
    public List<Attachment__c> companyAttachList{get;set;}
    public List<Attachment__c> dsmtContAttachList{get;set;}
    public List<Dsmtracker_Contact__c> contactList{get;set;}
    
    public String accid{get;set;}
    public String conid{get;set;}
  
    public boolean failedRegistration {get;set;}
    public User loginUser {get;set;}
   
    public String securityQst {get;set;}
    public String ansSecurityQst {get;set;}
    public boolean isEdit      {get;set;}
    public string currentPassword {get;set;}
    public string password        {get;set;}
    public string confirmPassword {get;set;}
    public boolean isPasswordChanged {get;set;}
    
    public boolean showbutton{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
   
    public dsmtTradeAllyProfileViewCont () {
    
        isPortal = true;
        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
        List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
            if(lstUser.size()>0){
                if(lstUser[0].IsPortalEnabled){
                    headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                }
                else{
                    headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                    isPortal = false;
                }
            }
    
        companyAttachList = new List<Attachment__c>();
        dsmtContAttachList = new List<Attachment__c>();
        contactList = new List<Dsmtracker_contact__c>();
        
        isEdit = false;
        String action = ApexPages.currentPage().getParameters().get('action');
        fetchdsmtContactAndTradeAllyAccount(); 
        
        
        if(tradeallyAccount != null){
            if(tradeallyAccount.Registration_Renewal_Date__c  <= Date.Today()){
                showbutton = true;
            }
        }
    }
    
    public void changeEditMode()
    {
        if(isEdit) isEdit = false; else isEdit = true;
    }
    
    public boolean isShowError{get;set;}
    public void updatePassword(){
        
        PageReference pg = Site.changePassword(password, confirmPassword, currentPassword);
        
        if(pg == null)
            isPasswordChanged = false;
        else
            isPasswordChanged = true;
        password = confirmPassword = currentPassword = null;
        isShowError = true;
    }

    public PageReference updateProfile() {
        try {
            
        } catch (Exception e) {
            system.debug('--e---'+e);
            ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg3);
            failedRegistration = true;
        }
        return null;
    }

   
    private void fetchdsmtContactAndTradeAllyAccount() {
        String usrid = UserInfo.getUserId();

        List < User > usrList = [select Id, SecurityQuestion__c, Ans_SecurityQuestion__c, ContactId, Contact.AccountId, Contact.Account.Name, Contact.Account.RecordType.Name, Contact.Name from user Where Id = : usrid];
        if (usrList != null && usrList.size() > 0) {
            loginUser = usrList.get(0);
            securityQst = usrList.get(0).SecurityQuestion__c;
            ansSecurityQst = usrList.get(0).Ans_SecurityQuestion__c;
            List < DSMTracker_Contact__c > dsmtContLst = null;
            if (usrList.get(0).ContactId != null) {
                accid = usrList.get(0).Contact.AccountId;
                conId = usrList.get(0).ContactId;

                
                dsmtContLst = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c,Active__c from DSMTracker_Contact__c where Contact__r.Id = : conId and Trade_Ally_Account__c != null limit 1];
                
            }else{
               String userid = usrList.get(0).id;
               dsmtContLst = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c,Active__c from DSMTracker_Contact__c where Portal_user__c = : userid and Trade_Ally_Account__c != null limit 1]; 
            }
            
            
            if (dsmtContLst != null && dsmtContLst.size() > 0) {
                     
                 dsmtcontact = dsmtContLst.get(0);
                 
                 dsmtContAttachList = [select name,Attachment_Name__c,Expires_On__c,Expired_Text__c,Business_Name_Text__c,File_Download_Url__c,File_Url__c,
                                     FileKey__c,Attachment_Type__c,Contractor_Instruction__c from attachment__c where Dsmtracker_Contact__c =: dsmtcontact.Id and status__c = 'Approved'];
                 
                 
                 List<Trade_Ally_Account__c> taList = [select id,name,Account__c, Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,Website__c,Phone__c,Email__c,Registration_Renewal_Date__c,
                                                            Outreach_rep__r.Phone,Outreach_rep__r.Email,Outreach_rep__r.Name from Trade_Ally_Account__c where Id =: dsmtcontact.Trade_Ally_Account__c];
                    if(taList != null && taList.size() > 0){
                        tradeallyAccount = taList.get(0);
                        
                        if(dsmtcontact.portal_Role__c == 'Manager'){
                        companyAttachList = [select name,Attachment_Name__c,Expires_On__c,Expired_Text__c,Business_Name_Text__c,File_Download_Url__c,File_Url__c,
                                     FileKey__c,Attachment_Type__c,Contractor_Instruction__c from attachment__c where Trade_Ally_Account__c =: tradeallyAccount.Id and Dsmtracker_Contact__c = null and status__c = 'Approved'];
                               }      
                                     
                        contactList = [select id,name,Phone__c,Email__c,Title__c,Active__c from Dsmtracker_Contact__c where review__c = null and Trade_ally_Account__c =: tradeallyAccount.Id and status__c = 'Approved'  and Active__c = true];             
                        
                     }
            }
            
        }
    }

   
    /******************* Fetching States from Custom Settings*******/
    public List < SelectOption > getStateList() {
        List < SelectOption > options = new List < SelectOption > ();
        options.add(new SelectOption(' ', '-- Select State --'));

        // Find all the countries in the custom setting
        Map < String, dsmt_Reg_States__c > states = dsmt_Reg_States__c.getAll();
        // Sort them by name
        List < String > stateNames = new List < String > ();
        stateNames.addAll(states.keySet());
        stateNames.sort();

        for (String statName: stateNames) {
            dsmt_Reg_States__c state = states.get(statName);
            options.add(new SelectOption(state.State_Name__c, state.Name));
        }
        return options;
    }

    /******************* Fetching Questions from Custom Settings*******/
    public List < SelectOption > getQuestions() {
        List < SelectOption > options = new List < SelectOption > ();

        // Find all the countries in the custom setting
        Map < String, SecurityQuestion__c > states = SecurityQuestion__c.getAll();

        // Sort them by name
        List < String > stateNames = new List < String > ();
        stateNames.addAll(states.keySet());
        stateNames.sort();

        for (String statName: stateNames) {
            SecurityQuestion__c state = states.get(statName);
            options.add(new SelectOption(state.Question__c, state.Question__c));
        }
        return options;
    }
    
    
  
}