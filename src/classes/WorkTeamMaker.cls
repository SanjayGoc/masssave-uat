public with sharing class WorkTeamMaker {

    private Date startDate; 
    private Integer daysOfCoverageNeeded;

    /**
    * Sunday, December 31, 2107 starts a new rotation of alternate schedules; we'll use that as our reference 
    */
    private static final Date zeroDate = Date.newInstance(2017, 12, 31);

    /**
    * @description Constructor 
    *
    * @param    startDate               Date on which to start looking for missing Work Teams
    * @param    daysOfCoverageNeeded    Integer representing the number of days to check for missing Work Teams 
    */
    public WorkTeamMaker(Date startDate, Integer daysOfCoverageNeeded){
        this.startDate = startDate;
        this.daysOfCoverageNeeded = daysOfCoverageNeeded;
    }

    /**
    * @description Based on the input date, return the correct week designation (ie. 'Week A', 'Week B', etc.) 
    *
    * @param    inputDate           Date for which we need the week designation
    * @param    weekDesignations    List of Strings representing the possible week desingations, eg. {'WEEK A', 'WEEK B'}
    * @return                       String that matches the Schedule_Week__c field on the Schedule_Line_Item__c, eg. 'Week A'
    */
    @testVisible private static String getWeekDesignation(Date inputDate, Set<String> weekDesignations){
        if (weekDesignations.isEmpty()) return '';
        List<String> sortedWeekDesignations = new List<String>(weekDesignations);
        sortedWeekDesignations.sort();
        Integer daysSinceZeroDate = zeroDate.daysBetween(inputDate);
        Integer remainder = Math.mod(daysSinceZeroDate, weekDesignations.size()*7);
        Integer indexForReturn = ((Double)remainder/7).intValue();
        return sortedWeekDesignations[indexForReturn];
    }

    public void createWorkTeamsForEmployees(List<Employee__c> employees){
        Set<Id> scheduleIds = new Set<Id>();
        Set<Id> employeeIds = new Set<Id>();
        for(Employee__c employee : employees)
        {
            scheduleIds.add(employee.Schedule__c);
            employeeIds.add(employee.Id);
        }
        if(scheduleIds.isEmpty()) return; 
        List<Work_Team__c> workTeamsToInsert = new List<Work_Team__c>();
        List<Schedule_Line_Item__c> scheduleLineItems = [SELECT 
                Id,
                Schedule_Week__c,
                Schedules__c,
                Unavailable__c,
                WeekDay__c
            FROM Schedule_Line_Item__c WHERE Schedules__c IN : scheduleIds];
        Set<String> weekDesignations = new Set<String>();
        Map<String, Schedule_Line_Item__c> scheduleLineItemsByScheduleWeekDesignationAndDayOfWeek = new Map<String, Schedule_Line_Item__c>();
        for (Schedule_Line_Item__c scheduleLineItem : scheduleLineItems){
            String key = scheduleLineItem.Schedules__c+scheduleLineItem.Schedule_Week__c+scheduleLineItem.WeekDay__c;
            scheduleLineItemsByScheduleWeekDesignationAndDayOfWeek.put(key, scheduleLineItem);
            weekDesignations.add(scheduleLineItem.Schedule_Week__c);
        }
        Date endDate = startDate.addDays(daysOfCoverageNeeded);
        List<Work_Team__c> existingWorkTeams = [SELECT Id, Service_Date__c, Captain__c
                FROM Work_Team__c 
                WHERE Service_Date__c >=: startDate 
                AND Service_Date__c <=: endDate
                AND Captain__c IN: employeeIds];
        Set<String> existingWorkTeamCaptainsAndServiceDates = new Set<String>();
        for(Work_Team__c existingWorkTeam : existingWorkTeams){
            existingWorkTeamCaptainsAndServiceDates.add(existingWorkTeam.Captain__c+''+existingWorkTeam.Service_Date__c);
        }
        for(Employee__c employee : employees){
            for(Integer i = 0; i <= daysOfCoverageNeeded; i++){
                Date serviceDate = startDate.Adddays(i);
                if (existingWorkTeamCaptainsAndServiceDates.contains(employee.Id+''+serviceDate)) continue;
                String serviceDateDayOfWeek = DateTime.newInstance(serviceDate, Time.newInstance(0, 0, 0, 0)).format('EEEE');
                String weekDesignation = getWeekDesignation(serviceDate, weekDesignations);
                String scheduleLineItemKey = employee.Schedule__c+weekDesignation+serviceDateDayOfWeek;
                Schedule_Line_Item__c scheduleLineItem = scheduleLineItemsByScheduleWeekDesignationAndDayOfWeek.get(scheduleLineItemKey);
                workTeamsToInsert.add(
                    new Work_Team__c(
                        Location__c = employee.Location__c,
                        Territory__c = employee.Territory__c,
                        Service_Date__c = serviceDate,
                        Captain__c = employee.Id,
                        Name = employee.Name,
                        Schedule_Line_Item__c = scheduleLineItem == null ? null : scheduleLineItem.Id,
                        Unavailable__c = employee.Status__c == 'Inactive' || !employee.Active__c || (scheduleLineItem != null && scheduleLineItem.Unavailable__c)
                    )
                );
            }
        }
        if(!workTeamsToInsert.isEmpty()){
            dsmtHelperClass.syncWorkTeam = true;
            insert workTeamsToInsert;
        }
    }
}