global class dsmtNaturalSourceCouponsController {
    /* Global Variables */
    global CouponsModal mdl{get{
        if(this.mdl==null)
            this.mdl = new CouponsModal();
        return this.mdl;
    }set;}
    global String dsmtProdId{get;set;}
    global String couponId{get;set;}
    global String custId{get;set;}
    global String quickC{get;set;}
    
    /* Constructor */
    global dsmtNaturalSourceCouponsController(){
        this.dsmtProdId = getUrlParameterValue('id');  
        this.couponId = getUrlParameterValue('copId');
        this.custId = getUrlParameterValue('custId');
        this.quickC = getUrlParameterValue('quickC');
    }
    
    /* Methods */
    global void init(){
        this.mdl.stepErrors.clear();
        this.mdl.previousPage = this.mdl.currentPage;
        if(this.dsmtProdId!=null && this.dsmtProdId!=''){
            List<DSMTracker_Product__c> dsmtProductList = [
                SELECT Id,
                (SELECT Id, Qualified_Product_List_Master__c, Qualified_Product_List_Master__r.Name, Product_URL__c,
                 Qualified_Product_List_Master__r.MOU__c, Qualified_Product_List_Master__r.MOU__r.Retailer__c,
                 Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name,
                 Qualified_Product_List_Master__r.MOU__r.Account__c,
                 Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name_Formula__c,
                 Qualified_Product_List_Master__r.MOU__r.Retailer__r.Account_Status__c 
                 FROM Qualified_Product_Lists__r
                 WHERE Qualified_Product_List_Master__r.Program__r.Account__r.Name =:this.mdl.cs.Account_Name__c 
                 //and Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name not in ('BestBuy.com')
                 ORDER BY Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name
                )
                FROM DSMTracker_Product__c
                WHERE Id =: dsmtProdId
            ];
            System.debug('dsmtProductList-----> '+dsmtProductList);
            if(dsmtProductList.size()>0){
               this.mdl.dsmtProduct = dsmtProductList.get(0); 
            }
        }
        if(this.custId!=null && this.custId!=''){
            List<Customer__c> lstCust = [
                SELECT Id, Name, First_Name__c, Last_Name__c, 
                Service_Street__c, Service_City__c, Service_State__c, Service_Zipcode__c, Customer_Phone__c, Email_address__c,Unit__c, 
                Account__c, Home_Type__c, How_do_you_heat_your_home__c, How_do_you_cool_your_home__c, Retailer_Name_Formula__c, Retailer__c 
                FROM Customer__c
                WHERE Id=:custId
            ];
            if(lstCust.size()>0){
                if(quickC!=null && quickC=='true'){
                    System.debug('quickC:::'+quickC);
                	this.mdl.customer = lstCust.get(0); 
                    this.mdl.coupon = updateQuickCoupon();
                    this.mdl.prodUrl = this.mdl.coupon.Product_URL__c;
                     this.mdl.selectedRetailer = lstCust.get(0).Retailer__c; 
                    System.debug('Couponnnnnn:::===='+this.mdl.coupon);
                    if(this.mdl.coupon!=null){
                        this.mdl.confirmMessage = createConfirmationMessage();
                        this.mdl.coupon.Confirmation_Message__c = String.valueOf(this.mdl.confirmMessage).replace(' A copy of this coupon has also been emailed to you.','');
                        UPDATE this.mdl.coupon;
                        this.mdl.currentPage = 'SUCCESS';
                    }
                    else{
                        String errorMsg = this.mdl.getErrorMessage('Error Message No Coupon Found', true, true);
                        this.mdl.stepErrors.add(errorMsg);  
                    }
                }
                else{
                	this.mdl.customer = lstCust.get(0); 
                    if(lstCust.get(0).Retailer__c!=null && lstCust.get(0).Retailer__c!=''){
                        this.mdl.selectedRetailer = lstCust.get(0).Retailer__c;    
                    }
                }  
            }
        }
        if(this.couponId!=null && this.couponId!=''){
            List<Coupon__c> lstCop = [SELECT Id, Name, Qualified_Product_List__r.Product_URL__c, Product_URL__c, Display_Format__c,Expiration_Date__c,Amount__c,
                                      		Confirmation_Message__c,Retailer_Name__c, Retailer_Name_Formula__c FROM Coupon__c WHERE Id=:couponId LIMIT 1];
            if(lstCop.size()>0){ 
                this.mdl.coupon = lstCop.get(0);
                this.mdl.confirmMessage=lstCop.get(0).Confirmation_Message__c;                
                this.mdl.selectedRetailer = lstCop.get(0).Retailer_Name__c;  
                this.mdl.expDate = convertDateToReadableFormat(lstCop.get(0).Expiration_Date__c);
                if(lstCop.get(0).Product_URL__c!= null){
                    this.mdl.prodUrl =lstCop.get(0).Product_URL__c;
                }else{
                    this.mdl.prodUrl =lstCop.get(0).Qualified_Product_List__r.Product_URL__c;
                }
                this.mdl.currentPage = 'SUCCESS';
            }
        }
    }
    global void searchCustomer(){
        try{
            this.mdl.backButtonPressed = '';
            this.mdl.stepErrors.clear();
            this.mdl.previousPage = this.mdl.currentPage;     
            List<Account> tmpAccount = dsmtNaturalSourceCouponsUtil.searchCustomer(this.mdl);  
            if(tmpAccount!=null && tmpAccount.size()>0){
                this.mdl.account = tmpAccount; 
                this.mdl.isEligible = dsmtNaturalSourceCouponsUtil.isEligible(this.mdl); 
                if(this.mdl.isEligible){ 
                    this.mdl.coupon = updateCouponCreateCustomer();  
                    if(this.mdl.coupon!=null){
                        this.mdl.currentPage = 'SUCCESS';
                        this.mdl.confirmMessage = createConfirmationMessage();
                        this.mdl.coupon.Confirmation_Message__c = this.mdl.confirmMessage;
                        UPDATE this.mdl.coupon; 
                    }else{
                        this.mdl.refId = fillInvalidCustomer(); 
                        String errorMsg = this.mdl.getErrorMessage('Error Message No Coupon Found', true, true);
                        this.mdl.stepErrors.add(errorMsg);     
                    }
                }else{
                    this.mdl.refId = fillInvalidCustomer(); 
                    String errorMsg = this.mdl.getErrorMessage('Error Message Past Partition', true, true);
                    this.mdl.stepErrors.add(errorMsg);  
                }
            }else{
                if(this.mdl.attempt<this.mdl.cs.Allowed_Attempt__c){ 
                    if(!this.mdl.isAddressVerified){
                        this.mdl.refId = fillInvalidCustomer(); 
                        String errorMsg = this.mdl.getErrorMessage('Error Message 1&2', true, true);  
                        this.mdl.stepErrors.add(errorMsg);          
                    }else if(!this.mdl.isAccountFound){
                        this.mdl.refId = fillInvalidCustomer(); 
                        String errorMsg = this.mdl.getErrorMessage('System_Fuel_Type_Does_not_Macth', true, true);  
                        this.mdl.stepErrors.add(errorMsg);          
                    }
                }else if(this.mdl.attempt>=this.mdl.cs.Allowed_Attempt__c){
                    this.mdl.refId = fillInvalidCustomer(); 
                    String errorMsg = this.mdl.getErrorMessage('Error Message 3', true, false).replace('{!mdl.refId}',this.mdl.refId);   
                    this.mdl.stepErrors.add(errorMsg);      
                } 
            } 
        }catch(Exception e){
            System.debug('Exception----> '+e.getStackTraceString());
            this.mdl.stepErrors.add(e.getMessage()); 
        }        
    }
    public Coupon__c updateQuickCoupon(){
        Coupon__c coupon = null;  
        List<Coupon__c> couponList = [
            SELECT id, Name, Retailer_Name__c, Customer__c, Status__c, Customer_Date__c,
            Amount__c, Expiration_Date__c, Expiration_Days__c, Customer_Email__c, 
            Customer_Phone__c, Product_URL__c, Qualified_Product_List__r.Product_URL__c, Display_Format__c, Show_Barcode__c,
            Qualified_Product_List__c, Qualified_Product_List__r.Program__c, 
            Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Timeframe__c,
            Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Number_of_Years__c
            FROM Coupon__c
            WHERE Retailer_Name__c =: this.mdl.customer.Retailer__c
            AND Status__c = 'New'
            AND Start_Date__c <= :Date.today() AND Expiration_Date__c >= :Date.today()
            AND Program_Account_Name__c =:mdl.cs.Account_Name__c ORDER BY CreatedDate DESC LIMIT 10
        ];
        System.debug('mdl.cs.Account_Name__c:::'+mdl.cs.Account_Name__c);
        System.debug('this.mdl.selectedRetailer:::'+this.mdl.selectedRetailer);
        if(couponList.size()>0){
            this.mdl.customer = upsertCustomer(false);
            coupon = couponList.get(0);
            coupon.Customer_Phone__c = this.mdl.customer.Customer_Phone__c;
            coupon.Customer_Email__c = this.mdl.customer.Email_address__c;
            coupon.Customer_Date__c = System.today();
            coupon.Reserved_Date__c = System.today(); 
            coupon.City__c = this.mdl.customer.Service_City__c;
            coupon.State__c = this.mdl.customer.Service_State__c;
            coupon.Account_Name__c = this.mdl.cs.Account_Name__c; 
             
            if(coupon.Expiration_Date__c != null){
                this.mdl.expirationDate = coupon.Expiration_Date__c;
                this.mdl.expDate = convertDateToReadableFormat(coupon.Expiration_Date__c);
            }else if(coupon.Expiration_Days__c != null){
                this.mdl.expirationDate = System.today().addDays(integer.valueOf(coupon.Expiration_Days__c));
                this.mdl.expDate = convertDateToReadableFormat( System.today().addDays(integer.valueOf(coupon.Expiration_Days__c)));
            }
            coupon.Status__c = 'Reserved';
            coupon.Customer__c = this.mdl.customer.Id;
            if(coupon.Product_URL__c  == null || coupon.Product_URL__c  == ''){
                coupon.Product_URL__c = coupon.Qualified_Product_List__r.Product_URL__c;
            }
            this.mdl.customer.Coupon_Name__c = coupon.Name;
            UPSERT this.mdl.customer;
            UPDATE coupon;
            return coupon;
        }
        else{
            return null;
        }
    }
    public String createConfirmationMessage(){
        String confMsg = '';
        System.debug('mdl.coupon.Display_Format__c----> '+mdl.coupon.Display_Format__c);
        System.debug('this.mdl.confMsgMap---> '+this.mdl.confMsgMap);
        System.debug('this.mdl.procodeUrlMap---> '+this.mdl.procodeUrlMap);
        System.debug('this.mdl.selectedRetailer---> '+this.mdl.selectedRetailer);
        if(this.mdl.coupon.Product_URL__c!=null && this.mdl.coupon.Product_URL__c!=''){
            this.mdl.prodUrl = this.mdl.coupon.Product_URL__c;
            //confMsg = confMsg.replace('href=""', 'href="'+this.mdl.coupon.Product_URL__c+'"');     
        }else{
            this.mdl.prodUrl = this.mdl.coupon.Qualified_Product_List__r.Product_URL__c; 
            //confMsg = confMsg.replace('href=""', 'href="'+this.mdl.prodUrl+'"');      
        }
        /*
        if(this.mdl.confMsgMap.containsKey(this.mdl.selectedRetailer)){
            confMsg = this.mdl.confMsgMap.get(this.mdl.selectedRetailer).Checklist_Information__c; 
        }else{
            confMsg = this.mdl.confMsgMap.containsKey('ALL') ? this.mdl.confMsgMap.get('ALL').Checklist_Information__c : 'Confirmation Message is not configured';
        }
        System.debug('confMsg---> '+confMsg);
        if(confMsg!='' && confMsg.contains('{!mdl.coupon.Amount__c}')){
            confMsg = confMsg.replace('{!mdl.coupon.Amount__c}', String.valueOf(this.mdl.coupon.Amount__c)); 
        }
        System.debug('confMsg---> 2'+confMsg);
        if(confMsg!='' && confMsg.contains('{!mdl.coupon.Name}')){
            confMsg = confMsg.replace('{!mdl.coupon.Name}', String.valueOf(this.mdl.coupon.Name)); 
        }
        if(confMsg!='' && confMsg.contains('{!mdl.coupon.Expiration_Date__c}')){
            confMsg = confMsg.replace('{!mdl.coupon.Expiration_Date__c}', convertDateToReadableFormat(this.mdl.coupon.Expiration_Date__c));
        }
        if(confMsg!='' && confMsg.contains('href=""')){
            if(this.mdl.coupon.Product_URL__c!=null && this.mdl.coupon.Product_URL__c!=''){
                this.mdl.prodUrl = this.mdl.coupon.Product_URL__c;
             	confMsg = confMsg.replace('href=""', 'href="'+this.mdl.coupon.Product_URL__c+'"');     
            }else{
                this.mdl.prodUrl = this.mdl.coupon.Qualified_Product_List__r.Product_URL__c; 
                confMsg = confMsg.replace('href=""', 'href="'+this.mdl.prodUrl+'"');      
            }
            System.debug('this.mdl.prodUrl---> '+this.mdl.prodUrl);
            
        }*/
        return confMsg;
    }
    public Coupon__c updateCouponCreateCustomer(){
        Coupon__c coupon = null; 
        Set<String> qplIds = new Set<String>(); 
        if(mdl.dsmtProduct!=null && mdl.dsmtProduct.Qualified_Product_Lists__r.size() > 0){
            for(Qualified_Product_List__c rec : mdl.dsmtProduct.Qualified_Product_Lists__r){
                qplIds.add(rec.Id);
            }    
        }
        System.debug('qplIds----> '+qplIds);
        System.debug('this.mdl.selectedRetailer----> '+this.mdl.selectedRetailer);
        List<Coupon__c> couponList = [
            SELECT id, Name, Retailer_Name__c, Customer__c, Header__c, Footer__c, Status__c, Customer_Date__c,
            Amount__c, Expiration_Date__c, Expiration_Days__c, Customer_Email__c, Retailer_Name_Formula__c,
            Customer_Phone__c, Product_URL__c, Display_Format__c, Show_Barcode__c,
            Qualified_Product_List__c, Qualified_Product_List__r.Program__c, 
            Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Timeframe__c,
            Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Number_of_Years__c,
            Qualified_Product_List__r.Product_URL__c
            FROM Coupon__c
            WHERE Retailer_Name__c =: this.mdl.selectedRetailer
            AND Qualified_Product_List__c IN : qplIds AND Qualified_Product_List__c != null
            AND Status__c = 'New'
            AND Start_Date__c <= :Date.today() AND Expiration_Date__c >= :Date.today() ORDER BY CreatedDate DESC LIMIT 10
        ];  
        if(couponList.size()>0){
            this.mdl.customer = upsertCustomer(true);
            coupon = couponList.get(0);
            coupon.Customer_Phone__c = this.mdl.customer.Customer_Phone__c;
            coupon.Customer_Email__c = this.mdl.customer.Email_address__c;
            coupon.Customer_Date__c = System.today();
            //coupon.Account__c = this.mdl.account.Id; 
            coupon.City__c = this.mdl.customer.Service_City__c;
            coupon.State__c = this.mdl.customer.Service_State__c;
            coupon.Retailer_Name__c = this.mdl.selectedRetailer; 
            coupon.Account_Name__c = this.mdl.cs.Account_Name__c;  
            if(coupon.Expiration_Date__c != null){
                this.mdl.expirationDate = coupon.Expiration_Date__c;
                this.mdl.expDate = convertDateToReadableFormat(coupon.Expiration_Date__c);
            }else if(coupon.Expiration_Days__c != null){
                this.mdl.expirationDate = System.today().addDays(integer.valueOf(coupon.Expiration_Days__c));
                this.mdl.expDate = convertDateToReadableFormat(System.today().addDays(integer.valueOf(coupon.Expiration_Days__c)));
            }
            coupon.Status__c = 'Reserved';
            coupon.Customer__c = this.mdl.customer.Id;
            this.mdl.customer.Coupon_Name__c = coupon.Name;
            UPSERT this.mdl.customer;
            UPDATE coupon;
        }
        return coupon;
    } 
    public Customer__c upsertCustomer(boolean isValid){
        System.debug('this.mdl.customer---> '+this.mdl.customer); 
        
        /*Create Premise*/
        Premise__c prem = new Premise__c();
        prem.Address__c =  this.mdl.customer.Service_Street__c !=null ? this.mdl.customer.Service_Street__c : '';
        if(this.mdl.customer.Unit__c!=null && this.mdl.customer.Unit__c!=''){
            prem.Address__c +=' '+this.mdl.customer.Unit__c;    
        }
        prem.City__c =  this.mdl.customer.Service_City__c !=null ? this.mdl.customer.Service_City__c : '';
        prem.State__c =  this.mdl.customer.Service_State__c !=null ? this.mdl.customer.Service_State__c : '';
        prem.Zip_Code__c =  this.mdl.customer.Service_Zipcode__c !=null ? this.mdl.customer.Service_Zipcode__c : '';
        insert prem;
        
        /*Create Premise end*/
        this.mdl.customer.Name = (this.mdl.customer.First_Name__c == null ? '' : this.mdl.customer.First_Name__c) +' '+ this.mdl.customer.Last_Name__c;  
        this.mdl.customer.Retailer__c = this.mdl.selectedRetailer; 
        
        if(this.mdl.dsmtProduct != null && String.valueOf(this.mdl.dsmtProduct.Id)!=''){
            this.mdl.customer.DSMTracker_Product__c = this.mdl.dsmtProduct.Id;
        }
        if(this.mdl.dsmtProduct != null && this.mdl.dsmtProduct.Qualified_Product_Lists__r.size() > 0){
            this.mdl.customer.Utility__c = this.mdl.dsmtProduct.Qualified_Product_Lists__r[0].Qualified_Product_List_Master__r.MOU__r.Account__c;
        } 
        if(!isValid){
            this.mdl.customer.Valid_Customer__c=false;
            List<Group> queueList = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'Coupon_Validation_Queue'];
            if(queueList.size() > 0)
                this.mdl.customer.OwnerId = queueList[0].Id;    
        }else{
            this.mdl.customer.Valid_Customer__c=true;
        }
        this.mdl.customer.premise__c = prem.id;
        
        /* Changing here */
        if(this.mdl.account!=null && this.mdl.account.size()>0){
            List<Customer__c> lstCust = new List<Customer__c>(); 
            for(Account acc: mdl.account){
                if(acc.Utility_Service_Type__c=='Electric'){
                    lstCust = acc.Customers1__r; 
                    if(lstCust!=null && lstCust.size()>0 && mdl.utilityType == 'Electric'){
                        this.mdl.customer.Id = lstCust.get(0).Id;   
                    }else{
                        this.mdl.customer.Electric_provider__c = acc.Utility__c;
                        this.mdl.customer.Electric_Provider_Name__c = acc.Provider__c;
                        this.mdl.customer.Electric_Account__c = acc.Id;  
                        this.mdl.customer.Electric_Account_Number__c = acc.Billing_Account_Number__c;
                    }
                }else if(acc.Utility_Service_Type__c=='Gas'){
                    lstCust = acc.Customers2__r;  
                    if(lstCust!=null && lstCust.size()>0 && mdl.utilityType == 'Gas'){
                        this.mdl.customer.Id = lstCust.get(0).Id;   
                    }else{ 
                        this.mdl.customer.Gas_provider__c = acc.Utility__c;
                        this.mdl.customer.Gas_Provider_Name__c = acc.Provider__c;  
                        this.mdl.customer.Gas_Account_Number__c = acc.Billing_Account_Number__c;
                        this.mdl.customer.Gas_Account__c = acc.Id;  
                    } 
                }else{
                    
                }
            }
        }
        /*
        if(this.mdl.account!=null){
            System.debug('this.mdl.account---> '+this.mdl.account);
            System.debug('this.mdl.utilityType---> '+this.mdl.utilityType);
            List<Customer__c> lstCust = new List<Customer__c>();
            if(mdl.utilityType == 'Electric'){
                lstCust = this.mdl.account.Customers1__r;    
                System.debug('lstCust-Electric-> '+lstCust);
                if(lstCust!=null && lstCust.size()>0){
                    this.mdl.customer.Id = lstCust.get(0).Id;    
                    this.mdl.customer.Electric_Account__c = this.mdl.account.Id;  
                }else{
                    this.mdl.customer.RecordTypeId = Schema.SObjectType.Customer__c.getRecordTypeInfosByName().get('EverSource Instant Rebate').getRecordTypeId();
                    this.mdl.customer.Electric_Account__c = this.mdl.account.Id;  
                }
                System.debug('this.mdl.Electric---> '+this.mdl.customer);
            }else if(mdl.utilityType == 'Gas'){
                lstCust = this.mdl.account.Customers2__r; 
                System.debug('lstCust-Gas-> '+lstCust);
                if(lstCust!=null && lstCust.size()>0){
                    this.mdl.customer.Id = lstCust.get(0).Id;  
                    this.mdl.customer.Gas_Account__c = this.mdl.account.Id;  
                }else{
                    this.mdl.customer.RecordTypeId = Schema.SObjectType.Customer__c.getRecordTypeInfosByName().get('EverSource Instant Rebate').getRecordTypeId();
                    this.mdl.customer.Gas_Account__c = this.mdl.account.Id;  
                }  
                System.debug('this.mdl.Gas---> '+this.mdl.customer);
            }else{ 
                this.mdl.customer.RecordTypeId = Schema.SObjectType.Customer__c.getRecordTypeInfosByName().get('EverSource Instant Rebate').getRecordTypeId();  
            } 
        }*/
        
        UPSERT  this.mdl.customer;
        System.debug('this.mdl.customer----> '+this.mdl.customer);
        return  this.mdl.customer;
    }
    
    public String fillInvalidCustomer(){ 
        this.mdl.customer = upsertCustomer(false);
        return  this.mdl.customer.External_Customer_Id__c;
    } 
	/* Modal */
    global class CouponsModal{
        global String backButtonPressed{get;set;}
        global String currentPage{get{
            if(currentPage == null || currentPage == '')
                currentPage = 'DEFAULT';
            return currentPage;
        }set;}
        global String previousPage{get;set;}
        global List<String> stepErrors{get{
            if(this.stepErrors == null){
                this.stepErrors = new List<String>();
            }
            return this.stepErrors;
        }set;}  
        global boolean hasError{get{return this.stepErrors.size() > 0;}} 
        global List<Account> account {get;set;}
        global Coupon__c coupon {get;set;}        
        global Customer__c customer{get{
            if(this.customer == null)
                this.customer = new Customer__c();
            return this.customer;
        }set;} 
        global DSMTracker_Product__c dsmtProduct;
        global Map<String, Checklist_Items__c> checkListConfigMap{get{ 
            System.debug('checkListName---> '+checkListName);
            if(checkListConfigMap == null){
                this.checkListConfigMap = new Map<String, Checklist_Items__c>();
                List<Checklist_Items__c> chkList = [SELECT Id, Section__c, Checklist_Information__c, Retailer_Name__c, Retailer_Name_Formula__c, Reference_ID__c
                                                    FROM Checklist_Items__c
                                                    WHERE Parent_Checklist__r.Unique_Name__c =: this.checkListName];
                
                if(chkList.size() > 0){
                    for(Checklist_Items__c chk : chkList){
                        this.checkListConfigMap.put(chk.Section__c, chk);        
                    }
                }
            }
            System.debug('this.checkListConfigMap--> '+this.checkListConfigMap);
            return this.checkListConfigMap;
        }set;}
        @TestVisible
        private String checkListName;
        global EverSourceIR_Site__c cs{get{
            if(this.cs == null)
                cs = EverSourceIR_Site__c.getInstance();
            return this.cs;
        }set;}
        global List<SelectOption> stateList {get{
            List<SelectOption> retailerList = new List<SelectOption>();            
            retailerList.add(new SelectOption('MA', 'Massachusetts')); 
            
            return retailerList;
        }}
        global String selectedState{get{
            if(this.selectedState==null)
               this.selectedState ='';
            return this.selectedState;
            
        }set;}
        global List<SelectOption> propertType {get{
            List<SelectOption> propOpt = new List<SelectOption>();
            propOpt.add(new SelectOption('', '--None--'));
            propOpt.add(new SelectOption('Single-family (1-2 residential units)', 'Single-family (1-2 residential units)'));
            propOpt.add(new SelectOption('Multi-family (3+ residential units)', 'Multi-family (3+ residential units)'));
            return propOpt;
        }set;}  
        global String selectedDrType{get;set;}
        global List<SelectOption> drType {get{
            List<SelectOption> propOpt = new List<SelectOption>();
            propOpt.add(new SelectOption('', '--None--'));
            propOpt.add(new SelectOption('Yes', 'Yes'));
            propOpt.add(new SelectOption('No', 'No'));
            return propOpt;
        }set;} 
        global String selectedPropType{get;set;}
        global List<SelectOption> thermoInstList {get{
            List<SelectOption> thermoOpt = new List<SelectOption>();
            thermoOpt.add(new SelectOption('', '--None--'));
            thermoOpt.add(new SelectOption('Central air conditioner and furnace', 'Central air conditioner and furnace'));
            thermoOpt.add(new SelectOption('Central air conditioner and electric resistance heat', 'Central air conditioner and electric resistance heat'));
            thermoOpt.add(new SelectOption('Electric resistance heat (no central air conditioner)', 'Electric resistance heat (no central air conditioner)'));
            thermoOpt.add(new SelectOption('Heat pump', 'Heat pump'));
            return thermoOpt;
        }set;}
        global String selectedThemoInst{get;set;} 
        global List<SelectOption> thermoReplaList {get{
            List<SelectOption> thermoReplOpt = new List<SelectOption>();
            thermoReplOpt.add(new SelectOption('', '--None--'));
            thermoReplOpt.add(new SelectOption('Manual thermostat', 'Manual thermostat'));
            thermoReplOpt.add(new SelectOption('Programmable thermostat', 'Programmable thermostat'));
            thermoReplOpt.add(new SelectOption('Unknown', 'Unknown')); 
            return thermoReplOpt;
        }set;}
        global String selectedThemoRepl{get;set;} 
        global List<SelectOption> retailerList {
            get {
                List<SelectOption> retailerList = new list<SelectOption>();            
                retailerList.add(new SelectOption('', '- None -'));    
                System.debug('dsmtProduct---. '+dsmtProduct);
                if(dsmtProduct != null){
                    Set<String> UniqueRetailerNameSet = new Set<String>();
                    for(Qualified_Product_List__c qpl : dsmtProduct.Qualified_Product_Lists__r){
                        if(qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name_Formula__c != null && qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Account_Status__c != 'Inactive'
                           && UniqueRetailerNameSet.add(qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name)){
                            //retailerList.add(new SelectOption(qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name_Formula__c, qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name));
                            retailerList.add(new SelectOption(qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name, qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name));
                        }
                    }
                }            
                return retailerList;
            }
        }
        global String selectedRetailer {get;set;}
        global String selectedRetailerType {get{
            if(this.selectedRetailer!=null && this.selectedRetailer!=''){
                List<Account> lstAcc = [SELECT Id, Name,Retailer_type__c FROM Account WHERE Name =:selectedRetailer LIMIT 1];
                if(lstAcc.size()>0){
                    return lstAcc.get(0).Retailer_type__c;
                }else{
                    return '';
                }
            }else{
                return '';
            }
        }set;}
        global Integer attempt{get{
            if(this.attempt==null)
                this.attempt=0;
            return this.attempt;
        }set;}
        global boolean isEligible{get{
            if(this.isEligible==null)
                this.isEligible = false;
            return this.isEligible;
        }set;}
        global String confirmMessage{get;set;}
        global Date expirationDate{get;set;}
        global String rating{get;set;}
        global boolean allowedToContinue{get{
            if(this.allowedToContinue == null)
                this.allowedToContinue =true;
            return this.allowedToContinue; 
        }set;}
        global String refId{get{
            if(this.refId==null)
                this.refId = '';
            return this.refId;
        }set;} 
        global boolean isAddressVerified{get{
            if(this.isAddressVerified==null)
                isAddressVerified=false;
            return isAddressVerified;
        }set;} 
        global boolean isAccountFound{get{
            if(this.isAccountFound==null)
                isAccountFound=false; 
            return isAccountFound;
        }set;}
        global String BillingStreet{get;set;}
        global String UnitNo{get;set;}
        global String BillingCity{get;set;}
        global String BillingPostalcode{get;set;}  
        global String prodUrl{get{
            if(prodUrl==null)
                prodUrl='';
            return prodUrl;
        }set;} 
        global Map<String, String> procodeUrlMap{get{
            if(this.procodeUrlMap == null){
                this.procodeUrlMap = new Map<String, String>();
                if(dsmtProduct != null){
                    for(Qualified_Product_List__c qpl : dsmtProduct.Qualified_Product_Lists__r){
                        if(qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name != null){
                            this.procodeUrlMap.put(qpl.Qualified_Product_List_Master__r.MOU__r.Retailer__r.Name, qpl.Product_URL__c);
                        }
                    }
                }
            } 
            return this.procodeUrlMap;
        }set;}
        global Map<String, Checklist_Items__c> confMsgMap {get{
            if(this.confMsgMap == null){
                this.confMsgMap = new Map<String, Checklist_Items__c>();
                if(checkListConfigMap!=null && checkListConfigMap.size()>0){
                    for(Checklist_Items__c ch: checkListConfigMap.values()){
                        if(ch.Reference_ID__c!=null && ch.Reference_ID__c=='Confirmation_Message'){
                        	this.confMsgMap.put(ch.Retailer_Name__c, ch);    
                        }
                    }    
                }
            }
            System.debug('this.confMsgMap----> '+this.confMsgMap);
            return this.confMsgMap;
        }set;}
        global String expDate{get{
            if(expDate==null)
                expDate='';
            return expDate;
        }set;}
        global String utilityType{get{
            if(utilityType==null)
                utilityType='';
            return utilityType;
        }set;}
        global CouponsModal(){
           init();
        }  
        private void init(){
            this.account = new List<Account>();
            this.coupon = new Coupon__c();
            this.dsmtProduct = new DSMTracker_Product__c(); 
            this.customer = new Customer__c();
            this.cs = EverSourceIR_Site__c.getInstance(UserInfo.getProfileId());
            this.checkListName = cs!=null ? cs.Checklist_Name__c:null;
        }
        global void moveToStep(){
            this.stepErrors.clear();
            backButtonPressed = 'backActionPerformed';
            String stepToMove = ApexPages.currentPage().getParameters().get('stepToMove'); 
            if(stepToMove != null && stepToMove != ''){
                this.currentPage = stepToMove;
            } 
        }
        global String getErrorMessage(String checkListName, boolean increateAttempt, boolean allowToContinue){
            String errorMessage = '';
            if(checkListName!=''){
                errorMessage = this.checkListConfigMap.get(checkListName)!=null 
                        && this.checkListConfigMap.containsKey(checkListName) 
                        && this.checkListConfigMap.get(checkListName).Checklist_Information__c!=''
                        ?this.checkListConfigMap.get(checkListName).Checklist_Information__c:'Please configure Message for '+checkListName;                
            }
            if(increateAttempt){
                this.attempt++;
            } 
            this.allowedToContinue = allowToContinue; 
            return errorMessage;
        }
    }
    /* Util Method */
    global static String getUrlParameterValue(String key){
        return ApexPages.currentPage().getParameters().containsKey(key) ? ApexPages.currentPage().getParameters().get(key):'';
    }
    /* RemoteMethods */
    @RemoteAction
    global static void saveBarcodeImage(String couponId, String blobValue){
        blobValue = blobValue.replace('data:image/png;base64,', '');
        blobValue = blobValue.replace(' ', '+');
        System.debug('blobValue ::::::'+ blobValue);
        
        Attachment attach = new Attachment();
        attach.Body = EncodingUtil.base64Decode(blobValue);
        attach.Name = 'barcode.png';
        attach.ContentType = 'image/png';
        attach.ParentID = couponId;
        INSERT attach;
        
        System.debug('attach.Body ::::::'+ attach.Body);
        
        Document doc = new Document();
        doc.Body = EncodingUtil.base64Decode(blobValue);
        doc.Name = 'barcode.png';
        doc.ContentType = 'image/png';
        doc.FolderId = [SELECT Id FROM Folder WHERE Name = 'Coupon Attachments'].Id;
        doc.IsPublic = true;
        INSERT doc;
        
        UPDATE new Coupon__c(id = couponId, AttachmentUrl__c = doc.Id);
    }
    
    @RemoteAction
    global static void saveRating(String couponId, String rating){
        UPDATE new Coupon__c(id = couponId, Customer_Rating__c = rating);
    }
    
    public String convertDateToReadableFormat(Date d){
        String retStr = '';
        Map<Integer, String> mapMonth = new Map<Integer, String>{
            1=>'January',
                2=>'February',
                3=>'March',
                4=>'April',
                5=>'May',
                6=>'June',
                7=>'July',
                8=>'August',
                9=>'September',
                10=>'October',
                11=>'November',
                12=>'December'
                }; 
                    if(d!=null){
                        String month = mapMonth.get(d.month());
                        //String day = String.valueOf(d.day())+''+getDayOfMonthSuffix(d.day());
                        String day = String.valueOf(d.day())+', ';
                        String year = String.valueOf(d.year());
                        
                        retStr = month+' '+day+' '+ year;
                    }
        return retStr;
    }
    
    public String getDayOfMonthSuffix(Integer n) {
        if (n == null) {
            return '';
        } 
        if (n >= 11 && n <= 13) {
            return 'th';
        }
        
        Integer modResult = Math.mod(n, 10);        
        if (modResult == 1) { 
            return 'st'; 
        } else if (modResult == 2) { 
            return 'nd'; 
        } else if (modResult == 3) { 
            return 'rd'; 
        } else { 
            return 'th';
        }
    }
}