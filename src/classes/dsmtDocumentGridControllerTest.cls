@isTest
public with sharing class dsmtDocumentGridControllerTest{

    public static testmethod void test(){
        test.startTest();
        Account acc = new Account();
        acc.Name = 'Xcel Energy NM';
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        acc.Billing_Account_Number__c= hexDigest.substring(0,11);
        insert acc;

        
        Invoice__c invobj = new Invoice__c();
        invobj.invoice_date__c = date.Today();
        invobj.Account__c = acc.Id;
        insert invObj;
        
        Invoice_Line_Item__c invl = new Invoice_Line_Item__c();
        invl.invoice__c = invObj.Id;
        insert invl;
        
        Recommendation_Scenario__c rs = new Recommendation_Scenario__c();
        insert rs;
        
        /*    
            Energy_Assessment__c ea = new Energy_Assessment__c();
            insert ea;
        */
    
        //Temporary soql till dev team handle null exception.
    
        Recommendation__c r = new Recommendation__c();
        insert r; 
    
        Trade_Ally_Account__c TAcc = new Trade_Ally_Account__c(name='test');
        insert TAcc;
    
        Review__c rev = new Review__c(Email__c='test@test.com',Trade_Ally_Account__c=TAcc.id,Status__c = 'Draft', Requested_By__c = Userinfo.getuserid(),Review_Type__c='Document');
        insert rev;
    
        Attachment__c attch = new Attachment__c();
        attch.Project__c = rs.Id;
        insert attch;
    
        ApexPages.StandardController sc = new ApexPages.StandardController(rev);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(invObj);
        ApexPages.StandardController sc2 = new ApexPages.StandardController(invl);
        ApexPages.StandardController sc3 = new ApexPages.StandardController(rs);
        //ApexPages.StandardController sc4 = new ApexPages.StandardController(ea);
        ApexPages.StandardController sc5 = new ApexPages.StandardController(r);
        
        dsmtDocumentGridController  cntrl = new dsmtDocumentGridController(sc);
        cntrl.getAllExistingAttachments();
        dsmtDocumentGridController  cntrl1 = new dsmtDocumentGridController(sc1);
        cntrl1.getAllExistingAttachments();
        dsmtDocumentGridController  cntrl2 = new dsmtDocumentGridController(sc2);
        cntrl2.getAllExistingAttachments();
        dsmtDocumentGridController  cntrl3 = new dsmtDocumentGridController(sc3);
        cntrl3.getAllExistingAttachments();
        /*dsmtDocumentGridController  cntrl4 = new dsmtDocumentGridController(sc4);
        cntrl4.getAllExistingAttachments();*/
        dsmtDocumentGridController  cntrl5 = new dsmtDocumentGridController(sc5);
        cntrl5.getAllExistingAttachments();
        
        test.stopTest();
    }

}