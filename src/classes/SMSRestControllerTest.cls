@istest
public class SMSRestControllerTest 
{
    static Customer__c cust=new Customer__c();
    static Employee__c em=new Employee__c();
    static Appointment__c app=new Appointment__c();
  static void setUpData()
    {
        
        Account  acc= Datagenerator.createAccount();
        acc.Billing_Account_Number__c= 'Gas-~~~123445';
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Account acc3 = new Account();
        acc3.Name = 'Xcel Energy NM';
        acc3.Billing_Account_Number__c= 'Ele-~~~1234';
        acc3.Utility_Service_Type__c= 'Electric';
        acc3.Account_Status__c= 'Active';
        insert acc3;
        
        
        Premise__c pre= Datagenerator.createPremise();
        insert pre;
        
        cust = Datagenerator.createCustomer(acc2.id,pre.id);
        //cust.Electric_Account__c=acc3.id;
        cust.Gas_Account__c=acc2.id;
        insert cust;
        Location__c loc= Datagenerator.createLocation();
        insert loc;
        em = Datagenerator.createEmployee(loc.id);
        em.Status__c='Approved';
        insert em;
         
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        EL.How_many_units__c='Single family';
        EL.How_many_do_you_own__c  = '1';
        
        update EL;  
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
        app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        update app;
    }
    static testmethod void test()
    {
        test.startTest();
        setUpData();
        SMS__c sms = new SMS__c();
        sms.Mobile_No__c='1234567812';
        sms.Message_Text__c='hello';
        sms.Response__c='sfklhsf';
        sms.Appointment__c=app.id;
        sms.Customer__c=cust.id;
        sms.Employee__c=em.id;
        insert sms;
        
        Appointment__c app =new Appointment__c ();
        //app.id=sms.Id;
        insert app;
        
        Workorder__c wo =new Workorder__c ();
        wo.Status__c='Cancel Requested';
        wo.Early_Arrival_Time__c=DateTime.newInstance(DateTime.now().year(), DateTime.now().month(), DateTime.now().day(), 10, DateTime.now().minute(),DateTime.now().second());
        wo.Late_Arrival_Time__c=DateTime.newInstance(DateTime.now().year(), DateTime.now().month(), DateTime.now().day(), 11, DateTime.now().minute(),DateTime.now().second());
        insert wo;
        
        //ApexPages.currentPage().getparameters().put('From');
        
        ApexPages.currentPage().getparameters().put('Body',sms.Message_Text__c);
        ApexPages.currentPage().getparameters().put('From',sms.Mobile_No__c);
        ApexPages.currentPage().getparameters().put('SmsSid',sms.Id);
        SMSRestController sc =new SMSRestController();
        sc.saveSMS();
        
        SMS__c sms2 = new SMS__c();
        sms2.Mobile_No__c='1234567812';
        sms2.Message_Text__c='hello';
        sms2.Response__c='sfklhsf';
        sms2.Message_Id__c=sms.id;
        insert sms2;
        
        sc.saveSMS();
        test.stopTest();
        
    }
}