@isTest
Public class dsmtTradeAllyProfileUpdateContTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        insert obj;            
       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
       //  dsmt.Contact__c= cont.id;
         //update dsmt;
        Review__c rev= Datagenerator.createReview(Tacc.Id);
        Attachment__c att= Datagenerator.createAttachment(Tacc.Id);
        
        ApexPages.currentPage().getParameters().put('dcid',dsmt.Id);
        ApexPages.currentPage().getParameters().put('id',rev.Id);
        ApexPages.currentPage().getParameters().put('taid',Tacc.Id);
        dsmtTradeAllyProfileUpdateCont controller = new dsmtTradeAllyProfileUpdateCont();
       // controller.TAId=Tacc.Id;
        //controller.TAId = Tacc.Id;
        controller.loadTradeallyAccount();
        controller.loadReviewData();
        controller.loadDsmtrackerContact();
        controller.checkReview();
        controller.copyAttachmentToReview(rev.Id);
        controller.saveAsDraft();
        controller.saveAsSubmit();
        controller.getStateList();

    }
      static testMethod void case1(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;

       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
       //  dsmt.Contact__c= cont.id;
         //update dsmt;
        Review__c rev= Datagenerator.createReview(Tacc.Id);
        Attachment__c att= Datagenerator.createAttachment(Tacc.Id);
      //  ApexPages.currentPage().getParameters().put('taid',Tacc.Id);
        ApexPages.currentPage().getParameters().put('dcid',dsmt.Id);
     //   ApexPages.currentPage().getParameters().put('id',rev.Id);
        dsmtTradeAllyProfileUpdateCont controller = new dsmtTradeAllyProfileUpdateCont();
       // controller.TAId=Tacc.Id;
        controller.loadTradeallyAccount();
        controller.loadReviewData();
        controller.loadDsmtrackerContact();
        controller.checkReview();
        controller.copyAttachmentToReview(rev.Id);
        controller.saveAsDraft();
      //  controller.saveAsSubmit();
        controller.getStateList();
        String url = controller.PortalURL;
        String org= controller.orgId;

    }
}