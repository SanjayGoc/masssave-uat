public class EANotesComponentController {
    public Id parentId{get;set;}
    public Note__c newNote{get{
        if(this.newNote == null){
            this.newNote = new Note__c();
        }
        return this.newNote;
    }set;}
    /*public List<Note> getNotes(){
        return [SELECT Id,Title,Body,CreatedDate FROM Note WHERE ParentId =:parentId ORDER BY CreatedDate DESC LIMIT 100];
    }*/
    
    @RemoteAction
    public static List<Note__c> queryNotes(Id parentId){
        Schema.SObjectType sobjectType = parentId.getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();
        
        return database.query('SELECT Id, Title__c, Body__c, CreatedDate FROM Note__c WHERE '+ sobjectName +' =:parentId ORDER BY CreatedDate DESC LIMIT 100');
    }
    
    public void saveNote(){
        String eassessId = ApexPages.currentPage().getParameters().get('assessId');
        
        if(this.newNote!= null && parentId != null){
            try{
                this.newNote.Energy_Assessment__c = eassessId;
                Schema.SObjectType sobjectType = parentId.getSObjectType();
                String sobjectName = sobjectType.getDescribe().getName();
                
                this.newNote.put(sobjectName, parentId);
            }catch(Exception ex){
                // FIELD NOT WRITABLE
                // 
                String noteId = this.newNote.Id;
                this.newNote = this.newNote.clone(false);
                this.newNote.Energy_Assessment__c = parentId;
                if(noteId != null && noteId.trim() != ''){
                    this.newNote.Id = noteId;
                }
            }
            if(!isValidSalesforceId(this.newNote.Id,Note.Class)){
                this.newNote.Id = null;
            }
            UPSERT this.newNote;
            
            /*if(eassessId != parentId)
            {
                Note__c note = this.newNote.clone(false, false);
                note.Energy_Assessment__c = eassessId;
                
                insert note;
            }*/
            
            this.newNote = null; 
        }
    }
    public void editNote(){
        String noteIdToEdit = getParam('noteIdToEdit');
        if(noteIdToEdit != null && noteIdToEdit.trim() != ''){
            List<Note__c> result = [SELECT Id, Title__c, Body__c, Energy_Assessment__c 
                                    FROM Note__c 
                                    WHERE Id =:noteIdToEdit];
            if(result.size() > 0){
                this.newNote = result.get(0);
            }
        }
    }
    public void deleteNote(){
        String noteId = getParam('noteId');
        if(noteId != null)
            delete new Note__c(Id = noteId);
    }
    
    private Boolean isValidSalesforceId( String sfdcId, System.Type t ){
        try {
            if ( Pattern.compile( '[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}' ).matcher( sfdcId ).matches() ){
                Id id = sfdcId;
                sObject sObj = (sObject) t.newInstance();
                sObj.Id = id;
                return true;
            }
        } catch ( Exception e ){ 
            // StringException, TypeException 
        }
        return false;
    }
    public String getParam(String paramKey){
        return ApexPages.currentPage().getParameters().get(paramKey);
    }
}