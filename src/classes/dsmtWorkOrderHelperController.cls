public class dsmtWorkOrderHelperController{
    
    public static string CancelWorkOrder(Id WoId){
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        req.setEndpoint(System_Config__c.getInstance().URL__c+'UpdateWorkOrderStatus?type=json');
        req.setMethod('POST');
        dsmtCallOut.CancelWorkOrderRequest cworder = new dsmtCallOut.CancelWorkOrderRequest();
        cworder.workOrderId = WoId;
        cworder.status = 'Cancelled';
        cworder.orgId = userinfo.getOrganizationId();
        String body = JSON.serialize(cworder);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        http = new Http();
        if(!Test.IsrunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
            return res.getBody();
        }
        return '';
    }
    
     public static string updateWorkOrder(String WoId,String Status){
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        req.setEndpoint(System_Config__c.getInstance().URL__c+'UpdateWorkOrderStatus?type=json');
        req.setMethod('POST');
        dsmtCallOut.CancelWorkOrderRequest cworder = new dsmtCallOut.CancelWorkOrderRequest();
        cworder.workOrderId = WoId;
        cworder.status = Status;
        cworder.orgId = userinfo.getOrganizationId();
        String body = JSON.serialize(cworder);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        http = new Http();
        if(!Test.IsrunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
            return res.getBody();
        }
        return '';
    }
    
    public static string OptimizeRoute(string optimizerId, date startDate, date endDate){
        dsmtCallOut.loginDetail ldObj = new dsmtCallOut.loginDetail();
        ldObj.sessionID = userinfo.getSessionId();
        ldObj.userName = Login_Detail__c.getInstance().UserName__c;
        ldObj.password = Login_Detail__c.getInstance().Password__c;
        ldObj.orgID = Login_Detail__c.getInstance().Org_Id__c;
        ldObj.securityToken = Login_Detail__c.getInstance().Security_Token__c;
        ldObj.serverURL = Login_Detail__c.getInstance().Server_URL__c;
        
        dsmtCallOut.Body bodyobj = new dsmtCallOut.Body();
        bodyobj.optimizerId = optimizerId;
        bodyobj.fromDate = startDate;
        bodyobj.toDate = endDate;
        bodyobj.currentTime = '';
        bodyobj.TimeZone = '-7';
        bodyobj.BatchMode = true;
        bodyObj.loginDetail  = ldObj;
        bodyObj.orgId = userinfo.getOrganizationId();
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        req.setEndpoint(System_Config__c.getInstance().URL__c+'OptimizeSchedule?type=json');
        req.setMethod('POST');
        String body = JSON.serialize(bodyObj);
        
        system.debug('--body ---'+body);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        http = new Http();
        if(!Test.IsrunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
            return res.getBody();
        }
        return '';
    }
}