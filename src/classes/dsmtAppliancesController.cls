public class dsmtAppliancesController extends dsmtEnergyAssessmentBaseController {
    public Appliance__c newAppliance{get;set;}
    public String recordTypeName {get;set;}
    public Map<String,List<Appliance__c>> appliancesByCategory {get;set;}
    public transient Map<Id,Appliance__c> appliancesById {get;set;} /* DSST-12300 | Kaushik Rathore | 28 Sept, 2018 */
    
    public dsmtAppliancesController()
    {
        fetchAppliancesByCategory();
    }
    
    public list<SelectOption> getApplianceCategoryOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Appliance__c.Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    
    public list<SelectOption> getDHWList()
    {
        String eassId = Apexpages.currentPage().getParameters().get('assessId');
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--Select DHW--'));
        
        set<String> rtSet = new set<String>{'Heat Pump', 'Storage Tank', 'Indirect Storage Tank', 'On Demand', 'Combo', 'Boiler + Indirect Storage Tank', 'Boiler + Tankless Coil'};
        
        for(Mechanical_Sub_Type__c a : [select id, Name 
                                        from Mechanical_Sub_Type__c 
                                        where Mechanical_Type__r.Mechanical__r.Energy_Assessment__c =: eassId 
                                        and RecordType.Name in : rtSet
                                        order by Name])
        {
            soList.add(new SelectOption(a.Id, a.Name));
        }
        
        return soList;
    }
    
    public Map<String,List<Appliance__c>> getAppliancesString(){
        appliancesByCategory = fetchAppliancesByCategory();
        //appliancesByCategoryCopy = new Map<String,List<Appliance__c>>(appliancesByCategory); /* DSST-12300 | Kaushik Rathore | 28 Sept, 2018 */
        return appliancesByCategory;
    }
     
    public Map<String,List<Appliance__c>> fetchAppliancesByCategory()
    {
        appliancesByCategory = new Map<String,List<Appliance__c>>();
        appliancesById = new Map<Id,Appliance__c>(); /* DSST-12300 | Kaushik Rathore | 28 Sept, 2018 */
        if(ea != null && ea.Id != null){
            String eaId = ea.Id;
            for(Appliance__c apl : Database.query('SELECT ' + getSObjectFields('Appliance__c') + ', RecordType.Name, RecordType.DeveloperName FROM Appliance__c WHERE Energy_Assessment__c=:eaId order by CreatedDate desc')){
                if(apl.Category__c != null){
                    List<Appliance__c> tempList = appliancesByCategory.get(apl.Category__c);
                    if(tempList == null){
                        tempList = new List<Appliance__c>();
                    }
                    tempList.add(apl);
                    appliancesByCategory.put(apl.Category__c, tempList);
                }
                appliancesById.put(apl.Id,apl.clone(true,true)); /* DSST-12300 | Kaushik Rathore | 28 Sept, 2018 */
            }   
        }
        
        return appliancesByCategory;
    }
    
    private Integer nextAutoNumber(String category,String type)
    {
        Integer next = 1;
        Map<String,List<Appliance__c>> m = appliancesByCategory;
        if(m.containsKey(category)){
            for(Appliance__c apl : m.get(category)){
                if(apl.Type__c == type){
                    next++;
                }
            }
        }
        return next;
    }
    
    public void addNewAppliance()
    {
        String category = getParam('category');
        String type = getParam('type');
        String location = getParam('location');
        String recordType = category + ' ' + type;
        recordTypeName = category + ' ' + type;
        recordTypeName = recordTypeName.replace(' ', '_');
        String recordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        
        try{
            Integer nextNumber = nextAutoNumber(category,type);
            
            this.newAppliance = new Appliance__c(
                recordTypeId = recordTypeId,
                Category__c = category,
                Type__c = type,
                Location__c = location,
                Energy_Assessment__c = ea.Id,
                Name = type + ' ' + nextNumber
            );
            
            INSERT this.newAppliance;
            
            if(this.newAppliance.Id != null){
                String applianceId= this.newAppliance.Id;
                List<Appliance__c> apls = Database.query('SELECT ' + getSObjectFields('Appliance__c') + ',RecordType.Name,RecordType.DeveloperName FROM Appliance__c WHERE Id=:applianceId');
                if(apls.size() > 0){
                    this.newAppliance = apls.get(0);
                }
            }
        }catch(Exception ex){
            System.debug('EXCEPTION :: ' + ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
            this.newAppliance = null;
        }
    }
    
    public void saveAppliance()
    {
        if(this.newAppliance != null){
            /* DSST-12496 | Kaushik Rathore | 28 Sept, 2018 : START */
            if(this.newAppliance.Type__c == 'Refrigeration' && this.newAppliance.Metered_kWh__c != null){
                this.newAppliance.Rated_kWh__c = this.newAppliance.Metered_kWh__c;
                this.newAppliance.Actual_Rated_kWh__c = this.newAppliance.Metered_kWh__c;
            }
            /* DSST-12496 | Kaushik Rathore | 28 Sept, 2018 : END */
            UPDATE this.newAppliance;
            
            String applianceId = this.newAppliance.Id;
            List<Appliance__c> apls = Database.query('SELECT ' + getSObjectFields('Appliance__c') + ',RecordType.Name,RecordType.DeveloperName FROM Appliance__c WHERE Id=:applianceId');
            if(apls.size() > 0){
                this.newAppliance = apls.get(0);
                recordTypeName = this.newAppliance.RecordType.DeveloperName;
            }
        }
    }
    
    public void saveAllAppliance()
    {
        if(this.appliancesByCategory != null){
            List<Appliance__c> aplList = new List<Appliance__c>();
            
            for(String key : appliancesByCategory.keyset())
            {
                /* START : DSST-12300 | Kaushik Rathore | 1 Oct 2018 */
                //aplList.addAll(appliancesByCategory.get(key));
                List<Appliance__c> newList = appliancesByCategory.get(key); 
                for(Appliance__c apl : newList){

                    /* DSST-12496 | Kaushik Rathore | 28 Sept, 2018 : START */
                    if(apl.Type__c == 'Refrigeration' && apl.Metered_kWh__c != null){
                        apl.Rated_kWh__c = apl.Metered_kWh__c;
                        apl.Actual_Rated_kWh__c = apl.Metered_kWh__c;
                    }
                    /* DSST-12496 | Kaushik Rathore | 28 Sept, 2018 : END */

                    /*Map<String,Object> oldCopy = appliancesById.get(apl.Id).getPopulatedFieldsAsMap();
                    Map<String,Object> newCopy = apl.getPopulatedFieldsAsMap();
                    
                    Integer changesFound = 0;
                    for(String field : oldCopy.keySet()){
                        if(oldCopy.get(field) != newCopy.get(field)){
                            changesFound++;
                            break;
                        }
                    }*/
                    
                    //if(changesFound > 0){
                        aplList.add(apl);
                    //}
                }
                /* END : DSST-12300 | Kaushik Rathore | 1 Oct 2018 */
            }
            /* DSST-12496 | Kaushik Rathore | 28 Sept, 2018 : START */
            /*for(Appliance__c apl : aplList){
                if(apl.Type__c == 'Refrigeration' && apl.Metered_kWh__c != null){
                    apl.Rated_kWh__c = apl.Metered_kWh__c;
                    apl.Actual_Rated_kWh__c = apl.Metered_kWh__c;
                }
            }*/
            /* DSST-12496 | Kaushik Rathore | 28 Sept, 2018 : END */
           
            /* START : DSST-12300 | Prasanjeet Sharma | 1 Oct 2018 */
            if(aplList != null && aplList.size() > 0)
            { 
                dsmtRecursiveTriggerHandler.preventApplianceTrigger = true;
                update aplList;
                
                UpdateAllRecords(JSON.serialize(aplList));
            }
            /* END : DSST-12300 | Prasanjeet Sharma | 1 Oct 2018 */
        }
        System.debug('END TIME : ' + System.now());
    }
    
    @future
    public static void UpdateAllRecords(String appliancesJson)
    {
        dsmtRecursiveTriggerHandler.preventApplianceTrigger = false;
        
        List<Appliance__c> aplList = (List<Appliance__c>) JSON.deserialize(appliancesJson, List<Appliance__c>.class);
        update aplList;
    }
    
    public void saveCloseAppliance()
    {
        if(this.newAppliance != null){
            UPDATE this.newAppliance;
        }
        this.newAppliance = null;
    }
    
    public void editAppliance()
    {
        String applianceId = getParam('applianceId');
        if(applianceId != null && applianceId.trim() !=''){
            List<Appliance__c> apls = Database.query('SELECT ' + getSObjectFields('Appliance__c') + ',RecordType.Name,RecordType.DeveloperName FROM Appliance__c WHERE Id=:applianceId');
            if(apls.size() > 0){
                this.newAppliance = apls.get(0);
                recordTypeName = this.newAppliance.RecordType.DeveloperName;
            }
        }else{
            saveAppliance();
            this.newAppliance = null;
        }
    }
    
    public void deleteAppliance()
    {
        String applianceId = getParam('applianceId');
        if(applianceId != null){
            DELETE new Appliance__c(Id = applianceId);
        }
    }
}