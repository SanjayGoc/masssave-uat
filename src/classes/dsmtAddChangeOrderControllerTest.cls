@istest
public class dsmtAddChangeOrderControllerTest
{
	@istest
    static void runtest()
    {
        Review__c re =new Review__c();
        re.Address__c='surat';
        insert  re;
        
        Project_Review__c pr =new Project_Review__c();
        //pr.id=re.id;
        insert pr;
        
        Recommendation__c rc =new Recommendation__c();
        insert rc;
        
        Change_Order_Line_Item__c col =new Change_Order_Line_Item__c();
        insert  col;
        
        ApexPages.currentPage().getParameters().put('id',re.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(re);
        dsmtAddChangeOrderController test = new dsmtAddChangeOrderController(sc);
        
        dsmtAddChangeOrderController daoc = new dsmtAddChangeOrderController();
        daoc.initJSON();
        daoc.CreateChagneOrderItems();
        daoc.createReviewRec();
        
    }   
}