@isTest
Public class EligibilityCheckUtilityTest{
    @isTest
    public static void runTest(){
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
         Premise__c prem = Datagenerator.createPremise();
         Customer__c cust = Datagenerator.createCustomer(acc2.Id,prem.Id);
        
        User portalUser = Datagenerator.CreatePortalUser();
        DSMTracker_Contact__c newDSMTracker = Datagenerator.createDSMTracker();
        Eligibility_Check__c elCheck = Datagenerator.createELCheck();
        elCheck.Customer__c = cust.Id;
        Appointment__c app = Datagenerator.createAppointment(elCheck.Id);
        app.Appointment_Status__c = 'Completed';
        app.DSMTracker_Contact__c = newDSMTracker.Id;
        Trade_Ally_Account__c newTAaccount = Datagenerator.createTradeAccount();
        
        EligibilityCheckUtility.checkGasutility('test');
        EligibilityCheckUtility.checkElectricutility('test');
        EligibilityCheckUtility.checkLandloardEligibility('Landlord');
        
        Program_Eligibility__c  pe = datagenerator.createProgramEL();
        
        EligibilityCheckUtility.CheckZipEligibility('1234','Gas','','',false);
        EligibilityCheckUtility.CheckZipEligibilityNew('1234','Gas','','','',false,false);
        // EligibilityCheckUtility controller = new EligibilityCheckUtility();
         EligibilityCheckUtility.newUnit('20','1','test',false,false);
         EligibilityCheckUtility.Unit('5+ units','test',true);
         EligibilityCheckUtility.WifiCheck(false,'5+ units');
         EligibilityCheckUtility.newWifiCheck(false,'5+ units','20',false);
         EligibilityCheckUtility.condoAssociation('Yes',false,'1');
         EligibilityCheckUtility.newcondoAssociation('20','Yes',false,'1',false);
         EligibilityCheckUtility.AccountCheck('Gas-~~~1234','test','64736848');
         EligibilityCheckUtility.newappAccountCheck('Gas-~~~1234','test','1234','Gas','');
         EligibilityCheckUtility.dsmtAccountCheck('Gas-~~~1234','test','64736848');
         //EligibilityCheckUtility.newdsmtAccountCheck('Gas-~~~1234','test','1234','Gas','');
         //EligibilityCheckUtility.newAccountCheck('Gas-~~~1234','test','1234','Gas','');
    }
      @IsTest
    static void testCase2(){
    test.startTest();
        Account acc = new Account();
        acc.Name = 'Xcel Energy NM';
        acc.Billing_Account_Number__c= 'Gas-~~~1234';
        acc.Utility_Service_Type__c= 'Gas';
        acc.Account_Status__c= 'Active';
        insert acc;
        Premise__c prem = Datagenerator.createPremise();
        Customer__c cust = Datagenerator.createCustomer(acc.Id,prem.Id);
        
        User portalUser = Datagenerator.CreatePortalUser();
        DSMTracker_Contact__c newDSMTracker = Datagenerator.createDSMTracker();
        Eligibility_Check__c elCheck = Datagenerator.createELCheck();
        elCheck.Customer__c = cust.Id;
        Appointment__c app = Datagenerator.createAppointment(elCheck.Id);
        app.Appointment_Status__c = 'Completed';
        app.DSMTracker_Contact__c = newDSMTracker.Id;
        Trade_Ally_Account__c newTAaccount = Datagenerator.createTradeAccount();
        
        newDSMTracker.Contact__c = portalUser.contactid;
        update  newDSMTracker;
        update app;
        update elCheck;
        
        System.runAs(portalUser){
            //EligibilityCheckUtility.newdsmtAccountCheck('Gas-~~~1234','test','1234','Gas','');
         //EligibilityCheckUtility.newAccountCheck('Gas-~~~1234','test','1234','Gas','');
        }
        test.stopTest();        
    }
}