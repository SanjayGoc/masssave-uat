public with sharing class dsmtRecommendationXMLController {

    public integer majorCount{get;set;}
    public string  eassid{get;set;}
    public string SelectedRecId{get;set;}
    public string projectId{get;set;}
    
    public String RecommendationId{get;set;}
    
    public string SelectedRecommendationType {get;set;}
    public string error {get;set;}
    
    public string MajorXml {
        get;
        set;
    }
    
    public string DirectInstallXml{
        get;
        set;
    }
    
    public dsmtRecommendationXMLController(){
        eassid = ApexPages.currentPage().getParameters().get('eassid');
        majorCount = 0;
    }
    
    public void recommendationList() {
        string eassid = ApexPages.currentPage().getParameters().get('eassid');
        string parentId = ApexPages.currentPage().getParameters().get('parentObjId');
        string parentField = ApexPages.currentPage().getParameters().get('parentField');
        string forProposal = ApexPages.currentPage().getParameters().get('forProposal');
        string proposalType = ApexPages.currentPage().getParameters().get('proposalType');      
        String proposalStatus = 'Installed';
        Set<string> recomIdSet = new Set<String>();
        
        for(Proposal_Recommendation__c p : database.query( ' select id, Recommendation__c from Proposal_Recommendation__c where '+
                                                           ' Recommendation__r.Energy_Assessment__c =: parentId and '+
                                                           ' Recommendation__r.Energy_Assessment__c != null and ' + 
                                                           ' Proposal__r.Status__c =: proposalStatus '))
        {
            recomIdSet.add(p.Recommendation__c);
        }
        
        system.debug('recomIdSet :::::' + recomIdSet);
        
        list<Recommendation__c> recmajorlist =null;
        if(forProposal == 'true'){
            recmajorlist = database.query(' select id, category__c, Surface__c, subCategory__c,eversourceDesc__c,gridDesc__c,location__c, '+
                                                              ' unitCost__c,Units__c,Annual_Energy_Savings__c,notes__c,Calculated_Cost__c,Lighting_Location_Picklist__c,Calculated_Incentive__c,Calculated_Payback__c,DSMTracker_Product__c,DSMTracker_Product__r.Name,DSMTracker_Product__r.Eligible_Proposal_Name_Type__c from Recommendation__c  where Recommendation_Scenario__c = null and '+
                                                                parentField + ' =: parentId and '+ 
                                                                parentField + ' != null and Id not in : recomIdSet order by CreatedDate asc');
        }  else {     
            recmajorlist = database.query(' select id, category__c, DSMTracker_Product__r.Eligible_Proposal_Name_Type__c, Surface__c, subCategory__c,eversourceDesc__c,gridDesc__c,location__c, '+
                                                              ' unitCost__c,Units__c,Annual_Energy_Savings__c,notes__c,Calculated_Cost__c,Lighting_Location_Picklist__c,Calculated_Incentive__c,Calculated_Payback__c,DSMTracker_Product__c,DSMTracker_Product__r.Name from Recommendation__c  where '+
                                                                parentField + ' =: parentId and '+
                                                                parentField + ' != null order by CreatedDate asc');
        }
        
        majorCount  = recmajorlist.size();
        //LoadXml
        DirectInstallXml = '<Employees>';

        integer cnt = 1;
        for (Recommendation__c rec : recmajorlist ) {
            DirectInstallXml += '<Employee EmployeeID="'+rec.Id+'">';
            
            DirectInstallXml += '<RecId>' + rec.Id + '</RecId>';
            
            if(forProposal == 'true' && recomIdSet.contains(rec.Id))
                DirectInstallXml += '<ShowHide>true</ShowHide>';
            else
                DirectInstallXml += '<ShowHide>false</ShowHide>';
            
            DirectInstallXml += '<Rownum>' + cnt + '</Rownum>';
            DirectInstallXml += '<Category><![CDATA[' + checkNull(rec.category__c) + ']]></Category>';
            DirectInstallXml += '<SubCategory><![CDATA[' + checkNull(rec.subCategory__c)+ ']]></SubCategory>';
            DirectInstallXml += '<EversourceDesc><![CDATA[' + checkNull(rec.eversourceDesc__c)+ ']]></EversourceDesc>';
            DirectInstallXml += '<GridDesc><![CDATA[' + checkNull(rec.gridDesc__c)+ ']]></GridDesc>';
            if(rec.Location__c != null){
                DirectInstallXml += '<Location><![CDATA[' + checkNull(rec.location__c)+ ']]></Location>';
            }else if(rec.Lighting_Location_Picklist__c != null){
                DirectInstallXml += '<Location><![CDATA[' + checkNull(rec.Lighting_Location_Picklist__c )+ ']]></Location>';
            }
            else if(rec.Surface__c != null){
                DirectInstallXml += '<Location><![CDATA[' + checkNull(rec.Surface__c)+ ']]></Location>';
            }
            
            if(rec.Units__c != null)
                DirectInstallXml += '<UnitCost><![CDATA[' + decimal.valueOf(rec.Units__c).setScale(2) + ']]></UnitCost>';
            else
                DirectInstallXml += '<UnitCost></UnitCost>';
                
            DirectInstallXml += '<CalculatedCost><![CDATA[' + rec.Calculated_Cost__c+ ']]></CalculatedCost>';
            DirectInstallXml += '<CalculatedIncentive><![CDATA[' + rec.Calculated_Incentive__c+ ']]></CalculatedIncentive>';
            //DirectInstallXml += '<CalculatedPayback><![CDATA[' + rec.Calculated_Payback__c+ ']]></CalculatedPayback>';
            
            
            
            if(rec.Annual_Energy_Savings__c > 0)
                DirectInstallXml += '<CalculatedPayback><![CDATA[' + rec.Calculated_Payback__c+ ']]></CalculatedPayback>';
            else
                DirectInstallXml += '<CalculatedPayback>Infinite</CalculatedPayback>';
             DirectInstallXml += '<AnnualEnergySavings><![CDATA[' + rec.Annual_Energy_Savings__c+ ']]></AnnualEnergySavings>';
            DirectInstallXml += '<Notes>'+ checkNull(rec.notes__c).escapeHtml4() + '</Notes>'; //DSST-13060 - PP
            if(rec.DSMTracker_Product__c == null){
                DirectInstallXml += '<Part></Part>';
            }else{
                DirectInstallXml += '<Part>'+ rec.DSMTracker_Product__r.Name.replace('<','').replace('>','') +'</Part>';
            }
            
            if(forProposal == 'true' && rec.DSMTracker_Product__r.Eligible_Proposal_Name_Type__c != proposalType) {
                DirectInstallXml += '<DisableRow>True</DisableRow>';
            } else {
                DirectInstallXml += '<DisableRow>False</DisableRow>';
            }
            DirectInstallXml += '</Employee>';
            cnt++;
        }

        DirectInstallXml += '</Employees>';
        
        system.debug('--DirectInstallXml---'+DirectInstallXml);
    }
    
    public void recommendationFuelList() {
        string eassid = ApexPages.currentPage().getParameters().get('eassid');
        string parentId = ApexPages.currentPage().getParameters().get('parentObjId');
        string parentField = ApexPages.currentPage().getParameters().get('parentField');
        string forProposal = ApexPages.currentPage().getParameters().get('forProposal');        
        
        list<Recommendation__c> recmajorlist =null;
        if(forProposal == 'true'){
            recmajorlist = database.query(' select id, category__c, subCategory__c,eversourceDesc__c,gridDesc__c,location__c, FuelType__c, Fuel2Type__c, '+
                                          ' FuelUnitSaving__c, Fuel2UnitSaving__c, FuelDollerSaving__c, Fuel2DollerSaving__c, FuelUnit__c, Fuel2Unit__c, FuelCost__c, Fuel2Cost__c, '+
                                          ' unitCost__c,Units__c,Annual_Energy_Savings__c,notes__c,Calculated_Cost__c,Lighting_Location_Picklist__c,Calculated_Incentive__c,Calculated_Payback__c,DSMTracker_Product__c,DSMTracker_Product__r.Name from Recommendation__c  where Recommendation_Scenario__c = null and '+
                                            parentField + ' =: parentId and '+
                                            parentField + ' != null order by CreatedDate asc');
        }else {        
            recmajorlist = database.query(' select id, category__c, subCategory__c,eversourceDesc__c,gridDesc__c,location__c, FuelType__c, Fuel2Type__c, '+
                                          ' FuelUnitSaving__c, Fuel2UnitSaving__c, FuelDollerSaving__c, Fuel2DollerSaving__c, FuelUnit__c, Fuel2Unit__c, FuelCost__c, Fuel2Cost__c, '+
                                          ' unitCost__c,Units__c,Annual_Energy_Savings__c,notes__c,Calculated_Cost__c,Lighting_Location_Picklist__c,Calculated_Incentive__c,Calculated_Payback__c,DSMTracker_Product__c,DSMTracker_Product__r.Name from Recommendation__c  where '+
                                            parentField + ' =: parentId and '+
                                            parentField + ' != null order by CreatedDate asc');
        }
        
        majorCount  = recmajorlist.size();
        //LoadXml
        DirectInstallXml = '<Orders>';

        integer cnt = 1;
        for (Recommendation__c rec : recmajorlist ) {
            
            if(rec.FuelType__c != null)
            {
                DirectInstallXml += '<Order>';
            
                DirectInstallXml += '<EmployeeID>' + rec.Id + '</EmployeeID>';
                DirectInstallXml += '<RecId>' + rec.Id + '</RecId>';
                DirectInstallXml += '<FuelType>' + rec.FuelType__c + '</FuelType>';
                DirectInstallXml += '<UnitSaving>' + rec.FuelUnitSaving__c + '</UnitSaving>';
                DirectInstallXml += '<FuelUnit>' + rec.FuelUnit__c + '</FuelUnit>';
                DirectInstallXml += '<FuelCost>' + rec.FuelCost__c + '</FuelCost>';
                DirectInstallXml += '<AmountSaving>' + rec.FuelDollerSaving__c + '</AmountSaving>';
                
                DirectInstallXml += '</Order>';            
            }
            
            if(rec.Fuel2Type__c != null)
            {
                DirectInstallXml += '<Order>';
            
                DirectInstallXml += '<EmployeeID>' + rec.Id + '</EmployeeID>';
                DirectInstallXml += '<RecId>' + rec.Id + '</RecId>';
                DirectInstallXml += '<FuelType>' + rec.Fuel2Type__c + '</FuelType>';
                DirectInstallXml += '<UnitSaving>' + rec.Fuel2UnitSaving__c + '</UnitSaving>';
                DirectInstallXml += '<FuelUnit>' + rec.Fuel2Unit__c + '</FuelUnit>';
                DirectInstallXml += '<FuelCost>' + rec.Fuel2Cost__c + '</FuelCost>';
                DirectInstallXml += '<AmountSaving>' + rec.Fuel2DollerSaving__c + '</AmountSaving>';
                
                DirectInstallXml += '</Order>';            
            }
            cnt++;
        }

        DirectInstallXml += '</Orders>';
        
        system.debug('--DirectInstallXml---'+DirectInstallXml);
    }
    
    public void proposalRecommendationList() {
        string eassid = ApexPages.currentPage().getParameters().get('eassid');
        string parentId = ApexPages.currentPage().getParameters().get('parentObjId');
        string parentField = ApexPages.currentPage().getParameters().get('parentField');
        
        list<Proposal_Recommendation__c> recmajorlist = database.query(' select id, Recommendation__r.Surface__c, Recommendation__r.Annual_Energy_Savings__c,Recommendation__r.category__c,Recommendation__r.Units__c, Recommendation__r.subCategory__c, Recommendation__r.eversourceDesc__c, Recommendation__r.gridDesc__c, Recommendation__r.location__c, '+
                                                                      ' Recommendation__c, Recommendation__r.unitCost__c, Recommendation__r.Calculated_Cost__c,Recommendation__r.Lighting_Location_Picklist__c,Recommendation__r.Calculated_Incentive__c,Recommendation__r.Calculated_Payback__c,Recommendation__r.notes__c,Recommendation__r.DSMTracker_Product__c,Recommendation__r.DSMTracker_Product__r.Name  from Proposal_Recommendation__c where '+
                                                                        parentField + ' =: parentId and '+
                                                                        parentField + ' != null');
        
        majorCount  = recmajorlist.size();
        //LoadXml
        DirectInstallXml = '<Skills>';

        integer cnt = 1;
        for (Proposal_Recommendation__c rec : recmajorlist ) {
            
            DirectInstallXml += '<Skill>';
            
            DirectInstallXml += '<RecId>' + rec.Id + '</RecId>';
            DirectInstallXml += '<RecommendationId>' + rec.Recommendation__c + '</RecommendationId>';
            DirectInstallXml += '<Rownum>' + cnt + '</Rownum>';
            DirectInstallXml += '<Category><![CDATA[' + checkNull(rec.Recommendation__r.category__c) + ']]></Category>';
            DirectInstallXml += '<SubCategory><![CDATA[' + checkNull(rec.Recommendation__r.subCategory__c)+ ']]></SubCategory>';
            DirectInstallXml += '<EversourceDesc><![CDATA[' + checkNull(rec.Recommendation__r.eversourceDesc__c)+ ']]></EversourceDesc>';
            DirectInstallXml += '<GridDesc><![CDATA[' + checkNull(rec.Recommendation__r.gridDesc__c)+ ']]></GridDesc>';
            
            if(rec.Recommendation__r.location__c != null){
                DirectInstallXml += '<Location><![CDATA[' + checkNull(rec.Recommendation__r.location__c)+ ']]></Location>';
            }else if(rec.Recommendation__r.Lighting_Location_Picklist__c!= null){
                DirectInstallXml += '<Location><![CDATA[' + checkNull(rec.Recommendation__r.Lighting_Location_Picklist__c)+ ']]></Location>';
            }else if(rec.Recommendation__r.Surface__c != null){
                DirectInstallXml += '<Location><![CDATA[' + checkNull(rec.Recommendation__r.Surface__c)+ ']]></Location>';
            }
            
            DirectInstallXml += '<UnitCost><![CDATA[' + checkNull(rec.Recommendation__r.Units__c)+ ']]></UnitCost>';
            
            DirectInstallXml += '<CalculatedCost><![CDATA[' + rec.Recommendation__r.Calculated_Cost__c+ ']]></CalculatedCost>';
            DirectInstallXml += '<CalculatedIncentive><![CDATA[' + rec.Recommendation__r.Calculated_Incentive__c+ ']]></CalculatedIncentive>';
           
            if(rec.Recommendation__r.Annual_Energy_Savings__c > 0)
                DirectInstallXml += '<CalculatedPayback><![CDATA[' + rec.Recommendation__r.Calculated_Payback__c+ ']]></CalculatedPayback>';
            else
                DirectInstallXml += '<CalculatedPayback>Infinite</CalculatedPayback>';
            
            DirectInstallXml += '<AnnualEnergySavings><![CDATA[' + rec.Recommendation__r.Annual_Energy_Savings__c+ ']]></AnnualEnergySavings>';
                
            DirectInstallXml += '<Notes>'+ checkNull(rec.Recommendation__r.notes__c).escapeHtml4() + '</Notes>'; // DSST-13060 - PP
                                               
            
             if(rec.Recommendation__r.DSMTracker_Product__c == null){
                DirectInstallXml += '<Part></Part>';
            }else{
                DirectInstallXml += '<Part>'+ rec.Recommendation__r.DSMTracker_Product__r.Name.replace('<','').replace('>','') +'</Part>';
            }
            
            DirectInstallXml += '</Skill>';
            cnt++;
        }

        DirectInstallXml += '</Skills>';
        
        system.debug('--DirectInstallXml---'+DirectInstallXml);
    }
    
    public String checkNull(String str)
    {
        if(str == null)
            return '';
        
        return str;
    }
}