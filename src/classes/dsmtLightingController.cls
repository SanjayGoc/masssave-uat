public class dsmtLightingController extends dsmtEnergyAssessmentBaseController {
    public Lighting__c lighting{get;set;}
    public Lighting_Location__c newLightingLocation{get;set;}
    private Map<String,Integer> locationsCountMap{get;set;}
    public String viewName{get;set;}
    
    public dsmtLightingController()
    {
        viewName = 'lightingGeneralPanel';
    }
    
    public PageReference validateRequestAndLighting()
    {
        PageReference pg = validateRequest();
        if(pg == null){
            String eaId = ea.Id;
            List<Lighting__c> lightings = Database.query('SELECT ' + getSObjectFields('Lighting__c') + ' FROM Lighting__c WHERE Energy_Assessment__c=:eaId ');
            if(lightings.size() > 0){
                lighting = lightings.get(0);
            }
            else {
                lighting = new Lighting__c(
                    //Name = ea.Name + ' Lighting'
                    Name = 'General Lighting',
                    General_Indoor_Lighting__c = 'Medium',
                    Efficient_Lights__c = 'Some',
                    General_Outdoor_Lighting__c = 'Medium',
                    Energy_Assessment__c = ea.Id,
                    Has_Outdoor_Fixtures__c = true
                );
                INSERT lighting;
                /*if(lighting.Id != null){
                    ea.Lighting__c = lighting.Id;    
                    UPDATE ea;
                }*/
                Id lightingId = ea.Lighting__c;
                lightings = Database.query('SELECT ' + getSObjectFields('Lighting__c') + ' FROM Lighting__c WHERE Id=:lightingId ');
                if(lightings.size() > 0){
                    lighting = lightings.get(0);
                }
            }
        }
        return pg;
    }
    
    public void openView()
    {
        //viewName = getParam('viewName');
        
        //saveLocationDetails();
        
        if(viewName.equalsIgnoreCase('lightingLocationPanel')){
            String llId = getParam('sObjectId');
            List<Lighting_Location__c> locations = Database.query('SELECT ' + getSObjectFields('Lighting_Location__c') + ' FROM Lighting_Location__c WHERE Id=:llId');
            if(locations.size() > 0){
                newLightingLocation = locations.get(0);
            }
        }
    }
    
    public void addNewLightingLocation()
    {
        String location = 'Living Space';
        if(location != null && String.isNotEmpty(location) && lighting != null && lighting.Id != null){
            Integer locationCount = 1;
            for(Lighting_Location__c loc : getLightingLocations()){
                if(loc.Location__c == location){
                    locationCount++;
                }
            }
            this.newLightingLocation = new Lighting_Location__c(
                Location__c = location,
                Lighting__c = lighting.Id,
                Energy_Assessment__c = ea.Id,
                Name = location + ' Lighting Set ' + ' ' + locationCount
            );
            INSERT this.newLightingLocation;
            viewName = 'lightingLocationPanel';
        }
    }
    
    public void saveLocationDetails()
    {
        if(newLightingLocation != null){
            this.newLightingLocation.Energy_Assessment__c = ea.Id;
            UPSERT this.newLightingLocation;
            
            String llId = this.newLightingLocation.Id;
            List<Lighting_Location__c> locations = Database.query('SELECT ' + getSObjectFields('Lighting_Location__c') + ' FROM Lighting_Location__c WHERE Id=:llId');
            if(locations.size() > 0){
                newLightingLocation = locations.get(0);
            }
        }
        if(lighting != null){
            UPSERT this.lighting;
            Id lightingId = this.lighting.Id;
            List<Lighting__c> lightings = Database.query('SELECT ' + getSObjectFields('Lighting__c') + ' FROM Lighting__c WHERE Id=:lightingId');
            if(lightings.size() > 0){
                lighting = lightings.get(0);
            }
        }
    }
    
    public void saveCloseLocationDetails()
    {
        if(newLightingLocation != null){
            this.newLightingLocation.Energy_Assessment__c = ea.Id;
            UPSERT this.newLightingLocation;
        }
        if(lighting != null){
            UPSERT this.lighting;
        }
        viewName = 'listPanel';
    }
    
    public void deleteLocation()
    {
        String locationId = getParam('locationId'); 
        if(locationId != null)
        {
            DELETE new Lighting_Location__c(Id = locationId);
        }
    }
    
    public list<SelectOption> getLightingLocationOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Lighting_Location__c.Location__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    
    public List<Lighting_Location__c> getLightingLocations()
    {
        List<Lighting_Location__c> locations = new List<Lighting_Location__c>();
        if(this.lighting != null && this.lighting.Id != null){
            String lightingId = lighting.Id;
            return Database.query('SELECT ' + getSObjectFields('Lighting_Location__c') + ' FROM Lighting_Location__c WHERE Lighting__c=:lightingId');
        }
        return locations;
    }
}