@isTest
public class AddReviewRecommendationExtnTest
{
    public testmethod static void test1()
    {
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
         
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        rc.Installed_Quantity__c = 10;
        rc.Change_Order_Quantity__c =5;
        rc.Invoice_Type__c ='NSTAR413';
        rc.Installed_Quantity__c =5;
        insert rc;
        
        Proposal__c p = new Proposal__c(Name_Type__c = 'Weatherization');
        insert p;
         
        Proposal_Recommendation__c prr = new Proposal_Recommendation__c();
        prr.Project__c = proj.id;
        prr.Proposal__c=p.id;
        prr.Recommendation__c =rc.id;
        insert prr;

        Review__c rev = new Review__c(Project__c = proj.id);
        rev.Status__c='New';
        insert rev;

        Project_Review__c pr = new Project_Review__c();
        pr.Project__c = proj.id;
        pr.Review__c =rev.id;
        insert pr;
         
        ApexPages.currentPage().getParameters().put('projects',proj.id);
        ApexPages.currentPage().getParameters().put('revid',rev.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(pr);
        AddReviewRecommendationExtn cntrl = new AddReviewRecommendationExtn (sc);
        
        cntrl.createReviewRec();
        cntrl.loadRecommendations();
        cntrl.dataSave = rc.id+'-test';
        cntrl.saveProjId();
    }
    
     public testmethod static void test2(){
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;

        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        rc.Installed_Quantity__c = 10;
        rc.Change_Order_Quantity__c =5;
        rc.Invoice_Type__c ='NSTAR413';
        rc.Installed_Quantity__c =5;
        rc.Change_Order_Reason__c='test';
        insert rc;
         
        Proposal__c p = new Proposal__c(Name_Type__c = 'Weatherization');
        insert p;
         
        Proposal_Recommendation__c prr = new Proposal_Recommendation__c();
        prr.Recommendation__c = rc.id;
        prr.Proposal__c =p.id;
        insert prr;
         
        Review__c rev1 = new Review__c(Project__c = proj.id);
        rev1.Status__c='New';
        insert rev1;
         
		ID rectypeid = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();
        Review__c rev = new Review__c(Project__c = proj.id);
        rev.RecordTypeId=rectypeid;
         rev.Status__c='New';
         rev.Billing_Review__c=rev1.id;
        insert rev;
       
        Project_Review__c pr = new Project_Review__c();
        pr.Project__c = proj.id;
        pr.Review__c =rev.id;
        insert pr;
        
        ApexPages.currentPage().getParameters().put('projid',proj.id);
        ApexPages.currentPage().getParameters().put('id',rev.id);
        ApexPages.currentPage().getParameters().put('recId',rc.id);
        AddReviewRecommendationExtn cntrl = new AddReviewRecommendationExtn ();
         
        cntrl.createReviewRec();
        cntrl.loadRecommendations();
        cntrl.dataSave = rc.id+'-test';
        cntrl.saveProjId();
       
        ApexPages.currentPage().getParameters().put('projid',null);
        ApexPages.currentPage().getParameters().put('id',null);
         
        cntrl.loadRecommendations();
    }
    @istest
    Public Static Void RuntestN(){
        Set<Id> propId = new Set<Id>();
        Map<Id,Id> rsMap = new Map<Id,Id>();
        Proposal__c p = new Proposal__c(Name_Type__c = 'Weatherization');
        insert p;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Proposal__c = p.id;
        insert proj;
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        rc.Installed_Date__c = date.today();
        rc.Unit_Cost__c = 12;
        rc.Quantity__c = 1;
        
        insert rc;
  
        Proposal_Recommendation__c prr = new Proposal_Recommendation__c();
        //prr.Project__c = proj.id;
        prr.Proposal__c=p.id;
        prr.Recommendation__c =rc.id;
        insert prr;
        
        Review__c rev = new Review__c(Project__c = proj.id);
        rev.Status__c='New';
        insert rev;
        
        Project_Review__c pr = new Project_Review__c();
        pr.Project__c = proj.id;
        pr.Review__c =rev.id;
        insert pr;
        update prr;
        ApexPages.currentPage().getParameters().put('projects',proj.id);
        ApexPages.currentPage().getParameters().put('revid',rev.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(pr);
        AddReviewRecommendationExtn cntrl = new AddReviewRecommendationExtn (sc);
        
        cntrl.createReviewRec();
        cntrl.loadRecommendations();
        cntrl.dataSave = rc.id+'-test';
        cntrl.saveProjId();
        cntrl.cancelProjects();
        
    }
}