@istest
public class dsmtInspectLineItemsTest
{
    public static testmethod void test()
    {
        Inspection_Request__c irc = new Inspection_Request__c();
        insert irc; 
        
        Inspection_Line_Item__c ili =new Inspection_Line_Item__c();
        ili.Inspection_Request__c =irc.Id;
        ili.Installed_Quantity__c = 0 ;
        
        insert ili;
           
        ApexPages.currentPage().getParameters().put('irId',irc.Id);
   
        dsmtInspectLineItems dsili = new dsmtInspectLineItems ();
        
        dsili.init();
        dsili.loadLineItemXml();
        
        dsili.save();
        
    }
}