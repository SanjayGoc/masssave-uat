public class CalculationTriggerHelper{
    public static void buildingSpecificationChanged(Energy_Assessment__c ea,Building_Specification__c bs){
        System.debug('FLOOR AREA :: ' + bs.Floor_Area__c);
        System.debug('PREVOIUS FLOOR AREA :: ' + bs.Previous_Floor_Area__c);
        //if(bs.Floor_Area__c != bs.Previous_Floor_Area__c)
        //{
            UpdateWindows(ea, null);
            updateExteriorWallsAndItsLayers(ea);
            updateAtticWallsAndItsLayers(ea);
        //}
    }
    
    public static void WallChanged(Thermal_Envelope_Type__c wall, wall__c wall2)
    {
        if(wall !=null)
        {
            if(wall.Previous_Area__c != null && wall.Exterior_Area__c !=wall.Previous_Area__c)
                UpdateWindows(null, wall);
        }
        else if(wall2 !=null)
        {
        
        }    
    }
    
    private static void UpdateWindows(Energy_Assessment__c ea, Thermal_Envelope_Type__c windowWall)
    {    
        List<Window__c> winList = null;
        
        if(windowWall !=null) 
        {
            winList = [Select Id From Window__c Where Actual_Wall__c =:windowWall.Id];
        }
        else
        {
           winList = [Select Id From Window__c Where Thermal_Envelope_Type__r.Energy_Assessment__c =:ea.Id];
        }
    
        if(winList !=null && winList.size() >0)
            update winList;
        
    }
    
    private static void updateAtticWallsAndItsLayers(Energy_Assessment__c ea){
        Set<Id> atticIds = new Set<Id>();
        for(Thermal_Envelope_Type__c attic : [ SELECT Id,Name,RecordType.Name FROM Thermal_Envelope_Type__c WHERE Energy_Assessment__c=:ea.Id AND RecordType.Name = 'Attic']){
            atticIds.add(attic.Id);
        }
        if(atticIds.size() > 0){
            List<Wall__c> atticWalls = [
                SELECT Id,Name ,(SELECT Id,Name FROM Layers__r WHERE RecordType.Name IN('Wood Frame','Insulation'))
                FROM Wall__c 
                WHERE Id IN :atticIds
            ];
            if(atticWalls.size() > 0){
                List<Layer__c> layersToUpdate = new List<Layer__c>();
                List<Wall__c> wallsToUpdate = new List<Wall__c>();
                
                for(Wall__c atticWall : atticWalls){
                    for(Layer__c layer : atticWall.Layers__r){
                        layersToUpdate.add(layer);
                    }
                    atticWall.Area__c=null;
                    wallsToUpdate.add(atticWall);
                }
                
                if(layersToUpdate.size() > 0){
                    UPDATE layersToUpdate;
                }
                if(wallsToUpdate.size() > 0){
                    UPDATE wallsToUpdate;
                }
            }
        }
    }
    private static void updateExteriorWallsAndItsLayers(Energy_Assessment__c ea){
        List<Thermal_Envelope_Type__c> teTypes = [
            SELECT Id,Name,RecordType.Name,
                (SELECT Id,Name FROM Layers__r WHERE RecordType.Name IN('Wood Frame','Insulation')) 
            FROM Thermal_Envelope_Type__c 
            WHERE Energy_Assessment__c=:ea.Id AND RecordType.Name = 'Exterior Wall'
        ];
        
        if(teTypes.size() > 0){
            List<Layer__c> layersToUpdate = new List<Layer__c>();
            List<Thermal_Envelope_Type__c> teTypesToUpdate = new List<Thermal_Envelope_Type__c>(); 
            
            for(Thermal_Envelope_Type__c teType : teTypes){
                for(Layer__c layer : teType.Layers__r){
                    layersToUpdate.add(layer);
                }
                teType.Exterior_Area__c = null;
                teTypesToUpdate.add(teType);
            }
            
            if(layersToUpdate.size() > 0){
                UPDATE layersToUpdate;
            }
            if(teTypesToUpdate.size() > 0){
                UPDATE teTypesToUpdate;
            }
        }
    }
    
    @future
    public static void updateAndReCalculateExteriorWallNetArea(Id exteriorWallId,Id eaId){
        if(exteriorWallId != null && eaId != null){
        
            UPDATE [SELECT Id FROM Energy_Assessment__c WHERE Id =:eaId];
            List<Window__c> windows = [SELECT Id FROM Window__c WHERE Actual_Wall__c =:exteriorWallId];
            if(windows == null || windows.size() == 0){
                UPDATE [Select Id From Window__c Where Thermal_Envelope_Type__r.Energy_Assessment__c =:eaId];
            }else{
                UPDATE windows;
            }
            
            Thermal_Envelope_Type__c exteriorWall = new Thermal_Envelope_Type__c(
                Id = exteriorWallId,
                Stop_Trigger__c = true,
                Net_Area__c = null
            );
            UPDATE exteriorWall;
        }
    }
    
    @future 
    public static void updateAndReCalculateThermalEnvelopeTypeRoofs(Id tetId){
        List<Ceiling__c> roofs = [SELECT Id FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:tetId];
        if(roofs.size() > 0){
            Map<Id,Ceiling__c> roofsMap = new Map<Id,Ceiling__c>(roofs);
            List<Layer__c> roofLayers = [SELECT Id FROM Layer__c WHERE Ceiling__c IN :roofsMap.keySet() AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            if(roofLayers.size() > 0){
                UPDATE roofLayers;
            }
            UPDATE roofs;
        }
    }
    
    @future
    public static void updateAndReCalculateThermalEnvelopeTypeWalls(Id tetId){
        List<Wall__c> walls = [SELECT Id FROM Wall__c WHERE Thermal_Envelope_Type__c =:tetId];
        if(walls.size() > 0){
            Map<Id,Wall__c> wallsMap = new Map<Id,Wall__c>(walls);
            List<Layer__c> wallLayers = [SELECT Id FROM Layer__c WHERE Wall__c IN :wallsMap.keySet() AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            if(wallLayers.size() > 0){
                UPDATE wallLayers;
            }
            UPDATE walls;
        }
    }
    
    @future
    public static void updateAndReCalculateThermalEnvelopeTypeFloors(Id tetId){
        List<Floor__c> floors = [SELECT Id FROM Floor__c WHERE Thermal_Envelope_Type__c =:tetId];
        if(floors.size() > 0){
            Map<Id,Floor__c> floorsMap = new Map<Id,Floor__c>(floors);
            List<Layer__c> floorLayers= [SELECT Id FROM Layer__c WHERE Floor__c IN :floorsMap.keySet() AND RecordType.Name IN('Wood Frame','Insulation','Masonry')];
            if(floorLayers.size() > 0){
                UPDATE floorLayers;
            }
            UPDATE floors;
        }
    }
    
    @future
    public static void updateFirstBasement(Id eaId){
        List<Thermal_Envelope_Type__c> firstBasement = [SELECT Id,Name,Perimeter__c,Exterior_Area__c FROM Thermal_Envelope_Type__c WHERE Energy_Assessment__c =:eaId AND RecordType.Name='Basement' ORDER BY CreatedDate LIMIT 1];
        if(firstBasement.size() > 0){
            Thermal_Envelope_Type__c basement = firstBasement.get(0);
            basement.Perimeter__c = null;
            UPDATE basement;
        }        
    }
    @future
    public static void updateFirstBasementCeilings(Id eaId){
        List<Thermal_Envelope_Type__c> firstBasement = [SELECT Id,Name,Perimeter__c,Exterior_Area__c FROM Thermal_Envelope_Type__c WHERE Energy_Assessment__c =:eaId AND RecordType.Name='Basement' ORDER BY CreatedDate LIMIT 1];
        if(firstBasement.size() > 0){
            List<Ceiling__c> basementCeilings = [SELECT Id,Name FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:firstBasement.get(0).Id];
            if(basementCeilings.size() > 0){
                for(Ceiling__c ceiling : basementCeilings){
                    ceiling.Area__c = null;
                    //ceiling.Perimeter__c = null;
                }
                UPDATE basementCeilings;
            }
        }
    }
    @future
    public static void updateFirstBasementFloors(Id eaId){
        List<Thermal_Envelope_Type__c> firstBasement = [SELECT Id,Name,Perimeter__c,Exterior_Area__c FROM Thermal_Envelope_Type__c WHERE Energy_Assessment__c =:eaId AND RecordType.Name='Basement' ORDER BY CreatedDate LIMIT 1];
        if(firstBasement.size() > 0){
            List<Floor__c> basementFloors = [SELECT Id,Name FROM Floor__c WHERE Thermal_Envelope_Type__c =:firstBasement.get(0).Id];
            if(basementFloors.size() > 0){
                for(Floor__c floor : basementFloors){
                    floor.Area__c = null;
                    //floor.Perimeter__c = null;
                }
                UPDATE basementFloors;
            }
        }
    }
}