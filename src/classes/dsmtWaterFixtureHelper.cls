public class dsmtWaterFixtureHelper{
    
    public static void CalculateCostAndSavings(List<Water_Fixture__c > wfList){
        
        for(Water_Fixture__c  wt : wfList){
            if(wt.Mechanical_Sub_Type__c == null){
                
                wt.KW__c = null;
                wt.KWH__c= null;
                wt.KWH_BTU__c = null;
                wt.Therms__c = null;
                wt.Therms_BTU__c = null;
                wt.Operating_Cost__c = null;
            }
        }
    }
    
    public static void CreateDefaultRecord(Set<Id> eaids){
        
        List<Water_Fixture__c> wflst = new List<Water_Fixture__c>();
        for(Id  eaid : eaids)
        {

            Id sinkrecordtypeid = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Sink').getRecordTypeId();
            Id showerrecordtypeid = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Shower').getRecordTypeId();
            Id Bathrecordtypeid = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Bath').getRecordTypeId();
             
            Water_Fixture__c wf = new Water_Fixture__c();
            wf.Name = 'Sink 1';
            wf.RecordTypeId = sinkrecordtypeid;
            wf.Energy_Assessment__c = eaid;
            wf.Location__c = 'Living Space';
            wf.Type__c = 'Sink';
            wflst.add(wf);
             
            Water_Fixture__c wf1 = new Water_Fixture__c();
            wf1.Name = 'Shower 1';
            wf1.RecordTypeId = showerrecordtypeid;
            wf1.Energy_Assessment__c = eaid;
            wf1.Location__c = 'Living Space';
            wf.Type__c = 'Shower';
             wflst.add(wf1);
             
            Water_Fixture__c wf2 = new Water_Fixture__c();
            wf2.Name = 'Bath 1';
            wf2.RecordTypeId = Bathrecordtypeid;
            wf2.Energy_Assessment__c = eaid;
            wf2.Location__c = 'Living Space';
            wf.Type__c = 'Bath';
            wflst.add(wf2);
             
         }
         if(wflst != null && wflst.size()>0){
             insert wflst;
         }
    }
    
    
    //Calculate Year values for Appliances and Appliances Recommendations
    public static integer CalculateApplianceYear(Building_Specification__c bs, boolean isRecommendation)
    {
        integer result = System.today().year();
        
        if(isRecommendation)
            return result;
        
        // appliance system year
        integer avgAge = SurfaceConstants.AverageAgeTableCommon.get('WaterFixture');
        integer defaultYear = SurfaceConstants.DefaultBuildingProfileObjectYear;
        integer buildingyear = 0;
        
        if(bs.Year_Built__c != '' && bs.Year_Built__c != null)
        {
            buildingyear = integer.valueOf(bs.Year_Built__c);
        }
        
        if(avgAge !=null)
        {
            defaultYear = date.today().year() - avgAge;
        }
        
        result = Math.Max(buildingYear, defaultYear);
        
        return result;
    }
    
    
    public static integer ComputeWaterFixtureQuantityCommon(Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> wfList) 
    {
        integer retval = null;
    
        if (bs != null && wfList != null && bs.Bedromm__c != null) 
        {
            Water_Fixture__c testMe = wf;
            if(wfList==null || wfList.size()==0)
            {
                wfList = new List<Water_Fixture__c>();
                wfList.add(wf);
            }

            Water_Fixture__c firstSameTypeWf = null;
            if(wfList !=null)
            {
                For(integer i=0; i< wfList.size(); i++)
                {
                    if(wfList[i].Type__c==testMe.Type__c)
                    {
                        firstSameTypeWf = wfList[i];
                    }
                }
            }
            
            if (((wf.Type__c == 'Sink' || 
                wf.Type__c == 'Shower' || 
                wf.Type__c == 'Bath') &&
                firstSameTypeWf.Id == testMe.Id) || ((wf.Type__c == 'Sink' || 
                wf.Type__c == 'Shower' || 
                wf.Type__c == 'Bath') &&
                firstSameTypeWf.Id == null && testMe.Id==null)) 
            {
    
                integer thisWFQuantity = 0;
                if(wf.Type__c == 'Sink')
                    thisWFQuantity = Math.Max(2, integer.valueOf(bs.Bedromm__c));
                else if(wf.Type__c == 'Shower')
                    thisWFQuantity = Math.Max(1, integer.valueOf(bs.Bedromm__c) - 1);
                else if(wf.Type__c == 'Bath')
                    thisWFQuantity = Math.Max(1, integer.valueOf(bs.Bedromm__c) - 2);
            
                integer otherWFQuantity = 0;

                if(wfList !=null)
                {
                    For(integer i=0; i< wfList.size(); i++)
                    {
                        Id curId = wfList[i].Id;

                        if(wfList[i].Type__c!=testMe.Type__c) continue;
                        if(curId ==null && testMe.Id ==null) continue;
                        if(testMe.Id == curId) continue;
                        
                        otherWFQuantity += integer.ValueOf(dsmtEAModel.ISNULL(wfList[i].Quantity__c));
                    }
                }
                //(wf.BuildingProfile.WaterFixtures.Where(bpwf => bpwf.WaterFixtureType == wf.WaterFixtureType && bpwf != testMe).Sum(bpwf => bpwf.Quantity) ? ? 0);
    
                retval = Math.Max(0, thisWFQuantity - otherWFQuantity);
            } 
            else 
            {
                retval = integer.valueOf(LightingAndApplianceConstant__c.getOrgDefaults().DefaultWaterFixtureQuantity__c);
            }
        }
    
        return retval;
    }
    

    public static integer ComputeWaterFixtureLowFlowQuantityCommon(Water_Fixture__c wf) {
        integer retval = null;
    
        // compute and return
        if (wf.Type__c == 'Sink' || wf.Type__c == 'Shower') {
            retval = integer.valueOf(LightingAndApplianceConstant__c.getOrgDefaults().DefaultLowFlowQuantity__c);
        }
    
        return retval;
    }
    
    
    public static decimal ComputeWaterFixtureShowersPerDayCommon(Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> wfList) {
        decimal retval = null;
        
        //MAX [0, (IF[EXIST User.WH.Shower(r)PerDay: SUM(1…r)[User.WH.Shower(r)PerDay]; ELSE Bldg.NumOccs ^0.8 ] * WH.Shower(r)ExGPM * Dft.WH.ShowerMins) - WH.ShowerSaved ]
        if (wf.Type__c == 'Shower') 
        {
            if (bs != null && bs.Occupants__c != null) 
            {
                
                decimal showersPerDay = 0;
                if(wfList ==null || wfList.size()==0)
                {
                    wfList = new List<Water_Fixture__c>();
                    wfList.add(wf);
                }

                if(wfList[0].Id == wf.Id || (wfList[0].Id ==null && wf.Id ==null))
                {
                    decimal totalShowersPerDay = (decimal) Math.Pow((double) bs.Occupants__c, (double) 0.8);

                    //system.debug('totalShowersPerDay ' + totalShowersPerDay);

                    if(wfList.size() > 1)
                    {
                        for(Water_Fixture__c a : wfList)
                        {
                            if(a.Id != wf.Id)
                            {
                                if(a.ShowersPerDay__c != null)
                                    showersPerDay += a.ShowersPerDay__c;
                            }
                        }
                    }

                     retval = Math.Max(0, totalShowersPerDay - showersPerDay);
                }                   
                else 
                {
                    retval = 1;
                }
            }
        }
    
        return dsmtEAModel.SetScale(retval);
    }
    
    
    public static decimal ComputeWaterFixtureShowerExGPMCommon(Water_Fixture__c wf) {
        decimal retval = null;
    
        if (wf.Type__c == 'Shower') {
            retval = LightingAndApplianceConstant__c.getOrgDefaults().DefaultShowerGPM__c;
        }
    
        return retval;
    }
    
    
    public static decimal ComputeWaterFixtureShowerInternalExGPMCommon(Water_Fixture__c wf) {
        decimal retval = null;
    
        if (wf.Type__c == 'Shower' && wf.GPM__c != null) {
            retval = wf.GPM__c * .52 + .69;
        }
    
        return retval;
    }
    
    
    public static decimal ComputeWaterFixtureShowerNewGPMCommon(Water_Fixture__c wf) {
        decimal retval = null;
    
        if (wf.Type__c == 'Shower') {
            retval = LightingAndApplianceConstant__c.getOrgDefaults().DefaultShowerLowFlowGPM__c;
        }
    
        return retval;
    }
    
    
    public static decimal ComputeWaterFixtureShowerInternalNewGPMCommon(Water_Fixture__c wf) {
        decimal retval = null;
    
        if (wf.Type__c == 'Shower' && wf.Low_Flow_GPM__c != null) {
            retval = wf.Low_Flow_GPM__c * .52 + .69;
        }
    
        return retval;
    }
    
    
    //POOL
    public static decimal ComputePoolPumpHPCommon(Water_Fixture__c wf) {
        decimal retval = null;
    
        // compute and return
        if (wf.Type__c == 'Pool') {
            if (wf.Pool_Size__c != null) {
                retval = SurfaceConstants.CommonDefaultPoolPumpHP.get(wf.Pool_Size__c);
            } else {
                retval = SurfaceConstants.CommonDefaultPoolPumpHP.get('Medium');
            }
        }
    
        return retval;
    }
    
    public static boolean ComputePoolHasOffSeasonUseCommon(Water_Fixture__c wf) {
        boolean retval = null;
    
        if (wf.Type__c == 'Pool') {
            retval = false;
        }
    
        return retval;
    }
    
    public static decimal ComputePoolAreaCommon(Water_Fixture__c wf) {
        decimal retval = null;
    
        // compute and return
        if (wf.Type__c == 'Pool') {
            if (wf.Length__c != null && wf.Width__c != null) {
                retval = wf.Length__c * wf.Width__c ;
            } else if (wf.Pool_Size__c != null) {
                retval = SurfaceConstants.CommonPoolAreaFactor.get(wf.Pool_Size__c);
            } else {
                retval = SurfaceConstants.CommonPoolAreaFactor.get('Medium');
            }
        }
        return retval;
    }
    
    public static String ComputeWaterFixturePoolHeatMonthsCommon(Water_Fixture__c wf) {
        String retval = null;
    
        if (wf.Type__c == 'Pool') {
            // default is same as OperatingMonths
            retval = wf.Months_Used__c;
        }
    
        return retval;
    }
    
    public static decimal ComputeWaterFixtureHotWaterGallonsPerDayCommon(Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> whattheFuckList, List<Water_Fixture__c> bathList) 
    {
        decimal retval = null;
    
        if(wf.Type__c == 'Shower')
            retval = Math.Max(0, ISNULL(computeExShowerUse(wf)) - ISNULL(computeShowerSaved(wf)));

        else if(wf.Type__c == 'Sink')
            retval = Math.Max(0, ISNULL(computeExSinkUse(wf, bs, whattheFuckList)) - ISNULL(computeSinkSaved(wf, bs, whattheFuckList)));

        else if(wf.Type__c == 'Bath')
            retval = computeBathUse(wf, bs, bathList);
        
        return dsmtEAModel.SetScale(retval);
    }

    private static decimal computeExShowerUse(Water_Fixture__c wf)
    {
        decimal retval = null;        
        // Dft.WH.ShowerUse = MAX [0, (IF[EXIST User.WH.Shower(r)PerDay: SUM(1…r)[User.WH.Shower(r)PerDay]; ELSE Bldg.NumOccs ^0.8 ] * WH.Shower(r)ExGPM * Dft.WH.ShowerMins) - WH.ShowerSaved ]
        if ( wf.ShowersPerDay__c !=null && wf.ShowerInternalExGPM__c !=null ) 
        {
            retval = dsmtEAModel.SetScale(wf.ShowersPerDay__c) * dsmtEAModel.SetScale(wf.ShowerInternalExGPM__c) * SurfaceConstants.DefaultShowerMinutes;
        }

        //system.debug('computeExShowerUse ' + retval);

        return retval;
    }
    
    public static decimal ComputeWaterFixtureTotalWaterGallonsPerDayHCommon(Water_Fixture__c wf) {
        decimal retval = null;

        if (wf.HotWaterGallonsPerDayH__c != null || wf.ColdWaterGallonsPerDayH__c != null) 
        {
            retval = (wf.HotWaterGallonsPerDayH__c != null ? wf.HotWaterGallonsPerDayH__c : 0) + 
                     (wf.ColdWaterGallonsPerDayH__c != null ? wf.ColdWaterGallonsPerDayH__c : 0);
        }

        return retval;
    }

    private static decimal computeShowerSaved( Water_Fixture__c wf )
    {
        decimal retval = null;

        decimal showerUse = computeExShowerUse( wf );

        if ( showerUse !=null 
            && wf.ShowersPerDay__c !=null 
            && wf.ShowerInternalExGPM__c !=null 
            && wf.ShowerInternalNewGPM__c !=null 
            && wf.Quantity__c !=null && wf.Quantity__c != 0 
            && wf.LowFlowQuantity__c !=null 
            && wf.ShowerInternalExGPM__c * wf.LowFlowQuantity__c != 0 ) 
        {
            retval = showerUse * dsmtEAModel.SetScale(wf.ShowerInternalExGPM__c - wf.ShowerInternalNewGPM__c) 
            / dsmtEAModel.SetScale(wf.ShowerInternalExGPM__c) * dsmtEAModel.SetScale(wf.LowFlowQuantity__c) / dsmtEAModel.SetScale(wf.Quantity__c);

            if ( retval > showerUse ) retval = showerUse;
        }

        return retval;
    }

    private static decimal computeExSinkUse( Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> wfList )
    {
        decimal retval = null;

        if ( bs.Occupants__c!=null ) 
        {
            // (9.3 + 2.46 * Bldg.NumOccs) - WH.AeratorSaved
            retval = 9.3 + 2.46 * bs.Occupants__c;

            // allocate sinkUse across all sinks in the BP
            integer totalSinks =0;
            if(wfList !=null)
            {
                For(Water_Fixture__c w : wfList)
                {
                    if(w.Type__c!= 'Sink') continue;
                    
                    totalSinks = (integer)  ISNULL(w.Quantity__c);
                }
            }
            

            if ( wf.Quantity__c!=null && totalSinks!=null && totalSinks != 0 ) 
            {
                retval *= (decimal)wf.Quantity__c / (decimal)totalSinks;
            }
        }

        return retval;
    }
    private static decimal computeSinkSaved( Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> wfList )
    {
        decimal retval = null;
        

        decimal sinkUse = computeExSinkUse( wf, bs, wfList );

        if ( wf.LowFlowQuantity__c!=null && sinkUse!=null ) 
        {
            retval = wf.LowFlowQuantity__c * SurfaceConstants.DefaultSinkAeratorSavings;

            if ( retval > sinkUse ) retval = sinkUse;
        }

        return retval;
    }
    private static decimal computeBathUse( Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> wfList )
    {
        decimal retval = null;
       

       if ( bs.Occupants__c!=null ) 
        {
            // 3.5 + 1.17 * Bldg.NumOccs

            retval = 3.5 + 1.17 * bs.Occupants__c;

            // allocate bathUse across all baths in the BP
            integer totalBaths = 0;

            if(wfList !=null)
            {
                For(Water_Fixture__c w : wfList)
                {
                    totalBaths = (integer)  ISNULL(w.Quantity__c);
                }
            }

            if ( wf.Quantity__c!=null && totalBaths!=null && totalBaths != 0 ) {
                retval *= (decimal)wf.Quantity__c / (decimal)totalBaths;
            }
        }

        return retval;
    }


    private static decimal ISNULL(decimal value)
    {
        if(value==null)return 0;
        return value;
    }

    public static decimal ComputeWaterFixtureSavedHotWaterGallonsPerDayCommon( Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> sinkList )
    {
        decimal retval = null;

        if(wf.WaterFixtureType__c == 'Shower' ) 
        {            
                retval = computeShowerSaved( wf );
        }
        else if(wf.WaterFixtureType__c == 'Sink' ) 
        {
            retval = computeSinkSaved( wf, bs, sinkList );
        }        

        return retval;
    }

    public static decimal ComputeWaterFixtureTotalWaterGallonsPerDayCommon( Water_Fixture__c wf )
    {
        decimal retval = null;

        if ( wf.HotWaterGallonsPerDay__c !=null || wf.ColdWaterGallonsPerDay__c !=null ) 
        {
            retval = ISNULL(wf.HotWaterGallonsPerDay__c) + ISNULL(wf.ColdWaterGallonsPerDay__c);
        }

        return dsmtEAModel.SetScale(retval);
    }

    public static decimal ComputeWaterFixtureHotWaterGallonsPerDayHCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        if ( wf.HotWaterGallonsPerDay__c !=null ) 
        {
            retval = dsmtMechanicalHelper.splitIntensValue( bp, wf.Fuel_Type__c, wf.HotWaterGallonsPerDay__c, 'IntensH', true );
        }

        return retval;
    }

    
    public static decimal ComputeWaterFixtureColdWaterGallonsPerDayHCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        if ( wf.ColdWaterGallonsPerDay__c !=null ) {
            retval = dsmtMechanicalHelper.splitIntensValue( bp, wf.Fuel_Type__c, wf.ColdWaterGallonsPerDay__c, 'IntensH', true );
        }

        return retval;
    }

    public static decimal ComputeWaterFixtureTotalWaterGallonsPerDayCCommon( Water_Fixture__c wf )
    {
        decimal retval = null;

        if ( wf.HotWaterGallonsPerDayC__c !=null || wf.ColdWaterGallonsPerDayC__c !=null ) {
            retval = ISNULL(wf.HotWaterGallonsPerDayC__c) + ISNULL(wf.ColdWaterGallonsPerDayC__c);
        }

        return retval;
    }

    
    public static decimal ComputeWaterFixtureHotWaterGallonsPerDayCCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        if ( wf.HotWaterGallonsPerDay__c !=null ) 
        {
            retval = dsmtMechanicalHelper.splitIntensValue( bp, wf.Fuel_Type__c, wf.HotWaterGallonsPerDay__c, 'IntensC', true );
        }

        return retval;
    }

    
    public static decimal ComputeWaterFixtureColdWaterGallonsPerDayCCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        if ( wf.ColdWaterGallonsPerDay__c !=null ) 
        {
            retval = dsmtMechanicalHelper.splitIntensValue( bp, wf.Fuel_Type__c, wf.ColdWaterGallonsPerDay__c, 'IntensC', true );
        }

        return retval;
    }

        public static decimal ComputeWaterFixtureTotalWaterGallonsPerDayShCommon( Water_Fixture__c wf )
    {
        decimal retval = null;

        if ( wf.HotWaterGallonsPerDaySh__c !=null || wf.ColdWaterGallonsPerDaySh__c !=null ) {
            retval = ISNULL(wf.HotWaterGallonsPerDaySh__c) + ISNULL(wf.ColdWaterGallonsPerDaySh__c);
        }

        return retval;
    }

    
    public static decimal ComputeWaterFixtureHotWaterGallonsPerDayShCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        if ( wf.HotWaterGallonsPerDay__c !=null ) 
        {
            retval = dsmtMechanicalHelper.splitIntensValue( bp, wf.Fuel_Type__c, wf.HotWaterGallonsPerDay__c, 'IntensSh', true );
        }

        return dsmtEAModel.SetScale(retval);
    }

    
    public static decimal ComputeWaterFixtureColdWaterGallonsPerDayShCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        if ( wf.ColdWaterGallonsPerDay__c !=null ) 
        {
            retval = dsmtMechanicalHelper.splitIntensValue( bp, wf.Fuel_Type__c, wf.ColdWaterGallonsPerDay__c, 'IntensSh', true );
        }

        return dsmtEAModel.SetScale(retval);
    }

    public static decimal  ComputeHotWaterGallonsPerDayMixAdjCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        
        dsmtEAModel.Temprature mo = bp.temp;
        
        if ( (wf.HotWaterGallonsPerDayMixAdjC__c !=null || wf.HotWaterGallonsPerDayMixAdjH__c !=null || wf.HotWaterGallonsPerDayMixAdjSh__c !=null)
            && mo.FractionDaysH !=null && mo.FractionDaysC !=null && mo.FractionDaysSh !=null ) 
        {
            retval = ISNULL(wf.HotWaterGallonsPerDayMixAdjH__c) * mo.FractionDaysH +
                ISNULL(wf.HotWaterGallonsPerDayMixAdjC__c) * mo.FractionDaysC +
                ISNULL(wf.HotWaterGallonsPerDayMixAdjSh__c) * mo.FractionDaysSh;
        }   

        return dsmtEAModel.SetScale(retval);
    }


    public static decimal ComputeHotWaterGallonsPerDayMixAdjHCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        
        Mechanical_Sub_Type__c dhw = GetDWH(bp,wf.Mechanical_Sub_Type__c);
        decimal hwTemp=null;
        decimal tColdWaterH=null;
        
        hwTemp = dsmtEnergyConsumptionHelper.getDHWProperty( bp, dhw, 'TankTemp__c' );
        tColdWaterH = dsmtEnergyConsumptionHelper.getDHWProperty( bp, dhw, 'TColdWaterH__c' );

        //system.debug('hwTemp>>> ' + hwTemp);
        //system.debug('tColdWaterH>>> ' + tColdWaterH);

        if ( hwTemp !=null && tColdWaterH !=null &&
            wf.HotWaterGallonsPerDayH__c !=null && wf.HotWaterMixTemp__c !=null &&
            (hwTemp - tColdWaterH) != 0 ) 
        {

            //CW.HGalPerDay * (CW.HotWaterMixTemp-WS.Tcold)/(WH(g).Ttank-WS.Tcold)
            retval = wf.HotWaterGallonsPerDayH__c * (wf.HotWaterMixTemp__c - tColdWaterH) /
                (hwTemp - tColdWaterH);
        }

        return dsmtEAModel.SetScale(retval);
    }

    public static decimal ComputeHotWaterGallonsPerDayMixAdjCCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        
        Mechanical_Sub_Type__c dhw = GetDWH(bp,wf.Mechanical_Sub_Type__c);
        decimal hwTemp=null;
        decimal TColdWaterC=null;

        hwTemp = dsmtEnergyConsumptionHelper.getDHWProperty( bp, dhw, 'TankTemp__c' );
        TColdWaterC = dsmtEnergyConsumptionHelper.getDHWProperty( bp, dhw, 'TColdWaterC__c' );

        /*//system.debug(' hwtemp ' + hwtemp);
        //system.debug(' TColdWaterC ' + TColdWaterC);
        //system.debug('wf.HotWaterGallonsPerDayC__c ' + wf.HotWaterGallonsPerDayC__c);
        //system.debug('wf.HotWaterMixTemp__c ' + wf.HotWaterMixTemp__c);*/

        if ( hwTemp !=null && TColdWaterC !=null &&
            wf.HotWaterGallonsPerDayC__c !=null && wf.HotWaterMixTemp__c !=null &&
            (hwTemp - TColdWaterC) != 0 ) 
        {

            //CW.HGalPerDay * (CW.HotWaterMixTemp-WS.Tcold)/(WH(g).Ttank-WS.Tcold)
            retval = wf.HotWaterGallonsPerDayC__c * (wf.HotWaterMixTemp__c - TColdWaterC) /
                (hwTemp - TColdWaterC);
        }

        return dsmtEAModel.SetScale(retval);
    }

    public static decimal ComputeHotWaterGallonsPerDayMixAdjShCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        
        Mechanical_Sub_Type__c dhw = GetDWH(bp,wf.Mechanical_Sub_Type__c);
        decimal hwTemp=null;
        decimal TColdWaterSh=null;
        
        hwTemp = dsmtEnergyConsumptionHelper.getDHWProperty( bp, dhw, 'TankTemp__c' );
        TColdWaterSh = dsmtEnergyConsumptionHelper.getDHWProperty( bp, dhw, 'TColdWaterSh__c' );

        if ( hwTemp !=null && TColdWaterSh !=null &&
            wf.HotWaterGallonsPerDaySh__c !=null && wf.HotWaterMixTemp__c !=null &&
            (hwTemp - TColdWaterSh) != 0 ) 
        {

            //CW.HGalPerDay * (CW.HotWaterMixTemp-WS.Tcold)/(WH(g).Ttank-WS.Tcold)
            retval = wf.HotWaterGallonsPerDaySh__c * (wf.HotWaterMixTemp__c - TColdWaterSh) /
                (hwTemp - TColdWaterSh);
        }

        return dsmtEAModel.SetScale(retval);
    }

    public static decimal ComputeHotWaterMixTempCommon( Water_Fixture__c wf, dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        
        Mechanical_Sub_Type__c dhw = GetDWH(bp,wf.Mechanical_Sub_Type__c);
        
        
        retval = dsmtEnergyConsumptionHelper.getDHWProperty( bp, dhw, 'MixTemp__c' );
        

        return retval;
    }

    public static Mechanical_Sub_Type__c GetDWH(dsmtEAModel.Surface bp, Id dhwId)
    {

        Mechanical_Sub_Type__c dhw = null;

         List<Mechanical_Sub_Type__c> mst = bp.mst;
        if(mst !=null)
        {
            For ( integer i=0, j= mst.size(); i < j; i++ ) 
            {
                Mechanical_Sub_Type__c m = mst[i];
                if(!dsmtEAModel.GetDHWSystem().contains(bp.recordtypemap.get(m.RecordTypeId))) continue;
                
                if(dhwId !=null)
                {
                    if(dhwId == m.Id)
                    {
                        return m;
                    }
                }
                else 
                {
                    //return m;
                }                
            }            
        }
        
        return dhw;
    }

    public static Mechanical_Sub_Type__c GetDWH(dsmtEAModel.Surface bp, Id dhwId, boolean getAny)
    {

        Mechanical_Sub_Type__c dhw = null;

         List<Mechanical_Sub_Type__c> mst = bp.mst;
        if(mst !=null)
        {
            integer idx=0;
            For ( integer i=0, j= mst.size(); i < j; i++ ) 
            {
                Mechanical_Sub_Type__c m = mst[i];
                if(!dsmtEAModel.GetDHWSystem().contains(bp.recordtypemap.get(m.RecordTypeId))) continue;
                
                if(dhwId !=null)
                {
                    if(dhwId == m.Id)
                    {
                        return m;
                    }
                }
                else 
                {
                    if(getAny && ((m.Primary_DHW__c) || (!m.Primary_DHW__c && idx==0)))
                        return m;
                }

                idx++;
            }            
        }
        
        return dhw;
    }

    public static decimal ComputeWaterFixtureLoadCommon(Water_Fixture__c waterFixture) {
        decimal retval = null;
    
        if (waterFixture.Type__c == 'Pool' &&
            waterFixture.PoolPumpWatts__c != null &&
            waterFixture.NumberOperatingMonths__c != null &&
            waterFixture.TimerHours__c != null &&
            waterFixture.WinterHours__c != null) {
    
            // Pool.PumpWatts/ 1000 * {# days in month(m)} * Pool.PumpTimerHrs; ELSE 0]] 
            retval = 0;
            for(integer i=0; i < waterFixture.NumberOperatingMonths__c; i++) {
                decimal hours = 0;
    
                hours = waterFixture.TimerHours__c;
                //} else if (waterFixture.HasOffSeasonUse__c == true) {
                //    hours = waterFixture.WinterHours.Value;
                //}
    
                retval += waterFixture.PoolPumpWatts__c / 1000;//* Constants.DaysPerMonth[month] * hours;
    
            }
    
            // convert from kWh/yr to MBtu/yr
            retval *= SurfaceConstants.CommonBtusByFuelType.get('Electricity').get('kWh') / 1000000;
        } else if (waterFixture.Type__c == 'Spa' &&
            waterFixture.SpaUseWatts__c != null &&
            waterFixture.SpaUseHours__c != null &&
            waterFixture.SpaIdleWatts__c != null &&
            waterFixture.SpaIdleHours__c != null) {
    
            //IF[User.Spa.Present=yes: (Spa.UseWatts*Spa.UseHours+Spa.IdleWatts*Spa.IdleHours) / 1000 * User.Spa.Months / 12; ELSE 0]
            // CSGENG-4159: months/12 moved to EnergyConsumptionH/C/Sh computation
            retval = (waterFixture.SpaUseWatts__c * waterFixture.SpaUseHours__c + waterFixture.SpaIdleWatts__c * waterFixture.SpaIdleHours__c) / 1000;
    
            // convert from kWh/yr to MBtu/yr
            retval *= SurfaceConstants.CommonBtusByFuelType.get('Electricity').get('kWh') / 1000000;
        }
    
        return retval;
    }    

    public static decimal ComputeWaterFixtureColdWaterGallonsPerDayCommon( Water_Fixture__c wf )
    {
        decimal retval = null;

        if ( wf.Type__c == 'Pool' ) 
        {
            //retval = wf.PoolEvaporationMonths.Sum( pem => (pem.GPD ?? 0) ) / 12M;
        }

        return retval;
    }
}