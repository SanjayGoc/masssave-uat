@isTest
Public class dsmtExceptionsControllerTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Trade_Ally_Type__c='HPC';
        update Tacc;
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
        
        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.W_9_Attached__c=false;
        reg.status__c ='Submitted';
        update reg;
        Exception_Template__c emailTemp= new Exception_Template__c(Object_Sub_Type__c='HPC', Reference_ID__c='ETN-0000003', Automatic__c = true, Active__c = true,
                                            Registration_Request_Level_Message__c= true);
        insert emailTemp;
        Exception__c ex= new Exception__c(Exception_Message__c='test',Registration_Request__c=reg.ID,Exception_Template__c=emailTemp.id, Disposition__c='Resolved', Automated__c=true);
        insert ex;
        Exception__c ex1= new Exception__c(Exception_Message__c='test',Registration_Request__c=reg.ID,Exception_Template__c=emailTemp.id, Disposition__c='Exclude', Automated__c=true);
        insert ex1;
        Email_Custom_Setting__c emailset= Datagenerator.createEmailCustSetting();
        emailset.RR_Submitted_Owner_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Submitted_TA_ThankYou_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_TA_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_DSMTContact_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        update emailset;
        ApexPages.currentPage().getParameters().put('id',reg.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(reg);
        dsmtExceptionsController cont= new dsmtExceptionsController(controller);
        cont.expId = ex1.Id;
        cont.expTempId = emailTemp.ID+',';
       // cont.savedString = ex.id+' column test2 column test3 column test4 column test5 ';
        cont.savedString = ex.id+' column test2 ';
        
        cont.getAllExistingExceptions();
        cont.getTypeOptions();
        cont.getStatusOptions();
        cont.save();
        cont.overrideAllExceptions();
        cont.getDespostionOptions();
        cont.getUserOptions();
        cont.getAllNonExistingExceptions();
        cont.saveExceptionPopup();
        cont.getfetchDepositionOptions();
        cont.expId = ex1.Id;
        cont.saveExp();
        }
        
}