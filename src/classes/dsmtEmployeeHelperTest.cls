@isTest
Public class dsmtEmployeeHelperTest{
    @isTest
    public static void runTest(){
        
        Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        wt.Service_Date__c = Date.today().AddDays(2);
        
        system.debug(wt);
        insert wt;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Work_Team__c = wt.Id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();
        insert wo;
        
        
        Territory__c ter = Datagenerator.createTerritory(regObj.Id,emp.Id,wt.Id);
        ter.Location__c = locObj.Id;
        
        insert ter;
        
        Territory_Detail__c td = Datagenerator.createTerritoryDetail(ter.Id);
        insert td;
        
        dsmtEmployeeHelper.InactivateEmployee(new Set<Id>{emp.Id});
        dsmtEmployeeHelper.activateEmployee(new Set<Id>{emp.Id});
        dsmtEmployeeHelper.SkipScheduingEmployee(new Set<Id>{emp.Id});
        
    }
}