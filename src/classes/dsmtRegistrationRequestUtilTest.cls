@isTest
public class dsmtRegistrationRequestUtilTest {
    
	testmethod static void testRun()
    {
       
        User u = Datagenerator.CreatePortalUser();
        
        System.runAs(u) {
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c(name='test',Trade_Ally_Type__c = 'HPC',Internal_Account__c = true, Stage__c = 'Active');
            insert ta;
            
            Registration_Request__c rr = new Registration_Request__c(First_Name__c='hemanshu',Last_Name__c='patel',Email__c='test@test.com',Status__c='test');
            rr.Trade_Ally_Account__c = ta.Id;
            insert rr;
            
            
            DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test1',phone__c='1234115',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true, Status__c='Approved',Title__c='Crew Chief');
            dsmt.Trade_Ally_Account__c = ta.Id;
            dsmt.Registration_Request__c = rr.Id;
            dsmt.BPI_Envelope_Attached__c = true;
            dsmt.CSL_Attached__c=true;
            insert dsmt;
            
            Attachment__c att1 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='Approved',Registration_Request__c=rr.Id);
            insert att1;            
            
            Attachment__c att = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='New');
            att.DSMTracker_Contact__c = dsmt.Id;
            insert att;
            
            
            dsmtRegistrationRequestUtil.approveRequest(rr.Id);
            
            dsmtRegistrationRequestUtil.approveDSMTContact(new String[]{dsmt.Id});
            
            
            dsmtRegistrationRequestUtil.RejectDSMTContact(new String[]{dsmt.Id});
            
            dsmtRegistrationRequestUtil.approveAttachments(new String[]{att.id});
            
            dsmtRegistrationRequestUtil.rejectAttachments(new String[]{att.id});
            
            Location__c empLoc = Datagenerator.createLocation();
            insert empLoc;
            Employee__c emp = Datagenerator.createEmployee(empLoc.Id);
            emp.Email__c='test2@test.com';
            insert emp;
            
            Appointment__c app = new Appointment__c();
            app.Employee__c = emp.Id;
            insert app;
            
            dsmtRegistrationRequestUtil.createPortalAccess(emp.Id);
            
            dsmtRegistrationRequestUtil.SendPortalRegistrationEmail(ta.Id);
            dsmtRegistrationRequestUtil.SendPortalRegistrationEmail(emp.Id);
            dsmtRegistrationRequestUtil.createTask('Subject','Desc',emp.Id, u.Id);
            dsmtRegistrationRequestUtil.createMessage('String subject', 'test@plient.com','test1@plient.com','String body',rr, 'Registration_Request__c','String direction');
            
            Service_Request__c sr = new Service_Request__c();
            insert sr;
            dsmtRegistrationRequestUtil.createMessage('String subject', 'test@plient.com','test1@plient.com','String body',sr, 'Service_Request__c','String direction');
        }
    }
    
    testmethod static void testRun1()
    {
        Registration_Request__c rr = new Registration_Request__c(First_Name__c='hemanshu',Last_Name__c='patel',Email__c='test@test.com',Status__c='Approved');
        insert rr;
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c(name='test',Trade_Ally_Type__c = 'HPC',Internal_Account__c = true, Stage__c = 'Active');
        insert ta;
        
        Attachment__c att1 = new Attachment__c(Trade_Ally_Account__c=ta.Id,Status__c='test',Registration_Request__c=rr.Id);
        insert att1;
        
        dsmtRegistrationRequestUtil.approveRequest(rr.Id);
        
        DSMTracker_Contact__c dsmt = Datagenerator.createDSMTracker();
        dsmtRegistrationRequestUtil.approveDSMTContact(new String[]{dsmt.Id});
        
        
        dsmtRegistrationRequestUtil.RejectDSMTContact(new String[]{dsmt.Id});
        
        Attachment__c att = Datagenerator.createAttachment(ta.Id);
        dsmtRegistrationRequestUtil.approveAttachments(new String[]{att.id});
        
        dsmtRegistrationRequestUtil.rejectAttachments(new String[]{att.id});
    }
    
    testmethod static void testRun2()
    {
        User u = Datagenerator.CreatePortalUser();
        
        System.runAs(u) {
            
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c(name='test',Trade_Ally_Type__c = 'HPC',Internal_Account__c = true, Stage__c = 'Active');
            insert ta;
            
            Location__c empLoc = Datagenerator.createLocation();
            insert empLoc;
            Employee__c emp = Datagenerator.createEmployee(empLoc.Id);
            insert emp;
            emp.Email__c='Email@plient.com';
            update emp;
            
            Appointment__c app = new Appointment__c();
            app.Employee__c = emp.Id;
            
            dsmtRegistrationRequestUtil.createPortalAccess(emp.Id);
        }
    }
}