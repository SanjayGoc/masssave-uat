public class CombustionDevicesComponentController {
    public boolean allowInsert{get;set;}
    public String parentType{get;set;}
    public String parentId{get;set;}
    public String eaId{get;set;}
    
    public Combustion_Device__c newDevice{get{
        if(newDevice == null){
            newDevice = new Combustion_Device__c();
        }
        return newDevice;
    }set;}
    public List<Combustion_Device__c> getDevices(){
        List<Combustion_Device__c> devices = new List<Combustion_Device__c>();
        if(parentId != null && parentType != null){
            devices = Database.Query(
                'SELECT '+ getSObjectFields('Combustion_Device__c') +
                    ',Appliance__r.Name,Appliance__r.Fuel_Type__c,Appliance__r.Capacity__c' +
                    ',Mechanical_Type__r.Name,Mechanical_Type__r.Fuel_Type__c,Mechanical_Type__r.Capacity_kBtu_h__c,Mechanical_Type__r.Combustion_Type__c'+
                ' FROM Combustion_Device__c WHERE {PARENT_TYPE} =:parentId'.replace('{PARENT_TYPE}',parentType));    
        }
        return devices;
    }
    public List<CombustionDeviceModel> getDeviceModels(){
        List<CombustionDeviceModel> models = new List<CombustionDeviceModel>();
        for(Combustion_Device__c device : getDevices()){
            if(device.Appliance__c == null && device.Mechanical_Type__c != null){
                Mechanical_Type__c m = device.Mechanical_Type__r;
                models.add(new CombustionDeviceModel(device.Id, m.Name, m.Fuel_Type__c, m.Capacity_kBtu_h__c, m.Combustion_Type__c, null,CombustionDeviceType.COMBUSTION ));
            }else if(device.Appliance__c != null && device.Mechanical_Type__c == null){
                Appliance__c a = device.Appliance__r;
                models.add(new CombustionDeviceModel(device.Id, a.Name, a.Fuel_Type__c, a.Capacity__c, null, null,CombustionDeviceType.APPLIANCE ));
            }
        }
        return models;
    }
    public List<CombustionDeviceModel> getNewDeviceModels(){
        List<CombustionDeviceModel> models = new List<CombustionDeviceModel>();
        for(Appliance__c apl : getAppliances()){
            models.add(new CombustionDeviceModel(apl.Id,apl.Name,CombustionDeviceType.APPLIANCE));
        }
        for(Mechanical_Type__c mech: getMechanicalTypes()){
            models.add(new CombustionDeviceModel(mech.Id,mech.Name,CombustionDeviceType.COMBUSTION));
        }
        return models;
    }
    public List<Appliance__c> getAppliances(){
        if(eaId != null){
            Set<Id> alreadyAdded = new Set<Id>();
            for(Combustion_Device__c device : getDevices()){
                if(device.Appliance__c != null){
                    alreadyAdded.add(device.Appliance__c);
                }
            }
            Set<String> applianceTypes = new Set<String>{'Range','Cooktop','Oven'};            
            return Database.query('SELECT ' + getSObjectFields('Appliance__c') + ' FROM Appliance__c WHERE Energy_Assessment__c=:eaId AND Id NOT IN:alreadyAdded AND Type__c IN:applianceTypes');
        }else{
            return new List<Appliance__c>();
        }
    }
    public List<Mechanical_Type__c> getMechanicalTypes(){
        if(eaId != null){
            Set<Id> alreadyAdded = new Set<Id>();
            for(Combustion_Device__c device : getDevices()){
                if(device.Mechanical_Type__c != null){
                    alreadyAdded.add(device.Mechanical_Type__c);
                }
            }           
            return Database.query('SELECT ' + getSObjectFields('Mechanical_Type__c') + ' FROM Mechanical_Type__c WHERE Mechanical__r.Energy_Assessment__c=:eaId AND Id NOT IN:alreadyAdded AND Combustion_Type__c != null');
        }else{
            return new List<Mechanical_Type__c>();
        }
    }
    public String getAppliancesJSON(){
        return JSON.serialize(getAppliances());
    }
    public void addNewCombustionDevices(){
        String applianceIds = ApexPages.currentPage().getParameters().get('applianceIds');
        String mechanicalTypeIds = ApexPages.currentPage().getParameters().get('mechanicalTypeIds');
        List<Combustion_Device__c> combustionDevices = new List<Combustion_Device__c>();
        if(applianceIds != null && applianceIds.trim() != ''){
            List<Id> applianceIdsList = applianceIds.split(',');
            List<Appliance__c> appliances = [SELECT Id,Name FROM Appliance__c WHERE Id IN:applianceIdsList];
            for(Appliance__c apl : appliances){
                combustionDevices.add(new Combustion_Device__c(
                    Safety_Aspect_Item__c = parentId,
                    Appliance__c = apl.Id,
                    Energy_Assessment__c = eaId,
                    Name = apl.Name
                ));
            }
        }
        if(mechanicalTypeIds != null && mechanicalTypeIds.trim() != ''){
            List<Id> mechanicalTypeIdsList = mechanicalTypeIds.split(',');
            List<Mechanical_Type__c> mechanicalTypes = [SELECT Id,Name FROM Mechanical_Type__c WHERE Id IN:mechanicalTypeIdsList];
            for(Mechanical_Type__c mech : mechanicalTypes){
                combustionDevices.add(new Combustion_Device__c(
                    Safety_Aspect_Item__c = parentId,
                    Mechanical_Type__c = mech.Id,
                    Energy_Assessment__c = eaId,
                    Name = mech.Name
                ));
            }
        }
        if(combustionDevices.size() > 0){
            INSERT combustionDevices;
        }
    }
    public void deleteDevice(){
        String deviceId = ApexPages.currentPage().getParameters().get('deviceId');
        if(deviceId != null){
            DELETE new Combustion_Device__c(Id = deviceId);
        }
    }
    
    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
    
    public Enum CombustionDeviceType{
        APPLIANCE,COMBUSTION
    }
    public class CombustionDeviceModel{
        public Id id{get;set;}
        public String name{get;set;}
        public String fuelType{get;set;}
        public decimal capacity{get;set;}
        public String vent{get;set;}
        public String commonVentingGroup{get;set;}
        public CombustionDeviceType deviceType{get;set;}
        public CombustionDeviceModel(Id id ,String name,String fuelType,decimal capacity,String vent,String commonVentingGroup,CombustionDeviceType deviceType){
            this.id = id;
            this.name = name;
            this.fuelType = fuelType;
            this.capacity = capacity;
            this.vent = vent;            
            this.commonVentingGroup = commonVentingGroup;
            this.deviceType = deviceType;
        }
        public CombustionDeviceModel(Id id,String name,CombustionDeviceType deviceType){
            this.id = id;
            this.name = name;
            this.deviceType = deviceType;
        }
    }
}