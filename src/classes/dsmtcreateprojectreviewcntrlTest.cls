@isTest
public with sharing class dsmtcreateprojectreviewcntrlTest{

    public static testmethod void test(){

        Recommendation_Scenario__c rs = new Recommendation_Scenario__c();
        insert rs;


        Review__c rev1 = new Review__c();
        rev1.type__c='Pre Work Review';
        rev1.Status__c='Failed Admin Review';
        rev1.Fail_Reason__c='Missing Signature~~~Missing Paperwork';
        rev1.Failure_Notes__c = 'test';
        rev1.Notes__c='test';
        insert rev1;
        
        Review__c rev = new Review__c();
        rev.type__c='Pre Work Review';
        rev.Status__c='Failed Admin Review';
        rev.Fail_Reason__c='Missing Signature~~~Missing Paperwork';
        rev.Failure_Notes__c = 'test';
        rev.Notes__c='test';
        rev.Pre_Work_Review__c = rev1.Id;
        insert rev;
        
        Project_Review__c pr = new Project_Review__c();
        pr.Review__c = rev1.Id;
        pr.Project__c = rs.Id;
        insert pr;
        
        ApexPages.currentPage().getParameters().put('RevId',rev.Id);
        ApexPages.currentPage().getParameters().put('selectedIds',rs.Id);
        dsmtcreateprojectreviewcntrl cntrl = new dsmtcreateprojectreviewcntrl();
        cntrl.doAddAction();
        
        }
        
}