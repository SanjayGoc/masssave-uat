public class dsmtThermalEnvelopeComponentController {
    public Id layerId{get;set;}
    public string thermalType {get;set;}
    public Layer__c layer{get;set;}
    
    public List<Schema.FieldSetMember> thermalTypeFields {get;set;}
    
    public void fetchThermalTypeFields(){
        if(thermalType != null){
            thermalTypeFields = getFieldSet(thermalType.replaceAll(' ','_')).getFields();
            system.debug('thermalTypeFields :::::' + thermalTypeFields);
        }else{
            thermalTypeFields = new List<Schema.FieldSetMember>();
        }
        system.debug('thermalTypeFields :::::' + thermalTypeFields);
        system.debug('thermalType :::::' + thermalType);
    }
    
    public dsmtThermalEnvelopeComponentController(){
        system.debug('thermalType :::::' + thermalType);
        thermalTypeFields = new List<Schema.FieldSetMember>();
        fetchThermalTypeFields();
        layer = new Layer__c();
        if(layerId != null){
            String query = 'SELECT '+ getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Id=:layerId';
            List<Layer__c> layers = Database.query(query);
            if(layers.size() > 0){
                layer = layers.get(0);
            }
        }
    }
    
    private static Schema.DescribeSObjectResult dsr = Thermal_Envelope_Type__c.sObjectType.getDescribe();
    private static Schema.FieldSet getFieldSet(String fieldSetName){
        Schema.FieldSet fset = null;
        Map<String,Schema.FieldSet> fsMap = dsr.fieldSets.getMap();
        for(String key : fsMap.keySet()){
            if(key.equalsIgnoreCase(fieldSetName)){
                fset = fsMap.get(fieldSetName);
            }
        }
        return fset;
    }
    
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        List<String> fields = new List<String>();
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            fields.add(dfield.getName());
        }
        return String.valueOf(fields).replace('(','').replace(')','');
    }
}