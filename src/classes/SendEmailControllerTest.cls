@isTest
public class SendEmailControllerTest
{
    @isTest
    Public static void test()
    {
        
        Account acc = DAtagenerator.createAccount();
        insert acc;
        
        Contact con = new contact();
        con.AccountId = acc.Id;
        con.LastName = 'Do Not Delete1';
        insert con;
        
        Trade_Ally_Account__c TAcc = new Trade_Ally_Account__c(name='test');
        TAcc.Internal_Account__c=true;
        insert TAcc;
        
        DSMTracker_Contact__c regCon = DAtagenerator.createDSMTracker();
        regCon.Trade_Ally_Account__c = TAcc.Id;
        regcon.First_Name__c='testuser';
        regcon.Last_Name__c='test';
        regcon.Status__c='new';
        update regCon;
        
        Service_Request__c ser = new Service_Request__c();
        ser.DSMTracker_Contact__c = regcon.Id;
        insert ser;
        
        Registration_Request__c reg = DAtagenerator.createRegistration();
        reg.DSMTracker_Contact__c = regCon.Id;
        update reg;
        
        Attachment__c att = new Attachment__c(Registration_Request__c =reg.id);
        att.Description__c='test';
        att.Attachment_Type__c='Contract Document';
        insert att;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        Recommendation__c recom =new Recommendation__c();
        recom.Recommendation_Scenario__c=proj.id;
        recom.Status__c='Pending Customer Acceptance';
        insert recom;

        
        APXTConga4__Conga_Template__c acct= new APXTConga4__Conga_Template__c();
        acct.Unique_Template_Name__c='Project Acceptance Template';
        insert acct;
        APXTConga4__Conga_Merge_Query__c acmq =new APXTConga4__Conga_Merge_Query__c();
        acmq.Unique_Conga_Query_Name__c='Project Acceptance Template Query';
        insert acmq;
        
        Apexpages.currentPage().getParameters().put('attachIds',att.Id);
        Apexpages.currentPage().getParameters().put('contractGenerated','New');
        Apexpages.currentPage().getParameters().put('dcid',regCon.Id);
        Apexpages.currentPage().getParameters().put('srid',ser.id); 
        ApexPages.Currentpage().getParameters().put('rrid',reg.Id);
        Apexpages.currentPage().getParameters().put('projid',proj.id);
        Apexpages.currentPage().getParameters().put('ObjectType','Recommendation_Scenario__c');
        SendEmailController cntrl = new SendEmailController();
        cntrl.recmIds=',';
        cntrl.getAllEmailTemplateFolders();
        cntrl.getEmailTemplatesByfolder();
        cntrl.loadContentForRegistrationRequest();
        cntrl.init();
        cntrl.loadContentsForServiceRequest();
        cntrl.loadContentsForDsmTrackerContact();
        cntrl.cancel();
        cntrl.ReloadPage();
        
        cntrl.selTemplateId = '00X4D000000QGdK';
        cntrl.emailSubject = 'test';
        cntrl.emailBody = 'test';
        cntrl.emailTo = 'test@test.com';
        cntrl.emailCC = 'tsset@test.com';
        cntrl.emailBCC= 'tsset3@test.com';
        
        cntrl.sendEmail();
        
        cntrl.redirectToCompleted();
        SendEmailController.AttachmentModel wrpAttMod = new SendEmailController.AttachmentModel();
        
    }
    
    @isTest
    public static void test1()
    {
        
        Account acc = DAtagenerator.createAccount();
        insert acc;
        
        Contact con = new contact();
        con.AccountId = acc.Id;
        con.LastName = 'Do Not Delete';
        insert con;
        
        DSMTracker_Contact__c reg = DAtagenerator.createDSMTracker();
        
        Attachment__c att = new Attachment__c(DSMTracker_Contact__c =reg.id);
        att.Attachment_Type__c='Issue Contract';
        insert att;
        
        Proposal__c proposal =new Proposal__c();
        proposal.Name='test';
        proposal.Type__c='Proposed';
        proposal.Name_Type__c = 'Weatherization';
        insert proposal;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        Barrier__c ba =new Barrier__c();
        insert ba;
        
        APXTConga4__Conga_Template__c acct= new APXTConga4__Conga_Template__c();
        acct.Unique_Template_Name__c='Project Acceptance Template';
        insert acct;
        APXTConga4__Conga_Merge_Query__c acmq =new APXTConga4__Conga_Merge_Query__c();
        acmq.Unique_Conga_Query_Name__c='Project Acceptance Template Query';
        insert acmq;
        
        ApexPages.currentPage().getParameters().put('proposalId',proposal.Id);
        ApexPages.Currentpage().getParameters().put('dcid',reg.Id);
        Apexpages.currentPage().getParameters().put('projid',proj.id);
        Apexpages.currentPage().getParameters().put('ObjectType','Barrier__c');
        
        SendEmailController cntrl = new SendEmailController();
        
        cntrl.getAllEmailTemplateFolders();
        cntrl.getEmailTemplatesByfolder();
        cntrl.loadContentForRegistrationRequest();
        cntrl.init();
        cntrl.loadContentsForServiceRequest();
        cntrl.loadContentsForDsmTrackerContact();
        cntrl.cancel();
        cntrl.ReloadPage();
        cntrl.loadContentForproject();
        cntrl.loadContentForProposal(proposal.Id);
        cntrl.loadContentForBarrier();
        cntrl.loadContentForEnergyAssessment();
        
        cntrl.selTemplateId = '00X4D000000QGdK';
        cntrl.emailSubject = 'test';
        cntrl.emailBody = 'test';
        cntrl.emailTo = 'test@test.com';
        cntrl.emailCC = 'tsset@test.com';
        cntrl.emailBCC= 'tsset3@test.com';
        cntrl.sendEmail();
    }
    
    @isTest
    public static void test2()
    {   
        Account acc = DAtagenerator.createAccount();
        insert acc;
        
        Contact con = new contact();
        con.AccountId = acc.Id;
        con.LastName = 'Do Not Delete';
        insert con;
        
        DSMTracker_Contact__c reg = DAtagenerator.createDSMTracker();
        
        Service_Request__c ser = new Service_Request__c();
        ser.DSMTracker_Contact__c = reg.Id;
        insert ser;
        
        Customer__c cust =new Customer__c();
        cust.Email__c='test@gmail.com';
        insert cust;
        
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Customer__c=cust.id;
        insert ir;
        
        
        Phase_I_Ticket_Detail__c ptd = new Phase_I_Ticket_Detail__c();
        ptd.Service_Request__c=ser.id;
        insert ptd;
        Attachment__c att = new Attachment__c();
        att.attachment_Type__c='Contract Document';
        att.status__c = 'New';
        insert att;
        
        Attachment attch= new Attachment();
        attch.Name= 'test';
        attch.ParentId= att.id;
        attch.Body= blob.ValueOf('test');
        insert attch;
        
        ApexPages.Currentpage().getParameters().put('srid',reg.Id);
        Apexpages.currentPage().getParameters().put('ObjectType','Inspection_Request__c');
        Apexpages.currentPage().getParameters().put('p1dId',ptd.Id);
    Apexpages.currentPage().getParameters().put('IRId',ir.Id);
        
        SendEmailController cntrl = new SendEmailController();
        cntrl.newCustomAttachmentId=att.id;
        cntrl.attachId=att.id;
        cntrl.loadContentForInspectionRequest();
        cntrl.loadContentForp1d();
        cntrl.checkCongaStatus();
        
        cntrl.selTemplateId = '00X4D000000QGdK';
        cntrl.emailSubject = 'test';
        cntrl.emailBody = 'test';
        cntrl.emailTo = 'test@test.com';
        cntrl.emailCC = 'tsset@test.com';
        cntrl.emailBCC= 'tsset3@test.com';
        cntrl.SRID = ser.Id;
        
        cntrl.sendEmail();
        
        Apexpages.currentPage().getParameters().put('projid',ptd.Id);
        
    }
    @istest
    public static void afterinsertupdatetesting()
    {
        Account acc = DAtagenerator.createAccount();
        insert acc;
        
        Contact con = new contact();
        con.AccountId = acc.Id;
        con.LastName = 'Do Not Delete';
        insert con;
        
        Energy_Assessment__c ea =Datagenerator.setupAssessment();

        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        test.startTest();
        
        DSMTracker_Contact__c dsmt = DAtagenerator.createDSMTracker();
        Service_Request__c sr = new Service_Request__c(Status__c = 'Approved');
        sr.DSMTracker_Contact__c=dsmt.id;
        insert sr;
        sr.Appointment__c=app.id;
        sr.Status__c = 'Rejected';
        update sr;
        
        /*sr.Status__c = 'Submitted';
        Id EST_RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
        sr.RecordtypeId = EST_RecordTypeId;
        sr.Sub_Type__c='Request PTO';
        update sr;
        
           
        sr.Status__c = 'Rejected';
        update sr;
        
        sr.Status__c = 'Submitted';
        sr.RecordtypeId = EST_RecordTypeId;
        sr.Sub_Type__c='Customer Cancellation/No Show';
        update sr;
        sr.Status__c = 'Rejected';
        update sr;
        
        sr.Status__c = 'Submitted';
        sr.RecordtypeId = EST_RecordTypeId;
        sr.Sub_Type__c='Request Return Visit/Combustion Safety Test';
        update sr;*/
        
        Barrier__c ba =new Barrier__c();
        ba.Energy_Assessment__c=ea.id;
        insert ba;
        
        Review__c rv = new Review__c();
        rv.Address__c = 'test';
        rv.Status__c='New';
        insert rv;
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Name='test';
        ir.Energy_Assessment__c=ea.id;
        ir.Contractor_Name__c=ta.id;
        //ir.Contractor_Name__c='test';
        insert ir;
        
        Proposal__c proposal =new Proposal__c();
        proposal.Name='test';
        proposal.Type__c='Proposed';
        proposal.Name_Type__c = 'Weatherization';
        insert proposal;
                
                Attachment__c att = new Attachment__c();
        att.attachment_Type__c='Customer Receipt SHV';
        att.status__c = 'New';
        att.Energy_Assessment__c=ea.id;
        att.Proposal__c=proposal.id;
        insert att;
        
        Attachment attch= new Attachment();
        attch.Name= 'test';
        attch.ParentId= att.id;
        attch.Body= blob.ValueOf('test');
        insert attch;


                Attachment__c att1 = new Attachment__c();
        att1.attachment_Type__c='Renter Receipt/Report';
        att1.status__c = 'New1';
        att1.Energy_Assessment__c=ea.id;
        att1.Proposal__c=proposal.id;
        insert att1;
        
        Attachment attch1= new Attachment();
        attch1.Name= 'test1';
        attch1.ParentId= att1.id;
        attch1.Body= blob.ValueOf('test1');
        insert attch1;

          Attachment__c att2 = new Attachment__c();
        att2.attachment_Type__c='Audit Report';
        att2.status__c = 'New2';
        att2.Energy_Assessment__c=ea.id;
        att2.Proposal__c=proposal.id;
        insert att2;
        
        Attachment attch2= new Attachment();
        attch2.Name= 'test2';
        attch2.ParentId= att2.id;
        attch2.Body= blob.ValueOf('test2');
        insert attch2;
        
        
        Apexpages.currentPage().getParameters().put('Id',ba.id);
        Apexpages.currentPage().getParameters().put('mailType','Issue Contract');
        Apexpages.currentPage().getParameters().put('ObjectType','Energy_Assessment__c');
        Apexpages.currentPage().getParameters().put('attachIds',attch.id);
        SendEmailController cntrl = new SendEmailController();
        cntrl.fillAttachmentModel();
        cntrl.loadContentForBarrier();
        cntrl.cancel();
        
        
        Apexpages.currentPage().getParameters().put('ObjectType','Proposal__c');
        Apexpages.currentPage().getParameters().put('mailType','Certificate of Inspection');
        SendEmailController cntrl3 = new SendEmailController();
        cntrl3.fillAttachmentModel();
        cntrl3.loadContentForReview();
                
        test.stopTest();
    }
    @istest
    Public Static Void Runtest5(){
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Barrier__c ba =new Barrier__c();
        ba.Energy_Assessment__c=ea.id;
        insert ba;
        test.startTest();
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        //proj.Trade_Ally_Account_Name__c = 'test cust name';
        proj.Energy_Assessment__c=ea.id;
        insert proj;
                
        APXTConga4__Conga_Template__c congatemp = new APXTConga4__Conga_Template__c();
        congatemp.Unique_Template_Name__c ='Barrier Incentive Form';
        insert congatemp;
        
        APXTConga4__Conga_Merge_Query__c congaqu = new APXTConga4__Conga_Merge_Query__c();
        congaqu.Unique_Conga_Query_Name__c = 'Barrier Detail For Incentive Form Query';
        insert congaqu;
        
        Attachment__c att = new Attachment__c();
        att.Energy_Assessment__c = ea.Id;
        att.Project__c = proj.Id;
        att.Attachment_Type__c = 'Barrier Incentive Form';
        att.Barrier__c = ba.id;
        insert att;
        
        Registration_Request__c rr = new Registration_Request__c();
        rr.Accurate_Summ_Backend_Check_Attached__c = true;
        insert rr;
        
        test.stopTest();
        Apexpages.currentPage().getParameters().put('Id',ba.id);
        Apexpages.currentPage().getParameters().put('ObjectType','Barrier__c');
        Apexpages.currentPage().getParameters().put('mailType','Barrier Incentive Form');
        
        SendEmailController cntrl3 = new SendEmailController();  
        cntrl3.fillAttachmentModel();
        cntrl3.sendEmail();
        cntrl3.checkCongaStatus();
        cntrl3.redirectToCompleted();
        cntrl3.loadContentForEnergyAssessment();
        cntrl3.loadContentForInspectionRequest();
        cntrl3.loadContentForRegistrationRequest();
        //cntrl3.loadContentForBarrier();
        cntrl3.emailSubject = null;
        cntrl3.emailBody = null;
    }
    @istest
    Public Static Void Runtest6(){
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Barrier__c ba =new Barrier__c();
        ba.Energy_Assessment__c=ea.id;
        insert ba;
        test.startTest();
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        //proj.Trade_Ally_Account_Name__c = 'test cust name';
        insert proj;
        
        APXTConga4__Conga_Template__c congatemp = new APXTConga4__Conga_Template__c();
        congatemp.Unique_Template_Name__c ='Barrier Incentive Form';
        insert congatemp;
        
        APXTConga4__Conga_Merge_Query__c congaqu = new APXTConga4__Conga_Merge_Query__c();
        congaqu.Unique_Conga_Query_Name__c = 'Barrier Detail For Incentive Form Query';
        insert congaqu;
        
        Attachment__c att = new Attachment__c();
        att.Energy_Assessment__c = ea.Id;
        att.Project__c = proj.Id;
        att.Attachment_Type__c = 'Barrier Incentive Form';
        att.Barrier__c = ba.id;
        insert att;
        att.Attachment_Type__c = 'Audit Report';
        update att;
        
        Registration_Request__c rr = new Registration_Request__c();
        rr.Accurate_Summ_Backend_Check_Attached__c = true;
        insert rr;
        
        test.stopTest();
        Apexpages.currentPage().getParameters().put('Id',ba.id);
        Apexpages.currentPage().getParameters().put('ObjectType','Barrier__c');
        Apexpages.currentPage().getParameters().put('mailType','Issue Contract');
        
        SendEmailController cntrl3 = new SendEmailController();  
        cntrl3.fillAttachmentModel();
        cntrl3.sendEmail();
        cntrl3.checkCongaStatus();
        cntrl3.redirectToCompleted();
        cntrl3.loadContentForEnergyAssessment();
        cntrl3.loadContentForInspectionRequest();
        cntrl3.loadContentForRegistrationRequest();
        cntrl3.loadContentForBarrier();
        cntrl3.emailSubject = null;
        cntrl3.emailBody = null;
        
        Apexpages.currentPage().getParameters().put('attId',att.id);
        Apexpages.currentPage().getParameters().put('attName',att.Attachment_Name__c );
        cntrl3.editAttachmentName();
        Apexpages.currentPage().getParameters().put('attName','Test' );
        cntrl3.saveAttachmentName();
    }
}