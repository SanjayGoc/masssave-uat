public class LeadContactMappings_Controller {
    /* Controller Properties */
    public Model model{get;set;}
    
    /* Constructor */
    public LeadContactMappings_Controller(){
        this.model=new Model();         
    }
    
    /* Local Classes */
    public class Model{
        public List<Lead_Contact_Mapping__c> leadContactMappings{get;set;} 
        public List<Schema.FieldSetMember> callListLineItemFields{get{
            if(this.callListLineItemFields == null)
                this.callListLineItemFields = Schema.SObjectType.Call_List_Line_Item__c.fieldSets.New_Lead_Contact_Fields.getFields();
            return this.callListLineItemFields;
        }private set;}
        public List<SelectOption> contactFields{get{
            if(this.contactFields == null){
                this.contactFields = new List<SelectOption>();//Schema.SObjectType.Contact.Fields.getMap().values();
                List<Schema.SObjectField> sObjectFields = Schema.SObjectType.Contact.Fields.getMap().values();
                for(Schema.SObjectField field : sObjectFields){
                    Schema.DescribeFieldResult res = field.getDescribe();
                    this.contactFields.add(new SelectOption(res.getName(), res.getLabel()));    
                }
            }
            return this.contactFields;
        }public set;}
        public List<SelectOption> leadFields{get{
            if(this.leadFields == null){
                this.leadFields = new List<SelectOption>();//Schema.SObjectType.Contact.Fields.getMap().values();
                List<Schema.SObjectField> sObjectFields = Schema.SObjectType.Lead.Fields.getMap().values();
                for(Schema.SObjectField field : sObjectFields){
                    Schema.DescribeFieldResult res = field.getDescribe();
                    this.leadFields.add(new SelectOption(res.getName(), res.getLabel()));    
                }
            }
            return this.leadFields;
        }public set;}
        
        public List<SelectOption> customerFields{get{
            if(this.customerFields == null){
                this.customerFields = new List<SelectOption>();//Schema.SObjectType.Contact.Fields.getMap().values();
                List<Schema.SObjectField> sObjectFields = Schema.SObjectType.customer__c.Fields.getMap().values();
                for(Schema.SObjectField field : sObjectFields){
                    Schema.DescribeFieldResult res = field.getDescribe();
                    this.customerFields.add(new SelectOption(res.getName(), res.getLabel()));    
                }
            }
            return this.customerFields;
        }public set;}
        
        /* Constructor */
        public Model(){
            this.loadLeadContactMappings();
        }
        private void loadLeadContactMappings(){
            Map<String,Lead_Contact_Mapping__c> callListLineItemFieldsMap = new Map<String,Lead_Contact_Mapping__c>();
            Integer i=0;
            for(Schema.SObjectField field :Schema.SObjectType.Call_List_Line_Item__c.fields.getMap().values()){
                Schema.DescribeFieldResult fieldRes = field.getDescribe();
                if(fieldRes.isCustom() && !fieldRes.isCalculated()){
                    callListLineItemFieldsMap.put(fieldRes.getName(),new Lead_Contact_Mapping__c(
                        Call_list_line_item_field__c = fieldRes.getName(),
                        Call_list_line_item_field_Label__c = fieldRes.getLabel(),
                        Call_list_line_item_field_Renamed_Label__c = fieldRes.getLabel(),
                        Index__c = i
                    ));
                    i++;
                }
            }
            
            this.leadContactMappings = [
                SELECT 
                  Id,
                  Call_list_line_item_field__c,
                  Call_list_line_item_field_Label__c,
                  Call_list_line_item_field_Renamed_Label__c,
                  Contact_Field__c,
                  Lead_field__c,
                  Index__c
                FROM 
                  Lead_Contact_Mapping__c 
                ORDER BY Index__c
            ];
            
            if(this.leadContactMappings.size() > 0 ){
                for(Lead_Contact_Mapping__c mapping : this.leadContactMappings){
                    if(callListLineItemFieldsMap.containsKey(mapping.Call_list_line_item_field__c)){
                        Lead_Contact_Mapping__c mapObj = callListLineItemFieldsMap.get(mapping.Call_list_line_item_field__c);
                        mapObj.Contact_Field__c = mapping.Contact_Field__c;
                        mapObj.Lead_field__c = mapping.Lead_field__c;
                        if(mapping.Call_list_line_item_field_Renamed_Label__c == null || mapping.Call_list_line_item_field_Renamed_Label__c == ''){
                            mapObj.Call_list_line_item_field_Renamed_Label__c = mapping.Call_list_line_item_field_Label__c;
                        }else{
                            mapObj.Call_list_line_item_field_Renamed_Label__c = mapping.Call_list_line_item_field_Renamed_Label__c;
                        }
                    }
                }
            }
            this.leadContactMappings = callListLineItemFieldsMap.values();
        }
        
        /* Actions */
        public void saveMappings(){
            try{
                delete [SELECT Id,Name FROM Lead_Contact_Mapping__c];
                Integer mappingSize = this.leadContactMappings.size();
                for(Lead_Contact_Mapping__c mapping : this.leadContactMappings){
                    if(mapping.Call_list_line_item_field_Renamed_Label__c == null || mapping.Call_list_line_item_field_Renamed_Label__c.trim() == ''){
                        mapping.Call_list_line_item_field_Renamed_Label__c = mapping.Call_list_line_item_field_Label__c;
                    }
                }
                if(mappingSize > 0){
                    insert this.leadContactMappings;
                    this.loadLeadContactMappings();
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, ' All Mapping(s) saved successfully.'));
                }else{
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, + 'No mapping to save. Please provide atleast one mapping.'));
                }
            }catch(Exception ex){
                ApexPages.addMessages(ex);
            }
            
        }
    }
}