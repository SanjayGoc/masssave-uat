public class BatchAvailableTimeSlotWT implements Database.batchable<SObject>,Database.Stateful,schedulable,Database.AllowsCallouts{ 
    
    public set<Id> stWTId = new Set<Id>();
    public BatchAvailableTimeSlotWT(Set<Id> setWTId){
        stWTId = setWTId;
    }
    
    public database.querylocator start(Database.batchableContext batchableContext){
         String sQuery ='select id,Work_Team__c,Work_Team__r.Service_Date__c from Work_Team_Member__c where Work_Team__c != null AND Work_Team__c Not In :stWTId'+(Test.isRunningTest()?' LIMIT 1':'');
         System.debug('===sQuery===='+sQuery);
         return Database.getQueryLocator(sQuery);
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> scope){ 
        Set<Id> wtId = new Set<Id>();
        Set<id> wtmIds = new Set<Id>();
        Map<Id,Id> mapWorkTM = new Map<Id,Id>();
        Map<Id,Work_Team__c> mapWT = new Map<Id,Work_Team__c>();
        Map<Id,DateTime> mapavailableSlot = new Map<Id,DateTime>();
        List<Available_Slot__c> lstASlot = new List<Available_Slot__c>();
        Map<Id,Appointment__c> mapTravelTime = new Map<Id,Appointment__c>();
        
        Work_Team_Member__c wtm = null;
        
        for(sObject o : scope){
            wtm = (Work_Team_Member__c)o;
            wtmIds.add(wtm.Id);
            wtId.add(wtm.Work_Team__c);
            //setDate.add(wtm.Service_Date__c);
            mapWorkTM.put(wtm.Work_Team__c,wtm.Id);
            
        }
        
        for(Work_Team__c w: [select Id,Start_Date__c,End_Date__c,Name from Work_Team__c where Id In :wtId AND Unavailable__c!=true AND Service_Date__c >= :Date.today() AND Start_Date__c!=null AND End_Date__c!=null]){
            Available_Slot__c objas = new Available_Slot__c();
            objas.Work_Team_Member__c = mapWorkTM.get(w.Id);
            objas.Start_Time__c = w.Start_Date__c;
            objas.End_Time__c = w.End_Date__c;
            objas.Duration__c = ((objas.End_Time__c.getTime())/1000/60 - (objas.Start_Time__c.getTime())/1000/60);
            objas.Slot_Time_and_Duration__c = objas.Start_Time__c.format('dd-MM-YYYY hh:mm a')+' - '+objas.End_Time__c.format('hh:mm a')+' '+objas.Duration__c+' min';
            objas.Work_Team_Member_Name__c = w.Name;
            
            lstASlot.add(objas);
        }
        
       
        
        
        if(lstASlot.size()>0){
            system.debug('@@size of lstASlot@@'+lstASlot.size());
            //insert lstASlot;
            System.enqueueJob(new QueueClass1(lstASlot));
        }
        
    }
    
    public void finish(Database.BatchableContext batchableContext) {
    
    }
    
    public void execute(SchedulableContext sc) {
        //BatchAvailableTimeSlotWT obj = new BatchAvailableTimeSlotWT();
        //database.executebatch(obj,200);
    }
}