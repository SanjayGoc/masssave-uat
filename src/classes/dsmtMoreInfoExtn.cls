public with sharing class dsmtMoreInfoExtn{

    public List<Exception__c> lstEnrollException{get;set;}
    public Integer counter{get;set;}
    public User usr{get;set;}
    public String templateName{get;set;}
    public Boolean isShow{get;set;}
    public Boolean isEnable{get;set;}
    public string savedString{get;set;}
    public string newinsertedExpId {get;set;}
    public list<String>lstStandardOutboundMessage{get;set;}
    public string selectedStandardExcpetion{get;set;}
    public list<SelectOption> lstStandardExceptionOptions{get;set;}
    public Map<Id,Exception__c> lstOverrideException = new Map<Id,Exception__c>();
    String[] Exceptions = new String[]{};
    
    public string expId{get;set;}
    public string vlu {get;set;}
    public string type {get;set;}
    String taskId = '';
    String RegRequestId = '';
    String projectId = '';
    public Registration_Request__c RegRequest{get;set;}
   
    String MLIId = '';
    Boolean isMLIId = false;
    set<string>insertedExceptionsIds = new set<string>();
    
    private map<string, string> prgm_fldr_map = new map<string, string>();
    public string selectedOutboundIds {get;set;}
    boolean isMIR2 = false;
    boolean isRejection = false;
    
    public dsmtMoreInfoExtn(){
        init();
    }
    
    public dsmtMoreInfoExtn(ApexPages.StandardController sc){
        init();
    }
    
    private void init(){
        selectedOutboundIds = '';
        RegRequest = new Registration_Request__c();
        isShow=true;
        newinsertedExpId = '';
        RegRequestId = apexpages.currentpage().getparameters().get('Id');//sc.getId();
        taskId= apexpages.currentpage().getparameters().get('TaskId');
        isMIR2 = apexpages.currentpage().getparameters().get('isMIR2') == 'true' ? true : false;
        isRejection = apexpages.currentpage().getparameters().get('isRejection') == 'true' ? true : false;
        fetchRegistrationRequest(RegRequestId);
        getAllExistingExceptions();
        system.debug('####'+lstEnrollException.size());
        if(lstEnrollException.size()>0){
            isShow = true;
        }else{
            isShow = false;
        }
        counter  = 0;
        savedString = '';
    }
    
    public void saveExp(){
        if(expId != null){            
            for(Exception__c exp : lstEnrollException){
                if(exp.Id == expId){
                    if(type == 'disposition'){
                        exp.Disposition__c = vlu;
                    }else if(type == 'Override'){
                        if(vlu == 'true' && exp.Override_Exception__c == false){
                            exp.Override_Exception__c = true;
                            exp.Overridden_By__c = UserInfo.getUserId();
                            exp.Overridden_Date__c = System.now();
                        }else{
                            exp.Override_Exception__c = false;
                        }
                    } 
                    update exp;
                    break;
                }
            }  
        }         
    }    
  
    private void fetchRegistrationRequest(String RRId){
        List<Registration_Request__c > lstRegRequest = [select id, Name,MIR_Email_Template__c,Trade_Ally_Type__c,Email__c from Registration_Request__c where id=:RRId];
        if(lstRegRequest.size()>0 ){
            RegRequest = lstRegRequest[0];
        }
    }
    
    public void getAllExistingExceptions(){
        
       lstEnrollException = [select id,Name,Exception_Message__c,FInal_Outbound_Message__c From Exception__c 
                             WHERE Registration_Request__c =: RegRequestId 
                             AND ((Is_Open__c = 1 AND Hidden__c = false AND  Sent_Date__c = null ) OR disposition__c = 'Insufficient information')]; 
    }

    public string concatenatedMsg {get;set;}
    
    public pagereference saveOutboundConcate(){
        try{
            if(RegRequest != null){
                string tplId = null;
            
                list<EmailTemplate>tplList = new list<EmailTemplate>();
                    if(isMIR2){
                        if(templateName == null || templateName == ''){           
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please select a Email Template.'));
                            return null;
                        }else{
                            tplList = [select id from EmailTemplate where DeveloperName =: templateName];
                            if(tplList.size()==0){
                                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Invalid Email Template Name.Please Correct name in Custom Setting.'));
                                return null;
                            }
                        }
                    }else{
                        if(RegRequest.MIR_Email_Template__c != null && RegRequest.MIR_Email_Template__c.trim().length()>0)
                            tplList = [select id from EmailTemplate where DeveloperName =: RegRequest.MIR_Email_Template__c];
                    }
                    system.debug('####' + tplList);
                    
                    if(tplList.size() > 0)
                         tplId = tplList[0].Id;
                    else{
                        List<Email_Custom_Setting__c> listEmailSett = [select id,RR_MIR_Email_Template__c from Email_Custom_Setting__c LIMIT 1];
                        if(listEmailSett.size() > 0){
                           String templatename = listEmailSett.get(0).RR_MIR_Email_Template__c;
                           tplList = [select id from EmailTemplate where DeveloperName =: 'Registration_Request_MIR_Template'];
                           tplId = tplList[0].Id;
                        }
                    }   
                
                    if(tplId == null){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'MIR Email Template is not found in Custom Setting.'));
                        return null;
                    }
                
                    if(RegRequest.Email__c == null){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This Registration Request does not have email address.'));
                        return null;
                    }
                    
                    RegRequest.Concatenated_Outbound_Messages_RichText__c = concatenatedMsg;
                    RegRequest.Outbound_Message_Ids__c= selectedOutboundIds;
                    update RegRequest;
                    
                
                    pagereference pg = new pagereference('/apex/dsmtMailTemplate?objId='+ RegRequest.id + '&TaskId=' + TaskId + '&TemplateID=' + tplId + '&MIR=true&exp_ids='+selectedOutboundIds+'&isRejection='+isRejection);
                
                    return pg;
            }    
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
        }    
        return null;
    }
    
    public List<SelectOption> getTemplateList(){
        
        List<SelectOption>lstOptions = new list<SelectOption>();        
        lstOptions.add(new selectOption('','--None--'));
      
        return lstOptions;
    }    
    
 
    
}