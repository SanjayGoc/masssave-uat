public class dsmtCreateProjectOnRecommCntrl{
    
    public Eligibility_Check__c custInteraction {get;set;} 
    public Employee__c emp {get;set;} 
    public Date stDate;
    public Date endDate;
    
    public dsmtCreateProjectOnRecommCntrl(){
        custInteraction = new Eligibility_Check__c();
        emp = new Employee__c();
        
        
    }
    
    public pagereference updateTerr(){
        stDate = custInteraction.Start_Date__c;
        endDate = custInteraction.End_Date__c;
        string empId = ApexPages.currentPage().getParameters().get('id');
        if(stDate != null && endDate != null){
            emp.Id = empId;
            
            update emp;
            List<Work_Team__c> lstWT = [select Id,Territory__c from Work_Team__c where Service_Date__c >= :stDate AND Service_Date__c <= :endDate AND Captain__c = :empId];
            if(lstWT.size()>0){
                for(Work_Team__c wt: lstWT){
                    wt.Territory__c = emp.Territory__c;
                }
                
                update lstWT;
            }
        }
        
        Pagereference pg = new Pagereference('/'+empId);
        pg.setRedirect(true);
        return pg;
    }
    
}