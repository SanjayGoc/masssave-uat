@IsTest
public with sharing class autologoutsetinactiveCntrlTest {
    
    @IsTest
    static void runTest(){
        
        User u = Datagenerator.CreatePortalUser();
        
        test.startTest();
        
        system.runAs(u){
       
            PageReference pageRef = Page.endlogout;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('uid',u.Id);
            
            autologoutsetinactiveCntrl cntrl = new autologoutsetinactiveCntrl();
            
            System.assertEquals(null,cntrl.relogin());
            System.assertEquals(null,cntrl.endlogin());
            System.assertEquals(null,cntrl.fetchUserId());
        }
        test.stopTest();
    }
    
    @IsTest
    static void validate_fetchUserId(){
        User u = Datagenerator.CreatePortalUser();
        
        test.startTest();
        
        system.runAs(u){
        PageReference pageRef = Page.endlogout;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('uid',u.Id);

        autologoutsetinactiveCntrl cntrl = new autologoutsetinactiveCntrl(); 
        //PageReference pageRef = cntrl.fetchUserId();
        
        //Beginning of System Asserts, verify pageRef and URL First
        System.assertEquals(null,cntrl.fetchUserId());
        //System.assertEquals('/apex/endlogout',pageRef.getUrl());
        
        //Verify the pageRef parameters as well
        //Map<String,String> pageParameters = pageRef.getParameters();
        //System.assertEquals(1,pageParameters.values().size());
        //System.assertNotEquals(null,pageParameters.get('uid'));
        }
        test.stopTest();
    }
    
    @IsTest
    static void validate_updateUserandLogout(){
        User u = Datagenerator.CreatePortalUser();
        
        test.startTest();
        
        system.runAs(u){
        
        PageReference pageRef = Page.endlogout;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('uid',u.Id);

        autologoutsetinactiveCntrl cntrl = new autologoutsetinactiveCntrl();
        
        System.assertNotEquals(null,cntrl.updateUserandLogout());
        }
        test.stopTest();
    }
}