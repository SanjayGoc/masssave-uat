public with sharing class dsmtWorkScopeReviewSummaryController{

    public string recommXml {get;set;}
    public string prjId{get;set;}
    public Review__c rev{get;set;}
    public string dataSave{get;set;}
    public String revId{get;set;}
    public decimal subTotal {get;set;}
    public decimal subTotalVirtual{get;set;}
    public decimal utilityIncentive {get;set;}
    public decimal utilityIncentiveVirtual{get;set;}
    public decimal customerContribution {get;set;}
    public decimal customerContributionVirtual{get;set;}
    public decimal customerDeposit {get;set;}
    public decimal remainingCustomerContribution {get;set;}
    public decimal remainingCustomerContributionVirtual{get;set;} // DSST-15177 - PP
    public String projId {get;set;}
    public decimal preWeatherization{get;set;}
    
    
    public List<SelectOption> projList {get;set;}
    
    public dsmtWorkScopeReviewSummaryController(){
        rev = new Review__c();
        prjId = ApexPages.currentPage().getParameters().get('prjid');
        revId = ApexPages.currentPage().getParameters().get('id');
        
        loadRecommendations();
    }
    
    public dsmtWorkScopeReviewSummaryController(ApexPages.StandardController controller) {
        rev = new Review__c();
        prjId = ApexPages.currentPage().getParameters().get('prjid');
        revId = ApexPages.currentPage().getParameters().get('id');
        
        loadRecommendations();
    }
    
    public PageReference createReviewRec(){
        
        String projId = ApexPages.currentPage().getParameters().get('projid');
        
        if(revId != null)
        {
            List<Review__C> lstPrj = [select Id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                        Energy_Assessment__r.Trade_Ally_Account__r.Email__c,First_Name__c,
                                        Last_Name__c,Phone__c,Email__c,Address__c,City__c,State__c,
                                        Zipcode__c,Trade_Ally_Account__c from Review__C 
                                        where Id =: revId limit 1]; 
            rev.Status__c = 'New';
            Id devRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();
    
            List<Dsmtracker_Contact__c> dsmtList = [select id,Dsmtracker_Contact__c,Trade_Ally_Account__c from Dsmtracker_Contact__c where Portal_User__c =: userinfo.getUSerID()
                                                        and Trade_Ally_Account__c  != null];
            if(dsmtList.size() > 0){
                rev.Trade_Ally_Account__c = dsmtList.get(0).Trade_Ally_Account__c;
            }                                   
            if(lstPrj.get(0).Energy_Assessment__c != null)    
                rev.Energy_Assessment__c = lstPrj.get(0).Energy_Assessment__c;
            
            rev.Type__c = 'Change Order Review';
            rev.First_Name__c = lstPrj.get(0).First_Name__c;
            rev.Last_Name__c = lstPrj.get(0).Last_Name__c;
            rev.Phone__c = lstPrj.get(0).Phone__c;
            rev.Email__c = lstPrj.get(0).Email__c;
            rev.Address__c = lstPrj.get(0).Address__c;
            rev.City__c = lstPrj.get(0).City__c;
            rev.State__c = lstPrj.get(0).State__c;
            rev.Zipcode__c = lstPrj.get(0).Zipcode__c;
            rev.Work_Scope_Review__c = lstPrj.get(0).Id;
            
            if(lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c != null)
                rev.Trade_Ally_Email__c = lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c;
            
            rev.RecordTypeId =  devRecordTypeId;  
            upsert rev; 
     
            rev = [select Id,Name,Project__c,Status__c,Work_Scope_Review__c,Trade_Ally_Account__r.Name,Energy_Assessment__r.Name from Review__c where Id = :rev.Id limit 1];
        }
        else if(projId != null)
        {    
            List<Project_Review__c> prList = [select id, Review__c, Project__c 
                                              from Project_Review__c 
                                              where Project__c =: projId
                                              and Review__c != null
                                              and (Review__r.RecordType.Name = 'Work Scope Review'
                                              or Review__r.RecordType.Name = 'Work Scope Review Manual')];
            
            if(prList.size() > 0)
                return new PageReference('/apex/AddRemoveReviewRecommendation?id='+prList[0].Review__c+'&projid='+projId).setRedirect(true);
        }
        
        return null;
    }
    
    private set<String> fetchProjectIdSet(Id reviewId)
    {
        set<String> projectIdSet = new Set<String>();
        
        List<Project_Review__c> revList = [select id, Name, Review__c, Project__c 
                                           from Project_Review__c 
                                           where Review__c =: reviewId
                                           and Review__c != null];
        for(Project_Review__c pr : revList)
        {
            projectIdSet.add(pr.Project__c);
        }
        
        return projectIdSet;
    }
    
    public void loadRecommendations()
    {
        if(projList == null || projList.size() == 0)
        {
            projList = new List<SelectOption>();
            projList.add(new SelectOption('All', 'All Projects'));
        }
        
        remainingCustomerContribution = customerDeposit = utilityIncentive = subTotal = customerContribution = preWeatherization = subTotalVirtual = 0;
        utilityIncentiveVirtual = customerContributionVirtual = remainingCustomerContributionVirtual = 0; // DSST-15177 - PP
        
        recommXml = '<Recommendations>';
        
        Set<String> projIds = new Set<String>();
        
        string revId1 = ApexPages.currentPage().getParameters().get('id');
        
        if(revId != null){
            projIds = fetchProjectIdSet(revId1);
        }else{
            String projId = ApexPages.currentPage().getParameters().get('projid');
            projIds.add(projId);
        }
        
        Set<Id> prjIds = new Set<Id>();
        map<string, decimal> projCollectedMap = new map<string, decimal>();
        map<string, decimal> projBarrierIncMap = new map<string, decimal>();
        
        String projects = ApexPages.currentPage().getParameters().get('projects');
        
        if(projects != null && projects != 'All'){
            projIds = new Set<string>();
            projIds.add(projects);
        }
        
        system.debug('projIds :::::' + projIds);
        
        Map<Id,Decimal> changeOrderMap = new Map<Id,Decimal>();
        
        if(projIds != null && projIds.size() > 0){
        
            String query = 'select ' + sObjectFields('Recommendation__c') +' Recommendation_Scenario__r.Total_Collected__c,Recommendation_Scenario__r.Barrier_Incentive__c, Recommendation_Scenario__r.Total_CustomerIncentive__c, Recommendation_Scenario__r.Customer_Cost__c, Id,DSMTracker_Product__r.Name,Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Type__c,Recommendation_Scenario__r.Status__c,Recommendation_Scenario__r.Installed_Date__c,Energy_Assessment__r.Primary_Provider__c,Dsmtracker_Product__r.EMHome_EMHub_PartID__c,(Select id,Project__c,Recommendation__c,Utility_Incentive_Share__c,Final_Recommendation_Incentive__c,proposal__r.current_Incentive__c,proposal__r.Final_Proposal_Incentive__c,proposal__r.Barrier_Incentive_Amount__c from Proposal_Recommendations__r where Project__c =:projIds) from Recommendation__c where Recommendation_Scenario__c  != null and  Recommendation_Scenario__c in :projIds order by CreatedDate desc limit 300 ';
            
            Set<Id> recId = new Set<Id>();
            
            for (Recommendation__c ir: database.query(query)) {
                recId.add(ir.Id);
                changeOrderMap.put(ir.id,ir.Change_Order_Quantity__c);

            }
            /*
            if(recId.size() > 0){
                List<Review__c> changeRevList = [select id from Review__c where (Work_Scope_Review__c =: revId or Billing_Review__c =: revId) and RecordType.Name = 'Change Order Review' order by CreatedDate Desc];
                
                if(changeRevList  != null && changeRevList.size() > 0){
                    
                    Set<Id> crId = new Set<Id>();
                    for( Review__c c : changeRevList){
                        crId.add(c.Id);
                    }
                
                    List<Change_Order_Line_Item__c> chageorderList = [select id,Change_Order_Quantity__c,Recommendation__c from Change_Order_Line_Item__c
                                                                        where Recommendation__c in : recId 
                                                                        //and Review__c =: revId 
                                                                        and change_Order_Review__c =: changeRevList.get(0).Id
                                                                        order by CreatedDate Desc];
                    for(Change_Order_Line_Item__c c : chageorderList){
                        if(changeOrderMap.get(c.Recommendation__c) == null && c.Change_Order_Quantity__c != null){
                            changeOrderMap.put(c.Recommendation__c,c.Change_Order_Quantity__c);
                        }
                    }
                }
            }
            */
            
            system.debug('--changeOrderMap---'+changeOrderMap);
            Set<String> proposalIds = new Set<String>();
            for (Recommendation__c ir: database.query(query)) {
                
                List<Proposal_Recommendation__c> ProposalRecList = ir.Proposal_Recommendations__r;  
                system.debug('ProposalRecList--'+ProposalRecList);
                
                if(ir.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(ir.Recommendation_Scenario__c + '~~~' + ir.Recommendation_Scenario__r.Name, ir.Recommendation_Scenario__r.Total_Collected__c);
                
                if(ir.Recommendation_Scenario__r.Barrier_Incentive__c != null){ 
                    projBarrierIncMap.put(ir.Recommendation_Scenario__c,ir.Recommendation_Scenario__r.Barrier_Incentive__c);
                }
                
                recommXml += '<Recommendation>';
                recommXml += '<Checked>' + false + '</Checked>';
                recommXml += '<ProjectId>' + ir.Recommendation_Scenario__c + '</ProjectId>';
                recommXml += '<ProjectName><![CDATA[<a href="/'+ir.Recommendation_Scenario__c+'" target="_blank">' + checkStringNull(ir.Recommendation_Scenario__r.Name) + '</a>]]></ProjectName>';
                recommXml += '<RecommendationId>' + ir.Id + '</RecommendationId>';
                recommXml += '<RecommendationName><![CDATA[' + checkStringNull(ir.DSMTracker_Product__r.Name) + ']]></RecommendationName>';
                
                if(ir.Quantity__c  == null)
                    recommXml += '<Quantity>0</Quantity>';
                else
                    recommXml += '<Quantity><![CDATA[' + ir.Quantity__c + ']]></Quantity>';
                
                if(changeOrderMap.get(ir.Id) == null)
                    recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
                else    
                    recommXml += '<ChangeOrderQuantity><![CDATA[' + changeOrderMap.get(ir.Id) + ']]></ChangeOrderQuantity>';
                
                if(changeOrderMap.get(ir.Id) != null){
                        recommXml += '<PartCost><![CDATA[' + changeOrderMap.get(ir.Id) * (ir.Unit_Cost__c == null ? 0 : ir.Unit_Cost__c)+ ']]></PartCost>';
                }else{  
                    if(ir.Part_Cost__c != null && ir.Part_Cost__c != 0){
                        recommXml += '<PartCost><![CDATA[' + ir.Part_Cost__c+ ']]></PartCost>';
                    }else{
                        recommXml += '<PartCost></PartCost>';
                    }
                }
                    
                recommXml += '<Reason><![CDATA[' + checkStringNull(ir.Change_Order_Reason__c)+ ']]></Reason>';
                
                recommXml += '<UtilityIncentive><![CDATA[' + ir.Total_Recommended_Incentive__c+ ']]></UtilityIncentive>';
                recommXml += '<PartId><![CDATA[' + checkStringNull(ir.Dsmtracker_Product__r.EMHome_EMHub_PartID__c)+ ']]></PartId>';
                recommXml += '<UtilityBeingCharged><![CDATA[' + checkStringNull(ir.Energy_Assessment__r.Primary_Provider__c)+ ']]></UtilityBeingCharged>';
                
                if(ir.Installed_Date__c == null)
                    recommXml += '<InstallDate></InstallDate>';
                else
                    recommXml += '<InstallDate><![CDATA[' + ir.Installed_Date__c.format().replace(' 00:00:00', '') + ']]></InstallDate>';
                
                if(ir.Invoice_Type__c == null)
                    recommXml += '<InvoiceType></InvoiceType>';
                else
                    recommXml += '<InvoiceType><![CDATA[' + ir.Invoice_Type__c + ']]></InvoiceType>';
                
               /*if(ir.Installed_Quantity__c == null)
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                else
                    recommXml += '<InstalledQuantity><![CDATA[' + ir.Installed_Quantity__c + ']]></InstalledQuantity>';
               */ 
                
                
                if(changeOrderMap.get(ir.Id) == null)
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                else
                    recommXml += '<InstalledQuantity><![CDATA[' + changeOrderMap.get(ir.Id) + ']]></InstalledQuantity>';
                    
                if(ir.Status__c != null)
                    recommXml += '<Status><![CDATA[' + ir.Status__c + ']]></Status>';
                else
                    recommXml += '<Status></Status>';
                
                recommXml += '</Recommendation>';
                
                if(ir.Unit_Cost__c != null && changeOrderMap.get(ir.Id) != null){
                    subTotalVirtual += ir.Unit_Cost__c * changeOrderMap.get(ir.Id);
                } else if(ir.Unit_Cost__c != null && ir.Quantity__c != null){
                    subTotalVirtual += ir.Unit_Cost__c * ir.Quantity__c;
                }
                
               if(ir.Unit_Cost__c != null && ir.Quantity__c != null)
                    subTotal += ir.Unit_Cost__c * ir.Quantity__c ;
                
                Proposal_Recommendation__c pr = null;
                if(!ir.Proposal_Recommendations__r.isEmpty()) { 
                    pr = ir.Proposal_Recommendations__r.get(0);
                }
                
                if(pr != null){
                    if(proposalIds.add(pr.proposal__c)){                        
                        utilityIncentiveVirtual += pr.Proposal__r.Final_Proposal_Incentive__c - pr.Proposal__r.Barrier_Incentive_Amount__c;    
                        preWeatherization += (pr.Proposal__r.Barrier_Incentive_Amount__c != null)?pr.Proposal__r.Barrier_Incentive_Amount__c:0;
                    }
                    // DSST-15812 - START - PP 
                    if(ir.Original_Part_Cost__c != null && pr.Proposal__r.Current_Incentive__c != null){
                        utilityIncentive += (ir.Original_Part_Cost__c * pr.proposal__r.current_Incentive__c * 0.01).setScale(2); // DSST-13594 - PP
                        
                    }
                    // DSST-15812 - END - PP
                }

                //if(ir.Recommendation_Scenario__r.Total_CustomerIncentive__c != null)
                //    utilityIncentive += ir.Recommendation_Scenario__r.Total_CustomerIncentive__c;
                
                //if(ir.Recommendation_Scenario__r.Customer_Cost__c != null && prjIds.add(ir.Recommendation_Scenario__c))
                //    customerContribution += ir.Recommendation_Scenario__r.Customer_Cost__c;
            }
        }
        
        system.debug('--recommXml ---'+recommXml );
        
       /* if(subTotal > 0)
            utilityIncentive = (subTotal * .75);*/
              
        
        customerContribution = subTotal - utilityIncentive - preWeatherization; //DSST-14253 - PP
        
        if(customerContribution != null){
            customerContribution.setScale(2);
        }
        
        customerContributionVirtual = subTotalVirtual - utilityIncentiveVirtual - preWeatherization; // DSST-14253 - PP
        
        boolean isAdd = false;
        
        if(projList.size() == 1)
            isAdd = true;
        
        for(String s : projCollectedMap.keyset())
        {
            customerDeposit += projCollectedMap.get(s);
            
            if(isAdd)
                projList.add(new SelectOption(s.split('~~~')[0], s.split('~~~')[1]));
        }
        
        /*for(String s : projBarrierIncMap.keyset())
        {
            preWeatherization += projBarrierIncMap.get(s);
        }*/
        
        remainingCustomerContribution = (customerContribution - customerDeposit);
        remainingCustomerContributionVirtual  = (customerContributionVirtual - customerDeposit); //DSST-15177 - PP
 
        recommXml += '</Recommendations>';
    }
    
    public string checkStringNull(string str){
        return str == null?'':str;
    }
    
    public void saveProjId(){
        
    }
    
     private static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
}