@isTest
public class dsmtAddEditServiceRequestCntrlTest {
    static testmethod void test(){
        
        
        //insert new System_Config__c();
                   
      /*  User u = Datagenerator.CreatePortalUser();
        
        DSMTracker_Contact__c dc = new DSMTracker_Contact__c();
        dc.Contact__c = u.ContactId;
        insert dc;
        */
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'test21234@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id,
        Billing_Account_Number__c= 'Gas-~~~1234'
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile where name ='Scheduler Custom Profile'];
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
       
        System.runAs (user1) {
            
            insert new System_Config__c(Arrival_Window_min_before_Appointment__c = 10, Arrival_Window_min_after_Appointment__c = 10);
        DSMTracker_Contact__c dc = new DSMTracker_Contact__c();
        dc.Contact__c = user1.ContactId;
        insert dc;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        Workorder__c wo = Datagenerator.createWo();
            wo.Early_Arrival_Time__c = Datetime.now().addHours(-1);
            wo.Late_Arrival_Time__c = Datetime.now().addHours(1);
            wo.Eligibility_Check__c = ec.id;
        insert wo;

        Premise__c pr = Datagenerator.createPremise();
        insert pr;
        
        Customer__c c = Datagenerator.createCustomer(portalAccount1.Id, pr.Id);
        insert c;
        
        Appointment__c apt = new Appointment__c();
        apt.Customer_Reference__c = c.Id;
        apt.DSMTracker_Contact__c = dc.Id;
        apt.Appointment_Start_Time__c=date.today();
        apt.Appointment_End_Time__c= date.today();
        insert apt;
               
        Service_Request__c sr = new Service_Request__c();
        sr.Account__c = portalAccount1.Id;
        sr.Appointment__c = apt.Id;
        sr.Customer__c = c.Id;
        insert sr;
        
        Trade_Ally_Account__c tac = Datagenerator.createTradeAccount();
        Attachment__c att = Datagenerator.createAttachment(tac.Id);
        att.Service_Request__c = sr.Id;
        update att;
         
        UCS_AgilePMO__Ticket__c t = new UCS_AgilePMO__Ticket__c();
        t.Project__c = 'Mass Save POC PII';
        t.MS20_Program_Stream__c = 'Operations';
        insert t;
      
        Phase_I_Ticket_Detail__c phtkt = new Phase_I_Ticket_Detail__c();
        phtkt.Service_Request__c = sr.Id;
        insert phtkt;
        
        Note n = new Note();
        n.ParentId = sr.Id;
        n.Title = 'Test Note';
        insert n;
        Test.startTest();
        ApexPages.currentPage().getParameters().put('id',sr.Id);
        dsmtAddEditServiceRequestCntrl controller = new dsmtAddEditServiceRequestCntrl();
        controller.saveAndSubmit();
        controller.saveTicket();
        dsmtAddEditServiceRequestCntrl.getAttachmentsByTicketId(sr.Id,phtkt.Id);
        dsmtAddEditServiceRequestCntrl.delAttachmentsById(att.Id, t.Id);
        controller.getBoilerFormDetails();
        controller.getFurnaceFormDetails();
        
        controller.getTicketTypes();
        controller.getSrNotes();
        controller.loadWorkOrders(); 
        String purl = controller.PortalURL;
        String org = controller.orgId;
        controller.newNote=new Note(ParentId=controller.objPTicket.id,Title='Testing');
        dsmtAddEditServiceRequestCntrl.CallLogModel clm=new dsmtAddEditServiceRequestCntrl.CallLogModel(portalAccount1.id);
        controller.newCallLog=clm;
        controller.newCount=2;
        controller.oldCount=1;
        controller.saveNote();
        controller.checkCongaStatus();
        Attachment attach=new Attachment();
        attach.Body=Blob.valueOf('Some Text');
        attach.ParentId=controller.objPTicket.id;
        attach.Name='Dummy Document';
        insert attach;
        controller.checkCongaStatus();
        dsmtAddEditServiceRequestCntrl.getOtherAttachmentsByTicketId(controller.objPTicket.id);
        Test.stopTest();
        } 
        
    }

    }
  /*  static testmethod void test2(){
        
        System_Config__c.getOrgDefaults().Arrival_Window_min_before_Appointment__c = 10;
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = hexDigest + 'test2@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id,
        Billing_Account_Number__c= 'Gas-~~~1234'
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile where name ='Scheduler Custom Profile'];
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
       
        System.runAs (user1) {
            insert new System_Config__c(Arrival_Window_min_before_Appointment__c = 10, Arrival_Window_min_after_Appointment__c = 10);
        DSMTracker_Contact__c dc = new DSMTracker_Contact__c();
        dc.Contact__c = user1.ContactId;
        insert dc;
        
        Workorder__c wo = Datagenerator.createWo();
            wo.Early_Arrival_Time__c = Datetime.now().addHours(-1);
            wo.Late_Arrival_Time__c = Datetime.now().addHours(1);
        insert wo;

        Premise__c pr = Datagenerator.createPremise();
        insert pr;
        
        Customer__c c = Datagenerator.createCustomer(portalAccount1.Id, pr.Id);
        insert c;
        
        Appointment__c apt = new Appointment__c();
        apt.Customer_Reference__c = c.Id;
        apt.DSMTracker_Contact__c = dc.Id;
        apt.Appointment_Start_Time__c=date.today();
        apt.Appointment_End_Time__c= date.today();
        insert apt;
               
        Service_Request__c sr = new Service_Request__c();
        sr.Account__c = portalAccount1.Id;
        sr.Appointment__c = apt.Id;
        sr.Customer__c = c.Id;
        insert sr;
        
        Trade_Ally_Account__c tac = Datagenerator.createTradeAccount();
        Attachment__c att = Datagenerator.createAttachment(tac.Id);
        att.Service_Request__c = sr.Id;
        update att;
         
        UCS_AgilePMO__Ticket__c t = new UCS_AgilePMO__Ticket__c();
        insert t;
      
        Phase_I_Ticket_Detail__c phtkt = new Phase_I_Ticket_Detail__c();
        phtkt.Service_Request__c = sr.Id;
        insert phtkt;
        
        Note n = new Note();
        n.ParentId = sr.Id;
        n.Title = 'Test Note';
        insert n;
        
        ApexPages.currentPage().getParameters().put('id','');
        dsmtAddEditServiceRequestCntrl controller = new dsmtAddEditServiceRequestCntrl();
        controller.saveAndSubmit();
        controller.saveTicket();
        dsmtAddEditServiceRequestCntrl.getAttachmentsByTicketId(sr.Id,phtkt.Id);
        dsmtAddEditServiceRequestCntrl.delAttachmentsById(att.Id, t.Id);
        controller.getTicketTypes();
        controller.getSrNotes();
        controller.loadWorkOrders(); 
        Note n1 = controller.newNote;
            n1.Title = 'Title';
        controller.saveNote();
        dsmtAddEditServiceRequestCntrl.CallLogModel cm =   controller.newCallLog;
        String purl = controller.PortalURL;
        String org = controller.orgId;
       
        } 
        
    }
}*/
    static testmethod void test3(){
        
        
        
        
        dsmtAddEditServiceRequestCntrl.CallLogModel model2 = new dsmtAddEditServiceRequestCntrl.CallLogModel(new List<Task>());
        Task tsk = model2.createTaskObject();
        INSERT tsk;
        dsmtAddEditServiceRequestCntrl.CallLogModel model3 = new dsmtAddEditServiceRequestCntrl.CallLogModel(tsk.Id);
        
        dsmtAddEditServiceRequestCntrl.CallLogModel model = new dsmtAddEditServiceRequestCntrl.CallLogModel(new List<Task>{tsk});
        model.WhatId = null;
        model.Id = null;
        model.Subject = null;
        model.Description = null;
        model.CreatedDate = null;
        model.CreatedByName = null;
        List<dsmtAddEditServiceRequestCntrl.CallLogModel> callLogs = model.callLogs;
        String asJson = model.asJson;
    }
}