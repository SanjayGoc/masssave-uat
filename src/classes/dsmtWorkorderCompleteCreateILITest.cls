@isTest
public class dsmtWorkorderCompleteCreateILITest
{
    public testmethod static void test1()
    {
    
    
       try{
    
       // List<Workorder_Type__c> wtypeList = new List<Workorder_Type__c>();
        Workorder_Type__c wot = new Workorder_Type__c();
        wot.Name = 'Expanded HEA';
        insert wot;
 
        Workorder__c  wo    = new Workorder__c();
        wo.Status__c   = 'Pending';
        wo.Workorder_Type__c = wot.id;
        insert wo;
        
          Workorder_Type_Invoice_Item_Setting__c work = new Workorder_Type_Invoice_Item_Setting__c();
        work.Workorder_Type__c = wot.id;
        work.Name = 'test';
        work.part_Id__c='test1';
        work.Is_not_CLEAResult__c= false;
        insert work;
        
        Dsmtracker_Product__c dsm = new Dsmtracker_Product__c();
        dsm.Eversource_Labor_Price__c = 12;
        dsm.EMHome_EMHub_PartID__c ='test';
      //  dsm.Workorder_Type_Invoice_Item_Setting__c=work.part_Id__c;
        insert dsm;
        
         Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c=false;
        insert ta;
        
        Appointment__c app = new Appointment__c();
        app.Workorder__c = wo.id;
        app.Trade_Ally_Account__c =ta.id;
        
        wo.Status__c = 'Completed';
        update wo;
        
        }catch(Exception e){
          system.debug('--e--'+e.getLineNumber());
        }
     
    }
}