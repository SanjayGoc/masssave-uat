@isTest
public class dsmtChangePasswordContTest {
	testmethod static void runTest()
    {
        User u = Datagenerator.CreatePortalUser();
        System.runAs(u)
        {
            DSMTracker_Contact__c dsmt = Datagenerator.createDSMTracker();
            Trade_Ally_Account__c ta = Datagenerator.createTradeAccount();
            dsmt.Contact__c = u.ContactId;
            dsmt.Trade_Ally_Account__c = ta.Id;
            update dsmt;
            
            dsmtChangePasswordCont cont = new dsmtChangePasswordCont();
            cont.isShowError = true;
            cont.password='test111';
            cont.confirmPassword='test1120';
            cont.updatePassword();
        }
    }
}