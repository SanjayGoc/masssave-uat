@istest
public class BasementFloorComponentCntrlTest
{
	@istest
     static void runTest()
     {
         Floor__c fc =new Floor__c();
         insert fc;
		
         Layer__c ly =new Layer__c();
         ly.Name='test';
         ly.HideLayer__c = false;
         ly.Floor__c=fc.id;
         insert ly;
         
         BasementFloorComponentCntrl bfcc = new BasementFloorComponentCntrl ();
         String newLayer = bfcc.newLayerType;
         bfcc.basementFloorId=fc.Id;
         bfcc.getLayerTypeOptions();
         bfcc.getFloorLayersMap();
         bfcc.saveFloor();
         
         ApexPages.currentPage().getParameters().put('layerType','Flooring');
         bfcc.addNewFloorLayer();
         ApexPages.currentPage().getParameters().put('index','0');
         bfcc.deleteFloorLayer();
     }
}