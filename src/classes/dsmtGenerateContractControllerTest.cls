@isTest
public class dsmtGenerateContractControllerTest{
    public testmethod static void test1(){
        
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        test.startTest();
       
        Proposal__c proposal =new Proposal__c();
        proposal.Energy_Assessment__c=ea.id;
        proposal.Name_Type__c='Weatherization';
        //proposal.Type__c = 'Insulation';
        proposal.Current_Incentive__c = 10;
        proposal.Status__c='Contracted';
        proposal.Deposit_Amount__c = 10;
        insert proposal;
        
        APXTConga4__Conga_Template__c Template = new APXTConga4__Conga_Template__c();
        //template.name = 'Mass Proposal Contract clearesult template Proposal';
        template.Unique_Template_Name__c = 'Mass Proposal Contract clearesult template';
        insert template;
                
        ApexPages.StandardController sc = new ApexPages.StandardController(proposal);
        dsmtGenerateContractController cont = new dsmtGenerateContractController(sc);
        cont.generateContract();
        test.stopTest();
        
    }
}