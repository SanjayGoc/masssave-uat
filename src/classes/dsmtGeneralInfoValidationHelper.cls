public class dsmtGeneralInfoValidationHelper extends dsmtEnergyAssessmentValidationHelper{
    public ValidationCategory validate(String categoryTitle,String uniqueName,Id customerId,ValidationCategory otherValidations){
        ValidationCategory general = new ValidationCategory(categoryTitle,uniqueName);
        try{
            List<Customer__c> customers = Database.query('SELECT '+getSObjectFields('Customer__c')+' FROM Customer__c WHERE Id=:customerId');
            if(customers.size() > 0){
                Customer__c customer = customers.get(0);
                if(customer.Gas_Provider_Name__c == null || customer.Gas_Provider_Name__c.trim() == ''){
                    general.errors.add(new ValidationMessage('Gas provider (Primary Energy Provider) must be selected'));
                }
                if(customer.Gas_Account_Number__c == null || customer.Gas_Account_Number__c.trim() == ''){
                    general.errors.add(new ValidationMessage('Gas Account number is required.'));
                }
                if(customer.Electric_Provider_Name__c == null || customer.Electric_Provider_Name__c.trim() == ''){
                    general.errors.add(new ValidationMessage('Gas provider (Secondary Energy Provider) must be selected'));
                }
                if(customer.Electric_Account_Number__c == null || customer.Electric_Account_Number__c.trim() == ''){
                    general.errors.add(new ValidationMessage('Electric Account number is required.'));
                }
            }
        }catch(Exception ex){
            general.systemExceptions.add(new ValidationMessage(ex));
        }
        return general;
    }
}