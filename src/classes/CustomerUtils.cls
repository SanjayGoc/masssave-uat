public class CustomerUtils{
   
   public static void createServiceRequest(List<Customer__c> cslist,Map<Id,Customer__c> oldMap){
        Map<Id,Customer__c> custMap=new Map<Id,Customer__c>();
        Id cusid;
        for(Customer__c cs : cslist){
          custMap.put(cs.id,cs);
          cusid=cs.id;
        }
        Map<Id,Energy_Assessment__c> assmtMap=new Map<id,Energy_Assessment__c>();
        if(custMap.size()>0){
            assmtMap.putAll([SELECT id,Name,Customer__c,Status__c FROM Energy_Assessment__c WHERE Customer__c=:custMap.Keyset() AND Status__c=:'Assessment Complete']);
        } 
        List<Review__c> revList=new List<Review__c>();
        if(assmtMap.size()>0){
            revList=[SELECT id,Name,Energy_Assessment__c,Status__c FROM Review__c WHERE Energy_Assessment__c=:assmtMap.keySet() AND Status__c=:'Batched'];
       
        }
        if(revList.size()>0){ 
          Id recid=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Billing Adjustment').getRecordTypeId();
          //Queue qt=[SELECT id,Name FROM Queue WHERE Name=:'Billing Adjustment queue'];
                Service_Request__c serReq=new Service_Request__c();
                serReq.Customer__c=cusid;
                serReq.Review__c=revList[0].id;
                serReq.OwnerId='00G4D000000eaUx';
                serReq.RecordTypeId=recid;
                insert serReq;
           }
    }
}