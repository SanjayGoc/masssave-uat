@isTest
Public class dsmtCancelWorkorderCntrlTest{
    @isTest
    public static void runTest(){
            Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.Createwo();
        wo.Location__c = locObj.Id;
        wo.Work_Team__c = wt.Id;
        wo.early_Arrival_Time__c = DAtetime.now();
        wo.late_Arrival_Time__c = Datetime.now().Addminutes(150);
        wo.Scheduled_Start_Date__c= DAtetime.now();
        wo.Scheduled_End_Date__c= Datetime.now().Addminutes(150);
        
        insert wo;
        ApexPages.currentPage().getParameters().put('id',wo.Id);
        dsmtCancelWorkorderCntrl cntrl = new dsmtCancelWorkorderCntrl();
        cntrl.init();
        cntrl.cancelWO.Cancellation_Notes__c = 'test';
        cntrl.cancelWO.Cancel_Reason__c = 'test';
        cntrl.CancelWorkOrder();
    }
    
}