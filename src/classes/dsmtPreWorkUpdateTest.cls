@isTest(seeAllData=true)
public class dsmtPreWorkUpdateTest{
    public testmethod static void test1(){
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea=[select id,name from Energy_Assessment__c where Status__c='Assessment Complete' order by LastmodifiedDate desc limit 1];
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        proj.Energy_Assessment__c = ea.Id;
        insert proj;
        String str = proj.Id;
        Test.startTest();
            dsmtPreWorkUpdate.updateProject(proj.Id); 
        Test.stopTest();
        
    }
}