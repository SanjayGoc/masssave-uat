public class dsmtTradeallyLoginCtrl {
    
    /* Public Instance Variables */
    public String username {get; set;}
    public String password {get; set;}
    public Boolean isLoginFailed{get;set;}
    public String session_Id{get;set;}   
    public String sectionTitle{get;set;}
    public Checklist__c pgCheckList{get{return [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Trade_Ally_Login_Form' LIMIT 1];}}
    
    public Set<String> allowedProfileIdToLogin{get;set;}
    /* CONSTRUCTOR */
    public dsmtTradeallyLoginCtrl(){
        isLoginFailed = false;
        
       setCookies();
    }
    
    
    /* PageReference Action Methods */
    public PageReference login(){
        if(username != null){
            username = username.trim();
        }
        try {
            List<User> ulist = [SELECT Id, IsActive,EULA_Agreement_Date__c, (SELECT Id,Name,Active__c,Portal_Role__c FROM DSMTracker_Contacts__r where Trade_ally_Account__c != null) FROM User WHERE username =:username ];       
            if (ulist.size() > 0) {
                    User u = ulist.get(0);
                    List<Dsmtracker_Contact__c> dsmtlist = u.Dsmtracker_Contacts__r;
                    if(dsmtlist.size() > 0){
                        system.debug('--dsmtlist--'+dsmtlist );
                        Dsmtracker_Contact__c dsmt = dsmtlist.get(0);
                        system.debug('--dsmt.Portal_Role__c --'+dsmt.Portal_Role__c );
                        if(dsmt.Active__c == false){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Your account has been disabled by manager. Please contact your manager.'));
                            return null;
                        }
                        if(dsmt.Portal_Role__c == null || dsmt.Portal_role__c == ''){
                           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Your account is not created so please contact your manager.'));
                            return null;
                        }
                    }
                    u.IsActive = true;
                    system.debug('--user--'+u);
                    update u;
                    
               
            } else {
                    /*PageReference pageRef = Page.dsmtTradeallyLogin;
                    pageRef.getParameters().put('fail','1');
                    pageRef.setRedirect(true);
                    return pageRef;*/
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Your login attempt has failed. Make sure the username and password are correct.'));
                    return null;
            } 
        } catch(Exception e) {
                   /* PageReference pageRef = Page.dsmtTradeallyLogin;
                    pageRef.getParameters().put('fail','1');
                    pageRef.setRedirect(true);
                    return pageRef;*/
                    system.debug('--e--'+e);
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Your login attempt has failed. Make sure the username and password are correct.'));
                    return null;
        }
        Online_Portal_URLs_Hierarchy__c credentials = Online_Portal_URLs_Hierarchy__c.getInstance();
        String reLoginPageUrl   = '/apex/dsmtReLogin?u='+EncodingUtil.urlEncode(username,'UTF-8')+'&p='+EncodingUtil.urlEncode(password,'UTF-8'); 
        PageReference reLoginPageRef = Site.login(credentials.system_adminu__c, credentials.system_adminp__c, reLoginPageUrl);
        
        if(reLoginPageRef == null){
            String reLoginPageUrl2   = '/apex/dsmtReLogin2?u='+EncodingUtil.urlEncode(username,'UTF-8'); 
            PageReference reLoginPageRef2 = Site.login(credentials.system_adminu__c, credentials.system_adminp__c, reLoginPageUrl2);
            return reLoginPageRef2;       
        }
        
        return reLoginPageRef;
    }
    
    public PageReference endlogin(){
      return null;
   }
        
    public PageReference relogin(){
        
        String startUrl = '/apex/dsmtTradeAllyRegistrationProcess';
        
        String usrname = ApexPages.currentPage().getParameters().get('u');    
        String pwd = ApexPages.currentPage().getParameters().get('p');   
        
        PageReference reLoginSuccessPageRef = Site.login(usrname, pwd, startUrl);
        
        if(reLoginSuccessPageRef == null){
            Online_Portal_URLs_Hierarchy__c credentials = Online_Portal_URLs_Hierarchy__c.getInstance();
            String reLoginPageUrl2 = '/apex/dsmtReLogin2?u='+EncodingUtil.urlEncode(usrname,'UTF-8'); 
            PageReference reLoginPageRef2 = Site.login(credentials.system_adminu__c, credentials.system_adminp__c, reLoginPageUrl2);
            return reLoginPageRef2;
        }
        return reLoginSuccessPageRef;
    }
    
    public PageReference relogin2(){
        String username = ApexPages.currentPage().getParameters().get('u');
        String fromcall = ApexPages.currentPage().getParameters().get('from');
        
        system.debug('--username--'+username);
        List<User> ulist =[Select Id,IsActive from User where username=:username ];       
        if(ulist.size() > 0){
          User u = ulist.get(0);
          u.IsActive = false;
          update u;
        }
        
        
        if(fromcall == null){
            PageReference pageRef = new PageReference('/apex/dsmtLogout2');
            pageRef.setRedirect(false);
            return pageRef;
        }else{
            PageReference pr = Page.dsmtTradeallyLogin;
            pr.setRedirect(true);
            return pr;
        }
    }
    public PageReference relogin3(){
       
       String usrname = ApexPages.currentPage().getParameters().get('u');
       User u =[Select Id,IsActive from User where username=:usrname and (UserType = 'PowerCustomerSuccess' OR UserType = 'CustomerSuccess') ];       
       u.IsActive = true;
       update u;
   
       PageReference pageRef = new PageReference('/apex/usernewlogout3?usr='+EncodingUtil.urlEncode(usrname,'UTF-8'));
       pageRef.setRedirect(false);
       return pageRef;
    }
    
    public PageReference redirectIfLoggedIn(){return null;}
    public pagereference endlogoutuser(){return null;}
    
    /* Void Action Methods */
    public void setCookies(){
       Cookie eaid = ApexPages.currentPage().getCookies().get('eaid');
       Cookie revId = ApexPages.currentPage().getCookies().get('revId');

       if(eaid == null){ 
           Cookie cokObj = new Cookie('eaid', ApexPages.currentPage().getParameters().get('eaid'), null, -1, true);
           ApexPages.currentPage().setCookies(new Cookie[]{cokObj});
       }else{
           eaid = new Cookie('eaid', ApexPages.currentPage().getParameters().get('eaid'), null, -1, true);
       }
       
       if(revId == null){ 
           Cookie cokObj = new Cookie('revId', ApexPages.currentPage().getParameters().get('revId'), null, -1, true);
           ApexPages.currentPage().setCookies(new Cookie[]{cokObj});
       }else{
           revId = new Cookie('revId', ApexPages.currentPage().getParameters().get('revId'), null, -1, true);
       }
       
       Cookie pagename = ApexPages.currentPage().getCookies().get('pagename');
       if(pagename == null){
         pagename = new Cookie('pagename', 'dsmtTradeallyLogin',null,-1,true);
       }else{
          pagename = new Cookie('pagename', 'dsmtTradeallyLogin', null, -1, true);
       } 
       ApexPages.currentPage().setCookies(new Cookie[]{pagename}); 
    }
    
     @RemoteAction
    public static boolean queryUser(String username)
    {
        String portalUsername = username;
        /*if(username != null){
            username = username.trim();
            Online_Portal_URLs_Hierarchy__c cs = Online_Portal_URLs_Hierarchy__c.getInstance(UserInfo.getProfileId());
            portalUsername = username + cs.Username_Postfix__c;
        }*/
        
        List<User> ulist =[Select Id,IsActive,EULA_Agreement_Date__c from User where username=:username];// AND ProfileId IN: allowedProfileIdToLogin];       
        if(ulist.size() > 0 && ulist[0].EULA_Agreement_Date__c != null){
            return true;
        }
        
        return false;
    }
    
    private void insertParameters(){}
    
    /* Other Methods */
    public list<Checklist_Items__c> getChecklists(){
        list<Checklist_Items__c> checklistItems = [
            SELECT id, Action_Required__c, Checklist_Information__c, Checklist__c,Parent_Checklist__c, Section__c, Sequence__c//, Status__c
            FROM Checklist_Items__c
            WHERE Parent_Checklist__r.Unique_Name__c = 'Trade_Ally_Login_Form' and Special_instruction_for_public_portal__c = true 
        ];
        
        if(checklistItems.size() > 0){
            this.sectionTitle = checklistItems.get(0).Section__c;
            return checklistItems;
        }
        
        return new list<Checklist_Items__c>();
    }
}