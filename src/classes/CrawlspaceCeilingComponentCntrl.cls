public with sharing class CrawlspaceCeilingComponentCntrl{
    public Id crawlspaceCeilingId{get;set;}
    public String newLayerType{get;set;}
    public Ceiling__c crawlspaceCeiling{get{
        if(crawlspaceCeiling == null){
            List<Ceiling__c> ceilings = Database.query('SELECT ' + getSObjectFields('Ceiling__c') + ',Thermal_Envelope_Type__r.SpaceConditioning__c FROM Ceiling__c WHERE Id=:crawlspaceCeilingId'); 
            if(ceilings.size() > 0){
                crawlspaceCeiling = ceilings.get(0);
            }
        }
        return crawlspaceCeiling;
    }set;}
    public void saveCeiling(){
        saveCeilingLayers();
        if(crawlspaceCeiling != null && crawlspaceCeiling.Id != null){
            crawlspaceCeiling.Exposed_To__c = crawlspaceCeiling.Roof_To__c;
            UPDATE crawlspaceCeiling;
            crawlspaceCeiling = null;
        }
    }
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    public List<Layer__c> ceilingLayers{get;set;}
    public Map<Id,Layer__c> getCeilingLayersMap(){
        Id ceilingId = crawlspaceCeiling.Id;
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Ceiling__c =:ceilingId AND HideLayer__c = false';
        ceilingLayers = Database.query(query);
        return new Map<Id,Layer__c>(ceilingLayers);
    }
    public void saveCeilingLayers(){
        if(ceilingLayers != null && ceilingLayers.size() > 0){
            UPSERT ceilingLayers;
        }
    }
    public void addNewCeilingLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Ceiling__c =:crawlspaceCeiling.Id AND RecordTypeId =:recordTypeId];
        if(layerType != null && m.containsKey(layerType)){
            Decimal nextNumber= layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Ceiling__c = crawlspaceCeiling.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            if(newLayer.Id != null){
                crawlspaceCeiling.Next_Crawlspace_Ceiling_Layer_Number__c = nextNumber + 1;
                UPDATE crawlspaceCeiling;
            }
            ceilingLayers.add(newLayer);
        }
    }
    public void deleteCeilingLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = ceilingLayers.get(index).Id);
        ceilingLayers.remove(index);
        saveCeiling();
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}