@istest
public class BasementWallComponentCntrlTest
{
	@istest
    static void runTest()
    {
        Wall__c wall =new Wall__c();
        insert wall;
        Layer__c ly =new Layer__c();
        insert ly;
        
		BasementWallComponentCntrl bwcc = new BasementWallComponentCntrl ();
		String newLayer = bwcc.newLayerType;
		bwcc.basementWallId =wall.Id;
        bwcc.getLayerTypeOptions();
        bwcc.getWallLayersMap();
        bwcc.saveWall();
        
        ApexPages.currentPage().getParameters().put('layerType','Flooring');
		bwcc.addNewBasementWallLayer();
        
        ApexPages.currentPage().getParameters().put('index','0');
		bwcc.deleteWallLayer();        
    }
}