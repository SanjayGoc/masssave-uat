@isTest
private class dsmtEAModelTest 
{	
	public static dsmtEAModel.Surface bp = new dsmtEAModel.Surface();
	public static Energy_Assessment__c gea = null;

	@testsetup static void SetupEnergyAssessment()
    {
    	dsmtCreateEAssessRevisionController.stopTrigger = true;
       	Energy_Assessment__c ea = dsmtEAModel.setupAssessment(); 
        Air_Flow_and_Air_Leakage__c ai = new Air_Flow_and_Air_Leakage__c();
        ai.Energy_Assessment__c = ea.Id;
        insert ai;
    }

    @isTest
    static void test1()
    {
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		Test.startTest();

		bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		bp.bs.Actual_Floor_Area__c = 2200;
		bp.bs.Floor_Area__c = 2200;
		update bp.bs;

		BuildingSpecificationTriggerHandler.calculateAdd(23,123);
		BuildingSpecificationTriggerHandler.calculateSub(23,123);
		BuildingSpecificationTriggerHandler.calculateAdd(null,null);
		BuildingSpecificationTriggerHandler.calculateSub(null,null);
		BuildingSpecificationTriggerHandler.calculateBAScf50('Ceiling', 'Conditioned', 'Wall', 34,22);
		BuildingSpecificationTriggerHandler.calculateBAScf50('Ceiling', 'Unconditioned', 'Wall', 34,22);
		BuildingSpecificationTriggerHandler.calculateBAScf50('Wall', 'Unconditioned', 'Wall', 34,22);
		BuildingSpecificationTriggerHandler.calculateBAScf50(null, 'Unconditioned', 'Wall', 34,22);
		BuildingSpecificationTriggerHandler.calculateBAScf50(null, 'Unconditioned', 'Floor', 34,22);
		BuildingSpecificationTriggerHandler.calculateBAScf50('Ceiling', 'Vented', 'Wall', 34,22);
		BuildingSpecificationTriggerHandler.calculateBAScf50('Ceiling', 'Col', 'Wall', 34,22);

		Test.stopTest();
    }

    @isTest
    static void test2()
    {
    	string soql = 'SELECT ' + dsmtHelperClass.sObjectFields('Building_Specification__c') + ' Id FROM Building_Specification__c Limit 1';
		List<Building_Specification__c> tList = database.query(soql);

		Test.startTest();

		Map<Id,Building_Specification__c> olMap = new Map<Id,Building_Specification__c>();
		olMap.put(tList[0].Id,tList[0].clone(true,true,true,true));

		tList[0].Year_Built__c = '1970';
		BuildingSpecificationTriggerHandler.UpdateAllApplianceOnBuildingYearChange(tList,olMap);

		Test.stopTest();
    }

    @isTest
    static void test3()
    {
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:ea.Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = ea.Id;
            insert te;        
        }

		Test.startTest();

		Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tet.RecordTypeid = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Basement').getRecordTypeId();
		insert tet;

		bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		//update bp.floorList;
		//delete bp.floorList;

		Test.stopTest();
    }

    @isTest
    static void test4()
    {
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		Test.startTest();

		bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		update bp.teList;
		delete bp.teList;

		Test.stopTest();
    }

    @isTest
    static void test5()
    {
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		List<Thermal_Envelope__c> teTypeList = [Select Id From Thermal_Envelope__c Where Energy_Assessment__c =:ea.Id Limit 1];
        Thermal_Envelope__c te = new Thermal_Envelope__c();

        if(teTypeList !=null && teTypeList.size() > 0)
        {
            te = teTypeList[0]; 
        }
        else 
        {
            te = new Thermal_Envelope__c();
            te.Energy_Assessment__c = ea.Id;
            insert te;        
        }

		Test.startTest();

		Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
		tet.Energy_Assessment__c = ea.Id;
		tet.Thermal_Envelope__c = te.Id;
		tet.RecordTypeid = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Attic').getRecordTypeId();
		insert tet;

		bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		//update bp.wallList;
		//delete bp.wallList;

		Test.stopTest();
    }

     @isTest
    static void test6()
    {
    	string soql = 'SELECT ' + dsmtHelperClass.sObjectFields('Energy_Assessment__c') + ' Id FROM Energy_Assessment__c Limit 1';
    	List<Energy_Assessment__c> eaList = database.query(soql);

		Energy_Assessment__c ea = eaList[0];		
		
		Test.startTest();

		update ea;

		Test.stopTest();
    }
	

	@isTest
	static void InitializeBuildingProfileTest()
	{	
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		Test.startTest();

		bp = dsmtEAModel.InitializeBuildingProfile(eaId);
		bp = dsmtEAModel.CloneSurfaceModel(bp);

		bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
		bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

		bp.avs = null;
		bp.av = null;
		bp.atticAccess =null;
		bp.ds = null;
        bp.thermostat = null;
        bp.cs = null;
        bp.hs = null;
        bp.rcmd = null;
        bp.product = null;
        bp.surfacePart = null;
        bp.bdrObj =null;
        bp.ProductPartData = null;
        bp.constantMap =null;
        bp.recmFuelSavingMap =null;
        bp.recommendationMap =null;
        bp.mechanicalTypeMap =null;
        bp.dsmProductPartMap =null;
        bp.PressurePanReadingMap =null;
        bp.rcmdList = null;
        bp.prodList = null;
        bp.surfacePartList =null;
        bp.AnnualOperatingCost =0;
        bp.Months = '';

        bp.temp.HandDishwashingIndoorGainC = 0;
        bp.temp.HandDishwashingIndoorGainCLat = 0;
        bp.temp.HandDishwashingIndoorGainH =0;
        bp.temp.TempH =0;
        bp.temp.TempC =0;
        bp.temp.TRoomH=0;
        bp.temp.TRoomC=0;
        

		Test.stopTest();
	}

	@isTest 
	static void UpdateThermostateModelTest()
	{
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];

		Energy_Assessment__c ea = eaList[0];

		Mechanical__c mech = new Mechanical__c();
		mech.Energy_Assessment__c = ea.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = ea.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);
		

		Test.startTest();

		insert mstList;

		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		dsmtEAModel.UpdateThermostateModel(bp);	

		Test.stopTest();
	}

	@isTest
	static void SetScaleTest()
	{
		dsmtEAModel.SetScale(123.02485);
	}

	@isTest
	static void ISNULLTest()
	{
		dsmtEAModel.ISNULL(457);
		dsmtEAModel.ISNULL(457,2);
	}
}