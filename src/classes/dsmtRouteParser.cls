public class dsmtRouteParser{

    public AddressParser addObj;
    
    public class Result{
        public string error;
        public string success;
        public double totalCost;
        public string totalRouteTime;
        public string totalDrivingTime;
        public string TotalserviceTime;
        public string idleTime;
        public string breakTime;
        public string totalDistance;
        public List<optimizeRoute> optimizeRoutes;
        
    }
    
    public class optimizeRoute{
        public double totalCost;
        public String totalRouteTime;
        public String totalDrivingTime;
        public String TotalserviceTime;
        public String idleTime;
        public String breakTime;
        public String totalDistance;
        public String workTeamId;
        public List<appointmentDetail> appointmentDetails;
    }
    
    public class appointmentDetail{
        
        public string workOrderId;
        public string workTeamId;
        public date serviceDate;
        public string serviceTime;
        public string serviceDuration;
        public string serviceTimeSlot;
        public string drivingTime;
        public datetime serviceDateTime;
        public string sequence;
        public string violation;
        
    }
    /*
    {"status":"OK","formatedAddress":"Bedla, Udaipur, Rajasthan, India","city":"Udaipur","state_long":"Rajasthan","state_short":"RJ",
    "country_long":"India","country_short":"IN","postalCode":"","address":"Bedla",
    "county":"Udaipur","latitude":"24.6469436","longitude":"73.6948234","googlePlaceId":"","addressSQLId":null,"orignalAddressText":null}*/
    
    public class AddressParser{
        public string status;
        public string formatedAddress;
        public string address;
        public string city;
        public string state_short;
        public string postalCode;
        public decimal latitude;
        public decimal longitude;
    }
    
    public class DistanceParser{
        public string driveTimeText;
        public string driveTimeSeconds;
        public string distanceText;
        public string distanceInMeter;
        public string status;
        public string originAddress;
        public string destinationAddress;
        public string originGeoLocation;
        public string destinationGeoLocation;
    }
}