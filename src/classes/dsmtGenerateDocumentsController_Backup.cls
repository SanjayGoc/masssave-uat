public class dsmtGenerateDocumentsController_Backup {
    
    public List<DocumentName> documentList{get;set;}
    public Boolean isShow{get;set;}
    public String eaId{get;set;}
    public string selectedDocuments {get;set;}
    public String templateID{get;set;}
    public String queryID{get;set;}
    public String newCustomAttachmentId{get;set;}
    public String[] documents{get;set;}
    public Integer counter{get;set;}
    public String docName{get;set;}
    public String attachments{get;set;}
    public String query{get;set;}
    Public String stdAttachmentId{get;set;}
    Public Integer innerCounter{get;set;}
    Public Map<String, List<mechanical_sub_type__c>> mechsubtypeMap = new Map<String, List<mechanical_sub_type__c>>();
    Public Map<String, List<Appliance__c>> applianceMap = new Map<String, List<Appliance__c>>();
    Public Integer totalSubTypes{get;set;}
    Public boolean clearMap{get;set;}
    Public Boolean isHPC{get;set;}
    Public String OFN{get;set;}
    public String[] invalidDocs{get;set;}
    public Boolean isSignatureRequired{get;set;}
    public String selectedDocName{get;set;}
    
    public dsmtGenerateDocumentsController_Backup () {
        innerCounter = 0;
        totalSubTypes = 0;
        documentList = new List<DocumentName>();
        eaId = apexpages.currentpage().getparameters().get('Id');
        
        Schema.DescribeFieldResult fieldResult =
        Energy_Assessment__c.Document_List__c.getDescribe();

        List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();
        List<String> docsToExcludeForHPC = new List<String>();
        docsToExcludeForHPC.add('IC Rated Recessed Rated Lights Sign Off');
        docsToExcludeForHPC.add('Customer Receipt SHV Form');
        docsToExcludeForHPC.add('Permit Authorization Form');
        docsToExcludeForHPC.add('Specified Measures Agreement');
        docsToExcludeForHPC.add('Enclosed Cavity Sheet');
        docsToExcludeForHPC.add('Master Disclosure Form');
        
        list<User> userList = [select id, Profile.Name from User where Id =: UserInfo.getUserId()];
        String profileName = userList[0].Profile.name;
        isHPC = profileName.contains('HPC');
        for( Schema.PicklistEntry ple : pickListEntries) {
            DocumentName document = new DocumentName();
            
            String label = ple.getLabel();
            if(isHPC && docsToExcludeForHPC.contains(label)) {
                continue;
            }

            document.name = label;
            documentList.add(document);
        }

        isShow = true;
        if(documentList.size()>0){
            isShow = true;
        }else{
            isShow = false;
        }
        attachments='';
        isSignatureRequired =  false;
        selectedDocName = '';
    }
    
     public void saveOutboundConcate() {
        if(selectedDocuments != null && selectedDocuments.length() > 0 ) {
           selectedDocuments = selectedDocuments.substring(0, selectedDocuments.length() - 1);
           documents = selectedDocuments.split(':');
        }
    }
    
    public void validateSubtypes() {
        List<String> docsToValidate = new List<String>();
        docsToValidate.add('Clothes Washer Rebate Form');
        docsToValidate.add('Early CAC and CHP Rebate Form');
        docsToValidate.add('Early Boiler Replacement');
        docsToValidate.add('Early Furnace Replacement Rebate Form');
        
        invalidDocs = new String[]{};
        if(documents!=null && documents.size() > 0) {
            for(String doc: documents) {
                if(docsToValidate.contains(doc)) {
                    if(doc == 'Clothes Washer Rebate Form') {
                        List<Appliance__c> appliances = [Select Id, Name, Make__c, Model__c, Fuel_Type__c FROM Appliance__c
                                                            where energy_assessment__c = : eaId
                                                            and RecordType.name ='Laundry Washer' 
                                                            and Send_for_Early_Rebate_Eligibility__c = True];

                        if(appliances != null && appliances.size() == 0) {
                                 invalidDocs.add('Clothes Washer Rebate Form');                
                        }                                     
                    }

                    
                    if(doc == 'Early CAC and CHP Rebate Form') {
                        List<mechanical_sub_type__c> mechsubtypes = [SELECT Make__c, Model__c, Cooling_Tons__c,Heating_Tons__c, Tons__c,Year__c, Energy_Assessment__r.Appointment_Date__c, Energy_Assessment__r.Customer__c, Energy_Assessment__r.Energy_Advisor__c 
                                                                        FROM Mechanical_Sub_Type__c 
                                                                        WHERE Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId 
                                                                        AND RecordType.Name In ('ASHP','Central A/C') 
                                                                        AND Send_for_Early_Rebate_Eligibility__c = true];

                        if(mechsubtypes != null && mechsubtypes.size() == 0) {
                            invalidDocs.add('Early CAC and CHP Rebate Form');                  
                        }
                    }
                    
                    if(doc == 'Early Boiler Replacement') {
                        List<mechanical_sub_type__c> mechsubtypes = [select id, Mechanical_Type__r.Name, Fuel_Type__c, Send_for_Early_Rebate_Eligibility__c 
                                                            from mechanical_sub_type__c 
                                                            where Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId
                                                            and RecordType.name like '%Boiler%' 
                                                            and Send_for_Early_Rebate_Eligibility__c = True];

                        if(mechsubtypes != null && mechsubtypes.size() == 0) {
                            invalidDocs.add('Early Boiler Replacement');
                        }
                    }
                    
                    if(doc == 'Early Furnace Replacement Rebate Form') {
                        //DSST-10559 START
                        List<mechanical_sub_type__c> mechsubtypes = [select id, Mechanical_Type__r.Name, Fuel_Type__c, Send_for_Early_Rebate_Eligibility__c 
                                                            from mechanical_sub_type__c 
                                                            where Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId
                                                            and Mechanical_Type__r.RecordType.name like '%Furnace%' 
                                                            and RecordType.name like '%Furnace%'
                                                            and Send_for_Early_Rebate_Eligibility__c = True];
                        //DSST-10559 END
                        if(mechsubtypes != null && mechsubtypes.size() == 0) {
                            invalidDocs.add('Early Furnace Replacement Rebate Form');                   
                        }
                    }
                }
            }
            
            for(String doc: invalidDocs) {
                documents.remove(documents.indexOf(doc));
            }
        }
    }
    
    public string getInvalidDocsJson() {
        return JSON.serialize(invalidDocs);
    }
    
    public string getDocsJson() {
        return JSON.serialize(documents);
    }
    
    public void generateCongaDocument() {
        System.debug('documents === '+documents +' '+ counter);
        String documentName = documents[counter];
        selectedDocName = documentName;
        String subtypeid = '';
        if(documentName == 'Enclosed Cavity Sheet') {
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Enclosed Cavity Insulation Template'];
            templateID = templates.get(0).ID;
            
            //get Conga Query                 
            List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Enclosed Cavity Insulation Query']; 
            queryID = queries.get(0).ID;
            query = queryID+'?pv0='+eaId;
            totalSubTypes = 0;
            isSignatureRequired =  true;
        } else if(documentName == 'Master Disclosure Form') {
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Master Disclosure Form'];
            templateID = templates.get(0).ID;
            
            //get Conga Query                 
            List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Master Disclosure Query']; 
            queryID = queries.get(0).ID;
            query = queryID+'?pv0='+eaId;
            totalSubTypes = 0;
            isSignatureRequired =  true;
        } else if(documentName == 'Clothes Washer Rebate Form') {
            List<Energy_Assessment__c> energyAssessments = [SELECT id, name, (Select Id, Name, Has_Agitator__c, Is_Equipment_Functional__c, RecordType.name FROM Appliances__r) FROM Energy_Assessment__c WHERE Id=: eaId ];

            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Clothes Washer Rebate Form Template'];
            templateID = templates.get(0).ID;

            //get Conga Query    
            if(innerCounter == 0)
            {
                List<Appliance__c> appliances = [Select Id, Name, Make__c, Model__c, Fuel_Type__c FROM Appliance__c
                                                            where energy_assessment__c = : eaId
                                                            and RecordType.name ='Laundry Washer' 
                                                            and Send_for_Early_Rebate_Eligibility__c = True];
                if(appliances != null && appliances.size() > 0)
                {
                    if(applianceMap.containsKey(eaId)==false)
                    {
                        totalSubTypes = appliances.size();
                        applianceMap.put(eaId, appliances);
                    }                        
                }                                            
            }      
            List<sObject> queries = [select Id, Unique_Conga_Query_Name__c from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Clothes Washer Rebate Form Query', 'Clothes Washer Rebate Form Query 2', 'Clothes Washer Rebate Form Query 3', 'Clothes Washer Rebate Form Query 4', 'Clothes Washer Rebate Form Query 5', 'Clothes Washer Rebate Form Query 6') ORDER BY Unique_Conga_Query_Name__c ASC]; 
            
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries)
            {                
                //system.debug('applianceMap.containsKey(eaId)--'+applianceMap.containsKey(eaId));
                if(applianceMap.containsKey(eaId) == true)
                {
                    List<Appliance__c> appliances = (List<Appliance__c>)applianceMap.get(eaId);
                    subtypeid = (appliances[innerCounter]).id;
                }
                
                if(q.Unique_Conga_Query_Name__c == 'Clothes Washer Rebate Form Query 6') {
                    queryID += q.ID +'?pv0='+ subtypeid + ',';
                } else {
                    queryID += q.ID +'?pv0='+ eaId + ',';
                }
            }
            query = queryID.removeEnd(',');
            if(clearMap) {
                applianceMap.clear();
                clearMap = false;
            }
        } else if(documentName == 'Early Refrigerator Rebate Form') {
            List<Energy_Assessment__c> energyAssessments = [SELECT id, name, (Select Id, Name, Has_Agitator__c, Is_Equipment_Functional__c, RecordType.name FROM Appliances__r) FROM Energy_Assessment__c WHERE Id=: eaId ];

            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Early Refrigerator Rebate Form Template'];
            templateID = templates.get(0).ID;

            //get Conga Query      
            List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Early Refrigerator Rebate Form Query']; 
            
            query = '';
            for(sObject queryObj: queries) {
                queryID = queryObj.ID;
                query += queryID+'?pv0='+eaId+',';
            }
            query = query.subString(0, query.length() -1);
            totalSubTypes = 0;
        } else if(documentName == 'Audit Receipt Form') {
            List<Energy_Assessment__c> energyAssessments = [SELECT id, name, (Select Id, Name, Has_Agitator__c, Is_Equipment_Functional__c, RecordType.name FROM Appliances__r) FROM Energy_Assessment__c WHERE Id=: eaId ];

            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Audit Receipt Template'];
            templateID = templates.get(0).ID;
            
            //get Conga Query      
            List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Audit Receipt SHV Query', 'Audit Receipt SHV Query 2', 'Audit Receipt SHV Query 3', 'Audit Receipt SHV Query 4', 'Audit Receipt SHV Query 5', 'Audit Receipt SHV Query 6', 'Audit Receipt SHV Query 7') ORDER BY Unique_Conga_Query_Name__c ASC]; 
            
            query = '';
            for(sObject queryObj: queries) {
                queryID = queryObj.ID;
                query += queryID+'?pv0='+eaId+',';
            }
            query = query.subString(0, query.length() -1);
            totalSubTypes = 0;
        } else if(documentName == 'IC Rated Recessed Rated lights Sign off') {
            List<Energy_Assessment__c> energyAssessments = [SELECT id, name, Trade_Ally_Account__r.Internal_Account__c, (Select Id, Name, Has_Agitator__c, Is_Equipment_Functional__c, RecordType.name FROM Appliances__r) FROM Energy_Assessment__c WHERE Id=: eaId ];
            Energy_Assessment__c ea = energyAssessments.get(0);
            
            Set<String> templateNames = new Set<String>();
            if(ea.Trade_Ally_Account__r.Internal_Account__c) {
                templateNames.add('1216-MS-Statewide-607806-IC Rated Recessed Lights Form-CR-R3b');
            } else {
                templateNames.add('1216-MS-Statewide-607806-IC Rated Recessed Lights Form-HPC-R3b');
            }
            
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c In :templateNames];
            templateID = templates.get(0).ID;
            
            //get Conga Query      
            List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('IC Recessed Lights Form (CR Or HPC) 1')]; 
            
            query = '';
            for(sObject queryObj: queries) {
                queryID = queryObj.ID;
                query += queryID+'?pv0='+eaId+',';
            }
            query = query.subString(0, query.length() -1);
            totalSubTypes = 0;
        } else if(documentName == 'Early CAC and CHP Rebate Form') {
            
            
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Early CAC and CHP Rebate Form Template'];
            templateID = templates.get(0).ID;
            
            //get Conga Query
            if(innerCounter == 0)
            {
                List<mechanical_sub_type__c> mechsubtypes = [SELECT Make__c, Model__c, Cooling_Tons__c,Heating_Tons__c, Tons__c,Year__c, Energy_Assessment__r.Appointment_Date__c, Energy_Assessment__r.Customer__c, Energy_Assessment__r.Energy_Advisor__c 
                                                                FROM Mechanical_Sub_Type__c 
                                                                WHERE Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId 
                                                                AND RecordType.Name In ('ASHP','Central A/C') 
                                                                AND Send_for_Early_Rebate_Eligibility__c = true];
                if(mechsubtypes != null && mechsubtypes.size() > 0)
                {
                    if(mechsubtypeMap.containsKey(eaId)==false)
                    {
                        totalSubTypes = mechsubtypes.size();
                        mechsubtypeMap.put(eaId, mechsubtypes);
                    }
                }                  
            }
            List<sObject> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Early CAC and CHP Rebate Form Query','Early CAC and CHP Rebate Form Query 2') ORDER BY Unique_Conga_Query_Name__c DESC]; 
            
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries)
            {                
                //system.debug('mechsubtypeMap.containsKey(eaId)--'+mechsubtypeMap.containsKey(eaId));
                if(mechsubtypeMap.containsKey(eaId) == true)
                {
                    List<mechanical_sub_type__c> mechsubTypes = (List<mechanical_sub_type__c>)mechsubtypeMap.get(eaId);
                    subtypeid = (mechsubTypes[innerCounter]).id;
                }
                
                if(q.Unique_Conga_Query_Name__c == 'Early CAC and CHP Rebate Form Query 2') {
                    queryID += q.ID +'?pv0='+ subtypeid + ',';
                } else {
                    queryID += q.ID +'?pv0='+ eaId + ',';
                }
            }
            
            query = queryID.removeEnd(',');
            if(clearMap) {
                mechsubtypeMap.clear();
                clearMap = false;
            }
            isSignatureRequired =  true;
            /**queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries){
               queryID += q.ID +'?pv0='+ eaId +',';
            }
            
            query = queryID.removeEnd(',');*/
            
        } else if(documentName == 'Permit Authorization Form') {

            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Permit Authorization Customer Signoff Template'];
            templateID = templates.get(0).ID;
            
            //get Conga Query                 
            List<sObject> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Permit Authorization Customer Signoff Query')]; 
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries){
               queryID += q.ID +'?pv0='+ eaId +',';
            }
            
            query = queryID.removeEnd(',');
            totalSubTypes = 0;
            isSignatureRequired =  true;
        } else if(documentName == 'Early Boiler Replacement') {
                
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Early Boiler Replacement Rebate Form Template'];
            templateID = templates.get(0).ID;
            
            //get Conga Query      
            if(innerCounter == 0)
            {
                List<mechanical_sub_type__c> mechsubtypes = [select id, Mechanical_Type__r.Name, Fuel_Type__c, Send_for_Early_Rebate_Eligibility__c 
                                                            from mechanical_sub_type__c 
                                                            where Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId
                                                            and Mechanical_Type__r.RecordType.name like '%Boiler%' 
                                                            and Send_for_Early_Rebate_Eligibility__c = True];
                if(mechsubtypes != null && mechsubtypes.size() > 0)
                {
                    if(mechsubtypeMap.containsKey(eaId)==false)
                    {
                        totalSubTypes = mechsubtypes.size();
                        mechsubtypeMap.put(eaId, mechsubtypes);
                    }                        
                }                                            
            }
            system.debug('mechsubtypeMap--'+mechsubtypeMap);
            //get Conga Query      
            List<sObject> queries = [select Id, Unique_Conga_Query_Name__c from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Early Boiler Replacement Rebate Form Query','Early Boiler Replacement Rebate Form Query 2', 'Early Boiler Replacement Rebate Form Query 3')]; 
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries)
            {                
                //system.debug('mechsubtypeMap.containsKey(eaId)--'+mechsubtypeMap.containsKey(eaId));
                if(mechsubtypeMap.containsKey(eaId) == true)
                {
                    List<mechanical_sub_type__c> mechsubTypes = (List<mechanical_sub_type__c>)mechsubtypeMap.get(eaId);
                    subtypeid = (mechsubTypes[innerCounter]).id;
                    
                }
                
                if(q.Unique_Conga_Query_Name__c == 'Early Boiler Replacement Rebate Form Query' || q.Unique_Conga_Query_Name__c == 'Early Boiler Replacement Rebate Form Query 3') {
                    queryID += q.ID +'?pv0='+ subtypeid + ',';
                } else {
                    queryID += q.ID +'?pv0='+ eaId + ',';
                }
            }

            query = queryID.removeEnd(',');
            if(clearMap) {
                mechsubtypeMap.clear();
                clearMap = false;
            }
        } else if(documentName == 'Early Furnace Replacement Rebate Form') {

            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Early Furnace Replacement Rebate Form'];
            templateID = templates.get(0).ID;
            system.debug('innerCounter--'+innerCounter);
            if(innerCounter == 0)
            {
                //DSST-10559 START
                List<mechanical_sub_type__c> mechsubtypes = [select id, Mechanical_Type__r.Name, Fuel_Type__c, Send_for_Early_Rebate_Eligibility__c 
                                                            from mechanical_sub_type__c 
                                                            where Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId
                                                            and Mechanical_Type__r.RecordType.name like '%Furnace%' 
                                                            and RecordType.name like '%Furnace%'
                                                            and Send_for_Early_Rebate_Eligibility__c = True];
                //DSST-10559 END
                if(mechsubtypes != null && mechsubtypes.size() > 0)
                {
                    if(mechsubtypeMap.containsKey(eaId)==false)
                    {
                        totalSubTypes = mechsubtypes.size();
                        mechsubtypeMap.put(eaId, mechsubtypes);
                    }                        
                }                                            
            }
            system.debug('mechsubtypeMap--'+mechsubtypeMap);
            //get Conga Query      
            List<sObject> queries = [select Id, Unique_Conga_Query_Name__c from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Early Furnace Replacement Rebate Form Query','Early Furnace Replacement Rebate Form Query 2', 'Early Furnace Replacement Rebate Form Query 3')]; 
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries)
            {                
                //system.debug('mechsubtypeMap.containsKey(eaId)--'+mechsubtypeMap.containsKey(eaId));
                if(mechsubtypeMap.containsKey(eaId) == true)
                {
                    List<mechanical_sub_type__c> mechsubTypes = (List<mechanical_sub_type__c>)mechsubtypeMap.get(eaId);
                    subtypeid = (mechsubTypes[innerCounter]).id;
                    
                }
                
                if(q.Unique_Conga_Query_Name__c == 'Early Furnace Replacement Rebate Form Query 2' || q.Unique_Conga_Query_Name__c == 'Early Furnace Replacement Rebate Form Query 3') {
                    queryID += q.ID +'?pv0='+ subtypeid + ',';
                } else {
                    queryID += q.ID +'?pv0='+ eaId + ',';
                }
            }
            
            query = queryID.removeEnd(',');
            if(clearMap) {
                mechsubtypeMap.clear();
                clearMap = false;
            }
        } else if(documentName == 'Customer Receipt SHV Form') {
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Customer Receipt SHV Form'];
            templateID = templates.get(0).ID;
            
            //get Conga Query      
            List<sObject> queries = [select Id, Unique_Conga_Query_Name__c from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Customer Receipt SHV Query','Customer Receipt SHV Query part 2','Customer Receipt SHV Query part 3','Customer Receipt SHV Query part 4', 'Customer Receipt SHV Query part 5', 'Customer Receipt SHV Query part 6') ORDER BY Unique_Conga_Query_Name__c ASC]; 
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries)
            {
                queryID += q.ID +'?pv0='+ eaId+ ',';
            }
            query = queryID.removeEnd(',');
            totalSubTypes = 0;
        } else if(documentName == 'Specified Measures Agreement') {
            
            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Specified Measures Agreement Form Template'];
            templateID = templates.get(0).ID;

            //get Conga Query                 
            List<sObject> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Specified Measures Agreement Form Query')]; 
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries){
               queryID += q.ID +'?pv0='+ eaId +',';
            }
            
            query = queryID.removeEnd(',');
            totalSubTypes = 0;
            isSignatureRequired =  true;
        } else if(documentName == 'HEAT Loan Intake Form') {
            
            List<String> querynames = new List<string>();
            querynames.add('HEAT_Loan_Intake_Query');
            querynames.add('HEAT_Loan_Intake_Query1');
            querynames.add('HEAT_Loan_Intake_Query2');
            querynames.add('HEAT_Loan_Intake_Query3');
            querynames.add('HEAT_Loan_Intake_Query4');
            querynames.add('HEAT_Loan_Intake_Query5');
            querynames.add('HEAT_Loan_Intake_Query6');
            querynames.add('HEAT_Loan_Intake_Query7');
            querynames.add('HEAT_Loan_Intake_Query8');
            querynames.add('HEAT_Loan_Intake_Query9');
            querynames.add('HEAT_Loan_Intake_Query91');
            querynames.add('HEAT_Loan_Intake_Query92');
            //DSST-8147 Start
            querynames.add('HEAT_Loan_Intake_Query93');
            querynames.add('HEAT_Loan_Intake_Query94');
            querynames.add('HEAT_Loan_Intake_Query95');
            querynames.add('HEAT_Loan_Intake_Query96');
            //DSST-8147 End

            //get Conga Template
            List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Eversource HEAT Loan Intake Form Template'];
            templateID = templates.get(0).ID;
            
            //get Conga Query                 
            List<sObject> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In: querynames ORDER BY Unique_Conga_Query_Name__c ASC]; 
            queryID = '';
            for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries){
               queryID += q.ID +'?pv0='+ eaId +',';
            }
            
            query = queryID.removeEnd(',');
            totalSubTypes = 0;
        }

        OFN = documentName;
        system.debug('query --' + query); 
        Attachment__c newCustomAttachment = new Attachment__c();
        newCustomAttachment.Status__c = 'New';
        newCustomAttachment.Attachment_Name__c = documentName +' - ' + eaId + ' - ' + subtypeid +'.pdf';
        newCustomAttachment.Energy_Assessment__c = eaId ;

        insert newCustomAttachment;
        newCustomAttachmentId = newCustomAttachment.ID;
        attachments += newCustomAttachmentId+',';
        
        System.debug('templateID === '+ templateID + ' & '+query + ' & '+newCustomAttachmentId);
    }   
    
    public boolean enablePollar{get;set;}
    public Id congaAttachmentId {get;set;}
    public boolean pollarSuccess{get;set;}
    public void checkCongaStatus(){
        try{
            pollarSuccess = false;
            enablePollar = true;
            Attachment__c customAttachment = [SELECT Id,Name, (SELECT Id,Name FROM Attachments) FROM Attachment__c WHERE Id =: newCustomAttachmentId];
            if(customAttachment != null && customAttachment.Attachments.size() > 0) {
                pollarSuccess = true;
                enablePollar = false;
                
                DocumentServiceWSFile.clsSalesForceLoginDetail loginDetail = new DocumentServiceWSFile.clsSalesForceLoginDetail();
                loginDetail.SessionID = UserInfo.getSessionId();
                loginDetail.serverURL = Login_Detail__c.getInstance().Server_URL__c;
                loginDetail.orgID = Login_Detail__c.getInstance().Org_Id__c;
                loginDetail.userName = Login_Detail__c.getInstance().Attachment_User_Name__c;
                loginDetail.password = Login_Detail__c.getInstance().Attachment_User_Password__c;
                loginDetail.securityToken = Login_Detail__c.getInstance().Attachment_User_Security_Token__c;
                
                DocumentServiceWSFile.clsQueryStringParameters clsQueryStringParametersObj = new DocumentServiceWSFile.clsQueryStringParameters();
                clsQueryStringParametersObj.SessionID = UserInfo.getSessionId();
                clsQueryStringParametersObj.ServerURL = Login_Detail__c.getInstance().Server_URL__c;
                clsQueryStringParametersObj.OrgId  = Login_Detail__c.getInstance().Org_Id__c;
                clsQueryStringParametersObj.AttachmentId = newCustomAttachmentId;
                clsQueryStringParametersObj.SecurityKey = 'intelcore2duo';
                clsQueryStringParametersObj.AccountKey = 'nokia710lumia';
                clsQueryStringParametersObj.Replace = 'YES';
                clsQueryStringParametersObj.RecordId = eaId;
                clsQueryStringParametersObj.AttachmentLookupCol = 'Energy_Assessment__c';
                clsQueryStringParametersObj.AttachmentType = documents[counter];

                stdAttachmentId = customAttachment.Attachments.get(0).Id;
                DocumentServiceWSFile.DocumentServiceWSFileSoap documentServiceWSFile = new DocumentServiceWSFile.DocumentServiceWSFileSoap();
                DocumentServiceWSFile.ArrayOfClsUploadProcessResult result = documentServiceWSFile.MigrateAttachment(clsQueryStringParametersObj, stdAttachmentId, 'attachment', loginDetail);
            }
        } catch(Exception ex) {
            enablePollar = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
    
    public PageReference cancel() {
        return new pagereference('/'+eaId);     
    }

    public class DocumentName {
        public String name{get;set;}
    }
}