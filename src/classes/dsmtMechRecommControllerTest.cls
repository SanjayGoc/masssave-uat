@isTest
public class dsmtMechRecommControllerTest {
    static testmethod void test(){
        
        
          dsmtMechRecommController cntrl = new dsmtMechRecommController();
          cntrl.parentRecordTypeName = 'Kerosene_Space_Heater';
          cntrl.recordTypeName = 'Kerosene_Space_Heater';
          cntrl.recordType = 'Kerosene Space Heater';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Mini_Split_A_C';
          cntrl.recordTypeName = 'Mini_Split_A_C';
          cntrl.recordType = 'Mini-Split A/C';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Pellet_Stove';
          cntrl.recordTypeName = 'Pellet_Stove';
          cntrl.recordType = 'Pellet Stove';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Evaporative_Cooler';
          cntrl.recordTypeName = 'Evaporative_Cooler';
          cntrl.recordType = 'Evaporative Cooler';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Gas_AC';
          cntrl.recordTypeName = 'Gas_AC';
          cntrl.recordType = 'Gas A/C';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Parlor_Heater';
          cntrl.recordTypeName = 'Parlor_Heater';
          cntrl.recordType = 'Parlor Heater';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Wall_Furnace';
          cntrl.recordTypeName = 'Wall_Furnace';
          cntrl.recordType = 'Wall Furnace';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Furnace';
          cntrl.recordTypeName = 'Furnace';
          cntrl.recordType = 'Furnace';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Boiler_Indirect_Storage_Tank';
          cntrl.recordTypeName = 'Boiler';
          cntrl.recordType = 'Boiler';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Boiler_Indirect_Storage_Tank';
          cntrl.recordTypeName = 'Indirect_Storage_Tank';
          cntrl.recordType = 'Boiler + Indirect Storage Tank';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Boiler_Tankless_Coil';
          cntrl.recordTypeName = 'Boiler';
          cntrl.recordType = 'Boiler';
          cntrl.getAssignDefaultValues(); 
          
          cntrl.parentRecordTypeName = 'Wood_Stove';
          cntrl.recordTypeName = 'Wood_Stove';
          cntrl.recordType = 'Wood Stove';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Storage_Tank';
          cntrl.recordTypeName = 'Storage_Tank';
          cntrl.recordType = 'Storage Tank';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Boiler_Tankless_Coil';
          cntrl.recordTypeName = 'Boiler';
          cntrl.recordType = 'Boiler + Tankless Coil';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'On_Demand';
          cntrl.recordTypeName = 'On_Demand';
          cntrl.recordType = 'On Demand';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Combo';
          cntrl.recordTypeName = 'Combo';
          cntrl.recordType = 'Combo';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Gas_A_C';
          cntrl.recordTypeName = 'Gas_A_C';
          cntrl.recordType = 'Gas A/C';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Furnace';
          cntrl.recordTypeName = 'Central_A_C';
          cntrl.recordType = 'Central A/C';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Central_A_C';
          cntrl.recordTypeName = 'Central_A_C';
          cntrl.recordType = 'Central A/C';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'ASHP';
          cntrl.recordTypeName = 'ASHP';
          cntrl.recordType = 'ASHP';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'DFHP';
          cntrl.recordTypeName = 'DFHP';
          cntrl.recordType = 'DFHP';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'DFGSHP';
          cntrl.recordTypeName = 'DFGSHP';
          cntrl.recordType = 'DFGSHP';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'GSHP';
          cntrl.recordTypeName = 'GSHP';
          cntrl.recordType = 'GSHP';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Gas_Heat_Pump';
          cntrl.recordTypeName = 'Gas_Heat_Pump';
          cntrl.recordType = 'Gas Heat Pump';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Gas_Pack';
          cntrl.recordTypeName = 'Gas_Pack';
          cntrl.recordType = 'Gas Pack';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Boiler';
          cntrl.recordTypeName = 'Boiler';
          cntrl.recordType = 'Boiler';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Steam_Boiler';
          cntrl.recordTypeName = 'Steam_Boiler';
          cntrl.recordType = 'Steam Boiler';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Furnace_with_Central_A_C';
          cntrl.recordTypeName = 'Furnace';
          cntrl.recordType = 'Furnace';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Furnace_with_Central_A_C';
          cntrl.recordTypeName = 'Central_A_C';
          cntrl.recordType = 'Central A/C';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Mini_Split_Heat_Pump';
          cntrl.recordTypeName = 'Mini_Split_Heat_Pump';
          cntrl.recordType = 'Mini-Split Heat Pump';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'DFGSHP';
          cntrl.recordTypeName = 'Furnace';
          cntrl.recordType = 'Furnace';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Boiler';
          cntrl.recordTypeName = 'Baseboard Distribution System';
          cntrl.recordType = 'Baseboard Distribution System';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Central_A_C';
          cntrl.recordTypeName = 'Duct_Distribution_System';
          cntrl.recordType = 'Duct Distribution System';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Combo';
          cntrl.recordTypeName = 'Duct_Distribution_System';
          cntrl.recordType = 'Duct Distribution System';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Heat_Pump';
          cntrl.recordTypeName = 'Heat_Pump';
          cntrl.recordType = 'Heat Pump';
          cntrl.getAssignDefaultValues();
          
          cntrl.parentRecordTypeName = 'Pellet Stove';
          cntrl.recordTypeName = 'Thermostat';
          cntrl.recordType = 'Thermostat';
          cntrl.getAssignDefaultValues();
    }
}