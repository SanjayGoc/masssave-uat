Public Class dsmtScheduleRequestCntrl{
  
    public String conId   {get;set;}
    
    public string RequestJSON{get;set;}
    public Dsmtracker_contact__c dsmtCon{get;set;}
    public Trade_Ally_Account__c ta{get;set;}
    public Set<Id> dsmtcIds{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
     
    public dsmtScheduleRequestCntrl(){
         isPortal = true;
        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
        List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
            if(lstUser.size()>0){
                if(lstUser[0].IsPortalEnabled){
                    headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                }
                else{
                    headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                    isPortal = false;
                }
            }
            
        dsmtcIds = new Set<Id>();
        loadContactAndTA();
     //   fetchScheduleRequests();
    }
    
    
    public void loadContactAndTA(){
        String usrid = UserInfo.getUserId();
        List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
        if(usrList != null && usrList.size() > 0){
            List<DSMTracker_Contact__c> dsmtList = null;
            if(usrList.get(0).ContactId != null){
                conId   = usrList.get(0).ContactId;
                dsmtList = [select id,Trade_Ally_Account__c,Portal_Role__c from DSMTracker_Contact__c where Contact__c =: conId limit 1];
                
            }else{
               dsmtList = [select id,Trade_Ally_Account__c,Portal_Role__c from DSMTracker_Contact__c where portal_user__c =: usrid AND Trade_Ally_Account__c != null limit 1];
            }
            
            if(dsmtList != null && dsmtList.size() > 0){
                dsmtCon = dsmtList.get(0);
                dsmtcIds.add(dsmtCon.id);
                
                List<Trade_Ally_Account__c> taList = [select id,Owner.Email from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                if(taList != null && taList.size() > 0){
                    ta = taList.get(0);
                    
                    if(dsmtCon.Portal_Role__c == 'Manager'){
                        List<Dsmtracker_Contact__c> dsmtconList = [select id,Super_User__c,Name,contact__c from DSMTracker_Contact__c where Trade_Ally_Account__c  =: ta.Id];
                        for(Dsmtracker_Contact__c dsmt : dsmtConList){
                           dsmtcIds.add(dsmt.id);
                        }
                    }
                }
            }
        }
    }
    
    
    
    public void fetchScheduleRequests(){

        Id RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
                
        RequestJSON = '';
        String queryString = 'Select Id,Name,Type__c,Sub_Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate  from Service_Request__c where RecordTypeId =: RecordTypeId and DSMTracker_Contact__c  In :dsmtcIds ';
        queryString += ' LIMIT 50000';
        
        list<Service_Request__c> listMsg = Database.query(queryString);
            
        list<MessageModal> listMessage = new List<MessageModal>();
        for(Service_Request__c msg : Database.query(queryString)){
        
                MessageModal mod = new MessageModal();
                mod.Id = msg.Id;
                mod.Name=msg.Name;
                mod.Type= msg.Type__c;
                mod.Subject = msg.Subject__c;
                mod.Status = msg.Status__c;
                mod.Priority = msg.Priority__c;
                mod.Description = msg.Description__c ;
                mod.subType = msg.Sub_Type__c;
                mod.CreatedDate = msg.CreatedDate.Format();
                    
                listMessage.add(mod);
            }
            system.debug('--listMessage---'+listMessage);
            RequestJSON = JSON.serialize(listMessage);
        
            system.debug('--listMessage---'+listMessage);
    }
    
    public class MessageModal{
        public string Id{get;set;}
        public String Name{get;set;}
        public string Subject{get;set;}
        public string Type{get;set;}
        public String subType{get;set;}
        public string Status{get;set;}
        public string Priority{get;set;}
        public string Description{get;set;}
        public String CreatedDate{get;set;}
    }
    
     
}