global with sharing class dsmtConsoleHomePageCntrl{
    
    global String conName {get;set;}
    global String conId   {get;set;}
    String accId;
    global Trade_Ally_Account__c ta{get;set;}
    global Integer UnreadMessage{get;set;}
    global Integer TotalMessage{get;set;}
    global String imgURL{get;set;}
    
    global Integer TotalEligible{get;set;}
    global Integer TotalIneligible{get;set;}
    global Integer TotalOpenTickets{get;set;}
    global Integer TotalMeages{get;set;}
    global Integer TotalNewMeages{get;set;} 
    
    public Integer TotalApprovedEmployee{get;set;}
    public Integer TotalDocuments{get;set;}
    Set<Id> dsmtcId = new Set<Id>();

    public string apptJSON{get;set;}
    public String installJSON {get;set;}
    public String PostReviewjSON{get;set;}
    public String energySpecTicketsJSON {get;set;}
    public Dsmtracker_contact__c dsmtCon{get;set;}
    public String portalRole{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
    global Integer TotalProject{get;set;}
    global Integer TotalCustomer{get;set;}
    public String ObjPrefix{get;set;}
    Public boolean isHPcEnergySpecialist {get;set;}
    
    public dsmtConsoleHomePageCntrl(){
           String ListViewURL;
           String ListViewId;
           String q = 'SELECT Name FROM Recommendation_Scenario__c LIMIT 1';
           String ProjectListLbl = Label.Project_List_View;
           ApexPages.StandardSetController ACC = new ApexPages.StandardSetController(Database.getQueryLocator(q));
           List<SelectOption> ListViews = ACC.getListViewOptions();
           for(SelectOption w : ListViews){
               if(w.getLabel()==ProjectListLbl){
                   ListViewId = w.getValue().left(15);
               }
           }
           ObjPrefix = Recommendation_Scenario__c.SObjectType.getDescribe().getKeyPrefix()+'?fcf='+ListViewId;
            
           isPortal = true;
           isHPcEnergySpecialist  = false;
           headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
            
            TotalEligible = 0;
            TotalIneligible = 0;
            TotalOpenTickets = 0;
            TotalNewMeages = 0;
            UnreadMessage = 0;
            TotalMessage = 0;
            TotalApprovedEmployee = 0;
            TotalDocuments = 0;
            TotalProject = 0;
            TotalCustomer = 0;
            
            ta = new Trade_Ally_Account__c();
            dsmtCon = new Dsmtracker_Contact__c();
            dsmtcId = new Set<Id>();
            
            GetContactInfo();
            
            if(dsmtCon.Portal_User__r.Profile.Name != 'HPC Energy Specialist'){
                portalRole = dsmtCon.Portal_Role__c;
            }
            
            TilesCounting();
      }
      
       public void GetContactInfo(){
       
        
            String usrid = UserInfo.getUserId();
            List<User> usrList  = [select Id,IsPortalEnabled,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
            if(usrList != null && usrList.size() > 0){
              //  if(usrList.get(0).ContactId != null){
                    conName = usrList.get(0).Contact.Name;
                    conId   = usrList.get(0).ContactId;
                    accId = usrList.get(0).Contact.AccountId;
                    String uid = usrList.get(0).id;
                    
                    if(usrList.get(0).IsPortalEnabled){
                           headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                       }
                       else{
                           headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                           isPortal = false;
                       }
                    
                     List < DSMTracker_Contact__c > dsmtList = [Select Id, Portal_User__r.Profile.Name,Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c where Portal_User__c = : uId and Trade_Ally_Account__c != null limit 1];
                    if(dsmtList != null && dsmtList.size() > 0){
                        dsmtCon = dsmtList.get(0);
                        dsmtcId.add(dsmtCon.Id);
                        
                        List<Trade_Ally_Account__c> taList = [select id,name,Account__c,owner.Name,Owner.Phone,Owner.Email,Account_Manager__r.Name,Account_Manager__r.phone,Account_Manager__r.email,
                                                                Address__c,City__c,State__c,Zip_Code__c,Website__c,Phone__c,Internal_Account__c,
                                                                Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,
                                                                Outreach_rep__r.Phone,Outreach_rep__r.Email,Outreach_rep__r.Name,(select id from DSMTracker_Contacts__r)
                                                                from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                        if(taList != null && taList.size() > 0){
                            ta = taList.get(0);
                            boolean isinternal = ta.Internal_Account__c;
                            if(isinternal && dsmtCon.Portal_Role__c == 'Energy Specialist'){
                               isHPcEnergySpecialist = true;
                            }
                            if(!isHPcEnergySpecialist && dsmtCon.Portal_User__r.Profile.Name != 'HPC Energy Specialist'){
                                for(Dsmtracker_Contact__c dsmt : ta.DSMTracker_Contacts__r){
                                   dsmtcId.add(dsmt.Id);
                                }
                            }
                        }
                    }
                }
            //}
     }
    
      global void TilesCounting(){
          //if(portalRole == 'Scheduler'){
              String userId = userinfo.getUserId();
              List<Dsmtracker_Contact__c> dsmtconList = [select id,Super_User__c,Name,Trade_Ally_Account__c from DSMTracker_Contact__c where Portal_User__c =: userinfo.getUSerId()];
                String taId = '';
                if(dsmtConList != null && dsmtConList.size() > 0){
                    taId = dsmtConList.get(0).Trade_Ally_Account__c;
                }
                
                TotalEligible = [Select count() from Appointment__c where Appointment_Status__c = 'Approved' AND (OwnerId = :userId or User__c =: UserId or Trade_Ally_Account__c =: taId )];
                TotalIneligible = [Select count() from Appointment__c where Appointment_Status__c  = 'Denied'  AND (OwnerId = :userId or user__c =: UserId or Trade_Ally_Account__c =: taId )];
         
         // }
          if(portalRole == 'Manager'){
              TotalApprovedEmployee = [select count() from Dsmtracker_Contact__c where (status__c = 'Approved' or Status__c = '') and Trade_Ally_Account__c =: ta.Id and Review__c = null];
              Date nxt30Date = Date.today()+30;
              TotalDocuments = [select count() from Attachment__c where Expires_On__c!=null AND Expires_On__c <= :nxt30Date AND Expires_On__c >= :Date.today() AND Trade_Ally_Account__c =: ta.Id];
          }
          
          Id RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
          
          TotalOpenTickets = [select count() from service_Request__c where Status__c != 'Closed' AND Dsmtracker_Contact__c in: dsmtcId and RecordTypeId !=: recordTypeId];
          
          TotalMeages = [select count() from Messages__c where Message_Direction__c = 'Outbound' AND Dsmtracker_Contact__c =: dsmtCon.Id];
          TotalProject = [select Count() From Recommendation_Scenario__c where Status__c = 'Work Scope Approved'];
          datetime apptStartDate = System.now().addMonths(-3);
          
          TotalCustomer = [select count() from appointment__c where Appointment_Start_Time__c != null and Appointment_End_Time__c != null and Appointment_Status__c not in ('Pending','Incomplete','Ineligible') and Customer_Reference__c != null and Appointment_Start_Time__c >=: apptStartDate AND Dsmtracker_Contact__c in: dsmtcId];
          
          for(Messages__c m:[select Id,To__c from Messages__c where Message_Direction__c = 'Outbound' AND Status__c != 'Opened' AND Opened__c = null AND Dsmtracker_Contact__c =: dsmtCon.Id]){
              //if(m.To__c == UserInfo.getUserEmail()){
                  TotalNewMeages = TotalNewMeages +1;
              //}
          }
          
      }
      
    public void initEnergySpecTickets(){
        //List<EnergySpecTicketModel> tickets = new List<EnergySpecTicketModel>();
        List<Object> tickets = new List<Object>();
        if(dsmtCon != null){
            Set<String> recTypes = new Set<String>{'Energy Specialist Ticket','Trade Ally Service Request'};    
            List<Service_Request__c> srs = [
                SELECT 
                    Id,Name,Type__c,Sub_Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,CreatedDate,
                    Workorder__c,Workorder__r.Name,Customer__c,Customer_Name__c,Program_Utility_Provider__c,Primary_Provider__c,Rebate_Status__c,Boiler_Type__c,Service_Request_Panel_Title__c
                FROM Service_Request__c 
                WHERE /*RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN :recTypes) AND */ Dsmtracker_Contact__c =: dsmtCon.Id];
            for(Service_Request__c sr : srs){
                tickets.add(new Map<String,Object>{
                    'Id' => sr.Id,
                    'Name' => sr.Name,
                    'Priority' => sr.Priority__c,
                    'Subject' => sr.Subject__c,
                    'Type' => (sr.Type__c == 'Trade Ally Service Request')? 'Ticket' : sr.Type__c,
                    'subType' => sr.Sub_Type__c,
                    'CreatedDate' => sr.CreatedDate.format(),
                    'Status' => sr.Status__c,
                    'Description' => sr.Description__c,
                    'WorkOrderId'=> sr.Workorder__c,
                    'WorkOrderName'=> sr.Workorder__r.Name,
                    'CustomerId'=> sr.Customer__c,
                    'CustomerName'=>sr.Customer_Name__c,
                    'ProgramUtilityProvider'=> (sr.Program_Utility_Provider__c != null) ? sr.Program_Utility_Provider__c.replace('CSG','CLR') : sr.Program_Utility_Provider__c,
                    'Provider'=>sr.Primary_Provider__c,
                    'RebateStatus'=>sr.Rebate_Status__c,
                    'BoilerType'=>sr.Boiler_Type__c,
                    'PanelTitle'=>sr.Service_Request_Panel_Title__c
                });
            }
            System.debug('srs ----> ' + srs);
        }
        energySpecTicketsJSON = JSON.serialize(tickets);
    }
    
    public String selectedAppointmentId {get;set;}
    public String selectedAssessId {get;set;}
    public String newEnergyAssesmentURL {get;set;}
    public void generateNewEnergyAssesmentURL() {
        system.debug('--selectedAppointmentId-'+selectedAppointmentId);
        List<Appointment__c> appointments = [select Id, Customer_Id__c
                                             from Appointment__c where id =: selectedAppointmentId];
         
        if(appointments != null && appointments.size() > 0){
            Appointment__c appointment = appointments.get(0);
        
            newEnergyAssesmentURL = '/apex/dsmtGeneralInfo?Id='+appointment.Id +'&custId='+appointment.Customer_Id__c;
        
            if(selectedAssessId!=null)
            {
                newEnergyAssesmentURL += '&assessId='+selectedAssessId;
                selectedAssessId = null;
            }
        }
    }
    
    public void initScheduledAppointments(){
            /* DSST-7926 - Added todayDate for query filter
             By : Prasanjeet Sharma*/
            Date todayDate = System.today();
            
            String filterquery =  ' Where Id != null And Appointment_Start_Time__c != null and Appointment_End_Time__c != null And DSMTracker_Contact__c in : dsmtcId And';
            
            filterquery += ' Appointment_Status__c in (\'scheduled\',\'Rescheduled\') AND (Appointment_Start_Time__c = NEXT_90_DAYS OR Appointment_Start_Date__c =: todayDate) And';

            
            if(filterquery.endswith('And')){
                filterquery = filterquery.substring(0,filterquery.length() - 3);
            }
            
            //DSST-13885 - Added Appointment_Start_Date__c in query
            //Edited by - Prasanjeet Sharma
            String query = 'select id, Appointment_Start_Date__c, DSMTracker_Contact__r.Name,IsAssessment__c,(select id, Assessment_Complete_Date__c,Name from Energy_Assessment__r order by CreatedDate desc limit 1), Name,Customer_Reference__c,Customer_Reference__r.Premise__r.name,Customer_Reference__r.Name,Appointment_Status__c ,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,Employee__r.Name,Workorder__r.name,Workorder__r.Address_Formula__c,CreatedDate from Appointment__c';
            
            query += filterquery ;
        
            apptJSON = '';
            String userId = userinfo.getUserId();
           // String queryString = 'Select Id,Name,Customer_Reference__r.Name,Appointment_Type__c,Appointment_Start_Time__c,Appointment_End_Time__c,Employee__r.Name,Appointment_Status__c,CreatedDate from Appointment__c where Appointment_Status__c=\'Scheduled\' AND Appointment_Start_Time__c = NEXT_90_DAYS';
        
           // queryString += ' LIMIT 50000';
            
            list<Appointment__c> listMsg = Database.query(query);
            
            
            list<AppointmentModal> listAppoint = new List<AppointmentModal>();
            for(Appointment__c apt : Database.query(query)){
                 
                system.debug(apt.Workorder__r.name);
                AppointmentModal mod = new AppointmentModal();
                mod.WorkOrdernumber=apt.Workorder__r.name;
                mod.PremiseNumber=apt.Customer_Reference__r.Premise__r.name;
                mod.Address=apt.Workorder__r.Address_Formula__c;
                mod.Id = apt.Id;
                
                mod.CustomerId = apt.Customer_Reference__c;
                mod.CustomerName= apt.Customer_Reference__r.Name;
             
                mod.AppointmentType= apt.Appointment_Type__c;
                
                //  mod.StartTime = string.valueOf(apt.Appointment_Start_Time__c.Month())+'/'+string.valueOf(apt.Appointment_Start_Time__c.day())+'/'+string.valueOf(apt.Appointment_Start_Time__c.year()) + ' '+string.valueOf(apt.Appointment_Start_Time__c.hour()) + ':'+string.valueOf(apt.Appointment_Start_Time__c.Minute());
                //  mod.EndTime = apt.Appointment_End_Time__c.Month()+'/'+apt.Appointment_End_Time__c.day()+'/'+apt.Appointment_End_Time__c.year() + ' '+apt.Appointment_End_Time__c.hour() + ':'+apt.Appointment_End_Time__c.Minute();
                
                String strConvertedDate =   apt.Appointment_Start_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
                mod.StartTime =  strConvertedDate ;
                strConvertedDate =   apt.Appointment_End_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
                mod.EndTime =  strConvertedDate ;
                 
                //mod.EndTime = date.newinstance(apt.Appointment_End_Time__c.year(), apt.Appointment_End_Time__c.month(), apt.Appointment_End_Time__c.day());
                mod.Employee = apt.DSMTracker_Contact__r.Name;
                mod.Status = apt.Appointment_Status__c;
                mod.CreatedDate = string.valueOf(apt.CreatedDate.Month())+'/'+string.valueOf(apt.CreatedDate.Day())+'/'+string.valueOf(apt.CreatedDate.Year());// date.newinstance(apt.CreatedDate.year(), apt.CreatedDate.month(), apt.CreatedDate.day());
                
                String actions = '';
                
                if(apt.IsAssessment__c)
                {
                    if(apt.Energy_Assessment__r==null || apt.Energy_Assessment__r.isEmpty())
                    {
                        //DSST-13885 - Added Appointment_Start_Date__c
                        //Edited by - Prasanjeet Sharma
                        if(apt.Appointment_Start_Date__c != null && apt.Appointment_Start_Date__c <= date.today())
                            actions += '<a href="javascript:void(0)" class="eventa assessment-link" style="right: 50px;" id="'+apt.Id+'" onclick=openEnergyAssesment(\''+apt.Id+'\'); title="Start New Assessment"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i></a>';
                    }                   
                    else
                    {
                        for(Energy_Assessment__c ea: apt.Energy_Assessment__r)
                        {
                            if(ea.Assessment_Complete_Date__c == null)
                            {
                                actions += '<a href="javascript:void(0)" style="right: 50px;" class="eventa assessment-link" id="'+apt.Id+'" onclick=openEnergyAssesmentEdit(\''+ea.Id+'\',\''+apt.Id+'\'); title="Continue Assessment"><i class="fa fa-calendar-o" aria-hidden="true"></i></a>';
                            }
                        }
                    }
                }
                if(dsmtCon.Portal_User__r.Profile.Name == 'HPC Energy Specialist'){
                    mod.action = actions;
                }
                listAppoint.add(mod);
            }
            system.debug('--listMessage---'+listAppoint );
            apptJSON = JSON.serialize(listAppoint );
        
        system.debug('--listMessage---'+listAppoint );
    }

    public integer getHESReviews(){
        return [SELECT Id FROM Review__c WHERE CreatedById =:UserInfo.getUserId()].size();
    }
    public integer getHESServiceRequests(){
        return [SELECT Id FROM Service_Request__c WHERE OwnerId =:UserInfo.getUserId() AND CreatedById != :UserInfo.getUserId()].size();
    }
    public integer getHESOpenContracts(){
        return [SELECT Id FROM Proposal__c WHERE Status__c = 'Proposed' AND CreatedById =:UserInfo.getUserId() AND Name_Type__c IN ('Air Sealing','Electric Thermostats','Weatherization')].size();
    }
    public integer getHESProjectsReadyForReview(){
        return [SELECT Id FROM Recommendation_Scenario__c WHERE CreatedById =:UserInfo.getUserId() AND Status__c = 'Contracted'].size();
    }
    public integer getHESProjectsReadyToInstall(){
        return [
          SELECT Id 
          FROM Project_Review__c 
          WHERE Review__c != NULL AND Project__c != NULL
            AND Review_Record_Type__c IN ('Work Scope Review Manual','Work Scope Review') 
            AND Project__r.CreatedById =:UserInfo.getUserId() 
            AND Review__r.Status__c IN ('Accepted','Scheduled','Pending Contractor Acceptance')
        ].size();
    }
    public integer getHESProjectsInstalled(){
        return [
          SELECT Id
          FROM Project_Review__c
          WHERE Review__c != NULL AND Project__c != NULL
            AND Review_Record_Type__c IN ('Work Scope Review Manual','Work Scope Review') 
            AND Project__r.CreatedById =:UserInfo.getUserId()
            AND Review__r.Status__c = 'Complete Work Order'
        ].size();
        //return [SELECT Id FROM Recommendation_Scenario__c WHERE CreatedById =:UserInfo.getUserId() AND Status__c = 'Installed'].size();
    }
    
    public integer getPendingAcceptance(){
        return [select id from Review__c where Type__c = 'Work Scope Review' and Status__c = 'Pending Contractor Acceptance'].size();
    }
    
    public integer getPendingScheduling(){
        return [select id from Review__c where Type__c = 'Work Scope Review' and Status__c = 'Accepted'].size();
    }
    
    public integer getPastDueInstalls(){
        return [select id from Review__c where Type__c = 'Work Scope Review' and Status__c = 'Past Install'].size();
    }
    
    public integer getFailedInvoiceReview(){
        return [select id from Review__c where (Type__c = 'Billing Review' OR Type__c = 'Invoice Review') and Status__c = 'Failed Completion Review'].size();
    }
    
    public integer getWorkScopes(){
        return [select id from Review__c where Type__c = 'Work Scope Review' and (Status__c = 'Work Scope Approved' or Status__c = 'Approved' or Status__c = 'Accepted')].size();
    }
    
    public integer getWorkScopesRejected(){
        return [select id from Review__c where Type__c = 'Pre Work Review' and (Status__c = 'Failed Work Scope Review')].size();
    }
    
    public void initInstalls(){
        Date dt = System.today().addDays(90);
        
        //String filterquery =  ' Where Id != null And Schedule_Date__c != null and Schedule_Date__c <=: dt And DSMTracker_Contact__c in : dsmtcId And';
        String filterquery =  ' Where Id != null And Schedule_Date__c != null and Schedule_Date__c <=: dt And';
        
         List < DSMTracker_Contact__c > dsmtList = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c 
         where Portal_User__c = : userinfo.getUserId() and Trade_Ally_Account__c != null limit 1];
         
         if(dsmtList != null && dsmtList.size() > 0){
             Id TaId = dsmtList.get(0).Trade_Ally_Account__c;
             filterquery =  ' Where Id != null And Schedule_Date__c != null and Schedule_Date__c <=: dt And Trade_Ally_Account__c =: TaId  And';
         }
        filterquery += ' Status__c in (\'scheduled\') And';
        filterquery += ' Type__c in (\'Work Scope Review\') And';
        
        if(filterquery.endswith('And')){
            filterquery = filterquery.substring(0,filterquery.length() - 3);
        }
        String query = 'select id, Name, First_Name__c, Last_Name__c, Address__c, City__c, Email__c, Phone__c, Schedule_Date__c, Schedule_Duration__c, Schedule_Window__c, CreatedDate from Review__c';
        
        query += filterquery ;
    
        installJSON = '';
        
        list<Review__c> reviews = Database.query(query);
        
        
        list<ReviewModal> listReviews = new List<ReviewModal>();
        for(Review__c r : reviews){
            ReviewModal mod = new ReviewModal();
            
            mod.Review = r.Id;
            mod.ReviewName = r.Name;
            mod.FirstName = r.First_Name__c;
            mod.LastName = r.Last_Name__c;
            mod.Address = r.Address__c;
            mod.City = r.City__c;
            mod.Phone = r.Phone__c;
            mod.Email = r.Email__c;
            
            if(r.Schedule_Date__c != null)
                mod.ScheduleDate = r.Schedule_Date__c.format();
            else
                mod.ScheduleDate = '';
                
            mod.ScheduleDuration = r.Schedule_Duration__c;
            mod.ScheduleWindow = r.Schedule_Window__c;
            mod.CreatedDate = r.Id;
         
            listReviews.add(mod);
        }
        installJSON = JSON.serialize(listReviews);
    }
    
    
    Public void initpostInspectionGird(){
    
        Set<String> taQueueSet =new Set<String>();
        if(ta != null){
               String taName = ta.Name;
               String taId = ta.Id;
               if(taName != null){
                  taName = taName.replace(' ','_').replace('\'','_').replace(',','_').replace('.','').replace('-','_').replace('&','_');
               }
                system.debug('--taName--'+taName); 
                
                String queuename = taName+'_Queue';
                system.debug('---queuename---'+queuename);
                List<Group> glist = [select Id from Group where Type = 'Queue' AND DeveloperNAME =: queuename]; 
                //DSST-7933 START
                //if(glist.size() > 0 || Test.isRunningTest()){
                    List<Review__c> ReviewList = (Test.isRunningTest()) ? new List<Review__c>{new Review__c()} : [Select id,name,First_Name__c, Last_Name__c, Address__c, City__c, Email__c, Phone__c,Have_you_contacted_the_Customer__c,Is_the_return_scheduled__c,Return_Scheduled_Date__c,Return_Schedule_Window__c,Inspection_Request__r.Inspection_Result__c,Trade_Ally_Account__c,OwnerId from Review__c where Trade_Ally_Account__c =: taId and RecordType.Developername = 'Post_Inspection_Result_Review' and Status__c In ('New','Manager Fail')];
                    //List<Review__c> ReviewList = (Test.isRunningTest()) ? new List<Review__c>{new Review__c()} : [Select id,name,First_Name__c, Last_Name__c, Address__c, City__c, Email__c, Phone__c,Have_you_contacted_the_Customer__c,Is_the_return_scheduled__c,Return_Scheduled_Date__c,Return_Schedule_Window__c,Inspection_Request__r.Inspection_Result__c,Trade_Ally_Account__c,OwnerId from Review__c where OwnerId =: glist.get(0).Id and RecordType.Developername = 'Post_Inspection_Result_Review' ];
                    //DSST-7933 END
                    system.debug('--ReviewList--'+ReviewList);
                                            
                    list<ReviewModal> listReviews = new List<ReviewModal>();
                    for(Review__c r : ReviewList){
                        ReviewModal mod = new ReviewModal();
                    
                        mod.Review = r.Id;
                        mod.ReviewName = r.Name;
                        mod.FirstName = r.First_Name__c;
                        mod.LastName = r.Last_Name__c;
                        mod.Address = r.Address__c;
                        mod.City = r.City__c;
                        mod.Phone = r.Phone__c;
                        mod.Email = r.Email__c;
                        mod.InspectionResult=r.Inspection_Request__r.Inspection_Result__c;
                        mod.HaveyoucontactedtheCustomer = r.Have_you_contacted_the_Customer__c;
                        mod.IsthereturnScheduled = r.Is_the_return_scheduled__c;
                        if(r.Return_Scheduled_Date__c != null){
                           mod.ReturnScheduleDate = r.Return_Scheduled_Date__c.format();
                        }
                        
                        mod.ReturnScheduleWindow = r.Return_Schedule_Window__c;
                        
                        listReviews.add(mod);
                    }
                    PostReviewjSON = JSON.serialize(listReviews);
                //}       
            }                                     
    }
    
    public class AppointmentModal{
        public String WorkOrdernumber{get;set;}
        public String PremiseNumber{get;set;}
        public String Address{get;set;}
        public string Id{get;set;}
        public string CustomerName{get;set;}
        public string AppointmentType{get;set;}
        public String StartTime{get;set;}
        public String EndTime{get;set;}
        public string Employee{get;set;}
        public string Status{get;set;}
        public String CreatedDate{get;set;}
        public String CustomerId{get;set;}
        public String action {get;set;}
        
    }
    
    public class ReviewModal{
        public String Review{get;set;}
        public String ReviewId{get;set;}
        public String ReviewName{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        public string Address{get;set;}
        public string City{get;set;}
        public string Phone{get;set;}
        public String Email{get;set;}
        public String ScheduleDate{get;set;}
        public string ScheduleDuration{get;set;}
        public string ScheduleWindow{get;set;}
        public String CreatedDate{get;set;}
        public string InspectionResult{get;set;}
        
        public String HaveyoucontactedtheCustomer{get;set;}
        public String IsthereturnScheduled{get;set;}
        public String ReturnScheduleDate{get;set;}
        public String ReturnScheduleWindow{get;set;}
    }
    
   /* public class postReviewModel{
        public String Id{get;set;}
        public String Name{get;set;}
        public String FirstName{get;set;}
        public String LastName{get;set;}
        public String Address{get;set;}
        public String City{get;set;}
        public String Phone{get;set;}
        public String Email{get;set;}
        public String HaveyoucontactedtheCustomer{get;set;}
        public String IsthereturnScheduled{get;set;}
        public String ReturnScheduleDate{get;set;}
        public String ReturnScheduleWindow{get;set;}
    }*/
}