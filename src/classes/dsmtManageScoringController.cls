public class dsmtManageScoringController
{
    public String insReqId {get;set;}
    public String title {get;set;}
    public String notes {get;set;}
    public String scoringId {get;set;}
    public String groupName {get;set;}
    public String noteId {get;set;}
    public String attachmentId {get;set;}
    public Inspection_Scoring__c insScoring {get;set;}
    public List<ScoringWrapper> scoringWrapperList {get;set;}
    public List<RepairNotesWrapper> repairNotesWrapper {get;set;}
    public Inspection_Request__c insReq{get;set;}
    public Boolean addToReport{get;set;}
    public Note__c cnote{get;set;}
    public String showSummary{get;set;}
    
    map<String, Repair_Summary_Note__c> summaryNoteMap;
    
    public dsmtManageScoringController()
    {
        scoringWrapperList = new List<ScoringWrapper>();
        repairNotesWrapper = new List<RepairNotesWrapper>();
        summaryNoteMap = new map<String, Repair_Summary_Note__c>();
        
        scoringId = ApexPages.currentPage().getParameters().get('scoreId');
        noteId  = ApexPages.currentPage().getParameters().get('noteid');
        showSummary = ApexPages.currentPage().getParameters().get('showSummary');
        
        if(scoringId != null && scoringId.trim().length() > 0){
            if(noteId != null){
                 List<Note__c> notelist = [select Id, Name,Body__c,Title__c,Inspection_Scoring__c,
                             (Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate,Add_to_Inspection_Report__c from Attachments__r) from Note__c 
                            where Id =: noteId];
                 if(notelist.size() > 0){
                    cnote = notelist.get(0);
                 }   
                 
            }else{
                cnote = new Note__c();
            }
            
        }
        
        init();
    }
    
    Set<String> scoringIds = new Set<String>();
    private void init()
    {
        
        repairNotesWrapper = fetchRepairNotesWrapper();
       
        noteId = attachmentId = title = groupName = notes = '';
        insReqId = ApexPages.currentPage().getParameters().get('id');
        scoringWrapperList = new List<ScoringWrapper>();
        
        insScoring = new Inspection_Scoring__c();
        
        map<String, List<InspectionScoringWrapper>> insScoringMap = new map<String, List<InspectionScoringWrapper>>();
        map<String, Inspection_Scoring__c> iscoreMap = new map<String, Inspection_Scoring__c>();
        
        List<Inspection_Request__c> insreqlist = [select id,name from inspection_request__c where id =: insReqId];
        if(insreqlist.size() > 0){
            insreq = insreqlist.get(0);
        }
        
        List<Inspection_Scoring__c> scoringlist = queryScoring();
        for(Inspection_Scoring__c s : scoringlist){
            scoringIds.add(s.id);
        }
        
        Map<String,List<Note__c>> notemap = queryNotes();
        
        for(Inspection_Scoring__c s : scoringlist)
        {
            String ky = s.Group__c+' ';
            
            if(s.Clone_Number__c != null)
                ky += s.Clone_Number__c;
                
            if(s.Category__c != null)
                ky += ' - ' + s.Category__c;
            
            if(s.Subcategory__c != null)
                ky += ' - ' + s.Subcategory__c;
            
            List<InspectionScoringWrapper> tempList = insScoringMap.get(ky);
            if(tempList == null)
                tempList = new List<InspectionScoringWrapper>();
            
            InspectionScoringWrapper isw = new InspectionScoringWrapper();
            
            if(iscoreMap.containskey(s.Id))
                isw.insScore = iscoreMap.get(s.Id);
            else
                isw.insScore = s;
            
            isw.attachments = s.Attachments__r;
            
            
            List<Note__c> notes = notemap.get(s.id);
            if(notes == null){
                notes = new List<Note__c>();
            }
            isw.notes = notes;
            
            tempList.add(isw);
            
            insScoringMap.put(ky, tempList);
        }
        
        for(String s : insScoringMap.keyset())
        {
            system.debug(s);
            scoringWrapperList.add(new ScoringWrapper(s, insScoringMap.get(s)));
        }
    }
    
    private list<Inspection_Scoring__c> queryScoring()
    {
        return [select id, Name, Category__c, Group__c, Notes__c, Status__c, Attachment_Id__c, Subcategory__c, Task__c, Clone_Number__c,
               (Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate,Add_to_Inspection_Report__c from Attachments__r) ,
               (select Id, Name, Body__c,Inspection_Scoring__c from Notes__r) 
                from Inspection_Scoring__c
                where Inspection_Request__c =: insReqId
                and Inspection_Request__c != null
                order by Group__c, Clone_Number__c,Sequence__c NULLS FIRST];
    }
    
    
    private Map<String,List<Note__c>> queryNotes(){
         
         List<Note__c> notelist = [select Id, Name,Body__c,Title__c,Inspection_Scoring__c,
                             (Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate,Add_to_Inspection_Report__c from Attachments__r) from Note__c 
                            where Inspection_Scoring__c in: scoringIds and Inspection_Scoring__c != null order by name];
         
         Map<String,List<Note__c>> notemap = new Map<String,List<Note__c>>();
         for(Note__c note : notelist){
              List<Note__c> notes = notemap.get(note.Inspection_Scoring__c);
              if(notes == null){
                  notes = new List<Note__c>();
              }
              notes.add(note);
              
              notemap.put(note.Inspection_Scoring__c,notes);
         }       
         return notemap;       
    }
    
    public void cloneGroup()
    {
        if(groupName != null)
        {
            list<String> splitedStr = groupName.split(' - ');
            if(splitedStr.size() > 0)
            {
                String query  = ' select id, Name, Category__c, Group__c,Sequence__c,Attachment_Id__c, Inspection_Request__c, Notes__c, Scoring_Template__c, Status__c, Subcategory__c, Task__c, Clone_Number__c'; 
                       query += ' from Inspection_Scoring__c ';
                       query += ' where Inspection_Request__c =: insReqId ';
                       query += ' and Inspection_Request__c != null';
                
                decimal groupNumber;
                String cloneNumber, category, subcategory;
                
                if(splitedStr[0].contains(' '))
                {
                    list<String> splitedGroup = splitedStr[0].split(' ');
                    groupNumber = decimal.valueOf(splitedGroup[0]);
                }else{
                    groupNumber = decimal.valueOf(splitedStr[0]);
                }
                
                query += ' and Group__c =: groupNumber ';
                
                category = splitedStr[1];
                query += ' and Category__c =: category ';
                
                if(splitedStr.size() == 3)
                {
                    subcategory = splitedStr[2];
                    query += ' and Subcategory__c =: subcategory ';
                }
                
                String firstQuery = query + ' order by Clone_Number__c desc NULLS LAST limit 1';
                       
                list<Inspection_Scoring__c> insScoringList = database.query(firstQuery);
                
                if(insScoringList.size() > 0)
                {
                    cloneNumber = insScoringList[0].Clone_Number__c;
                    query += ' and Clone_Number__c =: cloneNumber ';
                    
                    insScoringList = database.query(query);
                    
                    list<Inspection_Scoring__c> newList = new list<Inspection_Scoring__c>();
                    for(Inspection_Scoring__c i : insScoringList)
                    {
                        Inspection_Scoring__c iscore = i.clone(false, false);
                        
                        if(cloneNumber != null)
                            iscore.Clone_Number__c = ' ' + getNextChar(cloneNumber);
                        else
                            iscore.Clone_Number__c = ' A';
                            
                        newList.add(iscore);
                    }
                    
                    if(newList.size() > 0)
                        insert newList;
                }
            }
        }
        
        init();
    }
    
    public PageReference saveRecords()
    {
        list<Inspection_Scoring__c> insScoringList = new list<Inspection_Scoring__c>();
        
        for(ScoringWrapper sw : scoringWrapperList)
        {
            for(InspectionScoringWrapper insw : sw.insScoringList)
            {
                insScoringList.add(insw.insScore);
            }
        }
        
        try{
            
            update insScoringList;
            
            if(isSaveForOtherAction != null && isSaveForOtherAction){
               return null;
            }else{
               return new PageReference('/'+insReqId).setRedirect(true);
            }
        }
        catch(Exception ex){
            system.debug('Exception :::::' + ex.getMessage());
        }
        
        return null;
    }
    
    public boolean isSaveForOtherAction{get;set;}
    public PageReference saveNotes()
    {
       
        if(scoringId != null)
        {
            /*if(title != null && notes != null && title.trim().length() > 0)
                insert new Note__c(Inspection_Scoring__c = scoringId, Body__c = notes, Name = title);*/
                
            if(cnote != null){
                cnote.Inspection_Scoring__c = scoringId;
                upsert cnote;
            }    
            
            if(repairNotesWrapper.size() > 0)
            {
                List<Repair_Summary_Note__c> insertList = new List<Repair_Summary_Note__c>();
                
                for(RepairNotesWrapper r : repairNotesWrapper)
                {
                    if(r.isCreate)
                    {
                        Repair_Summary_Note__c rsn = new Repair_Summary_Note__c();
                        rsn.Repair_Summary_Template__c = r.repairSummary.Id;
                        rsn.Inspection_Scoring__c = scoringId;
                        rsn.Comment__c = r.repairSummary.Comment__c;
                        
                        if(summaryNoteMap.containskey(r.repairSummary.Id))
                            rsn.Id = summaryNoteMap.get(r.repairSummary.Id).Id;
                        
                        insertList.add(rsn);
                    }
                }
                
                if(insertList.size() > 0)
                    upsert insertList;
            }
        }
        
        if(isSaveForOtherAction != null && isSaveForOtherAction){
            return null;
        }else{
           PageReference nextPage = new PageReference('/apex/dsmtManageScoring?id='+insReqId);
            nextPage.setRedirect(true);
            return nextPage;
        }
        
    }
    
    public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    
    public String baseURL{
        get {
            return System.URL.getSalesforceBaseUrl().toExternalForm();
        }
    }
    
     public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    
    public Void deleteNote(){
        if(noteId != null){
            
            List<Attachment__c> attachlist = [select id from attachment__c where note__c =: noteId];
            if(attachlist.size() > 0){
               delete attachlist;
            }
            delete new Note__c(id = noteId);
        }
            
        init();

    }
    
    public void deleteAttachment(){
        if(attachmentId != null)
            delete new Attachment__c(id = attachmentId);
            
        init();    
    }
    
    public void updateAddToReportAttachment(){
        if(attachmentId != null) { 
           Attachment__c attch = new Attachment__c(id = attachmentId,Add_to_Inspection_Report__c = addToReport); 
           update attch;
        }

        init();   
    }
    
    public Inspection_Scoring__c inspScoring{get;set;}
    public list<RepairNotesWrapper> fetchRepairNotesWrapper()
    {
        list<RepairNotesWrapper> wrapList = new list<RepairNotesWrapper>();
        System.debug('scoringId ->'+scoringId);
        if(scoringId != null)
        {
            List<Inspection_Scoring__c> inspScoringList = [select id, Category__c, Subcategory__c, Task__c,Scoring_Template__c,
                                                           (Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate,Add_to_Inspection_Report__c from Attachments__r) 
                                                           from Inspection_Scoring__c
                                                           where id =: scoringId
                                                           and Category__c != null 
                                                           and Subcategory__c != null
                                                           and Task__c != null];
            
            if(inspScoringList.size() > 0)
            {    
                inspScoring = inspScoringList.get(0); 
                system.debug('--inspScoring--'+inspScoring);
                List<Repair_Summary_Note__c> summaryNoteList = [select id, Comment__c, Inspection_Scoring__c, Repair_Summary_Template__c
                                                                from Repair_Summary_Note__c
                                                                where Inspection_Scoring__c =: scoringId
                                                                and Repair_Summary_Template__c != null];
                
                for(Repair_Summary_Note__c r : summaryNoteList)
                {
                    summaryNoteMap.put(r.Repair_Summary_Template__c, r);
                }
                                                                
                List<Repair_Summary_Template__c> summaryList = [select id, CATEGORY__c, SUB_CATEGORY__c, TASK__c, COMMENT__c
                                                                from Repair_Summary_Template__c
                                                                where scoring_Template__c =: inspScoringList[0].Scoring_Template__c
                                                                and scoring_Template__c != null];
                if(summaryList.size() > 0)
                {
                    for(Repair_Summary_Template__c s : summaryList)
                    {
                        RepairNotesWrapper rn = new RepairNotesWrapper();
                        rn.repairSummary = s;
                        
                        if(summaryNoteMap.containskey(s.Id)){
                            rn.isSelected = true;
                            rn.repairNoteId = summaryNoteMap.get(s.Id).Id;
                            rn.repairSummary.COMMENT__c = summaryNoteMap.get(s.Id).COMMENT__c;
                        }
                        
                        wrapList.add(rn);
                    }
                }
            }
        }
        
        return wrapList;
    }
    
    public class RepairNotesWrapper 
    {
        public boolean isSelected {get;set;}
        public boolean isCreate {get;set;}
        public String repairNoteId {get;set;}
        public Repair_Summary_Template__c repairSummary {get;set;}
        
        public RepairNotesWrapper()
        {
            isSelected = false;
            isCreate = false;
            repairSummary = new Repair_Summary_Template__c();
        }
    }
    
    public static String getNextChar(String oldChar) {
        String key = 'ABCDEFGHIJKLMNOPQRSTUVWXYZAabcdefghijklmnopqrstuvwxyza';
        Integer index = key.indexOfChar(oldChar.charAt(0));
        return index == -1? null: key.substring(index+1, index+2);
    }
    
    
    public class ScoringWrapper
    {
        public String groupName {get;set;}
        public list<InspectionScoringWrapper> insScoringList {get;set;}
        
        public ScoringWrapper(String groupName, list<InspectionScoringWrapper> insScoringList)
        {
            this.groupName = groupName;
            this.insScoringList = insScoringList;
        }
    }
    
    public class InspectionScoringWrapper 
    {    
        public Inspection_Scoring__c insScore {get;set;}
        public List<Attachment__c> attachments {get;set;}
        public List<Note__c> notes {get;set;}
     
    }
    
   
}