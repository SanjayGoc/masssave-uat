public class dsmtGenerateDocAndMigrateCntrl{
    public boolean enablePollar{get;set;}
    public Id congaAttachmentId {get;set;}
    public boolean pollarSuccess{get;set;}
    public String QueryId{get;set;}
    public String TemplateId{get;set;}
    public String RecordId{get;set;}
    Public String pv0{get;set;}
    Public String ofn{get;set;}
    Public String MasterId{get;set;}
    Public integer oldAttachmentsSize{get;set;}
    Public String stdAttachmentId{get;set;}
    Public String ObjectType{get;set;}
    public boolean ispreview{get;set;}
    public Attachment__c customAttachment{get;set;}
    Public String FormType{get;set;}
    Public boolean isContract{get;set;}
    Public boolean isCustomerService{get;set;}

    public dsmtGenerateDocAndMigrateCntrl(){
        QueryId= ApexPages.currentPage().getParameters().get('QueryId');
        TemplateId= ApexPages.currentPage().getParameters().get('TemplateId');
        pv0= ApexPages.currentPage().getParameters().get('pv0');
        MasterId= ApexPages.currentPage().getParameters().get('MasterId');
        RecordId = Apexpages.currentPage().getParameters().get('Id');
        ObjectType = Apexpages.currentPage().getParameters().get('ObjectType');
        String previewStr = Apexpages.currentPage().getParameters().get('ispreview');
        FormType = Apexpages.currentPage().getParameters().get('FormType');
        String isContractStr = Apexpages.currentPage().getParameters().get('isContract');
        String isCustomerServiceStr = Apexpages.currentPage().getParameters().get('generateCustomerService');
        
        system.debug('--RecordId--'+RecordId);
        ispreview = false;
        if(previewStr != null){
           ispreview = Boolean.valueof(previewStr);
        }
        
        isContract = false;
        if(isContractStr != null){
           isContract = Boolean.valueof(isContractStr);
        }
        
        isCustomerService = false;
        if(isCustomerServiceStr != null){
           isCustomerService = Boolean.valueof(isCustomerServiceStr);
        }
        
        system.debug('--preview--'+ispreview);
        
        if(ObjectType == 'Inspection_Request__c') {
            ofn = 'COI Document';
            if(isCustomerService) {
                ofn = 'Customer Service Survey';
            }
        } else {
            ofn = 'Contract Document';
        }
        
    }
    
    public PageReference checkCongaStatus() {
        try{
            pollarSuccess = false;
            enablePollar = true;
            List<Attachment__c> customAttachmentlist = [SELECT Id,Name,Attachment_Name__c,Attachment_Type__c, (SELECT Id,Name FROM Attachments) FROM Attachment__c WHERE Id =: MasterId];
            system.debug('--customAttachmentlist--'+customAttachmentlist);
            if(customAttachmentlist != null && customAttachmentlist.size() > 0 && customAttachmentlist.get(0).Attachments.size() > 0) {
                
                customAttachment = customAttachmentlist.get(0);
                pollarSuccess = true;
                
                DocumentServiceWSFile.clsSalesForceLoginDetail loginDetail = new DocumentServiceWSFile.clsSalesForceLoginDetail();
                loginDetail.SessionID = UserInfo.getSessionId();
                loginDetail.serverURL = Login_Detail__c.getInstance().Server_URL__c;
                loginDetail.orgID = Login_Detail__c.getInstance().Org_Id__c;
                loginDetail.userName = Login_Detail__c.getInstance().Attachment_User_Name__c;
                loginDetail.password = Login_Detail__c.getInstance().Attachment_User_Password__c;
                loginDetail.securityToken = Login_Detail__c.getInstance().Attachment_User_Security_Token__c;
                
                DocumentServiceWSFile.clsQueryStringParameters clsQueryStringParametersObj = new DocumentServiceWSFile.clsQueryStringParameters();
                clsQueryStringParametersObj.SessionID = UserInfo.getSessionId();
                clsQueryStringParametersObj.ServerURL = Login_Detail__c.getInstance().Server_URL__c;
                clsQueryStringParametersObj.OrgId  = Login_Detail__c.getInstance().Org_Id__c;
                clsQueryStringParametersObj.RecordId = RecordId;
                clsQueryStringParametersObj.AttachmentLookupCol = ObjectType;
                clsQueryStringParametersObj.AttachmentId = MasterId;
                clsQueryStringParametersObj.AttachmentType = customAttachment.Attachment_Type__c;
                clsQueryStringParametersObj.SecurityKey = 'intelcore2duo';
                clsQueryStringParametersObj.AccountKey = 'nokia710lumia';
                clsQueryStringParametersObj.Replace = 'YES';
                
                System.debug('==REQUEST... '+ loginDetail + ' AND ' +clsQueryStringParametersObj );
                                
                stdAttachmentId = customAttachment.Attachments.get(0).Id;
                DocumentServiceWSFile.DocumentServiceWSFileSoap documentServiceWSFile = new DocumentServiceWSFile.DocumentServiceWSFileSoap();
                DocumentServiceWSFile.ArrayOfClsUploadProcessResult result = documentServiceWSFile.MigrateAttachment(clsQueryStringParametersObj, stdAttachmentId, 'attachment', loginDetail);
                List<Attachment__c> list_pc_records = [SELECT Id, Status__c,Project__c,Attachment_Type__c FROM Attachment__c WHERE Id = :customAttachment.Id AND Status__c = 'Completed' LIMIT 1];
                if(list_pc_records.size() > 0){
                     
                     enablePollar = false;
                     
                     if(ObjectType=='Project__c') {
                         List<Attachment__c> attlist = [SELECT Id FROM attachment__c WHERE Project__c = :RecordId and attachment_Type__c = 'Customer Signature'];
                         DELETE attlist;
                     } else if(ObjectType=='Barrier__c') {
                         List<Attachment__c> attlist = [SELECT Id FROM attachment__c WHERE Barrier__c= :RecordId and attachment_Type__c = 'Customer Signature'];
                         DELETE attlist;
                     } 
                     return redirectToCompleted();
                }
            }
        }catch(Exception ex) {
            enablePollar = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
       return null;
    }
     
    public PageReference redirectToCompleted() {
    
      String url = '';
      if(ispreview != null && ispreview){
        url = '/apex/dsmtPreviewAndEmailDocument?';
        url += 'Id='+RecordId;
        url += '&objectType='+ObjectType;
        url += '&attachType='+customAttachment.Attachment_Type__c;
        url += '&attachmentId='+MasterId;
        
        if((ObjectType == 'Inspection_Request__c' && customAttachment.Attachment_Type__c != 'Final Inspection Report')) {
            url += '&showSignboard=true';
        } 
        
        if(ObjectType == 'Proposal__c' && isContract == true) {
            url += '&showSignboard=true&isContract=true';
        } 
      }else{
         url = '/'+RecordId;
      }
      system.debug('--url--'+url);
      PageReference page = new PageReference(url);
      page.setRedirect(true);
      return page;
    }
}