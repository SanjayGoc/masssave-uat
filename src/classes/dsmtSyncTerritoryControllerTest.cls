@isTest
Public class dsmtSyncTerritoryControllerTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        acc.Billing_Account_Number__c= 'Gas-~~~1234344';
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        acc2.Utility_Service_Type__c='Gas';
        insert acc2;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        insert obj;
        
        Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today()-1;
        insert obj;
        
        Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        insert obj;
        
        Task tsk = new Task();    
        tsk.WhatId = obj.Id;
       // tsk.WhoId = l.Id;
        tsk.CallDisposition = 'Outbound';
        tsk.Status = 'Completed';
        tsk.Subject = ' Call To ';
        tsk.Description = 'test';
        tsk.ActivityDate = Date.Today();
        tsk.Type = 'Call';
        insert tsk;
        
        Note n = new Note();
        n.ParentId = obj.Id;
        n.Title = 'test';
        n.Body = 'test';
        insert n;
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
         Customer__c cust = Datagenerator.createCustomer(acc2.id,null);
        
        ApexPages.currentPage().getParameters().put('clliId',obj.Id);
        
        dsmtSyncTerritoryController controller = new dsmtSyncTerritoryController ();
        controller.autoRun();
        
        
    }
}