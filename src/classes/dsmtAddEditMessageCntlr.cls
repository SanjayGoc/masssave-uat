global class dsmtAddEditMessageCntlr{
    
    public string msgId{get;set;}
    public Messages__c objMsg{get;set;}
    public string body{get;set;}
    public boolean isNew{get;set;}
    public string msgDir{get;set;}
    public boolean isMsgRead{get;set;}
    
    public dsmtAddEditMessageCntlr(){
       isNew = true;
       isMsgRead= false;
       msgId = ApexPages.currentPage().getParameters().get('id');
       msgDir = ApexPages.currentPage().getParameters().get('msgFilter');
       string msgRead = ApexPages.currentPage().getParameters().get('msgRead');
       objMsg = new Messages__c();
       if(msgId!=null){
           List<Messages__c> lstMsgs = [select Id,Message_Date__c,Sent__c,From__c,CC__c,BCC__c,To__c,Offer_Letter_Message_Title__c,Name,Subject__c,Deleted__c,Message__c,Accepted_Rejected__c,Message_Type__c,Opened__c,Archive__c,Message_Direction__c from Messages__c where Id = :msgId Limit 1];
           if(lstMsgs.size()>0){
               objMsg = lstMsgs[0];
               body = objMsg.Message__c!=null?objMsg.Message__c:'';
               isNew = false;
           }
       }
       if(msgRead == 'true'){
           isMsgRead = true;
       }
    }
    
    public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    
    public void saveMsg(){
        //if(objMsg.Name != null && objMsg.Name!=''){
        if(!isMsgRead){
            objMsg.From__c = userinfo.getuserEmail();
            if(msgDir!=null){
                if(msgDir == 'Inbox')
                    objMsg.Message_Direction__c = 'Outbound';
                if(msgDir == 'Sent')
                    objMsg.Message_Direction__c = 'Inbound';
            }
            
            
            String usrid = UserInfo.getUserId();
            List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
            if(usrList != null && usrList.size() > 0){
                if(usrList.get(0).ContactId != null){
                   string conId   = usrList.get(0).ContactId;
                    
                    List<DSMTracker_Contact__c> dsmtList = [select id,Trade_Ally_Account__c from DSMTracker_Contact__c
                                                                where Contact__c =: conId /*and OAP_Profile__c = true */limit 1];
                    if(dsmtList != null && dsmtList.size() > 0){
                        List<Trade_Ally_Account__c> taList = [select id,Owner.Email from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                        if(taList != null && taList.size() > 0){
                            objMsg.To__c = taList[0].Owner.Email;
                            objMsg.To_Email__c = taList[0].Owner.Email;
                        }
                     }
                 }
            }
            system.debug('@@body@@'+body);
            if(body!=null){
                objMsg.Message__c = body;
            }
            upsert objMsg;
           }
            msgId = objMsg.Id;
            
        //}
    }
    
    @RemoteAction
    global static String getAttachmentsByTicketId(String pid) {
            list<Attachment__c> listAtts;
            if(pid!=null && pid!=''){
                //Integer count = [Select COUNT() from Attachment__c where Attachment_Type__c = 'OfferLetter' and CreatedBy =: uid and Messages__c = :pid];
                Integer count = [Select COUNT() from Attachment__c where Message__c = :pid];
                if(count > 0){
                  listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Message__c = :pid order by CreatedDate desc];
                } 
            }else{
                return null;
            }
            return JSON.serializePretty(listAtts);
          // return count;
            //return listAtts      
     }
     @RemoteAction
     global static String  delAttachmentsById(String aid, String tid) {
                
            Attachment__c a = [select id from Attachment__c where id=:aid];
            delete a;
            list<Attachment__c> listAtts;
            Integer count = [Select COUNT() from Attachment__c where  Message__c = :tid];
            if(count > 0){
              listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Message__c = :tid order by CreatedDate desc];
            } 
            return JSON.serializePretty(listAtts);
            //return listAtts      
      }
}