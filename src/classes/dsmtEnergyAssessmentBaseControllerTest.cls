@isTest
public class dsmtEnergyAssessmentBaseControllerTest
{
    public testmethod static void test1()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtRecursiveTriggerHandler.preventAirFlowAndAirLeakageTrigger = true;
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        Energy_Assessment__c ea = datagenerator.setupAssessment();
        
        Building_Specification__c bs =[select id from Building_Specification__c where id =: ea.Building_Specification__c];
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        test.startTest();
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc;
        Recommendation__c rc1 = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc1;
        
        Review__c rev = new Review__c(Project__c = proj.id, Status__C='New');
        insert rev;
        
        customer__c cus = new customer__c();
        cus.name = 'test';
        insert cus;
        
        Inspection_Request__c ir = new Inspection_Request__c(Energy_Assessment__c = ea.id);
        insert ir;
        
        Inspection_Line_Item__c ili = new Inspection_Line_Item__c(Recommendation__c = rc1.id,Inspection_Request__c=ir.id);
        insert ili;
        
        Air_Flow_and_Air_Leakage__c afl = new Air_Flow_and_Air_Leakage__c(Building_Specifications__c = bs.id);
        //  insert afl;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.Appointment_Type__c='Expanded HEA';
        insert ec;
        
        Workorder__c w = new Workorder__c(Eligibility_Check__c=ec.id);
        w.Early_Arrival_Time__c = datetime.now();
        w.Late_Arrival_Time__c = datetime.now();        
        insert w;
        
        Appointment__c apt = new Appointment__c(Workorder__c=w.id);
        insert apt;
        
        Thermal_Envelope__c te = new Thermal_Envelope__c(Energy_Assessment__c=ea.id);
        insert te;
        List<Thermal_Envelope_type__c> thList = new List<Thermal_Envelope_type__c>();
        
        dsmtEnergyAssessmentBaseController.ThermalEvelopeTypeWrapper wper = new dsmtEnergyAssessmentBaseController.ThermalEvelopeTypeWrapper('test',thList);
        
        ApexPages.currentPage().getParameters().put('prjId',proj.id);
        ApexPages.currentPage().getParameters().put('recid',rc.id);
        ApexPages.currentPage().getParameters().put('revid',rev.id);
        ApexPages.currentPage().getParameters().put('id',ir.id);
        
        // ApexPages.currentPage().getParameters().put('custId',cus.id);
        ApexPages.currentPage().getParameters().put('assessId','');
        ApexPages.currentPage().getParameters().put('custId',cus.id);
        ApexPages.currentPage().getParameters().put('id',apt.id);
        dsmtEnergyAssessmentBaseController cntrl = new dsmtEnergyAssessmentBaseController();
        list<note> no = new list<note>();
        
        list<attachment> atac = new list<attachment>();
        
        cntrl.saveNote();
        cntrl.editNote();
        cntrl.deleteNote();
        cntrl.loadBuildingSpecificationData(ea.id);
        cntrl.loadAirFlowAirLeakageData(bs.id);
        cntrl.loadAppointmentData(apt.id);
        cntrl.loadThermalEnvelopeData(ea.id);
        cntrl.saveGeneralInformation();
        
        //cntrl.saveBuildingSpecificationDetails();
        cntrl.saveAirFlowAirLeakageDetails();
        
        cntrl.sleepThread();
        String test11 = cntrl.uniqueExternalId;
        boolean test1= cntrl.hasPageError;
        //    Attachment attach = cntrl.getAttachment(att.id);
        
        /*cntrl.validateRequest();
        */
        test.stopTest();
    }
    
    public testmethod static void testValidateRequest()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtRecursiveTriggerHandler.preventAirFlowAndAirLeakageTrigger = true;
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        Energy_Assessment__c ea = datagenerator.setupAssessment();
        
        Building_Specification__c bs =[select id from Building_Specification__c where id =: ea.Building_Specification__c];
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        test.startTest();
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc;
        Recommendation__c rc1 = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc1;
        
        Review__c rev = new Review__c(Project__c = proj.id, Status__c='New');
        insert rev;
        
        customer__c cus = new customer__c();
        cus.name = 'test';
        insert cus;
        
        Inspection_Request__c ir = new Inspection_Request__c(Energy_Assessment__c = ea.id);
        insert ir;
        
        Inspection_Line_Item__c ili = new Inspection_Line_Item__c(Recommendation__c = rc1.id,Inspection_Request__c=ir.id);
        insert ili;
        
        Air_Flow_and_Air_Leakage__c afl = new Air_Flow_and_Air_Leakage__c(Building_Specifications__c = bs.id);
        //  insert afl;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.Appointment_Type__c='Expanded HEA';
        insert ec;
        
        Workorder__c w = new Workorder__c(Eligibility_Check__c=ec.id);
        w.Early_Arrival_Time__c = datetime.now();
        w.Late_Arrival_Time__c = datetime.now();        
        insert w;
        
        Appointment__c apt = new Appointment__c(Workorder__c=w.id);
        insert apt;
        
        Thermal_Envelope__c te = new Thermal_Envelope__c(Energy_Assessment__c=ea.id);
        insert te;
        List<Thermal_Envelope_type__c> thList = new List<Thermal_Envelope_type__c>();
        
        dsmtEnergyAssessmentBaseController.ThermalEvelopeTypeWrapper wper = new dsmtEnergyAssessmentBaseController.ThermalEvelopeTypeWrapper('test',thList);
        
        ApexPages.currentPage().getParameters().put('prjId',proj.id);
        ApexPages.currentPage().getParameters().put('recid',rc.id);
        ApexPages.currentPage().getParameters().put('revid',rev.id);
        ApexPages.currentPage().getParameters().put('id',ir.id);
        
        // ApexPages.currentPage().getParameters().put('custId',cus.id);
        ApexPages.currentPage().getParameters().put('assessId','');
        ApexPages.currentPage().getParameters().put('custId',cus.id);
        ApexPages.currentPage().getParameters().put('id',apt.id);
        dsmtEnergyAssessmentBaseController cntrl = new dsmtEnergyAssessmentBaseController();
        list<note> no = new list<note>();
        
        try{
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            cntrl.validateRequest();
        }catch(Exception ex){}
        
        test.stopTest();
    }
    
    public testmethod static void test11()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtRecursiveTriggerHandler.preventAirFlowAndAirLeakageTrigger = true;
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        Energy_Assessment__c ea = datagenerator.setupAssessment();      
        test.startTest();
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc;
        Recommendation__c rc1 = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc1;
        
        Review__c rev = new Review__c(Project__c = proj.id, status__c='New');
        insert rev;
        
        customer__c cus = new customer__c();
        cus.name = 'test';
        insert cus;
        
        Building_Specification__c bs = new Building_Specification__c();
        // insert bs;
        Air_Flow_and_Air_Leakage__c afl = new Air_Flow_and_Air_Leakage__c(Building_Specifications__c = bs.id);
        // insert afl;
        Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                                             Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.Appointment_Type__c='Expanded HEA';
        ec.Workorder_Type__c=woTypeList.id;
        insert ec;
        
        Workorder__c w = new Workorder__c(Eligibility_Check__c=ec.id);
        w.Late_Arrival_Time__c = system.now();
        w.Early_Arrival_Time__c = system.now();
        insert w;
        Appointment__c apt = new Appointment__c(Workorder__c=w.id);
        insert apt;
        Thermal_Envelope__c te = new Thermal_Envelope__c(Energy_Assessment__c=ea.id);
        insert te;
        
        Attachment att = new Attachment(Name='Test',ParentId=ea.id,body=blob.valueof('test'));
        insert att;
        note na = new note(parentid=ea.id,title='test');
        insert na; 
        
        ApexPages.currentPage().getParameters().put('prjId',proj.id);
        ApexPages.currentPage().getParameters().put('recid',rc.id);
        ApexPages.currentPage().getParameters().put('revid',rev.id);
        ApexPages.currentPage().getParameters().put('noteIdToEdit',na.id);
        ApexPages.currentPage().getParameters().put('miliseconds','1111');
        ApexPages.currentPage().getParameters().put('parentid',ea.Id);
        //ApexPages.currentPage().getParameters().put('KEY_ASSESSMENT_ID', ea.id);
        //ApexPages.currentPage().getParameters().put('KEY_CUSTOMER_ID',cus.id);
        ApexPages.currentPage().getParameters().put('id',apt.id);
        
        dsmtEnergyAssessmentBaseController cntrl = new dsmtEnergyAssessmentBaseController();
        
        list<note> no = new list<note>();
        no = cntrl.loadNotes(na.id);
        list<attachment> atac = new list<attachment>();
        atac = cntrl.loadAttachments(att.id);
        cntrl.newNote.title = 'test';
        cntrl.saveNote();
        cntrl.editNote();
        
        //cntrl.getParam('KEY_ASSESSMENT_ID');
        cntrl.validateRequest();
        //ApexPages.currentPage().getParameters().put('KEY_ASSESSMENT_ID',ea.Id);
        //cntrl.validateRequest();
        cntrl.loadBuildingSpecificationData(ea.id);
        cntrl.loadAirFlowAirLeakageData(bs.id);
        cntrl.loadAppointmentData(apt.id);
        cntrl.loadThermalEnvelopeData(ea.id);
        cntrl.saveGeneralInformation();
        bs.Bedromm__c = 1;
        bs.Occupants__c = 10;
        //cntrl.saveBuildingSpecificationDetails();
        //    cntrl.saveAirFlowAirLeakageDetails();
        dsmtEnergyAssessmentBaseController.doUploadAttachment('acctId', 'attachmentBody', 'attachmentName', 'mimeType', att.id );
        cntrl.sleepThread();
        apt.Workorder__c = null;
        cntrl.loadAppointmentData(apt.id);
        
        ApexPages.currentPage().getParameters().put('parentId',att.id);
        
        cntrl.saveNote();
        cntrl.deleteNote();
        cntrl.Provider = 'test';
        cntrl.AccountType = 'test';
        cntrl.AccountNumber = 'test';
        
        
        ApexPages.currentPage().getParameters().put('attachmentId',att.id);
        cntrl.deleteAttachment();
        cntrl.loadAirFlowAirLeakageData(bs.Id);
        cntrl.loadAppointmentData(apt.id);
        cntrl.loadThermalEnvelopeData(ea.id); 
        cntrl.loadBuildingSpecificationData(ea.id);     
        PageReference pageRef = new PageReference('ActiveParticipant/apex/?ActiveParticipant');
        Test.setCurrentPage(pageRef);
        Test.stopTest();
    }
    
    @istest
    Public Static Void Runtest2()
    {
        dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
        dsmtRecursiveTriggerHandler.preventAirFlowAndAirLeakageTrigger = true;
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        Energy_Assessment__c ea = datagenerator.setupAssessment();
        
        test.startTest();
        
        Building_Specification__c bs =[select id from Building_Specification__c where id =: ea.Building_Specification__c];
        ea.Building_Specification__c = bs.id;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.I_heat_my_home_with__c='Oil';
        ec.Appointment_Type__c='Expanded HEA';
        insert ec;
        
        
        
        Workorder__c wo = new Workorder__c();
        //wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();
        insert wo;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        appt.Appointment_Type__c='Lunch';
        insert appt;
        
        Attachment__c att = new Attachment__c();
        att.attachment_Type__c='Contract Document';
        att.status__c = 'Completed';
        insert att;
        
        Attachment attch= new Attachment();
        attch.Name= 'test';
        attch.ParentId= att.id;
        attch.Body= blob.ValueOf('test');
        insert attch;
        
        
        
        dsmtEnergyAssessmentBaseController cntrl = new dsmtEnergyAssessmentBaseController();
        cntrl.validateRequest();
        cntrl.loadBuildingSpecificationData(ea.Id);
        cntrl.loadAirFlowAirLeakageData(bs.Id);
        cntrl.loadAppointmentData(appt.Id);
        cntrl.saveGeneralInformation();
        dsmtEnergyAssessmentBaseController.cloneAttachmentToEA(ea.Id, attch.id);

        test.stopTest();
    }
}