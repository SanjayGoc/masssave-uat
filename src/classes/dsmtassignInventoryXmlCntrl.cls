public class dsmtassignInventoryXmlCntrl {

    public String available_invitem_grid_data_xml{get;set;}
    public String available_invitem_grid_summarydata_xml{get;set;}
    public String assigned_invitem_grid_data_xml{get;set;}
    public String assigned_invitem_grid_summarydata_xml{get;set;}
    public String gridtype{get;set;}
    public dsmtassignInventoryXmlCntrl(){
        
        gridtype = ApexPages.currentPage().getParameters().get('type');
        if(gridtype != null){
            if(gridtype == 'availabledetail'){
               getdataforAvailableInvItemGrid();
            }else if(gridtype == 'assigneddetail'){
               getdataforAssignedInvItemGrid();
            }else if(gridtype == 'availablesummary'){
               getsummarydataforAvailableGrid();
            }else if(gridtype == 'assignedsummary'){
               getsummarydataforAssignedGrid();
            }   
        }
    }
    
    public void getdataforAvailableInvItemGrid(){
       try{
          system.debug('--getdataforAvailableInvItemGrid Called--');
          string warehouse = ApexPages.currentPage().getParameters().get('warehouse');
          string product   = ApexPages.currentPage().getParameters().get('product');
          string location = ApexPages.currentPage().getParameters().get('location');
          
          system.debug('--warehouse--'+warehouse);
          system.debug('--product--'+product);
          system.debug('--location--'+location);
          
          if(location == null || location == '' || location == '0'){
             return;
          }
          
          String productids = '';
          
          if(product != 'All'){
             List<String> commasplit = product.split(',');
             
             for(String prd : commasplit){
               if(prd != 'All'){
                 productids += '\''+prd+'\',';     
               }
            }
            
            if(productids.length() > 0){
               productids = '('+productids.subString(0,productids.length() - 1)+')';
            }
          }
          
          
          String query = 'select id,name,Product__c,Product__r.Name,Status__c,Serial_Number__c,Warehouse__r.Name from Inventory_Item__c where status__c = \'Available\' and Warehouse__r.Location__c = \''+location+'\'' ;
          
          if(warehouse != null && warehouse != '' && warehouse != 'All'){
             query += ' and Warehouse__r.Id = \''+warehouse+'\'';
          }
          if(product != null && product != '' && product != 'All'){
             query += ' and Product__r.Id in '+productids;
          }
          
          List<Inventory_Item__c> invitemlist = Database.Query(query);
          system.debug('--invitemlist--'+invitemlist.size());
          available_invitem_grid_data_xml = '';
          
          available_invitem_grid_data_xml += '<InventoryItems>';
          for(Inventory_Item__c invitem : invitemlist){
              available_invitem_grid_data_xml += '<InventoryItem>';
              available_invitem_grid_data_xml += '<Id><![CDATA['+invitem.Id+']]></Id>';
              available_invitem_grid_data_xml += '<Name><![CDATA['+invitem.Name+']]></Name>';
              available_invitem_grid_data_xml += '<Product><![CDATA['+invitem.Product__r.Name+']]></Product>';
              //available_invitem_grid_data_xml += '<Warehouse><![CDATA['+invitem.Inventory_Item__r.Warehouse__r.Name+']]></Warehouse>';
              //available_invitem_grid_data_xml += '<StockInventory><![CDATA['+invitem.Inventory_Item__r.Stock_Number__c+']]></StockInventory>';
              available_invitem_grid_data_xml += '<SerialNumber><![CDATA['+invitem.Serial_Number__c+']]></SerialNumber>';
              available_invitem_grid_data_xml += '<Status><![CDATA['+invitem.Status__c+']]></Status>';
              available_invitem_grid_data_xml += '</InventoryItem>';
          }
          
          available_invitem_grid_data_xml += '</InventoryItems>';
          system.debug('--available_invitem_grid_data_xml--'+available_invitem_grid_data_xml);
       }catch(Exception e){
          system.debug('--exception--'+e);
       }
    }
    
    public void getdataforAssignedInvItemGrid(){
       try{
          
          system.debug('--getdataforAssignedInvItemGrid Called--');
          string employee = ApexPages.currentPage().getParameters().get('employee');
          string location = ApexPages.currentPage().getParameters().get('location');
          
          system.debug('--employee--'+employee);
          system.debug('--location--'+location);
          
          if(location == null || location == '' || location == '0'){
             return;
          }
          
          String query = 'select id,name,Employee__c,Employee__r.Name,Product__c,Product__r.Name,Status__c,Serial_Number__c,Stock_Number__c,Warehouse__r.Name from Inventory_Item__c where status__c = \'Assigned\' and Employee__r.Location__c = \''+location+'\'' ;
          
          if(employee != null && employee != '' && employee != 'All'){
             query += ' and Employee__c = \''+employee+'\'';
          }
          
          List<Inventory_Item__c> invitemlist = Database.Query(query);
          system.debug('--invitemlist--'+invitemlist.size());
          assigned_invitem_grid_data_xml = '';
          
          assigned_invitem_grid_data_xml += '<InventoryItems>';
          for(Inventory_Item__c invitem : invitemlist){
              assigned_invitem_grid_data_xml += '<InventoryItem>';
              assigned_invitem_grid_data_xml += '<Id><![CDATA['+invitem.Id+']]></Id>';
              assigned_invitem_grid_data_xml += '<Name><![CDATA['+invitem.Name+']]></Name>';
              assigned_invitem_grid_data_xml += '<Product><![CDATA['+invitem.Product__r.Name+']]></Product>';
              //assigned_invitem_grid_data_xml += '<Warehouse><![CDATA['+invitem.Inventory_Item__r.Warehouse__r.Name+']]></Warehouse>';
             // assigned_invitem_grid_data_xml += '<StockInventory><![CDATA['+invitem.Inventory_Item__r.Stock_Number__c+']]></StockInventory>';
              assigned_invitem_grid_data_xml += '<SerialNumber><![CDATA['+invitem.Serial_Number__c+']]></SerialNumber>';
              assigned_invitem_grid_data_xml += '<Status><![CDATA['+invitem.Status__c+']]></Status>';
              assigned_invitem_grid_data_xml += '<Employee><![CDATA['+invitem.Employee__r.Name+']]></Employee>';
              assigned_invitem_grid_data_xml += '</InventoryItem>';
          }
          
          assigned_invitem_grid_data_xml += '</InventoryItems>';
          system.debug('--assigned_invitem_grid_data_xml--'+assigned_invitem_grid_data_xml);
       }catch(Exception e){
          system.debug('--exception--'+e);
       }
    }
    
    public void getsummarydataforAvailableGrid(){
       try{
          system.debug('--getsummarydataforAvailableGrid Called--');
          string warehouse = ApexPages.currentPage().getParameters().get('warehouse');
          string product   = ApexPages.currentPage().getParameters().get('product');
          string location = ApexPages.currentPage().getParameters().get('location');
          
          system.debug('--warehouse--'+warehouse);
          system.debug('--product--'+product);
          system.debug('--location--'+location);
          
          if(location == null || location == '' || location == '0'){
             return;
          }
          
          String productids = '';
          
          if(product != 'All'){
             List<String> commasplit = product.split(',');
             
             for(String prd : commasplit){
               if(prd != 'All'){
                 productids += '\''+prd+'\',';     
               }
            }
            
            if(productids.length() > 0){
               productids = '('+productids.subString(0,productids.length() - 1)+')';
            }
          }
          
          String query = 'select count(id) t,Product__r.Name prd,Warehouse__r.Name warehouse from Inventory_Item__c where status__c = \'Available\' and Warehouse__r.Location__c = \''+location+'\'' ;
          
          if(warehouse != null && warehouse != '' && warehouse != 'All'){
             query += ' and Warehouse__r.Id = \''+warehouse+'\'';
          }
          if(product != null && product != '' && product != 'All'){
             query += ' and Product__r.Id in '+productids;
          }
          
          query += ' group by Warehouse__r.Name,Product__r.Name';
          system.debug('--query--'+query);
          
          List<AggregateResult> results = Database.Query(query);
          system.debug('--results--'+results.size());
          
          available_invitem_grid_summarydata_xml = '';
          
          available_invitem_grid_summarydata_xml += '<InventoryItems>';
          for(AggregateResult result : results){
              available_invitem_grid_summarydata_xml += '<InventoryItem>';
              available_invitem_grid_summarydata_xml += '<Product><![CDATA['+result.get('prd')+']]></Product>';
              available_invitem_grid_summarydata_xml += '<Warehouse><![CDATA['+result.get('warehouse')+']]></Warehouse>';
              available_invitem_grid_summarydata_xml += '<Count><![CDATA['+result.get('t')+']]></Count>';
              available_invitem_grid_summarydata_xml += '</InventoryItem>';
          }
          
          available_invitem_grid_summarydata_xml += '</InventoryItems>';
          system.debug('--available_invitem_grid_summarydata_xml--'+available_invitem_grid_summarydata_xml);
       }catch(Exception e){
          system.debug('--exception--'+e);
       }
    }
    
    public void getsummarydataforAssignedGrid(){
       try{
           system.debug('--getsummarydataforAssignedGrid Called--');
          string employee = ApexPages.currentPage().getParameters().get('employee');
          string location = ApexPages.currentPage().getParameters().get('location');
          
          system.debug('--employee--'+employee);
          system.debug('--location--'+location);
          
          if(location == null || location == '' || location == '0'){
             return;
          }
          
          String query = 'select count(id) t,Employee__r.Name emp,Product__r.Name prd from Inventory_Item__c where status__c = \'Assigned\' and Employee__r.Location__c = \''+location+'\'' ;
          
          if(employee != null && employee != '' && employee != 'All'){
             query += ' and Employee__c = \''+employee+'\'';
          }
          
          query += ' group by Employee__r.Name,Product__r.Name';
          system.debug('--query--'+query);
          
          List<AggregateResult> results = Database.Query(query);
          system.debug('--results--'+results.size());
          
          assigned_invitem_grid_summarydata_xml = '';
          
          assigned_invitem_grid_summarydata_xml += '<InventoryItems>';
          for(AggregateResult result : results){
              assigned_invitem_grid_summarydata_xml += '<InventoryItem>';
              assigned_invitem_grid_summarydata_xml += '<Product><![CDATA['+result.get('prd')+']]></Product>';
              assigned_invitem_grid_summarydata_xml += '<Employee><![CDATA['+result.get('emp')+']]></Employee>';
              assigned_invitem_grid_summarydata_xml += '<Count><![CDATA['+result.get('t')+']]></Count>';
              assigned_invitem_grid_summarydata_xml += '</InventoryItem>';
          }
          
          assigned_invitem_grid_summarydata_xml += '</InventoryItems>';
          system.debug('--assigned_invitem_grid_summarydata_xml--'+assigned_invitem_grid_summarydata_xml);
       }catch(Exception e){
          system.debug('--exception--'+e);
       }
    }
}