@isTest
Public class EligibilityCheckHelperModelTest
{
    @isTest
    public static void runTest()
    {
        Account acc = Datagenerator.createAccount();
        insert acc;
        
        Premise__c prem = Datagenerator.CreatePremise();
        insert prem;
        
        customer__c cust  = Datagenerator.createCustomer(acc.Id,prem.Id);
        insert cust;
        
        Call_List_Line_Item__c cli = Datagenerator.createLineItem(cust.Id);
       // insert cliObj;
        Program__c pro =new Program__c();
        pro.GL_String__c='test';
        insert pro;
        
        EligibilityCheckHelperModel echm = new EligibilityCheckHelperModel(cli.Id);
        
        echm.loadCustomer(cli.Id);
        echm.initEligibilityCheck();
        echm.loadContractors(pro.id);
    }
}