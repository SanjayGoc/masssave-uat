public class ViewDSMTrackerContactOnUserCntrl{
Public String Uid{get;set;}
Public User UserList{get;set;}
Public List<DSMTracker_contact__c> DSMTList{get;set;}

    Public ViewDSMTrackerContactOnUserCntrl(){
        Uid=Apexpages.currentPage().getParameters().get('Uid');
        UserList=[select id from User Where Id =: Uid];
        if(UserList!= null){
            DSMTList =[select id,Name,Portal_Role__c,Portal_User__c,Title__c,DSMT_Contact_Number__c from DSMTracker_Contact__c where Portal_User__c =: UserList.id];
        }
    }
}