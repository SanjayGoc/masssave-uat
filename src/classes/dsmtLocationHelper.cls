public class dsmtLocationHelper{

public static Map<String,String> locMap = new Map<String,String>();


    public static String findLocationSpace(String locn) {
        locMap.put('Attic','Attic');
        locMap.put('Basement','Foundation');
        locMap.put('Bathroom','Interior');
        locMap.put('Bedroom','Interior');
        locMap.put('Crawlspace','Foundation');
        locMap.put('Dining Room','Interior');
        locMap.put('Exterior','Exterior');
        locMap.put('Family Room','Interior');
        locMap.put('Foyer','Exterior');
        locMap.put('Garage','Exterior');
        locMap.put('Great Room','Interior');
        locMap.put('Hallway','Interior');
        locMap.put('Kitchen','Interior');
        locMap.put('Laundry Room','Interior');
        locMap.put('Living Room','Interior');
        locMap.put('Living Space','Interior');
        locMap.put('Living Space Closet','Interior');
        locMap.put('Master Bedroom','Interior');
        locMap.put('Utility Room','Interior');
        
        return locMap.get(locn);
        
    }
    
    public static Decimal getWeatherStTemp(Id wsId,String WsProperty,Decimal temp) {
         Map<Decimal,Decimal> tempValStartMap = new Map<Decimal,Decimal>();
        List <WeatherStationTemprature__c> wsTemp = [select TempIncr__c,TempStart__c,TempValues__c,WeatherStation__c,WSProperty__c from WeatherStationTemprature__c where WeatherStation__c =: wsId and WSProperty__c  =: WsProperty];
        if(wsTemp != null && wsTemp.size() > 0) {
        
            List<String> tmpValues = wsTemp.get(0).TempValues__c.split(',');
            Decimal startTemp = wsTemp.get(0).TempStart__c;
            Decimal tempIncr = wsTemp.get(0).TempIncr__c;
            
            for(String tmpValue : tmpValues) {
                tempValStartMap.put(startTemp,Decimal.valueOf(tmpValue));
                startTemp = startTemp + tempIncr; 
            }
            
        }
        
        
       
        Integer t1 = temp.intValue();
        Integer t2 = t1 + 1;
        Decimal retTempstart;
        
       
        
        if(tempValStartMap.get(t1) != null && tempValStartMap.get(t2) != null) {
            return retTempstart = tempValStartMap.get(t1) + (tempValStartMap.get(t2) - tempValStartMap.get(t1)) * (temp - t1);
        } else {
            List<Decimal> sortedKeysList = getSortedKeyset(tempValStartMap);
            if(sortedKeysList.size() > 0)
            {
                if(temp < tempValStartMap.get(sortedKeysList.get(0))) {
                  return   extrapolatedown(tempValStartMap.get(sortedKeysList.get(0)),tempValStartMap.get(sortedKeysList.get(1)),sortedKeysList.get(0),temp);
                }
                
                if(temp > tempValStartMap.get(sortedKeysList.get(0))) {
                  return extrapolateUp(tempValStartMap.get(sortedKeysList.get(sortedKeysList.size()-2)),tempValStartMap.get(sortedKeysList.get(sortedKeysList.size()-1)),sortedKeysList.get(sortedKeysList.size()-1),temp);
                }
            } 
        }
        return 0.0;
        //return tempValStartMap ;
    }
    
    public static Decimal getWeatherStTemp(List <WeatherStationTemprature__c> wsTempList,String WsProperty,Decimal temp) 
    {
         Map<Decimal,Decimal> tempValStartMap = new Map<Decimal,Decimal>();
         List <WeatherStationTemprature__c> wsTemp = new List <WeatherStationTemprature__c>();
         //[select TempIncr__c,TempStart__c,TempValues__c,WeatherStation__c,WSProperty__c from WeatherStationTemprature__c 
         //where WeatherStation__c =: wsId and WSProperty__c  =: WsProperty];
         
        if(wsTempList !=null && wsTempList.size() >0)
        {
            for(WeatherStationTemprature__c w : wsTempList)
            {
                
                
                if(w.WSProperty__c == WSProperty)
                {
                    wsTemp.add(w);
                }
            }
        }
        
      
        
        if(wsTemp != null && wsTemp.size() > 0) {
        
            List<String> tmpValues = wsTemp.get(0).TempValues__c.split(',');
            Decimal startTemp = wsTemp.get(0).TempStart__c;
            Decimal tempIncr = wsTemp.get(0).TempIncr__c;
            
          
            
            for(String tmpValue : tmpValues) {
                tempValStartMap.put(startTemp,Decimal.valueOf(tmpValue));
                startTemp = startTemp + tempIncr; 
            }

        }
        
        
       
        Integer t1 = temp.intValue();
        Integer t2 = t1 + 1;
        Decimal retTempstart;
        
       
        
        if(tempValStartMap.get(t1) != null && tempValStartMap.get(t2) != null) {
            return retTempstart = tempValStartMap.get(t1) + (tempValStartMap.get(t2) - tempValStartMap.get(t1)) * (temp - t1);
        } else {
            
            List<Decimal> sortedKeysList = getSortedKeyset(tempValStartMap);
            
          
            
            if(temp < tempValStartMap.get(sortedKeysList.get(0))) {
              return   extrapolatedown(tempValStartMap.get(sortedKeysList.get(0)),tempValStartMap.get(sortedKeysList.get(1)),sortedKeysList.get(0),temp);
            }
            
            if(temp > tempValStartMap.get(sortedKeysList.get(0))) {
              return extrapolateUp(tempValStartMap.get(sortedKeysList.get(sortedKeysList.size()-2)),tempValStartMap.get(sortedKeysList.get(sortedKeysList.size()-1)),sortedKeysList.get(sortedKeysList.size()-1),temp);
            }
            
            
        }
        return 0.0;
        //return tempValStartMap ;
    }
    
    public Decimal getFractionDays(String fracType,Id wsId,Id EAId,String calcFor) {
       
       Decimal retVal;
       if(fracType.equalsIgnoreCase('H')) {
          if(calcFor == 'Tset'){
              retVal = getWeatherStTemp( wsId, 'HH',computeTestH(EAId) - 14.00) / 8766.00;
          }
          else if(calcFor == 'Room'){
              retVal = getWeatherStTemp( wsId, 'HH',68.00 - 14.00) / 8766.00;
          }
          
       } else if(fracType.equalsIgnoreCase('C')) {
           if(calcFor == 'Tset'){
               retVal = getWeatherStTemp( wsId, 'CH',computeTestC(EAId) - 10.00) / 8766.00;
           }
           else if(calcFor == 'Room'){
               retVal = getWeatherStTemp( wsId, 'CH',76.00 - 10.00) / 8766.00;
           }
           
       }
       if(retVal > 1) {
           retVal = 1;
       }
       if(retVal <0) {
           retVal = 0;
       }
       
       return retVal; 
    }
    
    
    
    public Decimal computeTestH(Id EAId){
        return 0;
    }
    
    public Decimal computeTestC(Id EAId){
        
       return 0;
    }
    
    public static List<Decimal> getSortedKeyset(Map<Decimal,Decimal> dataMap) {
                List<Decimal> keySetList = new List<Decimal>();
                keySetList.addAll(dataMap.keySet());
                keySetList.sort();
                return keySetList;
    }
    
    public static decimal extrapolatedown(Decimal val0,Decimal val1,Decimal key1, Decimal temp) {
        Decimal  valdiff = val1 - val0;
         
        
        // return value
        return Math.Max( 0.0, val0 - (key1 - temp) * valdiff );
    
    }
    
    public static decimal extrapolateUp(Decimal val1,Decimal val2,Decimal key1, Decimal temp) {
        Decimal  valdiff = val1 - val2;
        // return value
        return Math.Max( 0.0, val2 + (temp - key1) * valdiff );
    }
    
   
    
    public Decimal adjustCoolingIndoorTemp(decimal setTemp,String coolingUsage) {
        decimal retVal = null;
        if(setTemp == null) {
            Map<String,Double> custUsageMap = new Map<String,Double>();
            
            List<CoolingSystemUsage__c> custUsageList = [select id,name,value__c from CoolingSystemUsage__c];
            
            for(CoolingSystemUsage__c cu : custUsageList){
                custUsageMap.put(cu.Name,cu.value__c);
            }
            
            if(String.isBlank(coolingUsage) || coolingUsage.equals('Never')) {
                retVal = custUsageMap.get('Never');
            } else {
                retVal = setTemp + custUsageMap.get(coolingUsage);
            }
        }
        
        return retVal;
    }
    
    
    
   
    
    /** NEW CHANGES STARTED FOR SPACE**/


public static decimal ComputeAtticTemprature (string spaceType, string tempType, Id EAId, Id WsID, string locationName)
{
    return 0.0;
}



public static decimal ComputeOtherTemprature (string spaceType, string tempType, Id EAId, Id WsID, string locationName)
{
    return 0.0;
}

public static decimal ComputeBelowGradeTemprature (string spaceType, string tempType, Id EAId, Id WsID, string locationName)
{
    return 0.0;
}

public static decimal ComputeExteriorTemprature (string spaceType, string tempType, Id EAId, Id WsID, string locationName)
{
    return 0.0;
}

public static decimal ComputeInteriorTemprature (string spaceType, string tempType, Id EAId, Id WsID, string locationName)
{
    return 0.0;
}

public static Boolean ComputeSpaceConditoned(Id EAId)
{
    /*boolean isCondtn = false;
    List<String> mechHeatCoolTypes = new List<String>();
    mechHeatCoolTypes.add('Heating');
    mechHeatCoolTypes.add('Heating+Cooling');
    mechHeatCoolTypes.add('Heating+DHW');
    mechHeatCoolTypes.add('Cooling');
                  
    List<Mechanical_Type__c> mechList = [select id,name from Mechanical_type__c where Mechanical_System__c in : mechHeatCoolTypes and Energy_Assessment__c =: eaId];
    Set<Id> mechIds = new Set<Id>();
    
    if(mechList != null) {
        for(Mechanical_Type__c mechType : mechList) {
            mechIds.add(mechType.Id);
        }
        
        List<Mechanical_Sub_Type__c> mechsubCondnList = [select id,NormalizedDistributionFractionH__c,NormalizedLoadFraction__c,HeatingIndoorTempUsed__c,NormalizedDistributionFractionC__c,NormalizedDistributionFractionSh__c,CoolingIndoorTempUsed__c from Mechanical_Sub_Type__c where Mechanical_Type__C in : mechIds];
        if(mechsubCondnList != null) {
            isCondtn = true;
        }
    }             
    
    return isCondtn;*/
    
    List<Thermal_Envelope_Type__c> lstTET = [select Id from Thermal_Envelope_Type__c where Energy_Assessment__c = :EAId AND (RecordType.Name = 'Attic' OR RecordType.Name = 'Basement') AND (SpaceConditioning__c = 'Conditioned' OR SpaceConditioning__c = 'Vented')];
    return false;
}

public static boolean IsBasementInsulated(Id EAId)
{
    /// logic for ehcking if basement is insulated or not
     
    return false;
}

public static boolean UseBasementCeilingInsulation(Id EAId)
{
    /// logic for ehcking if basement celling is insulated or not
    return false;
}

public static decimal findSolarGain(Id WsId,String Orienation){
    List<SolarGainByOperation__c> SolarGainByOperationLst = [select id,SolarGain__c from SolarGainByOperation__c 
                                                                where WeatherStation__c =: wsId and Orientation__c =: Orienation order by SolarGain__c];
    
    if(SolarGainByOperationLst  != null && SolarGainByOperationLst.size() > 0)
    {    
        return SolarGainByOperationLst.get(0).SolarGain__c;
    }
    return 0;
}

public static decimal computeTRefH(){
    Decimal retval = 55.7820513;
    return retval;
}

public static decimal computeTRefC(){
    Decimal retval = 78.6933962;
    return retval;
}

public static decimal THouseH(){
    Decimal retval = 55.7820513;
    return retval;
}

public static decimal THouseC(){
    Decimal retval = 78.6933962;
    return retval;
}

public static decimal findSolarGainH(Id WsId,String Orienation){
    List<SolarGainByOperation__c> SolarGainByOperationLst = [select id,SolarGain__c from SolarGainByOperation__c 
                                                                where WeatherStation__c =: wsId and Orientation__c =: Orienation order by SolarGain__c desc];
    
    if(SolarGainByOperationLst  != null && SolarGainByOperationLst.size() > 0){
       
        return SolarGainByOperationLst.get(0).SolarGain__c;
    }
    return 0;
}


/** NEW CHANGES END  FOR SPACE**/

}