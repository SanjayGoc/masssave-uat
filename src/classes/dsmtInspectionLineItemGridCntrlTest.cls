@istest
public class dsmtInspectionLineItemGridCntrlTest 
{
    
    @istest
    static void runtest()
    {
        
        Inspection_Request__c   ir =new Inspection_Request__c   ();
        insert ir;
        
       
        Inspection_Line_item__c  ili =new Inspection_Line_item__c ();
        ili.Inspection_Request__c =ir.id;
        ili.Description__c='5';
        ili.Spec_d_Quantity__c=5;
        ili.Change_Order_Quantity__c=10;
        insert ili;  
        
               
        ApexPages.currentPage().getParameters().put('irId',ili.id); 
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ir);
        dsmtInspectionLineItemGridCntrl cntrl1 = new dsmtInspectionLineItemGridCntrl(sc);       
        dsmtInspectionLineItemGridCntrl cntrl = new dsmtInspectionLineItemGridCntrl();
        
        cntrl.deleteItemId = ili.Id;
        
        cntrl.SaveInspectionLineItem();
        cntrl.reloadInspectionLineItem();
        
        cntrl.deleteInspectionLineItem();
    }
}