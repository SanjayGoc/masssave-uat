@IsTest
public with sharing class SiteRegisterControllerTest 
{
    @IsTest 
    static void testRegistration()
    {
        SiteRegisterController src = new SiteRegisterController();
        src.username = 'test@force.com';
        src.email = 'test@force.com';
        src.communityNickname = 'test';
       
        System.assert(src.registerUser() == null);    
        
        src.password = 'abcd1234';
        src.confirmPassword = 'abcd123';
        System.assert(src.registerUser() == null);
    }
}