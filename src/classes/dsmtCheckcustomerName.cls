global class dsmtCheckcustomerName implements Schedulable,database.batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String custId = customer__c.SObjectType.getDescribe().getKeyPrefix(); 

               // Date dt = Date.Today().Adddays(-1);
        String query = 'SELECT Id,Name,First_Name__c,Last_Name__c from Customer__c where Name like \''+'%'+custId+'%'+'\'';
        system.debug('--query---'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Customer__c> scope)
    {
        
        List<Customer__c> woToupdate = new List<Customer__c>();
        
        for (Customer__c  w : scope)
        {
            w.Name = w.First_Name__c + ' '+w.Last_Name__c;
            woToupdate.add(w);
        }
        if(woToupdate.size() > 0){
            update woToupdate;
        }
          
    }  
    global void finish(Database.BatchableContext BC)
    {
    
    }
    
    global void  execute(SchedulableContext sc){
        dsmtCheckcustomerName obj = new dsmtCheckcustomerName();
        database.executebatch(obj,200);    
    }
}