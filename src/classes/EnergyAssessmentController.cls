public class EnergyAssessmentController {
    public Set<String> getStylesheetUrls(){
        return new Set<String>{
              'https://www.patternfly.org/angular-patternfly/css/animations.css',
                'https://www.patternfly.org/angular-patternfly/css/jquery.dataTables.css',
                'https://www.patternfly.org/angular-patternfly/css/patternfly.css',
                'https://www.patternfly.org/angular-patternfly/css/patternfly-additions.css',
                'https://www.patternfly.org/angular-patternfly/css/angular-patternfly.css',
                'https://www.patternfly.org/angular-patternfly/css/patternfly-showcase.css',
                'https://www.patternfly.org/angular-patternfly/css/ng-docs.css',
                'https://www.patternfly.org/angular-patternfly/css/examples.css'
        };
    }
    public Set<String> getScriptUrls(){
        return new Set<String>{
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/jquery.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/bootstrap.min.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/bootstrap-select.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/jquery-ui.min.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/jquery.dataTables.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/dataTables.select.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/moment.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/d3.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/c3.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/patternfly-settings.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/patternfly-settings-colors.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/patternfly-settings-charts.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-dragdrop.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-datatables.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-datatables.select.min.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-sanitize.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-animate.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/ui-bootstrap-tpls.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-bootstrap-prettify.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/lodash.min.js',
                    //'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-patternfly.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-ui-router.min.js',
                    'https://www.patternfly.org/angular-patternfly/grunt-scripts/angular-drag-and-drop-lists.js'
        };
    }
    public List<String> getStaticResourceScripts(){
        return new List<String>{
            'ngEnergyAssessmentApp'
        };
    }
    public List<String> getStaticResourceStyles(){
        return new List<String>{
            'ngEnergyAssessmentSidebar'
        };
    }
    public Set<String> getAngularModules(){
        return new Set<String>{
              'ui.bootstrap','bootstrapPrettify','patternfly','ui.router'
        };
    }
}