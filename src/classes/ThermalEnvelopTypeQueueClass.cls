public class ThermalEnvelopTypeQueueClass implements Queueable {
   
    Id tEId;
    String ObjectName;
    String Type;
  
    public ThermalEnvelopTypeQueueClass (Id roId,String objName,String Typ) {
        tEId = roID;
        ObjectName = ObjName;
        Type = Type;
    }
    public void execute(QueueableContext context) {
        if(ObjectName == 'Floor'){
             Floor__c floorObj = new Floor__c();
             floorObj.Name = Type;
             floorObj.Thermal_Envelope_Type__c = TEId;
             insert floorObj;
        }else if(objectName == 'Ceiling'){
             Ceiling__c CeilObj = new Ceiling__c();
             CeilObj.Name = Type;
             CeilObj.Thermal_Envelope_Type__c  = tEId;
             insert CeilObj ;
        }else if(objectName == 'Wall'){
             Wall__c wallObj = new Wall__c ();
             wallObj.Name = Type;
             wallObj.Thermal_Envelope_Type__c  = tEId;
             insert wallObj;
        }
    }
}