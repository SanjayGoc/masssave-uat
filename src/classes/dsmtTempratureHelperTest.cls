@isTest
private class dsmtTempratureHelperTest 
{
	
	@testsetup static void SetupEnergyAssessment()
    {
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
		/*Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = testEA.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = testEA.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		insert mechType;

		Mechanical__c mechHotWater = new Mechanical__c();		
		mechHotWater.Energy_Assessment__c = testEA.Id;
		mechHotWater.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Hot Water').getRecordTypeId();
		insert mechHotWater;

		Mechanical_Type__c mechHotWaterType = new Mechanical_Type__c();
		mechHotWaterType.Energy_Assessment__c = testEA.Id;
		mechHotWaterType.Mechanical__c = mechHotWater.Id;
		mechHotWaterType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		insert mechHotWaterType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mechHotWater.Id;
		mechSubType.Mechanical_Type__c = mechHotWaterType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		mstList.add(mechSubType);

		insert mstList;*/


    }

    @isTest static void TempratureHelperTest1()
    {
    	Set<Id> eaId = new Set<Id>();
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
    	eaId.add(eaList[0].Id);

    	Test.startTest();

    	List<Appliance__c> aplList = new List<Appliance__c>();
		Appliance__c apl = New Appliance__c();

		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Refrigeration').getRecordTypeId();
		apl.Type__c = 'Refrigeration';
		apl.Name = 'Refrigeration 1';
		apl.Energy_Assessment__c = eaList[0].Id;

		aplList.add(apl);

		apl = New Appliance__c();

		apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Washer').getRecordTypeId();
		apl.Type__c = 'Washer';
		apl.Name = 'Washer 1';
		apl.Energy_Assessment__c = eaList[0].Id;

		aplList.add(apl);

		insert aplList;

    	dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
    	dsmtTempratureHelper.ComputeTemprature(bp);

    	Test.stopTest();
    }

    @isTest static void TempratureHelperTest2()
    {
    	Set<Id> eaId = new Set<Id>();
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
    	eaId.add(eaList[0].Id);

    	Test.startTest();

    	List<Water_Fixture__c> waterList = new List<Water_Fixture__c>();    	
		Water_Fixture__c wf = new Water_Fixture__c();

		wf.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Sink').getRecordTypeId();
		wf.Type__c = 'Sink';
		wf.Name = 'Sink 1';
		wf.Energy_Assessment__c = eaList[0].Id;

		waterList.add(wf);

		wf = new Water_Fixture__c();

		wf.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Bath').getRecordTypeId();
		wf.Type__c = 'Bath';
		wf.Name = 'Bath 1';
		wf.Energy_Assessment__c = eaList[0].Id;

		waterList.add(wf);

		insert waterList;

    	dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
    	dsmtTempratureHelper.ComputeTemprature(bp);

    	Test.stopTest();
    }

    @isTest static void TempratureHelperTest3()
    {
    	Set<Id> eaId = new Set<Id>();
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
    	eaId.add(eaList[0].Id);

    	Test.startTest();

    	/// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = eaList[0].Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = eaList[0].Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		insert mechType;		

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		insert mstList;

    	dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
    	dsmtTempratureHelper.ComputeTemprature(bp);

    	Test.stopTest();
    }

    @isTest static void TempratureHelperTest4()
    {
    	Set<Id> eaId = new Set<Id>();
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
    	eaId.add(eaList[0].Id);

    	Thermal_Envelope__c te = new Thermal_Envelope__c();
    	te.Energy_Assessment__c = eaList[0].Id;
    	insert te;

    	Test.startTest();

    	List<Thermal_Envelope_Type__c> tetList = new List<Thermal_Envelope_Type__c>();
		Thermal_Envelope_Type__c tet = New Thermal_Envelope_Type__c();

		tet.RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Attic').getRecordTypeId();		
		tet.Name = 'Attic';
		tet.Thermal_Envelope__c = te.Id;
		tet.Energy_Assessment__c = eaList[0].Id;

		tetList.add(tet);

		insert tetList;

    	dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
    	dsmtTempratureHelper.ComputeTemprature(bp);
    	
    	dsmtTempratureHelper.extrapolatedown(5,88,44, 68);
    	dsmtTempratureHelper.extrapolateup(5,88,44, 68);

    	dsmtTempratureHelper.getLightFactor(bp,'IntensH');
    	dsmtTempratureHelper.getLightFactor(bp,'IntensC');
    	dsmtTempratureHelper.getLightFactor(bp,'IntensSh');

    	dsmtTempratureHelper.ComputeTExtC(bp);
    	dsmtTempratureHelper.computeTExtH(bp);
    	dsmtTempratureHelper.ComputeMinTRefCommon(bp,'H');
    	dsmtTempratureHelper.GetHL_Dictionary(bp);

    	Map<Decimal, Decimal> tempMap = new Map<Decimal,Decimal>
    	{
    		5=>45,
    		8=>49,
    		2=>12,
    		9=>11,
    		3=>19
    	};
    	dsmtTempratureHelper.getSortedKeyset(tempMap);

    	dsmtTempratureHelper.AdjustCoolingIndoorTemp(45,'Never');
    	dsmtTempratureHelper.AdjustCoolingIndoorTemp(45,'Rarely');

    	dsmtTempratureHelper.ComputeSpaceTempCommon(bp,'H','Basement');
    	dsmtTempratureHelper.ComputeSpaceTempCommon(bp,'C','Basement');

    	dsmtTempratureHelper.computeTAtticH(bp,0,1,1,1);
    	dsmtTempratureHelper.computeTAtticC(bp,0,1,1,1);

    	dsmtTempratureHelper.ComputeIndoorGainOtherCommon(bp,'C');
    	dsmtTempratureHelper.ComputeIndoorGainOtherCommon(bp,'H');
    	dsmtTempratureHelper.ComputeIndoorGainOtherCommon(bp,'CLat');

    	dsmtTempratureHelper.ComputeIndoorGainGasCommon(bp,'C');
    	dsmtTempratureHelper.ComputeIndoorGainGasCommon(bp,'H');
    	dsmtTempratureHelper.ComputeIndoorGainGasCommon(bp,'CLat');

    	dsmtTempratureHelper.ComputeIndoorGainCommon(bp,'C');
    	dsmtTempratureHelper.ComputeIndoorGainCommon(bp,'H');
    	dsmtTempratureHelper.ComputeIndoorGainCommon(bp,'CLat');

    	dsmtTempratureHelper.ComputeSpaceTempCommon(bp,'H','Exterior');
    	dsmtTempratureHelper.ComputeSpaceTempCommon(bp,'C','Exterior');

    	 dsmtEnergyConsumptionHelper.ComputeHandDishwashingIndoorGainCommon(bp,'H');
    	 dsmtEnergyConsumptionHelper.ComputeHandDishwashingIndoorGainCommon(bp,'C');
    	 dsmtEnergyConsumptionHelper.ComputeHandDishwashingIndoorGainCommon(bp,'CLat');



    	Test.stopTest();
    }

    @isTest static void TempratureHelperTest5()
    {
    	Set<Id> eaId = new Set<Id>();
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
    	eaId.add(eaList[0].Id);

    	Test.startTest();

    	/// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = eaList[0].Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = eaList[0].Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();
		insert mechType;		

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		insert mstList;

    	dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
    	dsmtTempratureHelper.ComputeTemprature(bp);

    	Test.stopTest();
    }

    @isTest static void TempratureHelperTest6()
    {
    	Set<Id> eaId = new Set<Id>();
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];
    	eaId.add(eaList[0].Id);

    	Test.startTest();

    	/// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = eaList[0].Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Hot Water').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = eaList[0].Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		insert mechType;		

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = eaList[0].Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		mstList.add(mechSubType);

		insert mstList;

    	dsmtEAModel.SUrface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
    	dsmtTempratureHelper.ComputeTemprature(bp);

    	Test.stopTest();
    }
	
}