public class dsmtApplianceRecommController {
    public string  eassid {get;set;}
    public string applianceId {get;set;}
    public string applianceTypeId {get;set;}
    public string parentId {get;set;}
    public string parentField {get;set;}
    public String RecommendationId{get;set;}
    public String recordTypeName {get;set;}
    public String recordType {get;set;}
    public String parentRecordTypeName {get;set;}
    public Recommendation__c recommendation {get;set;}
    public String addOn {get;set;}
    public Appliance__c appliance {get;set;}
    public list<Recommendation__c> applianceRecom {get;set;}
    
    public dsmtApplianceRecommController() {
        recommendation = new Recommendation__c();
        recommendation.Status_Code__c = 'Recommended';
        applianceRecom = new list<Recommendation__c>();
        eassId = Apexpages.currentPage().getParameters().get('assessId');
    }
    
    public String getAssignDefaultValues()
    {
        appliance = new Appliance__c();
        applianceRecom = fetchapplianceRecom();
        return null;
    }
    
    public list<Recommendation__c> fetchapplianceRecom(){
        return [select id, Status__c, Operation__c, Status_Code__c, Part__c, Description__c, CreatedDate
                     from Recommendation__c
                     where Appliance__c =: parentId
                     and Appliance__c != null];
    }
    
    public void saveNewReccom() {
        
        recommendation.put(parentField, parentId);
        upsert recommendation;   
        
        appliance.Recommendation__c = recommendation.Id;
        
        upsert appliance;
        
        recommendation = new Recommendation__c();
        appliance = new Appliance__c();
        
        applianceRecom = fetchapplianceRecom();
    }
    
    public Pagereference DeleteRecommendation(){
        List<Recommendation__c> recList = [select id from Recommendation__c where id =: RecommendationId];
        
        if(recList != null && recList.size() > 0){
            delete recList;
        }
        return null;
    }
    
    public void editRecommendation()
    {
        string objectId = Apexpages.currentPage().getParameters().get('ReccomIdToEdit');
        if(objectId != null)
        {
            list<Recommendation__c> recList = Database.query('SELECT ' + getSObjectFields('Recommendation__c') + ',RecordType.Name,RecordType.DeveloperName FROM Recommendation__c WHERE Id =:objectId');
            if(recList.size() > 0)
            {
                recommendation = recList[0];
                String recomId = recommendation.Id;
                list<Appliance__c> applianceList = Database.query('SELECT ' + getSObjectFields('Appliance__c') + ',RecordType.Name,RecordType.DeveloperName FROM Appliance__c WHERE Recommendation__c =:recomId');
                if(applianceList.size() > 0)
                    appliance = applianceList[0];
            }
        }
    }
    
    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}