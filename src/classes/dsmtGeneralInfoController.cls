public class dsmtGeneralInfoController extends dsmtEnergyAssessmentBaseController {
    public List<Note> getNotes(){
        if(this.ea != null && this.ea.Id != null){
            return loadNotes(this.ea.Id);  
        }else{
            return new List<Note>();   
        }
    }
    public List<Attachment> getAttachments(){
        if(this.ea != null && this.ea.Id != null){
            return loadAttachments(this.ea.Id);  
        }else{
            return new List<Attachment>();   
        }
    }
    
    public String CustomerJSON{get;set;}
    public Account account{get;set;}
    public string accountSearchResultData {get;set;}
    public boolean showcustomerGrid{get;set;}
    public boolean IsInternalAccount{get;set;}
    
    public List<Selectoption> providerList{get;set;}
    Public string selectedProvider{get;set;}
    String customerid = ApexPages.currentPage().getParameters().get('custId');
    
    public dsmtGeneralInfoController(){
        showcustomerGrid = false;
        list<sobject> s;
        accountSearchResultData = JSON.serialize(s); 
        account = new Account();
        providerList = new List<SelectOption>();
        providerList.add(new selectoption('','--None--'));
        
        IsInternalAccount = false;
        String UserId = UserInfo.getUserId();
        List<User> usrList  = [select Id,user.profile.name from user Where Id =: UserId];
        
        if(usrList != null && usrList.size() > 0){

            User u = usrList.get(0);
            if(u.profile.name == 'System Administrator' || u.profile.name == 'DSMTracker IT Administrator') {
                IsInternalAccount = true;
            }
            List < DSMTracker_Contact__c > dsmtList = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Trade_Ally_Account__r.Internal_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c where Portal_User__c = : u.Id and Trade_Ally_Account__c != null limit 1];
            if(dsmtList != null && dsmtList.Size() > 0){
                IsInternalAccount = dsmtList.get(0).Trade_Ally_Account__r.Internal_Account__c;                
                System.debug('DSMT PROFILE '+ u.profile.name);
                if(IsInternalAccount && u.profile.name == 'CLR Energy Specialist'){
                    IsInternalAccount = true;
                }
            }
        }
        List<Customer__c> custList = [Select id,Zip_code__c from Customer__c where id =: customerid];
        if(custList.size() > 0){
            Customer__c c = custList.get(0);
            List<Program_Eligibility__c> ProgEligList = [Select id,Provider_Name__c,Zip_Code__c from Program_Eligibility__c where Zip_Code__c =: c.Zip_code__c and Provider_Name__c != null order by Provider_Name__c];
            
                For(Program_Eligibility__c pe : ProgEligList){
                    providerList.add(new selectoption(pe.Provider_Name__c,pe.Provider_Name__c));
                }
        } 
        
    }
    
 
    public void SearchCustomer(){
        showcustomerGrid = true;
        
        system.debug('--account.provider__c--'+account.provider__c);
        
        String AccFields = '';
         map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id'){
                 AccFields += dfield.getName() + ', ';
            }
        }
        
        String qry = 'select '+AccFields +' Id From Account where Id != null ';
        
        if(account.Provider__c != null && account.Provider__c != ''){
           qry += ' and provider__c = \''+account.provider__c+'\'';
        }
        if(account.Utility_Service_Type__c != null && account.Utility_Service_Type__c != ''){
           qry += ' and Utility_Service_Type__c = \''+account.Utility_Service_Type__c+'\''; 
        }
        if(account.Billing_Account_Number__c != null && account.Billing_Account_Number__c != ''){
           qry += ' and Billing_Account_Number__c = \''+account.Billing_Account_Number__c+'\''; 
        }
        
        qry += ' limit 200';
        system.debug('--qry--'+qry);
        List<Account> account_list  = database.Query(qry);
        
        system.debug('--account_list ---'+account_list );
        
        String ResField  = '';           
        
        list<SObject> searchResults = new list<SObject> ();
        searchResults = account_list;
        list<Object> data = new list<Object> ();
        for (SObject sObj: searchResults) {
            map<String, Object> dataRow = new map<String, Object> ();
            if( sObj.Id != null){
                dataRow.put('Id', sObj.Id);
            }
            for (Schema.FieldSetMember f: SObjectType.Account.FieldSets.Lookup_Account_Result_Fields.getFields()) {
                dataRow.put(f.fieldPath, sObj.get(f.fieldPath));
            }
            data.add(dataRow);
        }

        System.debug('QUERY RESULT COUNT :: ' + searchResults.size());
        accountSearchResultData = JSON.serialize(data);
        system.debug('--accountSearchResultData---'+accountSearchResultData);
    }

    public String selectedAccId{get;set;}
    public void replaceAccount(){
        
        //String customerid = ApexPages.currentPage().getParameters().get('custId');
        String ESSid = ApexPages.currentPage().getParameters().get('assessId');
        
        if(customerId != null){
            
            Customer__c cust = new Customer__c(id = customerId);
            
            if(selectedAccId != null){
               List<Account> acclist = [select id,Utility__c,Provider__c,Billing_Account_Number__c,Utility_Service_Type__c from Account where id =: selectedAccId];
               if(acclist.size() > 0){
                   
                   Account acc = acclist.get(0);
                   String type = acc.Utility_Service_Type__c;
                   if(type == 'Electric'){
                       cust.Electric_provider__c = acc.Utility__c;
                       cust.Electric_Provider_Name__c = acc.Provider__c;
                       cust.Electric_Account_Number__c = acc.Billing_Account_Number__c;
                       cust.Electric_Account__c = acc.id;
                   }else if(type == 'Gas'){
                       cust.Gas_provider__c = acc.Utility__c;
                       cust.Gas_Provider_Name__c = acc.Provider__c;
                       cust.Gas_Account_Number__c = acc.Billing_Account_Number__c;
                       cust.Gas_Account__c = acc.id;
                   }
                   
                   update Cust;
                   
                   List<Energy_Assessment__c> ealist = [select workorder__c,Appointment__r.Workorder__c,id from Energy_Assessment__c where id =: ESSId];
                   if(ealist.size() > 0){
                      Energy_Assessment__c ea = ealist.get(0);
                      /*if(ea.Appointment__r.Workorder__c != null){
                          WorkOrder__c wo = new WorkOrder__c(id = ea.Appointment__r.Workorder__c);
                          wo.Primary_Provider__c = null;
                          update wo;
                      }*/
                      
                      Energy_Assessment__c assessment = new Energy_Assessment__c(id = ESSId);
                      if(cust.Electric_provider__c != null){
                        assessment.Electric_Provider_Name__c = cust.Electric_Provider_Name__c ;   
                      }
                      if(cust.Electric_Account_Number__c != null){
                          assessment.Electric_Account_Number__c = cust.Electric_Account_Number__c;
                      }
                      if(cust.Gas_Provider_Name__c != null){
                          assessment.Gas_Provider_Name__c = cust.Gas_Provider_Name__c;
                      }
                      if(cust.Gas_Account_Number__c != null){
                          assessment.Gas_Account_Number__c = cust.Gas_Account_Number__c;
                      }
                      
                      //assessment.Primary_Provider__c = null;
                      update assessment;
                   }
                   
                   
               }
            }
        }
    }
    
        
    public SearchModel searchModel {
        get{
            if(searchModel == null){
                searchModel = new SearchModel();
            }
            return searchModel;
        }
        set;
    }
    
    public class SearchModel {
      
       public SearchModel() {
           
        }
        
      public list<Schema.FieldSetMember> sObjTypeSearchResultFields {
            get {
                return SObjectType.Account.FieldSets.Lookup_Account_Result_Fields.getFields();
            }
      }
     public String searchResultColumns {
        get {
            list<Object> columns = new list<Object> ();
            columns.add(new map<String, Object> {
                'text' => 'Action',
                'datafield' => 'Id',
                'width' => 170
                //'hidden' => true
            });
            
            for (Schema.FieldSetMember f: this.sObjTypeSearchResultFields) {
                if(f.label != 'Phone')
                    columns.add(new map<String, Object> {
                        'text' => f.label,
                        'datafield' => f.fieldPath
                    });
                else
                    columns.add(new map<String, Object> {
                        'text' => f.label,
                        'datafield' => f.fieldPath,
                        'width' => 100
                    });
            }
            return JSON.serialize(columns);
        }
    }
    }
}