public with sharing class SendApplicationToQueueExtn_SR {
    public Service_Request__c Revw{get;set;}
    
    public string selectedAction{get;set;}
    public string noteTitle{get;set;}
    public string noteBody{get;set;}
    public String selectedExcptions{set;get;}
    public string selectedStatus{get;set;}
    public string conts{get;set;}
    public string notesStr{get;set;}
    public boolean isDisplayForResubmit{get;set;}
    public List<Attachment__c> attchList{get;set;}
    private string stage;
    
    public string profileName = '';
    
    private final ApexPages.standardController theController;
    Date Schdate = null;
    
    public SendApplicationToQueueExtn_SR(ApexPages.StandardController controller){
        theController = controller;
        selectedAction = '';
        selectedExcptions = '';
        notesStr = '';
        
        Revw = new Service_Request__c();
        String ReId = controller.getId();
        List<Service_Request__c> lstRev = [select id,Name,Status__c from Service_Request__c WHERE Id=:ReId ];
                                    
        system.debug('-- Record Size -- '+lstRev.size());
        if(lstRev.size()>0){
            Revw = lstRev[0];
            
            
        }
        
        
       
        }
    
  
    
    public string FailReason{get;set;}
    
    public PageReference saveAndUpdate(){
        try{
            update revw;
            
             return new PageReference('/'+revw.id);
        }
        catch(Exception ex){
            string errmsg = ex.getMessage();
            if(errmsg.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
                errmsg = errmsg.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,')[1];
            }
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, errmsg);
            ApexPages.addMessage(msg);
            return null;
        }
         
    }
    
    public PageReference Cancel(){
        return new PageReference('/'+revw.id);
    }
    
}