@istest
public class dsmtApplianceRecommControllerTest 
{
	@istest
    static void runtest()
    {
        Recommendation__c rm =new Recommendation__c ();
        insert rm;
        
        Appliance__c app =new Appliance__c ();
        app.id=rm.id;
        insert app;
        
        dsmtApplianceRecommController darc =new dsmtApplianceRecommController ();
        darc.getAssignDefaultValues();
        darc.parentField=rm.AC_Other__c;
		darc.parentId=rm.Id;        
        darc.saveNewReccom();
        darc.editRecommendation();
        
    }
}