@istest
public class dsmtBarrierAUTest {
    @istest
    public static void dsmtBarrierAU(){
   
        Safety_Aspect__c sa = new Safety_Aspect__c();
        sa.Knob_Tube_Wiring_Addressed__c = true;
        sa.Dryer_Vent_Addressed__c = true;
        sa.High_CO_Addressed__c = true;
        insert sa;
        
        Energy_Assessment__c ea=Datagenerator.setupAssessment();
        ea.Safety_Aspect__c = sa.id;
        update ea; 
        test.startTest();
        Barrier__c br = new Barrier__c();
        br.Energy_Assessment__c = ea.id;
        br.Barrier_Cleared_K_T_W__c = true;
        br.Barrier_Cleared_Ventilation__c = true;
        br.Barrier_Cleared_Mech_Sys_Barr__c = true;
        insert br;
        br.Barrier_Cleared_K_T_W__c = false;
        br.Barrier_Cleared_Ventilation__c = false;
        br.Barrier_Cleared_Mech_Sys_Barr__c = false;
        update br;
 
        test.stopTest(); 
    }
}