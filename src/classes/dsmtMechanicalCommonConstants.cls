public class dsmtMechanicalCommonConstants
{
    public static map<String, decimal> CommonDuctLeakinessFactorTable()
    {
        return new map<String, decimal>{'Leaky' => .35,
                                        'Average' => .21,
                                        'Tight' => .065};
    }
    
    public static map<String, decimal> CommonSupplyDuctSegmentInsulationAmountRValue()
    {
        return new map<String, decimal>{'None' => 0,
                                        'Some' => 4,
                                        'Standard' => 6,
                                        'Lots' => 8,
                                        'Super' => 10,
                                        'Unknown' => 0};
    }
    
    public static map<String, decimal> CommonReturnDuctSegmentInsulationAmountRValue()
    {
        return new map<String, decimal>{'None' => 0,
                                        'Some' => 2.5,
                                        'Standard' => 6,
                                        'Lots' => 8,
                                        'Super' => 10,
                                        'Unknown' => 0};
    }
    
    public static map<String, decimal> CommonPipeInsulationAmountValue()
    {
        return new map<String, decimal>{'None' => 0,
                                        'Some' => 0.0001,
                                        'Lots' => 0.8};
    }
    
    public static map<String, decimal> BaseboardPipeInsulationAmount()
    {
        return new map<String, decimal>{'Lots' => 0.97,
                                        'Some' => 0.94,
                                        'None' => 0.92};
    }
    
    public static map<String, decimal> RadiatorsPipeInsulationAmount()
    {
        return new map<String, decimal>{'Lots' => 0.97,
                                        'Some' => 0.94,
                                        'None' => 0.92};
    }
    
    public static map<String, decimal> RadiantPipeInsulationAmount()
    {
        return new map<String, decimal>{'Lots' => 0.97,
                                        'Some' => 0.94,
                                        'None' => 0.92};
    }
    
    public static map<String, decimal> OnePipeSteamPipeInsulationAmount()
    {
        return new map<String, decimal>{'Lots' => 0.91,
                                        'Some' => 0.86,
                                        'None' => 0.82};
    }
    
    public static map<String, decimal> TwoPipeSteamPipeInsulationAmount()
    {
        return new map<String, decimal>{'Lots' => 0.91,
                                        'Some' => 0.86,
                                        'None' => 0.82};
    }
    
    public static map<String, decimal> GravityHotWaterPipeInsulationAmount()
    {
        return new map<String, decimal>{'Lots' => 0.91,
                                        'Some' => 0.86,
                                        'None' => 0.82};
    }
    
    public static map<String, decimal> CommonCoolingSystemUsageAddTemp()
    {
        return new map<String, decimal>{'Never' => 120,
                                        'Rarely' => 9,
                                        'Sometimes' => 6,
                                        'Often' => 3,
                                        'Normal' => 0};
    }
    
    public static map<String, decimal> CommonEfficiencyUnitConversionFactors()
    {
        return new map<String, decimal>{'AFUE' => 100,
                                        'CAE' => 1,
                                        'COP' => 1,
                                        'EER' => 3.412,
                                        'HSPF' => 3.412,
                                        'Percent' => 100,
                                        'SEER' => 3.412,
                                        'None' => 1};
    }
    
    public static map<String, decimal> CommonCapacityUnitConversionFactors()
    {
        return new map<String, decimal>{'kBtuPerHour' => .001,
                                        'BtuPerHour' => 1,
                                        'kW' => (.001/3.412),
                                        'None' => 1};
    }
}