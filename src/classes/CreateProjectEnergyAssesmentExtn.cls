public with sharing class CreateProjectEnergyAssesmentExtn {
    public Energy_Assessment__c Enas{get;set;}
    public list<Recommendation__c> recmajorlist{get;set;}
    public list<Recommendation__c> recdirectlist{get;set;}
    public string ProjectName{get;set;}
    public integer majorCount{get;set;}
    public integer directCount{get;set;}
    public string  eassid{get;set;}
    public string SelectedRecId{get;set;}
    public string projectId{get;set;}
    
    public String RecommendationId{get;set;}
    
    public string SelectedRecommendationType {get;set;}
    
    public addReccomModel  addReccomView {get;set;}
    
    public class addReccomModel {
        public string category {get;set;}
        public string subCategory {get;set;}
        public string eversourceDesc {get;set;}
        public string gridDesc {get;set;}
        public string location {get;set;}
        public string unit {get;set;}
        public string notes {get;set;}
        public string emHubPartId {get;set;}
    }
    
    public string error {get;set;}
    
    public void saveNewReccom() {
        
        Recommendation__c r = new Recommendation__c();
        r.Energy_Assessment__c = eassid;
        r.category__c = addReccomView.category;
        r.subCategory__c = addReccomView.subCategory;
        r.eversourceDesc__c = addReccomView.eversourceDesc;
        r.gridDesc__c = addReccomView.gridDesc;
        r.location__c = addReccomView.location;
        r.Units__c = addReccomView.unit;
        r.notes__c = addReccomView.notes;
        r.Recommendation_Scenario__c = projectId;
        r.DSMTracker_Product__c = addReccomView.emHubPartId ;
        
        insert r;   
    }
    
    public List<SelectOption> getCategoryOptions() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--Select Category--'));   
        for (AggregateResult ar : [SELECT Category__c FROM DSMTracker_Product__c where 
              ((Start_Date__c = null AND End_date__c = null) OR
                    (Start_Date__c = null AND End_date__c >= TODAY) OR
                    (Start_Date__c <= TODAY AND End_date__c = null) OR
                    (Start_Date__c <= TODAY AND End_date__c >= TODAY)
                ) group by Category__c 
            ]) {
        String n = (String) ar.get('Category__c');
        if(n != null){
            options.add(new SelectOption(n, n));
        }
        
        }
        return options;
    }
    
    public List<SelectOption> getSubcategoryOptions() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--Select Sub Category--'));   
        for (AggregateResult ar : [SELECT Sub_Category__c FROM DSMTracker_Product__c where Category__c =:addReccomView.category and 
             ((Start_Date__c = null AND End_date__c = null) OR
                    (Start_Date__c = null AND End_date__c >= TODAY) OR
                    (Start_Date__c <= TODAY AND End_date__c = null) OR
                    (Start_Date__c <= TODAY AND End_date__c >= TODAY)
             ) 
             group by Sub_Category__c
            ]) {
        String n = (String) ar.get('Sub_Category__c');
        if(n != null){
            options.add(new SelectOption(n, n));
        }
        
        }
        return options;
    }
    
    public void callSubCategory(){
        getSubcategoryOptions();
    }
    
    public List<SelectOption> getHubIdOptions() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--Select Part ID--'));   
        for (DSMTracker_Product__c ar : [SELECT Id,EMHome_EMHub_PartID__c FROM DSMTracker_Product__c where Category__c =:addReccomView.category AND Sub_Category__c =:addReccomView.subCategory and 
             ((Start_Date__c = null AND End_date__c = null) OR
                    (Start_Date__c = null AND End_date__c >= TODAY) OR
                    (Start_Date__c <= TODAY AND End_date__c = null) OR
                    (Start_Date__c <= TODAY AND End_date__c >= TODAY)
              ) 
             order by EMHome_EMHub_PartID__c
            limit 999]) {
        if(String.ISNOTBLANK(ar.id) && String.ISNOTBLANK(ar.EMHome_EMHub_PartID__c)){
            options.add(new SelectOption(ar.Id, ar.EMHome_EMHub_PartID__c));
        }
        
        }
        return options;
    }
    
    public void callHubId(){
        getHubIdOptions();
    }
    
    public List<SelectOption> getRecommendationTypeOptions() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--Select Recommendation Type--'));
        Schema.DescribeFieldResult fieldResult = Recommendation__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for(Schema.PicklistEntry p : ple)
        options.add(new SelectOption(p.getValue(), p.getValue()));        
        return options;
    }
    
    public string MajorXml {
        get;
        set;
    }
    
    public string DirectInstallXml{
        get;
        set;
    }
    
    public void initRecommendations(){
        addReccomView = new addReccomModel();
    }
    
    public CreateProjectEnergyAssesmentExtn(){
        
        eassId = Apexpages.currentPage().getParameters().get('id');
        MajorCount = 0;
    }
    
    public CreateProjectEnergyAssesmentExtn(ApexPages.StandardController controller){
        system.debug('--Apexpages.currentPage().getParameters()--'+Apexpages.currentPage().getParameters());
        MajorCount = 0;
        Enas = new Energy_Assessment__c();
        String EasId = controller.getId();
        initRecommendations();
        system.debug('--EnergyAssessmentId--'+EasId);
        eassId = controller.getId();
        projectId = Apexpages.currentPage().getParameters().get('projId'); 
        system.debug('--projectId--'+projectId);

        List<Energy_Assessment__c> lstEnas = [select id,Name from Energy_Assessment__c
                                                  WHERE Id=:EasId];
        
         
         system.debug('--recommendationMajorelist--'+recmajorlist);
         recdirectlist = [select id,name,Quantity__c,Hours__c,Building_Specification__c from Recommendation__c where Energy_Assessment__c=:EasId and Type__c='Direct Install'
                             and Recommendation_Scenario__c = null];
                             
         if(projectId != null && projectId != ''){
             List<Recommendation_Scenario__c> projectlist = [select id,name,Status__c,Recommendation_Type__c from Recommendation_Scenario__c where id =: projectId];
             if(projectlist.size() > 0){
                 Recommendation_Scenario__c project = projectlist.get(0);
                 ProjectName = project.name;
                 SelectedRecommendationType = project.Recommendation_Type__c ;
             }
         }                    

    }
        
    public void availableSkill() {
        //lstSkillsWrap = new List < MajorWrapper > ();
        recmajorlist =[select id,name,Quantity__c,Hours__c,Building_Specification__c,Total_Savings__c,Total_Energy_Savings_kWh__c,Recommended_Product__c,Status__c,isChecked__c from Recommendation__c  where 
                        Energy_Assessment__c=:EassId and Type__c='Major Measure' and Recommendation_Scenario__c = null];
        MajorCount  = recmajorlist.size();
        
        //LoadXml
        MajorXml = '<Skills>';

        integer cnt = 1;
        for (Recommendation__c rec : recmajorlist ) {
            
            MajorXml += '<Skill>';
            
            MajorWrapper sw = new MajorWrapper();
            
            MajorXml += '<Rownum>' + cnt + '</Rownum>';
            MajorXml += '<RecommendationId><![CDATA[' + rec.Id+ ']]></RecommendationId>';
            MajorXml += '<SkillName><![CDATA[' + rec.Recommended_Product__c+ ']]></SkillName>';
            MajorXml += '<Checked><![CDATA[' + rec.isChecked__c+ ']]></Checked>';
            MajorXml += '<Score><![CDATA[' + rec.Quantity__c+ ']]></Score>';
            MajorXml += '<IEv><![CDATA[' + rec.Status__c+ ']]></IEv>';
            MajorXml += '<TotalSaving><![CDATA[' + rec.Total_Energy_Savings_kWh__c+ ']]></TotalSaving>';
            MajorXml += '<Size>'+rec.Total_Savings__c + '</Size>';
            
            
            MajorXml += '</Skill>';
            cnt++;
        }

        MajorXml += '</Skills>';
    }
    
    public void availabledirectInstallList() {
        recmajorlist =[select id,isChecked__c,name,Quantity__c,Hours__c,Building_Specification__c,Total_Savings__c,Total_Energy_Savings_kWh__c,Recommended_Product__c,Status__c from Recommendation__c  where 
                        Energy_Assessment__c=:EassId and Type__c='Direct Install' and Recommendation_Scenario__c = null];
        MajorCount  = recmajorlist.size();
        //LoadXml
        DirectInstallXml = '<Skills>';

        integer cnt = 1;
        for (Recommendation__c rec : recmajorlist ) {
            
            DirectInstallXml += '<Skill>';
            
            MajorWrapper sw = new MajorWrapper();
            
            DirectInstallXml += '<Rownum>' + cnt + '</Rownum>';
            DirectInstallXml += '<RecommendationId><![CDATA[' + rec.Id+ ']]></RecommendationId>';
            DirectInstallXml += '<Checked><![CDATA[' + rec.isChecked__c+ ']]></Checked>';
            DirectInstallXml += '<SkillName><![CDATA[' + rec.Recommended_Product__c+ ']]></SkillName>';
            DirectInstallXml += '<Score><![CDATA[' + rec.Quantity__c+ ']]></Score>';
            DirectInstallXml += '<IEv><![CDATA[' + rec.Status__c+ ']]></IEv>';
            DirectInstallXml += '<TotalSaving><![CDATA[' + rec.Total_Energy_Savings_kWh__c+ ']]></TotalSaving>';
            DirectInstallXml += '<Size>'+rec.Total_Savings__c + '</Size>';
            
            
            
            DirectInstallXml += '</Skill>';
            cnt++;
        }

        DirectInstallXml += '</Skills>';
        
        system.debug('--DirectInstallXml---'+DirectInstallXml);
        
        
    }
    
    
    public void recommendationList() {
        string projId = ApexPages.currentPage().getParameters().get('projId');
        recmajorlist =[select id, category__c, subCategory__c,eversourceDesc__c,gridDesc__c,location__c, 
                       Units__c, notes__c  from Recommendation__c  where 
                       Recommendation_Scenario__c =: projId
                       and Recommendation_Scenario__c != null];
        
        MajorCount  = recmajorlist.size();
        //LoadXml
        DirectInstallXml = '<Skills>';

        integer cnt = 1;
        for (Recommendation__c rec : recmajorlist ) {
            
            DirectInstallXml += '<Skill>';
            
            MajorWrapper sw = new MajorWrapper();
            
            DirectInstallXml += '<RecId>' + rec.Id + '</RecId>';
            DirectInstallXml += '<Rownum>' + cnt + '</Rownum>';
            DirectInstallXml += '<Category><![CDATA[' + rec.category__c+ ']]></Category>';
            DirectInstallXml += '<SubCategory><![CDATA[' + rec.subCategory__c+ ']]></SubCategory>';
            DirectInstallXml += '<EversourceDesc><![CDATA[' + rec.eversourceDesc__c+ ']]></EversourceDesc>';
            DirectInstallXml += '<GridDesc><![CDATA[' + rec.gridDesc__c+ ']]></GridDesc>';
            DirectInstallXml += '<Location><![CDATA[' + balnkValueCheck(rec.location__c) + ']]></Location>';
            DirectInstallXml += '<unit><![CDATA[' + rec.Units__c+ ']]></unit>';
            DirectInstallXml += '<Notes>'+rec.notes__c + '</Notes>';
            
            
            
            DirectInstallXml += '</Skill>';
            cnt++;
        }

        DirectInstallXml += '</Skills>';
        
        system.debug('--DirectInstallXml---'+DirectInstallXml);
    }
    
    public string balnkValueCheck(string val){
        string s = '';
        if(val == null)
            return s;
        else
            return val;
    }
    
    public void saveProject()
    {
         Recommendation_Scenario__c recs = new Recommendation_Scenario__c();
         recs.name = ProjectName;
         recs.Energy_Assessment__c = eassid;
         
         if(SelectedRecommendationType == 'Direct Install'){
             recs.Status__c = 'Installed';
         }else{
             recs.Status__c = 'Proposed';
         }
         recs.Recommendation_Type__c = SelectedRecommendationType;
         
         insert recs;
         
         projectId = recs.Id;
    }
    
    
    public PageReference save(){
         
         if(SelectedRecId != null && SelectedRecId.split(',').size() > 0){
             
             List<Recommendation__c> recList = [select id,Type__c from Recommendation__c  where id =: SelectedRecId.split(',').get(0)];
             
             list<Recommendation__c> relist = new list<Recommendation__c>();
             for(String str : SelectedRecId.split(',')){
                 if(str != null && str != ''){
                     Recommendation__c Re = new Recommendation__c();
                     
                     if(SelectedRecommendationType == 'Direct Install'){
                         Re.Status__c = 'Installed';
                     }else{
                         Re.Status__c = 'Proposed';
                     }
                     Re.Recommendation_Scenario__c = projectId;
                     Re.id = str;
                     relist.add(Re);    
                 }
             }
             if(relist.size()>0){
                 update relist;
             }
         }
         
         return null;
    }
    
    public Pagereference DeleteRecommendation(){
        List<Recommendation__c> recList = [select id from Recommendation__c where id =: RecommendationId];
        
        if(recList != null && recList.size() > 0){
            delete recList;
        }
        return null;
    }
        
    public class MajorWrapper {
    
        public Skill__c objAvailableSkill {
            get;
            set;
        }
     
        public String SelectedSkill{get;set;}
        public Id SkillId{get;set;}
        public String SkillName{get;set;}
        public string selectEvaOpt {
            get;
            set;
        }
        public string surveyScore {
            get;
            set;
        }
        public MajorWrapper(){
            //inteEvaOpt = getInternalEvaList();
        }
    }
}