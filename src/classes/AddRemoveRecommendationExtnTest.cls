@isTest
public class AddRemoveRecommendationExtnTest
{
    public testmethod static void test1()
    {
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
                   
        Recommendation__c rc = new Recommendation__c();
        rc.Recommendation_Scenario__c = proj.id;
        insert rc;
        Recommendation__c rc1 = new Recommendation__c();
        rc1.Recommendation_Scenario__c = proj.id;
        insert rc1;

        Review__c rev = new Review__c();
        rev.Project__c = proj.id;
        insert rev;
        
       
        Inspection_Request__c ir =new Inspection_Request__c();
        insert ir;
        
        DSMTracker_Product__c dtp =new DSMTracker_Product__c();
        insert dtp;
        
        Inspection_Line_Item__c ili = new Inspection_Line_Item__c();
        ili.Recommendation__c = rc1.id;
        ili.Inspection_Request__c = ir.id;
       // ili.Reported_Part__c=dtp.Id;
        ili.Quick_Notes__c='Test';
        ili.Status__c='Action Required';
        insert ili;
        
        ApexPages.currentPage().getParameters().put('prjId',proj.id);
        ApexPages.currentPage().getParameters().put('recid',rc.id);
        ApexPages.currentPage().getParameters().put('revid',rev.id);
        ApexPages.currentPage().getParameters().put('id',ir.id);
        
        AddRemoveRecommendationExtn cntrl = new AddRemoveRecommendationExtn();
        
        cntrl.getListOfProj();
        cntrl.loadProjects();
        cntrl.loadRecommendations();
        //cntrl.dataSave = 'string';
        cntrl.loadLineItemXml();
        cntrl.SelectedRecId = rc.id;
        cntrl.saveInspectionItem();
        //cntrl.removeRecord();
    }
   
}