public class LayerTriggerHandler extends TriggerHandler implements ITrigger
{
    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            calculateDefaultValues(trigger.new, (Map<Id, Layer__c>) trigger.oldmap);
        } else if (trigger.isUpdate) {
            //Here we will call before update actions
            calculateDefaultValues(trigger.new, (Map<Id, Layer__c>) trigger.oldmap);
        } else if (trigger.isDelete) {
            //Here we will call before delete actions
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) {
            //Here we will call after insert actions
        } else if (trigger.isUpdate) {
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }
    
    private static void calculateDefaultValues(list<Layer__c> newList, Map<Id, Layer__c> oldMap)
    {
        try{
            Set<Id> TEId = new Set<Id>();
            Set<Id> WallId = new Set<Id>();
            Set<Id> CeilingId = new Set<Id>();
            Set<Id> FloorId = new Set<Id>();
            
            for(Layer__c l : newList)
            {
                // if wall / ceiling / floor populated remove tet lookup
                if(l.Wall__c != null) l.Thermal_Envelope_Type__c = null;
                if(l.Floor__c != null) l.Thermal_Envelope_Type__c = null;
                if(l.Ceiling__c != null) l.Thermal_Envelope_Type__c = null;
                    
                if(l.Thermal_Envelope_Type__c != null)
                {
                    TeId.add(l.Thermal_Envelope_Type__c);
                }
                else if(l.Wall__c != null)
                {
                    WallId.add(l.wall__c);
                }
                else if(l.Ceiling__c != null)
                {
                    ceilingId.add(l.ceiling__c);
                }
                else if(l.Floor__c != null)
                {
                    FloorId.add(l.floor__c);
                }
            }    
             
            Set<Id> setTETId = new Set<Id>();
            Set<Id> EaId = new Set<Id>();

            List<Layer__c> temLst = new List<Layer__c>();
            
            String query = '';
            
            if(teId != null && teId.size() > 0)
            { 
                List<Thermal_Envelope_Type__c> cList = [SELECT id, Energy_Assessment__c 
                FROM Thermal_Envelope_Type__c WHERE id in : teId];
            
                for(integer i =0, j = cList.size(); i < j; i++)
                {
                    Thermal_Envelope_Type__c w = cList[i];
                    setTETId.add(w.Id);
                    if(w.Energy_Assessment__c !=null)
                        EaId.add(w.Energy_Assessment__c);
                }
            
            }
            else if(FloorId != null && FloorId.size() > 0 )
            {  
                List<Floor__c> cList = [SELECT id, Thermal_Envelope_Type__c, 
                Thermal_Envelope_Type__r.Energy_Assessment__c 
                FROM Floor__c WHERE id in : FloorId];
            
                for(integer i =0, j = cList.size(); i < j; i++)
                {
                    Floor__c w = cList[i];
                    if(w.Thermal_Envelope_Type__c !=null)
                        setTETId.add(w.Thermal_Envelope_Type__c);

                    if(w.Thermal_Envelope_Type__r.Energy_Assessment__c !=null)
                        EaId.add(w.Thermal_Envelope_Type__r.Energy_Assessment__c);
                }
            }
            else if(WallId != null && WallId.size() > 0)
            {
                List<Wall__c> cList = [SELECT id, Thermal_Envelope_Type__c, 
                Thermal_Envelope_Type__r.Energy_Assessment__c 
                FROM Wall__c WHERE id in : WallId];
            
                for(integer i =0, j = cList.size(); i < j; i++)
                {
                    Wall__c w = cList[i];
                    if(w.Thermal_Envelope_Type__c !=null)
                        setTETId.add(w.Thermal_Envelope_Type__c);

                    if(w.Thermal_Envelope_Type__r.Energy_Assessment__c !=null)
                        EaId.add(w.Thermal_Envelope_Type__r.Energy_Assessment__c);
                }
            }
            else if(ceilingId != null && ceilingId.size() > 0)
            {
                List<Ceiling__c> cList = [SELECT id, Thermal_Envelope_Type__c, 
                Thermal_Envelope_Type__r.Energy_Assessment__c 
                FROM Ceiling__c WHERE id in : ceilingId];
            
                for(integer i =0, j = cList.size(); i < j; i++)
                {
                    Ceiling__c w = cList[i];
                    if(w.Thermal_Envelope_Type__c !=null)
                        setTETId.add(w.Thermal_Envelope_Type__c);

                    if(w.Thermal_Envelope_Type__r.Energy_Assessment__c !=null)
                        EaId.add(w.Thermal_Envelope_Type__r.Energy_Assessment__c);
                }
            }

            /// still not getting any thermal envelope type return as no result will be correct now
            if(setTETId ==null || setTETId.size()==0) return;
            if(EaId ==null || EaId.size()==0) return;

            
            dsmtEAModel.surface bp = dsmtEAModel.InitializeBuildingProfile(EaId); 
            if(bp==null) return;

            bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
            bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

            Id layerParentId=null;

            integer buildingyear =0;
            if(bp.bs.Year_Built__c != null && bp.bs.Year_Built__c !='') 
            {
               buildingyear = integer.valueof(bp.bs.Year_Built__c);
            }
            
            List<Layer__c> tList = null;
            for(Layer__c tet: newList)
            {
                if(tet.Thermal_Envelope_Type__c!=null)
                {
                    layerParentId = tet.Thermal_Envelope_Type__c;
                    tList = bp.allLayerMap.get(tet.Thermal_Envelope_Type__c);
                }
                else if(tet.Wall__c!=null)
                {
                    layerParentId = tet.Wall__c;
                    tList = bp.allLayerMap.get(tet.Wall__c);
                }
                else if(tet.Ceiling__c!=null)
                {
                    layerParentId = tet.Ceiling__c;
                    tList = bp.allLayerMap.get(tet.Ceiling__c);
                }
                else if(tet.Floor__c!=null)
                {
                    layerParentId = tet.Floor__c;
                    tList = bp.allLayerMap.get(tet.Floor__c);
                }

                if(tList ==null)tList = new List<Layer__c>();

                boolean layerFound =false;
                For(integer i=0, j= tList.size(); i <j;i++)
                {
                    Layer__c l = tList[i];
                    if(l.Id==tet.Id)
                    {
                        layerFound=true;
                        tList[i] = tet;
                        break;
                    }
                }

                if(!layerFound)
                {
                    tlist.add(tet);
                }

                bp.allLayerMap.put(layerParentId,tlist);
            }

            Map<Id,Wall__c> wallMap = new Map<Id,Wall__c>();
            Map<Id,Floor__c> floorMap = new Map<Id,Floor__c>();
            Map<Id,Ceiling__c> ceilingMap = new Map<Id,Ceiling__c>();
            Map<Id,Thermal_Envelope_Type__c> thermalMap = new Map<Id,Thermal_Envelope_Type__c>();

            if(bp.wallList !=null)
            {
                for(integer i =0, j = bp.wallList.size(); i < j; i++)
                {
                    Wall__c w = bp.wallList[i];
                    wallMap.put(w.Id,w);
                }
            }

            if(bp.FloorList !=null)
            {
                for(integer i =0, j = bp.floorList.size(); i < j; i++)
                {
                    Floor__c w = bp.floorList[i];
                    floorMap.put(w.Id,w);
                }
            }

            if(bp.ceilList !=null)
            {
                for(integer i =0, j = bp.ceilList.size(); i < j; i++)
                {
                    Ceiling__c w = bp.ceilList[i];
                    ceilingMap.put(w.Id,w);
                }
            }


            if(bp.telist !=null)
            {
                for(integer i =0, j = bp.telist.size(); i < j; i++)
                {
                    Thermal_Envelope_Type__c w = bp.telist[i];
                    thermalMap.put(w.Id,w);
                }
            }
            
            for(Layer__c tet: newList)
            {
                if(tet.IsFromFieldTool__c) continue;
                
                string parentInsulationAmount ='';
                bp.Layer = tet;
                
                if(tet.Thermal_Envelope_type__c != null && bp.allLayerMap.get(tet.Thermal_Envelope_type__c ) != null)
                {
                    bp.layers = bp.allLayerMap.get(tet.Thermal_Envelope_type__c);    
                }
                else if(tet.Wall__c != null && bp.allLayerMap.get(tet.Wall__c) != null)
                {
                    bp.layers = bp.allLayerMap.get(tet.Wall__c );    
                }
                else if(tet.Floor__c != null && bp.allLayerMap.get(tet.Floor__c) != null){
                    bp.layers = bp.allLayerMap.get(tet.Floor__c);    
                }
                else if(tet.Ceiling__c != null && bp.allLayerMap.get(tet.Ceiling__c) != null)
                {
                    bp.layers = bp.allLayerMap.get(tet.Ceiling__c);    
                }
                
                // get layer parent and tet if layer parent is not tet
                if(tet.Thermal_Envelope_Type__c !=null)
                {
                    bp.teType = thermalMap.get(tet.Thermal_Envelope_Type__c);
                    bp.wall=null;
                    bp.ceiling =null;
                    bp.floor =null;
                    parentInsulationAmount = bp.teType.Insul_Amount__c;
                }
                else if(tet.wall__c !=null)
                {
                    bp.wall = wallMap.get(tet.Wall__c);
                    bp.tetype = thermalMap.get(bp.wall.Thermal_Envelope_Type__c);
                    bp.ceiling =null;
                    bp.floor =null;
                    parentInsulationAmount = bp.wall.Insul_Amount__c;
                }
                else if(tet.floor__c !=null)
                {
                    bp.floor = floorMap.get(tet.Floor__c);
                    bp.tetype = thermalMap.get(bp.floor.Thermal_Envelope_Type__c);
                    bp.ceiling = null;
                    bp.wall=null;
                    parentInsulationAmount = bp.floor.Insul_Amount__c;
                }
                else if(tet.ceiling__c !=null)
                {
                    bp.ceiling = ceilingMap.get(tet.ceiling__c);
                    bp.tetype = thermalMap.get(bp.ceiling.Thermal_Envelope_Type__c);
                    bp.wall=null;
                    bp.floor =null;
                    parentInsulationAmount = bp.ceiling.Insul_Amount__c;
                }
                
                Layer__c newLayer = tet;
                //bp.layer = newLayer;
                
                boolean isInsert = Trigger.isInsert;
                
                if(bp.teType.ChangeType__c) isInsert = true;
                newLayer.Type__c = bp.RecordTypeMap.get(newLayer.RecordTypeId);

                if(newLayer.ActualCavityInsulationDepth__c <=0) newLayer.ActualCavityInsulationDepth__c=null;
                if(newLayer.ActualCavityInsulationAmount__c =='') newLayer.ActualCavityInsulationAmount__c = null;

                if(isInsert)
                {
                    newLayer.Year__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectYearCommon(bp);
                    newLayer.Material_Type__c = dsmtSurfaceHelper.ComputeLayerMaterialTypeCommon(bp); // insert
                    newLayer.Frame_Spacing__c= dsmtSurfaceHelper.ComputeLayerFramingSpacingCommon(bp); // insert
                    newLayer.Framing_Type__c = Saving_Constant__c.GetOrgDefaults().DefaultFramingType__c; // insert
                    newLayer.Enclosed__c = dsmtSurfaceHelper.ComputeLayerIsEnclosedCommon(bp); // insert,update
                    newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeLayerInsulationAmountCommon(bp); // insert
                    newLayer.Gaps__c = dsmtSurfaceHelper.ComputeLayerInsulationGapsCommon(bp); // insert
                    newLayer.Cavity_Insul_Type__c = dsmtSurfaceHelper.ComputeCavityInsulationTypeCommon(bp); // insert
                    newLayer.Overflow_Insul_Type__c = dsmtSurfaceHelper.ComputeOverflowInsulationTypeCommon(newLayer); // insert,update
                    newLayer.Thickness__c = dsmtSurfaceHelper.ComputeLayerThicknessCommon(bp); // insert
                    newLayer.S4S__c = dsmtSurfaceHelper.ComputeFrameDimensionsAreS4SCommon(bp); // insert
                    newLayer.RValuePerInch__c = dsmtSurfaceHelper.ComputeLayerRValuePerInchCommon(bp); // insert,update
                    newLayer.NominalFrameDepth__c = dsmtSurfaceHelper.ComputeNominalLayerFrameDepthCommon(bp); // insert
                    newLayer.NominalFrameWidth__c = Saving_Constant__c.GetOrgDefaults().DefaultFrameWidth__c; // insert
                    newLayer.Frame_Width__c = dsmtSurfaceHelper.ConvertFrameDimensionCommon(newLayer.NominalFrameWidth__c, newLayer.S4S__c,false); // insert,update
                    newLayer.Depth__c = dsmtSurfaceHelper.ConvertFrameDimensionCommon(newLayer.NominalFrameDepth__c,newLayer.S4S__c,false); // insert,update
                    newLayer.FrameFactor__c = dsmtSurfaceHelper.ComputeLayerFrameFactorCommon(bp); // insert,update                
                    newLayer.CavityRValuePerInch__c = dsmtSurfaceHelper.ComputeLayerCavityRValuePerInchCommon(bp); // insert,update
                    newLayer.OverflowRValuePerInch__c = dsmtSurfaceHelper.ComputeLayerOverflowRValuePerInchCommon(bp); // insert,update                
                    newLayer.Cavity_Insul_Depth__c =dsmtSurfaceHelper.ComputeLayerInsulationDepthCommon(bp); // insert
                    newLayer.JustCavityRValue__c = dsmtSurfaceHelper.ComputeLayerJustCavityRValueCommon(bp);   // insert,update             
                    newLayer.Cover_Frames__c = dsmtSurfaceHelper.ComputeLayerInsulationCoversFrameCommon(newLayer); //  insert,update
                    newLayer.Is_Filled__c = dsmtSurfaceHelper.ComputeLayerIsFilledCommon(newLayer); // insert                
                    newLayer.ExtraCoveringCavityRValue__c=dsmtSurfaceHelper.ComputeLayerExtraCoveringCavityRValueCommon(newLayer); // insert,update
                    newLayer.ExtraNotCoveringCavityRValue__c=dsmtSurfaceHelper.ComputeLayerExtraNotCoveringCavityRValueCommon(newLayer); // insert,update
                    newLayer.CavityRValue__c = dsmtSurfaceHelper.ComputeLayerCavityRValueCommon(bp); // insert,update
                    newLayer.R_Value__c = dsmtSurfaceHelper.ComputeLayerRValueCommon(bp); // insert,update
                    newLayer.Nominal_R_Value__c = dsmtSurfaceHelper.ComputeLayerNominalRValueCommon(bp); // insert,update
                    newLayer.Effective_R_Value__c = dsmtSurfaceHelper.ComputeLayerEffectiveRValueCommon(bp); // insert,update
                }
                else
                {     
                    integer Parentyear = (newLayer.Year__c ==null ? 0 : integer.ValueOf(newLayer.Year__c));
                    if(Parentyear != buildingyear)
                    {
                        newLayer.S4S__c = dsmtSurfaceHelper.ComputeFrameDimensionsAreS4SCommon(bp); // insert
                    }
                    newLayer.Year__c = buildingyear;
                    
                    boolean teTruesses = bp.teType.Trusses__c;
                    boolean parentTruss=false;
                    if(bp.ceiling!=null)
                    {
                        parentTruss = bp.ceiling.Has_Trusses__c;
                    }
                    if(bp.floor!=null)
                    {
                        parentTruss = bp.floor.Has_Trusses__c;
                    }
                    
                    if(teTruesses != parentTruss)
                    {
                         if(bp.ceiling!=null) bp.ceiling.Has_Trusses__c = teTruesses;
                         if(bp.floor!=null) bp.floor.Has_Trusses__c = teTruesses;                   
                         newLayer.Frame_Spacing__c = null;
                         newLayer.NominalFrameDepth__c =null;
                    }
                
                    /// UI default
                    if(newLayer.Material_Type__c==null || newLayer.Material_Type__c=='') newLayer.Material_Type__c = dsmtSurfaceHelper.ComputeLayerMaterialTypeCommon(bp); // insert
                    if(newLayer.Frame_Spacing__c==null) newLayer.Frame_Spacing__c= dsmtSurfaceHelper.ComputeLayerFramingSpacingCommon(bp); // insert
                    if(newLayer.NominalFrameDepth__c == null) newLayer.NominalFrameDepth__c = dsmtSurfaceHelper.ComputeNominalLayerFrameDepthCommon(bp); // insert
                    if(newLayer.NominalFrameWidth__c == null) newLayer.NominalFrameWidth__c = Saving_Constant__c.GetOrgDefaults().DefaultFrameWidth__c; // insert
                    if(newLayer.Framing_Type__c==null || newLayer.Framing_Type__c == '') newLayer.Framing_Type__c = Saving_Constant__c.GetOrgDefaults().DefaultFramingType__c; // insert
                    if(newLayer.Gaps__c ==null || newLayer.Gaps__c=='') newLayer.Gaps__c = dsmtSurfaceHelper.ComputeLayerInsulationGapsCommon(bp); // insert
                    /// UI default
                    
                    newLayer.Type__c = bp.RecordTypeMap.get(newLayer.RecordTypeId);                
                    newLayer.Enclosed__c = dsmtSurfaceHelper.ComputeLayerIsEnclosedCommon(bp); // insert,update                                
                    newLayer.RValuePerInch__c = dsmtSurfaceHelper.ComputeLayerRValuePerInchCommon(bp); // insert,update
                    newLayer.Frame_Width__c = dsmtSurfaceHelper.ConvertFrameDimensionCommon(newLayer.NominalFrameWidth__c, newLayer.S4S__c,false); // insert,update
                    newLayer.Depth__c = dsmtSurfaceHelper.ConvertFrameDimensionCommon(newLayer.NominalFrameDepth__c,newLayer.S4S__c,false); // insert,update
                    newLayer.FrameFactor__c = dsmtSurfaceHelper.ComputeLayerFrameFactorCommon(bp); // insert,update
                    
                    if(newLayer.Cavity_Insul_Type__c != 'Other' && newLayer.Cavity_Insul_Type__c !=null)
                        newLayer.CavityRValuePerInch__c = dsmtSurfaceHelper.ComputeLayerCavityRValuePerInchCommon(bp); // insert,update
                    
                    if(newLayer.Overflow_Insul_Type__c == null || newLayer.Overflow_Insul_Type__c =='')
                    {
                        newLayer.Overflow_Insul_Type__c = dsmtSurfaceHelper.ComputeOverflowInsulationTypeCommon(newLayer); // insert,update
                        newLayer.OverflowRValuePerInch__c = dsmtSurfaceHelper.ComputeLayerOverflowRValuePerInchCommon(bp); // insert,update
                    }
                    else
                    {
                        if(newLayer.Overflow_Insul_Type__c != 'Other')
                        {
                            newLayer.OverflowRValuePerInch__c = dsmtSurfaceHelper.ComputeLayerOverflowRValuePerInchCommon(bp);
                        }
                    }

                    // DSST-6348 - Cavity Insulation Depth error
                    if(NewLayer.ActualCavityInsulationDepth__c !=null)
                    {
                        newLayer.ActualCavityInsulationAmount__c =null;
                    }                    
                   
                    // DSST-6348 - Cavity Insulation Depth error
                    // #Code Block Start
                    if(newLayer.ActualCavityInsulationAmount__c ==null)
                    {
                        //newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeLayerInsulationAmountCommon(bp);

                        if(dsmtEAModel.GetWallRecordTypes().contains(bp.recordTypeMap.get(bp.tetype.RecordTypeId))
                            || newLayer.Wall__c !=null)
                        {
                            newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeWallInsulationAmountCommon(bp);
                            //newLayer.Insul_Amount__c = ComputeLayerInsulationAmountCommon(bp);
                        }
                        else if(dsmtEAModel.GetFloorCeilingRecordType().contains(bp.recordTypeMap.get(bp.tetype.RecordTypeId)) || 
                            newLayer.Floor__c !=null || 
                            newLayer.Ceiling__c !=null)
                        {
                            newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
                        }
                    }

                    //newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeLayerInsulationAmountCommon(bp); // insert
                    if(NewLayer.ActualCavityInsulationDepth__c ==null)
                    {
                        newLayer.Cavity_Insul_Depth__c = dsmtSurfaceHelper.ComputeLayerInsulationDepthCommon(bp);
                    }
                    else 
                    {
                        if(newLayer.ActualCavityInsulationDepth__c > newLayer.NominalFrameDepth__c && newLayer.Enclosed__c)
                        {
                            newLayer.ActualCavityInsulationDepth__c = newLayer.NominalFrameDepth__c;
                            newLayer.Cavity_Insul_Depth__c = newLayer.ActualCavityInsulationDepth__c;
                        }
                    }
                    // DSST-6348 - Cavity Insulation Depth error
                    // #Code Block End
                    
                    newLayer.JustCavityRValue__c = dsmtSurfaceHelper.ComputeLayerJustCavityRValueCommon(bp);   // insert,update
                    
                    if(!newlayer.Cover_Frames__c)
                        newLayer.Cover_Frames__c = dsmtSurfaceHelper.ComputeLayerInsulationCoversFrameCommon(newLayer); //  insert,update

                    if(bp.tetype.Change_Order__c)
                    {
                        newLayer.Cavity_Insul_Depth__c = bp.tetype.Cavity_Insul_Depth__c;
                        newLayer.Cavity_Insul_Type__c = bp.teType.Cavity_Insul_Type__c;
                        newLayer.ActualCavityInsulationDepth__c = bp.tetype.Cavity_Insul_Depth__c;
                        newLayer.ActualCavityInsulationType__c = bp.tetype.Cavity_Insul_Type__c;
                    }                        

                    newLayer.ExtraCoveringCavityRValue__c=dsmtSurfaceHelper.ComputeLayerExtraCoveringCavityRValueCommon(newLayer); // insert,update
                    newLayer.ExtraNotCoveringCavityRValue__c=dsmtSurfaceHelper.ComputeLayerExtraNotCoveringCavityRValueCommon(newLayer); // insert,update
                    newLayer.CavityRValue__c = dsmtSurfaceHelper.ComputeLayerCavityRValueCommon(bp); // insert,update
                    newLayer.R_Value__c = dsmtSurfaceHelper.ComputeLayerRValueCommon(bp); // insert,update
                    newLayer.Nominal_R_Value__c = dsmtSurfaceHelper.ComputeLayerNominalRValueCommon(bp); // insert,update
                    newLayer.Effective_R_Value__c = dsmtSurfaceHelper.ComputeLayerEffectiveRValueCommon(bp); // insert,update

                    // DSST-6348 - Cavity Insulation Depth error
                    // #Code Block Start
                    if(newLayer.ActualCavityInsulationAmount__c ==null && newLayer.ActualCavityInsulationDepth__c !=null)
                    {
                        //newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeLayerInsulationAmountCommon(bp);

                        if(dsmtEAModel.GetWallRecordTypes().contains(bp.recordTypeMap.get(bp.tetype.RecordTypeId))
                            || newLayer.Wall__c !=null)
                        {
                            newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeWallInsulationAmountCommon(bp);
                            //newLayer.Insul_Amount__c = ComputeLayerInsulationAmountCommon(bp);
                        }
                        else if(dsmtEAModel.GetFloorCeilingRecordType().contains(bp.recordTypeMap.get(bp.tetype.RecordTypeId)) || 
                            newLayer.Floor__c !=null || 
                            newLayer.Ceiling__c !=null)
                        {
                            newLayer.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
                        }
                    }
                    // DSST-6348 - Cavity Insulation Depth error
                    // #Code Block End

                    //system.debug('newLayer.Insul_Amount__c>>>'+newLayer.Insul_Amount__c);

                    ///DSST-14896
                    /// Make sure the inslation amount will be null if its none
                    if(newLayer.Insul_Amount__c!=null && newLayer.Insul_Amount__c.equalsIgnoreCase('none'))
                    {
                        newLayer.Insul_Amount__c = '';
                    }

                }
            }
        }catch(Exception ex){
            dsmtHelperClass.WriteMessage(ex);
        }
    }

    public static void UpdateLayerForParent(dsmtEAModel.Surface bp)
    {
        Layer__c newLayer = bp.Layer;
        newlayer.Year__c = bp.tetype.Year__c;
        newLayer.FrameFactor__c = dsmtSurfaceHelper.ComputeLayerFrameFactorCommon(bp); // insert,update                
        newLayer.CavityRValuePerInch__c = dsmtSurfaceHelper.ComputeLayerCavityRValuePerInchCommon(bp); // insert,update
        newLayer.OverflowRValuePerInch__c = dsmtSurfaceHelper.ComputeLayerOverflowRValuePerInchCommon(bp); // insert,update                
        newLayer.Cavity_Insul_Depth__c =dsmtSurfaceHelper.ComputeLayerInsulationDepthCommon(bp); // insert
        newLayer.JustCavityRValue__c = dsmtSurfaceHelper.ComputeLayerJustCavityRValueCommon(bp);   // insert,update             
        newLayer.Cover_Frames__c = dsmtSurfaceHelper.ComputeLayerInsulationCoversFrameCommon(newLayer); //  insert,update
        newLayer.Is_Filled__c = dsmtSurfaceHelper.ComputeLayerIsFilledCommon(newLayer); // insert                
        newLayer.ExtraCoveringCavityRValue__c=dsmtSurfaceHelper.ComputeLayerExtraCoveringCavityRValueCommon(newLayer); // insert,update
        newLayer.ExtraNotCoveringCavityRValue__c=dsmtSurfaceHelper.ComputeLayerExtraNotCoveringCavityRValueCommon(newLayer); // insert,update
        newLayer.CavityRValue__c = dsmtSurfaceHelper.ComputeLayerCavityRValueCommon(bp); // insert,update
        newLayer.R_Value__c = dsmtSurfaceHelper.ComputeLayerRValueCommon(bp); // insert,update
        newLayer.Nominal_R_Value__c = dsmtSurfaceHelper.ComputeLayerNominalRValueCommon(bp); // insert,update
        newLayer.Effective_R_Value__c = dsmtSurfaceHelper.ComputeLayerEffectiveRValueCommon(bp); // insert,update
    }
}