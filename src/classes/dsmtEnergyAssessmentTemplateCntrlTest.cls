@isTest
public class dsmtEnergyAssessmentTemplateCntrlTest
{
    public testmethod static void test1()
    {
        Energy_Assessment__c ea = datagenerator.setupAssessment();
        ea.Status__c ='Assessment Complete';
        update ea;
        Test.startTest();
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
      
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc;
        Recommendation__c rc1 = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        insert rc1;
        
        Review__c rev = new Review__c(Project__c = proj.id);
        insert rev;
           customer__c cus = new customer__c();
        cus.name = 'test';
        insert cus;
        
        Inspection_Request__c ir = new Inspection_Request__c(Energy_Assessment__c = ea.id);
        insert ir;
        
        Inspection_Line_Item__c ili = new Inspection_Line_Item__c(Recommendation__c = rc1.id,Inspection_Request__c=ir.id);
        insert ili;
        
     
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        ec.Appointment_Type__c='Expanded HEA';
        insert ec;
        
        Workorder__c w = new Workorder__c(Eligibility_Check__c=ec.id);
        w.Early_Arrival_Time__c = datetime.now();
        w.Late_Arrival_Time__c = datetime.now();        
        insert w;
        
        Appointment__c apt = new Appointment__c(Workorder__c=w.id);
        insert apt;
        
        Thermal_Envelope__c te = new Thermal_Envelope__c(Energy_Assessment__c=ea.id);
        insert te;
       
       Test.stopTest();
        ApexPages.currentPage().getParameters().put('prjId',proj.id);
        ApexPages.currentPage().getParameters().put('recid',rc.id);
        ApexPages.currentPage().getParameters().put('revid',rev.id);
        ApexPages.currentPage().getParameters().put('isComplete','No');
        
        // ApexPages.currentPage().getParameters().put('custId',cus.id);
       
        ApexPages.currentPage().getParameters().put('custId',cus.id);
        ApexPages.currentPage().getParameters().put('id',apt.id);
        
        dsmtEnergyAssessmentTemplateController dEAT=new dsmtEnergyAssessmentTemplateController();
        apexpages.currentpage().getparameters().put('assessId',ea.id);
        dEAT.getMenuItems();
        dsmtEnergyAssessmentTemplateController.totalRecommendation(ea.id);
        dEAT.completeAssessmentv1();
        dEAT.completeAssessment();
        dEAT.getShowIAmDone();
        dEAT.getCustomerInfo();
       
    }
}