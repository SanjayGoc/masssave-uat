@isTest
Public class dsmtThermalRecommControllertest{
 
    public testmethod static void test1(){
        
        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c();
        insert tet;
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = proj.id);
        rc.Thermal_Envelope_Type__c=tet.id;
        insert rc;
   
   
        dsmtThermalRecommController cntrl= new dsmtThermalRecommController();
        cntrl.mechanicalId ='test';
        cntrl.mechanicalTypeId ='test';
        cntrl.recordTypeName ='test';
        cntrl.parentRecordTypeName ='test';
        cntrl.addOn ='test';
        cntrl.recommendation=rc;
        list<recommendation__c> rclist = cntrl.fetchThermalEnvRecom();
        cntrl.getAssignDefaultValues();
        cntrl.parentId = tet.id;
        cntrl.RecommendationId = rc.id;
        cntrl.DeleteRecommendation();
        //cntrl.saveNewReccom(); 
    
    
  }
}