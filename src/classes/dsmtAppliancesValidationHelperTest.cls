@isTest
public class dsmtAppliancesValidationHelperTest{
    public testmethod static void test1(){
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Trade_Ally_Account__c = ta.Id;
        ea.Dsmtracker_contact__c = dsmt.Id;
        insert ea;
        Appliance__c api = new Appliance__c();
        api.Energy_Assessment__c = ea.Id;
        api.Category__c = 'Laundry';
        api.Type__c = 'Washer';
        insert api;
        
        dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
        dsmtAppliancesValidationHelper cont = new dsmtAppliancesValidationHelper(); 
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Dryer';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
     }
     
     public testmethod static void test3(){
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Trade_Ally_Account__c = ta.Id;
        ea.Dsmtracker_contact__c = dsmt.Id;
        insert ea;
        Appliance__c api = new Appliance__c();
        api.Energy_Assessment__c = ea.Id;
        api.Category__c = 'Kitchen';
        api.Type__c = 'Dishwasher';
        insert api;
        
        dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
        dsmtAppliancesValidationHelper cont = new dsmtAppliancesValidationHelper(); 
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Range';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Cooktop';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Oven';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Refrigeration';
        update api;
        cont.validate('test','test',ea.Id,testWrap);
     }
     
     public testmethod static void test4(){
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Trade_Ally_Account__c = ta.Id;
        ea.Dsmtracker_contact__c = dsmt.Id;
        insert ea;
        Appliance__c api = new Appliance__c();
        api.Energy_Assessment__c = ea.Id;
        api.Category__c = 'Other';
        api.Type__c = 'Dehumidifier';
        insert api;
        
        dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
        dsmtAppliancesValidationHelper cont = new dsmtAppliancesValidationHelper(); 
        cont.validate('test','test',ea.Id,testWrap);
        
        api.Type__c = 'Aquarium';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Water Bed Heater';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Well Pump';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Sump Pump';
        update api;
        cont.validate('test','test',ea.Id,testWrap);
        
        api.Type__c = 'General';
        update api;
        cont.validate('test','test',ea.Id,testWrap);
        
        api.Type__c = 'Holiday Lights';
        update api;
        cont.validate('test','test',ea.Id,testWrap);
        
        api.Type__c = 'Other';
        update api;
        cont.validate('test','test',ea.Id,testWrap);
     }
     
     public testmethod static void test5(){
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Trade_Ally_Account__c = ta.Id;
        ea.Dsmtracker_contact__c = dsmt.Id;
        insert ea;
        Appliance__c api = new Appliance__c();
        api.Energy_Assessment__c = ea.Id;
        api.Category__c = 'Electronics';
        api.Type__c = 'TV';
        insert api;
        
        dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
        dsmtAppliancesValidationHelper cont = new dsmtAppliancesValidationHelper(); 
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Game Console';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Home Office';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
     }
     
     public testmethod static void test2(){
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        insert dsmt;
        
        Energy_Assessment__c ea = new Energy_Assessment__c();
        ea.Trade_Ally_Account__c = ta.Id;
        ea.Dsmtracker_contact__c = dsmt.Id;
        insert ea;
        Appliance__c api = new Appliance__c();
        api.Energy_Assessment__c = ea.Id;
        api.Category__c = 'Laundry';
        api.Type__c = 'Washer';
        api.Primary_Refrigerator__c  = true;
        api.Hot_Loads_Week__c = 35;
        api.Warm_Loads_Week__c = 35;
        api.Cold_Loads_Week__c = 35;
        api.Capacity__c = 1.0;
        api.Water_Factor__c = 1.0;
        api.Remaining_Moisture__c = 0.1;
        api.MEF__c = 0.1;
        api.Indoor_HangLoads_Week__c = 35;
        api.Dryer_Loads_Per_Week__c = 35;
        api.Fuel_Type__c = 'Natural Gas';
        api.Primary_Refrigerator__c  = true;
        api.Dishwasher_Loads_Per_Week__c = 24;
        api.Dishwasher_Energy_Factor__c = 0.1;
        api.Volume__c =1;
        api.Quantity__c = 11;
        api.Diagonal__c = 125;
        api.SetTop_Box_Quantity__c = 15;
        api.Number_of_DVRs__c = 15;
        api.Desktop_PCs__c = 11;
        api.Notebook_PCs__c = 11;
        api.Other_Equipment__c = 11;
        api.Routers__c = 11;
        api.Refrigerator_Type__c = 'Refrigerator Only';
        insert api;
        
        dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
        dsmtAppliancesValidationHelper cont = new dsmtAppliancesValidationHelper(); 
        cont.validate('test','test',ea.Id,testWrap);
                
        api.Type__c = 'Dryer';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Category__c = 'Kitchen';
        api.Type__c = 'Dishwasher';
        update api;
        cont.validate('test','test',ea.Id,testWrap); 
        
        api.Type__c = 'Refrigeration';
        update api;
        cont.validate('test','test',ea.Id,testWrap);
        
        api.Category__c = 'Electronics';
        api.Type__c = 'TV';
        update api;
        cont.validate('test','test',ea.Id,testWrap);
     }
}