public class dsmtUploadAttachmentCntrl {
    public dsmtUploadAttachmentCntrl () {
        
    }
    
    public PageReference redirectToUpload(){
        String utility = ApexPages.Currentpage().getParameters().get('u');
        String recId = ApexPages.Currentpage().getParameters().get('rid');
        String lookupColumn = ApexPages.Currentpage().getParameters().get('lc');
        String viewType = ApexPages.Currentpage().getParameters().get('vt');
        String applicationType = ApexPages.Currentpage().getParameters().get('at');
        String AttachToChild = ApexPages.Currentpage().getParameters().get('ac');
        String ChildLookupCol = ApexPages.Currentpage().getParameters().get('cl');
        String profileName = utility + ' Customer Portal User';
        List<Profile> pList = [SELECT id, Name FROM Profile Where Name = :profileName];
        
        
        String uploadURL = Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Attachment_Upload_Url__c;
        String partnerURL = Dsmt_Salesforce_Base_Url__c.getOrgDefaults().PartnerURL__c;
        if(pList != null && pList.size() > 0) {
            uploadURL  = Dsmt_Salesforce_Base_Url__c.getInstance(pList.get(0).Id).Attachment_Upload_Url__c;
        }
        
        PageReference pageRef = new PageReference(uploadURL != null ? uploadURL : '');
        addParameter('RecordId',recId, pageRef);
        addParameter('AttachmentLookupCol',lookupColumn, pageRef);
        addParameter('SessionID', UserInfo.getSessionId(), pageRef);
        addParameter('ServerURL',partnerURL, pageRef);
        addParameter('OrgId',UserInfo.getOrganizationId().substring(0, 15), pageRef);
        addParameter('AccountKey','nokia710lumia', pageRef);
        addParameter('SecurityKey', 'intelcore2duo', pageRef);
        addParameter('ViewType',viewType, pageRef);
        addParameter('theme',utility, pageRef);
        addParameter('ApplicationType', applicationType, pageRef);
        addParameter('AttachToChild', AttachToChild, pageRef);
        addParameter('ChildLookupCol', ChildLookupCol, pageRef);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public void addParameter(String param, String val, PageReference pageRef) {
        if(val != null) {
            pageRef.getParameters().put(param, val);
        }
    } 
}