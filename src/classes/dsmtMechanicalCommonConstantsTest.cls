@istest
public class dsmtMechanicalCommonConstantsTest
{
	@istest
    static void runtest()
    {
        dsmtMechanicalCommonConstants dmcc =new dsmtMechanicalCommonConstants();
        
        dsmtMechanicalCommonConstants.CommonDuctLeakinessFactorTable();
        dsmtMechanicalCommonConstants.CommonSupplyDuctSegmentInsulationAmountRValue();
        dsmtMechanicalCommonConstants.CommonReturnDuctSegmentInsulationAmountRValue();
        dsmtMechanicalCommonConstants.CommonPipeInsulationAmountValue();
        dsmtMechanicalCommonConstants.BaseboardPipeInsulationAmount();
        dsmtMechanicalCommonConstants.RadiatorsPipeInsulationAmount();
        dsmtMechanicalCommonConstants.RadiantPipeInsulationAmount();
        dsmtMechanicalCommonConstants.OnePipeSteamPipeInsulationAmount();
        dsmtMechanicalCommonConstants.TwoPipeSteamPipeInsulationAmount();
        dsmtMechanicalCommonConstants.GravityHotWaterPipeInsulationAmount();
        dsmtMechanicalCommonConstants.CommonCoolingSystemUsageAddTemp();
        dsmtMechanicalCommonConstants.CommonEfficiencyUnitConversionFactors();
        dsmtMechanicalCommonConstants.CommonCapacityUnitConversionFactors();
    }
}