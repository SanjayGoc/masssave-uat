public   class dsmtInvoiceCoverSheetController {
    
   public String invoiceId {get;set;}
      public String Id {get;set;}
     public String url{get;set;}
    public double invoiceTotal {get;set;}
    public List<TransactionWrapper> transactionList {get;set;}
     public String sdate {get;set;}
      Public Date mydate{get;set;}
    
    public dsmtInvoiceCoverSheetController(ApexPages.StandardController controller) {
        init();
    }
    
    private void init()
    {
        invoiceTotal = 0;
       List<Document > doc=[SELECT Id,Name FROM Document WHERE Name= 'CLEAResult Logo' Limit 1];
       Id=doc[0].Id;
        url= System.URL.getSalesforceBaseURL().toExternalForm(); 
        invoiceId = ApexPages.currentPage().getParameters().get('id');
         List<Invoice__c> inv=[Select Invoice_Date__c FROM Invoice__c where Id=:invoiceId Limit 1];
        //DateTime dT=inv[0].Invoice_Date__c;
       // myDate = date.newinstance(dT.year(),dT.month(),dT.day());
        sdate = String.valueOf(inv[0].Invoice_Date__c);
        sdate=sdate.remove('-');
        transactionList = new List<TransactionWrapper>();
        
        List<Invoice_Line_Item__c> invLineLst = [select id,MEA_Id__c,Recommendation_Description__c,QTY__c,Program_Price__c 
                                                    from Invoice_Line_Item__c where invoice__c =: invoiceId];
        
        Map<String,Double> qtyMap = new Map<String,Double>();
        Map<String,Double> priceMap = new Map<String,Double>();
        
        for(Invoice_Line_Item__c invl :  invLineLst){
        if(invl.QTY__c!=null){
            if(qtyMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null){
                qtyMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,invl.QTY__c);
            }else{
                qtyMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,qtyMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+invl.QTY__c);
            }
          }
          else{
               qtyMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,0);
          }
            if(priceMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c) == null && invl.Program_Price__c != null){
                priceMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,invl.Program_Price__c );
                
            }else{
                if(invl.Program_Price__c != null){
                    priceMap.put(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c,priceMap.get(invl.MEA_Id__c + '~~~'+invl.Recommendation_Description__c)+invl.Program_Price__c );
                }
            }
        }
        
       // transactionList.add(new TransactionWrapper('AIR_SEALING_E', 'Door Sweep', 11, 0));
       // transactionList.add(new TransactionWrapper('AIR_SEALING_E', 'Exterior Door Weather Strip', 12, 0));
        
        for(String str : qtyMap.keyset()){
            transactionList.add(new TransactionWrapper(str.split('~~~').get(0), str.split('~~~').get(1), integer.valueOf(qtyMap.get(str)), priceMap.get(str)));
           if(priceMap.get(str)!=null){
           invoiceTotal=invoiceTotal+ integer.valueOf(priceMap.get(str));
           }
        }
                                           
        
    }
  
    public class TransactionWrapper
    {
        public String title {get;set;}
        public String description {get;set;}
        public integer quantity {get;set;}
        public decimal programPrice {get;set;}
        
        public TransactionWrapper(String title,
                                  String description,
                                  integer quantity,
                                  decimal programPrice)
        {
            this.title = title;
            this.description = description;
            this.quantity = quantity;
            this.programPrice = programPrice;
           
        }
    }
   
}