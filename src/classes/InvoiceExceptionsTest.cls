@istest
public class InvoiceExceptionsTest
{
	@istest
    static void test()
    {
        Exception_Template__c et =new Exception_Template__c();
        et.Active__c=true;
        et.Active__c=true;
        et.Invoice_Level_Message__c=true;
        et.Reference_ID__c='NV-00001';
        insert et;
        
        Invoice__c iv =new Invoice__c();
        insert iv;
        
        List<Invoice__c> lstRev = new List<Invoice__c>();
        lstRev.add(iv);
        
        Map<Id,Invoice__c> mapRev = new Map<Id,Invoice__c>();
        mapRev.put(iv.Id,iv);
        
        /*Invoice_line_item__c ili =new Invoice_line_item__c();
        insert ili;*/
        
        Exception__c exc= new Exception__c();
        exc.Invoice__c=iv.id;
        exc.Disposition__c='';
        insert exc;
        
      
        InvoiceExceptions ie =new InvoiceExceptions();
        InvoiceExceptions.InsertExceptionsOnRR(lstRev);
        InvoiceExceptions.CheckUpdateExceptions(lstRev, mapRev);
    }
}