@isTest
public class dsmtMechanicalControllerTest 
{
    
    static testmethod void test1()
    {
        //Energy_Assessment__c ea=Datagenerator.setupAssessment();
        
        LightingAndApplianceConstant__c lac = new LightingAndApplianceConstant__c();
        lac.DefaultIndoorLightingPerSqFt__c = 10;
        lac.DefaultOutdoorLightingUsage__c = 'Medium';
        lac.DefaultOutdoorLightingPerSqFt__c = 10;
        insert lac;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        Workorder__c wo1 = new Workorder__c();
        wo1.Eligibility_Check__c = ec.id;
        wo1.Early_Arrival_Time__c = datetime.now();
        wo1.Late_Arrival_Time__c = datetime.now();        
        wo1.Scheduled_Start_Date__c = datetime.now();
        wo1.Scheduled_End_Date__c = datetime.now();
        insert wo1;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo1.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;
        
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Year_Built__c='1955';
        bs.Bedromm__c = 3;
        bs.Occupants__c = 3;
        bs.Orientation__c = 'South';
        bs.Ceiling_Heights__c = 8;
        bs.Floors__C=1.0;
        bs.Floor_Area__c = 1800;
        
        //bs.Appointment__c = appt.id;        
        
        insert bs;
        
        WallRatio__c wr = new WallRatio__c();
        wr.Name = 'Rectangular';
        wr.Value__c = 1;
        insert wr;
        
        FloorWallExp__c fw = new FloorWallExp__c();
        fw.Name = 'Rectangular';
        fw.Value__c = 1;
        insert fw;
        
        Saving_Constant__c sc = new Saving_Constant__c();     
        sc.FrontBackToLeftRightWindowAreaFactor__c = 10;
        sc.DefaultWindowFilmTotalSolarRejection__c = 10;
        sc.InsulationQuiltLengthOfNightFactor__c = 10;
        insert sc;
        
        List<WallInsulationAmountRValue__c> wiavList = new List<WallInsulationAmountRValue__c>();
        
        WallInsulationAmountRValue__c wiav = new WallInsulationAmountRValue__c();
        wiav.Name = 'None';
        wiav.Value__c = 11;
        wiavList.add(wiav);
        
        WallInsulationAmountRValue__c wiav1 = new WallInsulationAmountRValue__c();
        wiav1.Name = 'Fills Cavity'; 
        wiav1.Value__c = 11;
        wiavList.add(wiav1);
        
        WallInsulationAmountRValue__c wiav2 = new WallInsulationAmountRValue__c();
        wiav2.Name = 'Lots';
        wiav2.Value__c = 11;
        wiavList.add(wiav2);
        
        WallInsulationAmountRValue__c wiav3 = new WallInsulationAmountRValue__c();
        wiav3.Name = 'Some';
        wiav3.Value__c = 11;
        wiavList.add(wiav3);
        
        WallInsulationAmountRValue__c wiav4 = new WallInsulationAmountRValue__c();
        wiav4.Name = 'Standard';
        wiav4.Value__c = 11;
        wiavList.add(wiav4);
        
        WallInsulationAmountRValue__c wiav5 = new WallInsulationAmountRValue__c();
        wiav5.Name = 'Super';
        wiav5.Value__c = 11;
        wiavList.add(wiav5);
        
        WallInsulationAmountRValue__c wiav6 = new WallInsulationAmountRValue__c();
        wiav6.Name = 'Unknown';
        wiav6.Value__c = 11;
        wiavList.add(wiav6);
        
        insert wiavList;
        test.startTest();
        WeatherStation__c ws = new WeatherStation__c();
        ws.CDLatentMult__c = 4.02;
        ws.CDMultPCFM__c = 0.6568881;
        ws.CDMult__c = 1.125;
        ws.CExposeMult__c = 1.860;
        ws.CHeightExp__c = 0.326;
        ws.CNFactor__c= 38.31;
        ws.CSolarH__c= 115237.00;
        ws.CZ1_2__c= 0;
        ws.Clg1PctDesignTempDryBulb__c = 86.50;
        ws.Clg1PctDesignTempWetBulb__c = 71.60;
        ws.ClgGrDiff__c = 27.00;
        ws.Elevation__c = 62.32;
        ws.HDMultPCFM__c = 1.084081;
        ws.HDMult__c = 1.175;
        ws.HNFactor__c = 23.48;
        ws.HSolarH__c = 208026;
        ws.Htg99PctDesignTemp__c =  10.80;        
        ws.LightC__c = 0.8900;
        ws.LightH__c =1.06000;
        ws.LightSh__c = 0.90000;
        ws.TGround__c = 50;        
        ws.NormalNFactor__c = 18.00;
        insert ws;
        
        List<WeatherStationTemprature__c> wstList = new List<WeatherStationTemprature__c>();
        WeatherStationTemprature__c wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 58;
        wst.TempValues__c = '170,162,154,152,143,139,135,132,123,119,115,110,101,94,85,79,69,61,56,51,45,39,30,24,14,13,11';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CD';
        wstList.add(wst);
        
        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 40;
        wst.TempValues__c = '1113,1233,1359,1488,1623,1763,1910,2069,2233,2400,2572,2749,2933,3130,3331,3535,3752,3971,4198,4429,4664,4907,5159,5417,5690,5967,6249,6541,6837,7138,7446,7755,8074';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'HDD';
        wstList.add(wst);
        
        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 58;
        wst.TempValues__c = '3101,2942,2758,2636,2505,2374,2257,2120,1976,1832,1675,1552,1379,1241,1106,956,830,682,590,515,440,378,296,245,185,129,100';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CH';
        wstList.add(wst);
        
        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 50;
        wst.TempValues__c = '2701,2514,2335,2165,2004,1849,1701,1557,1420,1289,1161,1041,930,822,719,626,539,460,386,318,257,203,155,116,86,61,41,28,21,15,10';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CDD';
        wstList.add(wst);
        
        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 40;
        wst.TempValues__c = '2302,2513,2646,2760,2895,3041,3203,3422,3572,3667,3807,3941,4081,4297,4438,4537,4795,4907,5060,5200,5313,5487,5659,5813,6049,6157,6292,6447,6567,6722,6862,6947,7164';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'HH';
        wstList.add(wst);
        
        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 68;
        wst.TempValues__c = '12575,10900,9348,7969,6728,5622,4666,3836,3154,2564,2049,1609,1231,935,690,505,376';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CDH';
        wstList.add(wst);
        
        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 5;
        wst.TempStart__c = -5;
        wst.TempValues__c = '0,0.000,0.000,0.012,0.042,0.125,0.230,0.370,0.507,0.723,0.828,0.920,0.977,1.000';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'HL';
        wstList.add(wst);
        
        insert wstList;
        
        List<SolarGainByOperation__c> sgList = new List<SolarGainByOperation__c>();
        SolarGainByOperation__c sg = new SolarGainByOperation__c();
        sg.WeatherStation__c = ws.Id;
        sg.Orientation__c = 'South';
        sg.SolarGain__c = 53605;        
        sgList.add(sg);
        
        sg = new SolarGainByOperation__c();
        sg.WeatherStation__c = ws.Id;
        sg.Orientation__c = 'East';
        sg.SolarGain__c = 60520;        
        sgList.add(sg);
        insert sgList;
        
        Energy_Assessment__c ea =new Energy_Assessment__c();
        
        ea.Building_Specification__c = bs.id;   
        ea.Building_Shape__c = 'Rectangular';
        ea.WeatherStation__c = ws.id;
        insert ea;
        
        dsmtEAModel.Surface newSurfaceObj = new dsmtEAModel.Surface();
        newSurfaceObj.ws = ws;
        
        
        Pressure_Pan_Reading__c ppr = new Pressure_Pan_Reading__c();
        
        /*WeatherStation__c ws = new WeatherStation__c();
ws.NormalNFactor__c = 100.00;*/
        
        /*dsmtEAModel.Surface newSurfaceObj = new dsmtEAModel.Surface();
newSurfaceObj.ws = ws;

Pressure_Pan_Reading__c ppr = new Pressure_Pan_Reading__c();


Account  acc= Datagenerator.createAccount();
insert acc;

Customer__c cust = Datagenerator.createCustomer(acc.id,null);
insert cust;

Eligibility_Check__c EL= Datagenerator.createELCheck();
Program_Eligibility__c PE= Datagenerator.createProgramEL();
Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
Tacc.Trade_Ally_Type__c='HPC';
Tacc.Internal_Account__c = true;
update Tacc;

DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
dsmt.Trade_Ally_Account__c =Tacc.id; 
dsmt.Status__c = 'Approved';
dsmt.Trade_Ally_Type_PL__c = 'HPC';
dsmt.Email__c = 'test@ucs.com';
update dsmt;

System_Config__c config = new System_Config__c();
config.Arrival_Window_min_after_Appointment__c = 30;
config.Arrival_Window_min_before_Appointment__c = 10;
insert config;

Appointment__c app= Datagenerator.createAppointment(EL.Id);
app.DSMTracker_Contact__c=dsmt.id;
app.Customer_Reference__c = cust.Id;
app.Appointment_Status__c = 'Scheduled';

Workorder__c wo = new Workorder__c(Status__c='Unscheduled');
wo.Requested_Start_Date__c  = Datetime.now();
wo.Requested_End_Date__c  = Datetime.now().Addhours(2);
wo.Scheduled_Start_Date__c = Datetime.now();
wo.Scheduled_End_Date__c= Datetime.now().Addhours(2);
wo.Scheduled_Start_Date__c = datetime.newInstance(2018, 3, 2, 12, 30, 0);
wo.Scheduled_End_Date__c =  datetime.newInstance(2018, 3, 2, 13, 30, 0);

Workorder_Type__c woType = new Workorder_Type__c(Name='Wifi');
woType.Qualifying_Audit__c  = true;
insert woType;

wo.workorder_Type__c = woType.Id;
insert wo;

app.Workorder__c = wo.Id;
update app;*/
        
        Mechanical__c mech = new Mechanical__c();
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
        mech.Mechanical_Types__c = 'Custom';
        insert mech;
        
        Mechanical_Type__c mt = new Mechanical_Type__c();
        mt.Mechanical__c = mech.id;
        //mt.Default_Hidden_Distribution_Systems__c = 'Mini-Split A/C','Ductless Distribution System';
        mt.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Mini-Split A/C').getRecordTypeId();
        insert mt;
        
        Id RecordTypeIdMechanicalSub_Type1 = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId(); 
        Mechanical_Sub_Type__c mst=new Mechanical_Sub_Type__c();
        mst.RecordTypeId=RecordTypeIdMechanicalSub_Type1;
        mst.Tank_Gallons__c=24;
        mst.Mechanical__c=mech.Id;
        mst.Total_Leakage__c = 10;
        mst.Fuel_Type__c='Electricity';
        mst.Name = 'test';
        mst.Energy_Assessment__c = ea.id;
        
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Mechanical_Sub_Type__c').getDescribe().fields.getMap();
        
        dsmtEnergyAssessmentBaseController eacntrl = new dsmtEnergyAssessmentBaseController();
        
        
        ApexPages.currentPage().getParameters().put('mechanicalRecordTypeId',Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId());
        ApexPages.currentPage().getParameters().put('mechanicalRecordTypeName','Custom');
        ApexPages.currentPage().getParameters().put('mechanicalTypeRecordTypeName','ASHP');
        
        List<String> strlst = new List<String>{'ASHP','Boiler','Baseboard Distribution System','Boiler + Indirect Storage Tank',
            'Boiler + Tankless Coil','Central A/C','Combo','Combo DHW',
            'Custom','DFGSHP','DFHP','Distribution System',
            'Duct Distribution System','Ductless Distribution System','Electric Resistance','Evaporative Cooler',
            'Fireplace','Furnace','GSHP','Gas A/C',
            'Gas Heat Pump', 'Gas Pack','Gravity Hot Water Distribution System','Heat Pump',
            'High Velocity Duct Distribution System', 'Hydro Air Distribution System','Indirect Storage Tank','Kerosene Space Heater',
            'Mini-Split A/C', 'Mini-Split Heat Pump','On Demand','One Pipe Steam Distribution System',
            'Parlor Heater', 'Pellet Stove','Radiant Distribution System','Radiators Distribution System',
            'Steam Boiler', 'Storage Tank','Tankless Coil','  Thermostat',
            'Two Pipe Steam Distribution System', 'Wall Furnace','Window/Wall A/C','Wood Stove'};
                
                dsmtMechanicalController cntrl = new dsmtMechanicalController();
        cntrl.ea = ea;
        cntrl.selectedMechanical = mech;
        cntrl.selectedCustomMechanical = mech;
        cntrl.selectedMechanicalType = mt;
        cntrl.panReading = ppr;
        newSurfaceObj.mstObj = mst;
        
        test.stoptest();
        
        cntrl.getMechanicalRecordTypes();
        cntrl.getMechanicalList();
        cntrl.changeRecordType();
        cntrl.deleteMechanicalSubType();
        cntrl.getQueryMechanicalSubTypes();
        cntrl.addNewMechanicalType(); // DSST-11963 | Kaushik Rathore | 4 Sept, 2018
        cntrl.addNewCustomMechanicalType(); // DSST-11963 | Kaushik Rathore | 4 Sept, 2018
        cntrl.updateThermalSubTypes(); // // DSST-11963 | Kaushik Rathore | 4 Sept, 2018 CONTINUE
        cntrl.createSteamSubTypes();
        cntrl.addNewMechanicalSubType();
        cntrl.deleteMechanicalType();
        cntrl.updateAndOpenMechanicalType(mt.id);
        cntrl.editMechanicalType();
        //cntrl.updateThermalSubTypes(); // // DSST-11963 | Kaushik Rathore | 4 Sept, 2018 CONTINUE
        cntrl.saveMechanicalTypeDetails();
        cntrl.addPanReading();
        cntrl.deletePanReading();
        cntrl.validateMechanicalRequest();
        
        //cntrl.updateMechanicalTypes(mech.Id);
        //addNewMechanicalType(cntrl,'Cooling','Custom','DFHP');
        
        
    } 
    
    private static void addNewMechanicalType(dsmtMechanicalController cntrl, String mechanicalRecordType,String mechanicalRecordTypeName, String mechanicalTypeRecordTypeName){
        ApexPages.currentPage().getParameters().put('mechanicalRecordTypeId',Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get(mechanicalRecordType).getRecordTypeId());
        ApexPages.currentPage().getParameters().put('mechanicalRecordTypeName',mechanicalRecordTypeName);
        ApexPages.currentPage().getParameters().put('mechanicalTypeRecordTypeName',mechanicalTypeRecordTypeName);
        cntrl.addNewMechanicalType();
    }
    
    /*
static testmethod void test(){

Account  acc= Datagenerator.createAccount();
insert acc;

Customer__c cust = Datagenerator.createCustomer(acc.id,null);
insert cust;

Eligibility_Check__c EL= Datagenerator.createELCheck();
Program_Eligibility__c PE= Datagenerator.createProgramEL();
Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
Tacc.Trade_Ally_Type__c='HPC';
Tacc.Internal_Account__c = true;
update Tacc;

DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
dsmt.Trade_Ally_Account__c =Tacc.id; 
dsmt.Status__c = 'Approved';
dsmt.Trade_Ally_Type_PL__c = 'HPC';
dsmt.Email__c = 'test@ucs.com';
update dsmt;

System_Config__c config = new System_Config__c();
config.Arrival_Window_min_after_Appointment__c = 30;
config.Arrival_Window_min_before_Appointment__c = 10;
insert config;

Appointment__c app= Datagenerator.createAppointment(EL.Id);
app.DSMTracker_Contact__c=dsmt.id;
app.Customer_Reference__c = cust.Id;
app.Appointment_Status__c = 'Scheduled';
Workorder__c wo = new Workorder__c(Status__c='Unscheduled');
wo.Requested_Start_Date__c  = Datetime.now();
wo.Requested_End_Date__c  = Datetime.now().Addhours(2);
wo.Scheduled_Start_Date__c = Datetime.now();
wo.Scheduled_End_Date__c= Datetime.now().Addhours(2);
wo.Scheduled_Start_Date__c = datetime.newInstance(2018, 3, 2, 12, 30, 0);
wo.Scheduled_End_Date__c =  datetime.newInstance(2018, 3, 2, 13, 30, 0);
Workorder_Type__c woType = new Workorder_Type__c(Name='Wifi');
woType.Qualifying_Audit__c  = true;
insert woType;
wo.workorder_Type__c = woType.Id;
insert wo;
app.Workorder__c = wo.Id;
update app;

//Energy_Assessment__c ea = new  Energy_Assessment__c();
//ea.Appointment__c = app.Id;
//insert ea;

Mechanical__c mech = new Mechanical__c();
mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
insert mech;

Mechanical_Type__c mt = new Mechanical_Type__c();
mt.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('ASHP').getRecordTypeId();
insert mt;


dsmtEnergyAssessmentBaseController eacntrl = new dsmtEnergyAssessmentBaseController();
//eacntrl.ea = ea;

ApexPages.currentPage().getParameters().put('mechanicalRecordTypeId',Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId());
ApexPages.currentPage().getParameters().put('mechanicalRecordTypeName','Custom');
ApexPages.currentPage().getParameters().put('mechanicalTypeRecordTypeName','ASHP');

List<String> strlst = new List<String>{'ASHP','Boiler','Baseboard Distribution System','Boiler + Indirect Storage Tank',
'Boiler + Tankless Coil','Central A/C','Combo','Combo DHW',
'Custom','DFGSHP','DFHP','Distribution System',
'Duct Distribution System','Ductless Distribution System','Electric Resistance','Evaporative Cooler',
'Fireplace','Furnace','GSHP','Gas A/C',
'Gas Heat Pump', 'Gas Pack','Gravity Hot Water Distribution System','Heat Pump',
'High Velocity Duct Distribution System', 'Hydro Air Distribution System','Indirect Storage Tank','Kerosene Space Heater',
'Mini-Split A/C', 'Mini-Split Heat Pump','On Demand','One Pipe Steam Distribution System',
'Parlor Heater', 'Pellet Stove','Radiant Distribution System','Radiators Distribution System',
'Steam Boiler', 'Storage Tank','Tankless Coil','  Thermostat',
'Two Pipe Steam Distribution System', 'Wall Furnace','Window/Wall A/C','Wood Stove'};

dsmtMechanicalController cntrl = new dsmtMechanicalController();
//cntrl.ea = ea;
cntrl.selectedMechanical = mech;
cntrl.getMechanicalRecordTypes();
cntrl.getMechanicalList();
//cntrl.getQueryMechanicalSubTypes();
cntrl.addNewMechanicalType();
cntrl.addNewCustomMechanicalType();
cntrl.createSteamSubTypes();
//cntrl.createDefaultSubTypes(mt.id,'custom',strlst);



}
*/
}