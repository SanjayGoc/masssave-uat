@istest
public class CrawlspaceFloorComponentCntrlTest 
{
	@istest
     static void runTest()
     {
         Floor__c fl = new Floor__c ();
         insert fl;
         
         Layer__c ly =new Layer__c();
         insert ly;
         
         CrawlspaceFloorComponentCntrl cfcc = new CrawlspaceFloorComponentCntrl ();
         cfcc.crawlspaceFloorId =fl.Id;
         cfcc.getFloorLayersMap();
         
         cfcc.saveFloor();
         ApexPages.currentPage().getParameters().put('layerType','Flooring');
  		 cfcc.addNewFloorLayer();
     }
}