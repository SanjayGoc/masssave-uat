public class dsmtTicketsCntlrHPC{
    public String conName {get;set;}
    public String conId   {get;set;}
    public Dsmtracker_contact__c dsmtCon{get;set;}
    Set<Id> dsmtcId = new Set<Id>();
    Public Trade_Ally_Account__c  ta{get;set;}
    
    public String result1;
    public User u{get;set;}
    public String accid;
    public Boolean isTac;
    public String message_Filter {set;get;}
    public List<MessagesModel> listMessage{set;get;}
    public Boolean isMessageInfoOpen{set;get;}
    public String selectedMessageId{set;get;}
    public List<Attachment__c> listAtt{set;get;}
    public Service_Request__c openTickets{set;get;}
    public String cloneEAId{set;get;}
    public String paymentResult1;
    public Service_Request__c paymentMsg{set;get;}
    public boolean displayPopUp{get;set;}
    public Set<Id> eaIds = new Set<Id>();
    string conTanentId = '';
    // -------------- PAGINATION -----------//
    // Json Result For Ajax Responder 
    public JSONObject jsonx {get;set;}
    public Integer TotalRecordsCount {get;set;}
    public String selectedRowIds{get;set;}
    public JSONObject jsonxPayment {get;set;}
    public Integer TotalRecordsCountPayment {get;set;}
    public Boolean showColorBox{get;set;}
    public string messageJSON{get;set;}
    
    public string tdareAllyAccId;
    public string dsmtTrackerConId;
    
     public Integer UnreadMessage{get;set;}
    public dsmtTicketsCntlrHPC(){
        init();
      //  listOwnerId();
        fetchMessages();
        //initPaginationSorting();
        
    
    }
    
    public void init(){
        selectedRowIds = '';
        tdareAllyAccId = '';
        dsmtTrackerConId = '';
        openTickets = new Service_Request__c();
        paymentMsg = new Service_Request__c();
        displayPopUp = false;
        listAtt = new List<Attachment__c>();
        conName = conId = '';
        message_Filter = 'Inbox';
        selectedMessageId = '';
        //cloneEAId = '';
        isMessageInfoOpen = false;
        //eaIds = new Set<Id>();
        listMessage = new List<MessagesModel>();
        
        isTac   = false;
        //fetchContact();
        if(accid != null && accid.length() >14){
        }
        
    }
    
    
    
    public void backToMessage(){
        isMessageInfoOpen = false;
        //openMsg = new Messages__c();
        listAtt = new List<Attachment__c>();
    }
    
    public void fetchMessages(){
        //Set<String> eaIdSet = portalUtility.getEnrollAppIdsforLandingPages(); 
        String userId = userinfo.getUserId();
        Set<Id> setUserId = new Set<Id>();
        //setUserId = listOwnerId();                 
        String queryString = 'Select Id,Name,Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate  from Service_Request__c';
        
       
        UnreadMessage = 0;
        List<Service_Request__c> messageLst = [select id,name from Service_Request__c];
        if(messageLst != null && messageLst.size() > 0){
            UnreadMessage  = messageLst.size();
        }
        
        queryString += ' LIMIT 50';
        system.debug('--queryString---'+queryString);
        listMessage = new List<MessagesModel>();
        for(Service_Request__c msg : Database.query(queryString)){
            MessagesModel mod = new MessagesModel();
            mod.msg = msg;
            mod.msgno = msg.Name;
            
            listMessage.add(mod);
        }
        system.debug('--listMessage---'+listMessage);
    }
    public void initMessages(){
        
        Id srRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
         
        system.debug('--dsmtTrackerConId--'+dsmtTrackerConId);  
        
        messageJSON = '';
        String userId = userinfo.getUserId();
        
        List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: userId];
            if(usrList != null && usrList.size() > 0){
              //  if(usrList.get(0).ContactId != null){
                    conName = usrList.get(0).Contact.Name;
                    conId   = usrList.get(0).ContactId;
                    accId = usrList.get(0).Contact.AccountId;
                    String uid = usrList.get(0).id;
                    
                     List < DSMTracker_Contact__c > dsmtList = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c where Portal_User__c = : uId and Trade_Ally_Account__c != null limit 1];
                    if(dsmtList != null && dsmtList.size() > 0){
                        dsmtCon = dsmtList.get(0);
                        dsmtcId.add(dsmtCon.Id);
                        
                        List<Trade_Ally_Account__c> taList = [select id,name,Account__c,owner.Name,Owner.Phone,Owner.Email,Account_Manager__r.Name,Account_Manager__r.phone,Account_Manager__r.email,
                                                                Address__c,City__c,State__c,Zip_Code__c,Website__c,Phone__c,
                                                                Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,
                                                                Outreach_rep__r.Phone,Outreach_rep__r.Email,Outreach_rep__r.Name,(select id from DSMTracker_Contacts__r)
                                                                from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                        if(taList != null && taList.size() > 0){
                            ta = taList.get(0);
                            
                                for(Dsmtracker_Contact__c dsmt : ta.DSMTracker_Contacts__r){
                                   dsmtcId.add(dsmt.Id);
                                }
                        }
                    }
                }
        
        Set<Id> setUserId = new Set<Id>();
      // setUserId = listOwnerId();
        String queryString = 
            'SELECT '+
              'Id,Name,Type__c,Sub_Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate  '+
              'FROM Service_Request__c ' + 
              'WHERE Status__c != \'Closed\' AND Dsmtracker_Contact__c in: dsmtcId and RecordTypeId != :srRecordTypeId ';
        
            queryString += ' order by name desc LIMIT 50000';
            
            list<Service_Request__c> listMsg = Database.query(queryString);
            
            
            list<MessageModal> listMessage = new List<MessageModal>();
            for(Service_Request__c msg : Database.query(queryString)){
                 
                
                MessageModal mod = new MessageModal();
                mod.Id = msg.Id;
                mod.Name = msg.Name;
                mod.Type= msg.Type__c;
                mod.subType = msg.Sub_Type__c;
                mod.Subject = msg.Subject__c;
                mod.Status = msg.Status__c;
                mod.Priority = msg.Priority__c;
                mod.Description = msg.Description__c ;
                mod.CreatedDate = msg.CreatedDate.format();
                    
                listMessage.add(mod);
            }
            system.debug('--listMessage---'+listMessage);
            messageJSON = JSON.serialize(listMessage);
        
        system.debug('--listMessage---'+listMessage);
    }
    
    public class MessageModal{
        public string Id{get;set;}
        public string Name{get;set;}
        public string Subject{get;set;}
        public string Type{get;set;}
        public String subType{get;set;}
        public string Status{get;set;}
        public string Priority{get;set;}
        public string Description{get;set;}
        public String CreatedDate{get;set;}
    }
    
    
     
  
    public class MessagesModel{
        public Service_Request__c msg{set;get;}
        public Boolean isOpenedBlank{set;get;}
        public Boolean IsDraft{get;set;}
        public String msgno{get;set;}
        public MessagesModel(){
            msg = new Service_Request__c();
            isOpenedBlank = false;
            
        }
    }
    
    
    
    @RemoteAction
    public static Service_Request__c queryMessage(string mid) {
        return [Select Id,Name,Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,createddate  from Service_Request__c where Id = :mid];
    }
    
    @RemoteAction
    public static  String saveRespond(String subject,String type,String status,String priority,String body,string taID,string dsmtConId) {
        
        
        Service_Request__c dm = new Service_Request__c();
        
        dm.Subject__c = subject;
        dm.Type__c = type;
        dm.Status__c = status;
        dm.Priority__c = priority;
        dm.Description__c = body;
        if(taId!=null&&taId!=''){
            dm.Trade_Ally_Account__c = taId;
        }
        if(dsmtConId!=null && dsmtConId!='')
            dm.DSMTracker_Contact__c = dsmtConId;
        
        upsert dm;
        
        return 'success';
    }
    
    public List<SelectOption> getTicketType()    
    {    
        List<SelectOption> options =  new List<SelectOption>();    
        options.add(new selectOption('','--- None ---'));    
        Schema.DescribeFieldResult fieldResult = Service_Request__c.Type__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }
    
    public List<SelectOption> getTicketStatus()    
    {    
        List<SelectOption> options =  new List<SelectOption>();    
        options.add(new selectOption('','--- None ---'));    
        Schema.DescribeFieldResult fieldResult = Service_Request__c.Status__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }
    
    public List<SelectOption> getTicketPriority()    
    {    
        List<SelectOption> options =  new List<SelectOption>();    
        options.add(new selectOption('','--- None ---'));    
        Schema.DescribeFieldResult fieldResult = Service_Request__c.Priority__c.getDescribe();    
        List<Schema.picklistEntry> ple = fieldResult.getPicklistValues();    
        for(Schema.picklistEntry f:ple)    
        {    
            options.add(new selectOption(f.getLabel(),f.getValue()));                    
        }    
        return Options;    
    }  

}