public with sharing class dsmtPostInspectionReviewSummaryCntrl{
    
    public string recommXml {get;set;}
    public string prjId{get;set;}
    public Review__c rev{get;set;}
    public string dataSave{get;set;}
    public String revId{get;set;}
    public decimal subTotal {get;set;}
    public decimal subTotalVirtual {get;set;}
    public decimal utilityIncentive {get;set;}
    public decimal utilityIncentiveVirtual {get;set;}
    public decimal customerContribution {get;set;}
    public decimal customerContributionVirtual{get;set;}
    public decimal customerDeposit {get;set;}
    public decimal remainingCustomerContribution {get;set;}
     public decimal remainingCustomerContributionVirtual{get;set;}
    public decimal preWeatherization{get;set;}
    
    
    public List<SelectOption> projList {get;set;}
    
    public dsmtPostInspectionReviewSummaryCntrl(){
        rev = new Review__c();
        prjId = ApexPages.currentPage().getParameters().get('prjid');
        revId = ApexPages.currentPage().getParameters().get('id');
        
        //loadRecommendations();
    }
    
    public dsmtPostInspectionReviewSummaryCntrl(ApexPages.StandardController controller) {
        rev = new Review__c();
        prjId = ApexPages.currentPage().getParameters().get('prjid');
        revId = ApexPages.currentPage().getParameters().get('id');
        
        //loadRecommendations();
    }
    
    
    
    private set<String> fetchProjectIdSet(Id reviewId)
    {
        set<String> projectIdSet = new Set<String>();
        
        List<Project_Review__c> revList = [select id, Name, Review__c, Project__c 
                                           from Project_Review__c 
                                           where Review__c =: reviewId
                                           and Review__c != null];
        for(Project_Review__c pr : revList)
        {
            projectIdSet.add(pr.Project__c);
        }
        
        return projectIdSet;
    }
    
    /*public void CreateReviewRecord(){
        if(revId != null)
        {
            List<Review__C> lstPrj = [select Id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                        Energy_Assessment__r.Trade_Ally_Account__r.Email__c,First_Name__c,
                                        Last_Name__c,Phone__c,Email__c,Address__c,City__c,State__c,
                                        Zipcode__c,Trade_Ally_Account__c from Review__C 
                                        where Id =: revId limit 1]; 
            rev.Status__c = 'New';
            Id devRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();
    
            List<Dsmtracker_Contact__c> dsmtList = [select id,Dsmtracker_Contact__c,Trade_Ally_Account__c from Dsmtracker_Contact__c where Portal_User__c =: userinfo.getUSerID()
                                                        and Trade_Ally_Account__c  != null];
            if(dsmtList.size() > 0){
                rev.Trade_Ally_Account__c = dsmtList.get(0).Trade_Ally_Account__c;
            }                                   
            if(lstPrj.get(0).Energy_Assessment__c != null)    
                rev.Energy_Assessment__c = lstPrj.get(0).Energy_Assessment__c;
            
            rev.Type__c = 'Change Order Review';
            rev.First_Name__c = lstPrj.get(0).First_Name__c;
            rev.Last_Name__c = lstPrj.get(0).Last_Name__c;
            rev.Phone__c = lstPrj.get(0).Phone__c;
            rev.Email__c = lstPrj.get(0).Email__c;
            rev.Address__c = lstPrj.get(0).Address__c;
            rev.City__c = lstPrj.get(0).City__c;
            rev.State__c = lstPrj.get(0).State__c;
            rev.Zipcode__c = lstPrj.get(0).Zipcode__c;
            rev.Work_Scope_Review__c = lstPrj.get(0).Id;
            
            if(lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c != null)
                rev.Trade_Ally_Email__c = lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c;
            
            rev.RecordTypeId =  devRecordTypeId;  
            upsert rev; 
     
            rev = [select Id,Name,Project__c,Status__c,Work_Scope_Review__c,Trade_Ally_Account__r.Name,Energy_Assessment__r.Name from Review__c where Id = :rev.Id limit 1];
        }
    }*/
    
    public void loadInspectionLineItem()
    {
        
        string revId1 = ApexPages.currentPage().getParameters().get('id');
        String projects = ApexPages.currentPage().getParameters().get('projects');
        if(projList == null || projList.size() == 0)
        {
            projList = new List<SelectOption>();
            projList.add(new SelectOption('All', 'All Projects'));
        }
        
        remainingCustomerContribution = customerDeposit = utilityIncentive = subTotal = subTotalVirtual = customerContribution = preWeatherization = 0;
        utilityIncentiveVirtual  = customerContributionVirtual = remainingCustomerContributionVirtual  = 0 ;
        
        recommXml = '<InspectionLineItems>';
        
        Set<String> projIds = new Set<String>();
        
        if(revId  != null){
            projIds = fetchProjectIdSet(revId1);
        }else{
            String projId = ApexPages.currentPage().getParameters().get('projid');
            projIds.add(projId);
        }
        
        Set<Id> prjIds = new Set<Id>();
        map<string, decimal> projCollectedMap = new map<string, decimal>();
        
        system.debug('--projects--'+projects);
        if(projects != null && projects != 'All'){
            projIds = new Set<string>();
            projIds.add(projects);
        }
        
        Set<String> irIdSet = new Set<String>();
        Review__c postinspectionReview = null;
        if(revId != null){
            List<Review__c> reviewList = [select id,name,Inspection_Request__c,Inspection_Request__r.Review__c from Review__c Where id =: revId];
            
            if(reviewList.Size() > 0){
                irIdSet.add(reviewList.get(0).Inspection_Request__c);
                postinspectionReview = reviewList.get(0);
            }
        }
        system.debug('--billing review--'+postinspectionReview.Inspection_Request__r.Review__c);
        Map<Id,Decimal> changeOrderMap = new Map<Id,Decimal>();
        
        
        
        system.debug('--projIds--'+projIds);
        Set<String> recmIds = new Set<String>();
        list<Inspection_Line_Item__c> ILIList = null;
        Set<String> uniqueprojectIds = new Set<String>();
        
        //getting recommendation from projects from reviews 
        String query = '';
        query += 'select '+ sObjectFields('Recommendation__c') +' Recommendation_Scenario__r.Name,Recommendation_Scenario__r.Barrier_Incentive__c,';
        query += ' Recommendation_Scenario__r.Total_Collected__c,(select id,spec_d_Quantity__c,Installed_Quantity__c,Inspected_Quantity__c,Description__c,Flag_for_Billing_Adjustment__c,Status__c,Quick_Notes__c,Inspection_Request__c from Inspection_Line_Items__r where inspection_request__c =: irIdSet) ,(Select id,Project__c,Recommendation__c,Utility_Incentive_Share__c, Final_Recommendation_Incentive__c,Proposal__r.Current_Incentive__c,proposal__r.Final_Proposal_Incentive__c,proposal__r.Barrier_Incentive_Amount__c from Proposal_Recommendations__r where Project__c =:projIds) from Recommendation__c ';
        query += '  where Recommendation_Scenario__c in : projIds ';
        query += '  order by createddate desc,Recommendation_Scenario__r.name asc,Recommendation_Description__c asc limit 300'; 
        
        system.debug('--query--'+query);
        
        List<Recommendation__c> recommendlist = database.query(query);
        
        Set<Id> recId = new Set<Id>();
            
        for (Recommendation__c ir: recommendlist) {
            recId.add(ir.Id);
            changeOrderMap.put(ir.id,ir.Change_Order_Quantity__c);
        }
        
        
        /* if(recId.size() > 0){
            List<Review__c> changeRevList = [select id from Review__c where (Post_inspection_Review__c =: revId or Billing_Review__c =: postinspectionReview.Inspection_Request__r.Review__c)
                                                and RecordType.Name = 'Change Order Review' order by CreatedDate Desc];
            
            Set<String> changeorderIds = new Set<String>();
            
            if(changeRevList  != null && changeRevList.size() > 0){
                
                for(Review__c changeorder : ChangeRevList){
                    changeorderIds.add(changeorder.id);
                }
                    
                Set<Id> crId = new Set<Id>();
                
                for( Review__c c : changeRevList){
                    crId.add(c.Id);
                }
            
                List<Change_Order_Line_Item__c> chageorderList = [select id,Change_Order_Quantity__c,Recommendation__c from Change_Order_Line_Item__c
                                                                    where Recommendation__c in : recId and change_Order_Review__c in: changeorderIds order by CreatedDate Desc];
                for(Change_Order_Line_Item__c c : chageorderList){
                    if(changeOrderMap.get(c.Recommendation__c) == null && c.Change_Order_Quantity__c != null){
                        changeOrderMap.put(c.Recommendation__c,c.Change_Order_Quantity__c);
                    }
                }
            }
        } */
          Set<String> proposalIds = new Set<String>();
        for(Recommendation__c recomm : recommendlist){
            
            Proposal_Recommendation__c pr = null;

            if(!recomm.Proposal_Recommendations__r.isEmpty()) { 
                pr = recomm.Proposal_Recommendations__r.get(0);
            }
            
             if(pr != null){
                if(proposalIds.add(pr.proposal__c)){
                    System.debug('--Final_Proposal_Incentive'+pr.Proposal__r.Final_Proposal_Incentive__c);
                    System.debug('--Barrier_Incentive_Amount'+pr.Proposal__r.Barrier_Incentive_Amount__c);
                    utilityIncentiveVirtual += (pr.Proposal__r.Final_Proposal_Incentive__c - pr.Proposal__r.Barrier_Incentive_Amount__c);    
                }
                
                if(recomm.part_Cost__c != null && pr.Proposal__r.Current_Incentive__c != null){
                    System.debug('--PartCost - '+recomm.part_cost__c);
                    System.debug('--current_Incentive - '+pr.proposal__r.current_Incentive__c);
                    utilityIncentive += (recomm.part_cost__c * pr.proposal__r.current_Incentive__c * 0.01);
                }
            }
            
            system.debug('--changeOrderMap.get(recomm.id)--'+changeOrderMap.get(recomm.id));
            system.debug('recomm.Unit_Cost__c -- '+recomm.Unit_Cost__c);
            system.debug('recomm.Quantity__c -- '+recomm.Quantity__c);
            if(recomm.Unit_Cost__c != null && recomm.Quantity__c != null)
                System.debug('recomm_Unit_Cost__c -- '+recomm.Unit_Cost__c);
                System.debug('recomm_Quantity__c -- '+recomm.Quantity__c);
                subTotal += recomm.Unit_Cost__c * recomm.Quantity__c ;
            
            if(recomm.Unit_Cost__c != null && changeOrderMap.get(recomm.id) != null){
                subTotalVirtual += recomm.Unit_Cost__c * changeOrderMap.get(recomm.id);
            } else if(recomm.Unit_Cost__c != null && recomm.Quantity__c != null){
                subTotalVirtual += recomm.Unit_Cost__c * recomm.Quantity__c;
            }
            
            
            if(recomm.Recommendation_Scenario__c != null && uniqueprojectIds.add(recomm.Recommendation_Scenario__c)){   
                preWeatherization += recomm.Recommendation_Scenario__r.Barrier_Incentive__c; 
            }
            
            
            List<Inspection_Line_Item__c> lineitems = recomm.Inspection_Line_Items__r;
             system.debug('--recomm.Recommendation_Description__c --'+recomm.Recommendation_Description__c);
            
            if(lineitems != null && lineitems.size() > 0){
                
                Inspection_Line_Item__c line = lineitems.get(0);
                 
                if(recomm.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(recomm.Recommendation_Scenario__c + '~~~' + recomm.Recommendation_Scenario__r.Name, recomm.Recommendation_Scenario__r.Total_Collected__c);
               
                recommXml += '<InspectionLineItem>';
                
                if(recomm.Recommendation_Scenario__r.Name == null){
                    recommXml += '<ProjectName></ProjectName>';
                }else{
                    recommXml += '<ProjectName><![CDATA[<a href="/'+recomm.Recommendation_Scenario__c+'" target="_blank">' + recomm.Recommendation_Scenario__r.Name + '</a>]]></ProjectName>';                
                }
                
                recommXml += '<InspectionLineItemId>' + line.Id + '</InspectionLineItemId>';
                
                if(line.Description__c == null){
                    recommXml += '<RecommendationDesc></RecommendationDesc>';
                }else{
                    recommXml += '<RecommendationDesc><![CDATA[' + line.Description__c + ']]></RecommendationDesc>';
                }
                
                if(line.Spec_d_Quantity__c == null){
                    recommXml += '<Quantity></Quantity>';
                }else{
                    recommXml += '<Quantity><![CDATA[' + line.Spec_d_Quantity__c + ']]></Quantity>';
                }
                
                
                if(recomm.Installed_Quantity__c == null){
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                }else{
                    recommXml += '<InstalledQuantity><![CDATA[' + recomm.Installed_Quantity__c+ ']]></InstalledQuantity>';
                }
                
                if(line.Inspected_Quantity__c == null){
                    recommXml += '<InspectedQuantity></InspectedQuantity>';
                }else{
                    recommXml += '<InspectedQuantity><![CDATA[' + line.Inspected_Quantity__c + ']]></InspectedQuantity>';
                }
                
                recommXml += '<FlagforBillingAdjustment><![CDATA[' + line.Flag_for_Billing_Adjustment__c+ ']]></FlagforBillingAdjustment>';
                
                if(line.Status__c == null){
                    recommXml += '<Status></Status>';
                }else{
                    recommXml += '<Status><![CDATA[' + line.Status__c + ']]></Status>';
                }
                
                if(line.Quick_Notes__c == null){
                    recommXml += '<Note></Note>';                
                }else{
                    recommXml += '<Note><![CDATA[' + line.Quick_Notes__c + ']]></Note>';
                }
                
                recommXml += '</InspectionLineItem>';
                
                
                
                
                
            }else{
                
                if(recomm.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(recomm.Recommendation_Scenario__c + '~~~' + recomm.Recommendation_Scenario__r.Name, recomm.Recommendation_Scenario__r.Total_Collected__c);
               
                recommXml += '<InspectionLineItem>';
                
                if(recomm.Recommendation_Scenario__r.Name == null){
                    recommXml += '<ProjectName></ProjectName>';
                }else{
                    recommXml += '<ProjectName><![CDATA[<a href="/'+recomm.Recommendation_Scenario__c+'" target="_blank">' + recomm.Recommendation_Scenario__r.Name + '</a>]]></ProjectName>';                
                }
                
                if(recomm.Recommendation_Description__c == null){
                    recommXml += '<RecommendationDesc></RecommendationDesc>';
                }else{
                    recommXml += '<RecommendationDesc><![CDATA[' + recomm.Recommendation_Description__c + ']]></RecommendationDesc>';
                }
                
                recommXml += '<Quantity>0</Quantity>';
                
                if(recomm.Installed_Quantity__c == null){
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                }else{
                    recommXml += '<InstalledQuantity><![CDATA[' + recomm.Installed_Quantity__c+ ']]></InstalledQuantity>';
                }
                
                recommXml += '<InspectedQuantity>0</InspectedQuantity>';
                
                recommXml += '<FlagforBillingAdjustment>'+ true +'</FlagforBillingAdjustment>';
                
                recommXml += '<Status>No Action Required</Status>';
                
                if(recomm.Change_Order_Reason__c == null){
                    recommXml += '<Note></Note>';                
                }else{
                    recommXml += '<Note><![CDATA[' + recomm.Change_Order_Reason__c + ']]></Note>';
                }
                
                recommXml += '</InspectionLineItem>';
                
            }     
        } 
        
       
       /* if(projIds != null && projIds.size() > 0){
            ILIList = [select Recommendation__c,Recommendation__r.Quantity__c,Recommendation__r.Unit_Cost__c,Recommendation__r.Recommendation_Scenario__r.Name,
                                                    Spec_d_Quantity__c,Installed_Quantity__c,Inspected_Quantity__c,Description__c,Flag_for_Billing_Adjustment__c,
                                                    Status__c,Quick_Notes__c,Inspection_Request__c,Recommendation__r.Recommendation_Scenario__r.Total_Collected__c,
                                                    Recommendation__r.Recommendation_Scenario__r.Barrier_Incentive__c  
                                                    from Inspection_Line_Item__c where Recommendation__r.Recommendation_Scenario__c IN : projIds and Inspection_Request__c =: irIdSet order by CreatedDate desc limit 300];
        }else{
            ILIList = [select Recommendation__c,Recommendation__r.Quantity__c,Recommendation__r.Unit_Cost__c,Recommendation__r.Recommendation_Scenario__r.Name,
                                                    Spec_d_Quantity__c,Installed_Quantity__c,Inspected_Quantity__c,Description__c,Flag_for_Billing_Adjustment__c,
                                                    Status__c,Quick_Notes__c,Inspection_Request__c,Recommendation__r.Recommendation_Scenario__r.Total_Collected__c,
                                                    Recommendation__r.Recommendation_Scenario__r.Barrier_Incentive__c  
                                                    from Inspection_Line_Item__c where Inspection_Request__c =: irIdSet order by CreatedDate desc limit 300];
        }    
            
           
            for (Inspection_Line_Item__c ir: ILIList) {
                 
                 recmIds.add(ir.Recommendation__c);
                 
                if(ir.Recommendation__r.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(ir.Recommendation__r.Recommendation_Scenario__c + '~~~' + ir.Recommendation__r.Recommendation_Scenario__r.Name, ir.Recommendation__r.Recommendation_Scenario__r.Total_Collected__c);
               
                recommXml += '<InspectionLineItem>';
                
                if(ir.Recommendation__r.Recommendation_Scenario__r.Name == null){
                    recommXml += '<ProjectName></ProjectName>';
                }else{
                    recommXml += '<ProjectName><![CDATA[<a href="/'+ir.Recommendation__r.Recommendation_Scenario__c+'" target="_blank">' + ir.Recommendation__r.Recommendation_Scenario__r.Name + '</a>]]></ProjectName>';                
                }
                
                recommXml += '<InspectionLineItemId>' + ir.Id + '</InspectionLineItemId>';
                
                if(ir.Description__c == null){
                    recommXml += '<RecommendationDesc></RecommendationDesc>';
                }else{
                    recommXml += '<RecommendationDesc><![CDATA[' + ir.Description__c + ']]></RecommendationDesc>';
                }
                
                if(ir.Spec_d_Quantity__c == null){
                    recommXml += '<Quantity></Quantity>';
                }else{
                    recommXml += '<Quantity><![CDATA[' + ir.Spec_d_Quantity__c + ']]></Quantity>';
                }
                
                
                if(ir.Installed_Quantity__c == null){
                    recommXml += '<InstalledQuantity></InstalledQuantity>';
                }else{
                    recommXml += '<InstalledQuantity><![CDATA[' + ir.Installed_Quantity__c+ ']]></InstalledQuantity>';
                }
                
                if(ir.Inspected_Quantity__c == null){
                    recommXml += '<InspectedQuantity></InspectedQuantity>';
                }else{
                    recommXml += '<InspectedQuantity><![CDATA[' + ir.Inspected_Quantity__c + ']]></InspectedQuantity>';
                }
                
                recommXml += '<FlagforBillingAdjustment><![CDATA[' + ir.Flag_for_Billing_Adjustment__c+ ']]></FlagforBillingAdjustment>';
                
                if(ir.Status__c == null){
                    recommXml += '<Status></Status>';
                }else{
                    recommXml += '<Status><![CDATA[' + ir.Status__c + ']]></Status>';
                }
                
                if(ir.Quick_Notes__c == null){
                    recommXml += '<Note></Note>';                
                }else{
                    recommXml += '<Note><![CDATA[' + ir.Quick_Notes__c + ']]></Note>';
                }
                
                recommXml += '</InspectionLineItem>';
                
                
                if(ir.Recommendation__r.Unit_Cost__c != null && ir.Recommendation__r.Quantity__c != null)
                    subTotal += ir.Recommendation__r.Unit_Cost__c * ir.Recommendation__r.Quantity__c ;
                
                if(ir.Recommendation__r.Unit_Cost__c != null && changeOrderMap.get(ir.Recommendation__c) != null){
                    subTotalVirtual += ir.Recommendation__r.Unit_Cost__c * changeOrderMap.get(ir.Recommendation__c);
                } else if(ir.Recommendation__r.Unit_Cost__c != null && ir.Recommendation__r.Quantity__c != null){
                    subTotalVirtual += ir.Recommendation__r.Unit_Cost__c * ir.Recommendation__r.Quantity__c;
                }
                
                if(ir.Recommendation__r.Recommendation_Scenario__c != null && uniqueprojectIds.add(ir.Recommendation__r.Recommendation_Scenario__c)){   
                    preWeatherization += ir.Recommendation__r.Recommendation_Scenario__r.Barrier_Incentive__c; 
                }
                
            } */
        
        recommXml += '</InspectionLineItems>';

        customerContribution = subTotal - utilityIncentive;
        if(customerContribution != null){
            customerContribution.setScale(2);
        }
        customerContributionVirtual = subTotalVirtual - utilityIncentiveVirtual;
        
        boolean isAdd = false;
        
        if(projList.size() == 1)
            isAdd = true;
        
        for(String s : projCollectedMap.keyset())
        {
            customerDeposit += projCollectedMap.get(s);
            
            if(isAdd)
                projList.add(new SelectOption(s.split('~~~')[0], s.split('~~~')[1]));
        }
        
        remainingCustomerContribution = (customerContribution - customerDeposit);
        remainingCustomerContributionVirtual  = (customerContributionVirtual - customerDeposit);
        
    } 
    
     private static String sObjectFields(String sObjName) {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
}