public class dsmtCancelWorkorderCntrl{
    
    public Workorder__c cancelWO {get;set;}
    public string error {get;set;}
    
    public dsmtCancelWorkorderCntrl(){
        cancelWO = new Workorder__c();
        init();
    }
    
    public void init(){
        list<Workorder__c> woList = [select id, Name, Cancellation_Notes__c, Cancel_Reason__c,External_Reference_ID__c,
                                     Call_me_back_to_reschedule__c, Reschedule_Callback_Date__c,Status__c,Scheduled_Date__c
                                     from Workorder__c
                                     where id =: ApexPages.currentPage().getParameters().get('id')];
         if(woList.size() > 0)
             cancelWo = woList[0];
    }
    
    public void getAppt(){     
    
    }
    
    public PageReference CancelWorkOrder(){
        
        String str1 = cancelWO.External_Reference_ID__c;
        String str = '';
        Date dt = cancelWo.Scheduled_Date__c;
        String str2 = cancelWo.Status__c;
        if(dt<system.today()){
            List<workOrder__c> woList = [select id,Cancel_Reason__c,Cancellation_Notes__c,
                                             Call_me_back_to_reschedule__c, Reschedule_Callback_Date__c
                                             from workorder__c where id =: cancelWo.Id];
                
                Note noteObj = new Note(ParentId = cancelWo.Id,
                     Body = cancelWO.Cancellation_Notes__c,
                     Title = cancelWO.Cancel_Reason__c);
                insert noteObj;
                
                if(woList != null && woList.size() > 0){
                    woList.get(0).Cancel_Reason__c = cancelWO.Cancel_Reason__c;
                    woList.get(0).Cancellation_Notes__c = cancelWO.Cancellation_Notes__c;
                    woList.get(0).Call_me_back_to_reschedule__c = cancelWO.Call_me_back_to_reschedule__c;
                    woList.get(0).Reschedule_Callback_Date__c = cancelWO.Reschedule_Callback_Date__c;
                    woList.get(0).Status__c = 'Cancelled';
                    update woList;
                }
                return new PageReference('/'+cancelWo.Id);       
        }
        else{
            if(str1!=null && str1!='')
            {
                str = dsmtWorkOrderHelperController.updateWorkOrder(cancelWO.Id+'|'+cancelWO.External_Reference_ID__c,'Cancelled');
            }
            else
            {
                str = dsmtWorkOrderHelperController.updateWorkOrder(cancelWO.Id,'Cancelled');
            }    
            
            error = '';
            try{
                if(str  == '"Success"' || Test.IsrunningTest()){
                    
                    List<workOrder__c> woList = [select id,Cancel_Reason__c,Cancellation_Notes__c,
                                                 Call_me_back_to_reschedule__c, Reschedule_Callback_Date__c
                                                 from workorder__c where id =: cancelWo.Id];
                    
                    Note noteObj = new Note(ParentId = cancelWo.Id,
                         Body = cancelWO.Cancellation_Notes__c,
                         Title = cancelWO.Cancel_Reason__c);
                    insert noteObj;
                    
                    if(woList != null && woList.size() > 0){
                        woList.get(0).Cancel_Reason__c = cancelWO.Cancel_Reason__c;
                        woList.get(0).Cancellation_Notes__c = cancelWO.Cancellation_Notes__c;
                        woList.get(0).Call_me_back_to_reschedule__c = cancelWO.Call_me_back_to_reschedule__c;
                        woList.get(0).Reschedule_Callback_Date__c = cancelWO.Reschedule_Callback_Date__c;
                        woList.get(0).Status__c = 'Cancelled';
                        update woList;
                    }
                    return new PageReference('/'+cancelWo.Id);
                    
                }else{
                    error = str;  
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, error);
                    ApexPages.addMessage(myMsg); 
                    return null;  
                }
            }
            catch(Exception e1){
              try{
                  str = dsmtWorkOrderHelperController.updateWorkOrder(cancelWO.Id,'Cancelled');
                 
                  if(str  == '"Success"' || Test.IsrunningTest()){
                  
                  }
              }catch(Exception e2){
                  str = dsmtWorkOrderHelperController.updateWorkOrder(cancelWO.Id,'Cancelled');
                 
                  if(str  == '"Success"' || Test.IsrunningTest()){
                  
                  }   
                  else{
                      String Labelstr= Label.DSMTracker_Log_Notification;
                      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.severity.WARNING,Labelstr);
                      ApexPages.addMessage(myMsg);
                      DSMTracker_Log__c dsmlog = new DSMTracker_Log__c();
                      dsmlog.Send_Notification_Email__c = true;
                      dsmlog.Type__c = 'Cancel Workorder';
                      dsmlog.Response__c = e2.getMessage();
                      dsmlog.request__c = Cancelwo.Id;
                      dsmlog.Time__c = DateTime.Now();
                      insert dsmlog;
                      return null;
                  }           
              }
             
             return new PageReference('/'+cancelWo.Id);
             }  
        }    
    }
}