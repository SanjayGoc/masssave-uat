/*@IsTest
public with sharing class dsmtAddEditScheduleRequestCntrlTest {
	@IsTest
    //static void validate_dsmtAddEditScheduleRequestCntrl_id(){
    static void runtest(){
        test.startTest();
        
        // Dummy Data
        
        User PortalUser = Datagenerator.CreatePortalUser();
        DSMTracker_Contact__c newDSMTracker = Datagenerator.createDSMTracker();
        Trade_Ally_Account__c newTAaccount = Datagenerator.createTradeAccount();
        Attachment__c newAtt = Datagenerator.createAttachment(newTAaccount.Id);
        
        Id RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
        Service_Request__c sr = new Service_Request__c(
            Type__c = 'Schedule Request',
            RecordTypeId = RecordTypeId,
            Dsmtracker_Contact__c = newDSMTracker.Id,
            Dsmtracker_Contact_Email__c = newDSMTracker.email__c,
            Dsmtracker_Contact_Name__c = newDSMTracker.Name,
            Trade_Ally_Account__c = newTAaccount.Id
        ); 
        insert sr;
        
        newAtt.Service_Request__c = sr.Id;
        // Test Case
        System.runAs(portalUser){
            PageReference pageRef = Page.dsmtAddEditScheduleRequest;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',sr.Id);
    
            dsmtAddEditScheduleRequestCntrl cntrl = new dsmtAddEditScheduleRequestCntrl();
            cntrl.body = null;
            cntrl.ConId = newDSMTracker.Id;
            cntrl.dsmtCon = newDSMTracker;
            cntrl.ta = newTAaccount;
            cntrl.PortalURL = null;
            cntrl.orgId = null;
            
            cntrl.SaveAsDraft();
            cntrl.SaveAsSubmit();
            
            String json_getAttachmentsBysrId = dsmtAddEditScheduleRequestCntrl.getAttachmentsBysrId(sr.Id);
            System.assertNotEquals(null,json_getAttachmentsBysrId);
            
            
        }
        
        String json_delAttachmentsById = dsmtAddEditScheduleRequestCntrl.delAttachmentsById(newAtt.Id,sr.Id);
        System.assertNotEquals(null,json_delAttachmentsById);
        
        test.stopTest();
    }
    
    @IsTest
    static void validate_dsmtAddEditScheduleRequestCntrl_srid(){
        test.startTest();
        
        // Dummy Data
        
        User PortalUser = Datagenerator.CreatePortalUser();
        
        List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: PortalUser.Id];
        DSMTracker_Contact__c newDSMTracker = Datagenerator.createDSMTracker();
        newDSMTracker.Contact__c = usrList.get(0).contactid;
        
        Trade_Ally_Account__c newTAaccount = Datagenerator.createTradeAccount();
        
        Id RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
        Service_Request__c sr = new Service_Request__c(
            Type__c = 'Schedule Request',
            RecordTypeId = RecordTypeId,
            Dsmtracker_Contact__c = newDSMTracker.Id,
            Dsmtracker_Contact_Email__c = newDSMTracker.email__c,
            Dsmtracker_Contact_Name__c = newDSMTracker.Name,
            Trade_Ally_Account__c = newTAaccount.Id
        ); 
        insert sr;
        
        Attachment__c newAtt = Datagenerator.createAttachment(newTAaccount.Id);
        newAtt.Service_Request__c = sr.Id;
        
        update newDSMTracker;
        update newAtt;
        // Test Case
        
            System.runAs(portalUser){
                PageReference pageRef = Page.dsmtAddEditScheduleRequest;
                Test.setCurrentPage(pageRef);
                pageRef.getParameters().put('srid','');
        
                dsmtAddEditScheduleRequestCntrl cntrl = new dsmtAddEditScheduleRequestCntrl();
                cntrl.body = null;
                cntrl.ConId = newDSMTracker.Id;
                cntrl.dsmtCon = newDSMTracker;
                cntrl.ta = newTAaccount;
                cntrl.PortalURL = null;
                //cntrl.orgId = null;
                
                cntrl.SaveAsDraft();
                cntrl.SaveAsSubmit();
                
                String json_getAttachmentsBysrId = dsmtAddEditScheduleRequestCntrl.getAttachmentsBysrId(sr.Id);
                System.assertNotEquals(null,json_getAttachmentsBysrId);
                
            }
        	String json_delAttachmentsById = dsmtAddEditScheduleRequestCntrl.delAttachmentsById(newAtt.Id,sr.Id);
        	System.assertNotEquals(null,json_delAttachmentsById);
        test.stopTest();
    }
}*/
@istest
public class dsmtAddEditScheduleRequestCntrlTest
{
 	@istest
    static void runtest()
        {
            Service_Request__c sr =new Service_Request__c();
            sr.Subject__c='test';
            sr.Sub_Type__c='';
            sr.Type__c='Both';
            insert sr;
            
            /*Dsmtracker_contact__c dc =new Dsmtracker_contact__c();
            
            inseret dc;*/
            dsmtAddEditScheduleRequestCntrl dasr =new dsmtAddEditScheduleRequestCntrl();  
        }
}