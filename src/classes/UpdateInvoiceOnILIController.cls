public class UpdateInvoiceOnILIController
{
    public String iliIds {get;set;}
    public UpdateInvoiceOnILIController(ApexPages.StandardController controller) {
        init();
    }

    public Invoice_Line_Item__c ili {get;set;}
    public List<Invoice_Line_Item__c> iliList {get;set;}
    
    public UpdateInvoiceOnILIController()
    {
        init();
    }
    
    public void init()
    {
        ili = new Invoice_Line_Item__c();
        ili.Invoice__c = ApexPages.currentPage().getParameters().get('id');
        
        iliList = new List<Invoice_Line_Item__c>();
        
        iliList = [select id, Name, Invoice__c, Recommendation__c, CreatedDate, LastModifiedDate 
                   from Invoice_Line_Item__c
                   where Invoice__c =: ApexPages.currentPage().getParameters().get('id')
                   order by Name];
    }
    
    public PageReference saveILI()
    {
        system.debug('ili.Invoice__c :::::'+ ili.Invoice__c);
        List<Invoice_Line_Item__c> iliUpdateList = new List<Invoice_Line_Item__c>();
        
        List<String> iliIdList = iliIds.split('~~~');
        
        for(String s : iliIdList)
        {
            if(s != '')
            {
                iliUpdateList.add(new Invoice_Line_Item__c(Invoice__c = ili.Invoice__c,
                                                           id = s));
            }
        }
        
        if(iliUpdateList.size() > 0)
            update iliUpdateList;
        
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('id'));
    }
}