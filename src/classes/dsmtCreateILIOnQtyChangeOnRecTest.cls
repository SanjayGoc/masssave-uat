@isTest
Public class dsmtCreateILIOnQtyChangeOnRecTest {
    
    @isTest
    public static void test1(){
        Recommendation__c rec = new Recommendation__c(Quantity__c = 10);
        insert rec;
        
        Invoice__c inv = new Invoice__c();
        insert inv;
        Invoice_Line_Item__c ili = new Invoice_Line_Item__c(Invoice__c = inv.id, Fuel_Type__c = 'test', Recommendation__c = rec.id, QTY__c = 10, Unit_Cost__c = 10, Reversal_Created__c = false);
        insert ili;
        
        rec.Quantity__c = 12;
        update rec;
    }
}