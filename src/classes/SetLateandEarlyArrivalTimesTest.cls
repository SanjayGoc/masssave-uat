@isTest
public class SetLateandEarlyArrivalTimesTest {
    static testmethod void testmethod1(){
        
        Time myTime = Time.newInstance(8, 3, 3, 0);
        DateTime dt = DateTime.newInstance(Date.today(), myTime);
        
        Location__c loc= Datagenerator.createLocation();
        insert loc;
        Employee__c em = Datagenerator.createEmployee(loc.id);
        em.Employee_Id__c = 'test123';
        em.Status__c='Approved';
        insert em;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, em.Id);
        wt.Start_Date__c = dt;
        insert wt;
        Workorder__c wo = new Workorder__c (Scheduled_Start_Date__c=dt,Scheduled_End_Date__c=dt.addminutes(20),Work_Team__c =wt.Id);
        insert wo;
        
        wo.Scheduled_Start_Date__c = dt.addminutes(20);
        update wo;
    }
    
}