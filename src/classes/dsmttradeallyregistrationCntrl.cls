public class dsmttradeallyregistrationCntrl {
    
    public Trade_Ally_Account__c objTAC{get;set;}
    public Registration_Request__c objRR{get;set;}
    public DSMTracker_Contact__c objDSMTContact{get;set;}
    public string RRId{get;set;}
    
    public String cmpny_name{get;set;}
    public String Address{set;get;}
    public String City{set;get;}
    public String State{set;get;}
    public String Zip{set;get;}
    public String Username{get;set;}
    public String password{get;set;}
    public String confirmPassword{get;set;}
    
    public boolean failedRegistration {get;set;} 
    
    public string labelHeader{get;set;}
    public string helpNote{get;set;}
    public string helpNoteTitle{get;set;}
    public string sectionNote{get;set;}
    public string sectionName{get;set;}
    
    public boolean isRegisterd{get;set;}
    public boolean isForDSMTContact{get;set;}
    Datetime EulaDAte = null;
    
    public string accountId;
    public Checklist__c pgCheckList{get{return [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Trade_Ally_Registration_Form' LIMIT 1];}}
    
    public dsmttradeallyregistrationCntrl(){
        objTAC = new Trade_Ally_Account__c();
        objRR = new Registration_Request__c();
        objDSMTContact = new DSMTracker_Contact__c();
        failedRegistration = false;
        isRegisterd = false;
        isForDSMTContact = false;
        
        for(Checklist_Items__c chi :getChecklists()){
            if(chi.Help_text_for_public_portal__c){
                helpNote = chi.Checklist_Information__c;
                helpNoteTitle = chi.Section__c;
            }
            else if(chi.Special_instruction_for_public_portal__c){
                sectionNote = chi.Checklist_Information__c;
                sectionName = chi.Section__c;
            }
        } 
        
        string encodeRRId = ApexPages.currentPage().getParameters().get('id');
        string dcId = ApexPages.currentPage().getParameters().get('dcid');
        if(encodeRRId != null && encodeRRId != ''){
            
            //RRId = EncodingUtil.base64Decode(encodeRRId ).tostring();
            system.debug('@@RRID@@'+encodeRRId );
            List<Registration_Request__c> lstRR = [select Id,Name,First_Name__c,Last_Name__c,Email__c,Phone__c,DSMTracker_Contact__c,DSMTracker_Contact__r.DSMT_Contact_Number__c ,DSMTracker_Contact__r.Contact__c,Registration_Number__c,Trade_Ally_Account__r.Name,Steps_Completed__c,
                                                    Trade_Ally_Account__r.Account__c,Trade_Ally_Account__r.EULA_Agreement_Date__c  from Registration_Request__c where Registration_Number__c= :encodeRRId ];
            
            if(lstRR.size()>0){
                objRR = lstRR[0];
                RRId = objRR.Id;
                EulaDAte  = objRR.Trade_Ally_Account__r.EULA_Agreement_Date__c;
                String dsmtId = objRR.DSMTracker_Contact__r.DSMT_Contact_Number__c;
                cmpny_name = objRR.Trade_Ally_Account__r.Name;
                Username = objRR.Email__c;
                
                if((dcId == null || dcId.trim() == '') && objRR.Steps_Completed__c != '1'){
                    isRegisterd = true;
                }
                
                if(dcId != null && dcId.trim() != ''){
                    isForDSMTContact = true;
                    dsmtId = dcId;
                }
                system.debug('--dsmtId--'+dsmtId);
                List<DSMTracker_Contact__c> dsmTrackConList = [select id,Contact__c,Portal_Role__c,Company_Name__C,First_Name__c,Last_Name__c,Phone__c,Account__c,Account__r.Name,Email__c from DSMTracker_Contact__c where DSMT_Contact_Number__c = :dsmtId]; 
                if(dsmTrackConList.size()>0 && dsmTrackConList.get(0).contact__c != null){
                    objDSMTContact = dsmTrackConList.get(0);
                    if(dcId != null && dcId.trim() != ''){
                        cmpny_name = objDSMTContact.Company_Name__C;
                        Username = objDSMTContact.Email__c;
                    }
                    List<User> userList = [select id from user where contactid = :objDSMTContact.contact__c];
                    if(userList.size()>0){
                        isRegisterd = true;
                    }
                }
                system.debug('--objDSMTContact --'+objDSMTContact);
                
            }
        }else if(dcId != null && dcId != ''){
            isForDSMTContact = true;
            List<DSMTracker_Contact__c> dsmTrackConList = [select id,Contact__c,Portal_Role__c,Company_Name__C,First_Name__c,Last_Name__c,Phone__c,Account__c,Account__r.Name,Email__c from DSMTracker_Contact__c where DSMT_Contact_Number__c = :dcId]; 
            objDSMTContact = dsmTrackConList.get(0);
            cmpny_name = objDSMTContact.Company_Name__C;
            Username = objDSMTContact.Email__c;
            system.debug('--objDSMTContact:dsId --'+objDSMTContact);
        }
    }
    
    /******************* Fetching States from Custom Settings*******/
    
    public List<SelectOption> getCountriesSelectList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Select State --'));        

        // Find all the countries in the custom setting
        Map<String, dsmt_Reg_States__c> states = dsmt_Reg_States__c.getAll();
       // Sort them by name
        List<String> stateNames = new List<String>();
        stateNames.addAll(states.keySet());
        stateNames.sort();
        //// System.debug('------country Names--'+stateNames);
      
        for (String statName : stateNames) {
            dsmt_Reg_States__c state = states.get(statName);
           // // System.debug('e state.State_Name__c------'+states.State_Name__c+'country.Name-----'+states.Name) ;
            options.add(new SelectOption(state.State_Name__c, state.Name));
        }
        // System.debug('--------'+options);
        return options;
    }
    
    public PageReference registrationClick(){
        failedRegistration = false;
         System.debug('------------------------------');
        // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            failedRegistration = true;
            system.debug('@@message@@'+msg);
            return null;
        }
        if (!isValidPasswordLength()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Password must be at least 10 characters long.');
            ApexPages.addMessage(msg);
            failedRegistration = true;
            return null;
        }
        if (!isValidUserName(Username)) {
            ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Username is already registered');
            ApexPages.addMessage(msg3);
            failedRegistration = true;
            return null;
        }
        if (!isValidCommunityNickname(Username)) {
            ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Username is already registered');
            ApexPages.addMessage(msg3);
            failedRegistration = true;
            return null;
        }
      
        
        Pattern p = Pattern.compile( '([a-zA-Z0-9+_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))');
        Matcher m = p.matcher(Username);
        if (!m.matches()) {
            ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Username in the Email format');
            ApexPages.addMessage(msg3);
            failedRegistration = true;
            return null;
        }
         
         if((!isForDSMTContact && objRR.Phone__c != null && objRR.Phone__c.length() != 14) || (isForDSMTContact && objDSMTContact.Phone__c != null && objDSMTContact.Phone__c.length() != 14)){
           ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Phone no must be 10 digit.');
            ApexPages.addMessage(msg3);
            failedRegistration = true;
            return null;
        }
      
        User cust = new User();
        String portalRole = '';
        if(isForDSMTContact){
            cust.firstname=objDSMTContact.First_Name__c;
            cust.lastname=objDSMTContact.Last_Name__c;
            cust.phone=objDSMTContact.Phone__c;
            cust.Email=objDSMTContact.Email__c;
        }else{
            cust.firstname=objRR.First_Name__c;
            cust.lastname=objRR.Last_Name__c;
            cust.phone=objRR.Phone__c;
            cust.Email=objRR.Email__c;
        }
        portalRole = objDSMTContact.Portal_Role__c;
        cust.UserName=Username;
        cust.CompanyName=cmpny_name;
        cust.City=City;
        cust.State =State;
        cust.PostalCode=Zip;
        cust.Street=Address;
        
        String id = ApexPages.currentPage().getParameters().get('id');
        if(EulaDAte == null){
            EulaDAte = Datetime.now();
        }
        
        //if(id == null)
        cust.EULA_Agreement_Date__c = EulaDAte;
        
        if(Username.length() > 40){
            cust.CommunityNickname = Username.substring(0,40);
        }else{
            cust.CommunityNickname = Username;
        }
        cust.timezonesidkey = 'America/New_York';
       try{
       
        List<Online_Portal_URLs_Hierarchy__c> ls = [SELECT TA_Manager_Portal_Profile__c, Scheduler_Portal_Profile__c, Energy_Specialist_Portal_Profile__c FROM Online_Portal_URLs_Hierarchy__c];
        //List<Online_Portal_URLs_Hierarchy__c> ls = [select TA_Manager_Portal_Profile__c from Online_Portal_URLs_Hierarchy__c];
        String profileName = '';
           if(ls.size() > 0){
               profileName = ls[0].TA_Manager_Portal_Profile__c;       
               if(portalRole == 'Manager'){
                   profileName = ls[0].TA_Manager_Portal_Profile__c;       
               }else if(portalRole == 'Scheduler'){
                   profileName = ls[0].Scheduler_Portal_Profile__c;    
               }else if(portalRole == 'Energy Specialist'){
                   profileName = ls[0].Energy_Specialist_Portal_Profile__c;    
               }
           }
        
        
        
        Profile ApprovedProfile=[SELECT id,Name FROM Profile WHERE Name=: profileName];
        
        String profileId =  null;
        if(ApprovedProfile != null){
            profileId = ApprovedProfile.Id;
            
        }
        system.debug('--profileId--'+profileId);
        try {
        cust.ProfileId = profileId;
        }
        catch(Exception e)
        {
            System.debug('Execption---' + e);
        }
       
            if(isForDSMTContact && objDSMTContact.Account__c != null){
                accountId = objDSMTContact.Account__c;
            }else if(!isForDSMTContact && objRR.Trade_Ally_Account__r.Account__c != null){
                accountId = objRR.Trade_Ally_Account__r.Account__c;
            }
            else{
            /********* fetching default accountid and save it in temperary variable****/  
                 
            List<Account> accList = [Select id from account where name=:'SITE SELF REGISTRATION TRADE ALLY' limit 1 ];   
            if(accList!=null && accList.size() > 0){
                accountId = accList.get(0).id;
            }
           }
            system.debug('@@AccountId@@'+accountId);
            String userId = null;
       
            userId = Site.createPortalUser(cust, accountId, password);
            
           if(Test.isRunningTest())
           {
               userId = UserInfo.getUserId();
           }
            system.debug('-userid-'+userid);
            if (userId == null || userId == '') {
                ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'We were unable to register you successfully');
                ApexPages.addMessage(msg3);
                failedRegistration = true;
                return null;
            }
            
            List<User> newUserList = [select id,contactid from user where id =:userId];
            String userContactId = null;
            if(newUserList.size()>0){
                User u = newUserList.get(0);
                updateUser(u.id,profileId);
                userContactId = u.Contactid;
            }
        
            
            Set<String> deletecontactids = new Set<String>();
            system.debug('--objDSMTContact --'+objDSMTContact);
            if(isForDSMTContact){
                objDSMTContact.City__c= City;
                objDSMTContact.State__c= State;
                objDSMTContact.Zip__c= Zip;
                objDSMTContact.Address__c= Address;
                objDSMTContact.Portal_User__c = userId;
                if(objDSMTContact.Contact__c != userContactId){
                   deletecontactids.add(objDSMTContact.Contact__c);
                   objDSMTContact.Contact__c = userContactId;
                }
                
                update objDSMTContact;
            }else{
                objRR.City__c= City;
                objRR.State__c= State;
                objRR.Zip__c= Zip;
                objRR.Address__c= Address;
                objRR.Steps_Completed__c = '2';
                objRR.Status__c = 'Registration Completed';
                
                update objRR;
                
                objDSMTContact.Portal_User__c = userId;
                
                if(objRR.DSmtracker_Contact__r.Contact__c != userContactId){
                    
                    deletecontactids.add(objRR.DSmtracker_Contact__r.Contact__c);
                    
                    objDSMTContact.contact__c = usercontactId;
                    
                }
                system.debug('--objDSMTContact --'+objDSMTContact);
                update objDSMTContact;
                
            }
            
            system.debug('--deletecontactids--'+deleteContactids);
            if(deletecontactids.size() > 0){
               dsmttradeallyregistrationCntrl.deleteContacts(deletecontactids);
            }
            
            
            if(isForDSMTContact){
                String startUrl = '';
                startUrl   = '/Tradeally/dsmtTradeAllyHomePage';
                PageReference ref = Site.login(Username, password, startUrl);
                return ref;
            }else{
                String startUrl = '';
                startUrl   = '/Tradeally/dsmtTradeAllyRegistrationProcess';
                PageReference ref = Site.login(Username, password, startUrl);
                return ref;
            }
       // }
        }
        catch(Exception ex){
            ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage());
            ApexPages.addMessage(msg3);
            failedRegistration = true;
            return null;
        }
        
        
        //return null;
    }
    
    private boolean isValidPasswordLength() {      
        if (password != null && password.length() >= 10) 
           return true;
           return false;               
    }
    private boolean isValidPassword() {
        
        return password == confirmPassword;
    }
    public Boolean isValidUserName(String usrname){
         Integer count = [select count() from User where username =: usrname];
         return (count>0)?false:true ;
    }
    public Boolean isValidCommunityNickname(String usrname){
         Integer count = [select count() from User where CommunityNickname =: usrname];
        return (count>0)?false:true ;
          
    }
    
     /* Other Methods */
    public list<Checklist_Items__c> getChecklists(){
        list<Checklist_Items__c> checklistItems = [
            SELECT id, Action_Required__c, Checklist_Information__c, Checklist__c,Parent_Checklist__c, Section__c, Sequence__c,Help_text_for_public_portal__c,Special_instruction_for_public_portal__c//, Status__c
            FROM Checklist_Items__c
            WHERE Parent_Checklist__r.Name = 'Trade Ally Registration Form'
        ];
        
        if(checklistItems.size() > 0)
            return checklistItems;
        
        return new list<Checklist_Items__c>();
    }
    
    @future(callout=true)
    public static void deleteContacts(Set<String> contactids){  
       system.debug('--contactids--'+contactids);
       //delete [select id from Contact where id in: contactids];
    }
    
    @future(callout=true)
    public static void updateUser(String userid,String ProfileId){  
       system.debug('--userid--'+userid+'--ProfileId--'+ProfileId);
       List<User> newUserList = [select id,contactid from user where id =:userid];
        if(newUserList.size()>0){
                User u = newUserList.get(0);
                u.ProfileId = profileId;
                update u;
       }
    }
}