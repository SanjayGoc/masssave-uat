@istest
public class AtticThermalEnvelopeTypeModelTest
{
    @testsetup static void SetupEnergyAssessment()
    {
        dsmtEAModel.setupAssessment(); 
    }

    @istest
    static void runtest()
    {
        List<Energy_Assessment__c> eaList = [Select Id, Customer__c, Appointment__c From
        Energy_Assessment__c Limit 1];

        Energy_Assessment__c testEA=eaList[0];

        List<Thermal_Envelope_Type__c> teTypeList = [Select Id, Next_Attic_Wall_Number__c, Next_Attic_Roof_Number__c From Thermal_Envelope_Type__c Where RecordType.Name = 'Attic' Limit 1]; 

        Thermal_Envelope_Type__c tet = teTypeList[0];

        Attic_Venting__c av = new Attic_Venting__c();
        insert av;

        //Ceiling__C ce = new Ceiling__c();
        //ce.Thermal_Envelope_Type__c = tet.Id;
        //insert ce;        

        Test.startTest();
        

        Attic_Space_Work_Order_Data__c asw = new Attic_Space_Work_Order_Data__c();
        insert asw;

        AtticThermalEnvelopeTypeModel atet = new AtticThermalEnvelopeTypeModel (tet ,asw,av);
        atet.refreshAtticPanel();
        atet.save();
        
        atet.queryAtticAWHFs();
        atet.deleteAtticAWHF();
        
        atet.addNewAtticRoof('Unfloored', 'exposedTo', false);
        /*atet.updateAtticRoofById(ce.id);
        atet.addNewAtticFloor('Unfloored', 'exposedTo');
        */
        Test.stopTest();
    }
    
    @istest
    static void runtest1()
    {
        List<Energy_Assessment__c> eaList = [Select Id, Customer__c, Appointment__c From
        Energy_Assessment__c Limit 1];

        Energy_Assessment__c testEA=eaList[0];

        List<Thermal_Envelope_Type__c> teTypeList = [Select Id, Next_Attic_Wall_Number__c From Thermal_Envelope_Type__c Where RecordType.Name = 'Attic' Limit 1]; 

        Thermal_Envelope_Type__c tet = teTypeList[0];

        Attic_Venting__c av = new Attic_Venting__c();
        insert av;        

        test.startTest();
        

        List<Wall__c> wallList = [Select Id From Wall__c Where Thermal_Envelope_Type__c=:tet.Id];
        List<Floor__c> floorList = [Select Id From Floor__c Where Thermal_Envelope_Type__c=:tet.Id];
        List<Ceiling__c> ceilList = [Select Id From Ceiling__c Where Thermal_Envelope_Type__c=:tet.Id];

        Attic_Space_Work_Order_Data__c asw = new Attic_Space_Work_Order_Data__c();
        insert asw;

        AtticThermalEnvelopeTypeModel atet = new AtticThermalEnvelopeTypeModel (tet ,asw,av);

        atet.addNewAtticAWHF();
        atet.deleteAtticFloorById(floorList[0].id);
        atet.deleteAtticRoofById(ceilList[0].id);
        atet.deleteAtticWallById(wallList[0].id);
    }

    @istest
    static void runtest2()
    {
        List<Energy_Assessment__c> eaList = [Select Id, Customer__c, Appointment__c From
        Energy_Assessment__c Limit 1];

        Energy_Assessment__c testEA=eaList[0];

        List<Thermal_Envelope_Type__c> teTypeList = [Select Id, Next_Attic_Wall_Number__c From Thermal_Envelope_Type__c Where RecordType.Name = 'Attic' Limit 1]; 

        Thermal_Envelope_Type__c tet = teTypeList[0];

        Attic_Venting__c av = new Attic_Venting__c();
        insert av;        

        test.startTest();       

        Attic_Space_Work_Order_Data__c asw = new Attic_Space_Work_Order_Data__c();
        insert asw;

        AtticThermalEnvelopeTypeModel atet = new AtticThermalEnvelopeTypeModel (tet ,asw,av);
        atet.refreshAtticPanel();
        atet.save();
        atet.addNewAtticWall('Unfloored', 'exposedTo');
    }

    @istest
    static void runtest3()
    {
        List<Energy_Assessment__c> eaList = [Select Id, Customer__c, Appointment__c From
        Energy_Assessment__c Limit 1];

        Energy_Assessment__c testEA=eaList[0];

        List<Thermal_Envelope_Type__c> teTypeList = [Select Id, Next_Attic_Floor_Number__c, Next_Attic_Roof_Number__c From Thermal_Envelope_Type__c Where RecordType.Name = 'Attic' Limit 1]; 

        Thermal_Envelope_Type__c tet = teTypeList[0];

        Attic_Venting__c av = new Attic_Venting__c();
        insert av;

        //Ceiling__C ce = new Ceiling__c();
        //ce.Thermal_Envelope_Type__c = tet.Id;
        //insert ce;        

        Test.startTest();
        

        Attic_Space_Work_Order_Data__c asw = new Attic_Space_Work_Order_Data__c();
        insert asw;

        AtticThermalEnvelopeTypeModel atet = new AtticThermalEnvelopeTypeModel (tet ,asw,av);

        atet.addNewAtticFloor('Unfloored', 'exposedTo');
        
        Test.stopTest();
    }

}