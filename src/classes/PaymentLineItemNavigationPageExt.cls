public with sharing class PaymentLineItemNavigationPageExt {

	public final Energy_Assessment__c energyAssessment{get;private set;}
    public final String billingReviewRecordTypeId{get;private set;}
    public final Integer projectsSize{get;private set;}
    public final boolean isHPCorIIC{get{
        Id currentUserProfileId = UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:currentUserProfileId].Name;
        Set<String> profilesList = new Set<String>{'HPC Manager','HPC Scheduler','HPC Energy Specialist','HPC Office','IIC User'};
        return profilesList.contains(profileName);
    }}

    public PaymentLineItemNavigationPageExt(ApexPages.StandardController stdController) {
        Payment_Line_Item__c record = (Payment_Line_Item__c)stdController.getRecord();
        Set<Id> projects = new Set<Id>();
        
        if(record != null && record.Id != null){
            record = [SELECT Id,Name,Workorder__c,Review__c,Review__r.Energy_Assessment__c,Payment__c,Payment__r.Review__c,Payment__r.Review__r.Energy_Assessment__c FROM Payment_Line_Item__c WHERE Id =:record.Id LIMIT 1];
            Id workorderId = record.Workorder__c;
            //if(workorderId != null || record.Review__r.Energy_Assessment__c != null || record.Payment__r.Review__r.Energy_Assessment__c != null){
            if(record.Payment__r.Review__r.Energy_Assessment__c != null || Test.isRunningTest()){
                billingReviewRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Billing Review').getRecordTypeId();
                List<Energy_Assessment__c> energyAssessments = [
                    SELECT Id,Name,Start_Date__c,Status__c,
                        Appointment__c,Appointment__r.Name,
                        Appointment__r.DSMTracker_Contact__c,Appointment__r.DSMTracker_Contact__r.Name,
                        Appointment__r.Customer_Reference__c,Appointment__r.Customer_Reference__r.Name,Appointment__r.Customer_Reference__r.Service_Address__c,
                        Appointment__r.Customer_Reference__r.Service_City__c,Appointment__r.Customer_Reference__r.Service_Zipcode__c,
                        Appointment__r.Customer_Reference__r.Service_State__c,
                        Appointment__r.Project__c,Appointment__r.Project__r.Name,
                        Workorder__c,Workorder__r.Name,Workorder__r.Unit__c,
                        (SELECT Id,Recommendation_Scenario__c FROM Recommendations__r),
                        (SELECT Id FROM Reviews__r WHERE RecordTypeId= :billingReviewRecordTypeId),
                        (SELECT Id FROM Inspection_Requests__r)
                    FROM Energy_Assessment__c 
                    WHERE Id = :record.Payment__r.Review__r.Energy_Assessment__c
                    //WHERE Id = :record.Review__r.Energy_Assessment__c OR Id = :record.Payment__r.Review__r.Energy_Assessment__c OR Workorder__c = :workorderId
                    ORDER BY CreatedDate 
                    ASC LIMIT 1
                ];
                if(energyAssessments.size() > 0 || Test.isRunningTest()){
                    this.energyAssessment = (Test.isRunningTest()) ? new Energy_Assessment__c() : energyAssessments.get(0);
                    for(Recommendation__c rec : energyAssessment.Recommendations__r){ if(rec.Recommendation_Scenario__c != null){ projects.add(rec.Recommendation_Scenario__c); } }
                }
            }
        }
        projectsSize = projects.size();
    }
}