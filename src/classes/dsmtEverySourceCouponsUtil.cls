public class dsmtEverySourceCouponsUtil {
	@TestVisible
    private static List<Account> testAcc;  
    
    public static List<Account> searchCustomer(dsmtEverySourceCouponsController.CouponsModal mdl){ 
        List<Account> retAccount = null; 
        String queyParam = '';
        EverSource_API_Credentials__c cs = EverSource_API_Credentials__c.getInstance(); 
        if(cs!=null && cs.Auth_Id__c!=null && cs.Auth_Token__c!=null){
            queyParam = 'auth-id='+cs.Auth_Id__c+'&auth-token='+cs.Auth_Token__c;
        }
        if(mdl.customer.Service_Street__c!=null && mdl.customer.Service_Street__c!=''){
            queyParam += '&street='+String.valueOf(mdl.customer.Service_Street__c).trim().replace(' ','+');    
        }
        if(mdl.customer.Unit__c!=null && mdl.customer.Unit__c!='' && !String.valueOf(mdl.customer.Unit__c).contains('@')){
            queyParam += '+'+String.valueOf(mdl.customer.Unit__c).trim().replace(' ','+');
        }
        if(mdl.customer.Service_City__c!=null && mdl.customer.Service_City__c!=''){
            queyParam += '&city='+String.valueOf(mdl.customer.Service_City__c).trim().replace(' ','+');    
        }
        if(mdl.customer.Service_State__c!=null && mdl.customer.Service_State__c!=''){
            queyParam += '&state='+String.valueOf(mdl.customer.Service_State__c).trim().replace(' ','+');    
        }
        if(queyParam!=''){
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            String url=cs.Smarty_Street_URL__c+'?'+queyParam;
            req.setEndpoint(cs.Smarty_Street_URL__c+'?'+queyParam);
            if(Test.isRunningTest()){
                req.setEndpoint('http://example.com/example/test?'+queyParam);
            }
            req.setMethod('GET');  
            req.setTimeout(20000);
            System.debug('req::=='+req);
            //if(!Test.isRunningTest()){
                res = http.send(req);
                System.debug(res.getBody()); 
            //}   
            System.debug('res:::'+res);
            if(res!=null){
                List<SmartyStreet> ssObjList = (List<SmartyStreet>) JSON.deserialize(res.getBody(), List<SmartyStreet>.class);
                string utilityType = null;
                if(ssObjList!=null && ssObjList.size()>0){
                    //String utilityType=''; 
                    if(mdl.customer.Heating_System_Fuel_Type__c!=null && mdl.customer.Heating_System_Fuel_Type__c=='Natural Gas' || mdl.customer.Heating_System_Fuel_Type__c=='Gas'){
                        mdl.utilityType='Gas';
                        utilityType = 'Eversource East Gas';
                    }else{
                        mdl.utilityType='Electric';
                        utilityType = 'Eversource East Electric';
                    }
                    SmartyStreet ssObj = ssObjList.get(0); 
                    List<Account> lstAcct = [
                        SELECT Id, Name, First_Name__c, Utility_Service_Type__c, Utility__c, Billing_Account_Number__c, Provider__c, Last_Name__c, BillingStreet, BillingCity, BillingState, BillingPostalcode, Email__c, Phone,
                        (SELECT Id FROM Customers__r LIMIT 1),
                        (SELECT Id, Name FROM Customers1__r LIMIT 1),
                        (SELECT Id, Name FROM Customers2__r LIMIT 1)
                        FROM Account 
                        WHERE Normalized_Barcode__c=:ssObj.delivery_point_barcode AND Normalized_Barcode__c!=''
                        AND Account_Status__c!='Inactive'
                        AND Last_Name__c =:mdl.customer.Last_Name__c
                        AND Utility__r.Name = :utilityType
                        ORDER BY Normalized_Barcode__c
                        LIMIT 10
                    ];
                    if(lstAcct.size()>0){ 
                        retAccount = lstAcct; 
                        mdl.isAddressVerified = true; 
                        for(Account acc:lstAcct){
                            if(acc.Utility_Service_Type__c!=null && mdl.utilityType!='' &&  mdl.utilityType!='Other' && (acc.Utility_Service_Type__c=='Combo'||acc.Utility_Service_Type__c==mdl.utilityType) && !mdl.isAccountFound){
                                mdl.isAccountFound=true; 
                            }    
                        }
                    }else{
                        mdl.isAddressVerified = false; 
                    }
                }else{
                    mdl.isAddressVerified = false; 
                } 
            } 
        } 
        if(testAcc!=null){
            return testAcc;
        }
        return retAccount;
    }
    public class SmartyStreet{
        public Integer input_index;
        public Integer candidate_index;
        public String delivery_line_1;
        public String last_line;
        public String delivery_point_barcode; 
    } 
    // Need to check here
    public static boolean isEligible(dsmtEverySourceCouponsController.CouponsModal mdl){ 
        boolean isEligible = true;
        Set<String> qplIds = new Set<String>();
        if(mdl.dsmtProduct!=null && mdl.dsmtProduct.Qualified_Product_Lists__r.size() > 0){
            for(Qualified_Product_List__c rec : mdl.dsmtProduct.Qualified_Product_Lists__r){
                qplIds.add(rec.Id);
            }    
        } 
        Date d = System.today();
        Date startDateOfYear = date.newInstance(d.year(), 1, 1);
        Set<String> setAcc = new Set<String>();
        for(Account acc: mdl.account){
            setAcc.add(acc.Id);
        }
        List<Coupon__c> custCouponList = [
            SELECT Id, Name, Customer_Date__c, 
            Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Participation_Limit__c,
            Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Timeframe__c,
            Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Number_of_Years__c
            FROM Coupon__c
            WHERE /*Retailer_Name_Formula__c =: mdl.selectedRetailer AND
            Qualified_Product_List__c IN : qplIds AND Qualified_Product_List__c != null AND */
            Customer__c != null AND Customer__r.Account__c IN :setAcc
            AND Start_Date__c <= :Date.today() AND Expiration_Date__c >= :Date.today()
            AND Status__c='Reserved' AND Customer_Date__c >=:startDateOfYear
        ];
        Integer count = 0;
        count += getMLICount(mdl);     
        Decimal participationLimit = mdl.cs.Allowed_Attempt__c != null ? mdl.cs.Allowed_Attempt__c : 0; 
        if(custCouponList.size()>0){
            if(custCouponList[0].Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Participation_Limit__c<participationLimit){
                participationLimit = custCouponList[0].Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Participation_Limit__c;    
            }
            for(Coupon__c c : custCouponList){
                if(c.Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Timeframe__c != null &&
                   c.Qualified_Product_List__r.Qualified_Product_List_Master__r.Program__r.Eligibility_Number_of_Years__c != null){                       
                       count++;
                   }
            }
        } 
        System.debug('participationLimit--->'+participationLimit);
        System.debug('count--->'+count);
        if(count >= participationLimit)
            isEligible = false; 
        
        return isEligible;
    }
    public static Integer getMLICount(dsmtEverySourceCouponsController.CouponsModal mdl){
        Set<String> setAcc = new Set<String>();
        for(Account acc: mdl.account){
            setAcc.add(acc.Id);
        }
        Set<String> qplIds = new Set<String>();
        if(mdl.dsmtProduct!=null && mdl.dsmtProduct.Qualified_Product_Lists__r.size() > 0){
            for(Qualified_Product_List__c rec : mdl.dsmtProduct.Qualified_Product_Lists__r){
                qplIds.add(rec.Id);
            }    
        }
        Date d = System.today();
        Date startDateOfYear = date.newInstance(d.year(), 1, 1);
        List<Measure_Line_Item__c> lstMLI = [
            SELECT Id, Name FROM Measure_Line_Item__c
            WHERE /*Qualified_Product_List__c IN :qplIds AND */
            Enrollment_Application__r.Customer__r.Account__c IN :setAcc
            AND Eligible_Measure__r.Measure_Name__c=:mdl.cs.Eligible_Measure_Name__c
            AND CreatedDate >= :startDateOfYear
            LIMIT 10
        ];
        return lstMLI.size();
        
    }
}