public with sharing class ReviewTriggerHandler implements ITrigger {
    
    public static boolean PreventReviewRecursion = false;
    public static boolean Preventinsertinvoice = false;
    public static boolean ReviewBatched = false;
    public ReviewTriggerHandler() {}
    
    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            PopulateEmployeeEmailAndOwnerId(trigger.new, null);
        } else if (trigger.isUpdate) {
            //Here we will call before update actions
            PopulateEmployeeEmailAndOwnerId(trigger.new, (map<id, Review__c>)trigger.OldMap);
        } else if (trigger.isDelete) {
            //Here we will call before delete actions
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }
    
    public void bulkAfter() {
        if (trigger.isInsert) {
            //Here we will call after insert actions
            UpdateProjectRecordStatus(trigger.new, (map<id, Review__c>) trigger.oldmap, true);
            SendAutomatedEmails(trigger.new);
        } else if (trigger.isUpdate) {
            //Here we will call after update actions
            UpdateProjectRecordStatus(trigger.new, (map<id, Review__c>) trigger.oldmap, false);
            
            UpdateProjectAsProposed(trigger.new, (map<id, Review__c>) trigger.oldmap);
            
            UpdateInstallDateOnRecommendations(trigger.new, (map<id, Review__c>) trigger.oldmap);
            
            if(!Preventinsertinvoice){
                InsertInvoiceLineItemWhenStatusBatched(trigger.new, (map<id, Review__c>) trigger.oldmap);
            }
            
            UpdateWorkScopeReviewOnSR(trigger.new, (map<id, Review__c>) trigger.oldmap);
            SendAutomatedEmails(trigger.new, (map<id, Review__c>) trigger.oldmap);
            
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }
    
    @TestVisible
    private void UpdateInstallDateOnRecommendations(list<Review__c> newlist, map<id, Review__c> oldmap)
    {
        Set<Id> revids = new Set<Id>();
        
        Map<Id,Date> revInsDateMap = new Map<Id,Date>();
        Map<Id,Id> revProjMap = new Map<Id,Id>();
        Map<Id,List<Recommendation__c>> projRecommendationMap = new Map<Id,List<Recommendation__c>>();
        
        for (Review__c rev : newlist) {
            if(rev.Installed_Date__c != null && rev.Installed_Date__c != oldmap.get(rev.Id).Installed_Date__c && rev.Type__c != 'Change Order Review') {
                revids.add(rev.Id);
                revInsDateMap.put(rev.Id,rev.Installed_Date__c);
            }
        }
        
        
        if(revids != null && revids.size() > 0) {
            List<Project_Review__c> revList = [select id,name,Review__c ,Project__c from Project_Review__c where Review__c in : revids];
            Set<Id> projId = new set<Id>();
            
            for(Project_Review__c rev : revList) {
                revProjMap.put(rev.Review__c,rev.Project__c );
                projId.add(rev.Project__c);
            }
            
            system.debug('--revProjMap.values()---'+revProjMap.values());
            if(revProjMap != null && revProjMap.values().size() > 0) {
                List<Recommendation__c> recomList = [select id,name,Recommendation_Scenario__c,Recommendation_Description__c,Quantity__c,Units__c,Installed_Date__c 
                                                     from Recommendation__c where Recommendation_Scenario__c in : projId];
                
                List<Recommendation__c> recomListtoupdate = new List<Recommendation__c>();                                        
                if(recomList != null) {
                    for(Recommendation__c recom : recomList) {
                        if(projRecommendationMap.get(recom.Recommendation_Scenario__c) == null){
                            List<Recommendation__c> recList = new List<Recommendation__c>();
                            recList.add(recom);
                            projRecommendationMap.Put(recom.Recommendation_Scenario__c,recList);
                        } else {
                            projRecommendationMap.get(recom.Recommendation_Scenario__c).add(recom);
                        }
                    }
                    
                    for (Review__c review : newlist) {
                        // if(projRecommendationMap.get(revProjMap.get(review.Id)) != null) {
                        for(Recommendation__c rec : recomList ) {
                            if(revInsDateMap.get(Review.Id) != null && rec.Installed_Date__c != revInsDateMap.get(Review.Id) ) {
                                
                                
                                rec.Installed_Date__c = revInsDateMap.get(Review.Id);
                                recomListtoupdate.add(rec);
                            }
                        }
                        //    }
                    }
                    
                    if(recomListtoupdate .size() > 0) {
                        dsmtRecommendationHelper.disableRolluptriggeronRec = true;
                        dsmtRecommendationHelper.StopRecTrigger = true;
                        update recomListtoupdate ;
                    }
                    
                }            
            }
        }
    }
    
    private void InsertInvoiceLineItemWhenStatusBatched(list<Review__c> newlist, map<id, Review__c> oldmap)
    {
        Set<Id> revids = new Set<Id>();
        
        Set<Id> projId = new Set<Id>();
        
        Set<String>  invoiceIds=new Set<String>();
        
        String WorkorderId = null;
        
        
        List<Invoice_Line_Item__c> iLItem = new List<Invoice_Line_Item__c>();
        List<Invoice__c> invData= new List<Invoice__c>();
        DAte installedDate = Date.Today();
        
        for (Review__c rev: newlist) {
            if (rev.Status__c == 'Batched' && rev.Status__c != oldmap.get(rev.Id).Status__c && rev.Type__c != 'Change Order Review'){
                revids.add(rev.Id);
                installedDate = rev.Installed_Date__c;
            }
        }
        
        if(installedDate == null){
            installedDate = Date.Today();
        }
        Date startDate = installedDate.toStartOfMonth();
        Date endDate = startDate.addMonths(1).addDays(-1);
        
        system.debug('--revids---'+revids);
        
        if (revids != null) {
            List<Project_Review__c> revList = [select id, name, Review__c,Review__r.Energy_Assessment__c,Review__r.Energy_Assessment__r.Workorder__c, Project__c from Project_Review__c where Review__c in: revids];
            
            Id EnergyAssessmentId = null;
            
            for (Project_Review__c rev: revList) {
                projId.add(rev.Project__c);
                if(rev.Review__r.Energy_Assessment__c != null){
                    EnergyAssessmentId  = rev.Review__r.Energy_Assessment__c;
                    workorderId = rev.Review__r.Energy_Assessment__r.Workorder__c;
                }
            }
            
            system.debug('--projId---'+projId);
            
            if (projId.size() > 0) {
                 // DSST-16299 by HP on 1st Dec 2018
                List<Recommendation__c> recomList = [select id,EXT_JOB_ID__c,Energy_Assessment__R.Name, name,Invoice_Type__c,Isinvoiced__c, Quantity__c,Installed_Date__c,Unit_Cost__c,
                                                     Energy_Assessment__r.Primary_Provider__c,DSMTracker_Product__r.EMHome_EMHub_PartID__c,Category__c,
                                                     Recommendation_Scenario__r.Type__c,Calculated_Incentive__c, Energy_Assessment__r.Appointment__r.Workorder__c,
                                                     (select Id,Voided__c from invoice_Line_Items__r where Voided__c = false),
                             (select Id,Proposal__c,Proposal__r.Final_Proposal_Incentive__c,Proposal__r.Barrier_Incentive_Amount__c from Proposal_Recommendations__r where Proposal__r.Status__c != 'Inactive')
                                                     from Recommendation__c where Recommendation_Scenario__c in: projId
                                                     /* and IsInvoiced__c = false */ and Invoice_Type__c  != null
                                                     and Invoice_Type__c != 'Not Found' and Installed_Date__c != null];
                
                Map<String,List<Recommendation__c>> RecTypeMap = new Map<String,List<Recommendation__c>>();
                List<Recommendation__c> temp = null;
                
                if (recomList.size() > 0) {
                    for (Recommendation__c r: recomList) {
                        system.debug('---r.Invoice_Type__c---'+r.Invoice_Type__c);
                        //  r.Isinvoiced__c = true;
                        if(r.Invoice_Type__c  != null && r.Invoice_Type__c != 'Not Found'){
                            invoiceIds.add(r.Invoice_Type__c);
                        }
                        
                        if(r.Category__c != 'Direct Install' && r.Recommendation_Scenario__r.Type__c != null){
                            if(RecTypeMap.get(r.Recommendation_Scenario__r.Type__c) != null){
                                
                                temp = RecTypeMap.get(r.Recommendation_Scenario__r.Type__c);
                                temp.add(r);
                                RecTypeMap.put(r.Recommendation_Scenario__r.Type__c,temp);
                            }else{
                                temp = new List<Recommendation__c>();
                                temp.add(r);
                                RecTypeMap.put(r.Recommendation_Scenario__r.Type__c,temp);
                            }
                        }
                    } 
                    // update recomList;
                }
                
                Map<String,Invoice__c> invTypeMap = new Map<String,Invoice__c>();
                
                boolean FirstInvLineItem = false;
                
                if(EnergyAssessmentId   != null){
                    List<Invoice_Line_Item__c> invLineList = [select id from invoice_Line_Item__c where Recommendation__r.Energy_Assessment__c =:EnergyAssessmentId
                                                              and Is_Distinct_Per_EA__c = true];
                    
                    if(invLineList != null && invLineList.size() > 0){
                        FirstInvLineItem = true;
                    }
                    
                }
                
                //  endDate = installedDate.addMonths(1).addDays(-1);
                
                List<Invoice__c> invList = [Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:invoiceIds 
                                            and Status__c = 'UnSubmitted' and Invoice_Date__c != null 
                                            And Invoice_Date__c >=: installedDate and invoice_Date__c <=: endDate ORDER BY Invoice_Date__c  ASC];
                
                for(Invoice__c inv : invList){
                    invTypeMap.put(inv.Invoice_Type__c,inv);
                }
                
                Set<Id> recId = new set<Id>();
                Set<Id> CreatedrecId = new set<Id>();
                
                List<Recommendation__c> InvToCreate = new List<Recommendation__c>();
                
                Set<String> ExtInvoiceType = new Set<String>();
                
                if(invList.size()>0) {
                    
                    for( Invoice__c  inv:invList){
                        ExtInvoiceType.add(inv.Invoice_Type__c);
                    }
                    
                    for( Invoice__c  inv:invList){
                        
                        
                        /* String target = '/'; 
String replacement = '';

String formatedDate= inv.Invoice_Date__c.format().replace(target, replacement); */
                        
                        String target = '/'; 
                        String replacement = '';                      
                        Datetime output = inv.Invoice_Date__c;                       
                        String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                        
                        for(Recommendation__c r: recomList) {
                            
                            /* system.debug('--r.Invoice_Type__c---'+r.Invoice_Type__c);
system.debug('--inv.Invoice_Type__c---'+inv.Invoice_Type__c);
system.debug('--r.Isinvoiced__c---'+r.Isinvoiced__c);*/
                            
                            if(inv.Invoice_Type__c  == r.Invoice_Type__c && (r.Isinvoiced__c == false || r.invoice_Line_Items__r.size() == 0) ){
                                
                                Invoice_Line_Item__c createILItemData;
                                
                                //system.debug('--r.Installed_Date__c---'+r.Installed_Date__c);
                                // system.debug('--inv.Invoice_Date__c---'+inv.Invoice_Date__c);
                                
                                if(r.Installed_Date__c <= inv.Invoice_Date__c){
                                    recId.add(r.Id);
                                    if(CreatedrecId.add(r.Id)){
                                        createILItemData = new Invoice_Line_Item__c(
                                            Invoice__c = inv.Id,
                                            Recommendation__c = r.Id,
                                            Workorder__c = r.Energy_Assessment__r.Appointment__r.Workorder__c,
                                            Status__c=inv.Status__c,
                                            QTY__c= r.Quantity__c,
                                            Unit_Cost__c = r.Unit_Cost__c,
                                            INSTALLED_DATE__c = r.INSTALLED_DATE__c,
                                            Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                            //Review__c = newlist.get(0).Id
                                        );
                                        if(r.Category__c != 'Direct Install'){
                                            createILItemData.Review__c = newlist.get(0).Id;
                                        }
                                        if(!FirstInvLineItem){
                                            FirstInvLineItem = true;
                                            createILItemData.Is_Distinct_Per_EA__c = true;
                                        }
                                        system.debug('--createILItemData---'+createILItemData);
                                        iLItem.add(createILItemData);
                                        
                                        r.Isinvoiced__c = true;
                                        r.EXT_JOB_ID__c = r.Energy_Assessment__R.Name+formatedDate+inv.Invoice_Type__c ;
                                    }
                                }else{
                                    if(recId.add(r.Id)){
                                        ExtInvoiceType.add(inv.Invoice_Type__c);
                                        InvToCreate.add(r);
                                    }
                                }
                            }else{
                                system.debug('---ExtInvoiceType---'+ExtInvoiceType);
                                system.debug('---inv.Invoice_Type__c---'+inv.Invoice_Type__c);
                                
                                if(recId.add(r.Id) && ExtInvoiceType.add(r.Invoice_Type__c)){
                                    system.debug('--r.Invoice_Type__c123---'+r.Invoice_Type__c);
                                    InvToCreate.add(r);
                                }
                            }
                            
                        }
                        system.debug('--InvToCreate--'+InvToCreate);
                    }
                }
                else  {
                    /*String target = '/'; 
String replacement = '';
String formatedDate= endDate.format().replace(target, replacement); */
                    
                    String target = '/'; 
                    String replacement = '';                      
                    Datetime output = endDate;                       
                    String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                    
                    for(Recommendation__c r: recomList) {
                        // DSST-14968 by HP on 1st Dec 2018
                        if(r.Invoice_Type__c != null && r.Invoice_Type__c != ''){
                            if(ExtInvoiceType.add(r.Invoice_Type__c)){
                                Invoice__c invRecord;
                                invRecord=new Invoice__c(
                                    Invoice_Type__c = r.Invoice_Type__c,
                                    Invoice_Date__c=endDate,
                                    Status__c = 'In Process',
                                    Provider__c = r.Energy_Assessment__r.Primary_Provider__c
                                );
                                invRecord.Invoice_Number__c = formatedDate + r.Invoice_Type__c;
                                
                                invData.add(invRecord);
                                
                            }
                        }
                    }
                    
                }
                
                system.debug('--invDate---'+invData);
                Set<String> invType = new Set<String>();
                
                for(Recommendation__c r : invToCreate){
                    // DSST-14968 by HP on 1st Dec 2018
                    if(r.Invoice_Type__c != null && r.Invoice_Type__c != ''){
                        if(invType.Add(r.Invoice_Type__c)){
                            /*String target = '/'; 
    String replacement = '';
    String formatedDate= endDate.format().replace(target, replacement); */
                            
                            String target = '/'; 
                            String replacement = '';                      
                            Datetime output = endDate;                       
                            String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                            
                            
                            Invoice__c invRecord;
                            invRecord=new Invoice__c(Invoice_Type__c = r.Invoice_Type__c,Invoice_Date__c=endDate,Status__c = 'In Process',Provider__c = r.Energy_Assessment__r.Primary_Provider__c);
                            invRecord.Invoice_Number__c = formatedDate + r.Invoice_Type__c;
                            
                            invData.add(invRecord);
                        } 
                    }                         
                    
                }       
                
                if(invData.size()>0){
                    insert invData;
                    
                    for(Invoice__c inv : invData){
                        invTypeMap.put(inv.Invoice_Type__c,inv);
                    }
                    system.debug('--invoiceIds---'+invoiceIds);
                    
                    invList=[Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:invoiceIds 
                             and Status__c = 'Unsubmitted' and Invoice_Date__c != null And Invoice_Date__c >=: installedDate
                             and invoice_Date__c <=: endDate ORDER BY Invoice_Date__c  ASC];
                    
                    Set<Id> NewRecId = new Set<Id>();
                    
                    if(invList.size()>0) {
                        for( Invoice__c  inv:invList){
                            if(inv.Invoice_Date__c != null){
                                for(Recommendation__c r: recomList) {
                                    
                                    /*String target = '/'; 
String replacement = '';
String formatedDate= inv.Invoice_Date__c.format().replace(target, replacement); */
                                    
                                    String target = '/'; 
                                    String replacement = '';                      
                                    Datetime output = inv.Invoice_Date__c;                       
                                    String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                                    
                                    system.debug('--r.Invoice_Type__c---'+r.Invoice_Type__c);
                                    system.debug('--inv.Invoice_Type__c---'+inv.Invoice_Type__c);
                                    system.debug('--r.Isinvoiced__c---'+r.Isinvoiced__c);
                                    
                                    if(inv.Invoice_Type__c  ==r.Invoice_Type__c  ){
                                        Invoice_Line_Item__c createILItemData;
                                        
                                        system.debug('--r.Installed_Date__c---'+r.Installed_Date__c);
                                        system.debug('--inv.Invoice_Date__c---'+inv.Invoice_Date__c);
                                        
                                        if(r.Installed_Date__c <= inv.Invoice_Date__c){
                                            if(CreatedrecId.add(r.Id)){
                                                system.debug('---inv.Id---'+inv.Id);
                                                createILItemData = new Invoice_Line_Item__c(
                                                    Invoice__c = inv.Id,
                                                    Recommendation__c = r.Id,
                                                    Workorder__c = r.Energy_Assessment__r.Appointment__r.Workorder__c,
                                                    Status__c=inv.Status__c,
                                                    QTY__c= r.Quantity__c,
                                                    Unit_Cost__c = r.Unit_Cost__c,
                                                    INSTALLED_DATE__c = r.INSTALLED_DATE__c,
                                                    Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                                    
                                                    //Review__c = newlist.get(0).Id
                                                );
                                                if(r.Category__c != 'Direct Install'){
                                                    createILItemData.Review__c = newlist.get(0).Id;
                                                }
                                                if(!FirstInvLineItem){
                                                    FirstInvLineItem = true;
                                                    createILItemData.Is_Distinct_Per_EA__c = true;
                                                }
                                                iLItem.add(createILItemData);
                                                r.Isinvoiced__c = true;
                                                r.EXT_JOB_ID__c = r.Energy_Assessment__R.Name+formatedDate+inv.Invoice_Type__c ;
                                            }
                                            system.debug('--createILItemData---'+createILItemData);
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if(RecTypeMap != null && RecTypeMap.keyset().size() > 0){
                    
                    Map<String,Decimal> RecTypeTotal = new Map<String,Decimal>();
                    
                    for(String str : RecTypeMap.keyset()){
                        RecTypeTotal = new Map<String,Decimal>();
                        
                        system.debug('--str---'+str);
                        
                        for(Recommendation__c rec : RecTypeMap.get(str)){
                            if(rec.invoice_Type__c != null && rec.Invoice_Type__c.contains('413')){
                                if(RecTypeTotal.get(rec.invoice_Type__c) != null){
                                    RecTypeTotal.put(rec.invoice_Type__c,RecTypeTotal.get(rec.invoice_Type__c)+rec.Calculated_Incentive__c);
                                }else{
                                    RecTypeTotal.put(rec.invoice_Type__c,rec.Calculated_Incentive__c);
                                }
                            }
                        }
                        
                        for(String s : RecTypeTotal.keyset()){
                            for( Invoice__c  inv:invList){
                                if(inv.Invoice_Type__c  == s && InstalledDate <= inv.Invoice_Date__c && Enddate >= inv.Invoice_Date__c){
                                    
                                    system.debug('--s--'+s);
                                    Invoice_Line_Item__c createILItemData = new Invoice_Line_Item__c(Invoice__c = invTypeMap.get(s).Id,Status__c=invTypeMap.get(s).Status__c,
                                        Calculated_Incentive_Amount__c = RecTypeTotal.get(s),Project_Type__c = str,MEA_Id__c = 'Incentive',
                                        //QTY__c= r.Quantity__c,
                                        //Unit_Cost__c = r.Unit_Cost__c,
                                        INSTALLED_DATE__c = INSTALLEDDATE,
                                        //   Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                        Review__c = newlist.get(0).Id,Workorder__c = workorderId );
                                    if(!FirstInvLineItem){
                                        FirstInvLineItem = true;
                                        createILItemData.Is_Distinct_Per_EA__c = true;
                                    }
                                    iLItem.add(createILItemData);
                                    break;
                                }
                            }
                        }
                    }
                }
                
                system.debug('--iLItem---'+iLItem);
                if(iLItem.size()>0){
                    dsmtRecommendationHelper.StopRecTrigger = true;
                    Preventinsertinvoice  = true;
                    insert iLItem;
                }
                dsmtRecommendationHelper.RollupOnProject = true;
                dsmtRecommendationHelper.disableRolluptriggeronRec = true;
                update recomList;
            }
        }
    }  
    
    private void PopulateEmployeeEmailAndOwnerId(list<Review__c> newlist, map<id, Review__c> oldmap)
    {
        if(!dsmtCreateEAssessRevisionController.stopExcReviewtrigger){
            Set<Id> setRevIds = new Set<Id>();
            Map<Id,String> mapPR = new Map<Id,String>();
            
            for(Review__c rev : newlist){
                setRevIds.add(rev.Id);
            }
            
            Set<Id> setEAId = new Set<Id>();
            Set<Id> setCreatedBy = new Set<Id>();
            Map<Id,String> mapEACreateUser = new Map<Id,String>();
            
            for(Project_Review__c pr: [select Id,Review__c,Project__c,Project__r.Energy_Assessment__c,Project__r.EA_Created_By__c,Project__r.Energy_Assessment__r.Energy_Advisor__c,Project__r.Energy_Assessment__r.Energy_Advisor__r.Email__c from Project_Review__c where Review__c In :setRevIds AND Project__c != null AND Project__r.Energy_Assessment__c != null AND
                                       Project__r.Energy_Assessment__r.Energy_Advisor__c != null AND Project__r.Energy_Assessment__r.Energy_Advisor__r.Email__c != null]){
                                           
                                           if(mapPR.get(pr.Review__c) == null)
                                               mapPR.put(pr.Review__c,pr.Project__r.Energy_Assessment__r.Energy_Advisor__r.Email__c);
                                           
                                           if(pr.Project__r.Energy_Assessment__c != null)
                                           {
                                               setEAId.add(pr.Project__r.Energy_Assessment__c);
                                               setCreatedBy.add(pr.Project__r.EA_Created_By__c);
                                               
                                               if(mapEACreateUser.get(pr.Review__c) == null)
                                                   mapEACreateUser.put(pr.Review__c,pr.Project__r.EA_Created_By__c);
                                           }
                                       }
            
            Map<Id,Boolean> mapCREmployee = new Map<Id,Boolean>();
            for(DSMTracker_Contact__c dsmt: [select Id,Portal_User__c,Trade_Ally_Account__r.Internal_Account__c 
                                             from DSMTracker_Contact__c 
                                             where Portal_User__c In :setCreatedBy 
                                             AND Trade_Ally_Account__c != null]){
                                                 
                                                 if(dsmt.Trade_Ally_Account__r.Internal_Account__c)
                                                     mapCREmployee.put(dsmt.Portal_User__c,true);
                                             }
            
            
            for(Review__c rev : newlist){
                if(mapPR.get(rev.Id) != null)
                    rev.Employee_Email__c = mapPR.get(rev.Id) ;
                
                if(mapEACreateUser.get(rev.Id) != null){
                    string usrId = mapEACreateUser.get(rev.Id);
                    
                    if(mapCREmployee.get(usrId) != null && mapCREmployee.get(usrId) == true){
                        rev.is_CR_Employee__c = true;
                        
                        if((oldMap == null || (oldMap != null && oldMap.get(rev.Id).Status__c != rev.Status__c)) && (Rev.Status__c == 'Failed Admin Review - Send to Energy Specialist' || Rev.Status__c == 'Failed Work Scope Review')){
                            system.debug('--usrId---'+usrId);
                            Rev.OwnerId = usrId ;
                        }
                    }
                }
            }
        }
    }
    
    private void UpdateProjectRecordStatus(list<Review__c> newlist, map<id, Review__c> oldmap, boolean isInsert)
    {
        set<Id> projid = new set<Id>();
        Set<Id> rejectedProjId = new Set<Id>();
        set<Id> approvedProjId  = new set<Id>();
        
        for(Review__c r:newlist)
        {
            if(isInsert)
            {
                if(r.Status__c == 'Passed Review')
                    projid.add(r.Project__c);
            }
            else
            {
                if(r.Status__c == 'Passed Review' && r.Status__c != oldmap.get(r.Id).Status__c)
                    projid.add(r.Project__c);
                
                if(r.Status__c == 'Rejected' && r.Status__c != oldmap.get(r.Id).Status__c)
                    rejectedProjId.add(r.Project__c);
                
                if(r.Status__c == 'Approved' && r.Status__c != oldmap.get(r.Id).Status__c && r.Type__c != 'Change Order Review')
                    approvedProjId.add(r.Project__c);
            }
        }
        
        if(ProjID != null && projId.size() > 0){
            List <Recommendation_Scenario__c> prjList = [select id,Status__c from Recommendation_Scenario__c where id in :projid ];
            if( prjList != null)
            {
                for(Recommendation_Scenario__c p : prjList)
                {
                    p.Status__c = 'Work Scope Approved';
                }
                update prjList;
            }
        }
        
        if(rejectedProjId != null && rejectedProjId.size() > 0){
            List <Recommendation_Scenario__c> prjList = [select id,Certification_of_Completion__c,Status__c from 
                                                         Recommendation_Scenario__c where id in :rejectedProjId ];
            if( prjList != null)
            {
                for(Recommendation_Scenario__c p : prjList)
                {
                    p.Certification_of_Completion__c = false;
                    p.Status__c = 'Rejected';
                }
                update prjList;
            }  
        }
        
        if(approvedProjId != null && approvedProjId.size() > 0){
            List <Recommendation_Scenario__c> prjList = [select id,Status__c from 
                                                         Recommendation_Scenario__c where id in :approvedProjId ];
            if( prjList != null)
            {
                for(Recommendation_Scenario__c p : prjList)
                {
                    p.Status__c = 'Approved For Invoice Submission';
                }
                update prjList;
            }  
        }
    }
    
    private void UpdateProjectAsProposed(list<Review__c> newlist, map<id, Review__c> oldmap)
    {
        set<Id> revIds = new set<Id>();
        set<Id> projIds = new set<Id>();
        Map<String, String> projRevMap = new Map<String, String>();
        Map<String, Recommendation_Scenario__c> projMap = new Map<String, Recommendation_Scenario__c>();
        
        for(Review__c rev : newlist){
            if((rev.Status__c == 'No Contract' || rev.Status__c == 'Cancelled') && rev.Status__c != oldmap.get(rev.Id).Status__c){
                revIds.add(rev.id);
            }
        }
        
        if(revIds.size() > 0){
            List<Project_Review__c> projRevList = [select id, Review__c, Project__c from Project_Review__c where Review__c in: revIds];
            
            for(Project_Review__c prjRev : projRevList){
                projIds.add(prjRev.Project__c);
            }
            
            List<Recommendation_Scenario__c> projList = [select id from Recommendation_Scenario__c where id in: projIds];
            if(projList != null && projList.size() > 0){
                for(Recommendation_Scenario__c p : projList){
                    p.Status__c = 'Proposed';
                }
                update projList;
            }
        }
    }
    
    public void UpdateWorkScopeReviewOnSR(list<Review__c> newlist, map<id, Review__c> oldmap){
        set<Id> revIds = new set<Id>();
        set<Id> projIds = new set<Id>();
        for(Review__c rev: newlist){
            if(rev.Type__c == 'Work Scope Review'){
                if((rev.Status__c == 'Approved' || rev.Status__c == 'Accepted') && rev.Status__c != oldmap.get(rev.Id).Status__c){
                    revIds.add(rev.id);        
                    
                }
            }
        }
        
        Map<Id,Project_Review__c > mapProjRev = new Map<Id,Project_Review__c >();
        
        if(revIds.size() > 0){
            List<Project_Review__c> projRevList = [select id, Review__c, Project__c,Review__r.Trade_Ally_Account__r.Name from Project_Review__c where Review__c in: revIds];
            
            for(Project_Review__c prjRev : projRevList){
                projIds.add(prjRev.Project__c);
                mapProjRev.put(prjRev.Project__c,prjRev);
            }
            
            List<queuesobject> lstQueue = [select id,QueueId from queuesobject where queue.name='Work Scope Review Assigned'];
            List<Service_Request__c> SRList = [select id,Review__c,Project__c,OwnerId,Status__c from Service_Request__c where Project__c in: projIds];
            if(SRList != null && SRList.size() > 0){
                for(Service_Request__c p : SRList){
                    if(mapProjRev.get(p.Project__c) != null){
                        p.Review__c = mapProjRev.get(p.Project__c).Review__c;
                        p.Contractor_Company_Name__c = mapProjRev.get(p.Project__c).Review__r.Trade_Ally_Account__r.Name;
                        if(lstQueue.size() > 0 )
                            p.OwnerId = lstQueue[0].QueueId;     
                        
                    }
                    
                }
                update SRList;
            }
        }
    }
    
    public void SendAutomatedEmails(list<Review__c> newlist, map<id, Review__c> oldmap){
        
        for(Review__c rev: newlist){
            if(rev.Type__c == 'Billing Review'){
                if((rev.Status__c == 'Failed Completion Review') && rev.Status__c != oldmap.get(rev.Id).Status__c){
                    //revIds.add(rev.id);        
                    dsmtReviewUtil.dsmtReviewSendEmail(rev.Id);
                }
                //DSST-7496 changed by Hemanshu Patel
            }else if(rev.Status__c == 'Failed Work Scope Review' || rev.Status__c == 'Manager Fail' && rev.Status__c != oldmap.get(rev.Id).Status__c){            
                dsmtReviewUtil.dsmtReviewSendEmail(rev.Id);
            }
        }
        
    }
    
    public void SendAutomatedEmails(list<Review__c> newlist){
        
        for(Review__c rev: newlist){
            if(rev.Type__c == 'Post-Inspection Result'){
                
                //revIds.add(rev.id);        
                dsmtReviewUtil.dsmtReviewSendEmail(rev.Id);
                
            }
        }
        
    }
}