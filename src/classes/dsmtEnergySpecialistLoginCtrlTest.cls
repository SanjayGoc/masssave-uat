@isTest
public class dsmtEnergySpecialistLoginCtrlTest {
	@isTest
    Public Static Void RunTest(){
        
        User u = Datagenerator.CreatePortalUser();
               
        System.runAs(u) {
           Checklist__c chklst = new Checklist__c(
                Unique_Name__c = 'Energy_Specialist_Login_Form',
                Reference_ID__c='erw343'
            );
            insert chklst;
            
            Checklist_Items__c chkitm = new Checklist_Items__c(
                Checklist__c='test chk',
                Reference_ID__c='A123',
                Parent_Checklist__c=chklst.id
            );
            insert chkitm;
            
             Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            insert ta;
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c=u.ContactId;
            dsmtc.Portal_User__c = u.Id;
            insert dsmtc;
            
            dsmtEnergySpecialistLoginCtrl dsmtlogin = new dsmtEnergySpecialistLoginCtrl();
            dsmtlogin.username='tuser@test.org';
            dsmtlogin.password='pwd';
            dsmtlogin.login();
            Checklist__c c = dsmtlogin.pgCheckList;
            dsmtTradeallyLoginCtrl dsmtloginend = new dsmtTradeallyLoginCtrl();
            ApexPages.currentPage().getParameters().put('u','tuser@test.org');
            ApexPages.currentPage().getParameters().put('p','pwd');
            dsmtlogin.relogin();
           // dsmtlogin.endlogin();
           ApexPages.currentPage().getParameters().put('u','tuser1@test.org');
            ApexPages.currentPage().getParameters().put('p','pwd');
            dsmtlogin.relogin2();
            
            ApexPages.currentPage().getParameters().put('u','tuser1@test.org');
            ApexPages.currentPage().getParameters().put('from','pwd');
            dsmtlogin.relogin2();
        	
           ApexPages.currentPage().getParameters().put('u','tuser@test.org');
        
           dsmtlogin.relogin3();
            
            dsmtlogin.getChecklists();
            
            dsmtlogin.endlogin();
            dsmtlogin.endlogoutuser();
            dsmtlogin.redirectIfLoggedIn();
            String session_Id = dsmtlogin.session_Id;
            String sectionTitle = dsmtlogin.sectionTitle;
        }
            
    }
    
    @isTest
    Public Static Void RunTest1(){
        
        User u = Datagenerator.CreatePortalUser();
               
        System.runAs(u) {
           Checklist__c chklst = new Checklist__c(
                Unique_Name__c = 'Energy_Specialist_Login_Form',
                Reference_ID__c='erw343'
            );
            insert chklst;
            
            Checklist_Items__c chkitm = new Checklist_Items__c(
                Checklist__c='test chk',
                Reference_ID__c='A123',
                Parent_Checklist__c=chklst.id,
                Special_instruction_for_public_portal__c= true
            );
            insert chkitm;
            
             Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            insert ta;
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c=u.ContactId;
            dsmtc.Portal_User__c = u.Id;
            dsmtc.Active__c=false;
            insert dsmtc;
            
            ApexPages.currentPage().getCookies().put('eaid',new Cookie('eaid', 'test', null, -1, true));
       		ApexPages.currentPage().getCookies().put('revId',new Cookie('revId', 'test', null, -1, true));
            
            dsmtEnergySpecialistLoginCtrl dsmtlogin = new dsmtEnergySpecialistLoginCtrl();
            dsmtlogin.username='tuser@test.org';
            dsmtlogin.password='pwd';
            dsmtlogin.login();
           
            dsmtlogin.getChecklists();
        }
            
    }
}