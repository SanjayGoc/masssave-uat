/**
        Name         : dsmtMailTemplatePageController
        Author       : 
        Date         : 28-05-2017
        Description  : This Class Send Email whith up to five attachments.
*/

public class dsmtMailTemplatePageController {
    public String toAddress {
        get;
        set;
    }
    public String toAddressName {
        get;
        set;
    }
    public String subject {
        get;
        set;
    }
    public String ccAddress {
        get;
        set;
    }
    public String body {
        get;
        set;
    }
    public String fromAddress {
        get;
        set;
    }
    public List < Attachment > attachmentList {
        get;
        set;
    }
    public boolean isShowSelectTemp {
        get;
        set;
    }
    public String selTemplateId {
        get;
        set;
    }
    public String regReqId {
        get;
        set;
    }
    public List < AttachmentModel > listAttmod {
        set;
        get;
    }
    public string bccAddress {
        get;
        set;
    }
    public string server_url {
        get;
        set;
    }
    public string GUID {
        get;
        set;
    }
    public Registration_Request__c RegRequest {
        get;
        set;
    }
    public string newGUID {
        get;
        set;
    }
    public string attach_ids {
        get;
        set;
    }
    public Task tsk {
        get;
        set;
    }
    public List < selectOption > toContactOptions {
        get;
        set;
    }
    public string toId {
        get;
        set;
    }
    public String No_of_days_for_responseControl {
        get;
        set;
    }
   
    public boolean isSendEmail{get;set;}
    
    set < String > setCustomerEmails = new set < String > ();
    
    public boolean isError;
    public Boolean isMIR = false;
    
    public String ReviewId = '';
    public String TaskId = '';
    
    String randStr = '';
    
    private String todoEmail;
    private String PlainText;
    boolean isRejection = false;
    public String OrgId{get;set;}
    // Constructor start
    public dsmtMailTemplatePageController() {
        No_of_days_for_responseControl = '1';

        OrgId = UserInfo.getOrganizationId();
        OrgId = OrgId.substring(0,15);
        server_url = System.URL.getSalesforceBaseUrl().toExternalForm();
        toId = todoEmail = ccAddress = '';
        listAttmod = new List < AttachmentModel > ();
        RegRequest  = new Registration_Request__c();
        toAddressName = '';
        
        
        attachmentList = new List < Attachment > ();
        attachmentList.add(new Attachment());
        attachmentList.add(new Attachment());
        attachmentList.add(new Attachment());
        
        selTemplateId = ApexPages.Currentpage().getParameters().get('TemplateID');
        regReqId          = ApexPages.Currentpage().getParameters().get('objId');
        String mir    = ApexPages.Currentpage().getParameters().get('MIR');
        TaskId        = ApexPages.Currentpage().getParameters().get('TaskId');
        String sendemail = ApexPages.Currentpage().getParameters().get('sendEmail');
        isRejection = apexpages.currentpage().getparameters().get('isRejection') == 'true' ? true : false;
        if (mir != null && mir == 'true')
            isMIR = true;
            
        tsk = new Task();
        toId = ApexPages.Currentpage().getParameters().get('toId');
        string ccEmails = ApexPages.Currentpage().getParameters().get('ccEmails');
         if(selTemplateId == null || (sendemail != null && Boolean.valueOf(sendemail) == true)){
           isSendEmail = true;
           getAllEmailTemplateFolders();
        }else{
           isSendEmail = false;
        }
        selFolderId = ApexPages.currentPage().getParameters().get('folderId');
        selTemplateId = ApexPages.currentPage().getParameters().get('TemplateID');
       
       
        getAllEmailTemplateFolders();
        isSendEmail = true;
        
           getEmailTemplatesByfolder();

        if (regReqId != null && regReqId.trim().length() > 0) {
            List < Registration_Request__c > lstRRs = [select id,Status__c,Outbound_Message_Ids__c,Email__c,DSMTracker_Contact__c,Trade_Ally_Account__c,(select id, name, Description from Attachments) 
                                                         FROM Registration_Request__c WHERE Id = : regReqId];
            if (lstRRs.size() > 0) {
                regRequest = lstRRs[0];
                
                if(ApexPages.Currentpage().getParameters().get('Pagename') == 'sendemail'){
                    selTemplateId = FetchTemplateId('sendemail',regRequest);
                }
           }
        }    
        
        
           ccEmails = ApexPages.Currentpage().getParameters().get('ccEmails');
           if (ccEmails != null && ccEmails.trim().length() > 0) {
               set < string > emlSet = new set < String > ();
               for (String eml: ccEmails.split(',')) {
                   if (eml != todoEmail && eml != null && eml.length() > 0 && !emlSet.contains(eml)) {
                      ccAddress += eml + ',';
                      emlSet.add(eml);
                   }
               }
          }
        
          bccAddress = Userinfo.getUserEmail();
          if (selTemplateId != null && selTemplateId.length() > 0) {
              isShowSelectTemp = false;
          }
        
        //Methods
        fillAttachmentModel();
       
        if (regRequest == null || regRequest.Id == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '<li>This Registration Request does not exist.</li>'));
        }else{
            toAddressName = regRequest.Email__c+',';
        }   
        
    }
    public List<SelectOption> folderList{get;set;}
    public String selFolderId{get;set;}
    
     /**
      * Get Email Templates By Folder to fill Folder dropdown 
      *
      * @param Nothing 
      * @return Nothing.
      */
   
    public void getAllEmailTemplateFolders(){
       folderList = new List<SelectOption>();
       templateList = new List<SelectOption>();
       
       Map<String,String> folderIdtoName = new Map<String,String>();
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true order by FolderId];
       for(EmailTemplate et : emailTemplatelist){
           //if(et.Folder.Name == 'DSMT MSD Custom' || et.Folder.Name == 'DSMT Smart $aver Custom'){
               folderIdtoName.put(et.folderId,et.Folder.Name);
          // }
       }
       system.debug('--folderIdtoName--'+folderIdtoName);
        folderList.add(new SelectOption('','--Select--'));
        templateList.add(new SelectOption('','--Select--'));
        for(String folderid : folderIdtoName.keySet()){
           system.debug('--folderid--'+folderid);
           system.debug('--folderIdtoName.get(folderid)--'+folderIdtoName.get(folderid));
           if(folderIdtoName.get(folderid) != null)
             folderList.add(new SelectOption(folderid, folderIdtoName.get(folderid)));
        }
    }
    
    public List<SelectOption> templateList{get;set;}
    
    /**
     * Get Email Templates By Folder to fill Folder dropdown and template selection
     *
     * @param Nothing 
     * @return Nothing.
     */
   
    public void getEmailTemplatesByfolder(){
       templateList = new List<SelectOption>();
       templateList.add(new SelectOption('','--Select--'));
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true and FolderId =: selFolderId order by Name];
       for(EmailTemplate et : emailTemplatelist){
          templateList.add(new SelectOption(et.Id, et.Name));
       }
    }
    
    private String FetchTemplateId(String PageName,Registration_Request__c RRequest){
            String tplId = '';
            String TemplName = '';
            list<EmailTemplate>tplList = new list<EmailTemplate>();
            if(PageName == 'sendemail'){
                //TemplName  = enrollApp.Send_Email_Template__c ;
            }else if(PageName == 'ReplyToMessage'){
                //TemplName  = enrollApp.Reply_Email_Template__c;
            }
            if(TemplName != null && TemplName.trim().length()>0)
                tplList = [select id from EmailTemplate where DeveloperName =: TemplName];
            
            system.debug('####' + tplList);
            
            if(tplList.size() > 0)
             tplId = tplList[0].Id;
             
            return tplId;
    }
    
    
    
    //Method that will send Emails to the selected ContactLists
    public pagereference sendEmail() {
        boolean isErr = false;
        string errMsg = '';
        
        if (toAddressName == null || toAddressName.trim().length() == 0) {
            errMsg += '<li>To is required for send email!</li>';
            isErr = true;
        }
        
        if (subject == null || subject.trim().length() == 0) {
            errMsg += '<li>Subject is required for send email!</li>';
            addMessage(errMsg);
            isErr = true;
        }
        
       
        if (isErr == false) {
            //Initialize/reset variables
            isError = false;
            string errorMsg = '';

            list < string > additionalList = new list < string > ();
            if (toAddressName != null && toAddressName.length() > 0) {
                additionalList = toAddressName.deletewhitespace().split(',');
            }
            list < string > ccAddressList = new list < string > ();
            
            
            system.debug('--ccAddress---'+ccAddress);
            if (ccAddress != null && ccAddress.length() > 0) {
                ccAddressList = ccAddress.deletewhitespace().split(',');
            }
            
            system.debug('--ccAddressList --'+ccAddressList);
            
            ccAddressList.add(Label.Bounce_Tracking_Email_Address); // Added on 12-7-2014 to enhance bounce email functionality.


            list < string > bccAddressList = new list < string > ();
            if (bccAddress != null && bccAddress.length() > 0) {
                bccAddressList = bccAddress.deletewhitespace().split(',');
                bccAddressList.add(UserInfo.getUserEmail());
            }

            // Attach Attachments
            list < Messaging.Singleemailmessage > emails = new list < Messaging.Singleemailmessage > ();
            list < Messaging.Emailfileattachment > listAttach = new list < Messaging.Emailfileattachment > ();
            
            string attachmentNamesList = 'Attachments:\n';
            set < Id > attIds = new set < Id > ();
            String attachmentUrl = '';
            
            for (AttachmentModel mod: listAttmod) {
                if (mod.isAttach) {
                    String newURL = '';
                     String oldURL =  mod.attachment.Email_File_Download_URL__c;
                     String[] questionsplit = oldURL.split('\\?');
                     String[] ampSplit = questionsplit[1].split('&');
                     newURL = questionsplit[0]+'?';
                     system.debug('--newURL--'+newURL);
                     system.debug('--ampSplit'+ampSplit);
                     for(String parameter : ampSplit){
                       String[] equalsplit = parameter.split('=');
                       if(equalsplit.size() > 1){
                         newURL += equalsplit[0]+'='+EncodingUtil.urlEncode(equalsplit[1],'UTF-8')+'&'; 
                       }else{
                         newURL += equalsplit[0]+'&'; 
                       }
                       
                     }
                     newURL = newURL.subString(0,newURL.length() - 1);
                     system.debug('--newURL--'+newURL);
                    attachmentUrl += '<li><a target="_self" href="' + newURL + '">' + mod.attachment.Attachment_Name__c + '</a></li>';
                    attIds.add(mod.attachment.id);
                    
                    
                }
            }

            //Add to,Cc,AttionalTo
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
            system.debug('---additionalList ---'+additionalList );
           
            
            if (additionalList != null && additionalList.size() > 0)
                email.setToAddresses(additionalList);
            if (ccAddressList != null && ccAddressList.size() > 0)
                email.setCcAddresses(ccAddressList);
            if (bccAddressList != null && bccAddressList.size() > 0)
                email.setBCcAddresses(bccAddressList);
            
            String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        
            while (randStr.length() < 18) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
               randStr += chars.substring(idx, idx+1);
            }
    
            email.setSubject(subject);
            email.setSaveAsActivity(false);
            List<OrgWideEmailAddress >lstOrgWideEmailAddress =[select id, Address, DisplayName from OrgWideEmailAddress
                                                          WHERE DisplayName='AMP Online Rebate Program']; // AND Address='prescriptiveincentives@duke-energy.com'];
           
            List<OrgWideEmailAddress> lstAllAddresses = [Select Id,Address,DisplayName from OrgWideEmailAddress]; // Get all so that we have a list of all senders
           
            if(lstOrgWideEmailAddress.size() >0){
                email.setOrgWideEmailAddressId(lstOrgWideEmailAddress[0].id);
                FromAddress =   lstOrgWideEmailAddress[0].Address;
            }
            String currUserId = UserInfo.getUserId(); 
          
            
            //Fill in the BODY        
            String taskDescription = body;
            if (attachmentUrl != '') {
                attachmentUrl = '<br/><b>Download Attachments Here:</b><br/>' + attachmentUrl;
            }
            
             if(fromAddress == '' || fromAddress == null)
                fromAddress = userinfo.getUserEmail();
                
            
            Messages__c msg = new Messages__c();
            msg.From__c = fromAddress;
            msg.To__c = string.join(email.getToAddresses(),',');
            
            if(email.getCCAddresses()!=null)
                msg.CC__c = string.join(email.getCCAddresses(),',');
            if(email.getBCCAddresses()!=null)
                msg.BCC__c = string.join(email.getBCCAddresses(),',');
                
            msg.Registration_Request__c = regReqId;
            msg.Subject__c = 'More Information Request';
            msg.Message__c = body ;
            msg.Mesage_Text_Only__c = htmlToText(body );
            msg.Message_Type__c = 'Send Email';
            msg.Message_Direction__c  = 'Outbound';
            msg.Status__c = 'Sent';
            msg.Sent__c = Datetime.Now();
            if(regRequest.DSMTracker_Contact__c!=null)
                msg.DSMTracker_Contact__c = regRequest.DSMTracker_Contact__c;
            if(regRequest.Trade_Ally_Account__c !=null)
                msg.Trade_Ally_Account__c = regRequest.Trade_Ally_Account__c;
                 
            insert msg;
            String MsgId = msg.id;
            
            email.setHtmlBody(body +  attachmentUrl);
            String PlainText = email.getPlainTextBody();
            if (listAttach != null && listAttach.size() > 0) {
                email.setFileAttachments(listAttach);
            }
            
            emails.add(email);
            
           
            
            try {
                if(!Test.isRunningTest())
                Messaging.sendEmail(emails);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Message Sent Successfully'));
                
                string exp_ids = ApexPages.currentPage().getParameters().get('exp_ids');
                List<Message_Exception__c> meLst = new List<Message_Exception__c>();
                Message_Exception__c me = null;
                
                if(exp_ids != null && exp_ids.trim().length() > 0){
                    list<Exception__c> exceptions = [select Id, Disposition__c from Exception__c where id in : exp_ids.split(',')];
                    if(exceptions != null && exceptions.size() > 0){
                        for(Exception__c e : exceptions){
                            me = new Message_Exception__c();
                            me.message__c = MsgId;
                            me.Exception__c = e.Id;
                            meLst.add(me);
                            e.Disposition__c = 'Awaiting Customer Information';
                        }
                        if(!Test.isRunningTest())
                            update exceptions;    
                    }
                }
                
                if(meLst.size() > 0){
                    upsert meLst;
                }
                
                for (Attachment att: attachmentList) {
                    att.body = null;
                }
                if (isMIR) {
                    string taskComments = email.getHtmlBody();
                    
                    //Method that will create task for current user
                    create_task(subject, taskComments, regReqId);
                    
                   
                    if(isRejection == false){
                       
                        regRequest.Status__C = 'More Information Requested';
                        regRequest.Next_Notification_Counter__c = regRequest.Next_Notification_Counter__c != null ? regRequest.Next_Notification_Counter__c + 1 : 1;
                    }
                    
                    if(!Test.isRunningTest())
                        update regRequest;
                    
                    if (regRequest.Outbound_Message_Ids__c == null)
                        regRequest.Outbound_Message_Ids__c = '';
                        
                    list < Exception__c > lstNeedUpdateExceptions = [select id 
                                                                     from Exception__c 
                                                                     WHERE Id in : regRequest.Outbound_Message_Ids__c.split(',')];
                    if (lstNeedUpdateExceptions.size() > 0) {
                        for (Exception__c exp: lstNeedUpdateExceptions) {
                            exp.Sent_Date__c = System.today();
                        }
                        if(!Test.isRunningTest())
                            update lstNeedUpdateExceptions;
                    }
                    
                    //Method that will create custom Exceptions
                    createCustomException();
                }
                
                return new Pagereference('/' + regReqId);
            } catch (Exception ex) {
                addMessage(ex.getMessage());
                for (Attachment att: attachmentList) {
                    att.body = null;
                }
                return null;
            }
        } else {
            addMessage(errMsg);
            return null;
        }
    }
        
        
    //Method that will create custom Exception record from Custom_Exception_Setting__c custom setting and link to Enrollment Application
    private void createCustomException() {
        Exception__c exp = new Exception__c(Registration_Request__c = regRequest.id, MIR__c = true, Hidden__c = true, Disposition__c = 'Awaiting Customer Information');
        
        String excepMsg = Email_Custom_Setting__c.getOrgDefaults().RR_MIR_Exception_Message__c;
        if (excepMsg == null || excepMsg == '') {
            excepMsg =  'A more information request was sent on XX/XX/XXXX and is pending';
        }
        exp.Exception_Message__c = excepMsg.replace('XX/XX/XXXX', datetime.now().format('MM/dd/yyyy'));
        exp.Internal__c = true;
        insert exp;
    }

    // Method that create task for user
    private void create_task(string subject, string description,String whatId) {
        tsk = new Task();
        tsk.Subject = 'Email : '+subject;
        tsk.OwnerId = UserInfo.getUserId();
        tsk.Description = htmlToText(description);
        tsk.WhatId = whatId ;
        tsk.Status = 'Completed';
        tsk.ActivityDate= system.today();
        insert tsk;
    }

    private String htmlToText(String htmlString) {
        String RegEx = '(</{0,1}[^>]+>)';
        if (htmlString == null)
            htmlString = '';
        return htmlString.replaceAll('&nbsp', '').replaceAll(';', '').replaceAll(RegEx, '');
    }

    // This method is for Cancel
    public pagereference cancel() {
        return new PageReference('/' + regReqId);
    }

    // This Method Fill HTML Body using Template    
    //***************************** IMPORTANT ***************************
    //** Must make sure that a contact exists with the last name 'Do Not Delete' in order for this to work.
    public PageReference fillHtmlBody() {
        
        if (selTemplateId != null && selTemplateId.trim().length() > 0) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {
                Label.Email_Address_for_Mail
            };
            mail.setToAddresses(toAddresses);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(true);
            mail.setSenderDisplayName('MMPT');
            mail.setWhatId(regReqId);
                
            List < Contact > lstContact = [select id, name, email from contact where Name = 'Do Not Delete'];
            Id conId = null;
            if(lstContact.size()>0){
                conId = lstContact[0].id;
            }
            else{
                List<Account> lstAcc = [select id from Account where Name='UCS'];
                Id AccId = null;
                if(lstAcc.size()>0)
                    AccId = lstAcc[0].Id;
                else{
                    Account acc = new Account(Name='UCS');
                    insert acc;
                    
                    AccId = acc.Id;
                }
                Contact c = new Contact(LastName='Do Not Delete',AccountId=AccId ,Email='support+dnd@unitedcloudsolutions.com');
                insert c;
                conId = c.Id;
            }
            
            mail.setTargetObjectId(lstContact[0].id);
            mail.setTemplateId(selTemplateId);
            Savepoint sp = Database.setSavepoint();

            try {
                if(!Test.isRunningTest()){
                    system.debug('--mail---'+mail);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                        mail
                    
                });
                }
                List < EmailTemplate > emailTemp = [select TemplateType, Id from EmailTemplate where id = : selTemplateId limit 1];
                if (emailTemp != null && emailTemp.size() > 0) {
                    if (emailTemp[0].TemplateType.equalsIgnoreCase('text')) {
                        body = mail.getPlainTextBody();
                    } else {
                        body = mail.getHtmlBody();
                        body = body.replace('<meta charset="utf-8">', '').replace('<br>', '<br/>');
                        body += '</body>';
                    }

                }
                subject = mail.getSubject();
                Database.rollback(sp);
            }
            Catch(Exception e) {
                addMessage(e.getMessage());
                Database.rollback(sp);
            }

        }
        return null;
    }

    public void fillAttachmentModel() {
        
        Map<String,boolean> isattachmap = new Map<String,boolean>();
        
        if(listAttmod != null && listAttmod.size() > 0){
           for(AttachmentModel mod : listAttmod){
              isattachmap.put(mod.Attachment.id,mod.isAttach);
           }
        }
        
        listAttmod = new List < AttachmentModel > ();

        for (Attachment__c attach: [select id, Name, Attachment_Name__c, Attachment_Type__c, File_Download_Url__c, File_Url__c, CreatedDate, CreatedBy.Name,Email_File_Download_URL__c from Attachment__c
            WHERE Registration_Request__c = : RegRequest.id
        ]) {
                AttachmentModel mod = new AttachmentModel();
                if(isattachmap.get(attach.id) != null){
                   mod.isAttach = isattachmap.get(attach.id);
                }else{
                   mod.isAttach = true;
                }
                mod.Attachment = attach;
                listAttmod.add(mod);
        }
    }
    
    public void addMessage(String Msg) {
        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, Msg));
    }

    public void call() {
    }
    
    //Modal class
    public class AttachmentModel {
        public Attachment__c attachment {
            set;
            get;
        }
        public Boolean isAttach {
            set;
            get;
        }
        public AttachmentModel() {
            attachment = new Attachment__c();
            isAttach = false;
        }
    }
    
    /**
     * Reload page based on Program
     *
     * @param Nothing 
     * @return Pagereference .
     */
   
    public PageReference reloadPage(){
       
       PageReference pg = null;
       pg = Page.dsmtMailTemplate;
       
       pg.getParameters().put('objId', regReqId);
       pg.getParameters().put('TemplateID', selTemplateId);
       pg.getParameters().put('folderId',selFolderId);
       pg.getParameters().put('MIR','true');
       pg.getParameters().put('ccEmails',ApexPages.Currentpage().getParameters().get('ccEmails'));
       pg.getParameters().put('toId',ApexPages.Currentpage().getParameters().get('toId'));
       pg.getParameters().put('TaskId',ApexPages.Currentpage().getParameters().get('TaskId'));
       pg.getParameters().put('ReviewId',ApexPages.Currentpage().getParameters().get('ReviewId'));
       pg.getParameters().put('isRejection',ApexPages.Currentpage().getParameters().get('isRejection'));
       pg.getParameters().put('exp_ids',ApexPages.Currentpage().getParameters().get('exp_ids'));
       pg.setRedirect(true);
       return pg;
       
    }
}