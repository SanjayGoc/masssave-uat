public class dsmtUpdateRecommendationController {
    public boolean isError{get;set;}
    public dsmtUpdateRecommendationController(ApexPages.StandardController controller) {

    }


    public void updateRecommendation() {
        try
        {
            dsmtRecommendationHelper.disableRolluptriggeronRec = true;
            String cqnt = ApexPages.currentPage().getParameters().get('cqnt');
            String iqnt = ApexPages.currentPage().getParameters().get('iqnt');
            Recommendation__c recom = new Recommendation__c(id = ApexPages.currentPage().getParameters().get('id'));
            
            if(cqnt != null && iqnt != null)
            {
                if(cqnt != null && cqnt.trim().length() > 0)
                    recom.Change_Order_Quantity__c = decimal.valueOf(cqnt);
                else if(cqnt == null || cqnt.trim().length() == 0)
                    recom.Change_Order_Quantity__c = null;
                    
                if(iqnt != null && iqnt.trim().length() > 0)    
                    recom.Change_Order_Reason__c = iqnt;
                else if(iqnt == null || iqnt.trim().length() == 0)
                    recom.Change_Order_Reason__c = null;
            }
                            
            update recom;
        }
        catch(Exception ex){
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
}