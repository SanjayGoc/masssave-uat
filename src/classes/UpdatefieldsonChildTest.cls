@isTest 
private class UpdatefieldsonChildTest 
{
    static testMethod void Method1() 
    {
        test.startTest();
        Customer__c cust = new Customer__c();
        cust.Name = 'test';
        cust.First_Name__c = 'test1';
        cust.Last_Name__c = 'test2';
        cust.Gas_Provider_Name__c ='National Grid Gas';
        cust.Electric_Provider_Name__c = 'National Grid Electric';
        cust.Service_Address__c = 'test1';
        cust.Service_State__c = 'test3';
        cust.Service_Zipcode__c = '60001';
        cust.Primary_Email__c ='test@test.com';
        cust.Electric_Account_Number__c = '123654122';
        cust.Gas_Account_Number__c = '56982332';
        insert cust;
        
        Workorder__c wrk = new Workorder__c();
        wrk.Customer__c = cust.Id;
        wrk.Early_Arrival_Time__c = DateTime.newInstance(2018, 1, 1, 10, 30, 0); 
        wrk.Late_Arrival_Time__c = Datetime.newInstance(2018, 1, 1, 11, 0, 0);
        insert wrk;
        
        
        Call_List_Line_Item__c call = new Call_List_Line_Item__c();
        call.Customer__c = cust.Id;
        insert call;
       
       // update cust;
        
        test.stopTest();
    }
}