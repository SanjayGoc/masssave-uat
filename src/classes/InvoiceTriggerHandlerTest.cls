@istest
public class InvoiceTriggerHandlerTest
{
	@istest
    static void runtest()
    {
 
        Account acc =new Account();
        acc.Name='test';
        acc.BillingStreet='';
        acc.BillingCity ='';
        acc.BillingState='';
        acc.BillingPostalCode='';
        acc.Bill_To_Email__c='test@gmail.com';
        acc.Bill_To_Phone__c='4526938';
        acc.Remit_Address__c='';
        acc.Remit_City__c='';
        acc.Remit_State__c='';
        acc.Remit_Postal_Code__c='';
        acc.Contract__c='';
        acc.Bill_to_Attn__c='';
        //iv.Id=acc.id;
        insert acc;
        
        Invoice__c iv =new Invoice__c();
        iv.Invoice_Type__c='NSTAR413';
        iv.Provider__c='Eversource East Gas';
        iv.Work_Type__c='ISM';
        iv.Account__c=acc.Id;
        insert iv;
        
        InvoiceType__c inv =new InvoiceType__c();
        inv.Name='test1';
        inv.IsActive__c=true;
        inv.Work_Type__c = iv.Work_Type__c;
        inv.Provider__c = iv.Provider__c;
        insert inv;
        
        InvoiceTriggerHandler ith =new InvoiceTriggerHandler();
        
    }
}