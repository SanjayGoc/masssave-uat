public with sharing class dsmtenrgyassandprojController{
    
    public List<Project_and_Energy_Assessment__c> lstprojea{get;set;}
    Public List<Eaprojobj> eaprojObjlist{get;set;}
    public String Eaid {get;set;}
   
    
    public dsmtenrgyassandprojController(ApexPages.StandardController sc){
        
        Eaid = sc.getId();
        lstprojea = new List<Project_and_Energy_Assessment__c>();        
        
        getAllpea();
        eaprojObjlist = new List<Eaprojobj>();
        for(Project_and_Energy_Assessment__c pea : lstprojea){
            Eaprojobj eaobj = new Eaprojobj();
            eaobj.Id = pea.Id;
            eaobj.Name = pea.Name;
            eaobj.energyassessment= pea.Energy_Assessment__r.Name;
            eaobj.project = pea.Project__r.Name;
            eaobj.specialincentive= pea.Special_Incentive__c;
            eaprojObjlist.add(eaobj);
        }
    }
    
     public void getAllpea(){
        
        lstprojea = [select Id,Name,Energy_Assessment__r.Name,Project__r.Name,Special_Incentive__c from Project_and_Energy_Assessment__c where Energy_Assessment__c =: Eaid];
        
     }
 
    Public class Eaprojobj{
        public String Id{get;set;}
        public String Name{get;set;}
        public string energyassessment{get;set;}
        public String project{get;set;}
        public String specialincentive{get;set;}
        
    }
}