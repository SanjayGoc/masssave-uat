global virtual class dsmtEnergyAssessmentBaseController {
    public static final String KEY_ASSESSMENT_ID = 'assessId';
    public static final String KEY_CUSTOMER_ID = 'custId';
    public static final String KEY_APPOINTMENT_ID = 'id';
    
    public Energy_Assessment__c ea{get;set;}
    public Building_Specification__c bs{get;set;}
    public Air_Flow_and_Air_Leakage__c afal{get;set;}
    public Customer__c customer{get;set;}
    public Eligibility_Check__c ec{get;set;}
    public Appointment__c appointment{get;set;}
    public Thermal_Envelope__c te{get;set;}
    
    public String accessorName{get;set;}
    
    Public String Provider{get;set;}
    Public String AccountType{get;set;}
    Public String AccountNumber{get;set;}
    
    // DSST-11271 by HP on 11th Sep 2018
    
    public static boolean donotExecuteWorkorderTrigger = false;
    public static boolean dsmtBIPopulateFieldOnAssessment = false;
    
    // INC0114588 by HP on 17th sep 2018 to Fix SOQL Issue
    
    public Map<Id,List<Appointment__c>> appMainMap = new Map<Id,List<Appointment__c>>();
    public Map<Id,List<Eligibility_Check__c>> ecMainMap = new Map<Id,List<Eligibility_Check__c>>();
    
    /* 
    Start Ticket DSST-6294
    Modified by Kaushik Rathore 
    Date: 13/07/2018
   */
    public String getCurrentPageName(){
        return ApexPages.currentPage().getUrl().substringAfter('/apex/').substringBefore('?');
    }
    /* End Ticket DSST-6294 */
    public String uniqueExternalId{get{
        return DateTime.now().format('dMyyyyHmsS');
    }}
    
    public boolean hasPageError{get{return ApexPages.hasMessages(ApexPages.severity.ERROR);}}
    public String getParam(String paramKey){
        return ApexPages.currentPage().getParameters().get(paramKey);
    }
    
    public Note newNote{get{
        if(this.newNote == null){
            this.newNote = new Note();
        }
        return this.newNote;
    }set;}
    
    
    public List<Note> loadNotes(Id parentId){
        return [SELECT Id,Title,Body,CreatedDate FROM Note WHERE ParentId =:parentId ORDER BY CreatedDate DESC];
    }
    public void saveNote(){
        String parentId = getParam('parentId');
        if(this.newNote!= null && parentId != null && parentId.trim() != ''){
            try{
                this.newNote.ParentId = parentId;
            }catch(Exception ex){
                // FIELD NOT WRITABLE
                // 
                String noteId = this.newNote.Id;
                this.newNote = this.newNote.clone(false);
                this.newNote.ParentId = parentId;
                if(noteId != null && noteId.trim() != ''){
                    this.newNote.Id = noteId;
                }
            }
            if(!isValidSalesforceId(this.newNote.Id,Note.Class)){
                this.newNote.Id = null;
            }
            UPSERT this.newNote;
            this.newNote = null;
        }
    }
    public void editNote(){
        String noteIdToEdit = getParam('noteIdToEdit');
        if(noteIdToEdit != null && noteIdToEdit.trim() != ''){
            List<Note> result = [SELECT Id,Title,Body,ParentId FROM Note WHERE Id =:noteIdToEdit ];
            if(result.size() > 0){
                this.newNote = result.get(0);
            }
        }
    }
    public void deleteNote(){
        String noteId = getParam('noteId');
        if(noteId != null)
            delete new Note(Id = noteId);
    }
    
    public List<Attachment> loadAttachments(Id parentId){
        return [SELECT Id,Name,CreatedDate FROM Attachment WHERE ParentId =:parentId ORDER BY CreatedDate DESC];
    }
    public void deleteAttachment(){
        String attachmentId = getParam('attachmentId');
        if(attachmentId != null)
            delete new Attachment(Id = attachmentId);
    }
    
    public PageReference validateRequest(){
        String assessId = getParam(KEY_ASSESSMENT_ID),
            custId = getParam(KEY_CUSTOMER_ID),
            appointmentId = getParam(KEY_APPOINTMENT_ID);
        
        ec = new Eligibility_Check__c();
        accessorName = 'N/A';
        loadAppointmentData(appointmentId);
        
        if((assessId == null || assessId.trim() == '') && custId != null && custId.trim() != '' && appointmentId != null && appointmentId.trim() != ''){
            //loadAppointmentData(appointmentId);
            dsmtRecursiveTriggerHandler.preventEligiblityCheckTrigger = true;
            // DSST-11271 by HP on 11th Sep 2018
            donotExecuteWorkorderTrigger = true;
            ec.No_Cap_Insulation__c = true;
            update ec;
            
            dsmtRecursiveTriggerHandler.preventEligiblityCheckTrigger = false;
            
            bs = new Building_Specification__c(
                Appointment__c = appointmentId
            );
            INSERT bs;
            
            ea = new Energy_Assessment__c(
                Customer__c = custId,
                Appointment__c = appointmentId,
                Status__c = 'New',
                Building_Specification__c = bs.Id,
                Eligibility_Check__c = (ec != null && ec.Id != null) ? ec.Id : null // DSST-9832 by Kaushik Rathore on 20th Sep 2018
            );
            dsmtWorkOrderHelper.UpdatefieldsonChild= true; // DSST-9832 by Kaushik Rathore on 20th Sep 2018
            INSERT ea;
            
            // CREATE NEW ASSESSMENT
            Pagereference pr = ApexPages.currentPage();
            pr.getParameters().put(KEY_ASSESSMENT_ID,ea.Id);
            pr.getParameters().put(KEY_CUSTOMER_ID,custId);
            pr.getParameters().put(KEY_APPOINTMENT_ID,appointmentId);
            pr.setRedirect(true);
            return pr;
        }else{
            if(assessId != null && assessId != '' && custId != null && custId.trim() != '' && appointmentId != null && appointmentId.trim() != ''){
                ea = new Energy_Assessment__c(
                    Customer__c = custId,
                    Appointment__c = appointmentId,
                    //Status__c = 'New', DSST-13879 | Kaushik Rathore | 17 Oct, 2018
                    Id = assessId,
                    Eligibility_Check__c = (ec != null && ec.Id != null) ? ec.Id : null // DSST-9832 by Kaushik Rathore on 20th Sep 2018
                );
                
                update ea;
            }
        }
        
        if(custId != null){
            List<Customer__c> customers = [
                SELECT 
                    Id,Name,First_Name__c,Last_Name__c,City_Text__c,Service_Address__c,State_text__c,Zipcode_text__c,Phone__c,Email__c,
                    Service_City__c,Service_State__c,Service_Zipcode__c,                    
                    Gas_Account_Number__c,Electric_Account_Number__c,Gas_Provider_Name__c,Electric_Provider_Name__c,Gas_Rate_Code__c,Electric_Rate_Code__c
                FROM Customer__c 
                WHERE Id=:custId
            ];
            if(customers.size() > 0){
                customer = customers.get(0);
            }
        }
        loadBuildingSpecificationData(assessId);
        //loadThermalEnvelopeData(assessId);
        return null;
    }
    
    public void loadBuildingSpecificationData(String assessId){
        if(assessId != null && assessId.trim() != ''){
            List<Energy_Assessment__c> eas = Database.query('SELECT ' + getSObjectFields('Energy_Assessment__c') + ',Trade_Ally_Account__r.Name,Trade_Ally_Account__r.Account__r.RecordType.Name,(SELECT Id,Name FROM Market_Outreach__r) FROM Energy_Assessment__c WHERE Id=:assessId');//[SELECT Id,Name,Building_Specification__c,Mechanical_Next_Number__c FROM Energy_Assessment__c WHERE Id=:assessId];
            if(eas.size() > 0){
                this.ea = eas.get(0);
                if(ea.Building_Specification__c == null){
                    bs = new Building_Specification__c();
                    INSERT bs;
                    
                    this.ea.Building_Specification__c = bs.Id;
                    UPDATE this.ea;
                }else{
                    bs = [
                        SELECT 
                            Id,Name,Year_Built__c,Actual_Year_Built__c,Bedromm__c,Actual_Bedrooms__c,Occupants__c,Actual_Occupants__c,Orientation__c,Natural_Gas__c,Is_Manufactured_Home__c,
                            Ceiling_Heights__c,Actual_Ceiling_Heights__c,Actual_Floors__c,Floors__c,Previous_Floor_Area__c,Actual_Floor_Area__c,Floor_Area__c,Finished_Basement_Area__c,Total_Floor_Area__c,Total_Volume__c,
                            Units__c,Dwelling_attached_on_Back__c,Dwelling_attached_on_Bottom__c,Dwelling_attached_on_Front__c,
                            Dwelling_attached_on_Left__c,Dwelling_attached_on_Right__c,Dwelling_attached_on_Top__c,Attached__c,Enclosed_Cavity_Areas__c
                        FROM Building_Specification__c 
                        WHERE Id=:ea.Building_Specification__c LIMIT 1];
                    bs.Previous_Floor_Area__c = bs.Floor_Area__c;
                }
                loadAirFlowAirLeakageData(bs.Id);
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid Energy assessment id ' + assessId));
            }
        }else{
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Url param "' + KEY_ASSESSMENT_ID + '" missing from url.'));
        }
    }
    public void loadAirFlowAirLeakageData(String buildingSpecificationId){
        if(buildingSpecificationId != null){
            List<Air_Flow_and_Air_Leakage__c> afals = [
                SELECT Id, CFM50__c, Leakiness__c, ActualCFM50__c, ActualLeakiness__c, Building_Specifications__c, 
                Next_Blower_Door_Reading_Number__c,Next_Pressure_Differential_Reading_Numbe__c,
                TightnessLimit__c, MechanicalVentilationSystemsRequiredCFM__c
                FROM Air_Flow_and_Air_Leakage__c
                WHERE Building_Specifications__c =:buildingSpecificationId
                ORDER BY CreatedDate DESC
                LIMIT 1
            ];
            if(afals.size() > 0){
                afal = afals.get(0);
            }else{
                afal = new Air_Flow_and_Air_Leakage__c(
                    Building_Specifications__c = buildingSpecificationId,
                    Next_Blower_Door_Reading_Number__c = 1,
                    Next_Pressure_Differential_Reading_Numbe__c = 1
                );
                INSERT afal;
            }
        }
    }
    
    public void loadAppointmentData(String appointmentId){
        if(appointmentId != null ){
            
            List<Appointment__c> appointments = null;
            
            // INC0114588  by HP on 17th sep 2018 to Fix SOQL Issue
            if(appMainMap.get(appointmentId) != null){
                appointments  = appMainMap.get(appointmentId);
            }else{
                 appointments  = [
                    SELECT Id,Appointment_Type__c,Name,Customer_Reference__c,Workorder__c,Workorder__r.Eligibility_Check__c,Notes__c,DSMTracker_Contact__c,DSMTracker_Contact__r.Name,
                    Customer_Reference__r.No_Cap_Insulation__c,Customer_Reference__r.Moderate_Income_Eligible__c,Customer_Reference__r.Duplex_Triple_Decker_Program__c
                    FROM Appointment__c 
                    WHERE Id=:appointmentId
                ];
                appMainMap.put(appointmentId,appointments);
                
            }
            if(appointments.size() > 0){
                appointment = appointments.get(0);
                if(appointment.DSMTracker_Contact__c != null && appointment.DSMTracker_Contact__r.Name != null && appointment.DSMTracker_Contact__r.Name.trim() != ''){
                    accessorName = appointment.DSMTracker_Contact__r.Name;
                }
                if(appointment.Workorder__c!= null){
                    if(appointment.Workorder__r.Eligibility_Check__c != null){
                        //INC0114588  by HP on 17th sep 2018 to Fix SOQL Issue
                        List<Eligibility_Check__c> ecs = null;
                        
                        if(ecMainMap.get(appointment.Workorder__r.Eligibility_Check__c) != null){
                            ecs = ecMainMap.get(appointment.Workorder__r.Eligibility_Check__c);
                        }
                        else{
                            ecs =  [
                                SELECT 
                                    Id,Name, Status__c,
                                    Heating_fuel_Type__c,Heating_Fuel_Type_Note__c,
                                    DHW_Fuel_Type__c,DHW_Fuel_Type_Note__c,
                                    Duplex_Triple_Decker_Program__c,Duplex_Triple_Decker_Program_Note__c,
                                    Moderate_Income_Eligible__c,Moderate_Income_Eligible_Note__c,
                                    No_Cap_Insulation__c,No_Cap_Insulation_Note__c,Attic__c,
                                    Appointment_Type__c,
                                    I_heat_my_home_with__c,How_many_units__c,Do_you_own_or_rent__c,Do_you_live_there__c,
                                    How_did_you_hear_about_this_program__c,Promo_Code__c,
                                    Email__c,
                                    Home_Size__c,Home_Style__c,Year_built_in__c,Thermostats__c
                                FROM Eligibility_Check__c
                                WHERE Id =:appointment.Workorder__r.Eligibility_Check__c
                            ];
                            ecMainMap.put(appointment.Workorder__r.Eligibility_Check__c,ecs);
                        }
                        if(ecs.size() > 0){
                            ec = ecs.get(0);
                            
                            if(ec.Appointment_Type__c == null)
                                ec.Appointment_Type__c = appointment.Appointment_Type__c;
                        }
                    }else{
                         // DSST-11271 by HP on 11th Sep 2018
                        donotExecuteWorkorderTrigger = true;
                        ec = new Eligibility_Check__c();
                        ec.customer__c = appointment.Customer_Reference__c;
                        ec.Duplex_Triple_Decker_Program__c = appointment.Customer_Reference__r.Duplex_Triple_Decker_Program__c;
                        ec.No_Cap_Insulation__c = appointment.Customer_Reference__r.No_Cap_Insulation__c;
                        ec.Moderate_Income_Eligible__c= appointment.Customer_Reference__r.Moderate_Income_Eligible__c;
                        
                        INSERT ec;
                        
                        Workorder__c wo = [SELECT Id,Name FROM Workorder__c WHERE Id=:appointment.Workorder__c LIMIT 1];
                        wo.Eligibility_Check__c = ec.Id;
                        UPDATE wo;
                    }
                }
            }
        }else{
            appointment = new Appointment__c();
        }
    }
    public void loadThermalEnvelopeData(String assessId){
        if(assessId != null ){
            List<Thermal_Envelope__c> tes = [
                SELECT Id,Name,Energy_Assessment__c
                FROM Thermal_Envelope__c
                WHERE Energy_Assessment__c=:assessId
            ];
            if(tes.size() > 0){
                te = tes.get(0);
            }else{
                te = new Thermal_Envelope__c();
                te.Energy_Assessment__c = assessId;
                INSERT te;
            }
        }else{
            te = new Thermal_Envelope__c();
        }
    }
    
    public void saveGeneralInformation(){
        if(this.ec != null && this.ec.id != null){
            UPDATE this.ec;
            
            if(this.appointment != null)
            {
                if(this.appointment.Appointment_Type__c != this.ec.Appointment_Type__c)
                {
                    this.appointment.Appointment_Type__c = this.ec.Appointment_Type__c;
                    this.appointment.Appointment_Type_Friendly_Name__c = this.ec.Appointment_Type__c;
                 
                    UPDATE this.appointment;
                }
            }
            
            if(this.appointment.Workorder__c != null)
            {
                List<Workorder_Type__c> workorderTypes = [SELECT Id,Name FROM Workorder_Type__c WHERE Name =:this.ec.Appointment_Type__c];
                if(workorderTypes.size() > 0)
                {
                    Workorder__c wo = [SELECT Id, Workorder_Type__c FROM Workorder__c WHERE Id=:appointment.Workorder__c LIMIT 1];
                    
                    if(wo.Workorder_Type__c != workorderTypes.get(0).Id)
                    {
                        wo.Workorder_Type__c = workorderTypes.get(0).Id;
                        UPDATE wo;    
                    }
                }
            }
            
            /*if(this.ea != null){
                List<Proposal__c> proposals = [SELECT Id,Name,Moderate_Income__c,No_Cap__c,Whole_Home__c FROM Proposal__c WHERE Energy_Assessment__c =:ea.Id];
                for(Proposal__c prop : proposals){
                    prop.Moderate_Income__c = this.ec.Moderate_Income_Eligible__c;
                    prop.No_Cap__c = this.ec.No_Cap_Insulation__c;
                    prop.Whole_Home__c = this.ec.Duplex_Triple_Decker_Program__c;
                }
                if(proposals.size() > 0){
                    UPDATE proposals;
                }
            }*/
        }
        
        if(this.customer != null && this.customer.id != null)
        {
            UPDATE this.customer;
        }
        
        if(this.ea.Building_Specification__c != null && this.ec.I_heat_my_home_with__c != null){
            bs = new Building_Specification__c(id=this.ea.Building_Specification__c); 
            bs.Natural_Gas__c = this.ec.I_heat_my_home_with__c;
            update bs;
        }
    }
    
    public void saveBuildingSpecificationDetails(){
        if(bs != null && bs.Id != null)
        {
            
            UPDATE bs;
            if(ea != null && ea.Id != null)
            {
                ea.Orientation__c = bs.Orientation__c;
                ea.Floor_Area__c = bs.Floor_Area__c;
                ea.Total_Volume__c = bs.Total_Volume__c;
                
                dsmtCreateEAssessRevisionController.stopTrigger = true;
                
                UPDATE ea;
            }
            
            String assessId = getParam(KEY_ASSESSMENT_ID);
            loadBuildingSpecificationData(assessId);
            
            Set<Id> eaIdSet = new Set<Id>();
            eaIdSet.add(assessId);
            dsmtFuture.updateBuildingModalObjects(eaIdSet);
        }
    }
    
    public void saveAirFlowAirLeakageDetails(){
        if(afal != null && afal.Id != null){
            UPDATE afal;
            
            String assessId = getParam(KEY_ASSESSMENT_ID);
            loadBuildingSpecificationData(assessId);
            loadAirFlowAirLeakageData(afal.Building_Specifications__c);
        }
    }
    
    @RemoteAction
    global static String doUploadAttachment(String acctId, String attachmentBody, String attachmentName,String mimeType, String attachmentId){
        if (acctId != null)
        {
            if (attachmentBody != null)
            {
                Attachment att = getAttachment(attachmentId);
                String newBody = '';
                if (att.Body != null)
                {
                    newBody = EncodingUtil.base64Encode(att.Body);
                }
                newBody += attachmentBody;
                att.Body = EncodingUtil.base64Decode(newBody);
                att.ContentType = mimeType;
                if (attachmentId == null)
                {
                    att.Name = attachmentName;
                    att.parentId = acctId;
                }
                
                upsert Att;
                
                return att.Id;
            }
            else
            {
                return 'Attachment Body was null';
            }
        }
        else
        {
            return 'Parent Id was null';
        }
        
        return null;
    }
    
    @RemoteAction
    global static String cloneAttachmentToEA(String acctId, String attachmentId){
        if (acctId != null)
        {
            Attachment att = getAttachment(attachmentId);
            String newBody = '';
            if (att.Body != null && att.ParentId != acctId)
            {
                Attachment eaAtt = att.clone(false, false);
                eaAtt.ParentId = acctId;
                
                insert eaAtt;
                
                /* Added for ticket DSST-6873
                   By: Prasanjeet Sharma
                */
                Attachment__c cAtt = new Attachment__c();
                cAtt.Attachment_Name__c = att.Name;
                cAtt.Application_Type__c = 'Custom';
                cAtt.Energy_Assessment__c = acctId;
                cAtt.Status__c = 'New';
                cAtt.Attachment_Type__c = 'Other';
                
                insert cAtt;
                
                Attachment sAttSign = att.clone(false, false);
                sAttSign.ParentID = cAtt.id;
                
                insert sAttSign;
                
                return eaAtt.Id;
            }
            else
            {
                /* Added for ticket DSST-6873
                   By: Prasanjeet Sharma
                */
                Attachment__c cAtt = new Attachment__c();
                cAtt.Attachment_Name__c = att.Name;
                cAtt.Application_Type__c = 'Custom';
                cAtt.Energy_Assessment__c = acctId;
                cAtt.Status__c = 'New';
                cAtt.Attachment_Type__c = 'Other';
                
                insert cAtt;
                
                Attachment sAttSign = att.clone(false, false);
                sAttSign.ParentID = cAtt.id;
                
                insert sAttSign;
            }
        }
        
        return null;
    }
    
    private static Attachment getAttachment(String attId) {
        list<Attachment> attachments = [SELECT Id, Body, ContentType, Description, Name, ParentId FROM Attachment WHERE Id =: attId];
        if(attachments.isEmpty()) {
            Attachment a = new Attachment();
            return a;
        } else {
            return attachments[0];
        }
    }
    
    protected Boolean isValidSalesforceId( String sfdcId, System.Type t ){
        try {
            if ( Pattern.compile( '[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}' ).matcher( sfdcId ).matches() ){
                Id id = sfdcId;
                sObject sObj = (sObject) t.newInstance();
                sObj.Id = id;
                return true;
            }
        } catch ( Exception e ){ 
            // StringException, TypeException 
        }
        return false;
    }
    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
    
    public void sleepThread(){
        String milisecondsStr = getParam('miliseconds');
        if(milisecondsStr != null && milisecondsStr.trim() != '' ){
            Long startTime = DateTime.now().getTime();
            Long finishTime = DateTime.now().getTime();
            Long miliseconds = Long.valueOf(milisecondsStr);
            while ((finishTime - startTime) < miliseconds) {
                finishTime = DateTime.now().getTime();
            }
        }
    }

    public class ThermalEvelopeTypeWrapper
    {
        public string type {get;set;}
        public list<Thermal_Envelope_Type__c> tEnvTypList {get;set;}
        
        public ThermalEvelopeTypeWrapper(string type, list<Thermal_Envelope_Type__c> tEnvTypList)
        {
            this.type = type;
            this.tEnvTypList = tEnvTypList;
        }
    }
    
}