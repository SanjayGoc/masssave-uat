@isTest
Public class dsmtAddEditMessageCntlrTest
{
    @isTest
    public static void runTest()
    {
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;

        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.HIC_Attached__c=true;
        update reg;
        
         Messages__c msg= Datagenerator.createMessage();
         Attachment__c att= Datagenerator.createAttachment(Tacc.ID);
         att.Message__c= msg.id;
         update att;
        
         ApexPages.currentPage().getParameters().put('id',msg.id);
         dsmtAddEditMessageCntlr controller = new dsmtAddEditMessageCntlr();
        
        controller.saveMsg();
        
        dsmtAddEditMessageCntlr.getAttachmentsByTicketId(msg.id);
        dsmtAddEditMessageCntlr.delAttachmentsById(att.id, msg.id);
        
        String temp =controller.PortalURL;
        String temp2 =controller.orgId;
        
       
        
    }
    static testMethod void case2()
    {
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = Datagenerator.CreatePortalUser();
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 )
        {
        //Create account
        Account portalAccount1 = new Account(Name = 'TestAccount',OwnerId = portalAccountOwner1.Id,Billing_Account_Number__c= 'Gas-~~~1234');
        insert portalAccount1;
        
        User user1 = portalAccountOwner1;
       
        System.runAs ( user1)
        {
         Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
         DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
         dsmt.Trade_Ally_Account__c =Tacc.id; 
         dsmt.Contact__c= user1.contactID;
         update dsmt;
            
         Messages__c msg= Datagenerator.createMessage();
         Attachment__c att= Datagenerator.createAttachment(Tacc.ID);
         att.Message__c= msg.id;
         update att;
            
         ApexPages.currentPage().getParameters().put('id',msg.id);
         dsmtAddEditMessageCntlr controller = new dsmtAddEditMessageCntlr();
            
         controller.saveMsg();
            
         dsmtAddEditMessageCntlr.getAttachmentsByTicketId(msg.id);
         dsmtAddEditMessageCntlr.delAttachmentsById(att.id, msg.id);
            
         String temp =controller.PortalURL;
        }
     }
       // controller2.gridpage();
    }
}