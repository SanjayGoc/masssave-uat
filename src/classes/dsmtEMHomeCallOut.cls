public class dsmtEMHomeCallOut {
    
    @future(callout = true)
    public static void SyncDefaultEMHome(String eAssessId,String appointmentId) {

        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;

        req.setEndpoint('http://gtesdemo.dsmtracker.com/DSMHomeEnergyWebAPI/api/GetDefaultHomeDetail');

        req.setMethod('GET');

        req.setHeader('content-type', 'application/json');
        req.setTimeOut(120000);
        http = new Http();
        
        try{
            if (!Test.isRunningTest()) {
                res = http.send(req);
                System.debug(res.getBody());
                DefaultHomeDetailMapping.DefaultHomeMapObjects(eAssessId,appointmentId, res.getBody());
            }
        }
        catch(Exception ex){
            system.debug('Exception :' + ex.getMessage());
        }
    }
}