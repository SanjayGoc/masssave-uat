public with sharing class EnergyAssessmentNavigationPageExt {

	public final Energy_Assessment__c energyAssessment{get;private set;}
    public final String billingReviewRecordTypeId{get;private set;}
    public final Integer projectsSize{get;private set;}
    public final boolean isHPCorIIC{get{
        Id currentUserProfileId = UserInfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:currentUserProfileId].Name;
        Set<String> profilesList = new Set<String>{'HPC Manager','HPC Scheduler','HPC Energy Specialist','HPC Office','IIC User'};
        return profilesList.contains(profileName);
    }}

    public EnergyAssessmentNavigationPageExt(ApexPages.StandardController stdController) {
        Energy_Assessment__c record = (Energy_Assessment__c)stdController.getRecord();
        Set<Id> projects = new Set<Id>();

        if(record != null && record.Id != null){
            Id energyAssessmentId = record.Id;
            billingReviewRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Billing Review').getRecordTypeId();
            List<Energy_Assessment__c> energyAssessments = [
                SELECT Id,Name,Start_Date__c,Status__c,
                    Appointment__c,Appointment__r.Name,
                    Appointment__r.DSMTracker_Contact__c,Appointment__r.DSMTracker_Contact__r.Name,
                    Appointment__r.Customer_Reference__c,Appointment__r.Customer_Reference__r.Name,Appointment__r.Customer_Reference__r.Service_Address__c,Appointment__r.Customer_Reference__r.Service_State__c,
                    Appointment__r.Customer_Reference__r.Service_City__c,Appointment__r.Customer_Reference__r.Service_Zipcode__c,
                    Appointment__r.Project__c,Appointment__r.Project__r.Name,
                    Workorder__c,Workorder__r.Name,Workorder__r.Unit__c,
                    (SELECT Id,Recommendation_Scenario__c FROM Recommendations__r),
                    (SELECT Id FROM Reviews__r WHERE RecordTypeId= :billingReviewRecordTypeId),
                    (SELECT Id FROM Inspection_Requests__r)
                FROM Energy_Assessment__c 
                WHERE Id = :energyAssessmentId 
                ORDER BY CreatedDate 
                ASC LIMIT 1
            ];
            if(energyAssessments.size() > 0){
                this.energyAssessment = energyAssessments.get(0);
                for(Recommendation__c rec : energyAssessment.Recommendations__r){ if(rec.Recommendation_Scenario__c != null){ projects.add(rec.Recommendation_Scenario__c); } }
            }
        }
        projectsSize = projects.size();
    }
}