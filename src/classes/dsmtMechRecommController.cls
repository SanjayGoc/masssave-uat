public class dsmtMechRecommController {
    public string  eassid {get;set;}
    public string mechanicalId {get;set;}
    public string mechanicalTypeId {get;set;}
    public string parentId {get;set;}
    public string parentField {get;set;}
    public String RecommendationId{get;set;}
    public String recordTypeName {get;set;}
    public String recordType {get;set;}
    public String mechSTypeId {get;set;}
    public String parentRecordTypeName {get;set;}
    public Recommendation__c recommendation {get;set;}
    public String addOn {get;set;}
    public Mechanical_Sub_Type__c mecSubType {get;set;}
    public list<Recommendation__c> MechRecom {get;set;}
    
    public dsmtMechRecommController() {
        recommendation = new Recommendation__c();
        recommendation.Status_Code__c = 'Recommended';
        MechRecom = new list<Recommendation__c>();
        eassId = Apexpages.currentPage().getParameters().get('assessId');
        
        MechRecom = fetchMechRecom();
    }
    
    public String getAssignDefaultValues()
    {
        system.debug('parentRecordTypeName :::::' + parentRecordTypeName);
        system.debug('recordTypeName :::::' + recordTypeName);
        
        mechSTypeId  = parentId;
        mecSubType = new Mechanical_Sub_Type__c();
        if(((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Kerosene_Space_Heater') && recordTypeName == 'Kerosene_Space_Heater')){
            mecSubType.Location__c = 'Living Space';
            mecSubType.Fuel_Type__c = 'Kerosene';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Mini_Split_A_C') && recordTypeName == 'Mini_Split_A_C')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Usage__c = 'Fully Used';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Pellet_Stove') && recordTypeName == 'Pellet_Stove')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Fuel_Type__c = 'Wood Pellet';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Evaporative_Cooler') && recordTypeName == 'Evaporative_Cooler')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.Distribution_Efficiency__c = 100;
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Gas_AC') && recordTypeName == 'Gas_AC')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Parlor_Heater') && recordTypeName == 'Parlor_Heater')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Wall_Furnace') && recordTypeName == 'Wall_Furnace')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Furnace') && recordTypeName == 'Furnace')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Boiler_Indirect_Storage_Tank') && recordTypeName == 'Boiler')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Boiler_Indirect_Storage_Tank') && recordTypeName == 'Indirect_Storage_Tank')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Collector_Type__c = 'Flat Plate';
            mecSubType.Storage_Type__c = 'Pre Heat Tank';
            mecSubType.Compromised__c = 'No';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Boiler_Tankless_Coil') && recordTypeName == 'Boiler')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
            mecSubType.Collector_Type__c = 'Flat Plate';
            mecSubType.Storage_Type__c = 'Pre Heat Tank';
            mecSubType.Compromised__c = 'No';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Wood_Stove') && recordTypeName == 'Wood_Stove')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Fuel_Type__c = 'Wood';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Storage_Tank') && recordTypeName == 'Storage_Tank')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.DHWVentType__c = 'Atmospheric';
            mecSubType.Pilot_Type__c = 'Standing Gas';
            mecSubType.Collector_Type__c = 'Flat Plate';
            mecSubType.Storage_Type__c = 'Pre Heat Tank';
            mecSubType.Compromised__c = 'No';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'On_Demand') && recordTypeName == 'On_Demand')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.DHWVentType__c = 'Sealed';
            mecSubType.Pilot_Type__c = 'Standing Gas';
            mecSubType.Collector_Type__c = 'Flat Plate';
            mecSubType.Storage_Type__c = 'Pre Heat Tank';
            mecSubType.Compromised__c = 'No';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Combo') && recordTypeName == 'Combo')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
            mecSubType.Collector_Type__c = 'Flat Plate';
            mecSubType.Storage_Type__c = 'Pre Heat Tank';
            mecSubType.Compromised__c = 'No';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Gas_A_C') && recordTypeName == 'Gas_A_C')
        {
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
            mecSubType.Location__c = 'Living Space';
            mecSubType.Usage__c = 'Fully Used';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Furnace') && recordTypeName == 'Central_A_C')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Usage__c = 'Fully Used';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Central_A_C') && (recordTypeName == 'Central_A_C' || recordTypeName == 'Gas_A_C'))
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Usage__c = 'Fully Used';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'ASHP') && recordTypeName == 'ASHP')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.Emergency_Heat_Type__c = 'Manual (Emergency Heat)';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'DFHP') && recordTypeName == 'DFHP')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.Emergency_Heat_Type__c = 'Manual (Emergency Heat)';
        }
        
        if(parentRecordTypeName == 'DFHP')
        {
            mecSubType.Heating_System__c = 'ASHP';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'DFGSHP') && recordTypeName == 'DFGSHP')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.GSHPGround_Loop_Type__c = 'Closed Loop';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'GSHP') && recordTypeName == 'GSHP')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.GSHPGround_Loop_Type__c = 'Closed Loop';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Gas_Heat_Pump') && recordTypeName == 'Gas_Heat_Pump')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Gas_Pack') && recordTypeName == 'Gas_Pack')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Usage__c = 'Fully Used';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Boiler') && recordTypeName == 'Boiler')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Steam_Boiler') && recordTypeName == 'Steam_Boiler')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Furnace_with_Central_A_C') && recordTypeName == 'Furnace')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Furnace_with_Central_A_C') && recordTypeName == 'Central_A_C')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Usage__c = 'Fully Used';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Mini_Split_Heat_Pump') && recordTypeName == 'Mini_Split_Heat_Pump')
        {
            mecSubType.Location__c = 'Living Space';
            mecSubType.Usage__c = 'Fully Used';
        }
        
        if(parentRecordTypeName == 'DFGSHP')
        {
            mecSubType.Heating_System__c = 'GSHP';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'DFGSHP' || parentRecordTypeName == 'DFHP') && recordTypeName == 'Furnace')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Natural Gas';
            mecSubType.Combustion_Type__c = 'Atmospheric';
        }
        
        if((parentRecordTypeName == 'Boiler' ||
            parentRecordTypeName == 'Boiler_Indirect_Storage_Tank' ||
            parentRecordTypeName == 'Boiler_Tankless_Coil' ||
            parentRecordTypeName == 'Custom' || 
            parentRecordTypeName == 'Furnace' ||
            parentRecordTypeName == 'Steam_Boiler') && 
           (recordTypeName == 'Baseboard Distribution System' ||
            recordTypeName == 'Radiators Distribution System' ||
            recordTypeName == 'Radiant Distribution System' ||
            recordTypeName == 'One Pipe Steam Distribution System' ||
            recordTypeName == 'Two Pipe Steam Distribution System' ||
            recordTypeName == 'Gravity Hot Water Distribution System'))
        {
            mecSubType.Baseboard_Distribution_Type__c = 'Baseboard';
            mecSubType.Piping_Location__c = 'Basement';
        }
        
        if((parentRecordTypeName == 'Central_A_C' ||
            parentRecordTypeName == 'Gas_A_C' || 
            parentRecordTypeName == 'Custom' || 
            parentRecordTypeName == 'ASHP' ||
            parentRecordTypeName == 'DFGSHP' ||
            parentRecordTypeName == 'DFHP' ||
            parentRecordTypeName == 'Boiler' ||
            parentRecordTypeName == 'Boiler_Indirect_Storage_Tank' ||
            parentRecordTypeName == 'Boiler_Tankless_Coil' ||
            parentRecordTypeName == 'Steam_Boiler' ||
            parentRecordTypeName == 'GSHP' ||
            parentRecordTypeName == 'Furnace' ||
            parentRecordTypeName == 'Gas_Pack' ||
            parentRecordTypeName == 'Gas_Heat_Pump' ||
            parentRecordTypeName == 'Evaporative_Cooler') && 
           (recordTypeName == 'Duct_Distribution_System' ||
            recordTypeName == 'High_Velocity_Duct_Distribution_System' ||
            recordTypeName == 'Hydro_Air_Distribution_System' ||
            recordTypeName == 'Ductless_Distribution_System'))
        {
            mecSubType.Leakiness__c = 'Average';
            mecSubType.Leakage_Test_Method__c = 'Not Tested';
            mecSubType.Location__c = 'Basement';
            mecSubType.Supply_Ducts_Location__c = 'Basement';
            mecSubType.Return_Ducts_Location__c = 'Basement';
            mecSubType.Supply_Ducts_Insulation_Amount__c = 'Some';
            mecSubType.Return_Ducts_Insulation_Amount__c = 'Some';
            mecSubType.Supply_Ducts_Material_Type__c = 'Rigid Metal';
            mecSubType.Return_Ducts_Material_Type__c = 'Rigid Metal';
        }
        
        if((parentRecordTypeName == 'Combo' ||
           parentRecordTypeName == 'Custom') && 
          (recordTypeName == 'Baseboard_Distribution_System' ||
           recordTypeName == 'Radiators_Distribution_System' ||
           recordTypeName == 'Radiant_Distribution_System' ||
           recordTypeName == 'Gravity_Hot_Water_Distribution_System'))
        {
            mecSubType.Baseboard_Distribution_Type__c = 'Baseboard';
            mecSubType.Piping_Location__c = 'Basement';
        }
        
        if((parentRecordTypeName == 'Custom' || parentRecordTypeName == 'Heat_Pump') && recordTypeName == 'Heat_Pump')
        {
            mecSubType.Location__c = 'Basement';
            mecSubType.Fuel_Type__c = 'Electricity';
            mecSubType.Collector_Type__c = 'Flat Plate';
            mecSubType.Storage_Type__c = 'Pre Heat Tank';
            mecSubType.Compromised__c = 'No';
        }
        
        if(recordTypeName == 'Thermostat' && 
          (parentRecordTypeName == 'Kerosene_Space_Heater' ||
           parentRecordTypeName == 'Mini_Split_A_C' ||
           parentRecordTypeName == 'Pellet_Stove' || 
           parentRecordTypeName == 'Parlor_Heater' || 
           parentRecordTypeName == 'Wall_Furnace' || 
           parentRecordTypeName == 'Wood_Stove' ||
           parentRecordTypeName == 'Custom' || 
           parentRecordTypeName == 'Combo' ||
           parentRecordTypeName == 'ASHP' ||
           parentRecordTypeName == 'DFGSHP' ||
           parentRecordTypeName == 'GSHP' ||
           parentRecordTypeName == 'DFHP' ||
           parentRecordTypeName == 'Boiler' ||
           parentRecordTypeName == 'Boiler_Indirect_Storage_Tank' ||
           parentRecordTypeName == 'Boiler_Tankless_Coil' ||
           parentRecordTypeName == 'Steam_Boiler' ||
           parentRecordTypeName == 'Furnace_with_Central_A_C' ||
           parentRecordTypeName == 'Electric_Resistance' ||
           parentRecordTypeName == 'Furnace' ||
           parentRecordTypeName == 'Gas_Pack' ||
           parentRecordTypeName == 'Gas_Heat_Pump' ||
           parentRecordTypeName == 'Mini_Split_Heat_Pump' ||
           parentRecordTypeName == 'Evaporative_Cooler'))
            mecSubType.Voltage__c = 'Low';
        
        mecSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        
        if(Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().containskey(recordType))
            recommendation.RecordTypeId = Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        
        MechRecom = fetchMechRecom();
        
        return null;
    }
    
    public list<Recommendation__c> fetchMechRecom(){
        system.debug('parentId :::::' + parentId);
        return [select id, Status__c, Operation__c, Part__c, Description__c, CreatedDate, Status_Code__c
                     from Recommendation__c
                     where Mechanical_Sub_Type__c =: parentId
                     and Mechanical_Sub_Type__c != null];
    }
    
    public void editReccom()
    {
        List<Recommendation__c> recList = [select id, Status_Code__c, Sash__c, U_Factor__c, SHGC__c, Preserve_Add_l_Props__c, Quantity__c, Operation__c, Part__c, Pricing_Effective_Date__c, Treat_From_Interior__c, Description__c,
                                           Type__c, Add_Depth__c, Remove_Exist__c, Added_Nominal_R_Value__c, Assembly_Nominal_R_Value__c, Include_in_total_R_Value__c, Total_R_Value__c
                                           from Recommendation__c where id =: RecommendationId];
        
        if(recList != null && recList.size() > 0){
            recommendation = recList[0];
            String recomId = recommendation.Id;
            list<Mechanical_Sub_Type__c> applianceList = Database.query('SELECT ' + getSObjectFields('Mechanical_Sub_Type__c') + ',RecordType.Name,RecordType.DeveloperName FROM Mechanical_Sub_Type__c WHERE Recommendation__c =:recomId');
            if(applianceList.size() > 0)
                mecSubType = applianceList[0];
        }
    }
    
    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
    
    public void saveNewReccom() {
        
        recommendation.put(parentField, parentId);
        upsert recommendation;   
        
        mecSubType.Recommendation__c = recommendation.Id;
        mecSubType.Mechanical__c = mechanicalId;
        mecSubType.Mechanical_Type__c = mechanicalTypeId;
        
        upsert mecSubType;
        
        recommendation = new Recommendation__c();
        mecSubType = new Mechanical_Sub_Type__c();
        MechRecom = fetchMechRecom();
    }
    
    public Pagereference DeleteRecommendation(){
        List<Recommendation__c> recList = [select id from Recommendation__c where id =: RecommendationId];
        
        if(recList != null && recList.size() > 0){
            delete recList;
        }
        return null;
    }
}