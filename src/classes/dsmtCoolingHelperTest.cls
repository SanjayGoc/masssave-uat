@isTest
private class dsmtCoolingHelperTest {
	
	@isTest static void test1()
    {
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = testEA.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = testEA.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Set<Id> eaId = new Set<Id>();
		eaId.add(testEA.Id);

		//system.debug('mstList[0].Energy_Assessment__c>>>'+mstList[0].Energy_Assessment__c);
		//system.debug('eaId>>>'+eaId);

		dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);

		bp.mstObj = bp.mst[0];
		bp.mechType = mechType;

		Test.stopTest();
    }

    @isTest static void test2()
    {
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = testEA.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = testEA.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Window/Wall A/C').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Window/Wall A/C').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Ductless Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();
    }

    @isTest static void test3()
    {
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = testEA.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = testEA.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Mini-Split A/C').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Mini-Split A/C').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Ductless Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();
    }

    @isTest static void test4()
    {
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = testEA.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating+Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = testEA.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('GSHP').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('GSHP').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('GSHP Cooling').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();
    }

    @isTest static void test5()
    {
        Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = testEA.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating+Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = testEA.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('ASHP').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('ASHP').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('ASHP Cooling').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();
    }
	
	
}