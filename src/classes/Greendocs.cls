public class Greendocs {
    public class GenerateGreenDocResponse_element {
        public String GenerateGreenDocResult;
        private String[] GenerateGreenDocResult_type_info = new String[]{'GenerateGreenDocResult','http://tempuri.org/',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'GenerateGreenDocResult'};
    }
    public class GenerateGreenDoc_element {
        public String sessionID;
        public String serverURL;
        public String printCollateralID;
        public String objectID;
        public String environment;
        public Boolean saveAsPDF;
        private String[] sessionID_type_info = new String[]{'sessionID','http://tempuri.org/',null,'0','1','false'};
        private String[] serverURL_type_info = new String[]{'serverURL','http://tempuri.org/',null,'0','1','false'};
        private String[] printCollateralID_type_info = new String[]{'printCollateralID','http://tempuri.org/',null,'0','1','false'};
        private String[] objectID_type_info = new String[]{'objectID','http://tempuri.org/',null,'0','1','false'};
        private String[] environment_type_info = new String[]{'environment','http://tempuri.org/',null,'0','1','false'};
        private String[] saveAsPDF_type_info = new String[]{'saveAsPDF','http://tempuri.org/',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://tempuri.org/','true','false'};
        private String[] field_order_type_info = new String[]{'sessionID','serverURL','printCollateralID','objectID','environment','saveAsPDF'};
    }
    public class TreesGreenDocWSSoap {
        public String endpoint_x = Dsmt_Salesforce_Base_Url__c.getInstance().DSMT_Greendocs_WS_Endpoint__c;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://tempuri.org/', 'Greendocs'};
        public String GenerateGreenDoc(String sessionID,String serverURL,String printCollateralID,String objectID,String environment,Boolean saveAsPDF) {
            Greendocs.GenerateGreenDoc_element request_x = new Greendocs.GenerateGreenDoc_element();
            request_x.sessionID = sessionID;
            request_x.serverURL = serverURL;
            request_x.printCollateralID = printCollateralID;
            request_x.objectID = objectID;
            request_x.environment = environment;
            request_x.saveAsPDF = saveAsPDF;
            Greendocs.GenerateGreenDocResponse_element response_x;
            Map<String, Greendocs.GenerateGreenDocResponse_element> response_map_x = new Map<String, Greendocs.GenerateGreenDocResponse_element>();
            response_map_x.put('response_x', response_x);
            if(!Test.isRunningTest()){
                WebServiceCallout.invoke(
                  this,
                  request_x,
                  response_map_x,
                  new String[]{endpoint_x,
                  'http://tempuri.org/GenerateGreenDoc',
                  'http://tempuri.org/',
                  'GenerateGreenDoc',
                  'http://tempuri.org/',
                  'GenerateGreenDocResponse',
                  'Greendocs.GenerateGreenDocResponse_element'}
                );
            }
            response_x = response_map_x.get('response_x');
            return response_x.GenerateGreenDocResult;
        }
    }
}