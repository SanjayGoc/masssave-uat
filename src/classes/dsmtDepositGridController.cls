public with sharing class dsmtDepositGridController{
    
    public List<Deposit_and_Payment__c> lstDeposit{get;set;}
    String RecId = '';
    public Recommendation_Scenario__c proj{get;set;}
    
    Public List<DepositPaymentObj> attachObjlist{get;set;}
    
    public dsmtDepositGridController(ApexPages.StandardController sc){
        
        lstDeposit = new List<Deposit_and_Payment__c>();
        
        String type = sc.getRecord().getSObjectType().getDescribe().getName();
        if(type  == 'Recommendation_Scenario__c'){
            RecId = sc.getId();    
            proj = new Recommendation_Scenario__c();
            proj = [select id,Energy_Assessment__r.Customer__c,Energy_Assessment__r.customer__r.Name,Name from Recommendation_Scenario__c where id =: RecId];   
        }
        
        lstDeposit = [select Id,Name,RecordType.Name,Deposit_Type__c,Project_Deposit_Amount__c,Date__c from Deposit_and_Payment__c where Project__c = : RecId ORDER BY CreatedDate desc limit 999]; 
        attachObjlist = new List<DepositPaymentObj>();
        for(Deposit_and_Payment__c attach : lstDeposit){
            DepositPaymentObj attachobj = new DepositPaymentObj();
            attachObj.attachId = attach.Id;
            attachobj.Name = attach.Name;
            attachObj.Type = attach.RecordType.Name;
            attachObj.Amount = String.valueOf(attach.Project_Deposit_Amount__c);
            attachObj.deposittype = string.valueOf(attach.Deposit_Type__c);
            attachObj.ddate = string.valueOf(attach.Date__c);
            attachObjlist.add(attachObj);
        }
        
    }
    
    
    
    Public class DepositPaymentObj{
        public String attachId{get;set;}
        public String Name{get;set;}
        public String Type{get;set;}
        public String Amount{get;set;}
        public String deposittype{get;set;}
        public String ddate{get;set;}
    }
}