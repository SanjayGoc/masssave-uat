public with sharing class dsmtForgotPasswordController {
     public String username {get; set;} 
     public String password {get; set;}
     public String sectionTitle{get;set;}
      public Checklist__c pgCheckList{get{return [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Trade_Ally_Forgot_Password_Form' LIMIT 1];}}
     
     public dsmtForgotPasswordController() {}
     
     public PageReference forgotPassword() {
     
        if(username == null || username.trim().length() == 0){
          ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Enter Username.!');
          ApexPages.addMessage(msg3);
          return null;
        }
        
        Pattern p = Pattern.compile( '([a-zA-Z0-9+_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))');
        Matcher m = p.matcher(username);
        if (!m.matches()) {
            ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter Username in the Email format');
            ApexPages.addMessage(msg3);
            return null;
         }
         
          Integer  count = [select count() from User where username =: username];
            if(count == 0){
                ApexPages.Message msg3 = new ApexPages.Message(ApexPages.Severity.ERROR, 'User not found');
                ApexPages.addMessage(msg3);
                return null; 
            }
            Online_Portal_URLs_Hierarchy__c credentials = Online_Portal_URLs_Hierarchy__c.getInstance();
            String reLoginPageUrl   = '/apex/relogin3?u='+EncodingUtil.urlEncode(username,'UTF-8'); 
            PageReference reLoginPageRef = Site.login(credentials.system_adminu__c, credentials.system_adminp__c, reLoginPageUrl);
            
            if(reLoginPageRef == null){
                return null;
            } 
            return reLoginPageRef;
        }
        
        public list<Checklist_Items__c> getChecklists(){
            list<Checklist_Items__c> checklistItems = [
                SELECT id, Action_Required__c, Checklist_Information__c, Checklist__c,Parent_Checklist__c, Section__c, Sequence__c
                FROM Checklist_Items__c
                WHERE Parent_Checklist__r.Unique_Name__c = 'Trade_Ally_Forgot_Password_Form' and Special_instruction_for_public_portal__c = true 
            ];
            
            if(checklistItems.size() > 0){
                this.sectionTitle = checklistItems.get(0).Section__c;
                return checklistItems;
            }
            
            return new list<Checklist_Items__c>();
       }
    
       public PageReference forgotPassword2() {
        String username = ApexPages.currentPage().getParameters().get('u');
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.dsmtForgotPasswordConfirmOAP;
        
        pr.setRedirect(true);
        return success?pr:null ;
       
       } 
}