global class dsmtPreWorkUpdate
{
    webservice static void updateProject(String projectIds) 
    { 
        Id prjRecordTypeId = Schema.SObjectType.Recommendation_Scenario__c.getRecordTypeInfosByName().get('Locked For Review').getRecordTypeId();
        Recommendation_Scenario__c objProj = new Recommendation_Scenario__c();
        objProj.Id = projectIds;
        objProj.Submitted_For_Pre_Work_Approval_Date__c = Date.today(); 
        objProj.RecordTypeId = prjRecordTypeId ;
        
        update objProj;
        
        List<Recommendation_Scenario__c> projectlist = [select id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                                            Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c,
                                                            Energy_Assessment__r.Customer__r.First_Name__c,Energy_Assessment__r.Customer__r.Last_Name__c,
                                                            Energy_Assessment__r.Customer__c
                                                            ,Address__c,City__c,State__c,Zip__c,Phone__c,Email__c,Is_Heat_Loan__c  from Recommendation_Scenario__c where id =: projectIds];
        
        if(projectlist.size() > 0){
           Recommendation_Scenario__c project = projectlist.get(0);
           
            Id preRevRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Pre Work Review').getRecordTypeId();
            Review__c rev = new Review__c();
            rev.Project__c = projectIds;
            rev.Type__c = 'Pre Work Review';
            rev.RecordTypeId = preRevRecordTypeId ;
            rev.Status__c = 'Pending Review';
            rev.Trade_ally_account__c = project.Energy_assessment__r.Trade_ally_account__c;
            rev.Dsmtracker_contact__c = project.Energy_Assessment__r.Energy_Advisor__r.Dsmtracker_contact__c;
            rev.Address__c = project.Address__c;
            rev.City__c = project.City__c ;
            rev.Zipcode__c = project.Zip__c;
            rev.State__c = project.State__c;
            rev.Phone__c = project.Phone__c;
            rev.Email__c = project.Email__c;
            rev.First_Name__c = project.Energy_Assessment__r.Customer__r.First_Name__c;
            rev.Last_Name__c = project.Energy_Assessment__r.Customer__r.Last_Name__c;
            rev.Energy_Assessment__c = project.Energy_Assessment__c;
            rev.Customer__c = project.Energy_Assessment__r.Customer__c;
            rev.Heat_Loan__c = project.Is_Heat_Loan__c;
            insert rev;
            
            List<Project_Review__c> prList = new List<Project_Review__c>();
            Project_Review__c pr = null;
            
            pr = new Project_Review__c();
            pr.Project__c = project.Id;
            pr.Review__c = rev.Id;
            prList.add(pr);
            
            
            if(prList.size() > 0){
                insert prList;
            }
        }
        
        
        
    }
}