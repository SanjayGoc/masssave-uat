public class jqxGridController{
    public void jqxGridController(){
    }

    @remoteAction
    public static AccountModel refreshData(){
        AccountModel model = new AccountModel();

        model.accounts = [Select id,Name,Type from Account limit 200];
        model.contacts = [Select id,Title,FirstName,LastName,AccountId from Contact where AccountId!=null limit 200];
        model.typeOptions = getAccountTypes();

        return model;
    }
    public class AccountModel{
        public List<Account> accounts;
        public List<Contact> contacts;
        public List<String> typeOptions;
    }

    public static List<String> getAccountTypes(){
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = Account.Type.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple){
            options.add(f.getValue());
        }
        return options;
    }
}