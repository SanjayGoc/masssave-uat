@isTest
Public class dsmtAddEditTicketCntlrTest
{
    @isTest
    public static void runTest()
    {
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();

        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;

        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c =Tacc.ID;
        reg.HIC_Attached__c=true;
        update reg;

        Messages__c msg= Datagenerator.createMessage();
        UCS_AgilePMO__Ticket__c tk= Datagenerator.createUCSAgilePMOTk();
        Attachment__c att= Datagenerator.createAttachment(Tacc.ID);
        att.Message__c= msg.id;
        update att;
        
        ApexPages.currentPage().getParameters().put('id',tk.id);
        
        dsmtAddEditTicketCntlr controller = new dsmtAddEditTicketCntlr();
 
        controller.saveTicket();
        controller.saveAndSubmit();
        controller.getTicketTypes();
        
        dsmtAddEditTicketCntlr.getAttachmentsByTicketId(msg.id);
        dsmtAddEditTicketCntlr.delAttachmentsById(att.id, msg.id);
        
        String temp =controller.PortalURL;
        String temp2 =controller.orgId; 
    }
    static testMethod void case2()
    {
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = System.now().millisecond() + 'tst222@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) 
        {
    
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id,
        Billing_Account_Number__c= 'Gas-~~~1234'
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'tst2225@test.com'
        );
        insert contact1;
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile Limit 1];
        User user1 = new User(
        Username = System.now().millisecond() + 'tst2225@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
       
        System.runAs (user1) 
        {
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
         DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.Id; 
         dsmt.Contact__c= user1.contactID;
         update dsmt;
         Messages__c msg= Datagenerator.createMessage();
         Attachment__c att= Datagenerator.createAttachment(Tacc.Id); 
         att.Message__c= msg.id;
         update att;
         ApexPages.currentPage().getParameters().put('id',msg.id);
        dsmtAddEditTicketCntlr controller = new dsmtAddEditTicketCntlr();
        //dsmtAddEditTicketCntlr.SkillsWrapper wrap= new dsmtAddEditTicketCntlr.SkillsWrapper();
        controller.saveTicket();
        dsmtAddEditTicketCntlr.getAttachmentsByTicketId(msg.id);
        dsmtAddEditTicketCntlr.delAttachmentsById(att.id, msg.id);
        String temp =controller.PortalURL;
        }
        }
       // controller2.gridpage();
    }
}