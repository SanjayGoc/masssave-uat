public with sharing class dsmtExceptionsController{
    
    public List<Exception__c>lstRRequestException{get;set;}
    public Integer counter{get;set;}
    public User usr{get;set;}
    public Boolean isShow{get;set;}
    public Boolean isEnable{get;set;}
    public Boolean enableExceptions{get;set;}  
         
    public Boolean HideException{get;set;}
    public Boolean AddException{get;set;}
    public string savedString{get;set;}
    public string expTempId{get;set;}
    public string popexpmsg{get;set;}
    public string newinsertedExpId {get;set;}
    public List<String> newExpId {get;set;}
    public list<String>lstDepositionMessage {get;set;}
    public string selectedStandardExcpetion{get;set;}
    public list<SelectOption> lstStandardExceptionOptions{get;set;}
    public list<SelectOption> lstDepositionOptions{get;set;}
    public Map<Id,Exception__c> lstOverrideException = new Map<Id,Exception__c>();
    public List<Exception__c> lstaddException{get;set;}
    public String[] despositions = new String[]{};
    public List<Exception_Template__c> lstaddException2{get;set;}
    List<string>  existExceptions = new List<string>();
    public boolean ss{get;set;}
    
    public string expId{get;set;}
    public string vlu {get;set;}
    public string type {get;set;}
    
    String RRId = '';
    public Registration_Request__c RegRequest{get;set;}
    set<string>insertedExceptionsIds = new set<string>();
    
    private map<string, string> prgm_fldr_map = new map<string, string>();
    
    public dsmtExceptionsController(ApexPages.StandardController sc){
        enableExceptions= false;
        lstaddException2  = new List<Exception_Template__c>();
        RegRequest = new Registration_Request__c();
        HideException=false;
        AddException=false;
        isShow=true;
        newinsertedExpId = '';
        String type = sc.getRecord().getSObjectType().getDescribe().getName();
        if(type  == 'Registration_Request__c'){
            RRId = sc.getId();
            fetchRegistrationRequest(RRId);
        }
        
        getAllExistingExceptions();
        getfetchDepositionOptions(); 
        getAllNonExistingExceptions();
        
        ss = false;
        if(lstRRequestException.size()>0){
            isShow = true;
        }else{
            isShow = false;
        }
        counter  = 0;
        savedString = '';
        usr = [select id,name from User where id=:UserInfo.getUserId() ];
        
        falseOverrideExceptions();
        
       
    }
    
    public void saveExp(){
        if(expId != null){            
            for(Exception__c exp : lstRRequestException){
                if(exp.Id == expId){
                    if(type == 'disposition'){
                        exp.Disposition__c = vlu;
                    }else if(type == 'Override'){
                        if(vlu == 'true' && exp.Override_Exception__c == false){
                            exp.Override_Exception__c = true;
                            exp.Overridden_By__c = UserInfo.getUserId();
                            exp.Overridden_Date__c = System.now();
                        }else{
                            exp.Override_Exception__c = false;
                        }
                    } 
                    update exp;
                    break;
                }
            }  
        }         
    }    
    
    
    private void fetchRegistrationRequest(String RRId){
        List<Registration_Request__c> lstRR = [select id, Name, Status__c from Registration_Request__c where id=:RRId];
        if(lstRR.size()>0 ){
            RegRequest = lstRR[0];
            if (lstRR[0].Status__c == 'Approved') {
                enableExceptions= false;            
            }else  {
               enableExceptions= true;            
            }
        }
    }
    
    public void getAllExistingExceptions(){
     
        if(HideException == false)
            lstRRequestException = [select id,CreatedDate,Exception_Message__c,Type__c,Override_Exception__c,Outbound_Message__c,CreatedById,CreatedBy.Name,CreatedBy.FirstName,CreatedBy.LastName, Status__c,Overridden_By__r.Name,Overridden_Date__c
                                  ,Overridden_By__c,Disposition__c,OverrideByName__c,Exception_Template__r.Program_Type_MS__c,CreatedDate__c,Overridden_By__r.FirstName,Overridden_By__r.LastName,Application_Section__c 
                                  From Exception__c WHERE Registration_Request__c =:RRId AND Disposition__c  != 'Resolved' 
                                  limit 1000]; //AND Exception_Template__r.GTES__Program_Type_MS__c =: enrollApp.Program__r.Name
        
        if(HideException == true)
            lstRRequestException = [select id,CreatedDate,Exception_Message__c,Type__c,Override_Exception__c,Outbound_Message__c,CreatedById,CreatedBy.Name,CreatedBy.FirstName,CreatedBy.LastName, Status__c,Overridden_By__r.Name,Overridden_Date__c
                                  ,Overridden_By__c,Disposition__c,OverrideByName__c,Exception_Template__r.Program_Type_MS__c,CreatedDate__c,Overridden_By__r.FirstName,Overridden_By__r.LastName,Application_Section__c 
                                  From Exception__c WHERE Registration_Request__c =:RRId and Override_Exception__c = false AND Disposition__c  != 'Resolved' 
                                  limit 1000]; //AND Exception_Template__r.Program_Type_MS__c =: enrollApp.Program__r.Name
            
       
        for(Exception__c  exp :lstRRequestException  ){
             if(exp.Outbound_Message__c != null )
                 exp.Outbound_Message__c = exp.Outbound_Message__c.replaceAll('\'','<>').replaceAll('\r\n','<br>').replaceAll('\n','<br>');
             if(exp.Exception_Message__c != null )
                 exp.Exception_Message__c = exp.Exception_Message__c.replaceAll('\'','<>').replaceAll('\r\n','<br>').replaceAll('\n','<br>');
            
        }
    }
    
    public List<String> getTypeOptions(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Exception__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add('--NONE-');
        for(Schema.PicklistEntry f : ple){
           options.add(f.getValue());
        }       
        return options;
    } 
    
    public List<String> getStatusOptions(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Exception__c.Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add('--NONE-');
        for(Schema.PicklistEntry f : ple){
           options.add(f.getValue());
        }       
        return options;
    } 
    
    public void save(){
        if(savedString != null && savedString.trim().length()>0){
            
            List<String>lstCols = savedString.split('column') ;
           
            if(lstCols.size()==5){
                Exception__c exp = new Exception__c();
                insertedExceptionsIds.add(lstCols[0]);
                if(lstCols[0].length()>=15){
                    exp.id = lstCols [0];
                }else{
                    exp.Type__c = 'Manual';
                    exp.Registration_Request__c = RRId;
                }
               
                exp.Exception_Message__c = lstCols [1];
                exp.Disposition__c= lstCols [2];
                
                if(lstCols [3] == '1')
                    exp.Override_Exception__c= true;
                else
                    exp.Override_Exception__c= false;
                    
                exp.Outbound_Message__c= lstCols [4];
                if(exp.Outbound_Message__c != null )
                 exp.Outbound_Message__c = exp.Outbound_Message__c.replaceAll('<br>','\n');
                
                exp.Final_Outbound_Message__c = exp.Outbound_Message__c;
                upsert exp; 
                newinsertedExpId = exp.id;
            }
           
        }
    }
    
    public void overrideAllExceptions(){
      
     
     
      for(Exception__c exp : [Select id,Override_Exception__c,Overridden_By__c,Overridden_Date__c from Exception__c where Registration_Request__c =: apexpages.currentpage().getparameters().get('id') and Override_Exception__c = false]){
         
        exp.Override_Exception__c = true;
        exp.Overridden_By__c = UserInfo.getUserId();
        exp.Overridden_Date__c = System.now();
        lstOverrideException.put(exp.id,exp);   
      }
      if(lstOverrideException.size() > 0)
      {
        update lstOverrideException.values();
        isEnable = True;
      }
      
       
    }
    
     public List<String> getDespostionOptions(){
        List<String> options = new List<String>();
        options.add('--NONE-');
        
        String appType = '';
        options.add('Message 1');
        options.add('Message 2');
             
        return options;
    } 
    
    public void falseOverrideExceptions(){
      
      
      for(Exception__c exp : [Select id,Override_Exception__c,Overridden_By__c,Overridden_Date__c from Exception__c where Registration_Request__c =: apexpages.currentpage().getparameters().get('id') and Override_Exception__c = false ]){
         
           lstOverrideException.put(exp.id,exp);
             
      }
      if(lstOverrideException.size() > 0){
        isEnable = false;
               
      }
      else{
        isEnable = true;
        
      }
      
      } 
    
  
    
    public List<User> getUserOptions(){
        
        return [select id,name from user where IsActive = true];
    }    
    
    
    public void getAllNonExistingExceptions(){
        ss = true;
        existExceptions = new List<String>();
        
            List<Registration_Request__c> lstRR = [select id from Registration_Request__c where id=:apexpages.currentpage().getparameters().get('id')];
            if(lstRR.size()>0){
                for(Exception__c exp : [Select Exception_Template__c from Exception__c where Is_Open__c = 1 and Registration_Request__c =: apexpages.currentpage().getparameters().get('id')]){
                   if(exp.Exception_Template__c != null)
                       existExceptions.add(exp.Exception_Template__c);
                }
                system.debug('--existExceptions--'+existExceptions);
                lstaddException2 = [select id,Exception_Message__c,Outbound_Message__c,Program_Type_MS__c,Application_Section__c
                                    from Exception_Template__c where id NOT IN : existExceptions and Registration_Request_Level_Message__c = true and Active__c = true and Manual__c = true and Type__c = 'Exception Message']; 
                
                system.debug('#### lstaddException2 :::::'+ lstaddException2);
            }
            
        for(Exception_Template__c exp : lstaddException2 ){
             if(exp.Outbound_Message__c != null )
                 exp.Outbound_Message__c = exp.Outbound_Message__c.replaceAll('\'','<>').replaceAll('\r\n','<br>').replaceAll('\n','<br>');
             if(exp.Exception_Message__c != null )
                 exp.Exception_Message__c = exp.Exception_Message__c.replaceAll('\'','<>').replaceAll('\r\n','<br>').replaceAll('\n','<br>');
        }                     
    }
    
    
    
    public void saveExceptionPopup(){
        ss = true;
        isEnable = false;
        if(expTempId != '' && expTempId != null){
            List<String> ExpTempIDs = new List<String>();
            List<Exception__c> NewExp = new List<Exception__c>();
            ExpTempIDs.addALL(expTempId.split(','));
            
            lstaddException2 = [select id,Exception_Message__c,Outbound_Message__c,Program_Type_MS__c,Application_Section__c
                                  From Exception_Template__c where id IN : ExpTempIDs];
                                     
            for(Exception_Template__c expt :   lstaddException2){
            
                Exception__c exp = new Exception__c();
                exp.Exception_Message__c = (expt.Exception_Message__c != null && expt.Exception_Message__c.length() > 32000) ? expt.Exception_Message__c.substring(0, 32000) : expt.Exception_Message__c;
                exp.Outbound_Message__c = expt.Outbound_Message__c;
                
                exp.Registration_Request__c = apexpages.currentpage().getparameters().get('id');
                exp.Exception_Template__c = expt.id;
                NewExp.add(exp);
            }
            if(NewExp.size() > 0)
            { 
                Insert NewExp;
            }
        }
        
      
            
        if(popexpmsg != '' && popexpmsg != null)
        {
            Exception__c exp = new Exception__c();
            exp.Exception_Message__c = popexpmsg;
            exp.Outbound_Message__c = popexpmsg;
            exp.Final_Outbound_Message__c = popexpmsg;
            exp.Registration_Request__c = apexpages.currentpage().getparameters().get('id');
            if(exp.Exception_Message__c != '' &&  exp.Outbound_Message__c != '')
            {
                Insert exp;
            }
        }                           
    }
    
       public void getfetchDepositionOptions(){
        lstDepositionOptions = new list<SelectOption>();
        lstDepositionMessage= new list<String>();   
        List<Custom_Message__c> lstCustomMessages = new List<Custom_Message__c>();
        
            List<Registration_Request__c > lstEaRecords = [select id from Registration_Request__c where id=:apexpages.currentpage().getparameters().get('id')];
            
            if(lstEaRecords.size()>0){
                lstCustomMessages = [select id,Custom_Message__c FROM Custom_Message__c where Display_For_Registration_Request__c =: true And Type__c = 'Disposition Message'];
            }
        
        system.debug('lstCustomMessages :::::'+ lstCustomMessages);
        lstDepositionOptions.add(new SelectOption('','--Select--'));
        for(Custom_Message__c cus : lstCustomMessages ){
           lstDepositionOptions.add(new SelectOption(cus.Custom_Message__c,cus.Custom_Message__c));
           lstDepositionMessage.add(cus.Custom_Message__c);
        }       
        
     }
     
    
  }