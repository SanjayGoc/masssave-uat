@isTest
Public class dsmtFutureTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Customer__c custObj = Datagenerator.createCustomer(acc.Id,null);
        custObj.Service_Street_Number__c = '40';
        custObj.Service_Street__c = 'PARKER';
        custObj.Service_Street_Suffix__c = 'ST';
        custObj.Service_Street_Misc__c = '9';
        insert custObj;
        
        dsmtFuture.CreatePremise(new Set<Id>{custObj.Id});
        
        dsmtFuture.getCreatableFieldsSOQL('Account','','');
    }
}