global class dsmtInspectionLineItemGridCntrl{
    Public String irId{get;set;}
    public Inspection_Line_item__c ili{get;set;}
    public String oldILIIds{get;set;}
    public String deleteItemId{get;set;}
    public List<Inspection_Line_Item__c> iliList{get;set;}

    
    public  dsmtInspectionLineItemGridCntrl(){
        irId = ApexPages.currentPage().getParameters().get('irId');
        system.debug('=--irID--'+irId);
        InspectionLinrItem();
        setOldILIIds();
    }
    
    public dsmtInspectionLineItemGridCntrl(ApexPages.StandardController sc){
        //irId = ApexPages.currentPage().getParameters().get('irId');
        //system.debug('=--irID--'+irId);
        
        String type = sc.getRecord().getSObjectType().getDescribe().getName();
        if(type  == 'Inspection_Request__c'){
            irId = sc.getId();
        }
        
        InspectionLinrItem();
        setOldILIIds();
    }
    
    global Void InspectionLinrItem(){
        iliList = [Select id,name,Inspection_Request__c,Description__c,Spec_d_Quantity__c,Change_Order_Quantity__c,Installed_Quantity__c,
                   Inspected_Quantity__c,Change_Order_Reason__c,Status__c,Quick_Notes__c,Replaced_Part__r.name,Replaced_Part__r.Description__c 
                   from Inspection_line_item__c where Inspection_Request__c =: irId order by replaced_part__c];
        if(iliList.size() > 0){
           ili = iliList.get(0);
        }           
        
    }
    
    global Void SaveInspectionLineItem(){
        system.debug('iliList : ' + iliList);
        if(iliList!=null && iliList.size()>0)
        {
            update iliList;
        }     
    }
    
    public void setOldILIIds() {
        oldILIIds = '';
        for(Inspection_line_item__c il : iliList) {
            oldILIIds += il.Id + '::';
        }
    }
    
    global Void reloadInspectionLineItem(){
        setOldILIIds();
        InspectionLinrItem();
    }
    
    public void deleteInspectionLineItem() {
        if(deleteItemId != null) {
            Inspection_Line_Item__c insLineItem = [Select Recommendation__c  From Inspection_Line_Item__c Where id =: deleteItemId ];
            
            if(insLineItem.Recommendation__c != null) {
                Delete [Select Id From Recommendation__c Where Id= :insLineItem.Recommendation__c ];
            }
            
            delete insLineItem;
            InspectionLinrItem();
            deleteItemId = null;
        }
    }
}