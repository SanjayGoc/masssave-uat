@IsTest
public class SR_AddNote_CtrlExtensionTest {

    testmethod static void testrun() {
    
        User u = Datagenerator.CreatePortalUser();
        
        Trade_Ally_Account__c ac = new Trade_Ally_Account__c();
        ac.Name='Subhadip Test';
        insert ac;
        
        DSMTracker_Contact__c c = new DSMTracker_Contact__c();
        c.Contact__c = u.ContactId;
        c.Trade_Ally_Account__c = ac.id;
        insert c;
        
        Id rcTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
        
        Service_Request__c sr = new Service_Request__c();
        sr.DSMTracker_Contact__c = c.Id;
        sr.RecordTypeId = rcTypeId;
        insert sr;
        
        System.runAs(u) {
            Service_Request__c a = SR_AddNote_CtrlExtension.addNote(sr.Id, 'test note by Subhadip');
            Service_Request__c b = SR_AddNote_CtrlExtension.getServiceRequestById(sr.Id);
            System.debug(a);
            System.debug(b);
        }
    }
}