Public Class dsmtIRsubmitForApprovalcntrl{
    Public String irId{get;set;}
    public List<Inspection_Line_Item__c> iliList{get;set;}
    public Review__c rId{get;set;}
    
    Public dsmtIRsubmitForApprovalcntrl(){
        irId = ApexPages.currentPage().getParameters().get('irId');
        system.debug('--irId--'+irId);
        submitForApprovalIR();
    }
    
    public Void submitForApprovalIR(){
        iliList = [Select id,name,Inspection_Request__c,Description__c,Spec_d_Quantity__c,Change_Order_Quantity__c,Installed_Quantity__c,
                   Inspected_Quantity__c,Change_Order_Reason__c,Status__c,Quick_Notes__c,Replaced_Part__r.name,
                   Recommendation__c,Recommendation__r.Recommendation_Scenario__c,Recommendation__r.Recommendation_Scenario__r.name,
                   Inspection_Request__r.Review__c from Inspection_line_item__c where Inspection_Request__c =: irId];
    }
    
    public PageReference CreateCOReview(){
        List<RecordType> reviewRecordType = [Select id, name, DeveloperName From RecordType where SobjectType='Review__c' and DeveloperName = 'Change_Order_Review'];
        if(reviewRecordType.Size() > 0){
            RecordType rt= reviewRecordType.get(0);
            dsmtRecommendationHelper.StopRecTrigger = true;
            List<Inspection_request__c> irList = [Select id,name,Customer__c,Review__c,Review__r.First_Name__c,Review__r.Last_Name__c,Review__r.Address__c,
                                                  Review__r.City__c,Review__r.State__c,Review__r.Zipcode__c,Review__r.Phone__c,Review__r.Email__c,
                                                  Contractor_Name__c,Inspector_Name__c From Inspection_Request__c where Id =: irid];
            
            if(irList.Size() > 0){
                Inspection_request__C ir = irList.get(0);
                
                Review__c CoReview = new Review__c();
                CoReview.RecordTypeId = rt.id;
                CoReview.First_name__c = ir.Review__r.First_Name__c;
                CoReview.Last_Name__c = ir.Review__r.Last_Name__c;
                CoReview.Address__c = ir.Review__r.Address__c;
                CoReview.City__c = ir.Review__r.City__c;
                CoReview.State__c = ir.Review__r.State__c;
                CoReview.Zipcode__c = ir.Review__r.Zipcode__c;
                CoReview.Phone__c = ir.Review__r.Phone__c;
                CoReview.Email__c = ir.Review__r.Email__c;
                CoReview.Trade_Ally_Account__c = ir.Contractor_Name__c;
                CoReview.Dsmtracker_contact__c = ir.Inspector_Name__c;
                CoReview.Status__c = 'Approved';
                CoReview.Type__c = 'Change Order Review';
                CoReview.Billing_Review__c = ir.Review__c;
                CoReview.Inspection_Request__c = ir.id;
                
                rId = CoReview; 
                if(CoReview != null){
                    insert CoReview;
                }
                
                ir.Status__c = 'Ready for Review';
                update ir;
                
                List<Change_Order_Line_Item__c> COLlist = new List<Change_Order_Line_Item__c>();                
                if(iliList.Size() > 0){
                    for(Inspection_line_item__c ili : iliList){
                    
                        Change_Order_Line_Item__c col = new Change_Order_Line_Item__c();
                    
                        col.Project__c = ili.Recommendation__r.Recommendation_Scenario__c;
                        col.Recommendation__c = ili.Recommendation__c;
                        //col.Recommendation_Description__c = ili.Description__c;
                        col.Original_Quantity__c = ili.Installed_Quantity__c;
                        col.Change_Order_Quantity__c = ili.Inspected_Quantity__c;
                        if(ili.Quick_Notes__c == null){
                            col.Change_Order_Reason__c = 'Adjustment';                            
                        }else{
                            col.Change_Order_Reason__c = ili.Quick_Notes__c;                            
                        }
                        col.Billing_Review__c = ili.Inspection_Request__r.Review__c; 
                        col.Change_Order_Review__c = CoReview.id;
                        col.Inspection_Line_Item__c = ili.Id;
                        col.Status__c = 'Approved';
                        COLlist.add(col);    
                    }
                    if(COLlist.Size() > 0){
                        //RecommendationHelper.StopRecTrigger = true;
                        dsmtRecommendationHelper.disableRolluptriggeronRec = true;
                        dsmtRecommendationHelper.StopRecTrigger = true;
                        insert COLlist;
                    }
                    
                }                
             
            }
           /* Set<String> reviewid = new Set<String>();
            List<Workorder__c> woList = new List<Workorder__c>();
            if(irList.size() > 0){
               Inspection_request__c insReq =  irList.get(0);
               
               if(insReq.Review__c != null){
                   reviewid.add(insReq.Review__c);
               }
              
               if(reviewid.size() > 0){
                  woList = [Select id,name,Review__c,status__c from Workorder__c where Review__c =: reviewid and status__c not in ('Completed','Cancelled')];
                    if(woList.size() > 0){
                       for(Workorder__c wo : woList) {
                          wo.Status__c = 'Completed';
                       }
                    }
                  }
                  if(woList.size() > 0){
                       update woList;     
                  }
               }*/
            }
        
        return new PageReference('/'+rid.Id);
    }
    
}