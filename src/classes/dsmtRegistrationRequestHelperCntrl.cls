public class dsmtRegistrationRequestHelperCntrl {
    private Registration_Request__c regRequest{get;set;}
    
    private User AccountManager{get;set;}
    
    public dsmtRegistrationRequestHelperCntrl(Registration_Request__c regRequest){
        this.regRequest = regRequest;
    }
    
    public void sendEmailProcess(){
        List<User> ulist = [SELECT Id,Name,Email FROM User WHERE Id =: regRequest.Account_Manager__c];
       
        if(ulist.size() > 0){
            AccountManager = ulist.get(0);
        }
        
        if(regRequest.Status__c == 'Submitted'){
             this.sendEmailForSubmitted();
        }else if(regRequest.Status__c == 'Approved'){
            this.sendEmailForApproved();
        }else if(regRequest.Status__c == 'Rejected'){
            this.sendEmailForRejected();
        }
    }
    

    public static void sendEmail(Set<Id> regRequestIds){
        List<Registration_Request__c> regRequests = [SELECT Id,Name,Status__c,OwnerId,Account_Manager__c,Trade_Ally_Account__c,Dsmtracker_contact__c,Email__c,Dsmtracker_contact__r.Contact__c FROM Registration_Request__c WHERE Id IN :regRequestIds];
        for(Registration_Request__c regRequest : regRequests){
            dsmtRegistrationRequestHelperCntrl cntrl = new dsmtRegistrationRequestHelperCntrl(regRequest);
            cntrl.sendEmailProcess();   
        }
    }
    
    private void sendEmail(Id TargetObjectId,String whatid, String whatidType ,List<String> toAddressList,List<String> ccAddressList, List<String> bccAddressList,String templateUniqueName,OrgWideEmailAddress owe,String fromEmailAddress,String msgdirection){
        
        List<EmailTemplate> emailTemplatelist = [SELECT Id FROM EmailTemplate WHERE DeveloperName =:templateUniqueName LIMIT 1];
        EmailTemplate emailTemplate = null;
        if(emailTemplatelist.size() > 0){
            emailTemplate = emailTemplatelist.get(0);
        }
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        List<Messaging.SingleEmailMessage> emaillist = new List<Messaging.SingleEmailMessage>();
        
        if(TargetObjectId != null){
            email.setTargetObjectId(TargetObjectId);
        }
        if(emailTemplate != null){
           email.setTemplateId(emailTemplate.Id);
        }    
        if (toAddressList != null && toAddressList.size() > 0)
            email.setToAddresses(toAddressList);
        if (ccAddressList!= null && ccAddressList.size() > 0)
            email.setCcAddresses(ccAddressList);
        if (bccAddressList!= null && bccAddressList.size() > 0)
            email.setBCcAddresses(bccAddressList);
       
        email.setSaveAsActivity(false);
        email.setWhatId(whatid);
        
        if(owe != null){
           email.setOrgWideEmailAddressId(owe.id);
        }
        
        email.setSenderDisplayName(fromEmailAddress);
        email.setTreatTargetObjectAsRecipient(false);
        
        emaillist.add(email);
        
        if(!Test.isrunningTest()){
           List<Messaging.SendEmailResult> sendEmailResults = Messaging.sendEmail(emaillist);
        }
        
        
        List<Task> tasklist = new List<Task>();
        List<Messages__c> msglist = new List<Messages__c>();
        
        for(Messaging.SingleEmailMessage mail : emaillist){
             system.debug('--mail--'+mail);
             
             tasklist.add(this.createTask(mail.getSubject(),email.getHtmlBody(),whatid));
             
             String toaddress = (mail.toAddresses != null)?String.join(mail.toAddresses,','):'';
             msglist.add(this.createMessage(mail.getSubject(),fromEmailAddress,toaddress ,mail.getHtmlBody(),whatid, whatidtype,msgdirection));
        }
        
        if(tasklist.size() > 0){
           insert tasklist;
        }
        
        if(msglist.size() > 0){
           insert msglist;
        }
    }
    
    private void sendEmailForSubmitted(){
           
            List<String> toaddressList = new List<String>();
            
            if(AccountManager != null){  // Owner is User
               toaddressList.add(AccountManager.Email);
            }
            
            String targetObjId = regRequest.Dsmtracker_contact__r.Contact__c;
            
            list < string > ccAddressList = new list < string > ();
            list < string > bccAddressList = new list < string > ();
            
            String fromaddress = Userinfo.getUserEmail();
            
            String submittedTempalateName = Email_Custom_Setting__c.getOrgDefaults().RR_Submitted_Owner_Email_Template__c;
            
            // Send to Owner for notification
            this.sendEmail(targetObjId,regRequest.Id, 'Registration_Request__c' ,toaddressList,ccAddressList ,bccAddressList ,submittedTempalateName,null, fromaddress,'Inbound'); 
            
            String thankyouSubmittedTemplateName = Email_Custom_Setting__c.getOrgDefaults().RR_Submitted_TA_ThankYou_Email_Template__c;
            toaddressList = new List<String>();
            toaddressList.add(regRequest.Email__c);
            
            if(AccountManager != null){
               fromaddress = AccountManager.Email;
            }else{
               fromaddress = '';
            }
            
            // Send to TA for Thank you email
            this.sendEmail(targetObjId,regRequest.Id, 'Registration_Request__c' ,toaddressList,ccAddressList ,bccAddressList ,thankyouSubmittedTemplateName ,null, fromaddress,'Inbound'); 
    }
    
    private void sendEmailForApproved(){
            
            List<String> toaddressList = new List<String>();
            list < string > ccAddressList = new list < string > ();
            list < string > bccAddressList = new list < string > ();
            
            toaddressList.add(regRequest.Email__c);
            
            String fromaddress = null;
            
            if(AccountManager != null){
               fromaddress = AccountManager.Email;
            }else{
               fromaddress = '';
            }
            
            String targetObjId = regRequest.Dsmtracker_contact__r.Contact__c;
            
            String approvedToTaTemplate = Email_Custom_Setting__c.getOrgDefaults().RR_Approved_TA_Email_Template__c;
            // Send notification to TA for approved RR
            this.sendEmail(targetObjId, regRequest.id, 'Registration_Request__c' ,toaddressList,ccAddressList ,bccAddressList ,approvedToTaTemplate ,null, fromaddress,'Outbound'); 
     
            // Send registration mail to all approved dsmt contact under registration request     
            String dsmtTemplate = Email_Custom_Setting__c.getOrgDefaults().RR_Approved_DSMTContact_Email_Template__c;
              
            List<Dsmtracker_Contact__c> dsmtlist = [select id,Status__c,Title__c,Email__c,Contact__c from Dsmtracker_Contact__c where Registration_Request__c =: regRequest.id and Status__c = 'Approved' and is_Primary__c = false and Portal_Role__c != null and Portal_Role__c != ''];
            
            for(Dsmtracker_Contact__c dsmt : dsmtlist){
                system.debug('--dsmt--'+dsmt);
                toaddressList = new List<String>();
                toaddressList.add(dsmt.Email__c);
                
                // Send notification to DSMT Contact for approved RR    
                this.sendEmail(dsmt.Contact__c, dsmt.id, 'Dsmtracker_Contact__c' ,toaddressList,ccAddressList ,bccAddressList ,dsmtTemplate ,null, fromaddress,'Outbound'); 
            }
    }
    
    private void sendEmailForRejected(){
            
            List<String> toaddressList = new List<String>();
            list < string > ccAddressList = new list < string > ();
            list < string > bccAddressList = new list < string > ();
            
            toaddressList.add(regRequest.Email__c);
            
            String fromaddress = null;
            
            if(AccountManager != null){
               fromaddress = AccountManager.Email;
            }else{
               fromaddress = '';
            }
            
            String targetObjId = regRequest.Dsmtracker_contact__r.Contact__c;
            
            String approvedToTaTemplate = Email_Custom_Setting__c.getOrgDefaults().RR_Rejected_TA_Email_Template__c;
            
            // Send notification to TA for Rejected RR
            this.sendEmail(targetObjId, regRequest.id, 'Registration_Request__c' ,toaddressList,ccAddressList ,bccAddressList ,approvedToTaTemplate ,null, fromaddress,'Outbound'); 
     
            
    }
    
    private Task createTask(String subject, String description, String whatid){
        Task task = new Task();
        task.Subject = 'Email: ' + subject;
        task.Description = convertHtmlToString(description);
        task.OwnerId = this.regRequest.OwnerId;
        task.WhatId = whatid;
        task.Status = 'Completed';
        task.ActivityDate = System.today();
        return task;
    }
    
    private Messages__c createMessage(String subject, String fromEmailAddress,String toEmailAddresses,String body,String parentid, String parentType,String direction){
        Messages__c msg = new Messages__c();
       try{
       
        msg.From__c = fromEmailAddress;
        msg.To__c = toEmailAddresses;
        
        msg.Subject__c = subject;
        msg.Message__c = body;
        msg.Mesage_Text_Only__c  = convertHtmlToString(body);
        msg.Message_Type__c  = 'Send Email';
        msg.Message_Direction__c  = direction;
        msg.Status__c = 'Sent';
        msg.Sent__c = Datetime.Now();
        if(parentType == 'Registration_Request__c'){
            msg.Registration_Request__c  = parentid;
            if(regRequest.Dsmtracker_contact__c != null){
                msg.DSMTracker_Contact__c = regRequest.Dsmtracker_contact__c;
            }
        }else if(parentType == 'DSMTracker_Contact__c'){
            msg.DSMTracker_Contact__c = parentid;
        }
        
        if(regRequest.Trade_Ally_Account__c !=null)
                msg.Trade_Ally_Account__c = regRequest.Trade_Ally_Account__c;
        
        system.debug('--msg--');
       }catch(Exception e){
          system.debug('--Exception e --'+e.getStackTraceString());
       } 
        return msg;
    }
    private String convertHtmlToString(String htmlString){
        if(htmlString == null)
            htmlString = '';
        
        return htmlString.replaceAll('&nbsp;', '').replaceAll(';','').replaceAll('(</{0,1}[^>]+>)','');
    }
    
    
   
}