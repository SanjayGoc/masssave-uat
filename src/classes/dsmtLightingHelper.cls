public class dsmtLightingHelper{
    
    //Query Builiding Specification based on Energy Assessment
    public static Map<String, Building_Specification__c> queryBuildingSpecificationMap(Set<String> eaIdSet)
    {
        Map<String, Building_Specification__c> buildSpecMap = new Map<String, Building_Specification__c>();
        
        Map<String, String> eaBspecIdMap = new Map<String, String>();
        for(Energy_Assessment__c ea : [select id, Building_Specification__c 
                                       from Energy_Assessment__c
                                       where id in : eaIdSet])
        {
            eaBspecIdMap.put(ea.Id, ea.Building_Specification__c);
        }
        
        if(eaBspecIdMap.size() > 0)
        {
            Map<Id, Building_Specification__c> bsMap = new Map<Id, Building_Specification__c>([select id, Year_Built__c, Bedromm__c, Occupants__c,
                                                                                               Floor_Area__c 
                                                                                               from Building_Specification__c
                                                                                               where id in : eaBspecIdMap.values()]);
            
            for(String key : eaBspecIdMap.keyset())
            {
                if(bsMap.containskey(eaBspecIdMap.get(key)))
                {
                    buildSpecMap.put(key, bsMap.get(eaBspecIdMap.get(key)));
                }
            }
        }
        
        return buildSpecMap;
    }
    
    public static decimal ComputeDefaultIndoorLightingCommon(Building_Specification__c bs, Lighting__c l) {
        decimal retval = null;
    	
        // Algorithms: ( 0.6 * Bldg.FloorAreaTotal + 300 * MIN [Bldg.NumOccs, 2 + 0.5 * (Bldg.NumOccs- 2)] ) * Lighting.IntUseFactor * (1 – (0.35 * Lighting.EffPct))
        retval = ((bs.Floor_Area__c != null ? bs.Floor_Area__c : 0) * LightingAndApplianceConstant__c.getOrgDefaults().DefaultIndoorLightingPerSqFt__c + 300.0 *
                Math.Min((bs.Occupants__c != null ? bs.Occupants__c : 1), 2 + .5 * ((bs.Occupants__c != null ? bs.Occupants__c : 1) - 2))) *
            SurfaceConstants.CommonIndoorLightingMultiplier.get(l.General_Indoor_Lighting__c != null ? l.General_Indoor_Lighting__c : LightingAndApplianceConstant__c.getOrgDefaults().DefaultIndoorLightingUsage__c) *
            (1 - .65 * (l.Efficient_Lighting_Fraction__c != null ? l.Efficient_Lighting_Fraction__c : 0));
    
       
       return retval;
    }
    
    
    public static decimal ComputeEfficientLightingFractionCommon(Lighting__c bp) {
        decimal retval = null;
    
        // lookup based on EfficientLightingQualifier
        if (bp.Efficient_Lights__c != null) {
            retval = SurfaceConstants.CommonEfficientLightingFraction.get(bp.Efficient_Lights__c);
        }
    
        return retval;
    }
    
    
    public static decimal ComputeDefaultOutdoorLightingCommon(Building_Specification__c bs, Lighting__c l) {
        decimal retval = null;
    
        // Algorithm: ( 0.05 * Bldg.FloorAreaTotal + 50 ) * Lighting.ExtUseFactor * (1 – (0.65 * Lighting.EffPct))
        retval = ((bs.Floor_Area__c != null ? bs.Floor_Area__c : 0) * LightingAndApplianceConstant__c.getOrgDefaults().DefaultOutdoorLightingPerSqFt__c + 50.0) *
            SurfaceConstants.CommonOutdoorLightingMultiplier.get(l.General_Outdoor_Lighting__c != null ? l.General_Outdoor_Lighting__c : LightingAndApplianceConstant__c.getOrgDefaults().DefaultOutdoorLightingUsage__c) *
            (1 - .65 * (l.Efficient_Lighting_Fraction__c != null ? l.Efficient_Lighting_Fraction__c : 0));
    
        // return
        //return retval;
        return retval;
    }
    
    
    public String ComputeIndoorLightingCommon(Lighting__c bp) {
        String retval = null;
    
        // get default
        retval = LightingAndApplianceConstant__c.getOrgDefaults().DefaultIndoorLightingUsage__c;
    
        return retval;
    }
    
    
    public String ComputeOutdoorLightingCommon(Lighting__c bp) {
        String retval = null;
    
        if (bp.Has_Outdoor_Fixtures__c == false) {
            retval = 'None';
        } else {
            // get default
            retval = LightingAndApplianceConstant__c.getOrgDefaults().DefaultOutdoorLightingUsage__c;
        }
    
        return retval;
    }
    
    public static decimal ComputeRemainingIndoorLightingCommon( Lighting__c dl, Map<Id,Lighting_Location__c> lsList )
    {
        decimal retval = null;

        // Get Total Indoor Lighting
        retval = dl.DefaultIndoorLighting__c;

        if(lsList !=null)
        {
            // Subtract off lighting that is in LivingSpace
            Set<Id> keys = lsList.keySet();
            For( Id key : keys) 
            {
                Lighting_Location__c lbs = lsList.get(key);
                if ( lbs.LocationSpace__c == 'LivingSpace' ) 
                {
                    retval = Math.Max( 0, (decimal)retval - ((lbs.Total_Watts__c ==null ? 0 : lbs.Total_Watts__c) * (lbs.Usage__c ==null ? 0 : lbs.Usage__c)) 
                    * SurfaceConstants.DaysPerYear / 1000 );
                }
            }
        }
    
        // subtract off applied lighting
        retval = Math.Max( 0, (decimal)retval - (dl.AppliedIndoorLighting__c ==null ? 0 : dl.AppliedIndoorLighting__c) );

        // return
        return retval;
    }
    
    public static decimal ComputeRemainingOutdoorLightingCommon( Lighting__c dl, Map<Id, Lighting_Location__c> lsList )
    {
        decimal retval = null;

        // Get Total Indoor Lighting
        retval = dl.DefaultOutdoorLighting__c;

        if(lsList !=null)
        {
            Set<Id> keys = lsList.keySet();
            // Subtract off lighting that is not in LivingSpace
            For( Id key : keys) 
            {
                Lighting_Location__c lbs = lsList.get(key);
                if ( lbs.LocationSpace__c != 'LivingSpace' ) 
                {
                    retval = Math.Max( 0, (decimal)retval - ((lbs.Total_Watts__c ==null ? 0 : lbs.Total_Watts__c) * (lbs.Usage__c ==null ? 0 : lbs.Usage__c)) 
                    * SurfaceConstants.DaysPerYear / 1000 );
                }
            }
        }
    
        // subtract off applied lighting
        retval = Math.Max( 0, (decimal)retval - (dl.AppliedOutdoorLighting__c ==null ? 0 : dl.AppliedOutdoorLighting__c) );

        // return
        return retval;
    }
    
    public static decimal ComputeDefaultLightingKWhPerYearCommon( Lighting__c ls )
    {
        decimal retval = null;

        // compute and return
        retval = (ls.RemainingIndoorLighting__c == null ? 0 : ls.RemainingIndoorLighting__c) + (ls.RemainingOutdoorLighting__c == null ? 0 : ls.RemainingOutdoorLighting__c);

        return retval;
    }
    
    public static decimal ComputeDefaultLightingLoadCommon( Lighting__c ls, string type )
    {
        decimal retval = null;

        if(type == 'Indoor')
        {
            // compute and return 
            if ( ls.KWhPerYear__c != null ) {
                decimal kWhPerYear = ls.RemainingIndoorLighting__c;
                decimal MBTUsPerYear = kWhPerYear * SurfaceConstants.CommonBtusByFuelType.get('Electricity').get('kWh') / 1000000;
                retval = MBTUsPerYear;
            }    
        }
        else if(type =='OutDoor')
        {
            // compute and return 
            if ( ls.KWhPerYear__c != null ) {
                decimal kWhPerYear = ls.RemainingOutdoorLighting__c;
                decimal MBTUsPerYear = kWhPerYear * SurfaceConstants.CommonBtusByFuelType.get('Electricity').get('kWh') / 1000000;
                retval = MBTUsPerYear;
            }    
        }
        else
        {
            // compute and return 
            if ( ls.KWhPerYear__c != null ) {
                decimal kWhPerYear = ls.KWhPerYear__c;
                decimal MBTUsPerYear = kWhPerYear * SurfaceConstants.CommonBtusByFuelType.get('Electricity').get('kWh') / 1000000;
                retval = MBTUsPerYear;
            }    
        }
        

        return retval;
    }
    
    
    public static integer ComputeWaterFixtureQuantityCommon(Water_Fixture__c wf, Building_Specification__c bs, List<Water_Fixture__c> wfList) {
        integer retval = null;
    
        retval = integer.valueOf(LightingAndApplianceConstant__c.getOrgDefaults().DefaultWaterFixtureQuantity__c);
        
        return retval;
    }
    
    public static void calculateLightingSavings(Id eaId)
    {
        list<Lighting__c> lightingList = [select id, Savings_per_unit_annually__c,
                                         (select id, Total_Watts__c
                                          from Lighting_Locations__r)
                                          from Lighting__c
                                          where Energy_Assessment__c =: eaId];
        
        if(lightingList.size() > 0)
        {
            Map<String, Lighting_Wattage__c> lightingWattageMap = Lighting_Wattage__c.getAll();
            
            List<Recommendation__c> recomList = [select id, Usage__c, Quantity__c, DSMTracker_Product__c, DSMTracker_Product__r.Name
                                                 from Recommendation__c
                                                 where Energy_Assessment__c =: eaId
                                                 and Subcategory__c = 'Lighting'];
                                                 
            if(recomList.size() > 0)
            {
                decimal originalWattage = 0;
                decimal newWattage = 0;
                decimal usages = 0;
                decimal kWhPrice = 0;
                
                for(Lighting_Location__c lLoc : lightingList[0].Lighting_Locations__r)
                {
                    if(lLoc.Total_Watts__c != null)
                        originalWattage += lLoc.Total_Watts__c;
                }
                
                for(Recommendation__c r : recomList)
                {
                    if(r.DSMTracker_Product__c != null && lightingWattageMap.containsKey(r.DSMTracker_Product__r.Name))
                    {
                        if(r.Usage__c != null)
                        {
                            newWattage += lightingWattageMap.get(r.DSMTracker_Product__r.Name).Part_Wattage__c;
                            kWhPrice   += lightingWattageMap.get(r.DSMTracker_Product__r.Name).kWh_Price__c;
                            usages += r.Usage__c;
                        }
                    }
                }
                
                decimal savings = (((originalWattage - newWattage)/1000) * usages * 365) * kWhPrice;
                
                Lighting__c lighting = new Lighting__c(Id = lightingList[0].Id,
                                                       Savings_per_unit_annually__c = savings);
                
                update lighting;
            }        
        }
    }

    public static decimal ComputeLightBulbSetLoadCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        Lighting_Location__c lbs = bp.lightLocObj;

        // compute and return 
        if ( lbs.Total_Watts__c != null && lbs.Usage__c != null ) 
        {
            decimal kWhPerYear = (decimal)lbs.Total_Watts__c * (decimal)lbs.Usage__c 
            * SurfaceConstants.DaysPerYear / 1000;

            decimal MBTUsPerYear = kWhPerYear * 
            SurfaceConstants.CommonBtusByFuelType.get('Electricity').get('kWh') / 1000000;
            
            retval = MBTUsPerYear;
        }

        return retval;
    }


}