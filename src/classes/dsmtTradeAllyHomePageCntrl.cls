global class dsmtTradeAllyHomePageCntrl{
    
    global String conName {get;set;}
    global String conId   {get;set;}
    String accId;
    global Trade_Ally_Account__c ta{get;set;}
    global Integer UnreadMessage{get;set;}
    global Integer TotalMessage{get;set;}
    global String imgURL{get;set;}
    
    global Integer TotalEligible{get;set;}
    global Integer TotalIneligible{get;set;}
    global Integer TotalOpenTickets{get;set;}
    global Integer TotalMeages{get;set;}
    global Integer TotalNewMeages{get;set;} 
    
    public Integer TotalApprovedEmployee{get;set;}
    public Integer TotalDocuments{get;set;}
    Set<Id> dsmtcId = new Set<Id>();

    public string apptJSON{get;set;}
    public String energySpecTicketsJSON {get;set;}
    public Dsmtracker_contact__c dsmtCon{get;set;}
    public String portalRole{get;set;}
    public List < DSMTracker_Contact__c > dsmtList {get;set;}
    
    public dsmtTradeAllyHomePageCntrl(){
            
            dsmtList  = new List<DSMTracker_Contact__c >();
            TotalEligible = 0;
            TotalIneligible = 0;
            TotalOpenTickets = 0;
            TotalNewMeages = 0;
            UnreadMessage = 0;
            TotalMessage = 0;
            TotalApprovedEmployee = 0;
            TotalDocuments = 0;
            
            ta = new Trade_Ally_Account__c();
            dsmtCon = new Dsmtracker_Contact__c();
            dsmtcId = new Set<Id>();
            
            GetContactInfo();
            
            portalRole = dsmtCon.Portal_Role__c;
            
            TilesCounting();
      }
      
       public void GetContactInfo(){
       
        
            String usrid = UserInfo.getUserId();
            List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
            if(usrList != null && usrList.size() > 0){
                if(usrList.get(0).ContactId != null){
                    conName = usrList.get(0).Contact.Name;
                    conId   = usrList.get(0).ContactId;
                    accId = usrList.get(0).Contact.AccountId;
                    
                      dsmtList = [Select Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c where Contact__r.Id = : conId and Trade_Ally_Account__c != null limit 1];
                    if(dsmtList != null && dsmtList.size() > 0){
                        dsmtCon = dsmtList.get(0);
                        dsmtcId.add(dsmtCon.Id);
                        
                        List<Trade_Ally_Account__c> taList = [select id,name,Account__c,owner.Name,Owner.Phone,Owner.Email,Account_Manager__r.Name,Account_Manager__r.phone,Account_Manager__r.email,
                                                                Address__c,City__c,State__c,Zip_Code__c,Website__c,Phone__c,
                                                                Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,
                                                                Outreach_rep__r.Phone,Outreach_rep__r.Email,Outreach_rep__r.Name,(select id from DSMTracker_Contacts__r)
                                                                from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                        if(taList != null && taList.size() > 0){
                            ta = taList.get(0);
                            
                            //if(dsmtcon.portal_Role__c == 'Manager'){
                                for(Dsmtracker_Contact__c dsmt : ta.DSMTracker_Contacts__r){
                                   dsmtcId.add(dsmt.Id);
                                }
                            //}
                        }
                    }
                }
            }
     }
    
      global void TilesCounting(){
          if(portalRole == 'Scheduler'){
              String userId = userinfo.getUserId();
                 // TotalEligible = [Select count() from Appointment__c where Appointment_Status__c = 'Approved' AND (OwnerId = :userId or user__c =: UserId)];
               TotalEligible = [Select count() from Appointment__c where Appointment_Status__c = 'Approved' AND Trade_Ally_Account__c =: dsmtList.get(0).Trade_Ally_Account__c]; //T-00490
                //TotalIneligible = [Select count() from Appointment__c where Appointment_Status__c  = 'Denied'  AND (OwnerId = :userId or user__c =: UserId)];
                 TotalIneligible = [Select count() from Appointment__c where Appointment_Status__c  = 'Denied' AND Trade_Ally_Account__c =: dsmtList.get(0).Trade_Ally_Account__c]; //T-00490
          }
          if(portalRole == 'Manager'){
              TotalApprovedEmployee = [select count() from Dsmtracker_Contact__c where (status__c = 'Approved' or Status__c = '') and Trade_Ally_Account__c =: ta.Id and Review__c = null];
              Date nxt30Date = Date.today()+30;
              TotalDocuments = [select count() from Attachment__c where Expires_On__c!=null AND Expires_On__c <= :nxt30Date AND Expires_On__c >= :Date.today() AND Trade_Ally_Account__c =: ta.Id];
          }
          
          Id RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
          
          TotalOpenTickets = [select count() from service_Request__c where Status__c != 'Closed' AND Dsmtracker_Contact__c in: dsmtcId and RecordTypeId !=: recordTypeId];
          
          TotalMeages = [select count() from Messages__c where Message_Direction__c = 'Outbound' AND Dsmtracker_Contact__c =: dsmtCon.Id];
          for(Messages__c m:[select Id,To__c from Messages__c where Message_Direction__c = 'Outbound' AND Status__c != 'Opened' AND Opened__c = null AND Dsmtracker_Contact__c =: dsmtCon.Id]){
              //if(m.To__c == UserInfo.getUserEmail()){
                  TotalNewMeages = TotalNewMeages +1;
              //}
          }
          
      }
      
    public void initEnergySpecTickets(){
        //List<EnergySpecTicketModel> tickets = new List<EnergySpecTicketModel>();
        List<Object> tickets = new List<Object>();
        if(dsmtCon != null){
            Set<String> recTypes = new Set<String>{'Energy Specialist Ticket','Trade Ally Service Request'};    
            List<Service_Request__c> srs = [
                SELECT 
                    Id,Name,Type__c,Sub_Type__c,Subject__c,Status__c,Priority__c,Description__c,Trade_Ally_Account__c,DSMTracker_Contact__c,CreatedDate,
                    Workorder__c,Workorder__r.Name,Customer__c,Customer_Name__c,Program_Utility_Provider__c,Primary_Provider__c,Rebate_Status__c,Boiler_Type__c,Service_Request_Panel_Title__c
                FROM Service_Request__c 
                WHERE /*RecordTypeId IN (SELECT Id FROM RecordType WHERE Name IN :recTypes) AND */ Dsmtracker_Contact__c =: dsmtCon.Id];
            for(Service_Request__c sr : srs){
                tickets.add(new Map<String,Object>{
                    'Id' => sr.Id,
                    'Name' => sr.Name,
                    'Priority' => sr.Priority__c,
                    'Subject' => sr.Subject__c,
                    'Type' => (sr.Type__c == 'Trade Ally Service Request')? 'Ticket' : sr.Type__c,
                    'subType' => sr.Sub_Type__c,
                    'CreatedDate' => sr.CreatedDate.format(),
                    'Status' => sr.Status__c,
                    'Description' => sr.Description__c,
                    'WorkOrderId'=> sr.Workorder__c,
                    'WorkOrderName'=> sr.Workorder__r.Name,
                    'CustomerId'=> sr.Customer__c,
                    'CustomerName'=>sr.Customer_Name__c,
                    'ProgramUtilityProvider'=> (sr.Program_Utility_Provider__c != null) ? sr.Program_Utility_Provider__c.replace('CSG','CLR') : sr.Program_Utility_Provider__c,
                    'Provider'=>sr.Primary_Provider__c,
                    'RebateStatus'=>sr.Rebate_Status__c,
                    'BoilerType'=>sr.Boiler_Type__c,
                    'PanelTitle'=>sr.Service_Request_Panel_Title__c
                });
            }
            System.debug('srs ----> ' + srs);
        }
        energySpecTicketsJSON = JSON.serialize(tickets);
    }
    
      public void initScheduledAppointments(){
            
             String filterquery =  ' Where Id != null And Appointment_Start_Time__c != null and Appointment_End_Time__c != null And DSMTracker_Contact__c in : dsmtcId And';
            
            filterquery += ' Appointment_Status__c in (\'scheduled\',\'Rescheduled\') And';

            
            if(filterquery.endswith('And')){
                filterquery = filterquery.substring(0,filterquery.length() - 3);
            }
            String query = 'select id,DSMTracker_Contact__r.Name, Name,Customer_Reference__c,Customer_Reference__r.Premise__r.name,Customer_Reference__r.Name,Appointment_Status__c ,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,Employee__r.Name,Workorder__r.name,Workorder__r.Address_Formula__c,CreatedDate from Appointment__c';
            
            query += filterquery ;
        
            apptJSON = '';
            String userId = userinfo.getUserId();
           // String queryString = 'Select Id,Name,Customer_Reference__r.Name,Appointment_Type__c,Appointment_Start_Time__c,Appointment_End_Time__c,Employee__r.Name,Appointment_Status__c,CreatedDate from Appointment__c where Appointment_Status__c=\'Scheduled\' AND Appointment_Start_Time__c = NEXT_90_DAYS';
        
           // queryString += ' LIMIT 50000';
            
            list<Appointment__c> listMsg = Database.query(query);
            
            
            list<AppointmentModal> listAppoint = new List<AppointmentModal>();
            for(Appointment__c apt : Database.query(query)){
                 
                system.debug(apt.Workorder__r.name);
                AppointmentModal mod = new AppointmentModal();
                mod.WorkOrdernumber=apt.Workorder__r.name;
                mod.PremiseNumber=apt.Customer_Reference__r.Premise__r.name;
                mod.Address=apt.Workorder__r.Address_Formula__c;
                mod.Id = apt.Id;
                
                mod.CustomerId = apt.Customer_Reference__c;
                mod.CustomerName= apt.Customer_Reference__r.Name;
             
                mod.AppointmentType= apt.Appointment_Type__c;
              //  mod.StartTime = string.valueOf(apt.Appointment_Start_Time__c.Month())+'/'+string.valueOf(apt.Appointment_Start_Time__c.day())+'/'+string.valueOf(apt.Appointment_Start_Time__c.year()) + ' '+string.valueOf(apt.Appointment_Start_Time__c.hour()) + ':'+string.valueOf(apt.Appointment_Start_Time__c.Minute());
              //  mod.EndTime = apt.Appointment_End_Time__c.Month()+'/'+apt.Appointment_End_Time__c.day()+'/'+apt.Appointment_End_Time__c.year() + ' '+apt.Appointment_End_Time__c.hour() + ':'+apt.Appointment_End_Time__c.Minute();
                String strConvertedDate =   apt.Appointment_Start_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
                 mod.StartTime =  strConvertedDate ;
                 strConvertedDate =   apt.Appointment_End_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
                 mod.EndTime =  strConvertedDate ;
                 
                //mod.EndTime = date.newinstance(apt.Appointment_End_Time__c.year(), apt.Appointment_End_Time__c.month(), apt.Appointment_End_Time__c.day());
                mod.Employee = apt.DSMTracker_Contact__r.Name;
                mod.Status = apt.Appointment_Status__c;
                mod.CreatedDate = string.valueOf(apt.CreatedDate.Month())+'/'+string.valueOf(apt.CreatedDate.Day())+'/'+string.valueOf(apt.CreatedDate.Year());// date.newinstance(apt.CreatedDate.year(), apt.CreatedDate.month(), apt.CreatedDate.day());
                    
                listAppoint.add(mod);
            }
            system.debug('--listMessage---'+listAppoint );
            apptJSON = JSON.serialize(listAppoint );
        
        system.debug('--listMessage---'+listAppoint );
    }
    
    public class AppointmentModal{
        public String WorkOrdernumber{get;set;}
        public String PremiseNumber{get;set;}
        public String Address{get;set;}
        public string Id{get;set;}
        public string CustomerName{get;set;}
        public string AppointmentType{get;set;}
        public String StartTime{get;set;}
        public String EndTime{get;set;}
        public string Employee{get;set;}
        public string Status{get;set;}
        public String CreatedDate{get;set;}
        public String CustomerId{get;set;}
        
    }
}