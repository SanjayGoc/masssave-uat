@isTest
public class populateWeatherStationBIBUTest{
    public testMethod static void test1(){
        
        List <WeatherStationTemprature__c> wsTempList = new List <WeatherStationTemprature__c>();
        
        WeatherStation__c ws = new WeatherStation__c(WeatherStationName__c = 'station1');
        insert ws;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        WallRatio__c wr = new WallRatio__c(Name = 'Rectangular', Value__c = 10);
        insert wr;
        
        System_Config__c systemConfig = new System_Config__c(Arrival_Window_min_before_Appointment__c = 40,
                                                             Arrival_Window_min_after_Appointment__c = 60);
        insert systemConfig;
        
        Saving_Constant__c sc = new Saving_Constant__c(DefaultDoorWidth__c = 10,
                                                       DefaultDoorHeight__c = 20,
                                                       FrontBackToLeftRightWindowAreaFactor__c = 23);
        insert sc;
        
        WallInsulationAmountRValue__c wiar = new WallInsulationAmountRValue__c(Name = 'Standard',
                                                                               Value__c = 12);
        insert wiar;
        
        LightingAndApplianceConstant__c lightApplianceConstant = new LightingAndApplianceConstant__c(DefaultIndoorLightingPerSqFt__c = 30,
                                                                                                     DefaultIndoorLightingUsage__c = '23',
                                                                                                     DefaultOutdoorLightingPerSqFt__c = 30,
                                                                                                     DefaultOutdoorLightingUsage__c = '23');
        insert lightApplianceConstant;
        
        Workorder__c wo = new Workorder__c();
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();         
        insert wo;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Floor_Area__c = 1;
        bs.Orientation__c = 'South';
        bs.Floors__C=1.0;
        bs.Bedromm__c = 1;
        bs.Ceiling_Heights__c = 10;
        bs.Appointment__c = appt.id;
        bs.Occupants__c = 10;
        bs.Year_Built__c='2018';
        
        insert bs;
        
        Customer__c cust = new Customer__c(Name = 'tst',
                                           Service_Zipcode__c = '34567');
                                           
        insert cust;
        
        ZipWeatherStation__c z = new ZipWeatherStation__c(PostalCode__c = '34567',
                                                          TmyType__c = 'TMY2');
        insert z;
        
        Energy_Assessment__c ea = new Energy_Assessment__c(Building_Specification__c = bs.Id,
                                                           Building_Shape__c = 'Rectangular',
                                                           Customer__c = cust.Id,
                                                           Assessment_Type__c = 'HEA (Home Energy Assessment)',
                                                           Appointment__c = appt.Id,
                                                           Workorder__c = wo.Id);
        
        try
        {
            insert ea;
        }catch(Exception ex){}
        
        WeatherStationTemprature__c wst = new WeatherStationTemprature__c(WSProperty__c='property',TempStart__c=1,TempIncr__c=1,
                                                                          TempValues__c='1,2,3,4,5',WeatherStation__c=ws.id,
                                                                          WeatherStationPostalCode__c = '34567',
                                                                          TMYType__c = 'TMY2');
        insert wst;
        
        wsTempList.add(wst);
        
        test.startTest();
        
        
        
        test.stopTest();
    }
}