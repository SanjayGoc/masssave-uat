@isTest
public class dsmtTradeAllyRegistrationProcesCntrlTest{
    @isTest
    public Static Void RunTest(){
       
        User u = Datagenerator.CreatePortalUser();
        system.runAs(u){
        
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            ta.Trade_Ally_Type__c = 'HPC';
            insert ta;
                    
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c= u.contactId;
            insert dsmtc;
            
            
            /*Document doc = new Document();
            doc.Name = 'test';
            doc.DeveloperName = 'HPC';
            doc.body = Blob.valueof('12345');
            doc.folderid = [select id from folder where name = 'HPC'].id;
            insert doc;*/
            
            Registration_Request__c rr = new Registration_Request__c();
            rr.First_Name__c='test RR';
            rr.DSMTracker_Contact__c=dsmtc.id;
            rr.Trade_Ally_Account__c=ta.id;
            rr.Steps_Completed__c = '2';
            rr.Status__c = 'Registration Completed';
            rr.Counties__c='Counties__c';
            rr.Sub_Counties__c='Sub_Counties__c';
            rr.Proficiences__c='Proficiences__c';
            rr.Program_Sponsors__c='Program_Sponsors__c';
            insert rr;
            
            DSMTracker_Contact__c dsmtc1 = new DSMTracker_Contact__c();
            dsmtc1.Name='test';
            dsmtc1.Super_User__c=true;
            dsmtc1.Trade_Ally_Account__c=ta.id;
            dsmtc1.contact__c= u.contactId;
            dsmtc1.Registration_Request__c = rr.Id;
            insert dsmtc1;
            
            Region_Based_Program__c rbp = new Region_Based_Program__c();
            rbp.County__c = 'Counties__c';
            rbp.Trade_Ally_Types__c='HPC';
            insert rbp;
            
            rr = [select id,Registration_Number__c from Registration_Request__c where id =: rr.id];
            system.debug('--rr--'+rr );    
            
            ApexPages.currentPage().getParameters().put('id',rr.Registration_Number__c); 
            
            dsmtTradeAllyRegistrationProcessCntrl cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            DSMTracker_Contact__c newEmployee = cntrl.newEmployee;
            boolean isEmailRequiredForNewEmployee = cntrl.isEmailRequiredForNewEmployee;
            String attachLic = cntrl.attachLic;
            String attachExpdate=cntrl.attachExpdate;
            Map<String, Checklist_Items__c> pgCheckListItemsMap = cntrl.pgCheckListItemsMap;
            Checklist__c pgCheckList = cntrl.pgCheckList;
            String PortalURL = cntrl.PortalURL;
            String orgId = cntrl.orgId;
            Integer stepToMove = cntrl.stepToMove;
            cntrl.saveNewEmployee();
            cntrl.rerender();
            cntrl.continueRegistrationProcess();
            
            ApexPages.currentPage().getParameters().put('step','3'); 
            cntrl.moveToStep();
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            ApexPages.currentPage().getParameters().put('step','6'); 
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            ApexPages.currentPage().getParameters().put('step','8'); 
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            ApexPages.currentPage().getParameters().put('step','9'); 
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            ApexPages.currentPage().getParameters().put('step','10'); 
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            ApexPages.currentPage().getParameters().put('step3',rr.Registration_Number__c); 
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            cntrl.docViewerProceedAction();
            cntrl.saveAddressInfo();
            cntrl.saveTerritoriesInfo();
            cntrl.getattachmentList();
            cntrl.getExceptionList();
            cntrl.updateException();
            cntrl.replaceAttachment();
            cntrl.completeException();
            cntrl.alreadyDoneDoc();
            Attachment__c atc = new Attachment__c();
            atc.Status__c='Completed';
            atc.Attachment_Type__c='Agreement';
            atc.Registration_Request__c=rr.Id;
            insert atc;
            cntrl.attachmentId = atc.Id;
            cntrl.check_print_collateral_status();
         
        }
    }
    
     @isTest
    public Static Void RunTest1(){
       
        User u = Datagenerator.CreatePortalUser();
        system.runAs(u){
        
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            ta.Trade_Ally_Type__c = 'HPC';
            insert ta;
                    
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c= u.contactId;
            insert dsmtc;
            
            
            /*Document doc = new Document();
            doc.Name = 'test';
            doc.DeveloperName = 'HPC';
            doc.body = Blob.valueof('12345');
            doc.folderid = [select id from folder where name = 'HPC'].id;
            insert doc;*/
            
            Registration_Request__c rr = new Registration_Request__c();
            rr.First_Name__c='test RR';
            rr.DSMTracker_Contact__c=dsmtc.id;
            rr.Trade_Ally_Account__c=ta.id;
            rr.Steps_Completed__c = '2';
            rr.Status__c = 'Registration Completed';
            rr.Counties__c='Counties__c';
            rr.Sub_Counties__c='Sub_Counties__c';
            rr.Proficiences__c='Proficiences__c';
            rr.Program_Sponsors__c='Program_Sponsors__c';
            insert rr;
            
            DSMTracker_Contact__c dsmtc1 = new DSMTracker_Contact__c();
            dsmtc1.Name='test';
            dsmtc1.Super_User__c=true;
            dsmtc1.Trade_Ally_Account__c=ta.id;
            dsmtc1.contact__c= u.contactId;
            dsmtc1.Registration_Request__c = rr.Id;
            insert dsmtc1;
            
            Region_Based_Program__c rbp = new Region_Based_Program__c();
            rbp.County__c = 'Counties__c';
            rbp.Trade_Ally_Types__c='HPC';
            insert rbp;
            
            rr = [select id,Registration_Number__c from Registration_Request__c where id =: rr.id];
            system.debug('--rr--'+rr );    
            
            ApexPages.currentPage().getParameters().put('id',rr.Registration_Number__c); 
            
            dsmtTradeAllyRegistrationProcessCntrl cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            DSMTracker_Contact__c newEmployee = cntrl.newEmployee;
            boolean isEmailRequiredForNewEmployee = cntrl.isEmailRequiredForNewEmployee;
            String attachLic = cntrl.attachLic;
            String attachExpdate=cntrl.attachExpdate;
            Map<String, Checklist_Items__c> pgCheckListItemsMap = cntrl.pgCheckListItemsMap;
            Checklist__c pgCheckList = cntrl.pgCheckList;
            String PortalURL = cntrl.PortalURL;
            String orgId = cntrl.orgId;
            Integer stepToMove = cntrl.stepToMove;
            cntrl.saveNewEmployee();
            cntrl.rerender();
            cntrl.continueRegistrationProcess();
            
            ApexPages.currentPage().getParameters().put('step','3'); 
            cntrl.moveToStep();
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            Exception__c exc = new Exception__c();
            exc.DSMTracker_Contact__c = dsmtc.Id;
            exc.Request_Date_Of_Hire__c = Date.today().addMonths(7);
            exc.Expiry_date__c = Date.today().addMonths(-1);
            exc.Required_Attachment_Type__c = 'BPI Building Analyst';
            
            Exception__c exc1 = new Exception__c();
            exc1.DSMTracker_Contact__c = dsmtc.Id;
            exc1.Expiry_date__c = Date.today().addMonths(-1);
            exc1.Required_Attachment_Type__c = 'BPI Building Analyst';
            insert exc;
            insert exc1;
            cntrl.generateEmployeeModal();

            
            Attachment__c atc = new Attachment__c();
            atc.Status__c='Completed';
            atc.Attachment_Type__c='Small_biz_disclaimer';
            atc.Registration_Request__c=rr.Id;
            insert atc;
            cntrl.attachmentId = atc.Id;
            cntrl.check_print_collateral_status();
            
            atc.Attachment_Type__c='SMA_Agreement';
            update atc;
            cntrl.check_print_collateral_status();
            
            ApexPages.currentPage().getParameters().remove('id');
            cntrl.checkAfterLogin();
            cntrl.getCountriesSelectList();
            if(cntrl.employeeModallist != null && cntrl.employeeModallist.size() > 0)
            {
                cntrl.employeeid = cntrl.employeeModallist.get(0).Employee.id;
                cntrl.employeeModallist.get(0).exceptionList.add(exc);
                cntrl.employeeModallist.get(0).exceptionList.add(exc1);
            }
            cntrl.updateDSMTExceptions();
            cntrl.nextEmployee();
            dsmtTradeAllyRegistrationProcessCntrl.saveSign(rr.Id, 'docname', 'imgURI', 'attachType');
         
        }
    }
    
    @isTest
    public Static Void RunTest2(){
       
        User u = Datagenerator.CreatePortalUser();
        system.runAs(u){
        
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            ta.Trade_Ally_Type__c = 'HVAC';
            insert ta;
                    
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c= u.contactId;
            dsmtc.BPI_Envelope_Attached__c = true;
            dsmtc.CSL_Attached__c=true;
            insert dsmtc;
            
            
            /*Document doc = new Document();
            doc.Name = 'test';
            doc.DeveloperName = 'HPC';
            doc.body = Blob.valueof('12345');
            doc.folderid = [select id from folder where name = 'HPC'].id;
            insert doc;*/
            
            Registration_Request__c rr = new Registration_Request__c();
            rr.First_Name__c='test RR';
            rr.DSMTracker_Contact__c=dsmtc.id;
            rr.Trade_Ally_Account__c=ta.id;
            rr.Steps_Completed__c = '2';
            rr.Status__c = 'Registration Completed';
            rr.Counties__c='Counties__c';
            rr.Sub_Counties__c='Sub_Counties__c';
            rr.Proficiences__c='Proficiences__c';
            rr.Program_Sponsors__c='Program_Sponsors__c';
            insert rr;
            
            DSMTracker_Contact__c dsmtc1 = new DSMTracker_Contact__c();
            dsmtc1.Name='test';
            dsmtc1.Super_User__c=true;
            dsmtc1.Trade_Ally_Account__c=ta.id;
            dsmtc1.contact__c= u.contactId;
            dsmtc1.Registration_Request__c = rr.Id;
            insert dsmtc1;
            
            Region_Based_Program__c rbp = new Region_Based_Program__c();
            rbp.County__c = 'Counties__c';
            rbp.Trade_Ally_Types__c='HPC';
            insert rbp;
            
            rr = [select id,Registration_Number__c from Registration_Request__c where id =: rr.id];
            system.debug('--rr--'+rr );    
            
            ApexPages.currentPage().getParameters().put('id',rr.Registration_Number__c); 
            
            dsmtTradeAllyRegistrationProcessCntrl cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            DSMTracker_Contact__c newEmployee = cntrl.newEmployee;
            boolean isEmailRequiredForNewEmployee = cntrl.isEmailRequiredForNewEmployee;
            String attachLic = cntrl.attachLic;
            String attachExpdate=cntrl.attachExpdate;
            Map<String, Checklist_Items__c> pgCheckListItemsMap = cntrl.pgCheckListItemsMap;
            Checklist__c pgCheckList = cntrl.pgCheckList;
            String PortalURL = cntrl.PortalURL;
            String orgId = cntrl.orgId;
            Integer stepToMove = cntrl.stepToMove;
            cntrl.saveNewEmployee();
            cntrl.rerender();
            cntrl.continueRegistrationProcess();
            
            ApexPages.currentPage().getParameters().put('step','3'); 
            cntrl.moveToStep();
            cntrl = new dsmtTradeAllyRegistrationProcessCntrl(); 
            
            Exception__c exc = new Exception__c();
            exc.DSMTracker_Contact__c = dsmtc.Id;
            exc.Request_Date_Of_Hire__c = Date.today().addMonths(7);
            exc.Expiry_date__c = Date.today().addMonths(-1);
            exc.Required_Attachment_Type__c = 'BPI Building Analyst';
            
            Exception__c exc1 = new Exception__c();
            exc1.DSMTracker_Contact__c = dsmtc.Id;
            exc1.Expiry_date__c = Date.today().addMonths(-1);
            exc1.Required_Attachment_Type__c = 'BPI Building Analyst';
            insert exc;
            insert exc1;
            cntrl.generateEmployeeModal();

            
            Attachment__c atc = new Attachment__c();
            atc.Status__c='Completed';
            atc.Attachment_Type__c='Small_biz_disclaimer';
            atc.Registration_Request__c=rr.Id;
            insert atc;
            cntrl.attachmentId = atc.Id;
            cntrl.check_print_collateral_status();
            
            atc.Attachment_Type__c='SMA_Agreement';
            update atc;
            cntrl.check_print_collateral_status();
            
            ApexPages.currentPage().getParameters().remove('id');
            cntrl.checkAfterLogin();
            cntrl.getCountriesSelectList();
            if(cntrl.employeeModallist != null && cntrl.employeeModallist.size() > 0)
            {
                cntrl.employeeid = cntrl.employeeModallist.get(0).Employee.id;
                cntrl.employeeModallist.get(0).exceptionList.add(exc);
                cntrl.employeeModallist.get(0).exceptionList.add(exc1);
                cntrl.employeeModallist.get(0).Employee.BPI_Envelope_Attached__c=true;
                cntrl.employeeModallist.get(0).Employee.CSL_Attached__c=true;
                update cntrl.employeeModallist.get(0).Employee;
            }
            cntrl.updateDSMTExceptions();
            cntrl.nextEmployee();
            dsmtTradeAllyRegistrationProcessCntrl.saveSign(rr.Id, 'docname', 'imgURI', 'attachType');
         
        }
    }
    
}