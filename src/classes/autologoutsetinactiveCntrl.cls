public class autologoutsetinactiveCntrl {

    public PageReference relogin() {
        return null;
    }


    public PageReference endlogin() {
        return null;
    }

   public PageReference fetchUserId() {
        Id uid = ApexPages.currentPage().getParameters().get('uid');
        system.debug('--fetchUserId uid--'+uid);
        String startUrl   = '/apex/endlogout?uid='+uid; 
        Online_Portal_URLs_Hierarchy__c credentials = Online_Portal_URLs_Hierarchy__c.getInstance();
        PageReference ref = Site.login(credentials.system_adminu__c, credentials.system_adminp__c, startUrl);
        return ref;
    }
    
    public PageReference updateUserandLogout() {
        //PageReference pageRef1 = new PageReference('/' + ApexPages.currentPage());
        //pageRef1.getParameters().put('uid2', '005g0000001mIAA');
        Id uid = ApexPages.currentPage().getParameters().get('uid');
        system.debug('--updateUserandLogout Uid--'+uid);
        User u2 =[Select IsActive,ProfileId, Username from User where id=:uid];
        system.debug('--u2--'+u2);
        
        
        //updated by pradip patel on 5/5/2015 for prevent to deactive of system portal login and guest users
        
        Online_Portal_URLs_Hierarchy__c credentials = Online_Portal_URLs_Hierarchy__c.getInstance();
        system.debug('--o--'+credentials );
        
        Map<Id,Profile> profileIds = new Map<id,profile>([SELECT Id,UserLicenseId FROM Profile where UserLicenseId  in (SELECT Id FROM UserLicense where name ='Guest User License')]);
        system.debug('--profileIds--'+profileIds);
        
        if(profileIds != null && u2 != null && profileIds.get(u2.ProfileId) == null && credentials != null && credentials.system_adminu__c != u2.Username){
            u2.IsActive = false;
            if(!Test.isRunningTest()) {
            	update u2;
            }
        }

       
        PageReference pageRef = new PageReference('/secur/logout.jsp?id='+uid);
        pageRef.setRedirect(true);
        return pageRef;
     }

}