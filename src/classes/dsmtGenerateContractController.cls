// Ticket Number:  DSST-8414
public with sharing class dsmtGenerateContractController {
    public Proposal__c proposal{get;set;}
    public dsmtGenerateContractController(ApexPages.StandardController stdController) {
        String proposalId = stdController.getId();
        this.proposal = [SELECT Id, Name, Deposit_Amount__c,Calc_Deposit_Amount__c, Total_Cost__c,
                                Energy_Assessment__c,Deposit_Type__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c
                                FROM Proposal__c WHERE Id =: proposalId];
                                
        if(this.proposal.Total_Cost__c > 0){
            this.proposal.Deposit_Amount__c = (this.proposal.Deposit_Amount__c != null) ? this.proposal.Calc_Deposit_Amount__c : 0.00;
        }

    }

    public PageReference generateContract(){

        Set<String> propIds = new Set<String>();
        propIds.add(this.proposal.Id);
        Boolean isContract = true;   //DSST-14093
        List<Proposal__c> proplist = [select Name,id,Type__c,Add_to_Contract__C,Name_Type__c ,Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c from proposal__c where Energy_Assessment__c =: this.proposal.Energy_Assessment__c];
        String AttachType = null;
        List<String> types = new List<String>();
        
        if(proplist.size() > 0){
            
            for(Proposal__c p : proplist){
                p.Add_to_Contract__c = false;
                if(propIds.contains(p.Id)){
                    p.Add_to_Contract__c = true;   
                    
                    if(AttachType == null){
                        if(p.Type__c == 'Insulation'){
                            AttachType = 'Contract for Product and Services - Weatherization';
                        }else if(p.Type__c  == 'Air Sealing'){
                            AttachType  ='Contract for Product and Services - Air Sealing';
                        }else if(p.Type__c  == 'Electric Thermostats'){
                            AttachType  = 'Contract for Product and Services - TStats';
                        }
                    }else{
                        if(p.Type__c == 'Insulation' && !AttachType.contains('Weatherization')){
                            AttachType = AttachType +' and Weatherization';
                        }else if(p.Type__c == 'Air Sealing' && !AttachType.contains('Air Sealing')){
                            AttachType = AttachType +' and Air Sealing';
                        }else if(p.Type__c  == 'Electric Thermostats' && !AttachType.contains('Tstats')){
                            AttachType = AttachType +' and Tstats';
                        }
                    }    

                    p.Deposit_Amount__c = this.proposal.Deposit_Amount__c;
                    p.Deposit_Type__c = this.proposal.Deposit_Type__c;   
                }
              
                    
            }
            
            if(AttachType == null){
               AttachType  = 'Contract';
            }
            if(attachType.contains('Air Sealing and Weatherization') || attachType.contains('Weatherization and Air Sealing')){
                attachType = 'Contract for Product and Services - Air Sealing and Weatherization';
            }
            if(attachType.contains('Air Sealing and Tstats') || attachType.contains('Tstats and Air Sealing')){
                attachType = 'Contract for Product and Services - Air Sealing and Tstats';
            }
            if(attachType.contains('Weatherization And Tstats') || attachType.contains('Tstats and Weatherization')){
                attachType = 'Contract for Product and Services - Weatherization and Tstats';
            }
            
            update proplist;
        
            //Proposal__c proposal = proplist.get(0);
            String Congatemp = 'Mass Proposal Contract clearesult template';
            if(proposal.Energy_Assessment__r.Trade_Ally_Account__c != null && proposal.Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c == false){
                
                List<APXTConga4__Conga_Template__c> TradeallyCongaTemplateList = [select Id,Unique_Template_Name__c from APXTConga4__Conga_Template__c Where Trade_Ally_Account__c =: proposal.Energy_Assessment__r.Trade_Ally_Account__c limit 1];
                Congatemp = !TradeallyCongaTemplateList.isEmpty() ? TradeallyCongaTemplateList[0].Unique_Template_Name__c : 'HPC Mass Proposal Template'; 
                isContract = !TradeallyCongaTemplateList.isEmpty() ? true : false; 
            }   
            
            system.debug('--Congatemp--'+Congatemp);
            
            List<String> queryList = new List<String>();
            queryList.add('Proposal_contract_Query1');
            queryList.add('Proposal_contract_Query2');
            queryList.add('Proposal_contract_Query3');
            queryList.add('Proposal_contract_Query4');
            queryList.add('Proposal_contract_Query5');
            queryList.add('Proposal_contract_Query6');
            queryList.add('Proposal_contract_Query7');
            queryList.add('Proposal_contract_Query8');
            queryList.add('Proposal_contract_Query9');
            queryList.add('Proposal_contract_Query91');
            queryList.add('Proposal_contract_Query92');
            List<APXTConga4__Conga_Template__c> CongaTemplateList = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c =: Congatemp];
            List<APXTConga4__Conga_Merge_Query__c> CongaqueryList = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In: queryList order By Unique_Conga_Query_Name__c ASC];
            String query = '';
            for(APXTConga4__Conga_Merge_Query__c q : CongaqueryList)
            {
                query += q.ID +'?pv0='+proposal.Energy_Assessment__c+ ',';
            }
            query = query.removeEnd(',');
            
            String templateID = CongaTemplateList.get(0).id;
            system.debug('--query--'+query);
            
            Attachment__c cAttpdf = new Attachment__c();
            cAttpdf.Status__c = 'New';
            cAttpdf.Energy_Assessment__c = proposal.Energy_Assessment__c;
            cAttpdf.Proposal__c = proposal.Id;
            cAttpdf.Attachment_Type__c = AttachType;
            System.debug('Attachment Type :-'+AttachType);
            
            
            insert cAttpdf;  

            String url = '/apex/dsmtGenerateDocAndMigrate?Id='+proposal.Id +'&ObjectType=Proposal__c&MasterId='+cAttpdf.Id +'&QueryId='+query+'&TemplateId='+templateID+'&ispreview=true&ofn=Proposal Contract Document&isContract=' +isContract; 
            PageReference pgref = new PageReference(url);
            return pgref;

        }
        
        return null;
    }
}