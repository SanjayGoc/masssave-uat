@istest
public class CombustionDevicesComponentControllerTest
{
    @istest
    static void CombustionDevicesComponentControllertest()
    {
           
        Energy_Assessment__c  ea = dsmtEAModel.setupAssessment();

        Mechanical__c mech = new Mechanical__c();        
        mech.Energy_Assessment__c = ea.Id;
        mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
        insert mech;

        Mechanical_Type__c mechType = new Mechanical_Type__c();
        mechType.Energy_Assessment__c = ea.Id;
        mechType.Mechanical__c = mech.Id;
        mechType.Name='test';
        mechType.Fuel_Type__c='Oil';
        mechType.Capacity_kBtu_h__c=10;
        mechType.Combustion_Type__c='Atmospheric';
        mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();        

        Mechanical_Type__c mechType2 = new Mechanical_Type__c();
        mechType2.Energy_Assessment__c = ea.Id;
        mechType2.Mechanical__c = mech.Id;
        mechType2.Name='test';
        mechType2.Fuel_Type__c='Oil';
        mechType2.Capacity_kBtu_h__c=10;
        mechType2.Combustion_Type__c='Atmospheric';

        insert mechType;
        insert mechType2;
        

        List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

        Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = ea.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = ea.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
        mstList.add(mechSubType);

        mechSubType = new Mechanical_Sub_Type__c();
        mechSubType.Energy_Assessment__c = ea.Id;
        mechSubType.Mechanical__c = mech.Id;
        mechSubType.Mechanical_Type__c = mechType.Id;
        mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
        mstList.add(mechSubType);

        Test.startTest();

        insert mstList;
                
        Safety_Aspect__c sa  = new Safety_Aspect__c();
        insert sa;
        Safety_Aspect_Item__c sai=new Safety_Aspect_Item__c();
        sai.Safety_Aspect__c=sa.id;
        insert sai;
            
        Combustion_Device__c cdevice =new Combustion_Device__c();
        cdevice.Name='test';
        cdevice.Energy_Assessment__c = ea.Id;
        cdevice.Mechanical_Type__c=mechType.id;
        
        insert cdevice;
        
        String applianceIds='';//api.id+','+api2.id;
        String mechanicalTypeIds=mechType.id+','+mechType2.id;


        CombustionDevicesComponentController cdcc =new CombustionDevicesComponentController();
        
        Combustion_Device__c cd = cdcc.newDevice;
        cd.Energy_Assessment__c=ea.id;
        insert cd;
        cdcc.parentId=ea.id;
        cdcc.parentType='Energy_Assessment__c';
        cdcc.eaid=ea.id;
        cdcc.getDeviceModels();
        
        cdcc =new CombustionDevicesComponentController();
        ApexPages.currentPage().getParameters().put('applianceIds',applianceIds);
        ApexPages.currentPage().getParameters().put('mechanicalTypeIds',mechanicalTypeIds);
        system.debug('mechanicalTypeIds---'+mechanicalTypeIds);
        cdcc.parentId=ea.id;
        cdcc.parentType='Energy_Assessment__c';
        cdcc.getDeviceModels();
        cdcc.getNewDeviceModels();
        //cdcc.addNewCombustionDevices();

        Test.stopTest();
     
    }
}