@isTest
public class BatchAvailableTimeSlotTest {
    public static testMethod void unitTest() {
        Location__c Lc = new Location__c();
        Lc.name = 'test';
        insert Lc;           
        
        Employee__c Em = new Employee__c( location__c = Lc.Id);
        Em.Employee_Id__c = 'Test';
        Em.name = 'Test';
        Em.Active__c = true;
        Em.Status__c ='Active'; 
        insert Em;
        
        Employee__c Em1 = new Employee__c( location__c = Lc.Id);
        Em1.Employee_Id__c = 'Test1';
        Em1.name = 'Test1';
        Em1.Active__c = true;
        Em1.Status__c ='Active'; 
        insert Em1;
        
        Work_Team__c Wt = new Work_Team__c();
        Wt.name = 'Test';
        Wt.Captain__c = Em.Id;
        Wt.Unavailable__c = false;
        wt.Service_Date__c = DAte.Today();
        wt.Start_Date__c = datetime.newInstance(2013, 8, 14, 12, 30, 0);
        wt.End_Date__c = datetime.newInstance(2016, 9, 15, 12, 30, 0);
        insert Wt;
        
        Work_Team_Member__c Wtm = new Work_Team_Member__c();
        Wtm.Work_Team__c = Wt.Id;
        Wtm.Employee__c = Em1.Id;
        insert Wtm;
        
        Appointment__c App = new Appointment__c();
        App.Work_Team__c = Wt.Id;
        App.Appointment_Status__c = 'Scheduled';
        App.Appointment_Start_Time__c = datetime.newInstance(2014, 9, 15, 12, 30, 0);
        App.Appointment_End_Time__c = datetime.newInstance(2015, 9, 15, 12, 30, 0);
        insert App;
        
        Available_Slot__c Av = new Available_Slot__c();
        Av.Work_Team_Member__c = Wtm.Id;
        Av.Current_Address__c = 'Ringgold Nebraska 69167';
        Av.Next_Address__c = 'Ringgold Rd North Platte, NE 69101';
        
        insert Av;
        
        Appointment__c App1 = new Appointment__c();
        App1.Work_Team__c = Wt.Id;
        App1.Appointment_Status__c = 'Scheduled';
        App1.Appointment_Start_Time__c = datetime.newInstance(2016, 9, 15, 12, 30, 0);
        App1.Appointment_End_Time__c = datetime.newInstance(2015, 9, 15, 12, 30, 0);
        insert App1;
        
        BatchAvailableTimeSlot Bats = new BatchAvailableTimeSlot();
        DataBase.executeBatch(Bats);
        
    }
}