public class dsmtValidateExceptionsRR
{
   public static boolean isStopValidateException = false;
   public static void InsertExceptionsOnRR(List<Registration_Request__c> RRlist){
       
        Map<String,Exception_Template__c> mapExt = new Map<String,Exception_Template__c>();     
        //Gets all expection with that check box checked.
        for(Exception_Template__c ext : [SELECT id,Online_Application_Section__c,Name, Reference_ID__c,Attachment_Needed__c,Required_Attachment_Type__c, 
                                            Outbound_Message__c,Internal__c, Exception_Message__c,Short_Rejection_Reason__c ,OwnerId,Type__c,Object_Sub_Type__c FROM Exception_Template__c  
                                        WHERE Automatic__c = true and Active__c = true and Registration_Request_Level_Message__c= true]) {
          mapExt.put(ext.Reference_Id__c,ext);
        } 
        
        Set<Id> rrids = new Set<id>();
        for(Registration_Request__c rr :RRlist){
             rrids.add(rr.id);   
        }
        
        Map<String,Exception__c> mapExceptions = new Map<String,Exception__c>();
        for(Exception__c Ex :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c, Registration_Request__c,Registration_Request__r.id FROM Exception__c 
                                WHERE Registration_Request__r.Id IN: rrids AND Disposition__c  != 'Resolved' ]){    
            mapExceptions.put(Ex.Registration_Request__c+'-'+Ex.Exception_Template__r.Reference_ID__c,Ex);     
        }      
        
        List<Exception__c> ExceptionsCreated = new List<Exception__c>();
          
        for(Registration_Request__c RRNew : RRlist) {
            
            List<string> AutoExceptionsList = new List<string>();
            system.debug('--RRNew.Trade_Ally_Type__c--'+RRNew.Trade_Ally_Type__c);
            
            for(String refId : mapExt.keyset()){
                Exception_Template__c temp = mapExt.get(refId);
                
                if(RRNew.Trade_Ally_Type__c != null && temp != null && temp.Object_Sub_Type__c != null && temp.Object_Sub_Type__c.contains(RRNew.Trade_Ally_Type__c)){
                      
                     if(refId == 'ETN-0000001' && RRNew.HIC_Attached__c== false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000001')==null) 
                            AutoExceptionsList.add('ETN-0000001'); //HIC Upload
                     }
                     
                     if(refId == 'ETN-0000002' && RRNew.W_9_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000002')==null) 
                            AutoExceptionsList.add('ETN-0000002'); //W-9 Upload
                     }
                     
                     if(refId == 'ETN-0000003' && RRNew.BPI_Gold_Star_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000003')==null) 
                            AutoExceptionsList.add('ETN-0000003'); //BPI Gold Star Upload
                     }
                     
                     if(refId == 'ETN-0000004' && RRNew.Liability_1M_2M_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000004')==null) 
                            AutoExceptionsList.add('ETN-0000004'); //Liability $1M/$2M
                     }
                     
                     if(refId == 'ETN-0000005' && RRNew.Lead_Safe_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000005')==null) 
                            AutoExceptionsList.add('ETN-0000005'); //Lead Safe
                     }
                     
                     if(refId == 'ETN-0000006' && RRNew.Auto_1M_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000006')==null) 
                            AutoExceptionsList.add('ETN-0000006'); //Auto $1M
                     }
                     
                     if(refId == 'ETN-0000007' && RRNew.Excess_Liability_1M_2M_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000007')==null) 
                            AutoExceptionsList.add('ETN-0000007'); //Excess Liability $1M/$2M
                     }
                     
                     if(refId == 'ETN-0000008' && RRNew.Workers_Comp_500K_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000008')==null) 
                            AutoExceptionsList.add('ETN-0000008'); //Workers Comp $500K
                     }
                     
                     if(refId == 'ETN-0000009' && RRNew.Logo_for_Online_Profile_Attached__c== false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000009')==null) 
                            AutoExceptionsList.add('ETN-0000009'); //Logo for Online Profile
                     }
                     
                     if(refId == 'ETN-0000010' && RRNew.Accurate_Summ_Backend_Check_Attached__c == false){
                        If(mapExceptions.get(RRNew.id+'-ETN-0000010')==null) 
                            AutoExceptionsList.add('ETN-0000010'); //Accurate Summary background check
                     }
               }
                
           }
            
            
            system.debug('--AutoExceptionsList--'+AutoExceptionsList);
            for(string autolist :AutoExceptionsList){
                if(mapExt.get(autolist) !=null) {
                    Exception_Template__c Exceptiontemp = mapExt.get(autolist);
                    Exception__c ExceptionstoAdd = new Exception__c(); 
                    ExceptionstoAdd.Internal__c = Exceptiontemp.Internal__c; 
                    ExceptionstoAdd.Exception_Message__c= Exceptiontemp.Exception_Message__c; 
                    ExceptionstoAdd.Outbound_Message__c=Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Registration_Request__c =  RRNew.Id;
                    ExceptionstoAdd.Exception_Template__c = Exceptiontemp.id;
                    ExceptionstoAdd.FInal_Outbound_Message__c = Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Short_Rejection_Reason__c = Exceptiontemp.Short_Rejection_Reason__c ;
                    ExceptionstoAdd.Automated__c = true;
                    ExceptionstoAdd.OwnerId =RRNew.OwnerId;
                    ExceptionstoAdd.Attachment_Needed__c = Exceptiontemp.Attachment_Needed__c;
                    ExceptionstoAdd.Required_Attachment_Type__c = Exceptiontemp.Required_Attachment_Type__c;
                    ExceptionstoAdd.Online_Application_Section__c = Exceptiontemp.Online_Application_Section__c;
                    ExceptionstoAdd.Type__c = Exceptiontemp.Type__c;
                    ExceptionsCreated.add(ExceptionstoAdd);    
                }
            }     
       }
        
        system.debug('--ExceptionsCreated--'+ExceptionsCreated);
        dsmtValidateExceptionsRR.isStopValidateException = true;
        if(ExceptionsCreated.size()>0){
           insert ExceptionsCreated;  
        }   
   }
   
   public static void CheckUpdateExceptions(List<Registration_Request__c> RRlist , Map<Id,Registration_Request__c> oldmap){
        Map<string,List<Exception__c>> ExceptionsMap = new Map<string,List<Exception__c>>();
        for(Exception__c Exceptions :[SELECT id,name,Exception_Message__c , Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c, Registration_Request__c,Registration_Request__r.id FROM Exception__c 
                                            WHERE Registration_Request__r.Id=: oldmap.keyset() AND Disposition__c  != 'Resolved' ]){
                List<Exception__c> lsExceptions = ExceptionsMap.get(Exceptions.Registration_Request__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c);
                if(lsExceptions  == NULL)
                    lsExceptions  = new List<Exception__c>();
                lsExceptions.add(Exceptions );
                ExceptionsMap.put(Exceptions.Registration_Request__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c,lsExceptions);        
        }
        
         List<Exception__c> ExceptionstoUpdate = new List<Exception__c>();
         Set<Id> RRId = new Set<Id>();
         for(Registration_Request__c RR : RRlist){
              RRId.add(RR.Id);        
         }
         
         for(Registration_Request__c rr : RRlist){
             
                String keyValue = rr.id + '-ETN-0000001'; //HIC Upload
                if(ExceptionsMap.containskey(keyValue)){
                     if(rr.Trade_Ally_Type__c != null && rr.HIC_Attached__c== true){
                        for(Exception__c exp : ExceptionsMap.get(keyValue)){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000002')){  //W-9 Upload
                     if(rr.W_9_Attached__c== true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000002')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000003')){ //BPI Gold Star Upload
                     if(rr.Trade_Ally_Type__c != null && rr.BPI_Gold_Star_Attached__c== true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000003')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000004')){ //Liability $1M/$2M
                     if(rr.Trade_Ally_Type__c != null && rr.Liability_1M_2M_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000004')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000005')){ //Lead Safe
                     if(rr.Trade_Ally_Type__c != null && rr.Lead_Safe_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000005')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000006')){ //Auto $1M
                     if(rr.Trade_Ally_Type__c != null && rr.Auto_1M_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000006')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000007')){ //Excess Liability $1M/$2M
                     if(rr.Trade_Ally_Type__c != null && rr.Excess_Liability_1M_2M_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000007')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000008')){ //Workers Comp $500K
                     if(rr.Trade_Ally_Type__c != null && rr.Workers_Comp_500K_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000008')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000009')){ //Logo for Online Profile
                     if(rr.Trade_Ally_Type__c != null && rr.Logo_for_Online_Profile_Attached__c== true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000009')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-ETN-0000010')){ //Accurate Summary background check
                     if(rr.Trade_Ally_Type__c != null && rr.Accurate_Summ_Backend_Check_Attached__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-ETN-0000010')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
         }
         
          dsmtValidateExceptionsRR.isStopValidateException = true;
            system.debug('--ExceptionstoUpdate--'+ExceptionstoUpdate);
            if(ExceptionstoUpdate.size()>0){
                update ExceptionstoUpdate;
            }
   }
}