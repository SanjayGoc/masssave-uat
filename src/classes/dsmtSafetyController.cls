public class dsmtSafetyController extends dsmtEnergyAssessmentBaseController {
    public Safety_Aspect__c safetyAspect{get;set;}
    public Safety_Aspect_Item__c newSafetyAspectItem{get;set;}
    
    public Safety_Test__c newSafetyTest{get;set;}
    public String newSafetyTestFieldsetName{get;set;}
    public SMAModel smaModel{get;private set;} // DSST-11577 | Kaushik Rathore | 19 Sept, 2018
    
    public String viewName{get;set;}
    public static final Id CAZ_TEST_RECORD_TYPE_ID = Schema.SObjectType.Safety_Test__c.getRecordTypeInfosByName().get('CAZ Test').getRecordTypeId();
    public static final Id COMBUSTION_TEST_RECORD_TYPE_ID = Schema.SObjectType.Safety_Test__c.getRecordTypeInfosByName().get('Combustion Test').getRecordTypeId();
    public static final Id APPLIANCE_TEST_RECORD_TYPE_ID = Schema.SObjectType.Safety_Test__c.getRecordTypeInfosByName().get('Appliance Test').getRecordTypeId();
    public static final Id COMBUSTION_DEVICE_TEST_RECORD_TYPE_ID = Schema.SObjectType.Safety_Test__c.getRecordTypeInfosByName().get('Combustion Test Device').getRecordTypeId();
    public Id combustionTestRecordTypeId{get{return COMBUSTION_TEST_RECORD_TYPE_ID;}}
    public Id applianceTestRecordTypeId{get{return APPLIANCE_TEST_RECORD_TYPE_ID;}}
    public Id CAZTestRecordTypeId{get{return CAZ_TEST_RECORD_TYPE_ID;}}
    public Id CombustionDeviceTestRecordTypeId{get{return COMBUSTION_DEVICE_TEST_RECORD_TYPE_ID;}}
    
    public Pagereference validateRequestAndSafetyAspect()
    {
        PageReference pg = validateRequest();
        if(pg == null){
            if(ea.Safety_Aspect__c == null){
                safetyAspect = new Safety_Aspect__c();
                INSERT safetyAspect;
                if(safetyAspect.Id != null){
                    ea.Safety_Aspect__c = safetyAspect.Id;    
                    UPDATE ea;
                }
            }
            Id safetyAspectId = ea.Safety_Aspect__c;
            List<Safety_Aspect__c> safetyAspects = Database.query('SELECT ' + getSObjectFields('Safety_Aspect__c') + ' FROM Safety_Aspect__c WHERE Id=:safetyAspectId');
            if(safetyAspects.size() > 0){
                safetyAspect = safetyAspects.get(0);
            }
        }
        return pg;
    }
    
    public dsmtSafetyController()
    {
        queryAsbestosLocations();
    }
    
    public void addAsbestosLocation()
    {
        if(safetyAspect != null && safetyAspect.Id != null){
            Asbestos_Location__c newLocation = new Asbestos_Location__c(
                Safety_Aspect__c = safetyAspect.Id
            );
            INSERT newLocation;
            if(asbestosLocations == null){
                asbestosLocations = new List<Asbestos_Location__c>();
            }
            asbestosLocations.add(newLocation);
        }
    }
    
    private List<Asbestos_Location__c> queryAsbestosLocations()
    {
        asbestosLocations = new List<Asbestos_Location__c>();
        if(safetyAspect != null && safetyAspect.Id != null){
            Id safetyAspectId = safetyAspect.Id;
            asbestosLocations = Database.query('SELECT ' + getSObjectFields('Asbestos_Location__c') + ' FROM Asbestos_Location__c WHERE Safety_Aspect__c=:safetyAspectId');
        //}else{
        //    asbestosLocations.add(new Asbestos_Location__c());
        }
        return asbestosLocations;
    }
    
    public void deleteAsbestosLocation()
    {
        String locationIndexString = getParam('locationIndex');
        if(locationIndexString != null){
            Integer locationIndex = Integer.valueOf(locationIndexString);
            Asbestos_Location__c location = asbestosLocations.get(locationIndex);
            DELETE new Asbestos_Location__c(Id = location.Id);
            asbestosLocations.remove(locationIndex);
        }
    }
    
    public List<Asbestos_Location__c> asbestosLocations{get;set;}
    
    public void openView()
    {
        viewName = getParam('viewName');
        UPDATE this.safetyAspect;
        /** START : DSST-11578 | Kaushik Rathore | 15 Sept, 2018 **/  
        Id safetyAspectId = ea.Safety_Aspect__c;
        List<Safety_Aspect__c> safetyAspects = Database.query('SELECT ' + getSObjectFields('Safety_Aspect__c') + ' FROM Safety_Aspect__c WHERE Id=:safetyAspectId');
        if(safetyAspects.size() > 0){
            safetyAspect = safetyAspects.get(0);
            smaModel = new SMAModel(safetyAspectId); // DSST-11577 | Kaushik Rathore | 19 Sept, 2018
        }
        /** END : DSST-11578 | Kaushik Rathore | 15 Sept, 2018 **/  
        if(viewName.equalsIgnoreCase('SafetyAspectGeneralPanel')){
            queryAsbestosLocations();
        }else if(viewName.equalsIgnoreCase('safetyAspectItemPanel')){
            String saiId = getParam('sObjectId');
            List<Safety_Aspect_Item__c> locations = Database.query('SELECT ' + getSObjectFields('Safety_Aspect_Item__c') + ' FROM Safety_Aspect_Item__c WHERE Id=:saiId');
            if(locations.size() > 0){
                newSafetyAspectItem = locations.get(0); 
            }
        }
    }
    
    public void saveSafetyAspect()
    {
        if(safetyAspect != null){
            if(safetyAspect.Gas_Leaks__c != null && safetyAspect.Gas_Leaks__c == false){
                safetyAspect.Gas_Leaks_Significant__c = false;
                safetyAspect.Gas_Leaks_Description__c = null;
            }
            if(safetyAspect.High_CO_present__c == false){
                safetyAspect.Combustion_Safety_Fail_Identified_in__c = null;
                safetyAspect.Heating_System_Existing_CO_ppm__c = null;
                safetyAspect.Heating_System_Revised_CO_ppm__c = null;
                safetyAspect.Heating_System_Existing_Draft_Pa__c = null;
                safetyAspect.Hot_Water_Heater_Existing_CO_ppm__c = null;
                safetyAspect.Hot_Water_Heater_Existing_Draft_Pa__c = null;
                safetyAspect.MSB_Other_location_1__c = null;
                safetyAspect.Other_Existing_CO_ppm__c = null;
                safetyAspect.Other_Existing_Draft_Pa__c = null;
            }
            if(safetyAspect.Knob_Tube_Wiring_Present__c == false){
                safetyAspect.Knob_Tube_Wiring_Addressed__c = false;
                safetyAspect.KTW_Electrician_Letter_on_File__c = false;
                safetyAspect.Knob_and_Tube_identified_in__c = null;
                safetyAspect.Knob_Tube_Wiring_Other_location_1__c = null;
                safetyAspect.Knob_Tube_Wiring_Other_location_2__c = null;
            }
            if(asbestosLocations != null && asbestosLocations.size() > 0){
                UPSERT asbestosLocations;
            }
            // DSST-11577 | Kaushik Rathore | 19 Sept, 2018 : START
            if(smaModel != null){
                smaModel.save(false);
                smaModel = null;
            }
            // DSST-11577 | Kaushik Rathore | 19 Sept, 2018 : END
            UPDATE this.safetyAspect;
            viewName = 'listPanel';
        }
    }
    
    public void saveSafetyAspectItem()
    {
        if(newSafetyAspectItem != null){
            UPSERT this.newSafetyAspectItem;
            viewName = 'listPanel';
        }
    }
    
    public void deleteSafetyAspectItem()
    {
        String safetyAspectItemId = getParam('safetyAspectItemId'); 
        if(safetyAspectItemId != null){
            DELETE new Safety_Aspect_Item__c(Id = safetyAspectItemId);
        }
    }
    
    public void addNewSafetyAspectItem()
    {
        Integer nextNumber = getSafetyAspectItems().size() + 1;
        this.newSafetyAspectItem = new Safety_Aspect_Item__c(
            Safety_Aspect__c = safetyAspect.Id,
            Name = 'Safety Aspect ' + nextNumber
        );
        INSERT this.newSafetyAspectItem;
        if(this.newSafetyAspectItem.Id != null){
            viewName = 'SafetyAspectItemPanel';
        }
    }
    
    public List<Safety_Aspect_Item__c> getSafetyAspectItems()
    {
        List<Safety_Aspect_Item__c> items = new List<Safety_Aspect_Item__c>();
        if(this.safetyAspect != null && this.safetyAspect.Id != null){
            String safetyAspectId = safetyAspect.Id;
            return Database.query('SELECT ' + getSObjectFields('Safety_Aspect_Item__c') + ' FROM Safety_Aspect_Item__c WHERE Safety_Aspect__c=:safetyAspectId');
        }
        return items;
    }
    
    public List<Combustion_Device__c> getCombustionDevices()
    {
        List<Combustion_Device__c> devices = new List<Combustion_Device__c>();
        if(newSafetyAspectItem != null && newSafetyAspectItem.Id != null){
            String safetyAspectItemId = newSafetyAspectItem.Id;   
            devices = Database.query('SELECT ' + getSObjectFields('Combustion_Device__c') + ' FROM Combustion_Device__c WHERE Safety_Aspect_Item__c=:safetyAspectItemId');
        }
        return devices;
    }
    
    public List<Safety_Test__c> getCAZTests()
    {
        List<Safety_Test__c> tests = new List<Safety_Test__c>();
        if(newSafetyAspectItem != null && newSafetyAspectItem.Id != null){
            String safetyAspectItemId = newSafetyAspectItem.Id;
            tests = Database.query('SELECT ' + getSObjectFields('Safety_Test__c') + ' FROM Safety_Test__c WHERE Safety_Aspect_Item__c =:safetyAspectItemId AND RecordTypeId=:CAZ_TEST_RECORD_TYPE_ID');
        }
        return tests;
    }
    
    public List<Safety_Test__c> getCombustionTests()
    {
        List<Safety_Test__c> tests = new List<Safety_Test__c>();
        if(newSafetyAspectItem != null && newSafetyAspectItem.Id != null){
            String safetyAspectItemId = newSafetyAspectItem.Id;
            tests = Database.query('SELECT ' + getSObjectFields('Safety_Test__c') + ' FROM Safety_Test__c WHERE Safety_Aspect_Item__c =:safetyAspectItemId AND RecordTypeId=:COMBUSTION_TEST_RECORD_TYPE_ID');
        }
        return tests;
    }
    
    public List<Safety_Test__c> getApplianceTests()
    {
        List<Safety_Test__c> tests = new List<Safety_Test__c>();
        if(newSafetyAspectItem != null && newSafetyAspectItem.Id != null){
            String safetyAspectItemId = newSafetyAspectItem.Id;
            tests = Database.query('SELECT ' + getSObjectFields('Safety_Test__c') + ' FROM Safety_Test__c WHERE Safety_Aspect_Item__c =:safetyAspectItemId AND RecordTypeId=:APPLIANCE_TEST_RECORD_TYPE_ID');
        }
        return tests;
    }
    
    public void addNewSafetyTest()
    {
        String safetyTestId = getParam('safetyTestId');
        String recordTypeId = getParam('recordTypeId');

        if((safetyTestId == null || safetyTestId.trim() == '') && recordTypeId != null){
            viewName = 'SafetyTestPanel';
            String newName = newSafetyTestFieldsetName;
            newSafetyTest = new Safety_Test__c(
                RecordTypeId = recordTypeId,
                Safety_Aspect__c = safetyAspect.Id,
                Safety_Aspect_Item__c = newSafetyAspectItem.Id,
                Name = 'New ' + newName.replace('_',' ')
            );
            INSERT newSafetyTest;
            // ADD NEW TEST
        }else if(safetyTestId != null && safetyTestId.trim() != ''){
            List<Safety_Test__c> tests = Database.query('SELECT ' + getSObjectFields('Safety_Test__c') + ' FROM Safety_Test__c WHERE Id =:safetyTestId');
            if(tests.size() > 0 ){
                newSafetyTest = tests.get(0);
                viewName = 'SafetyTestPanel';   
            }
            // EDIT
        }else{
            newSafetyTest = null;
        }
        queryCombustionDeviceTests();
    }
    
    public void deleteSafetyTest()
    {
        String safetyTestId = getParam('safetyTestId');
        if(safetyTestId != null){
            DELETE new Safety_Test__c(Id = safetyTestId);
        }
    }
    
    public void queryCombustionDeviceTests()
    {
        if(newSafetyTest != null && newSafetyTest.Id != null){
            Id combustionTestId = newSafetyTest.Id;
            combustionDeviceTests = Database.query('SELECT ' + getSObjectFields('Safety_Test__c') + ' FROM Safety_Test__c WHERE Combustion_Test__c=:combustionTestId AND RecordTypeId=:COMBUSTION_DEVICE_TEST_RECORD_TYPE_ID');
        }
    }
    
    public List<Safety_Test__c> combustionDeviceTests{get;set;}
    
    public void saveSafetyTest()
    {
        if(newSafetyTest != null){
            UPSERT newSafetyTest;
            if(combustionDeviceTests != null && combustionDeviceTests.size() > 0){
                Integer testIndex = 0;
                String testType = 'Combustion Test';
                for(Safety_Test__c test : combustionDeviceTests){
                    testIndex++;
                    test.Name = testType + ' ' + testIndex;
                    test.Safety_Aspect__c = safetyAspect.Id;                    
                }
                UPSERT combustionDeviceTests;
            }
            viewName = 'SafetyAspectItemPanel';
        }
    }
    
    public void addNewCombustionDeviceTest()
    {
        if(combustionDeviceTests != null)
        {
            Safety_Test__c combustionDeviceTest = new Safety_Test__c(
                RecordTypeId = COMBUSTION_DEVICE_TEST_RECORD_TYPE_ID,
                Combustion_Test__c = newSafetyTest.Id,
                Safety_Aspect_Item__c = newSafetyAspectItem.Id,
                Safety_Aspect__c = safetyAspect.Id
            );
            INSERT combustionDeviceTest;
            combustionDeviceTests.add(combustionDeviceTest);
        }
    }
    
    public void removeCombustionDeviceTest()
    {
        String testIndex = getParam('testIndex');
        if(testIndex != null && testIndex != '')
        {
            Safety_Test__c test = combustionDeviceTests.get(Integer.valueOf(testIndex));
            if(test != null && test.Id != null){
                DELETE new Safety_Test__c(Id = test.Id);
            }
            combustionDeviceTests.remove(Integer.valueOf(testIndex));
        }
    }
    // DSST-11577 | Kaushik Rathore | 19 Sept, 2018 : START
    public class SMAModel{
        public Id safetyAspectId{get;private set;}
        public List<SMA_Record__c> smaRecords{get;set;}
        public List<SMA_Record__c> smaRecordsToDelete{get;set;}
        public SMAModel(Id safetyAspectId){
            this.safetyAspectId = safetyAspectId;
            initialize();
        }
        private void initialize(){
            smaRecords = new List<SMA_Record__c>();
            smaRecordsToDelete = new List<SMA_Record__c>();
            if(safetyAspectId != null){
                for(SMA_Record__c sma :[
                    SELECT Id,Name,Type__c,Value__c,SMA_Notes__c,Other_SMA__c,Safety_Aspect__c 
                    FROM SMA_Record__c 
                    WHERE Safety_Aspect__c =:safetyAspectId 
                    ORDER BY CreatedDate
                    LIMIT 3
                ]){
                    smaRecords.add(sma);
                }
            }
        }
        public void addNewSMARecord(){
            smaRecords.add(new SMA_Record__c(
                Safety_Aspect__c = safetyAspectId
            ));
        }
        public void removeSMARecord(){
            String smaIndexString = ApexPages.currentPage().getParameters().get('smaIndex');
            if(smaIndexString != null && smaIndexString.trim() != ''){
                Integer smaIndex = Integer.valueOf(smaIndexString);
                SMA_Record__c smaRecordToDelete = smaRecords.get(smaIndex);
                if(smaRecordToDelete != null && smaRecordToDelete.Id != null){
                    smaRecordsToDelete.add(new SMA_Record__c(
                        Id = smaRecordToDelete.Id
                    ));
                }
                smaRecords.remove(smaIndex);
            }
        }
        public void save(boolean reInitialize){
            if(smaRecordsToDelete != null && smaRecordsToDelete.size() > 0){
                DELETE smaRecordsToDelete;
            }
            if(smaRecords != null && smaRecords.size() > 0){
                UPSERT smaRecords;
            }

            if(reInitialize)
                initialize();
        }
    }
    // DSST-11577 | Kaushik Rathore | 19 Sept, 2018 : END
}