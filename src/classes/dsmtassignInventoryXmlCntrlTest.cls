@isTest
Public class dsmtassignInventoryXmlCntrlTest{
    @isTest
    public static void runTest(){
        
        Location__c  loc = Datagenerator.createLocation();
        insert loc;
        
        Region__c  reg = Datagenerator.CreateRegion(loc.Id);
        insert reg;
        
        Employee__c emp = Datagenerator.createEmployee(loc.Id);
        emp.Employee_Id__c = 'test123';
        insert emp;
        
        Warehouse__c ware = Datagenerator.createWarehouse();
        ware.Location__c = loc.Id;
        insert ware;
        
        Product__c prod = Datagenerator.createProduct();
        insert prod;
        
        Inventory_Item__c inv = new Inventory_Item__c();
        inv.Warehouse__c = ware.Id;
        inv.status__c = 'Available';
        inv.Product__c = prod.Id;
        inv.Employee__c = emp.Id;
        insert inv;
        
        ApexPages.currentPage().getParameters().put('type','availabledetail');
        ApexPages.currentPage().getParameters().put('warehouse',ware.Id);
        ApexPages.currentPage().getParameters().put('product',prod.Id);
        ApexPages.currentPage().getParameters().put('location',loc.Id);
          
        dsmtassignInventoryXmlCntrl cntrl =new dsmtassignInventoryXmlCntrl();
        
        ApexPages.currentPage().getParameters().put('type','assigneddetail');
        cntrl =new dsmtassignInventoryXmlCntrl();
        
        ApexPages.currentPage().getParameters().put('type','availablesummary');
        cntrl =new dsmtassignInventoryXmlCntrl();
        
        ApexPages.currentPage().getParameters().put('type','assignedsummary');
        cntrl =new dsmtassignInventoryXmlCntrl();
        
        
    }
}