public class WallTriggerHandler extends TriggerHandler implements ITrigger{
    
    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            calculateDefaultValues(trigger.new, (Map<Id, Wall__c>) trigger.oldmap, trigger.isInsert);
            //DSST-6924: Changes made by DeveloperName – Begin
            dsmtRecursiveTriggerHandler.preventWallUpdateAfterInsert = true;
        }
        else if (trigger.isUpdate && dsmtRecursiveTriggerHandler.preventWallUpdateAfterInsert == false) {
            //DSST-6924: Changes made by DeveloperName – End
            //Here we will call before update actions
            calculateDefaultValues(trigger.new, (Map<Id, Wall__c>) trigger.oldmap, trigger.isInsert);
        } else if (trigger.isDelete) {
            //Here we will call before delete actions
            deleteLayers(trigger.oldmap.keyset());
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) 
        {
            CreateDefaultLayer(trigger.new);
        } else if (trigger.isUpdate) {
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }

    private static String sObjectFields(String sObjName) 
    {
        String fields = '';
        map<String, Schema.SObjectField > fieldsMap = Schema.getGlobalDescribe()
            .get(sObjName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield: fieldsMap.Values()) {
            schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.getName() + '' != 'Id')
                fields += dfield.getName() + ', ';
        }
        return fields;
    }
    
    private static void calculateDefaultValues(list<Wall__c> newList, Map<Id, Wall__c> oldMap, boolean isInsert)
    {
        try{
     
            Set<Id> setwallId = new Set<Id>();
            Set<Id> EaId = new Set<Id>();
            Set<Id> setTETId = new Set<Id>();

            for(wall__c te : newList)
            {
                if(te.Id!=null)
                    setwallId.add(te.Id);

                if(te.Thermal_Envelope_Type__c !=null)
                    setTETId.add(te.Thermal_Envelope_Type__c);
            }

            if(setTETId ==null || setTETId.size() ==0) return;

            Map<Id,Thermal_Envelope_Type__c> tetMap = new Map<Id,Thermal_Envelope_Type__c> ([Select Id, Energy_Assessment__c From 
            Thermal_Envelope_Type__c Where Id in :setTETId]);

            if(tetMap ==null || tetMap.size() ==0) return;

            For(Id key : setTETId)
            {
                if(tetMap.get(key)==null)continue;
                if(tetMap.get(key).Energy_Assessment__c ==null)continue;
                EaId.add(tetMap.get(key).Energy_Assessment__c);
            }

            dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(EaId);

            if(bp==null) return;
                
            
            if(bp.teList !=null && bp.teList.size() >0)
            {
                bp.AtticList  = new List<Thermal_Envelope_Type__c>();
                for(Thermal_Envelope_Type__c te : bp.teList)
                {   
                    if(bp.recordTypeMap.get(te.RecordTypeID) == 'Attic')
                    {
                        bp.AtticList.add(te);
                    }
                }    
            }

             // consutruct temprature and building model
            bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
            bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

            /// update base model first
           // bp = dsmtBuildingModelHelper.UpdateBaseBuildingThermal(bp);
            bp = dsmtBuildingModelHelper.UpdateThermalAreaModel(bp);
            bp.ai = dsmtBuildingModelHelper.UpdateAirFlowModel(bp.ai, bp).clone(true,true,true,true);

            bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
            bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

            integer wallCount = newList.size();
            if(isInsert)
            {
                if(bp.wallList ==null) bp.wallList = new List<Wall__c>();

                For(integer i=0 ;i< wallCount; i++)
                {
                    bp.wallList.add(newList[i]);
                }
            }
            else 
            {
                For(integer i=0 ;i< wallCount; i++)
                {
                    if(bp.wallList ==null) bp.wallList = new List<Wall__c>();

                    if(bp.wallList.size() ==0)
                    {
                        bp.wallList.add(newList[i]);
                    }
                    else 
                    {
                        boolean isFound=false;
                        for(integer j=0; j<bp.wallList.size();j++)
                        {
                            if(bp.wallList[j].Id==newList[i].Id)
                            {
                                bp.wallList[j] = newList[i];
                                isFound=true;
                                break;
                            }
                        }

                        if(!isFound)
                        {
                            bp.wallList.add(newList[i]);
                        }
                    }
                }    
            }

            tetMap = new Map<Id,Thermal_Envelope_Type__c>();

            if(bp.teList !=null && bp.teList.size()>0)
            {
                for(integer i=0; i<bp.teList.size();i++)
                {
                    tetMap.put(bp.teList[i].Id,bp.teList[i]);
                }
            }

            for(Wall__c tet: newList)
            {
                if(tet.IsFromFieldTool__c) continue;

                if(tet.Thermal_Envelope_Type__c ==null) continue;

                if(tetMap !=null && tetMap.size()>0)
                {
                    bp.teType = tetMap.get(tet.Thermal_Envelope_Type__c);
                }

                if(bp.teType ==null)continue;

                
                if(tet.Id !=null)
                {
                    if(bp.allLayerMap !=null && bp.allLayerMap.size()>0)
                    {
                        bp.layers = bp.allLayerMap.get(tet.Id);
                    }
                }

                bp.layers = dsmtBuildingModelHelper.UpdateLayerModel(bp.layers,bp);

                Id parentId = tet.Thermal_Envelope_Type__c;
                string recTypeName = bp.RecordTypeMap.get(bp.teType.RecordTypeId);
                    
                bp.wall = tet;

                ManageActuals(tet);

                tet.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);

                bp.Location = tet.Location__c;
                tet.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
                bp.LocationSpace = tet.LocationSpace__c;
                bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
                bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
                bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

                tet.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
                bp.LocationType = tet.LocationType__c;

                if(tet.Add__c =='Rim Joist')
                {
                    tet.isRim__c = true;
                }

                if(recTypeName == 'Basement' || recTypeName == 'Crawlspace')
                {
                   if(tet.Exposed_To__c == null || tet.Exposed_To__c == '') 
                   {
                       if(tet.isRim__c)
                       {
                           tet.Exposed_To__c = 'Exterior';
                       }
                       else if(tet.Type__c == 'Basement Exterior')
                       {
                            tet.Exposed_To__c = 'Exterior';
                       }
                       else
                       {
                           tet.Exposed_To__c = 'Ground';
                       }
                   }
                }
                else if(recTypeName == 'Attic')
                {
                    if(tet.Exposed_To__c == null || tet.Exposed_To__c == '') tet.Exposed_To__c = 'Exterior';
                }

                if(tet.Add__c==null || tet.Add__c == '') 
                {
                    if(recTypeName == 'Attic')
                    {
                        tet.Add__c = 'Unfinished Exterior';
                    }
                }
                
                if(bp.teType.ChangeType__c)
                {
                    isInsert = true;
                    tet.Name = '';
                }

                if(recTypeName == 'Basement' || recTypeName == 'Crawlspace')
                {
                    tet.Space_Type__c = dsmtSurfaceHelper.GetSurfaceSpaceType().get(recTypeName + ' ' + tet.Exposed_To__c);
                }
                else
                {
                    tet.Space_Type__c = dsmtSurfaceHelper.GetSurfaceSpaceType().get(recTypeName);
                }

                tet.Type__c = dsmtSurfaceHelper.ComputeWallTypeCommon(bp);

                if(tet.Type__c =='Basement Exterior')
                    tet.Name = tet.Name.Replace('Basement Wall', tet.Type__c + ' Wall');

                if(tet.ActualWidth__c ==null)
                {
                    tet.Width__c = dsmtSurfaceHelper.ComputeWallWidthCommon(bp);
                }
                else {
                    tet.Width__c = tet.ActualWidth__c;
                }

                if(tet.ActualHeight__c !=null)
                {
                    tet.Height__c = tet.ActualHeight__c /12;
                }
                else {
                    tet.Height__c = dsmtSurfaceHelper.ComputeWallHeightCommon(bp);
                    tet.HeightInch__c = dsmtEAModel.ISNULL(tet.Height__c) * 12;    
                }

                if(tet.Name == '' || tet.Name == null)
                {
                    tet.Name = tet.Type__c;
                    if(recTypeName == 'Crawlspace' && tet.Type__c == 'Basement Wall') tet.Name = 'Crawlspace Wall';
                    if(recTypeName == 'Crawlspace' && tet.Type__c == 'Basement Exterior') tet.Name = 'Crawlspace Exterior Wall';
                    if(recTypeName == 'Crawlspace' && tet.Type__c == 'Basement Rim Joist') tet.Name = 'Crawlspace Rim Joist';
                }

                // DSST-6348 - Cavity Insulation Depth error
                // #Code Block Start
                if(bp.layers !=null && bp.layers.size() > 0)
                {
                    Layer__c wfLayer = dsmtSurfaceHelper.GetFirstFramingLayer(bp.layers,1);
                    if(wfLayer !=null)
                    {
                        if(wfLayer.ActualCavityInsulationDepth__c !=null && wfLayer.ActualCavityInsulationDepth__c !=0)
                        {
                            tet.ActualInsulationAmount__c = null;
                        }
                    }
                }

                //system.debug('tet.ActualInsulationAmount__c>>>'+tet.ActualInsulationAmount__c);
                if(tet.ActualInsulationAmount__c == null)
                {
                    tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeWallInsulationAmountCommon(bp);
                }
                // #Code Block End

                if (isInsert)
                {
                    //tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeWallInsulationAmountCommon(bp);
                    tet.Gaps__c = SurfaceConstants.DefaultInsulationGaps;                                        
                    tet.Exposed_ft__c =1;
                }
                

                if(tet.ActualArea__c == null)
                {
                    tet.Area__c = dsmtSurfaceHelper.ComputeWallAreaCommon(bp);
                }

                if(tet.Gaps__c == null || tet.Gaps__c == '')
                {
                    tet.Gaps__c = SurfaceConstants.DefaultInsulationGaps; 
                }

                tet.Net_Area__c = dsmtSurfaceHelper.ComputeAreaNet(bp);
                tet.AreaBack__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Back');
                tet.AreaFront__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Front');
                tet.AreaLeft__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Left');
                tet.AreaRight__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Right');                
                tet.RValueConst__c = dsmtSurfaceHelper.ComputeRValueConst(bp);
                tet.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(bp);                
                tet.ExposedRValue__c = dsmtSurfaceHelper.ComputeWallExposedRValueCommon(bp);
                tet.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeWallEffectiveRValueCommon(bp);
                tet.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeWallNominalRValueCommon(bp);
                tet.UA__c = dsmtSurfaceHelper.ComputeWallUACommon(bp);                                
                tet.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(tet.R_Value__c);                

                tet.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'H');
                tet.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'C');
                tet.LoadH__c = dsmtSurfaceHelper.ComputeWallLoadCommon(bp, 'H');
                tet.LoadC__c = dsmtSurfaceHelper.ComputeWallLoadCommon(bp, 'C');
            }
        }
        catch(Exception ex)
        {
            dsmtHelperClass.WriteMessage(ex);
        }
    }
    
    public static void deleteLayers(Set<Id> wallIdSet)
    {
        List<Layer__c> layerList = [select id from Layer__c where Wall__c in : wallIdSet];
        
        if(layerList.size() > 0)
        {
            delete layerList;
        }
    }

    private static void ManageActuals(Wall__c tet)
    {
        if(tet.ActualArea__c <=0) tet.ActualArea__c=null;
        if(tet.ActualPerimeter__c <=0) tet.ActualPerimeter__c=null;
        if(tet.ActualWidth__c<=0)tet.ActualWidth__c=null;
        if(tet.ActualHeight__c<=0)tet.ActualHeight__c=null;
        if(tet.ActualInsulationAmount__c == '') tet.ActualInsulationAmount__c = null;
    }

    private static void CreateDefaultLayer(List<Wall__c> wallList)
    {
        Map<Id,string> recTypeMap = dsmtHelperClass.GetRecordType();

        dsmtEAModel.Surface bp = new dsmtEAModel.Surface();        
        bp.recordTypeMap = recTypeMap;
        
        Set<Id> EaId = new Set<Id>();
        Set<Id> setTETId = new Set<Id>();

        for(Wall__c te : wallList)
        {
            if(te.IsFromFieldTool__c) continue;

            if(te.Thermal_Envelope_Type__c !=null)
                setTETId.add(te.Thermal_Envelope_Type__c);
        }

        if(setTETId ==null || setTETId.size() ==0) return;

        String query = 'select ' + sObjectFields('Thermal_Envelope_Type__c') +' Id from Thermal_Envelope_Type__c where Id In :setTETId order by CreatedDate Asc ';

        Map<Id,Thermal_Envelope_Type__c> tetMap = new Map<Id,Thermal_Envelope_Type__c> ((List<Thermal_Envelope_Type__c>)database.query(query));
        
        if(tetMap ==null || tetMap.size()==0)return;

        for(Wall__c te : wallList)
        {
            if(te.IsFromFieldTool__c) continue;
            if(te.Thermal_Envelope_Type__c ==null) continue;

            bp.teType = tetMap.get(te.Thermal_Envelope_Type__c);
            bp.wall = te;

            dsmtSurfaceHelper.CreateNewLayers(bp);
        }        

    }
}