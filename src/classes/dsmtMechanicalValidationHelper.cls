public class dsmtMechanicalValidationHelper extends dsmtEnergyAssessmentValidationHelper{
	public ValidationCategory validate(String categoryTitle,String uniqueName,Id eaId,ValidationCategory otherValidations){
		ValidationCategory validations= new ValidationCategory(categoryTitle,uniqueName);

		if(eaId != null){

			Map<String,Integer> primaryCount = new Map<String,Integer>();

			Map<String,List<Mechanical_Sub_Type__c>> mstMap = new Map<String,List<Mechanical_Sub_Type__c>>();
			for(Mechanical_Sub_Type__c mst : Database.query('SELECT ' + getSObjectFields('Mechanical_Sub_Type__c') + ',RecordType.Name,Mechanical__r.Name FROM Mechanical_Sub_Type__c WHERE Energy_Assessment__c =:eaId')){
				List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();
				if(mstMap.containsKey(mst.RecordType.Name)){
					mstList = mstMap.get(mst.RecordType.Name);
				}
				mstList.add(mst);
				mstMap.put(mst.RecordType.Name, mstList);

				if(mst.Mechanical__c != null && mst.Mechanical__r.Name != null){
					Integer count = 0;
					if(primaryCount.containsKey(mst.Mechanical__r.Name)){
						count = primaryCount.get(mst.Mechanical__r.Name);
					}
					if(mst.IsPrimary__c == true || mst.Primary_DHW__c == true){
						count++;
					}
					primaryCount.put(mst.Mechanical__r.Name,count);
				} 
			}
			Integer i = 0;
			for(String mechanicalName : primaryCount.keySet()){
				Integer count = primaryCount.get(mechanicalName);
				if(count == 0){
					ValidationCategory mechanicalValidations = new ValidationCategory(mechanicalName,'Mechanical_'+i);
					mechanicalValidations.errors.add(new ValidationMessage('Please specify primary system.'));
					validations.addCategory(mechanicalValidations);
				}
			}
			for(String mstRecordTypeName : mstMap.keySet()){
				List<Mechanical_Sub_Type__c> mstList = mstMap.get(mstRecordTypeName);
				for(Mechanical_Sub_Type__c mst : mstList){
					if(mst.HeatingSystemType__c != null && mst.HeatingSystemType__c.trim() != ''){
						ValidationCategory heatingSystemValidations = new ValidationCategory(mst.Name,mst.Id);
						validateHeatingSystem(heatingSystemValidations,mst,otherValidations);
						validations.addCategory(heatingSystemValidations);
					}else if(mst.CoolingSystemType__c != null && mst.CoolingSystemType__c.trim() != ''){
						ValidationCategory coolingSystemValidations = new ValidationCategory(mst.Name,mst.Id);
						validateCoolingSystem(coolingSystemValidations,mst,otherValidations);
						validations.addCategory(coolingSystemValidations);
					}else if(mst.DHWType__c != null && mst.DHWType__c.trim() != ''){
						ValidationCategory dhwSystemValidations = new ValidationCategory(mst.Name,mst.Id);
						validateDHWSystem(dhwSystemValidations,mst,otherValidations);
						validations.addCategory(dhwSystemValidations);
					}
				}
			}
		}

		return validations;
	}

	private void validateHeatingSystem(ValidationCategory validations,Mechanical_Sub_Type__c hs,ValidationCategory otherValidations){
		if(hs.Fuel_Type__c == null || String.valueOf(hs.Fuel_Type__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Please Specify heating system Fuel Type.'));
		}
		if(hs.Year__c == null || String.valueOf(hs.Year__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Please Specify heating system Year.'));
		}else if(hs.Year__c != null && String.valueOf(hs.Year__c).trim().length() != 4){
			validations.errors.add(new ValidationMessage('Please Specify valid Year.'));
		}else if(hs.Year__c > System.today().year() + 1){
			validations.errors.add(new ValidationMessage('Year must me less than or equal to ' + (System.today().year() + 1)));
		}
	}
	private void validateCoolingSystem(ValidationCategory validations,Mechanical_Sub_Type__c cs,ValidationCategory otherValidations){
		if(cs.Year__c == null || String.valueOf(cs.Year__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Please Specify heating system Year.'));
		}else if(cs.Year__c != null && String.valueOf(cs.Year__c).trim().length() != 4){
			validations.errors.add(new ValidationMessage('Please Specify valid Year.'));
		}else if(cs.Year__c > System.today().year() + 1){
			validations.errors.add(new ValidationMessage('Year must me less than or equal to ' + (System.today().year() + 1)));
		}
	}
	private void validateDHWSystem(ValidationCategory validations,Mechanical_Sub_Type__c dhw,ValidationCategory otherValidations){
		if(dhw.Fuel_Type__c == null || String.valueOf(dhw.Fuel_Type__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Please Specify water heater Fuel Type.'));
		}
		if(dhw.Year__c == null || String.valueOf(dhw.Year__c).trim() == ''){
			validations.errors.add(new ValidationMessage('Please Specify DHW Year.'));
		}else if(dhw.Year__c != null && String.valueOf(dhw.Year__c).trim().length() != 4){
			validations.errors.add(new ValidationMessage('Please Specify valid Year.'));
		}else if(dhw.Year__c > System.today().year() + 1){
			validations.errors.add(new ValidationMessage('Year must me less than or equal to ' + (System.today().year() + 1)));
		}
	}
}