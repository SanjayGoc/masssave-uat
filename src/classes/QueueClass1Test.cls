@isTest
public class QueueClass1Test {
   public static testMethod void firstMethod(){
    
       list<Available_Slot__c> avs = new list<Available_Slot__c>();
       Available_Slot__c av = new Available_Slot__c();
       av.End_Time__c = datetime.newInstance(2015, 9, 15, 12, 30, 0);
       av.Start_Time__c =  datetime.newInstance(2014, 9, 15, 12, 30, 0); 
       avs.add(av);
       insert avs;
        
        Location__c Lc = new Location__c();
        Lc.name = 'test';
        insert Lc; 
       
        Employee__c Em = new Employee__c( location__c = Lc.Id);
        Em.Employee_Id__c = 'Test';
        Em.name = 'Test';
        Em.Active__c = true;
        Em.Status__c ='Active'; 
        insert Em;
        
        Employee__c Em1 = new Employee__c( location__c = Lc.Id);
        Em1.Employee_Id__c = 'Test1';
        Em1.name = 'Test1';
        Em1.Active__c = true;
        Em1.Status__c ='Active'; 
        insert Em1;
        
        Work_Team__c Wt = new Work_Team__c();
        Wt.name = 'Test';
        Wt.Captain__c = Em.Id;
        Wt.Unavailable__c = false;
        wt.Service_Date__c = DAte.Today();
        wt.Start_Date__c = datetime.newInstance(2013, 8, 14, 12, 30, 0);
        wt.End_Date__c = datetime.newInstance(2016, 9, 15, 12, 30, 0);
        insert Wt;
        
        Work_Team_Member__c Wtm = new Work_Team_Member__c();
        Wtm.Work_Team__c = Wt.Id;
        Wtm.Employee__c = Em1.Id;
        insert Wtm;
       
       Workorder_Type__c wty = new Workorder_Type__c();
       wty.Name = 'test';
       insert wty;
       
       Skill__c sk = new Skill__c();
       sk.name='test';
       insert sk;
           
       Required_Skill__c rs = new Required_Skill__c();
       rs.Workorder_Type__c = wty.Id;
       rs.Skill__c = sk.Id;
       rs.Minimum_Score__c = 10;
       insert rs;
       
       Employee_Skill__c es = new Employee_Skill__c();
       es.Skill__c = sk.Id;
       es.Survey_Score__c = 10 ;
       insert es;
       
      QueueClass1 Qc = new QueueClass1(avs);
       System.enqueueJob(Qc);
      
  }
}