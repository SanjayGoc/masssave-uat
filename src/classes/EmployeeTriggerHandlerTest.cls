@isTest public class EmployeeTriggerHandlerTest {
    @isTest static void coverInsert(){
        Location__c location = new Location__c();
        insert location;
        Test.startTest();
        insert new Employee__c(
            Location__c = location.Id,
            Employee_Id__c = '09875',
            Name = 'Test Employee',
            Status__c = 'Active'
        );
        Test.stopTest();
    }
    
    @isTest static void coverUpdate(){
        Location__c location = new Location__c();
        insert location;
        Employee__c employee = new Employee__c(
            Location__c = location.Id,
            Employee_Id__c = '09875',
            Name = 'Test Employee',
            Status__c = 'Active'
        );
        insert employee;
        Schedules__c schedule = new Schedules__c();
        insert schedule;
        Work_Team__c workteam = [SELECT Id FROM Work_Team__c LIMIT 1];
        Workorder__c workorder = new Workorder__c(
            Work_Team__c = workteam.Id,
            Status__c = 'Scheduled',
            Early_Arrival_Time__c = DateTime.newInstance(1700, 1, 1),
            Late_Arrival_Time__c = DateTime.newInstance(1700, 1, 1)
        );
        insert workorder;
        employee.Schedule__c = schedule.Id;
        Test.startTest();
        update employee; 
        Test.stopTest();
    }
}