public with sharing class BasementFloorComponentCntrl{
    public Id basementFloorId{get;set;}
    public String newLayerType{get;set;}
    public Floor__c basementFloor{get{
        if(basementFloor == null){
            List<Floor__c> floors = Database.query('SELECT ' + getSObjectFields('Floor__c') + ',Thermal_Envelope_Type__r.SpaceConditioning__c FROM Floor__c WHERE Id=:basementFloorId'); 
            if(floors.size() > 0){
                basementFloor = floors.get(0);
            }
        }
        return basementFloor;
    }set;}
    public void saveFloor(){
        saveFloorLayers();
        if(basementFloor != null && basementFloor.Id != null){
            basementFloor.Exposed_To__c = basementFloor.Floor_To__c;
            UPDATE basementFloor;
            basementFloor = null;
        }
    }
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    public List<Layer__c> floorLayers{get;set;}
    public Map<Id,Layer__c> getFloorLayersMap(){
        Id floorId = basementFloor.Id;
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Floor__c =:floorId AND HideLayer__c = false';
        floorLayers = Database.query(query);
        return new Map<Id,Layer__c>(floorLayers);
    }
    public void saveFloorLayers(){
        if(floorLayers != null && floorLayers.size() > 0){
            UPSERT floorLayers;
        }
    }
    public void addNewFloorLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Floor__c =:basementFloor.Id AND RecordTypeId =:recordTypeId];
        if(layerType != null && m.containsKey(layerType)){
            Decimal nextNumber= layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Floor__c = basementFloor.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            if(newLayer.Id != null){
                basementFloor.Next_Basement_Floor_Layer_Number__c = nextNumber + 1;
                UPDATE basementFloor;
            }
            if(floorLayers == null){
                floorLayers = new List<Layer__c>();
            }
            floorLayers.add(newLayer);
        }
    }
    public void deleteFloorLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = floorLayers.get(index).Id);
        floorLayers.remove(index);
        saveFloor();
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}