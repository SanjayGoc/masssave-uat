@istest
public class BasementCeilingComponentCntrlTest
{
    @istest
    static void runTest()
    {
        
        Ceiling__c cl =new Ceiling__c();
        insert cl;
        
        Layer__c ly =new Layer__c();
        ly.Name='test';
        ly.HideLayer__c = false;
        ly.Ceiling__c=cl.id;
        insert ly;
        
        BasementCeilingComponentCntrl bccc = new BasementCeilingComponentCntrl ();
        String newLayer = bccc.newLayerType;
        bccc.basementCeilingId=cl.Id;
        bccc.getLayerTypeOptions();
        bccc.getCeilingLayersMap();
        bccc.saveCeiling();

        ApexPages.currentPage().getParameters().put('layerType','Flooring');
        bccc.addNewCeilingLayer();
        
        ApexPages.currentPage().getParameters().put('index','0');
        bccc.deleteCeilingLayer();
    }
}