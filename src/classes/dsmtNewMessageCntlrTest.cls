@isTest
public class dsmtNewMessageCntlrTest
{
    @istest
    public Static Void RunTest()
    {
        
         User u = Datagenerator.CreatePortalUser();
        
        system.runAs(u)
        {

            Registration_Request__c rr = new Registration_Request__c();
            insert rr;
            
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            insert ta;
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c= u.contactId;
            insert dsmtc;
            
            dsmtNewMessageCntlr dsmtmsg = new dsmtNewMessageCntlr();
                    
            Messages__c objMsg = new Messages__c(); 
               objMsg.From__c = u.id;
               objMsg.Message_Direction__c = 'Outbound';
               objMsg.To__c = ta.Owner.Email;
               objMsg.Trade_Ally_Account__c = ta.id;
               objMsg.Dsmtracker_Contact__c = dsmtc.Id;
               objMsg.Message_Type__c = 'Notification';
               objMsg.status__c = 'Sent';
            insert objMsg;
            
            Attachment__c atch = new Attachment__c();
            atch.Status__c='New';
            atch.Message__c=objMsg.id;
            insert atch;
            
            ApexPages.currentPage().getParameters().put('id', objMsg.id);
            ApexPages.currentPage().getParameters().put('RRId', rr.Registration_Number__c);
            
            dsmtmsg = new dsmtNewMessageCntlr();
            
            dsmtmsg.checkMessage();
            dsmtmsg.CancelAction();
            dsmtmsg.loadRegistrationRequest();
            dsmtmsg.SaveAsDraft();
            dsmtmsg.saveAndSubmit();
            
            dsmtNewMessageCntlr.delAttachmentsById(atch.id,objMsg.id);
            
            dsmtNewMessageCntlr.getAttachmentsByTicketId(atch.Id);
        
        }
        
    }
}