@isTest
Public class dsmtCallOutTest{
    @isTest
    public static void runTest(){
        Location__c loc = Datagenerator.createLocation();
        insert loc;
        
        Employee__c emp = Datagenerator.createEmployee(loc.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, emp.Id);
        insert wt;
        
        Territory__c t =  Datagenerator.createTerritory(null, emp.Id, wt.Id);
        t.Location__c = loc.Id;
        insert t;
        
        Workorder__c wo = new Workorder__c(Work_Team__c = wt.Id,
                                           Scheduled_Start_Date__c = system.now(),
                                           Scheduled_End_Date__c = system.now().addDays(1));
        insert wo;
        
        dsmtCallOut cntrl = new dsmtCallOut();
        dsmtCallOut.SyncWorkTeams(emp.Id);
        
        dsmtCallOut.SyncTerritory(loc.Id);
        
        set<id> woId = new set<Id>();
        woId.add(wo.Id);
        
        dsmtCallOut.CreateWorkOrder(woId);
        dsmtCallout.CancelWorkOrder(woId);
        dsmtCallOut.FixedWorkOrder(new map<id, boolean>{wo.Id => false});
    }
}