public class dsmtLocationScheduleController
{
    
    public List<SelectOption> dsmtScheduleOption{get;set;}
    public Location__c locObj;

    public dsmtLocationScheduleController(ApexPages.StandardController stdController) {
        this.locObj = (Location__c)stdController.getRecord();
        GetSchedules();
    }
    
    public void GetSchedules(){
        dsmtScheduleOption = new List<SelectOption>(); 
        dsmtScheduleOption.add(new SelectOption('', '--Select Schedule--'));   
        for(Schedules__c sc : [select id,name from schedules__c where location__c =: locObj.Id]){
            dsmtScheduleOption.add(new SelectOption(sc.Id, sc.Name));
        }   
    }

}