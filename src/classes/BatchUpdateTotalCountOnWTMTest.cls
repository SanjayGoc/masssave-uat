@isTest
public class BatchUpdateTotalCountOnWTMTest 
{
	static testMethod void FirstMethod() 
	{
		 Location__c Lo = new Location__c();
        	Lo.name='TestL';
        	insert Lo;
        	
        Employee__c ecc = new Employee__c(); 
            ecc.Name ='Name';
            ecc.Employee_Id__c='123';
        	ecc.Location__c=Lo.id;
           
            insert ecc;
        
            Work_Team__c Wte = new Work_Team__c();
            Wte.Name='NameWT';
        	Wte.Captain__c=ecc.Id;
        	
            
            insert Wte;
        
         Test.startTest();
            BatchUpdateTotalCountOnWTM obj1 = new BatchUpdateTotalCountOnWTM();
            DataBase.executeBatch(obj1);
        Test.stopTest();
	}
    static testMethod void FirstMethod1() 
	{
		    Location__c Lo = new Location__c();
        	Lo.name='TestL';
        	insert Lo;
        	
        Employee__c ecc = new Employee__c(); 
            ecc.Name ='Name';
            ecc.Employee_Id__c='123';
        	ecc.Location__c=Lo.id;
            insert ecc;
        
            Work_Team__c Wte = new Work_Team__c();
            Wte.Name='NameWT';
        	Wte.Unavailable__c = true;
        	Wte.Captain__c=ecc.Id;
        	insert Wte;
        
         Test.startTest();
            BatchUpdateTotalCountOnWTM obj1 = new BatchUpdateTotalCountOnWTM();
            DataBase.executeBatch(obj1);
        	obj1.execute(null);
        Test.stopTest();
	}
}