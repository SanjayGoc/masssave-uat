@istest
public class dsmtProjectAndEAExceptionsTest
{
	@istest
    static void runtest()
    {
        Exception_Template__c et =new Exception_Template__c();
        et.Reference_ID__c='PE-001';
        et.Type__c='Insurance';
        et.Online_Portal_Text__c='test';
        et.Active__c=true;
        et.Automatic__c=true;
        et.Project_and_Energy_Assessment__c=true;
        insert et;
        
        Exception__c ex =new Exception__c();
        ex.Disposition__c='Resolved';
        ex.Exception_Template__c=et.id;
        ex.Automated__c=true;
        insert ex;
        
        Project_and_Energy_Assessment__c pea = new  Project_and_Energy_Assessment__c();
        pea.Special_Incentive__c='Moderate Income';
        insert pea;
        
        dsmtProjectAndEAExceptions cntrl =new dsmtProjectAndEAExceptions();
    }
}