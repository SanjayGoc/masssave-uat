public class dsmtMarketOutreachController extends dsmtEnergyAssessmentBaseController {
	public Market_Outreach__c marketOutreach {get;set;}

	public dsmtMarketOutreachController() {
		
	}

	public Pagereference validateRequestAndMarketOutreach()
    {
        PageReference pg = validateRequest();
        if(pg == null){
        	System.debug('EA :' + ea);
            if(ea != NULL && ea.Id != null && (ea.Market_Outreach__r == null || ea.Market_Outreach__r.size() == 0)){
                marketOutreach = new Market_Outreach__c();
                marketOutreach.Energy_Assessment__c = ea.Id;
                INSERT marketOutreach;
            }else{
            	marketOutreach = ea.Market_Outreach__r.get(0);
            }
            Id marketOutreachId = marketOutreach.Id;
            List<Market_Outreach__c> marketOutreachs = Database.query('SELECT ' + getSObjectFields('Market_Outreach__c') + ' FROM Market_Outreach__c WHERE Id=:marketOutreachId');
            if(marketOutreachs.size() > 0){
                marketOutreach = marketOutreachs.get(0);
            }
        }
        return pg;
    }

    public void saveMarketOutreach(){
    	if(marketOutreach != NULL){
    		UPDATE marketOutreach;
    	}
    }
}