public class dsmtTerritoryHelper{
    
    public static boolean Syncterritory = true;
    @future(Callout = true)
    public static void CreateTerritoryDetails(Set<Id> terId){
        
        List<Territory__c> terList = [select id, Address__c,Location__c from Territory__c where id in : terId];
        
        Set<Id> locId = new Set<Id>();
        
        DSMTracker_Log__c log = new DSMTracker_Log__c();
        
        List<DSMTracker_Log__c> logToInsert = new List<DSMTracker_Log__c>();
        
        List<Territory_Detail__c> tdToInsert = new List<Territory_Detail__c>();
        Territory_Detail__c td = null;
        
        for(Territory__c ter : terList){
            locId .add(ter.Location__c);
            log = new DSMTracker_Log__c();
            log.Time__c = Datetime.now();
            log.Type__c = 'Address Validation';
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = new HTTPResponse();
            dsmtRouteParser.AddressParser obj1 = null;
            
            string address = EncodingUtil.urlEncode(ter.Address__c, 'UTF-8');
            req.setEndpoint(System_Config__c.getInstance().URL__c+'GetAddress?type=json&address='+Address);
            req.setMethod('GET');
            req.setTimeOut(120000);
            
            log.Request__c = req.getBody();
            
            http = new Http();
            if(!Test.isRunningTest()){
                res = http.send(req);
            }else{
                Map<String, String> boady2 = new Map<String, String>();
                boady2.put('status','test');
                boady2.put('address','test');
                boady2.put('City','test');
                boady2.put('latitude','1.32');
                boady2.put('longitude','12.4');
                boady2.put('formatedAddress','test');
                boady2.put('state_short','test');
                boady2.put('postalCode','test');
                String sbody = JSON.serialize(boady2);
                res.setBody(sbody);
            
            }
            
            log.Response__c = res.getBody();
            logToInsert.add(log);
            //  System.debug(res.getBody());
        
            obj1 = (dsmtRouteParser.AddressParser) System.JSON.deserialize(res.getBody(), dsmtRouteParser.AddressParser.class);
            
            td = new Territory_Detail__c();
            td.Coordinates__Longitude__s = obj1.Longitude;
            td.Coordinates__Latitude__s = obj1.Latitude;
            td.Territory__c = ter.Id;
            tdToInsert.add(td);
        
            system.debug('--obj--'+obj1.formatedAddress); 
        }
       /* if(tdToInsert.size() > 0){
            dsmtTerritoryHelper.Syncterritory = false;
            insert tdToInsert;
        }*/
        
        terList = [select id,name,Address__c,Radius__c,Active__c from Territory__c where id in : TerId]; ///*and id = 'a1A4D00000094HM'*/
        
       /* List<Territory_Detail__c> tdList = [select id,Territory__c,Territory__r.Location__c,Coordinates__Latitude__s,Coordinates__Longitude__s from Territory_Detail__c
                                                where Territory__c in : terId];
        
        List<Territory_Detail__c> tempList = null;
        
        Map<Id,List<Territory_Detail__c>> tdMap = new Map<Id,List<Territory_Detail__c>>();
        
        Set<Id> locId = new set<Id>();
        
        for(Territory_Detail__c td1 : tdList){
            
            locId.add(td1.Territory__r.Location__c);
            if(tdMap.get(td1.Territory__c) != null){
                tempList = tdMap.get(td1.Territory__c);
                tempList.add(td1);
                tdMap.put(td1.Territory__c,tempList);
            }else{
                tempList = new List<Territory_Detail__c>();
                tempList.add(td1);
                tdMap.put(td1.Territory__c,tempList);
            }
        }
        */
        
        List<dsmtCallout.territoryDetail> newtdList = new List<dsmtCallout.territoryDetail>();
        dsmtCallout.territoryDetail td2 = null;
        
        for(Territory__c ter : terList){
            td2 = new dsmtCallout.territoryDetail();
            td2.territoryId = ter.Id;
            td2.territoryName = ter.Name;
            td2.territoryAddress = ter.Address__c;
            td2.isActive = ter.Active__c;
            if (ter.Radius__c !=null) {
              if(ter.Radius__c >=0) {
                td2.territoryRadius  = ter.Radius__c;
               }
            }
            newtdList.add(td2);
        }
        
        String str = JSON.serialize(newtdList);
        
       /* List<dsmtCallout.territoryCoordinates> tcList = new List<dsmtCallout.territoryCoordinates>();
        dsmtCallout.territoryCoordinates tc = null;
        
        for(Territory_Detail__c tdc : tdList){
            tc = new dsmtCallout.territoryCoordinates();
            tc.territoryId = tdc.Territory__c;   
            tc.latitude = string.valueOf(tdc.Coordinates__Latitude__s);   
            tc.longitude = string.valueOf(tdc.Coordinates__Longitude__s);  
            tcList.add(tc); 
        }
        
        String str1 = JSON.serialize(tcList);*/
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        
     
        req.setEndpoint(System_Config__c.getInstance().URL__c+'CreateTerritory?type=json');
        req.setMethod('POST');
        
//        String body = '{"territoryDetail":'+str+',"territoryCoordinates":'+str1+'}';
        String body = '{"territoryDetail":'+str+'}';
        system.debug('--str+str1--'+body);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        req.setTimeout(20000);
        http = new Http();
         if(!Test.isRunningTest()){
        res = http.send(req);
        System.debug(res.getBody());
        }
        
        if(tdToInsert.size() > 0){
            dsmtTerritoryHelper.Syncterritory = false;
            insert tdToInsert;
        }
        
        List<Location__c> locList = [select id, Last_Sync_Date__c from location__c where id =: locId];
        
        if(locList != null && locList.size() > 0){
            locList.get(0).Last_Sync_Date__c = Datetime.now();
            update locList;
        }
        if(logToInsert.size() > 0){
            insert logToInsert;
        }
    }
}