@isTest
public class SafetyAspectAIAUTest {
    
    static testmethod void test(){
        
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
		
        
        test.startTest();
        
        Safety_Aspect__c sa = new Safety_Aspect__c();
        sa.Knob_Tube_Wiring_Addressed__c = true;
        sa.Dryer_Vent_Addressed__c = true;
        sa.High_CO_Addressed__c = true;
        sa.Knob_Tube_Wiring_Present__c = true;
        
        insert sa;
        
        
        
        Id RecordTypeId = Schema.SObjectType.Barrier__c.getRecordTypeInfosByName().get('Incentive Barrier').getRecordTypeId(); 
        Barrier__c br = new Barrier__c();
        br.Energy_Assessment__c = ea.id;
        br.Barrier_Cleared_K_T_W__c = true;
        br.Barrier_Cleared_Ventilation__c = true;
        br.Barrier_Cleared_Mech_Sys_Barr__c = true;
        br.RecordTypeId = RecordTypeId;
       // insert br;
        
        ea.Safety_Aspect__c = sa.Id;
        update ea;
        sa.Moisture_Present__c = true;
        update sa;
        Test.stopTest();
    }
    
    static testmethod void test2(){
         test.startTest();
        
        Safety_Aspect__c sa = new Safety_Aspect__c();
        sa.Knob_Tube_Wiring_Addressed__c = true;
        sa.Dryer_Vent_Addressed__c = true;
        sa.High_CO_Addressed__c = true;
        sa.Knob_Tube_Wiring_Present__c = true;
        
        insert sa;
        
        SMA_Record__c sma = new SMA_Record__c();
        sma.Safety_Aspect__c = sa.Id;
        sma.Value__c = 'Essential to Work Scope';
        insert sma;
        
        SMA_Record__c sma1 = new SMA_Record__c();
        sma1.Safety_Aspect__c = sa.Id;
        sma1.Type__c = 'Other';
        sma1.Value__c = 'Recommended';
        insert sma1;
        
        SMA_Record__c sma2 = new SMA_Record__c();
        sma2.Safety_Aspect__c = sa.Id;
        insert sma2;
        
        update sa;
        test.stopTest();
    }
}