public class dsmtEmployeeHelper{
    
    public static void InactivateEmployee(Set<Id> empId){
        
        List<Work_Team__c> wtList = [select id,Unavailable__c,Captain__r.Employment_End_Date__c,Service_Date__c from work_Team__c 
                                            where Captain__c in : empId ];
        
        Set<Id> wtId = new Set<Id>();
        Set<Id> setWorkTeamIds = new Set<Id>();
        
        List<Work_Team__c> wtListupdate = new List<Work_Team__c>();
        
        //Map<Id,
        
        if(wtList != null && wtList.size() > 0){
            for(Work_Team__c wt : wtList){
                Date endDate = wt.Captain__r.Employment_End_Date__c!=null?wt.Captain__r.Employment_End_Date__c: Date.today();
                
                /*if(wt.Captain__r.Employment_End_Date__c >= Date.today()){
                    endDate = endDate + 1;
                }
                else{
                    endDate = wt.Captain__r.Employment_End_Date__c;
                }*/
                if(wt.Service_Date__c > endDate){
                    wt.Unavailable__c  = true;
                    wtId.add(wt.Id);
                    wtListupdate.add(wt);
                    setWorkTeamIds.add(wt.Id);
                }
            }
            
            for(Work_Team__c wt : [select id,Unavailable__c,Captain__r.Employment_End_Date__c,Service_Date__c,Captain__r.Employment_Start_Date__c,
                                        Schedule_Line_Item__r.unavailable__c  from work_Team__c where Captain__c in : empId
                                        and Schedule_Line_Item__r.unavailable__c = false AND Id Not In :setWorkTeamIds]){
                    if(!wt.Schedule_Line_Item__r.unavailable__c){
                        wt.Unavailable__c  = false;
                        
                    }
                    if(wt.Captain__r.Employment_End_Date__c!=null){
                        if(wt.Service_Date__c > wt.Captain__r.Employment_End_Date__c)
                            wt.Unavailable__c  = true;
                        
                    }
                    if(wt.Captain__r.Employment_Start_Date__c!=null){
                        if(wt.Service_Date__c < wt.Captain__r.Employment_Start_Date__c)
                                wt.Unavailable__c  = true;
                    }
                    wtListupdate.add(wt);
            }
            
            if(wtListupdate.size() > 0){
                update wtListupdate;
            }
            
            List<Appointment__c> aptList = [select id,Appointment_Status__c,Appointment_Start_Date__c,Employee__r.Employment_End_Date__c
                     from Appointment__c where
                    Work_Team_Member__r.Work_Team__c in : empId and Appointment_Status__c != 'Unavailable' ];
                    
            
            List<Appointment__c> aptListupdate = new List<Appointment__c> ();
            if(aptList != null && aptList.size() > 0){
                for(Appointment__c apt : aptList){
                    if(apt.Appointment_Start_Date__c >= apt.Employee__r.Employment_End_Date__c){
                        apt.Appointment_Status__c = 'Cancelled';
                        aptListupdate.add(apt);
                    }
                }
                
                if(aptListupdate.size() > 0){
                    update aptListupdate;
                }
            }
            
            List<WorkOrder__c> workOrderlst = [select id,Status__c from workOrder__c where work_Team__c in : wtId];
            
            if(workOrderlst != null && workOrderlst.size() > 0){
                String dtConverted = DateTime.now().format('MM/dd/yyyy h:mm a');
                
                for(WorkOrder__c wo : workOrderlst){
                    wo.Status__c = 'Requires Rescheduling';
                    //wo.Items_To_Review__c = 'Employee Inactivated on '+dtConverted;
                }
                
                update workOrderlst;
            }
        }
    }
    
    public static void activateEmployee(Set<Id> empId){
        
        List<Work_Team__c> wtList = [select id,Unavailable__c,Captain__r.Employment_End_Date__c,Service_Date__c,Captain__r.Employment_Start_Date__c,
                                        Schedule_Line_Item__r.unavailable__c  from work_Team__c where Captain__c in : empId
                                        and Schedule_Line_Item__r.unavailable__c = false];
                                            
        
        Set<Id> wtId = new Set<Id>();
        
        List<Work_Team__c> wtListupdate = new List<Work_Team__c>();
        
        //Map<Id,
        
        if(wtList != null && wtList.size() > 0){
            for(Work_Team__c wt : wtList){
                
               // if(wt.Service_Date__c >= wt.Captain__r.Employment_End_Date__c){
                    if(!wt.Schedule_Line_Item__r.unavailable__c){
                        wt.Unavailable__c  = false;
                    }
                    if(wt.Captain__r.Employment_End_Date__c!=null){
                        if(wt.Service_Date__c > wt.Captain__r.Employment_End_Date__c)
                            wt.Unavailable__c  = true;
                        
                    }
                    if(wt.Captain__r.Employment_Start_Date__c!=null){
                        if(wt.Service_Date__c < wt.Captain__r.Employment_Start_Date__c)
                                wt.Unavailable__c  = true;
                    }
               // }
            }
            
            update wtList;
        }
    }
    public static void SkipScheduingEmployee(Set<Id> empId){
    
    }
}