public class dsmtFutureHelper{
    
    public static boolean customerUpdated = false;
    public static boolean woupdatedFromSchedueled = true;
    public static boolean woupdatedFromAppointment = true;
    
    @future
    public static void CreateunavailableAppointment(Set<Id> apptId){
        
        dsmtCallOut.unavailableHoursDetail unDetail = new dsmtCallOut.unavailableHoursDetail();
    }
    
    @future
    public static void updateCustomer(set<Id> custId){
        
        customerUpdated = true;
        
        List<Customer__c> custList = [select id,name,first_Name__c,Last_Name__c from customer__c where id in : custId];
        
        if(custList != null && custList.size() > 0){
            for(Customer__c cust : custList){
                if(cust.Name != null && (cust.Name.length() == 15 || cust.Name.length() == 18)){
                    
                    if(cust.First_Name__c != null){
                        cust.Name = cust.First_Name__c + ' ' + cust.Last_Name__c;
                    }else{
                        cust.Name = cust.Last_Name__c;
                    }
                }
            }
            update custList;
        }
    }
    @future
    public static void dsmtUpdateInvoicetype(set<Id> eaId){
        List<Energy_Assessment__c> ealist=[select id,name,Primary_Provider__c,Secondary_Provider__c,Workorder__c from Energy_Assessment__c where id in:eaId];
        if(ealist != null && eaList.size() > 0 && eaList.get(0).Workorder__c != null){
            Workorder__c  wo = new Workorder__c();
            wo.Id = eaList.get(0).Workorder__c;
            wo.Primary_Provider__c = eaList.get(0).Primary_Provider__c ;
            wo.Secondary_Provider__c= eaList.get(0).Secondary_Provider__c;
            update wo;
            
            List<Workorder__c> woList = [select id,Dsmtracker_Product__c ,Primary_Provider__c,Secondary_Provider__c,Workorder_Type__c,
                                        Invoice_Type__c  from Workorder__c where id =: wo.Id];
            setInvoiceType(woList);
                                            
        }
    }  
    
    public static void setInvoiceType(List<Workorder__c> recList){
        
        Set<Id> prodId = new Set<Id>();
        Set<Id> wtypeId = new Set<Id>();
        
        for(Workorder__c rec : recList){
                        
            if(rec.Dsmtracker_Product__c != null){
                prodId.add(rec.Dsmtracker_Product__c);
            }
            
            if(rec.Workorder_Type__c != null){
                wtypeId.add(rec.Workorder_Type__c);
            }
        }
        
        if(prodId.size() > 0){
                List<Dsmtracker_Product__c> dsmtProdList = [select id,name,Category__c,RecordType.Name,Sub_Category__c,Surface__c
                                                                 from Dsmtracker_Product__c where id in : prodId];
                
                Map<Id,Dsmtracker_Product__c> dsmtProdMap = new Map<Id,Dsmtracker_Product__c>();
                
                for(Dsmtracker_Product__c dsmt : dsmtProdList){
                    dsmtProdMap.put(dsmt.Id,dsmt);
                }
                
                List<Workorder_Type__c> wtypeList = [select id,name from Workorder_Type__c where id in : wtypeId];
                
                Map<Id,String> wtypeMap = new Map<Id,String>();
                
                for(Workorder_type__c wtype : wtypeList){
                    wtypeMap.put(wtype.Id,wtype.Name);
                }
                
                String Type = null;
                String PrimaryProvider = null;
                String SecondaryProvider = null;
                String RecTypeName = null;
                
                for(Workorder__c rec : recList){
                    Type = null;
                    PrimaryProvider = null;
                    SecondaryProvider = null;
                    RecTypeName = null;
                
                    if(rec.Dsmtracker_Product__c != null){
                        
                        system.debug('--dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c---'+dsmtProdMap.get(rec.Dsmtracker_Product__c).Category__c);
                        
                        RecTypeName = dsmtProdMap.get(rec.Dsmtracker_Product__c).RecordType.Name;
                        

                            
                            if(rec.Workorder_Type__c != null){

                                    Type = wtypeMap.get(rec.Workorder_Type__c);
                                    PrimaryProvider = rec.Primary_Provider__c;
                                    SecondaryProvider = rec.Secondary_Provider__c;
                                    
                                    system.debug('--Type ---'+Type );
                                    
                                    if(Type == 'HEA (Home Energy Assessment)' || Type == 'Landlord Visit' || Type == 'Expanded HEA' ||
                                        Type == 'Duct Specification' || Type == 'Combustion Safety Return Visit'){
                                        
                                        if(PrimaryProvider == 'Eversource East Electric'){
                                            if(SecondaryProvider == 'Eversource East Gas' || SecondaryProvider == null || SecondaryProvider == ''){
                                                rec.Invoice_Type__c = 'NSTAR418';
                                            }
                                        }else if(PrimaryProvider == 'Eversource West Electric'){
                                            if(SecondaryProvider == null || SecondaryProvider == ''){
                                                rec.Invoice_Type__c = 'WMECO418';
                                            }
                                        }else if(PrimaryProvider == 'Eversource East Gas'){
                                            if(SecondaryProvider == 'Eversource East Electric'){
                                                  rec.Invoice_Type__c = 'NSTARGAS416';
                                            }else if(SecondaryProvider == 'National Grid Electric'){
                                                
                                                    rec.Invoice_Type__c = 'NSTARGAS416';
                                            }else if(SecondaryProvider == null || SecondaryProvider == ''){
                                                rec.Invoice_Type__c = 'NSTARGAS416';
                                            }
                                        }
                                    } else if(Type == 'SHV (Special Home Visit)' || Type == 'Renter Visit'){
                                            
                                            if(PrimaryProvider == 'Eversource East Electric'){
                                                if(SecondaryProvider == 'Eversource East Gas' || SecondaryProvider == null || SecondaryProvider == ''){
                                                    rec.Invoice_Type__c = 'NSTAR416S';
                                                }
                                            }else if(PrimaryProvider == 'Eversource West Electric'){
                                                if(SecondaryProvider == null || SecondaryProvider == ''){
                                                    rec.Invoice_Type__c = 'WMECO416S';
                                                }
                                            }else if(PrimaryProvider == 'Eversource East Gas'){
                                                if(SecondaryProvider == 'Eversource East Electric'){
                                                    
                                                        rec.Invoice_Type__c = 'NSTARGAS416S';
                                                    
                                                }else if(SecondaryProvider == 'National Grid Electric'){
                                                   
                                                }else if(SecondaryProvider == null || SecondaryProvider == ''){
                                                    rec.Invoice_Type__c  = 'NSTARGAS416S';
                                                }
                                            }
                                    }else if(Type == 'In-Process Inspection' || Type == 'Post Work Inspection' || Type == 'Post Work Inspection with Loan Verification' ||
                                                Type == 'Loan Verification' || Type == 'Re-Inspection'){
                                                
                                        if(PrimaryProvider == 'Eversource East Gas'){
                                            rec.Invoice_Type__c = 'NSTARGAS413';
                                        }
                                        else if(PrimaryProvider == 'Eversource East Electric'){
                                            rec.Invoice_Type__c = 'NSTAR413';
                                        }
                                        
                                        else if(PrimaryProvider == 'Eversource West Electric'){
                                            rec.Invoice_Type__c = 'WMECO413';
                                        }
                                    }
                            }
                       
                    }
                    
                    if(rec.Invoice_Type__c == null){
                        rec.Invoice_Type__c = 'Not Found';
                    }
                }
            }
            update recList;
            
            list<Invoice_Line_Item__c> iliList = new list<Invoice_Line_Item__c>();
            Map<Id,Id> RecReviewMap = new Map<Id,Id>();

            
            for(Invoice_Line_Item__c ili : database.query('select ' + RecommendationTriggerHandler.getSObjectFields('Invoice_Line_Item__c') + ', Recommendation__r.Quantity__c from Invoice_Line_Item__c where Workorder__c =: wo.Id and Recommendation__c = null and Review__c = null and Unit_Cost__c > 0 and Reversal_Created__c = false'))
            {
             RecReviewMap.put(ili.Recommendation__c,ili.Review__c);
                if(ili.Unit_Cost__c != null && ili.Unit_Cost__c > 0)
                {
                    Invoice_Line_Item__c iliClone = ili.clone(false, false);
                   // DSST-11487 by HP on 19th Sep 2018
                   String fuelTypeOld=ili.Fuel_Type__c;
                    // New Code Fix for  DSMT 11487 11477 9731--done
                    iliClone.QTY__c=(ili.QTY__c* -1);
                    if(fuelTypeOld=='Gas'){
                    iliClone.Savings_Gas__c='0';
                    }
                    else if(fuelTypeOld=='Oil'){
                    iliClone.Savings_Oil__c='0';
                    }
                    else if(fuelTypeOld=='Propane'){
                    iliClone.Savings_Propane__c='0';
                    }
                    else if(fuelTypeOld=='Electric'){
                    iliClone.SAVINGS_ELE__c='0';
                    }
                    else{
                      iliClone.Savings_Other__c='0';
                    }
                    
                   
                    iliClone.Reversal_Created__c = true;
                    // code fix ends
                    ili.Reversal_Created__c  = true;
                    iliList.add(iliClone);
                    iliList.add(ili);
                }
            }
            
            if(iliList.size() > 0){
                upsert iliList;
            }
            Set<Id> woId = new set<Id>();
            Set<Id> wtypeId1 = new Set<Id>();
            Date ScheduleDAte;
        
    }
        

    public static void CreateILIAndPLI(Set<Id> woId,Set<Id> wtypeId,Date ScheduleDAte){
        
        List<Appointment__c> appList = [select id,Workorder__c,Trade_Ally_Account__c,Trade_Ally_Account__r.Internal_Account__c,
                                        (Select Id,Assessment_Complete_Date__c  from Energy_Assessment__r) from Appointment__c
                                                    where workorder__c in  : woId];
        if(appList != null && appList.size() > 0 && appList.get(0).Energy_Assessment__r != null && appList.get(0).Energy_Assessment__r.size() > 0){
            ScheduleDAte = appList.get(0).Energy_Assessment__r[0].Assessment_Complete_Date__c  ;                              
        }
        system.debug('--ScheduleDAte---'+ScheduleDAte);
        Map<Id,boolean> appMap = new Map<Id,Boolean>();
        Map<Id,Appointment__c> appMapNew = new Map<Id,Appointment__c>();
        
        for( Appointment__c app : appList){
            appMap.put(app.Workorder__c,app.Trade_Ally_Account__r.Internal_Account__c);
            appMapNew.put(app.Workorder__c,app);
        }
        
        List<Workorder_Type__c> wtypeList = [select id,name from Workorder_Type__c where id in : wtypeId];
        
        Map<Id,String> wtypeMap = new Map<Id,String>(); 
        
        for(Workorder_Type__c wtype : wtypeList){
            wtypeMap.put(wtype.Id,wtype.Name);
        }
        
        List<Workorder_Type_Invoice_Item_Setting__c> wtypeSetting = [select id,name,Name__c,Workorder_Type__c,part_Id__c,Is_not_CLEAResult__c
                                                                      from Workorder_Type_Invoice_Item_Setting__c where Workorder_Type__c in : wtypeMap.values()];
        Map<String,String> partIdMap = new Map<String,String>();
        
        for(Workorder_Type_Invoice_Item_Setting__c wsetting : wtypeSetting){
            partIdMap.put(wsetting.Name__c,wsetting.Part_Id__c);
        }
        
        if(partIdMap.keyset().size() > 0){
            List<Dsmtracker_Product__c> dsmtList = [select id,name,Eversource_Labor_Price__c,EMHome_EMHub_PartID__c 
                                                             from dsmtracker_Product__c where EMHome_EMHub_PartID__c in : partIdMap.values()];
            
            if(dsmtList != null && dsmtList.size() > 0){
                Map<String,Dsmtracker_Product__c> dsmtMap = new Map<String,Dsmtracker_Product__c>();
                
                for(Dsmtracker_Product__c dsmt : dsmtList){
                    dsmtMap.put(dsmt.EMHome_EMHub_PartID__c,dsmt);
                }
                
                List<Invoice_Line_Item__c> invLineList = new List<Invoice_Line_Item__c>();
                Invoice_Line_Item__c newinvLine = null;
                
                MAp<String,String> invoiceIds = new Map<String,String>();
                
                List<Workorder__c> woList = [select id,Invoice_Type__c,Customer__c,Primary_Provider__c,Status__c,Workorder_Type__c,Scheduled_Date__c from Workorder__c where id in : woId
                and Invoice_Type__c != 'Not Found'];
                
                for(Workorder__c  wo : woList){
                    invoiceIds.put(wo.Invoice_Type__c,wo.Primary_Provider__c);
                }
                Set<String> newInvId = invoiceIds.keyset();
                
                Date startDate = ScheduleDAte.toStartOfMonth();
                Date endDate = startDate.addMonths(1).addDays(-1);
                
                List<Invoice__c > invList=[Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in: newInvId 
                                         and Status__c = 'Unsubmitted' and Invoice_Date__c >=: ScheduleDAte 
                                         and Invoice_Date__c <=: EndDate  ORDER BY Invoice_Date__c  ASC];
                
                if(invList != null && invList.size() > 0){
                
                }else{
                    String target = '/'; 
                    String replacement = '';                      
                    Datetime output = endDate;                       
                    String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                    
                      
                      List<Invoice__c> invData= new List<Invoice__c>();

                      for(String s : invoiceIds.keyset()) {
                          Invoice__c invRecord;
                          invRecord=new Invoice__c(
                          Invoice_Type__c = s,
                          Invoice_Date__c=endDate,
                          Status__c = 'In Process',
                          Provider__c = invoiceIds.get(s)
                          );
                          invRecord.Invoice_Number__c = formatedDate + s;

                          invData.add(invRecord);
                          
                      }
                      if(invData.size() > 0){
                          insert invData;
                      }
                      invList=[Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:invoiceIds.keyset()
                                         and Status__c = 'Unsubmitted' ORDER BY Invoice_Date__c  ASC];
                }
                
                    for(Workorder__c  wo : woList){
                        
                        if(wo.Status__c == 'Completed' && wo.Workorder_Type__c != null){
                                
                            for( Invoice__c  inv:invList){
                                    
                                if(ScheduleDAte <= inv.Invoice_Date__c){
                                 
                                    String woType = wtypeMap.get(wo.Workorder_Type__c);
                                    
                                    if(appMap.get(wo.Id) == false){
                                          woType += '_HPC';  
                                    }
                                    
                                    system.debug('--woType ---'+woType);
                                    
                                    system.debug('--partIdMap.get(woType)---'+partIdMap.get(woType));
                                    system.debug('--dsmtMap---'+dsmtMap);
                                    system.debug('--dsmtMap123---'+dsmtMap.get(partIdMap.get(woType )));
                                    
                                    if(woType  != null && partIdMap.get(woType) != null ){
                                        
                                        if(dsmtMap.get(partIdMap.get(woType)) != null){
                                        
                                            Dsmtracker_Product__c dsmt = dsmtMap.get(partIdMap.get(woType ));
                                            
                                            if(dsmt != null){
                                                newinvLine = new Invoice_Line_Item__c();
                                                newinvLine.Workorder__c = wo.Id;
                                                newinvLine.Invoice__c = inv.Id;
                                                newinvLine.Qty__c = 1;
                                                newinvLine.Unit_cost__c = dsmt.Eversource_Labor_Price__c;
                                                newinvLine.DSMTracker_Product__c = dsmt.Id;
                                                newinvLine.Part_Id__c = dsmt.EMHome_EMHub_PartID__c;
                                                invLineList.add(newinvLine);
                                                break;
                                            }
                                        
                                        } 
                                    }
                                }
                            }
                            
                            String woType = wtypeMap.get(wo.Workorder_Type__c);
                                    
                            if(appMap.get(wo.Id) == false){
                                  woType += '_HPC';  
                            }
                            
                            
                        }
                    }
                    
                    
             
             List<Energy_Assessment__c> eaList = [select id,workorder__r.Scheduled_Date__c from Energy_Assessment__c
                                                     where workorder__c in : woId];
             
             boolean FirstILI = false;
             if(eaList  != null && eaList.size() > 0){
                 String eaId = eaList.get(0).Id;
                 List<Recommendation__c> recomList = [select id,EXT_JOB_ID__c,Energy_Assessment__R.Name, name,Invoice_Type__c,Isinvoiced__c, Quantity__c,Installed_Date__c,Unit_Cost__c,
                                                Energy_Assessment__r.Primary_Provider__c,DSMTracker_Product__r.EMHome_EMHub_PartID__c,Category__c,
                                                    Recommendation_Scenario__r.Type__c,Calculated_Incentive__c,Installed_Quantity__c,
                                                    (select Id,Voided__c from invoice_Line_Items__r where Voided__c = false)
                                                  from Recommendation__c where Category__c = 'Direct Install'  and Energy_Assessment__c =: eaId
                                                  and Invoice_Type__c != 'Not Found'];
                
                if(recomList  != null && recomList.size() > 0){
                Map<String,List<Recommendation__c>> RecTypeMap = new Map<String,List<Recommendation__c>>();
                List<Recommendation__c> temp = null;
                
                Set<String> newinvoiceIds = new Set<String>();
                                
                if (recomList.size() > 0) {
                  for (Recommendation__c r: recomList) {
                      system.debug('---r.Invoice_Type__c---'+r.Invoice_Type__c);
                   //  r.Isinvoiced__c = true;
                    if(r.Invoice_Type__c  != null && r.Invoice_Type__c != 'Not Found'){
                        newinvoiceIds.add(r.Invoice_Type__c);
                    }
                    
                  } 
                 // update recomList;
                }
             
             Map<String,Invoice__c> invTypeMap = new Map<String,Invoice__c>();
             
             DAte installedDate = ScheduleDAte;
             endDate = installedDate.addMonths(1).addDays(-1);
             
             invList = [Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:newinvoiceIds 
                                         and Status__c = 'UnSubmitted' and Invoice_Date__c != null 
                                         And Invoice_Date__c >=: installedDate and invoice_Date__c <=: endDate ORDER BY Invoice_Date__c  ASC];
                
                for(Invoice__c inv : invList){
                    invTypeMap.put(inv.Invoice_Type__c,inv);
                }
                
                Set<Id> recId = new set<Id>();
                Set<Id> CreatedrecId = new set<Id>();
                
                List<Recommendation__c> InvToCreate = new List<Recommendation__c>();
                
                Set<String> ExtInvoiceType = new Set<String>();
                List<Invoice__c> invData= new List<Invoice__c>();
                startDate = ScheduleDAte.toStartOfMonth();
                endDate = startDate.addMonths(1).addDays(-1);

                    
                if(invList.size()>0) {
                    
                     for( Invoice__c  inv:invList){
                         ExtInvoiceType.add(inv.Invoice_Type__c);
                     }
                         
                     for( Invoice__c  inv:invList){
                        
                        String target = '/'; 
                        String replacement = '';                      
                        Datetime output = inv.Invoice_Date__c;                       
                        String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                        
                        
                        for(Recommendation__c r: recomList) {
                            
                            
                           if(inv.Invoice_Type__c  == r.Invoice_Type__c && (r.Isinvoiced__c == false || r.invoice_Line_Items__r.size() == 0) ){
                              
                              Invoice_Line_Item__c createILItemData;
                             
                                    recId.add(r.Id);
                                    if(CreatedrecId.add(r.Id)){
                                     createILItemData = new Invoice_Line_Item__c(
                                         Invoice__c = inv.Id,
                                         Recommendation__c = r.Id,
                                         Status__c=inv.Status__c,
                                         QTY__c= r.Quantity__c,
                                         Unit_Cost__c = r.Unit_Cost__c,
                                         INSTALLED_DATE__c = r.INSTALLED_DATE__c,
                                         Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                         //Review__c = newlist.get(0).Id
                                     );
                                     if(!FirstILI){
                                         FirstILI = true;
                                         createILItemData.Is_Distinct_Per_EA__c = true;
                                     }
                                     system.debug('--createILItemData---'+createILItemData);
                                   invLineList.add(createILItemData);
                                   
                                   r.Isinvoiced__c = true;
                                   r.EXT_JOB_ID__c = r.Energy_Assessment__R.Name+formatedDate+inv.Invoice_Type__c ;
                                   }
                             
                           }else{
                               system.debug('---ExtInvoiceType---'+ExtInvoiceType);
                               system.debug('---inv.Invoice_Type__c---'+inv.Invoice_Type__c);
                               
                               if(recId.add(r.Id) && ExtInvoiceType.add(r.Invoice_Type__c)){
                                    system.debug('--r.Invoice_Type__c123---'+r.Invoice_Type__c);
                                    InvToCreate.add(r);
                                }
                           }
                           
                        }
                        system.debug('--InvToCreate--'+InvToCreate);
                    }
                }
                else  {
                   /* String target = '/'; 
                    String replacement = '';
                     
                    String formatedDate= endDate.format().replace(target, replacement); */
                    
                    String target = '/'; 
                    String replacement = '';                      
                    Datetime output = endDate;                       
                    String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);



                      for(Recommendation__c r: recomList) {
                          if(ExtInvoiceType.add(r.Invoice_Type__c)){
                              Invoice__c invRecord;
                              invRecord=new Invoice__c(
                              Invoice_Type__c = r.Invoice_Type__c,
                              Invoice_Date__c=endDate,
                              Status__c = 'In Process',
                              Provider__c = r.Energy_Assessment__r.Primary_Provider__c
                              );
                              invRecord.Invoice_Number__c = formatedDate + r.Invoice_Type__c;
    
                              invData.add(invRecord);
                              
                          }
                      }
                          
                  }
        
                    system.debug('--invDate---'+invData);
                    Set<String> invType = new Set<String>();
                        
                    for(Recommendation__c r : invToCreate){
                        if(invType.Add(r.Invoice_Type__c)){
                            String target = '/'; 
                            String replacement = '';                      
                            Datetime output = endDate;                       
                            String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);
                            

                            Invoice__c invRecord;
                            invRecord=new Invoice__c(
                            Invoice_Type__c = r.Invoice_Type__c,
                            Invoice_Date__c=endDate,
                            Status__c = 'In Process',
                            Provider__c = r.Energy_Assessment__r.Primary_Provider__c
                            );
                            invRecord.Invoice_Number__c = formatedDate + r.Invoice_Type__c;
                            
                            invData.add(invRecord);
                        }                          
                                      
                    }       
                    
                    if(invData.size()>0){
                       insert invData;
                       
                       for(Invoice__c inv : invData){
                            invTypeMap.put(inv.Invoice_Type__c,inv);
                        }
                       system.debug('--invoiceIds---'+invoiceIds);
                       
                       invList=[Select Id,Status__c,Invoice_Date__c,Invoice_Type__c  From Invoice__c where   Invoice_Type__c in:newinvoiceIds 
                                                     and Status__c = 'Unsubmitted' and Invoice_Date__c != null And Invoice_Date__c >=: installedDate
                                                     ORDER BY Invoice_Date__c  ASC];
                        
                        Set<Id> NewRecId = new Set<Id>();
                        
                        if(invList.size()>0) {
                             for( Invoice__c  inv:invList){
                                 
                                for(Recommendation__c r: recomList) {
                                    
                                    String target = '/'; 
                                    String replacement = '';                      
                                    Datetime output = inv.Invoice_Date__c;                       
                                    String formatedDate= output.formatGMT('yyyy/MM/dd').replace(target, replacement);

                                    
                                    system.debug('--r.Invoice_Type__c---'+r.Invoice_Type__c);
                                    system.debug('--inv.Invoice_Type__c---'+inv.Invoice_Type__c);
                                    system.debug('--r.Isinvoiced__c---'+r.Isinvoiced__c);
                                      
                                      if(inv.Invoice_Type__c  ==r.Invoice_Type__c  ){
                                              Invoice_Line_Item__c createILItemData;
                                                 
                                                system.debug('--r.Installed_Date__c---'+r.Installed_Date__c);
                                                system.debug('--inv.Invoice_Date__c---'+inv.Invoice_Date__c);
                                        
                                               // if(r.Installed_Date__c <= inv.Invoice_Date__c){
                                                    if(CreatedrecId.add(r.Id)){
                                                     system.debug('---inv.Id---'+inv.Id);
                                                     createILItemData = new Invoice_Line_Item__c(
                                                     Invoice__c = inv.Id,
                                                     Recommendation__c = r.Id,
                                                     Status__c=inv.Status__c,
                                                     QTY__c= r.Quantity__c,
                                                     Unit_Cost__c = r.Unit_Cost__c,
                                                     INSTALLED_DATE__c = r.INSTALLED_DATE__c,
                                                     Part_Id__c = r.DSMTracker_Product__r.EMHome_EMHub_PartID__c
                                                     
                                                     //Review__c = newlist.get(0).Id
                                                 );
                                                     if(!FirstILI){
                                                         FirstILI = true;
                                                         createILItemData.Is_Distinct_Per_EA__c = true;
                                                     }
                                                      invLineList.add(createILItemData);
                                                r.Isinvoiced__c = true;
                                                  r.EXT_JOB_ID__c = r.Energy_Assessment__R.Name+formatedDate+inv.Invoice_Type__c ;
                                                    }
                                                system.debug('--createinvLineListData---'+createILItemData);
                                                
                                           //}
                                       }
                                    }
                                }
                            }
                        }
                        update recomList;
                        }
                    }
                    if(invLineList.size() > 0){
                        
                        insert invLineList;
                    }
                    
                   
                    
                }
                
            }
    }
       
  
     @Future(callout=true)
    public static void callEcCustFuture(Map<Id,Id> mapWoCust,Set<Id> setECId){
        List<Eligibility_Check__c> lstECUpdate = new List<Eligibility_Check__c>();
        Map<Id,Eligibility_Check__c> mapEC = new Map<ID,Eligibility_Check__c>();
        for(Eligibility_Check__c ec: [select Id,Customer__c from Eligibility_Check__c where Id In :setECId and Customer__c = null]){
            mapEC.put(ec.Id,ec);
            
        }
        for(Workorder__c wo: [select Id,Customer__c,Eligibility_Check__c from Workorder__c where Id In :mapWoCust.keyset()]){
            if(wo.Eligibility_Check__c != null && wo.Customer__c != null){
                if(mapEC.get(wo.Eligibility_Check__c) != null){
                    Eligibility_Check__c objEC = mapEC.get(wo.Eligibility_Check__c);
                    objEC.Customer__c = wo.Customer__c;
                    
                    lstECUpdate.add(objEC);
                }
            }
        }
        
        if(lstECUpdate.size()>0)
            update lstECUpdate;
    }
    
    public static String getGuestSession(){
       
           dsmtCallOut.loginDetail ldObj = new dsmtCallOut.loginDetail();
            
            ldObj.userName = Login_Detail__c.getInstance().Generate_Session_UserName__c;
            ldObj.password = Login_Detail__c.getInstance().Generate_Session_Password__c;
            String orgid = Login_Detail__c.getInstance().Org_Id__c;
            if(orgid != null){
                orgid = orgid.substring(0,15);
            } 
            ldObj.orgID = orgid;
            ldObj.securityToken = Login_Detail__c.getInstance().Generate_Session_Token__c;
            ldObj.serverURL = Login_Detail__c.getInstance().Server_URL__c;
            
            
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            
            String url = Dsmt_Salesforce_Base_Url__c.getInstance().Generate_Guest_Session_URL__c;
            if(url != null){
               req.setEndpoint(url);
            }
            
           
            
            req.setMethod('POST');
            req.setTimeOut(120000);
            req.setHeader('AuthorizeToken', Login_Detail__c.getInstance().Authorize_Token__c);
            String body = JSON.serialize(ldObj);
            system.debug('--body ---'+body);
            req.setBody(body);
            req.setHeader('content-type', 'application/json');
            http = new Http();
           
          String sessionId = null;  
          if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
            
            if(res.getBody() != null){
                Map <String, Object> root = (Map <String, Object>) JSON.deserializeUntyped(res.getBody());
                sessionId = (String)root.get('SessionId');
            }
          }
       
       system.debug('--sessionId--'+sessionId);   
       return sessionId;   
    }    
}