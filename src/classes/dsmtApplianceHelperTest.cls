@isTest
private class dsmtApplianceHelperTest{
    
   @isTest
   static void test1()
   {
      dsmtCreateEAssessRevisionController.stopTrigger = true;
      dsmtEAModel.setupAssessment();

      Test.startTest();

      List<Appliance__c> aplList = [Select Id, Type__c, RecordTypeId From Appliance__c];

      update aplList;

      Test.stopTest();
   }

   @isTest
   static void test2()
   {
      dsmtCreateEAssessRevisionController.stopTrigger = true;
      Energy_Assessment__c ea = dsmtEAModel.setupAssessment();
      
      Air_Flow_and_Air_Leakage__c ai = new Air_Flow_and_Air_Leakage__c();
      ai.Energy_Assessment__c = ea.Id;
      insert ai;

      Test.startTest();

      List<Appliance__c> aplList = new List<Appliance__c>();

      Appliance__c apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other Dehumidifier').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'Dehumidifier';
      aplList.add(apl);

      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other Other').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'Other';
      aplList.add(apl);

      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Other General').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'General';
      aplList.add(apl);
    
      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Refrigeration').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'Refrigeration';
      aplList.add(apl);

      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Electronics TV').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'TV';
      aplList.add(apl);

      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Dishwasher').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'Dishwasher';
      aplList.add(apl);

      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Kitchen Range').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'Range';
      aplList.add(apl);

      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Dryer').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'Dryer';
      aplList.add(apl);

      apl = new Appliance__c();
      apl.RecordTypeId = Schema.SObjectType.Appliance__c.getRecordTypeInfosByName().get('Laundry Washer').getRecordTypeId();
      apl.Energy_Assessment__c = ea.Id;
      apl.Type__c = 'Washer';
      aplList.add(apl);

      
      insert aplList;

      Set<Id> eaId = new Set<Id>();
      eaId.add(ea.Id);

      dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
      bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
      bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

      //system.debug('bp.aplList.size()>>>'+bp.aplList.size());

      For(integer i=0, j = bp.aplList.size(); i <j; i++)
      {
         bp.applObj = bp.aplList[i];
         ApplianceTriggerHandler.updateApplianceSystem(bp);
      }

      dsmtApplianceHelper.ComputeRefrigeratorMeteringMinutesCommon(bp.aplList[0]);
      dsmtApplianceHelper.GetGameConsoleTV(bp);
      dsmtEnergyConsumptionHelper.ComputeHandDishwashingIndoorGainHCommon(bp);

      Test.stopTest();
   }

    @isTest
   static void test3()
   {
      dsmtCreateEAssessRevisionController.stopTrigger = true;
      Energy_Assessment__c ea =  dsmtEAModel.setupAssessment();

      Test.startTest();

      dsmtApplianceHelper.fillSurfaceModal(ea);

      Set<string> idset = new Set<string>();
      idset.add(ea.Building_Specification__c);

      dsmtApplianceHelper.UpdateAllApplianceOnBuildingYearChange(idset);

      idset = new Set<string>();
      idset.add(ea.Id);
      dsmtApplianceHelper.queryBuildingSpecificationMap(idset);


      Test.stopTest();
   }
}