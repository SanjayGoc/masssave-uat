public class DistributionSystemRecommendationHelper
{
    public static String GetDuctMeasureOperation(integer operationType)
    {
        string result  = 'Duct Sealing';

        if(operationType ==1)
        {
            result = 'Pipe Insulation';
        }
        else if(operationType ==2)
        {
            result = 'Duct Sealing';    
        }
        else if(operationType ==3)
        {
                
        }
        else if(operationType ==4)
        {
            result = 'Custom';  
        }
        else if(operationType ==5)
        {
            result = 'Remove Existing';
        }
        else if(operationType ==6)
        {
            result = 'Add Equipment';   
        }
        else if(operationType ==7)
        {
            result = 'ECM Upgrade';     
        }

        return result;
    }

    public static decimal ComputePipeInsulationLength( Recommendation__c dsRecommendation )
    {
        decimal retval = null;

        if ( dsRecommendation.Quantity__c != null && dsRecommendation.PipeInsulationPieceLength__c != null ) 
        {
            retval = dsRecommendation.Quantity__c * dsRecommendation.PipeInsulationPieceLength__c;
        }

        return retval;
    }

    public static decimal ComputeFinalPipeInsulationLength(Recommendation__c dsRecommendation, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( dsRecommendation.RemoveExistingPipeInsulation__c ) 
        {
            retval = dsRecommendation.PipeInsulationLength__c;
        }
        else 
        {
            if ( bp.mstObj != null ) 
            {
                Mechanical_Sub_Type__c liveDS = bp.mstObj;
                if ( liveDS != null ) 
                {
                    retval = dsRecommendation.PipeInsulationLength__c + dsmtEAmodel.ISNULL(liveDS.PipeInsulationAmountValue__c);
                }
            }
            else {
                retval = dsRecommendation.PipeInsulationLength__c;
            }
        }

        return retval;
    }

    public static decimal ComputeRecommendationReductionCommon(Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( rec.Operation__c == 'Duct Sealing' && bp.mstObj != null ) 
        {
            // CSGENG-5083: Set up a dependency on LeakageTestMethod in order to force the Dynamic DCP to run
            //      This makes the GUI redisplay Reduction based on a new scaleFactor
            string ltm = bp.mstObj.Leakage_Test_Method__c;
            //Project recProject = rec.Project;

            system.debug('ltm>>> ' + ltm);

            Blower_Door_Reading__c bdp =  null; //BlowerDoorPair.FirstLinkedTo( rec, recProject );

            if ( bdp != null && bdp.Starting_CFM50__c != null && bdp.Ending_CFM50__c != null ) 
            {
                //CSGENG-5083: Blower-Door-Pair is CFM50. Reduction is CFM25.  Do the conversion.
                retval = (bdp.Starting_CFM50__c - bdp.Ending_CFM50__c) / 1.57;

                // CSGENG-5206: distribute reduction among all distribution systems attached to this blower-door-pair
                decimal recCount = 1; //(decimal)recProject.Recommendations.Where( r => bdp == BlowerDoorPair.FirstLinkedTo( r, recProject ) ).Count();
                retval = retval / recCount;
            }
            else if ( (rec.ActualTestInTotalLeakage__c !=null ||
                rec.ActualTestInSupplyLeakage__c !=null && rec.ActualTestInReturnLeakage__c !=null) &&
            (rec.ActualTestOutTotalLeakage__c !=null || rec.ActualTestOutSupplyLeakage__c !=null && 
                rec.ActualTestOutReturnLeakage__c !=null) ) 
            {
                retval = rec.TestInTotalLeakage__c - rec.TestOutTotalLeakage__c;
            }
            else if ( bp.mstObj != null  && bp.ProductPartData != null ) 
            {
                Mechanical_Sub_Type__c ds = bp.mstObj.clone(true,true,true,true);
                Mechanical_Type__c mech = bp.mechType;

                // Create a DeepCopy of the DistributionSystem.
                // Make sure we don't copy the Recommendations from the original.

                Mechanical_Sub_Type__c copyDS =ds.clone(true,true,true,true);

                DistributionSystem_Part__c part = (DistributionSystem_Part__c) bp.ProductPartData;

                if ( copyDS != null && part != null && ds != null ) 
                {
                    if ( part.IsAeroseal__c == 1 ) 
                    {
                        // if this is an Aeroseal part, then simply set the final value to "tight"                      
                        copyDS.ActualSupplyLeakage__c = null;
                        copyDS.ActualReturnLeakage__c = null;
                        copyDS.ActualTotalLeakage__c = null;                        
                        copyDS.ActualLeakageTestMethod__c = 'Not Tested';

                        copyDS.Leakiness__c = 'Tight';
                    }
                    else if ( copyDS.Total_Leakage__c != null
                                && copyDS.Total_Leakage__c != 0
                                && ds.Leakage_Test_Method__c != null
                                && ltm != 'Not Tested'
                                && ltm != 'Pressure Pan Estimate @50Pa' ) 
                    {

                        decimal newCfm25 = copyDS.Total_Leakage__c;

                        if ( part.CFM25PerHour__c != 0 && rec.Hours__c != 0 && copyDS.Total_Leakage__c !=null ) 
                        {
                            newCfm25 = newCfm25 - (dsmtEAmodel.ISNULL(part.CFM25PerHour__c) * dsmtEAmodel.ISNULL(rec.Hours__c));
                        }

                        // get minimum practical cfm25 (Tight)
                        // ABS e-mail 5/2/11: The total of these two S+R should be “tight”:  0.065 * air flow,
                        decimal minCfm25 = .065 * dsmtEAmodel.ISNULL(ds.Airflow__c);

                        // take the bigger of the two
                        newCfm25 = Math.Max( minCfm25, newCfm25 );

                        // Clear various Actual values after setting the new CFM25. The new CFM25 takes
                        // precedence over all others. Note: This must be done after deciding what the
                        // new value will be.
                        copyDS.ActualSupplyLeakage__c = null;
                        copyDS.ActualReturnLeakage__c = null;
                        copyDS.ActualTotalLeakage__c = null;                        
                        copyDS.Leakiness__c = null;

                        // Now set the CFM25. 
                        copyDS.ActualTotalLeakage__c = newCfm25 * copyDS.LeakageGUITotalConversionFactor__c;
                        copyDS.Total_Leakage__c = copyDS.ActualTotalLeakage__c;
                    }
                    else 
                    {
                    	system.debug('default one');
                        // CSGENG-4029: Any time duct sealing is proposed based on an as-is home that has not gotten a duct test (eg without choosing a 
                        //      "test method" there should be one recommendation available: "Perform duct sealing" or "seal ducts" or the like. 
                        //      The action of implementing this recommendation is to move existing ducts one notch tighter (from leaky to average 
                        //      or from average to tight), plus if any disconnects are checked, they should be un-checked. This should be the behavior in all programs. 
                        copyDS.ActualSupplyLeakage__c = null;
                        copyDS.ActualReturnLeakage__c = null;
                        copyDS.ActualTotalLeakage__c  = null;

                        if( ds.Leakiness__c =='Average' ) 
                        {
                            copyDS.Leakiness__c = 'Tight';
                        }
                        else if(ds.Leakiness__c =='Leaky' )
                        {
                            copyDS.Leakiness__c = 'Average';
                        }
                    }

                    if(bp.mst !=null)
	                {
	                    For(Integer i=0; i< bp.mst.size(); i++)
	                    {
	                        if(bp.mst[i].Id==copyDS.Id)
	                        {
	                            bp.mst[i]=copyDS;
	                            break;
	                        }
	                    }
	                }
	                else {
	                    bp.mst = new List<Mechanical_Sub_Type__c>();
	                    bp.mst.add(copyDS);
	                }

	                bp.mstObj = copyDS;
                }

                dsmtDistributionSystemTriggerHandler.UpdateDistributionSystem(bp);

                system.debug('copyDS.Total_Leakage__c>>>' + copyDS.Total_Leakage__c);
                    
                retval = Math.Max( 0, dsmtEAmodel.ISNULL(ds.Total_Leakage__c - copyDS.Total_Leakage__c));
            }
        }

        return retval;
    }

    public static decimal ComputeTestHours(Recommendation__c recom, dsmtEAmodel.Surface bp, dsmtEAmodel.Surface baseBP)
    {
        Blower_Door_Reading__c bdp = null;

        if ( bdp != null && bdp.Time_Spent__c != null )
            return Decimal.valueof(bdp.Time_Spent__c);
        else
            return recom.Hours__c; // use recommendations planned hours if nothing else set.
    }

    public static decimal ComputeTestInNetTotalLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( bp != null ) 
        {
            decimal leakageGUISupplyConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUISupplyConversionFactor( bp, rec.TestLeakageTestMethod__c );

            decimal leakageGUIReturnConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUIReturnConversionFactor( bp, rec.TestLeakageTestMethod__c );

            decimal supplyLeakFraction = dsmtDistributionSystemHelper.computeDistributionSystemSupplyLeakFraction( bp, rec.TestLeakageTestMethod__c );

            decimal returnLeakFraction = dsmtDistributionSystemHelper.computeDistributionSystemReturnLeakFraction( bp, rec.TestLeakageTestMethod__c );

            decimal leakageGUITotalConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUITotalConversionFactor( supplyLeakFraction, returnLeakFraction, leakageGUISupplyConversionFactor, leakageGUIReturnConversionFactor );

            retval = dsmtEAModel.ISNULL(rec.TestInTotalLeakage__c) / leakageGUITotalConversionFactor;
        }

        return retval;
    }

    public static decimal ComputeTestInTotalLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( rec.ActualTestInSupplyLeakage__c != null && rec.ActualTestInReturnLeakage__c != null ) 
        {
            retval = rec.ActualTestInSupplyLeakage__c + rec.ActualTestInReturnLeakage__c;
        }
        else {

            // CSGENG-3107: Don't compute defaults if leakage test method has been specified
            if ( rec != null && rec.ActualTestLeakageTestMethod__c == null ) {
                if ( rec.TestLeakageTestMethod__c == 'BD Subtraction @50Pa' ) {
                    if ( rec.TestInRegistersTapedCFM50__c != null && 
                         rec.TestInDuctPD__c != null && 
                         rec.TestInWholeHouseCFM50__c != null && 
                         rec.TestInDuctPD__c <= 50 &&
                        (12.7154139391661 - (double) Math.Pow( (double) (50.0 - (double) rec.TestInDuctPD__c), (double) 0.65 )) != 0 ) 
                    {
                        retval = (rec.TestInWholeHouseCFM50__c - rec.TestInRegistersTapedCFM50__c) * (12.7154139391661 / (12.7154139391661 - (decimal) Math.Pow( (double) (50.0 - (double) rec.TestInDuctPD__c), (double) 0.65 )));
                    }
                }
                else {
                    if ( rec.TestInSupplyLeakage__c != null && 
                         rec.TestInReturnLeakage__c != null )
                        retval = rec.TestInSupplyLeakage__c + rec.TestInReturnLeakage__c;
                }
            }
        }

        return retval;
    }

    public static decimal ComputeTestInSupplyLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        // If actualtotal hasvalue
        // return cfm25sOut
        // else return defaultcfm25sout
        if ( rec.ActualTestInTotalLeakage__c != null ) {
            // equivalent of CFM25SOut
            if ( (rec.ActualTestInReturnLeakage__c != null) && (rec.ActualTestInTotalLeakage__c != null) ) {
                retval = rec.TestInTotalLeakage__c - rec.TestInReturnLeakage__c;
            }
            else {
                retval = rec.TestInTotalLeakage__c * (1 - SurfaceConstants.DefaultReturnDuctLeakFraction);
            }
        }
        else {
            // CSGENG-3107: Don't compute defaults if leakage test method has been specified
            if ( rec.ActualTestLeakageTestMethod__c == null ) {
                // equivalent of DefaultCFM25SOut
                if ( bp.mstObj != null ) 
                {
                    decimal defaultCFM25SOut = bp.mstObj.DefaultCFM25S__c;
                    if ( defaultCFM25SOut != null ) 
                    {
                        retval = defaultCFM25SOut * dsmtEAmodel.ISNULL(dsmtDistributionSystemHelper.computeTotalDuctsLeakQualCoeff( bp, true ) , 1);
                    }
                }
            }
        }

        return retval;
    }


    public static decimal ComputeTestInReturnLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        // If actualtotal hasvalue
        // return cfm25rOut
        // else return defaultcfm25sout
        if ( rec.ActualTestInTotalLeakage__c != null ) {
            // equivalent of CFM25ROut
            if ( (rec.ActualTestInSupplyLeakage__c != null) && (rec.ActualTestInTotalLeakage__c != null) ) {
                retval = rec.TestInTotalLeakage__c - rec.TestInSupplyLeakage__c;
            }
            else {
                retval = rec.TestInTotalLeakage__c * SurfaceConstants.DefaultReturnDuctLeakFraction;
            }
        }
        else {
            // CSGENG-3107: Don't compute defaults if leakage test method has been specified
            if ( rec.ActualTestLeakageTestMethod__c == null ) {
                // equivalent of DefaultCFM25ROut
                if ( bp != null ) 
                {
                    decimal defaultCFM25ROut = bp.mstObj.DefaultCFM25R__c;
                    if ( defaultCFM25ROut != null ) 
                    {
                        retval = defaultCFM25ROut * dsmtEAmodel.ISNULL(dsmtDistributionSystemHelper.computeTotalDuctsLeakQualCoeff( bp, false ) , 1);
                    }
                }
            }
        }

        return retval;
    }

    // ComputeTestOutNetTotalLeakage
    // <summary> Compute Total Leakage for TestOut of a DistributionSystemRecommendation</summary>
    public static decimal ComputeTestOutNetTotalLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( baseBP != null ) {
            decimal leakageGUISupplyConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUISupplyConversionFactor( baseBP, rec.TestLeakageTestMethod__c );
            decimal leakageGUIReturnConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUIReturnConversionFactor( baseBP, rec.TestLeakageTestMethod__c );
            decimal supplyLeakFraction = dsmtDistributionSystemHelper.computeDistributionSystemSupplyLeakFraction( baseBP, rec.TestLeakageTestMethod__c );
            decimal returnLeakFraction = dsmtDistributionSystemHelper.computeDistributionSystemReturnLeakFraction( baseBP, rec.TestLeakageTestMethod__c );
            decimal leakageGUITotalConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUITotalConversionFactor( supplyLeakFraction, returnLeakFraction, leakageGUISupplyConversionFactor, leakageGUIReturnConversionFactor );

            retval = rec.TestOutTotalLeakage__c / leakageGUITotalConversionFactor;
        }

        return retval;
    }

    // ComputeTestOutNetSupplyLeakage
    // <summary> Compute Supply Leakage for TestOut of a DistributionSystemRecommendation</summary>
    public static decimal ComputeTestOutNetSupplyLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( baseBP  != null ) {
            decimal leakageGUISupplyConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUISupplyConversionFactor( baseBP, rec.TestLeakageTestMethod__c );

            retval = rec.TestOutSupplyLeakage__c / leakageGUISupplyConversionFactor;
        }

        return retval;
    }

    // ComputeTestOutNetReturnLeakage
    // <summary> Compute Return Leakage for TestOut of a DistributionSystemRecommendation</summary>
    public static decimal ComputeTestOutNetReturnLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( baseBP  != null ) {
            decimal leakageGUIReturnConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUIReturnConversionFactor( baseBP, rec.TestLeakageTestMethod__c );

            retval = dsmtEAModel.ISNULL(rec.TestOutReturnLeakage__c) / leakageGUIReturnConversionFactor;
        }

        return retval;
    }

    // ComputeTestOutTotalLeakage
    // <summary> Compute Total Leakage for TestOut of a DistributionSystemRecommendation</summary>
    public static decimal ComputeTestOutTotalLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        if ( rec.ActualTestOutSupplyLeakage__c != null && rec.ActualTestOutReturnLeakage__c != null ) {
            retval = rec.TestOutSupplyLeakage__c + rec.TestOutReturnLeakage__c;
        }
        else {

            // CSGENG-3107: Don't compute defaults if leakage test method has been specified
            if ( rec != null && rec.ActualTestLeakageTestMethod__c == null ) {
                if ( rec.TestLeakageTestMethod__c == 'BD Subtraction @50Pa' ) {
                    if ( rec.TestOutRegistersTapedCFM50__c != null && rec.TestOutDuctPD__c != null && rec.TestOutWholeHouseCFM50__c != null && rec.TestOutDuctPD__c <= 50 &&
                    (12.7154139391661 - (decimal) Math.Pow( (double) (50.0 - (double)rec.TestOutDuctPD__c), (double) 0.65 )) != 0 ) {
                        // Math.Pow(50,.65) == 12.7154139391661
                        retval = (rec.TestOutWholeHouseCFM50__c - rec.TestOutRegistersTapedCFM50__c) * (12.7154139391661 / (12.7154139391661 - (decimal)Math.Pow( (double) (50.0 - (double)rec.TestOutDuctPD__c), (double) 0.65 )));
                    }
                }
                else {
                    if ( rec.TestOutSupplyLeakage__c != null && rec.TestOutReturnLeakage__c != null )
                        retval = rec.TestOutSupplyLeakage__c + rec.TestOutReturnLeakage__c;
                }
            }
        }

        return retval;
    }

    // ComputeTestOutSupplyLeakage
    // <summary> Compute Supply Leakage for TestOut of a DistributionSystemRecommendation</summary>
    public static decimal ComputeTestOutSupplyLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        // If actualtotal hasvalue
        // return cfm25sOut
        // else return defaultcfm25sout
        if ( rec.ActualTestOutTotalLeakage__c != null ) {
            // equivalent of CFM25SOut
            if ( (rec.ActualTestOutReturnLeakage__c != null) && (rec.ActualTestOutTotalLeakage__c != null) ) {
                retval = rec.TestOutTotalLeakage__c - rec.TestOutReturnLeakage__c;
            }
            else {
                retval = rec.TestOutTotalLeakage__c * dsmtDistributionSystemHelper.computeDistributionSystemSupplyLeakFraction( baseBP, rec.TestLeakageTestMethod__c );
            }
        }
        else {
            // CSGENG-3107: Don't compute defaults if leakage test method has been specified
            if ( rec.ActualTestLeakageTestMethod__c == null ) {
                // equivalent of DefaultCFM25SOut
                if ( bp != null ) {
                    decimal defaultCFM25SOut = bp.mstObj.DefaultCFM25S__c;
                    if ( defaultCFM25SOut != null ) {
                        retval = defaultCFM25SOut * dsmtEAmodel.ISNULL(dsmtDistributionSystemHelper.computeTotalDuctsLeakQualCoeff( bp, true ) , 1);
                    }
                }
            }
        }

        return retval;
    }

    // ComputeTestOutReturnLeakage
    // <summary> Compute Return Leakage for TestOut of a DistributionSystemRecommendation</summary>
    public static decimal ComputeTestOutReturnLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
    {
        decimal retval = null;

        // If actualtotal hasvalue
        // return cfm25rOut
        // else return defaultcfm25sout
        if ( rec.ActualTestOutTotalLeakage__c != null ) {
            // equivalent of CFM25ROut
            if ( (rec.ActualTestOutSupplyLeakage__c != null) && (rec.ActualTestOutTotalLeakage__c != null) ) {
                retval = rec.TestOutTotalLeakage__c - rec.TestOutSupplyLeakage__c;
            }
            else {
                retval = rec.TestOutTotalLeakage__c * dsmtDistributionSystemHelper.computeDistributionSystemReturnLeakFraction( bp, rec.TestLeakageTestMethod__c );
            }
        }
        else {
            // CSGENG-3107: Don't compute defaults if leakage test method has been specified
            if ( rec.ActualTestLeakageTestMethod__c == null ) {
                // equivalent of DefaultCFM25ROut
                if ( bp != null ) 
                {
                    decimal defaultCFM25ROut = bp.mstObj.DefaultCFM25R__c;
                    if ( defaultCFM25ROut != null ) {
                        retval = defaultCFM25ROut * dsmtEAmodel.ISNULL(dsmtDistributionSystemHelper.computeTotalDuctsLeakQualCoeff( bp, false ) , 1);
                    }
                }
            }
        }

        return retval;
    }

    public static decimal ComputeTestInNetSupplyLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
	{
		decimal retval = null;
		Mechanical_Sub_Type__c baseDS = baseBP.mstObj;

		if ( baseDS != null ) 
		{
			decimal leakageGUISupplyConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUISupplyConversionFactor(baseBP, rec.TestLeakageTestMethod__c);

			retval = rec.TestInSupplyLeakage__c / leakageGUISupplyConversionFactor;
		}

		return retval;
	}

	public static decimal ComputeTestInNetReturnLeakageCommon( Recommendation__c rec, dsmtEAmodel.Surface baseBP, dsmtEAmodel.Surface bp )
	{
		decimal retval = null;
		Mechanical_Sub_Type__c baseDS = baseBP.mstObj;

		if ( baseDS != null ) 
		{
			decimal leakageGUISupplyConversionFactor = dsmtDistributionSystemHelper.computeDistributionSystemLeakageGUIReturnConversionFactor(baseBP, rec.TestLeakageTestMethod__c);

			retval = dsmtEAModel.ISNULL(rec.TestInReturnLeakage__c) / leakageGUISupplyConversionFactor;
		}

		return retval;
	}

}