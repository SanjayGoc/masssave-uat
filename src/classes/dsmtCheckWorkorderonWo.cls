global class dsmtCheckWorkorderonWo implements Schedulable,database.batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        
        String query = 'SELECT Id,Employee__c,Dsmtracker_contact__c ,Customer_Reference__c,Eligibility_Check__c,Appointment_Type__c,';
        query += 'Appointment_Start_Time__c,Appointment_End_Time__c,Customer_Eligibility__c,Description__c,unit__c,Appointment_Type_Friendly_Name__c';
        query += ' from Appointment__c where workorder__c = null and Dsmtracker_contact__c != null and Employee__c = null';
        system.debug('--query---'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Appointment__c> scope)
    {
        
        Set<String> woTypes = new Set<String>();
        Set<String> customerIds = new Set<String>();
        Set<String> Eligibilityids = new Set<String>();
        List<Appointment__c> AppointForCreateWo = new List<Appointment__c>();
      
        List<Appointment__c> woToCreate = new List<Appointment__c >();
        
        for (Appointment__c app : scope)
        {
            system.debug('--app.Id---'+app.Id);
            if(app.Employee__c == null && app.Dsmtracker_contact__c != null ){
              if(app.Appointment_Type_Friendly_Name__c != null){
                 woTypes.add(app.Appointment_Type_Friendly_Name__c);
              }
              if(app.Customer_Reference__c != null){
                  customerIds.add(app.Customer_Reference__c);
              }
              if(app.Eligibility_Check__c != null){
                 Eligibilityids.add(app.Eligibility_check__c);
              }
              AppointForCreateWo.add(app);
          }
        }
        
          Map<String,Workorder_Type__c> woTypeMap = new Map<String,Workorder_Type__c>();
      List<Workorder_Type__c> wtypelist = [select id,name from Workorder_Type__c where name in: woTypes];
      if(wtypelist.size() > 0){
          for(Workorder_Type__c wotype : wtypelist){
             woTypeMap.put(wotype.name,wotype);
          }
      }
      
      Map<String,Customer__c> customerMap = new Map<String,Customer__c>([select id,Service_Address_Full__c,Service_City__c,Service_State__c,Service_Postal_Code__c from customer__c where Id in: customerIds]);
      
      Map<String,Eligibility_check__c> eligibilityCheckMap = new Map<String,Eligibility_check__c>([select id,Phone__c,Email__c from Eligibility_check__c where id in: Eligibilityids]);
      
      Map<Id,Workorder__c> newWoMap = new Map<Id,Workorder__c>();
      
      for(Appointment__c app : AppointForCreateWo){
          Workorder__c wo = new Workorder__c();
          wo.Requested_Start_Date__c = app.Appointment_Start_Time__c;
          wo.Requested_End_Date__c = app.Appointment_End_Time__c;
          wo.Requested_Date__c = app.Appointment_Start_Time__c.Date();
        
          wo.Early_Arrival_Time__c = app.Appointment_Start_Time__c;
          wo.Late_Arrival_Time__c = app.Appointment_End_Time__c;
        
          wo.Scheduled_Start_Date__c = app.Appointment_Start_Time__c;
          wo.Scheduled_End_Date__c = app.Appointment_End_Time__c;
          wo.Scheduled_Date__c = wo.Requested_Date__c;
          wo.Manually_Created_Workorder__c = true;
          wo.Fixed_Workteam__c = true;
          wo.Tradeally_workorder__c = true;
          wo.Status__c='Scheduled';
          
          if(woTypeMap.get(app.Appointment_Type_Friendly_Name__c) != null){
             wo.Workorder_Type__c = woTypeMap.get(app.Appointment_Type_Friendly_Name__c).Id;
          }
          
          if(customerMap.get(app.Customer_Reference__c) != null){
             customer__c customer = customerMap.get(app.Customer_Reference__c);
             wo.customer__c = customer.Id;
             wo.Address__c = customer.Service_Address_Full__c;
             wo.City__c = customer.Service_City__c;
             wo.State__c = customer.Service_State__c;
             wo.Zipcode__c = customer.Service_Postal_Code__c;
             wo.Zip__c = customer.Service_Postal_Code__c;
          }
          
          if(eligibilityCheckMap.get(app.Eligibility_Check__c) != null){
             Eligibility_Check__c EC = eligibilityCheckMap.get(app.Eligibility_Check__c);
             wo.Phone__c = EC.phone__c;
             wo.Email__c = EC.Email__c;
             wo.Eligibility_Check__c = EC.id;
          }
          
          wo.HPC__c = true;
          wo.Customer_Eligibility__c = app.Customer_Eligibility__c;
          wo.Notes__c = app.Description__c;
          wo.Unit__c = app.unit__c;
          newWomap.put(app.Id,wo);
      }
      
      system.debug('--newWomap--'+newWomap);
      
      //insert new work order list 
      if(newWomap.size() > 0){
         insert newWomap.values();
      }
      
      for(Appointment__c app : AppointForCreateWo){
         system.debug('--app--'+app);
         Workorder__c wo = newWomap.get(app.Id);
         system.debug('--wo--'+wo);
         app.Workorder__c = wo.id;
      }
      
      update AppointForCreateWo;
      
          
    }  
    global void finish(Database.BatchableContext BC)
    {
    
    }
    
    global void  execute(SchedulableContext sc){
        dsmtCheckWorkorderonWo obj = new dsmtCheckWorkorderonWo();
        database.executebatch(obj,1);    
    }
}