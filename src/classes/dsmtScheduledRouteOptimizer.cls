global class dsmtScheduledRouteOptimizer implements Schedulable{

        global void  execute(SchedulableContext sc){
            
            string name =  Date.Today().Month()+'-'+Date.Today().Day()+'-'+Date.Today().Year();

            Route_Optimization__c ro = new Route_Optimization__c();
            ro.Name = name;
            ro.Service_Date__c = Date.Today();
            ro.Requested_Date_Time__c = System.now();
            ro.Status__c = 'New';
            insert ro;
            
            System.enqueueJob(new QueueClass(ro.Id));

            
        }
}