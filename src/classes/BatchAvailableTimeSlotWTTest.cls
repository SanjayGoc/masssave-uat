@isTest
public class BatchAvailableTimeSlotWTTest {
       public static testMethod void unitTest() {
       Location__c Lc = new Location__c();
       Lc.name = 'test';
       Lc.Use_default_address_for_routing__c = true;
       insert Lc;           
       List<Employee__c> lstEmp = new List<Employee__c>();
       Employee__c Em = new Employee__c( location__c = Lc.Id);
       Em.Employee_Id__c = 'Test';
       Em.name = 'Test';
       Em.Active__c = true;
       Em.Status__c ='Active';    
       lstEmp.add(Em);
       
       Employee__c Em1 = new Employee__c( location__c = Lc.Id);
       Em1.Employee_Id__c = 'Test1';
       Em1.name = 'Test1';
       Em1.Active__c = true;
       Em1.Status__c ='Active';
       lstEmp.add(Em1);
            
       Employee__c Em2 = new Employee__c( location__c = Lc.Id);
       Em2.Employee_Id__c = 'Test2';
       Em2.name = 'Test2';     
       Em2.Active__c = true;
       Em2.Status__c ='Active';   
       lstEmp.add(Em2);
       insert lstEmp;
        
       Work_Team__c Wt = new Work_Team__c();
       Wt.name = 'Test';
       Wt.Captain__c = Em1.Id;
       Wt.Unavailable__c = false;
       wt.Service_Date__c = system.Today();
       wt.Start_Date__c = system.Now();
       wt.End_Date__c = system.now().addDays(12);
       insert wt;
       
       Work_Team__c Wt1 = new Work_Team__c();
       Wt1.name = 'Test';
       Wt1.Captain__c = Em2.Id;
       Wt1.Unavailable__c = false;
       wt1.Service_Date__c = system.Today().addDays(1);
       insert Wt1;
       
       
       //test.stopTest();
       Set<Id> sid = new Set<Id>();
       sid.add(Wt.Id);
       
        Work_Team_Member__c Wtm = new Work_Team_Member__c();
        Wtm.Work_Team__c = Wt.Id;
        Wtm.Employee__c = Em1.Id;
        insert Wtm;
     
       test.startTest(); 
       BatchAvailableTimeSlotWT Bats = new BatchAvailableTimeSlotWT(sid);
       DataBase.executeBatch(Bats);
       test.stopTest();
   }
}