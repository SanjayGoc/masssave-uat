@isTest
public class dsmtScheduleRequestCntrlTest {
	testmethod static void testLoadContactAndTA()
    {
        User u = Datagenerator.CreatePortalUser();
        
        Trade_Ally_Account__c ac = new Trade_Ally_Account__c();
        ac.Name='Test';
        insert ac;
        
        DSMTracker_Contact__c c = new DSMTracker_Contact__c();
        c.Contact__c = u.ContactId;
        c.Trade_Ally_Account__c = ac.id;
        insert c;
        
        Id rcTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
        
        Service_Request__c sr = new Service_Request__c();
        sr.DSMTracker_Contact__c = c.Id;
        sr.RecordTypeId = rcTypeId;
        insert sr;
        
        System.runAs(u) {
            dsmtScheduleRequestCntrl cntrl = new dsmtScheduleRequestCntrl();
        	cntrl.fetchScheduleRequests();
        }
    }
}