public class dsmtBuildingInfoValidationHelper extends dsmtEnergyAssessmentValidationHelper{

    public ValidationCategory validate(String categoryTitle,String uniqueName,Id bsId,ValidationCategory otherValidations,dsmtAirFlowInfoValidationHelper airFlowInfoHelper){
        ValidationCategory building = new ValidationCategory(categoryTitle,uniqueName);
        
        Building_Specification__c bs =[
            SELECT 
                Id,Name,Year_Built__c,Bedromm__c,Occupants__c,Orientation__c,Natural_Gas__c,Is_Manufactured_Home__c,
                Ceiling_Heights__c,Floors__c,Floor_Area__c,Finished_Basement_Area__c,Total_Floor_Area__c,Total_Volume__c,
                Units__c,Dwelling_attached_on_Back__c,Dwelling_attached_on_Bottom__c,Dwelling_attached_on_Front__c,
                Dwelling_attached_on_Left__c,Dwelling_attached_on_Right__c,Dwelling_attached_on_Top__c,Attached__c
            FROM Building_Specification__c 
            WHERE Id=:bsId LIMIT 1
        ];
        
        if(bs.Ceiling_Heights__c != null && bs.Ceiling_Heights__c <= 0){
            bs.Ceiling_Heights__c = null;
        }
        Map<String,String> reqFieldsMap = new Map<String,String>{
            'Year_Built__c'=>'Year Built',
            //'Bedromm__c' => '# Bedrooms',
            'Occupants__c' => '# Occupants',
            //'Floors__c' => '# Floors',
            'Floor_Area__c' => 'Floor Area'
            //'Ceiling_Heights__c' => 'Ceiling Height'
        };
        validateRequiredFields(bs,reqFieldsMap,building);
        
        /* YEAR BUILT VALIDATIONS */
        System.debug('@@@ bs.Year_Built__c : ' + bs.Year_Built__c);
        try{
            /*if(bs.Year_Built__c != null && bs.Year_Built__c.trim() != '' && Integer.valueOf(bs.Year_Built__c) < 1600){
                building.errors.add(new ValidationMessage('Year Built must be greater than equal to 1600'));        
            }else*/ 
            if(bs.Year_Built__c != null && bs.Year_Built__c.trim() != '' && Integer.valueOf(bs.Year_Built__c) > System.Today().year()){
                building.errors.add(new ValidationMessage('Year Built must be less then equal to ' + System.Today().year()));     
            }
            /*if(bs.Year_Built__c != null && bs.Year_Built__c.trim() != '' && Integer.valueOf(bs.Year_Built__c) >= 1600 && Integer.valueOf(bs.Year_Built__c) < 1850 ){
                building.warnings.add(new ValidationMessage('Year Built should probably be greater than equal to 1850'));
            }
            if(bs.Year_Built__c != null && bs.Year_Built__c.trim() != '' && Integer.valueOf(bs.Year_Built__c) == System.Today().year() ){
                building.warnings.add(new ValidationMessage('Year Built should probably be less then equal to ' + (System.Today().year() - 1)));
            }
            if(bs.Year_Built__c != null && bs.Year_Built__c.trim() != '' && Integer.valueOf(bs.Year_Built__c) > 0 && Integer.valueOf(bs.Year_Built__c) < 1978 ){
                otherValidations.warnings.add(new ValidationMessage('Homes built before 1978 may contain lead paint.  Confirm that lead paint is absent or Lead Safe Practices will need to be observed at this site'));
            }*/
        }catch(Exception ex){
            building.errors.add(new ValidationMessage('Year Built must be number.'));
        }
        
        /* BEDROOM VALIDATIONS */
        if(bs.Bedromm__c == null || String.valueOf(bs.Bedromm__c).trim() == '' || Integer.valueOf(bs.Bedromm__c) <= 0){
            building.errors.add(new ValidationMessage('# Bedroom requires an explicit value'));
        }
        /*if(bs.Bedromm__c != null && Integer.valueOf(bs.Bedromm__c) > 10){
            building.warnings.add(new ValidationMessage('# Bedroom should problably be less than equal to 10'));
        }
        if(bs.Bedromm__c != null && Integer.valueOf(bs.Bedromm__c) > 0){
            building.infos.add(new ValidationMessage('To be considered a bedroom a room must be at least 80 square feet, have a closet, a window, and a door separating it from the main living area.'));
        }*/
        
        /* OCCUPANTS VALIDATION */
        if(bs.Occupants__c!= null && Integer.valueOf(bs.Occupants__c) <= 0){
            building.errors.add(new ValidationMessage('# Occupants requires an explicit value'));
        }
        /*if(bs.Occupants__c!= null && Integer.valueOf(bs.Occupants__c) > 20){
            building.warnings.add(new ValidationMessage('# Occupants should probably less than euqal to 20.'));
        }*/
        
        /* Orientation validations */
        /*if(bs.Orientation__c == null || bs.Orientation__c.trim() == ''){
            building.errors.add(new ValidationMessage('Orientation requires an explicit value'));
        }*/
        
        /* Primary Heating Fuel validations */
        /*if(bs.Natural_Gas__c == null || bs.Natural_Gas__c.trim() == ''){
            building.errors.add(new ValidationMessage('Primary heating fuel required an explicit value.'));
        }*/
        
        /* Ceiling Heights validations */ 
        if(bs.Ceiling_Heights__c == null || String.valueOf(bs.Ceiling_Heights__c).trim() == '' || Integer.valueOf(bs.Ceiling_Heights__c) <= 0 ){
            building.errors.add(new ValidationMessage('Ceiling Height requires an explicit value'));
        }
        /*if(bs.Ceiling_Heights__c != null && Integer.valueOf(bs.Ceiling_Heights__c) > 0 && Integer.valueOf(bs.Ceiling_Heights__c) <= 6 ){
            building.warnings.add(new ValidationMessage('Celling height should probably be greater than equal to 6'));
        }else if(bs.Ceiling_Heights__c != null && Integer.valueOf(bs.Ceiling_Heights__c) > 10){
            building.warnings.add(new ValidationMessage('Celling height should probably be less than equal to 10'));
        }*/
        
        /* # Floors VALIDATION */
        if(bs.Floors__c != null && Integer.valueOf(bs.Floors__c) <= 0){
            building.errors.add(new ValidationMessage('# Floor required an explicit value.'));
        }/*else if(bs.Floors__c!= null && Integer.valueOf(bs.Floors__c) > 5){
            building.errors.add(new ValidationMessage('# Floor must be less than equal to 5'));
        }*/
        
        /* Floor Area VALIDATION */
        if(bs.Floor_Area__c == null || String.valueOf(bs.Floor_Area__c) == '' || bs.Floor_Area__c < 0){
            building.errors.add(new ValidationMessage('Floor Area required an explicit value'));
        }else if(bs.Floor_Area__c != null && Integer.valueOf(bs.Floor_Area__c) > 50000){
            //building.errors.add(new ValidationMessage('Floor Area must be less than equal to 20000'));
            building.errors.add(new ValidationMessage('Floor Area must be less than equal to 50000'));
        }else if(bs.Floor_Area__c != null && Integer.valueOf(bs.Floor_Area__c) < 100){
            //building.errors.add(new ValidationMessage('Floor Area must be greater than or equal to 400'));
            building.errors.add(new ValidationMessage('Floor Area must be greater than or equal to 100'));
        }/*else if(bs.Finished_Basement_Area__c != null && bs.Finished_Basement_Area__c > bs.Floor_Area__c){
            building.errors.add(new ValidationMessage('It is unlikely that the area of finished basements is greater than the area of living space floors.'));
        }*/
        
        /*if(bs.Floor_Area__c != null && Integer.valueOf(bs.Floor_Area__c) > 400 && Integer.valueOf(bs.Floor_Area__c) < 650){
            building.warnings.add(new ValidationMessage('Floor Area should probably be greater than or equal to 650'));
        }else if(bs.Floor_Area__c != null && Integer.valueOf(bs.Floor_Area__c) > 10000){
            building.warnings.add(new ValidationMessage('Floor Area should probably be less than or equal to 10000'));
        }*/
        
        decimal defaultTotalVolume = 0;
        if(bs.Floor_Area__c != null && bs.Ceiling_Heights__c != null){
            if(bs.Finished_Basement_Area__c == null){
                defaultTotalVolume = bs.Floor_Area__c * bs.Ceiling_Heights__c ;
            }else{
                defaultTotalVolume = bs.Finished_Basement_Area__c + (bs.Floor_Area__c * bs.Ceiling_Heights__c );
            }
            
            decimal calculatedTotalVolume = ((defaultTotalVolume * 20)/100);
            if(bs.Total_Volume__c == null || String.valueOf(bs.Total_Volume__c).trim() == '' || Integer.valueOf(bs.Total_Volume__c) <= 0){
                building.errors.add(new ValidationMessage('Total Volume must be within 20% under ' + Integer.valueOf(defaultTotalVolume)));
            }else if(bs.Total_Volume__c != null && Integer.valueOf(bs.Total_Volume__c) > 0){
                if(bs.Total_Volume__c > (calculatedTotalVolume + defaultTotalVolume)){
                    building.errors.add(new ValidationMessage('Total Volume must be within 20% over DefaultTotalVolume (' + Integer.valueOf(defaultTotalVolume) + ')'));
                }else if(bs.Total_Volume__c < (defaultTotalVolume - calculatedTotalVolume)){
                    building.errors.add(new ValidationMessage('Total Volume must be within 20% under DefaultTotalVolume (' + Integer.valueOf(defaultTotalVolume) + ')'));               
                }
                
                calculatedTotalVolume = ((defaultTotalVolume * 10)/100);
                if(bs.Total_Volume__c < (defaultTotalVolume - calculatedTotalVolume)){
                    building.warnings.add(new ValidationMessage('Total Volume should be within 10% under DefaultTotalVolume (' + Integer.valueOf(defaultTotalVolume) + ')'));                           
                }else if(bs.Total_Volume__c > (calculatedTotalVolume + defaultTotalVolume)){
                    building.warnings.add(new ValidationMessage('Total Volume should be within 10% over DefaultTotalVolume (' + Integer.valueOf(defaultTotalVolume) + ')'));                                       
                }
            }
        }
        
        building.addCategory(airFlowInfoHelper.validate('Air Flow','AirFlowTabValidations',bs,otherValidations));
        return building;
    }
}