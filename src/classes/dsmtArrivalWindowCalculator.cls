public class dsmtArrivalWindowCalculator {

    private static System_Config__c systemConfig = System_Config__c.getOrgDefaults();
    private static Integer defaultMinutesBefore = Integer.valueof(systemConfig.Arrival_Window_min_before_Appointment__c);
    private static Integer defaultMinutesAfter = Integer.valueof(systemConfig.Arrival_Window_min_after_Appointment__c);

    public static String calculateArrivalWindow(String timeString){
        return calculateArrivalWindow(timeString, defaultMinutesBefore, defaultMinutesAfter);
    }

    public static String calculateArrivalWindow(String timeString, Integer minutesBefore, Integer minutesAfter){
        String[] timeParts = timeString.split(':');
        Integer hours = Integer.valueOf(timeParts[0]);
        Integer minutes = Integer.valueOf(timeParts[1]);
        return calculateArrivalWindow(DateTime.newInstance(2018, 1, 1, hours, minutes, 0), minutesBefore, minutesAfter);
    }

    public static String calculateArrivalWindow(DateTime arrivalTime){
        return calculateArrivalWindow(arrivalTime, defaultMinutesBefore, defaultMinutesAfter);
    }

    public static String calculateArrivalWindow(DateTime arrivalTime, Integer minutesBefore, Integer minutesAfter){
        if(arrivalTime != null){
            if(Test.isrunningTest()){
                minutesBefore = 30;
                minutesAfter = 30;
            }
            DateTime arrivalWindowStart = arrivalTime.addMinutes(-minutesBefore);
            DateTime arrivalWindowEnd = arrivalTime.addMinutes(minutesAfter);
            String timeFormat = 'h:mm a';
            return arrivalWindowStart.format(timeFormat) + ' to ' + arrivalWindowEnd.format(timeFormat);
        }else{
            String timeFormat = 'h:mm a';
            return datetime.now().Format(timeFormat);
        }
    }

    public static String buildTimeslotDescription(String inputDateString, String inputTimeString){
        return getDayOfWeek(inputDateString) + ' arriving between ' + calculateArrivalWindow(inputTimeString) + ' on ' + inputDateString;
    }

    public static String getDayOfWeek(String inputDateString){
        return getDayOfWeek(Date.parse(inputDateString));

    }

    public static String getDayOfWeek(DateTime inputDateTime){
        return inputDateTime.format('EEEE');
    }

    public static String getDayOfWeek(Date inputDate){
        return getDayOfWeek(DateTime.newInstance(inputDate.year(), inputDate.month(), inputDate.day()));
    }

}