@isTest
public class dsmtServiceRequestsCntrlTest {

    static testmethod void test(){
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = hexDigest +'@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id,
        Billing_Account_Number__c= 'Gas-~~~1234'
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        
        hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        hexDigest = EncodingUtil.convertToHex(hash);
        
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile Limit 1];
        User user1 = new User(
        Username = hexDigest +'@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
       
        System.runAs (user1) { 
        Workorder__c wo = Datagenerator.createWo();
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();
        insert wo;

        Premise__c pr = Datagenerator.createPremise();
        insert pr;
        
        Customer__c c = Datagenerator.createCustomer(portalAccount1.Id, pr.Id);
        insert c;
        
        Trade_Ally_Account__c tac = Datagenerator.createTradeAccount();
            
        DSMTracker_Contact__c dc = new DSMTracker_Contact__c();
        dc.Contact__c = user1.ContactId;
        dc.Trade_Ally_Account__c = tac.Id; 
        insert dc;
            
        Id srRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Phase I Ticket Detail').getRecordTypeId();
        Service_Request__c sr = new Service_Request__c();
        sr.Account__c = portalAccount1.Id;
        sr.Trade_Ally_Account__c = tac.Id;
        sr.Customer__c = c.Id;
        sr.RecordTypeId = srRecordId;
        sr.DSMTracker_Contact__c = dc.Id;
        insert sr;
            
        Id ptdRecordId = Schema.SObjectType.Phase_I_Ticket_Detail__c.getRecordTypeInfosByName().get('Early Boiler & Furnace Request').getRecordTypeId();
        Phase_I_Ticket_Detail__c phtkt = new Phase_I_Ticket_Detail__c();
        phtkt.Service_Request__c = sr.Id;
        phtkt.RecordTypeId = ptdRecordId;
        insert phtkt;
        
        
        
        
        dsmtServiceRequestsCntrl controller = new dsmtServiceRequestsCntrl();
        controller.initMessages();
        controller.backToMessage();
            
        controller.getTicketPriority();
        controller.getTicketStatus();
        controller.getTicketType();
        dsmtServiceRequestsCntrl.queryMessage(sr.Id);
        dsmtServiceRequestsCntrl.saveRespond('Test','Heat Loan', 'In Process','Medium', 'Test1', tac.Id, dc.Id);
       
       
        } 
        
        
        
    }
}
}