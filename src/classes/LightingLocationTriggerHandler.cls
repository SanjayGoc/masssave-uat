public class LightingLocationTriggerHandler extends TriggerHandler implements ITrigger{
    
    public void bulkBefore() 
    {

        if (trigger.isInsert) 
        {
            //Here we will call before insert actions
            calculateLightingSavings(trigger.new, (Map<Id, Lighting_Location__c>) trigger.oldmap);

        } else if (trigger.isUpdate) 
        {
            //Here we will call before update actions
            calculateLightingSavings(trigger.new, (Map<Id, Lighting_Location__c>) trigger.oldmap);

        } 
        else if (trigger.isDelete) 
        {
            //Here we will call before delete actions
            UpdateLighting(trigger.old);
            
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() 
    {
        if (trigger.isInsert) 
        {        
            UpdateLighting(trigger.new);
        } 
        else if (trigger.isUpdate) 
        {         
            UpdateLighting(trigger.new);
        } 
        else if (trigger.isDelete) {
            //Here we will call after delete actions
        } 
        else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }

    //@future
    public static void UpdateLighting(List<Lighting_Location__c> ls)
    {
        if(ls ==null) return;
        
        Set<Id> parentId = new Set<Id>();

        For(Lighting_Location__c l : ls)
        {
            if(l.Lighting__c ==null) continue;

            parentId.add(l.Lighting__c);
        }

        if(parentId ==null || parentId.size()==0) return;

        List<Lighting__c> lightList = new List<Lighting__c>();
        For(Id i: parentId)
        {
            lightList.add(new Lighting__c(id=i));
        }

        if(lightList ==null || lightList.size()==0) return;

        update lightList;

    }
    
    public static void calculateLightingSavings(list<Lighting_Location__c> newList, Map<Id, Lighting_Location__c> oldMap)
    {
       
        Set<Id> eaIdSet = new Set<Id>();
        Set<String> lightingIdSet = new Set<String>();
        
        for(Lighting_Location__c a : newList)
        {
            lightingIdSet.add(a.Lighting__c);
            eaIdSet.add(a.Energy_Assessment__c);
        }
        
        dsmtEAModel.Surface bp  = dsmtEAModel.InitializeBuildingProfile(eaIdSet);
        if(bp==null) return;

       

        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        /// Assign the current variable to list
        integer idx =0;
        for(Lighting_Location__c a : newList)
        {
            if(bp.lighlocList == null || bp.lighlocList.size()==0 || a.Id ==null)
            {

               if(bp.lighlocList ==null) bp.lighlocList = new List<Lighting_Location__c>();

                bp.lighlocList.add(a);
            }
            else {
                For(Lighting_Location__c l : bp.lighlocList)
                {
                    if(l.Id== a.Id)
                    {
                        bp.lighlocList[idx] = a;
                    }

                    idx++;
                }
            }            
        }

        for(Lighting_Location__c a : newList)
        {
            bp.lightLocObj =a;

            if(a.Location__c ==null)
                a.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);

            bp.Location = a.Location__c;

            a.Year__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectYearCommon(bp);
            
            a.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
            bp.LocationSpace = a.LocationSpace__c;

            bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
            bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
            bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

            a.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
            bp.LocationType = a.LocationType__c;

            a.RegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');

            a.RegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');

            a.ExpectedUsefuleLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedUsefulLifeCommon('LightBulbSet');

            a.ExpectedRemainingLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedRemainingLifeCommon(a.ExpectedUsefuleLife__c, a.Year__c);

            a.Efficiency__c = 1;
            a.Load__c = dsmtLightingHelper.ComputeLightBulbSetLoadCommon(bp);

            bp.Load = a.Load__c;
            bp.FuelType = 'Electricity';
            bp.FuelUnits = 'kWh';
            bp.Efficiency = a.Efficiency__c;
            
            a.EnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'H');
            a.EnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'C');
            a.EnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'Sh');
            
            bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
            bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
            bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;
        
            a.FuelConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'H');
            a.FuelConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'C');
            a.FuelConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'Sh');
            
            a.AnnualEnergyConsumption__c =bp.AnnualEnergyConsumption;
            a.AnnualFuelConsumption__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualFuelConsumptionCommon(bp);

            a.IndoorGainPlugC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'C');
            a.IndoorGainPlugH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'H');
            a.IndoorGainPlugCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'CLat');

            a.IndoorGainC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'C');
            a.IndoorGainH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'H');
            a.IndoorGainCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'CLat');

            date dt = Date.today();

            /// Update Parent Lighting record to correct its usage
            if(bp.genLignList !=null && bp.genLignList.size()!=0)            
            {
                For(Lighting__c ls : bp.genLignList )
                {
                    if(ls.Id == a.Lighting__c)
                    {
                        bp.genLigtObj = ls;
                        LightingTriggerHandler.UpdateDefaultLight(bp);
                    }
                }
            }

            bp.Load = a.Load__c;
            bp.FuelType = 'Electricity';
            bp.FuelUnits = 'kWh';
            bp.Efficiency = a.Efficiency__c;

            bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
            bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
            bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;
            
            bp.FuelUsage = dsmtEnergyConsumptionHelper.GetFuelUsageList(bp, dt);

            a.AnnualOperatingCost__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualOperatingCostCommon(bp);
            
        }
    }

    public static Lighting_Location__c ApplyLightingLocationRecommendation(dsmtEAModel.Surface bp, Lighting_Location__c a)
    {
        bp.Location = a.Location__c;

        a.Year__c = bp.rcmd.Pricing_Effective_Date__c.Year();
        
        a.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
        bp.LocationSpace = a.LocationSpace__c;

        bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
        bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
        bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

        a.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
        bp.LocationType = a.LocationType__c;

        a.RegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');

        a.RegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');

        a.ExpectedUsefuleLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedUsefulLifeCommon('LightBulbSet');

        a.ExpectedRemainingLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedRemainingLifeCommon(a.ExpectedUsefuleLife__c, a.Year__c);

        a.Efficiency__c = 1;
        a.Load__c = dsmtLightingHelper.ComputeLightBulbSetLoadCommon(bp);

        bp.Load = a.Load__c;
        bp.FuelType = 'Electricity';
        bp.FuelUnits = 'kWh';
        bp.Efficiency = a.Efficiency__c;
        
        a.EnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'H');
        a.EnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'C');
        a.EnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'Sh');
        
        bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
        bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
        bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;
    
        a.FuelConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'H');
        a.FuelConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'C');
        a.FuelConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'Sh');
        
        a.AnnualEnergyConsumption__c =bp.AnnualEnergyConsumption;
        a.AnnualFuelConsumption__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualFuelConsumptionCommon(bp);

        a.IndoorGainPlugC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'C');
        a.IndoorGainPlugH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'H');
        a.IndoorGainPlugCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'CLat');

        a.IndoorGainC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'C');
        a.IndoorGainH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'H');
        a.IndoorGainCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'CLat');

        date dt = Date.today();

        /// Update Parent Lighting record to correct its usage
        /*if(bp.genLignList !=null && bp.genLignList.size()!=0)            
        {
            For(Lighting__c ls : bp.genLignList )
            {
                if(ls.Id == a.Lighting__c)
                {
                    bp.genLigtObj = ls;
                    LightingTriggerHandler.UpdateDefaultLight(bp);
                }
            }
        }*/

        bp.Load = a.Load__c;
        bp.FuelType = 'Electricity';
        bp.FuelUnits = 'kWh';
        bp.Efficiency = a.Efficiency__c;

        bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
        bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
        bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;
        
        bp.FuelUsage = dsmtEnergyConsumptionHelper.GetFuelUsageList(bp, dt);

        a.AnnualOperatingCost__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualOperatingCostCommon(bp);

        return a;
    }
}