@isTest
Public class dsmtAppointmentControllerHPCTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        insert obj;
        
        Workorder_Type__c wotp = new Workorder_Type__c(Name='test',Est_Work_Time__c=2);
        insert wotp;
        
        Location__c loc= Datagenerator.createLocation();
        insert loc;
        Employee__c em = Datagenerator.createEmployee(loc.id);
        em.Employee_Id__c = 'test123';
        em.Status__c='Approved';
        insert em;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, em.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Workorder_Type__c = wotp.Id;
        wo.Early_Arrival_Time__c = DateTime.now();
        wo.Late_Arrival_Time__c = DateTime.now().addMinutes(120);
        insert wo;
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        Appointment__c app=Datagenerator.createAppointment(EL.Id);
        app.Cancellation_Date__c=date.Today();
        app.Notes__c='test';
        app.DSMTracker_Contact__c=dsmt.id;
        app.Workorder__c = wo.Id;
        app.Customer_Reference__c=cust.id;
         update app;
        Appointment__c app1= Datagenerator.createAppointment(EL.Id);
       // app1.Trade_Ally_Account__c = Tacc.Id;
       // app1.DSMTracker_Contact__c=dsmt.id;
       
        //app1.Workorder__c = wo.Id;
        
      //   update app1;
        //  dsmt.Contact__c= cont.id;
        //update dsmt;
        
      Test.startTest();
        dsmtAppointmentControllerHPC controller = new dsmtAppointmentControllerHPC();
        Date dt = Date.today();
        controller.appStartDate=app;
        controller.Selectedstatus = 'Assessments';
        controller.selectedAppointmentId=app.Id;
        controller.editApp = app1;
        controller.ta = Tacc;
        controller.startTme = dt.month()+'/'+dt.day()+'/'+dt.year()+' 18:00';
        controller.endTme = dt.month()+'/'+dt.day()+'/'+dt.year()+' 20:00';
        controller.woId = app.Id;
        controller.wtId = wt.Id;
        controller.SelectedAppointmentType ='test';
        controller.AppointmentId=app.id;
        controller.GetContactInfo();
        controller.CancenAppointment();
        controller.queryAppointment();
        controller.updateAppoinment();
        app1.Trade_Ally_Account__c = Tacc.Id;
        app1.DSMTracker_Contact__c=dsmt.id;
        app1.Workorder__c = wo.Id;
        update app1;
         controller.editApp = app1;
         controller.updateAppoinment();
        controller.SaveUnAvailableAppointment();
        controller.fetchworkorder();
        controller.generateNewEnergyAssesmentURL();
        controller.gotoRestartEnergyAssesment();
        controller.startInspection();
        app.Workorder__c = wo.Id;
        update app;
        controller.getWorkTypeslist();
          
      //  controller.fetchworkorder();
        Test.stopTest();
      //  controller.cloneCallScriptLineItem(obj.Id);
        
    }
      static testMethod void case2(){
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        system.debug('##########' + hexDigest );
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = hexDigest + 'test2@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id,
        Billing_Account_Number__c= 'Gas-~~~1234'
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile where Name ='HPC Office' Limit 1];
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
       // ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
       
        System.runAs ( user1) {
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Internal_Account__c = true;
        update Tacc;
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
         dsmt.Contact__c= user1.contactID;
         dsmt.Trade_Ally_Account__c=Tacc.id;
         dsmt.Portal_User__c = user1.Id;
        // dsmt.Super_User__c=true;
         update dsmt;
         Appointment__c app= Datagenerator.createAppointment(EL.Id);
         app.DSMTracker_Contact__c=dsmt.id;
         app.Appointment_Status__c='Scheduled';
        
         update app;
       // ApexPages.currentPage().getParameters().put('uid',user1.id);
        dsmtAppointmentControllerHPC controller = new dsmtAppointmentControllerHPC();
       // dsmtAppointmentController.EventsWrapper appoint = new dsmtAppointmentController.EventsWrapper();
        controller.GetContactInfo();
        controller.FillSelectOption();
        controller.Selectedstatus ='Installations';
        controller.SelectedAppointmentType ='All Assessments';
        controller.GetAppointments();
        
      //  controller.initScheduledAppointments();
      //  controller.TilesCounting();
        
        }
        }
       // controller2.gridpage();
    }
    static testMethod void case3(){
      UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        system.debug('##########' + hexDigest );
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = hexDigest + 'test2@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id,
        Billing_Account_Number__c= 'Gas-~~~1234'
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile where Name ='IIC User' Limit 1];
        User user1 = new User(
        Username = System.now().millisecond() + 'test12345@test.com',
       // ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
       
        System.runAs ( user1) {
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Internal_Account__c = true;
        update Tacc;
         Review__c rev=new Review__c();
        rev.Status__c = 'Scheduled';
        rev.Schedule_Date__c=date.Today();
        rev.Schedule_Duration__c='Full Day';
        rev.Schedule_Window__c='AM';
        insert rev;
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
         dsmt.Contact__c= user1.contactID;
         dsmt.Trade_Ally_Account__c=Tacc.id;
         dsmt.Portal_User__c = user1.Id;
        // dsmt.Super_User__c=true;
         update dsmt;
         Appointment__c app= Datagenerator.createAppointment(EL.Id);
         app.DSMTracker_Contact__c=dsmt.id;
         app.Appointment_Status__c='Scheduled';
         update app;
       // ApexPages.currentPage().getParameters().put('uid',user1.id);
        dsmtAppointmentControllerHPC controller = new dsmtAppointmentControllerHPC();
        controller.GetContactInfo();
        controller.Selectedcontacts='test';
        controller.GetAppointments();
        controller.FillSelectOption();
    
        }
        
      
    
        
        }
        
        
    }
    
      static testMethod void case4(){
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        system.debug('##########' + hexDigest );
        
        Profile profile1 = [Select Id from Profile where name = 'DSMTracker Dispatch Manager'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = hexDigest + 'test2@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
      
        System.runAs ( portalAccountOwner1 ) {
       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Internal_Account__c = true;
        update Tacc;
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
         dsmt.Trade_Ally_Account__c=Tacc.id;
         update dsmt;
         Appointment__c app= Datagenerator.createAppointment(EL.Id);
         app.DSMTracker_Contact__c=dsmt.id;
         app.Appointment_Status__c='Scheduled';
         app.Appointment_Type__c='All Assessments';
        
         update app;
        dsmtAppointmentControllerHPC controller = new dsmtAppointmentControllerHPC();
        controller.GetContactInfo();
        controller.FillSelectOption();
        
        }
        
       // controller2.gridpage();
    }
    
}