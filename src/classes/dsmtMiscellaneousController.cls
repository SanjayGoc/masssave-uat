public class dsmtMiscellaneousController extends dsmtEnergyAssessmentBaseController {
    public Miscellaneous_Part__c newPart{get;set;}
    
    public dsmtMiscellaneousController()
    {
    
    }
    
    public list<SelectOption> getPartCategoryOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Miscellaneous_Part__c.Category__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    
    private Integer nextAutoNumber(String category,String type){
        Integer next = 1;
        Map<String,List<Miscellaneous_Part__c>> m = getMiscellaneousPartsByCategory();
        if(m.containsKey(category)){
            for(Miscellaneous_Part__c apl : m.get(category)){
                if(apl.Type__c == type){
                    next++;
                }
            }
        }
        return next;
    }
    
    public Map<String,List<Miscellaneous_Part__c>> getMiscellaneousPartsByCategory(){
        Map<String,List<Miscellaneous_Part__c>> m = new Map<String,List<Miscellaneous_Part__c>>();
        if(ea != null && ea.Id != null){
            String eaId = ea.Id;
            for(Miscellaneous_Part__c part : Database.query('SELECT ' + getSObjectFields('Miscellaneous_Part__c') + ' FROM Miscellaneous_Part__c WHERE Energy_Assessment__c=:eaId')){
                if(part.Category__c != null){
                    List<Miscellaneous_Part__c> tempList = m.get(part.Category__c);
                    if(tempList == null){
                        tempList = new List<Miscellaneous_Part__c>();
                    }
                    tempList.add(part);
                    m.put(part.Category__c, tempList);
                }
            }   
        }
        return m;
    }
    
    public decimal partAmount {get;set;}
    public void addNewMiscellaneousPart(){
        partAmount = 0;
        
        String category = getParam('category');
        String type = getParam('type');
        
        List<DSMTracker_Product__c> dsmtProductList = [select id, Eversource_Labor_Price__c 
                                                       from DSMTracker_Product__c
                                                       where Name =: type
                                                       limit 1];
        
        if(dsmtProductList.size() > 0 && dsmtProductList[0].Eversource_Labor_Price__c != null)
            partAmount = dsmtProductList[0].Eversource_Labor_Price__c;
        
        try{
            Integer nextNumber = nextAutoNumber(category,type);
            this.newPart = new Miscellaneous_Part__c(
                Category__c = category,
                Quantity__c = 1,
                Type__c = type,
                DSMTracker_Product__c = (dsmtProductList.size() > 0 ? dsmtProductList[0].Id : null),
                Total_Cost__c = partAmount,
                Energy_Assessment__c = ea.Id,
                Name = type + ' ' + nextNumber
            );
            INSERT this.newPart;
            if(this.newPart.Id != null){
                String partId= this.newPart.Id;
                List<Miscellaneous_Part__c> parts = Database.query('SELECT ' + getSObjectFields('Miscellaneous_Part__c') + ' FROM Miscellaneous_Part__c WHERE Id=:partId');
                if(parts.size() > 0){
                    this.newPart = parts.get(0);
                }
            }
        }catch(Exception ex){
            System.debug('EXCEPTION :: ' + ex.getMessage());
            this.newPart = null;
        }
    }
    
    public void savePart(){
        if(this.newPart != null){
            if(partAmount != null && this.newPart.Quantity__c != null)
                this.newPart.Total_Cost__c = partAmount * this.newPart.Quantity__c;
                
            UPDATE this.newPart;
            this.newPart = null;
        }
    }
    
    public void editPart(){
        String partId = getParam('partId');
        if(partId != null && partId.trim() !=''){
            List<Miscellaneous_Part__c> parts = Database.query('SELECT ' + getSObjectFields('Miscellaneous_Part__c') + ' FROM Miscellaneous_Part__c WHERE Id=:partId');
            if(parts.size() > 0){
                this.newPart = parts.get(0);
                
                List<DSMTracker_Product__c> dsmtProductList = [select id, Eversource_Labor_Price__c 
                                                       from DSMTracker_Product__c
                                                       where Name =: this.newPart.Type__c
                                                       limit 1];
        
                if(dsmtProductList.size() > 0 && dsmtProductList[0].Eversource_Labor_Price__c != null)
                    partAmount = dsmtProductList[0].Eversource_Labor_Price__c;
            }
        }else{
            this.newPart = null;
        }
    }
    
    public void deletePart(){
        String partId = getParam('partId');
        if(partId != null){
            DELETE new Miscellaneous_Part__c(Id = partId);
        }
    }
}