public class CreateNewProjectReviewController
{
    public String displayMessage {get;set;}
    public Project_Review__c projectReview {get;set;}
    public Boolean isError {get;set;}
    public Boolean isvalidation {get;set;}
    public String recId;
    
    public CreateNewProjectReviewController(ApexPages.StandardController ctrl){
    
        String reviewId = ApexPages.currentPage().getParameters().get('rid');
        String projectId = ApexPages.currentPage().getParameters().get('pid');
        
        projectReview = new Project_Review__c();
        
        if(reviewId != null) {
            projectReview.review__c = reviewId;
            recId = reviewId;
        }
            
        if(projectId != null)
        {
            projectReview.Project__c = projectId;
            recId = projectId;
        }
            
        displayMessage = '';
        isError = false;
        isvalidation = false;
    }
    
    public pageReference beforesave(){
        displayMessage = '';
        isError = false;
        isvalidation = false;
        
        if(projectReview.Project__c != null && projectReview.review__c!= null){
            list<Recommendation_Scenario__c> projectlist = [Select Id,Energy_Assessment__c,Customerid__c From Recommendation_Scenario__c WHERE Id =: projectReview.Project__c]; 
            list<review__c> reviewlist = [Select Id,Energy_Assessment__c,Customer__c From review__c WHERE Id =: projectReview.review__c]; 
            
            if(projectlist.get(0).Customerid__c != reviewlist.get(0).Customer__c) {
                displayMessage = 'The Project Review cannot be created because the Customer is not matching.';
            }
            else if(projectlist.get(0).Energy_Assessment__c != reviewlist.get(0).Energy_Assessment__c) 
            {
                displayMessage = 'The Energy Assessment on the Project and Review does not match. Are you sure you want to link the Project and Review?';
                isvalidation = true;
            }
        } else {
            displayMessage = 'The Project Review cannot be created because the Customer is not matching.';
        }
        
        if(displayMessage != ''){
            isError = true;
            return null;
        } else {
            insert projectReview;
        }
        
        return new pageReference('/' + projectReview.Id);
    }
    
    public pageReference Save(){
        insert projectReview;
        return new pageReference('/' + projectReview.Id);
    }
    
    public pageReference cancel(){
        
        return new pageReference('/' + recId);
    }
    
    
    
}