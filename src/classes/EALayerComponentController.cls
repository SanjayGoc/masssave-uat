public with sharing class  EALayerComponentController {
    public Id layerId{get;set;}
    public transient String headerColorCode{get;set;}
    public String parentType {get;set;}
    public Layer__c layer{get{
        if(layer == null){
            layer = new Layer__c();
        }
        if(layer != null && layerHeaderColorMap.containsKey(layer.RecordType.Name)){
            headerColorCode = layerHeaderColorMap.get(layer.RecordType.Name);
        }else{
            headerColorCode = '#f2f2f2';
        }
        return layer;
    }set;}
    public List<Schema.FieldSetMember> getLayerFields(){
        List<Schema.FieldSetMember> layerFields = new List<Schema.FieldSetMember>();
        if(layer!=null && layer.Type__c != null){
            system.debug('layer.Type__c :::::'+ layer.Type__c);
            layerFields = getFieldSet(layer.Type__c.replaceAll(' ','_')).getFields();
        }else{
            layerFields = new List<Schema.FieldSetMember>();
        }
        return layerFields;
    }
    private static Map<String,String> layerHeaderColorMap = new Map<String,String>{
        'Flooring' => '#ffd9cc',
        'Insulation'=>'#fff2cc',
        'Masonry'=>'#f2ffcc',
        'Other'=>'#d9ffcc',
        'Plaster'=>'#ccffd9',
        'Roofing'=>'#ccfff2',
        'Sheathing'=>'#cce6ff',
        'Siding'=>'#ccccff',
        'Unknown'=>'#e6ccff',
        'Wallboard'=>'#ffccff',
        'Wood Frame'=> '#e6ccb3'//'#ffcccc'
    };
    
    public transient String insulAmountValue{get;set;}
    public void fillInsulAmountRValuesMap(){
        if(layer != null && layer.Id != null){
            if(layer.Wall__c != null){
                Wall__c wall = [SELECT Id,Name,Type__c FROM Wall__c WHERE Id=:layer.Wall__c];
                
                if(wall.Type__c == 'Gable Wall' || wall.Type__c == 'Knee Wall'){
                    Map<string,decimal> tmap = new map<string,decimal>();
                    Map<string, CommonAtticInsulationAmountRValue__c> csMap = CommonAtticInsulationAmountRValue__c.getall();
                    Set<string> keys = csMap.keySet();
                    for(string t : keys){
                        CommonAtticInsulationAmountRValue__c airv = csMap.get(t);
                        tmap.put(t,airv.Value__c);
                    }
                    insulAmountValue = scanInsulationAmountDecimalDictionary(tmap, layer.CavityRValue__c);
                }else if (wall.Type__c == 'Basement Wall' || 
                            wall.Type__c == 'Basement Exterior' ||
                            wall.Type__c == 'Basement Buffer' ||
                            wall.Type__c == 'Basement Other Dwelling' ||
                            wall.Type__c == 'Interior Other Dwelling' ||
                            wall.Type__c == 'Rim Joist' ||
                            wall.Type__c == 'Basement Rim Joist') {
                    Map<string,decimal> tmap = new map<string,decimal>();
                    Map<string, WallInsulationAmountRValue__c> csMap = WallInsulationAmountRValue__c.getall();
                    Set<string> keys = csMap.keySet();
                    for(string t : keys)
                    {
                        WallInsulationAmountRValue__c sirv = csMap.get(t);
                        tmap.put(t,sirv.Value__c);
                    }
                    insulAmountValue = scanInsulationAmountDecimalDictionary(tmap, layer.CavityRValue__c);
                }
            }else if(layer.Thermal_Envelope_Type__c != null && layer.Thermal_Envelope_Type__r.RecordType.Name != null){
                Set<String> tetRecordTypeNames = new Set<String>{
                    'Exterior Wall','Garage Wall','Interior Buffer Wall','Band Joist'
                };
                if(tetRecordTypeNames.contains(layer.Thermal_Envelope_Type__r.RecordType.Name)){
                    Map<string,decimal> tmap = new map<string,decimal>();
                    Map<string, WallInsulationAmountRValue__c> csMap = WallInsulationAmountRValue__c.getall();
                    Set<string> keys = csMap.keySet();
                    for(string t : keys)
                    {
                        WallInsulationAmountRValue__c sirv = csMap.get(t);
                        tmap.put(t,sirv.Value__c);
                    }
                    insulAmountValue = scanInsulationAmountDecimalDictionary(tmap, layer.CavityRValue__c);   
                }
            }
        }
    }
    public static string scanInsulationAmountDecimalDictionary( Map<string, decimal> idd, decimal ival){
        string retval = 'None';
        List<String> keyList = new List<String>{'Fills Cavity', 'Lots', 'None', 'Some', 'Standard', 'Super', 'Unknown'};
        if ( ival != 0 ) 
        {               
            for(string key : keyList)
            {
                // skip None
                if ( key == 'None' ) continue;
                // skip Unknown
                if ( key == 'Unknown' ) continue;
                // skip FillsCavity
                if ( key == 'Fills Cavity' ) continue;

                // initialize with first valid entry...
                if ( retval == 'None'){
                    retval = key;
                }
                // get last entry that's smaller or equal to the number looked for...
                if ( ival >= idd.get(key) ){
                    system.debug('current key : ' + key + ' ' + idd.get(Key));
                    retval = key;
                }
            }
        }
        return retval;
    }
    public EALayerComponentController(){
        if(layerId != null){
            String query = 'SELECT '+ getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Id=:layerId';
            List<Layer__c> layers = Database.query(query);
            if(layers.size() > 0){
                layer = layers.get(0);
            }
        }
        fillInsulAmountRValuesMap();
    }
    
    private static Schema.DescribeSObjectResult dsr = Layer__c.sObjectType.getDescribe();
    private static Schema.FieldSet getFieldSet(String fieldSetName){
        Schema.FieldSet fset = null;
        Map<String,Schema.FieldSet> fsMap = dsr.fieldSets.getMap();
        for(String key : fsMap.keySet()){
            if(key.equalsIgnoreCase(fieldSetName)){
                fset = fsMap.get(fieldSetName);
            }
        }
        return fset;
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        List<String> fields = new List<String>();
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            fields.add(dfield.getName());
        }
        return String.valueOf(fields).replace('(','').replace(')','');
    }
}