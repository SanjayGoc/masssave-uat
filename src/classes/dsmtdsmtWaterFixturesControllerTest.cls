@isTest
public class dsmtdsmtWaterFixturesControllerTest {
    
    static testmethod void test(){
        
        
        Energy_Assessment__c ea = Datagenerator.setupAssessmentWithWorkOrder();
        insert ea;
        
        ApexPages.currentPage().getParameters().put('assessId',ea.id);
        
        test.startTest();
        dsmtdsmtWaterFixturesController dsmtWater = new dsmtdsmtWaterFixturesController();
        dsmtWater.getWaterFixtureTypeOptions();
        dsmtWater.getDHWList();
        dsmtwater.ea=ea;
        dsmtWater.getWaterFixturesString();
        ApexPages.currentPage().getParameters().put('type','Bath');
        dsmtwater.addNewWaterFixture(); 
        dsmtwater.getWaterFixtureRecordTypes();
        dsmtwater.saveWaterFixture();
        dsmtwater.saveCloseWaterFixture();
        dsmtwater.editWaterFixture();
        dsmtwater.deleteWaterFixture();
        test.stopTest();
    }
    
}