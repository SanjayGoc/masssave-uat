public with sharing class dsmtReturnToContractorCon {
    public final Inspection_Request__c IR;
    public dsmtReturnToContractorCon(ApexPages.StandardController stdController) {
        this.IR = (Inspection_Request__c)stdController.getRecord();
    }
    
    public PageReference createReviewRec() {
    
        //check if the user belongs to Inspection Manager Review
       // try{
            Id recordTypeId = [ SELECT id from RecordType where DeveloperName = 'Post_Inspection_Repair' ].Id;
            Inspection_Request__c  IRr =[select Inspector_Name__r.name,Contractor_Name__c,Contractor_Name__r.Email__c from Inspection_Request__c where id= : IR.id];
            String QueueID = [Select Queue.Id, Queue.Name from QueueSObject WHERE Queue.Type ='Queue' AND Queue.Name =: IRr.Contractor_Name__r.name Limit 1].Queue.Id;
            Review__c review = new Review__c ();
            review.Type__c = 'Post-Inspection Repair'; 
            review.recordTypeId  = recordTypeId ;
            review.ownerID = QueueID;
            review.Inspection_Request__c = IR.id;
            review.Trade_Ally_Account__c = Ir.Contractor_Name__c;
            review.Trade_Ally_Email__c = Ir.Contractor_Name__r.Email__c;
            review.Status__c = 'Pending Contractor Acceptance';
            //create a lookup field on review to Inspection request and update here
            
            insert review;
            List<Attachment__c> lstAtt = [select Id,Review__c from Attachment__c where Inspection_Request__c =:IR.id];
            if(lstAtt.size()>0){
                for(Attachment__c att: lstAtt){
                    att.Review__c = review.Id;
                }
                
                update lstAtt;
             }
            // Redirect the user back to the original page
            PageReference pageRef = new PageReference('/' + IR.id);
            pageRef.setRedirect(true);
            return pageRef;
        /* }
        catch(Exception ex){
            //dev console was not workin so had this to check for errors will remove once complete
            Account a = new account(id='0014D000009XRdD');
            a.Description = ex.getStackTraceString()+' >> '+ex.getmessage();
            update a;
            return null;
            
        }*/
        
    }
    
}