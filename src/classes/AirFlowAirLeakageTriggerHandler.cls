public class AirFlowAirLeakageTriggerHandler extends TriggerHandler implements ITrigger{
    
    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            calculateDefaultValues(trigger.new, (Map<Id, Air_Flow_and_Air_Leakage__c>) trigger.oldmap);
        } else if (trigger.isUpdate) {
            //Here we will call before update actions
            calculateDefaultValues(trigger.new, (Map<Id, Air_Flow_and_Air_Leakage__c>) trigger.oldmap);
        } else if (trigger.isDelete) {
            //Here we will call before delete actions
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) {
            //Here we will call after insert actions
        } else if (trigger.isUpdate) {
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }
    
    private static void calculateDefaultValues(list<Air_Flow_and_Air_Leakage__c> newList, Map<Id, Air_Flow_and_Air_Leakage__c> oldMap)
    {
        Set<Id> eaIdSet = new Set<Id>();
        
        for(Air_Flow_and_Air_Leakage__c a : newList)
        {
            if(a.IsFromFieldTool__c) continue;

            eaIdSet.add(a.Energy_Assessment__c);
        }

        if(eaIdSet ==null || eaIdSet.size() ==0) return;
        
        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaIdSet);

        bp = dsmtBuildingModelHelper.UpdateThermalAreaModel(bp);

        //newBP.temp = dsmtTempratureHelper.ComputeTemprature(newBP);
        //newBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(newBP);
        
        for(Air_Flow_and_Air_Leakage__c a : newList)
        {
            bp.ai = a;
            
            a.Efficiency__c = 1;
            a.HasInfiltrationRegain__c = true;
            
            if(a.ActualLeakiness__c == null)
                a.Leakiness__c = dsmtAirFlowHelper.ComputeShellLeakinessCommon(bp);
            
            if(a.ActualCFM50__c == null || a.ActualCFM50__c == 0)
                a.CFM50__c = dsmtAirFlowHelper.ComputeShellCFM50Common(bp);
            
            if(a.ActualACHC__c == null)
                a.ACHC__c = dsmtAirFlowHelper.ComputeAirInfiltrationACHCCommon(bp);
            
            if(a.ActualACHH__c == null)
                a.ACHH__c = dsmtAirFlowHelper.ComputeAirInfiltrationACHHCommon(bp);
            
            a.ACH__c = dsmtAirFlowHelper.ComputeAirInfiltrationACHCommon(bp);
            
            if(a.Location__c == null)
                a.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);
            
            bp.Location = a.Location__c;
            
            a.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);            
           
            bp.LocationSpace = a.LocationSpace__c;
            bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
            bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
            bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);
            
            a.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
            bp.LocationType = a.LocationType__c;
            
            a.RegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');
            a.RegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');

            if(bp.bs.Year_Built__c !=null && bp.bs.Year_Built__c !='')
                a.Year__c = integer.valueOf(bp.bs.Year_Built__c);

            bp.eaObj = dsmtBuildingModelHelper.UpdateEnergyAssessmentObject(bp);
             
            a.TightnessLimit__c = bp.eaObj.BuildingTightnessLimit__c;
            a.MechanicalVentilationSystemsRequiredCFM__c = dsmtBuildingModelHelper.ComputeMechanicalVentilationSystemsRequiredCFMCommon(bp);

            a.ExpectedUsefulLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedUsefulLifeCommon('WaterFixture');
            a.ExpectedRemainingLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedRemainingLifeCommon(a.ExpectedUsefulLife__c, a.Year__c);
            
            
        }
    }

    public static string getSObjectFields(string objName)
    {
        return dsmtHelperClass.sObjectFields(objName);
    }
}