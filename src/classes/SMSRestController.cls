public with sharing class SMSRestController {
    public string paramMap {get;set;}
    public SMSRestController() {
        System.debug('----- Page called -----------');
        System.debug('--- Paramete Map --------' + ApexPages.currentPage().getParameters());
        paramMap = ApexPages.currentPage().getParameters() == null ? 'N/A' : JSON.serialize(ApexPages.currentPage().getParameters());
        
    }
    
    public void saveSMS() {
    
        SMS__c s = new SMS__c();
        s.Mobile_No__c = ApexPages.currentPage().getparameters().get('From');
        s.Message_Text__c = ApexPages.currentPage().getparameters().get('Body');
        s.Response__c = paramMap;
        
        String msgId = ApexPages.currentPage().getparameters().get('SmsSid');
        List<SMS__c> allSMS = new List<SMS__c>();
        if(msgId != null && msgId.length() > 0){
            allSMS = [SELECT ID,Customer__c,Appointment__c from SMS__c Where Message_Id__c=:msgId];
        }
        system.debug('--allSMS--'+allSMS);
        for(SMS__c oSMS : allSMS) {
            s.Original_SMS__c = oSMS.Id;
            s.Customer__c = oSMS.Customer__c;
            s.Appointment__c = oSMS.Appointment__c;
        }
        
        if(allSMS.isEmpty()) {
            String phonNum =  s.Mobile_No__c.indexOf('+1') >= 0 ? s.Mobile_No__c.replace('+1', '%') : '%'+s.Mobile_No__c; 
            phonNum = phonNum.replaceAll('[^\\d]', '');
            List<String> strList = new List<String>();
            while (phonNum.length() > 0){
                strList.add(phonNum.subString(0,1));
                phonNum = phonNum.subString(1);
            }
            phonNum = String.join(strList, '%');
            phonNum = '%' + phonNum + '%';
            system.debug('--phonNum--'+phonNum);
            List<SMS__c> allCustomerSMS = [SELECT ID,Customer__c from SMS__c Where Mobile_No__c like :phonNum AND Customer__c != null];
            
            List<Customer__c> allCustomer = [SELECT ID from Customer__c Where Phone__c like :phonNum and Send_SMS_Notification__c = true];
            if(allCustomer != null && allCustomer.size() > 0) {
                s.Customer__c = allCustomer.get(0).Id;
            } else if(allCustomerSMS != null && allCustomerSMS.size() > 0) {
                s.Customer__c = allCustomerSMS.get(0).Customer__c;
                s.Failed_Reason__c = allCustomerSMS.get(0).Customer__c;
            }else {
                s.Failed_Reason__c = 'Original SMS as well Customer not found';
            }
        }
        
        DateTime currentTime = DAtetime.now();
        
        String woid = null;
        if(s.Appointment__c != null){
           List<Appointment__c> applist = [select id, Workorder__c from appointment__c where id =: s.Appointment__c];
           if(applist.size() > 0){
              woid = applist.get(0).Workorder__c;
           }
        }else{
           List<Appointment__c> applist = [select id,Customer_Reference__c,Workorder__c from Appointment__c where Customer_Reference__c =: s.Customer__c and Appointment_Start_Time__c >=: currentTime and appointment_status__c = 'Scheduled' order by Appointment_Start_Time__c asc limit 1];   
           if(applist.size() > 0){
              woid = applist.get(0).Workorder__c;
              s.Appointment__c = applist.get(0).id;
           }
        }
        
        if(woid != null){
            WorkOrder__c wo = new WorkOrder__c(id = woid);
            if(s.Message_Text__c.equalsIgnoreCase('C')){
               wo.Status__c = 'Cancel Requested';
            }else if(s.Message_Text__c.equalsIgnoreCase('R')){
               wo.Status__c = 'Reschedule Appointment Requested'; 
            }
            
            update wo;
        }  
        
        
        s.status__c = 'Received';
        insert s;
    }
}