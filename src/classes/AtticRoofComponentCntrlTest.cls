@istest
public class AtticRoofComponentCntrlTest {
    static testMethod void test1() {
        
        Ceiling__c ar=new Ceiling__c();
        //ar.Ceiling__c='test';
        insert ar;
        
        Layer__c l = new Layer__c();
        l.Name='test';
        l.HideLayer__c = false;
        l.Ceiling__c=ar.id;
       // l.Ceiling__c=ar.id;
        insert l;
        
        AtticRoofComponentCntrl cntrl = new AtticRoofComponentCntrl();
        String newLayer = cntrl.newLayerType;
        cntrl.atticRoofId=ar.Id;
        cntrl.getLayerTypeOptions();
        cntrl.getRoofLayersMap();
        cntrl.saveRoof();
        
        ApexPages.currentPage().getParameters().put('layerType','Flooring');
        cntrl.addNewRoofLayer();
        
        ApexPages.currentPage().getParameters().put('index','0');
        cntrl.deleteRoofLayer();
       // cntrl.saveRoofLayers();  
    }
      
}