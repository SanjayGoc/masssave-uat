public class dsmtdsmtWaterFixturesController extends dsmtEnergyAssessmentBaseController {
    public Water_Fixture__c newWaterFixture{get;set;}
    public integer noOfBedrooms {get;set;}
    public Map<String,List<Water_Fixture__c>> waterFixturesByRecordType {get;set;}
    
    public dsmtdsmtWaterFixturesController()
    {
        newWaterFixture = new Water_Fixture__c();
        noOfBedrooms = 0;
        
        list<Energy_Assessment__c> eaList = [select id, Building_Specification__r.Bedromm__c
                                             from Energy_Assessment__c
                                             where id =: Apexpages.currentPage().getParameters().get('assessId')];
        if(eaList.size() > 0 && eaList[0].Building_Specification__r.Bedromm__c != null)
            noOfBedrooms = integer.valueOf(eaList[0].Building_Specification__r.Bedromm__c);
    }
    
    public list<SelectOption> getWaterFixtureTypeOptions()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Water_Fixture__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    
    public List<SelectOption> getWaterFixtureRecordTypes()
    {
        List<selectOption> options = new List<SelectOption>();
        for(RecordType rtype : [select Id,Name from RecordType where sObjectType='Water_Fixture__c']){
            options.add(new SelectOption(rtype.Id,rType.Name));
        }
        return options;
    }
    
    public list<SelectOption> getDHWList()
    {
        String eassId = Apexpages.currentPage().getParameters().get('assessId');
        list<SelectOption> soList = new list<SelectOption>();
        soList.add(new SelectOption('', '--Select DHW--'));
        
        set<String> rtSet = new set<String>{'Heat Pump', 'Storage Tank', 'Indirect Storage Tank', 'On Demand', 'Combo', 'Boiler + Indirect Storage Tank', 'Boiler + Tankless Coil'};
        
        for(Mechanical_Sub_Type__c a : [select id, Name 
                                        from Mechanical_Sub_Type__c 
                                        where Mechanical_Type__r.Mechanical__r.Energy_Assessment__c =: eassId 
                                        and RecordType.Name in : rtSet
                                        order by Name])
        {
            soList.add(new SelectOption(a.Id, a.Name));
        }
        
        return soList;
    }
    
    public Map<String,List<Water_Fixture__c>> getWaterFixturesString(){
        waterFixturesByRecordType = fetchWaterFixturesByRecordType();
        return waterFixturesByRecordType;
    }
    
    public Map<String,List<Water_Fixture__c>> fetchWaterFixturesByRecordType()
    {
        waterFixturesByRecordType = new Map<String,List<Water_Fixture__c>>();
        if(ea != null && ea.Id != null){
            String eaId = ea.Id;
            for(Water_Fixture__c wf : Database.query('SELECT ' + getSObjectFields('Water_Fixture__c') + ', RecordType.Name, RecordType.DeveloperName FROM Water_Fixture__c WHERE Energy_Assessment__c=:eaId')){
                if(wf.RecordType.Name != null){
                   List<Water_Fixture__c> tempList = waterFixturesByRecordType.get(wf.RecordType.Name);
                    if(tempList == null){
                        tempList = new List<Water_Fixture__c>();
                    }
                    tempList.add(wf);
                    waterFixturesByRecordType.put(wf.RecordType.Name, tempList);
                }
            }   
        }
        return waterFixturesByRecordType;
    }
    
    private Integer nextAutoNumber(String type)
    {
        Integer next = 1;
        Map<String,List<Water_Fixture__c>> m = waterFixturesByRecordType;
        if(m.containsKey(type)){
            next = next+m.get(type).size();
        }
        return next;
    }
    
    public void addNewWaterFixture()
    {
        String type = getParam('type');
        String location = getParam('location');
        String recordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get(type).getRecordTypeId();
        
        try{
            Integer nextNumber = nextAutoNumber(type);
            this.newWaterFixture = new Water_Fixture__c(
                recordTypeId = recordTypeId,
                Type__c = type,
                Location__c = location,
                Energy_Assessment__c = ea.Id,
                Name = type + ' ' + nextNumber
            );
            INSERT this.newWaterFixture;
            if(this.newWaterFixture.Id != null){
                String waterFixtureId= this.newWaterFixture.Id;
                List<Water_Fixture__c> wfs = Database.query('SELECT ' + getSObjectFields('Water_Fixture__c') + ',RecordType.Name,RecordType.DeveloperName FROM Water_Fixture__c WHERE Id=:waterFixtureId');
                if(wfs.size() > 0){
                    this.newWaterFixture = wfs.get(0);
                }
            }
        }catch(Exception ex){
            System.debug('EXCEPTION :: ' + ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            this.newWaterFixture = null;
        }
    }
    
    public void saveWaterFixture()
    {
        if(newWaterFixture != null && newWaterFixture.Id != null){
            UPDATE newWaterFixture;
            
            String waterFixtureId = newWaterFixture.Id;
            List<Water_Fixture__c> wfs = Database.query('SELECT ' + getSObjectFields('Water_Fixture__c') + ',RecordType.Name,RecordType.DeveloperName FROM Water_Fixture__c WHERE Id=:waterFixtureId');
            if(wfs.size() > 0){
                newWaterFixture = wfs.get(0);
            }
        }
    }
    
    public void saveAllWaterFixture()
    {
        if(this.waterFixturesByRecordType != null){
            List<Water_Fixture__c> aplList = new List<Water_Fixture__c>();
            
            for(String key : waterFixturesByRecordType.keyset())
            {
                aplList.addAll(waterFixturesByRecordType.get(key));
            }
            
            update aplList;
        }
    }
    
    public void saveCloseWaterFixture()
    {
        if(newWaterFixture != null && newWaterFixture.Id != null){
            UPDATE newWaterFixture;
            newWaterFixture = null;
        }
    }
    
    public void editWaterFixture()
    {
        String waterFixtureId = getParam('waterFixtureId');
        if(waterFixtureId != null && waterFixtureId.trim() != ''){
            List<Water_Fixture__c> wfs = Database.query('SELECT ' + getSObjectFields('Water_Fixture__c') + ',RecordType.Name,RecordType.DeveloperName FROM Water_Fixture__c WHERE Id=:waterFixtureId');
            if(wfs.size() > 0){
                newWaterFixture = wfs.get(0);
            }
        }else{
            saveWaterFixture();
            newWaterFixture = null;
        }
    }
    
    public void deleteWaterFixture()
    {
        String waterFixtureId = getParam('waterFixtureId');
        if(waterFixtureId != null && waterFixtureId.trim() != ''){
            DELETE new Water_Fixture__c(Id = waterFixtureId);
        }
    }
}