@istest
public class RecommendationScenarioTriggerHandlerTest
{
	@istest
    static void runtest()
    {
        Enrollment_Application__c ea = new Enrollment_Application__c();
        //ea.Name='test';
        insert ea;
        
        Recommendation_Scenario__c rc =new Recommendation_Scenario__c();
        rc.Status__c='Installed';
        ea.id=rc.id;
        insert rc;
        
        Workorder__c wo =new Workorder__c();
        insert wo;
        
        Appointment__c app= new Appointment__c();
        insert app;
        
        RecommendationScenarioTriggerHandler rsth =new RecommendationScenarioTriggerHandler();
        rsth.afterInsert();
                          
    }
}