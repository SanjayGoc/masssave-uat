public class dsmtCSRUtils {
    
    //returns queues and 
    public static List<Id> getComplexOwnerIdsForUser(Id userId)
    {
        
        List<Id> full_ids_list = new List<Id>();
        full_ids_list.add(userId); 
        
        Set<Id> grpids = getGroupsForIds(userId);
        
        system.debug('--grpids--'+grpids);
        
        /*List<GroupMember> group_members_list=[SELECT Group.Name,Group.Id FROM GroupMember WHERE (UserOrGroupId = :userId)
                                              AND Group.Type = 'Queue' LIMIT 5000];
        Set<String> groupids = new Set<String>();
        for (GroupMember gm: group_members_list)        
            groupids.add(gm.Group.Id);  
        
        system.debug('--groupids--'+groupids); */
        
        for(String gid : grpids){
           full_ids_list.add(gid);
        }
        
        return full_ids_list;
        
        
    }
    
    // return all group ids the user belongs to via direct or indirect membership
    public static Set<Id> getGroupsForIds(String userId){
        
        Set<Id> usergrpids =  new Set<Id>();
        usergrpids.add(userId);
        
        system.debug('--usergrpids--'+usergrpids);
        
        List<Group> rolegrouplist = [select id,Name,Related.Name  from Group where relatedId in (select UserRoleId from User where id =: userId)];
        for(Group rolegroup : rolegrouplist){
           usergrpids.add(rolegroup.Id);
        }
        system.debug('--usergrpids--'+usergrpids);
        
        
        Set<Id> output = new Set<Id>();
    
        list<GroupMember> records = [select id, GroupId, UserOrGroupId  from GroupMember  where UserOrGroupId in: usergrpids and UserOrGroupId != null and Group.Type = 'Queue'];
         
        for (GroupMember record:records)
        {
            system.debug('--record--'+record);
            output.add(record.GroupId);
        }
       
        return output;
    }
    
    public static List<Group> getQueuesList(Id userid)
    {
        Set<Id> grpids = getGroupsForIds(userId);
        
        List<Group> queues = new List<Group>();
        if(grpids.size() > 0){
            queues = [select id,Name from Group where id in: grpids];
        }
        
        
        /*List<GroupMember> groupMemberList = [select Group.Id, 
                              Group.Name, 
                              Group.Type
                              from GroupMember                               
                              where UserOrGroupId = :userId 
                              AND Group.Type = 'Queue'
                              limit 500];
        
        
        for(GroupMember gm:groupMemberList)
            queues.add(new Group(Id=gm.Group.Id,Name=gm.Group.Name));*/
        
        return queues;
    }
    
    public static Set<id> GetUserIdsFromGroup(Set<Id> groupIds)
    {
        // store the results in a set so we don't get duplicates
        Set<Id> result=new Set<Id>();
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String groupType = Schema.SObjectType.Group.getKeyPrefix();
        Set<Id> groupIdProxys = new Set<Id>();
        // Loop through all group members in a group
        for(GroupMember m : [Select Id, UserOrGroupId From GroupMember Where GroupId in :groupIds])
        {
            // If the user or group id is a user
            if(((String)m.UserOrGroupId).startsWith(userType))
            {
                result.add(m.UserOrGroupId);
            }
            // If the user or group id is a group
            // Note: there may be a problem with governor limits if this is called too many times
            else if (((String)m.UserOrGroupId).startsWith(groupType))
            {
                // Call this function again but pass in the group found within this group
                groupIdProxys.add(m.UserOrGroupId);
            }
        }
        if(groupIdProxys.size() > 0)
        {
            result.addAll(GetUSerIdsFromGroup(groupIdProxys));
        }
        return result;  
    }
    public static Set<id> GetUserIdsFromGroupName(String groupName)
    {
        system.debug('--groupName--'+groupName);
        
        List<Group> gplist = [select id from Group where name =: groupName];
        if(gplist.size() > 0){
           String gpid = gplist.get(0).id; 
           Set<Id> gpids = new Set<Id>{gpid};
           return GetUserIdsFromGroup(gpids);
       }   
       return new Set<id>(); 
    }
}