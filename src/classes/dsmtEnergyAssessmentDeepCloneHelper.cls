public class dsmtEnergyAssessmentDeepCloneHelper
{
    
    public static String DeepCloneEnergyAssessment(String assessId, String appointmentId)
    {
        List<Energy_Assessment__c> eas = Database.query('SELECT ' + getSObjectFields('Energy_Assessment__c') + ' FROM Energy_Assessment__c WHERE Id=:assessId');
        List<Appointment__c> appointments = [select Id, Workorder__c, Customer_Id__c
                                             from Appointment__c where id =: appointmentId];
                                             
        if(eas.size() > 0 && appointments.size() > 0)
        {
            Energy_Assessment__c ea = eas[0].clone(false, false);
            ea.Status__c = 'New';
            ea.Workorder__c = appointments[0].Workorder__c;
            ea.Appointment__c = appointmentId;
            ea.Building_Specification__c = cloneBuilding(eas[0].Building_Specification__c, appointmentId);
            
            //insert ea;
            
            //Method that will clone the related Air Flow
            cloneAirFlow(assessId, ea.Id, ea.Building_Specification__c);
            
            
            //Method that will clone the related Recommendations
            cloneEARecommendations(assessId, ea.Id, ea.Building_Specification__c);
            
            
            return ea.Id;
        }
        
        return null;
    }
    
    public static String cloneBuilding(String bSpecId, String appointmentId)
    {
        List<Building_Specification__c> bSpec = Database.query('SELECT ' + getSObjectFields('Building_Specification__c') + ' FROM Building_Specification__c WHERE Id=:bSpecId');
        
        if(bSpec.size() > 0 && bSpec.size() > 0)
        {
            Building_Specification__c bSpecRec = bSpec[0].clone(false, false);
            bSpecRec.Appointment__c = appointmentId;
            
            //insert bSpecRec;
            
            return bSpecRec.Id;
        }
        
        return null;
    }
    
    public static void cloneEARecommendations(String assessId, String newAssessId, String bSpecId)
    {
        list<Recommendation__c> afList = Database.query('SELECT ' + getSObjectFields('Recommendation__c') + ' FROM Recommendation__c WHERE Energy_Assessment__c =: assessId');
        
        if(afList.size() > 0)
        {
            list<Recommendation__c> insertList = new list<Recommendation__c>();
            
            for(Recommendation__c a : afList)
            {
                Recommendation__c rec = a.clone(false, false);
                rec.Energy_Assessment__c = newAssessId;
                rec.Building_Specification__c = bSpecId;
                
                insertList.add(rec);
            }
            
            //if(insertList.size() > 0)
              //  insert insertList;
        }
    }
    
    public static void cloneMechanical()
    {
        
    }
    
    public static void cloneAirFlow(String assessId, String newAssessId, String bSpecId)
    {
        list<Air_Flow_and_Air_Leakage__c> afList = Database.query('SELECT ' + getSObjectFields('Air_Flow_and_Air_Leakage__c') + ' FROM Air_Flow_and_Air_Leakage__c WHERE Energy_Assessment__c =: assessId');
        
        if(afList.size() > 0)
        {
            list<Air_Flow_and_Air_Leakage__c> insertList = new list<Air_Flow_and_Air_Leakage__c>();
            
            for(Air_Flow_and_Air_Leakage__c a : afList)
            {
                Air_Flow_and_Air_Leakage__c rec = a.clone(false, false);
                rec.Energy_Assessment__c = newAssessId;
                rec.Building_Specifications__c = bSpecId;
                
                insertList.add(rec);
            }
            
            //if(insertList.size() > 0)
              //  insert insertList;
        }
    }
    
    public static void cloneThermalEnvelope()
    {
        
    }
    
    public static void cloneLightning()
    {
    
    }
    
    public static void cloneAppliance()
    {
    
    }
    
    public static void cloneWaterFixtures()
    {
    
    }
    
    public static void cloneMiscellaneous()
    {
    
    }
    
    public static void cloneSafetyAspects()
    {
    
    }
    
    public static void cloneProposals()
    {
    
    }
    
    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}