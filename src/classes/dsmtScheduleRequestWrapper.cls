public class dsmtScheduleRequestWrapper{
    
    public LoginWrapper loginDetail;
    public WorkOrderDetailWrapper workOrderDetail;
    public Address addObj;
    
    
    public class Address{
        public string status;
        public string Address;
        public string City;
        public string county;
        public string latitude;
        public string longitude;
        public string state_long;
        public string country_long;
        public string state_short;
        public string country_short;
        public string postalCode;
    }
    
    public class LoginWrapper{
        public string sessionID;
    }
    
    public class WorkOrderDetailWrapper{
        public string workOrderId;
        public string city;
        public string country;
        public string postalCode;
        public string serviceAddress;
        public string serviceDuration;
        public string territoryId;
        public string state;
        public list<string> employeeId;
        public list<string> workTeamId;
        public list<string> planDate;
        public list<string> serviceTimeSlots;
    }
}