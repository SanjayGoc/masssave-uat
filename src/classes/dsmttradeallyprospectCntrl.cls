public with sharing class dsmttradeallyprospectCntrl {
    
    public Trade_Ally_Account__c objTAC{get;set;}
    public String sectionTitle{get;set;}
    public Checklist__c pgCheckList{get{return [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Trade_Ally_Prospect_Form' LIMIT 1];}}
    
    public dsmttradeallyprospectCntrl(){
        objTAC = new Trade_Ally_Account__c();
        //objTAc.Title__c = 'Crew Chief';
        
    }
    
    public PageReference saveTAA(){
        if(objTAC!=null && objTAC.Name!=null && objTAC.Name != ''){
        if(objTAc.Title__c == null || objTAc.Title__c.trim()=='')
        {
            objTAc.Title__c = 'Crew Chief';
        }
            objTAC.EULA_Agreement_Date__c = DAtetime.now();
            insert objTAC;
            
            PageReference pg = new PageReference('/apex/dsmtTradeallyProspectThankyou');  
            pg.setRedirect(true);
            return pg; 
        }
            
        return null;
    }
    
    /* Other Methods */
    public list<Checklist_Items__c> getChecklists(){
        list<Checklist_Items__c> checklistItems = [
            SELECT id, Action_Required__c, Checklist_Information__c, Checklist__c,Parent_Checklist__c, Section__c, Sequence__c
            FROM Checklist_Items__c
            WHERE Parent_Checklist__r.Unique_Name__c = 'Trade_Ally_Prospect_Form'
        ];
        
        if(checklistItems.size() > 0){
            this.sectionTitle = checklistItems.get(0).Section__c;
            return checklistItems;
        }
        
        return new list<Checklist_Items__c>();
    }
}