public class dsmtThermalCrawlspaceFloorController
{
    public Id parentId {get;set;}
    public String crawFloorId {get;set;}
    public String RecommendationId {get;set;}
    public Crawlspace_Floor__c newCrawlspaceFloor{get;set;}
    public Recommendation__c recommendation {get;set;}
    
    public dsmtThermalCrawlspaceFloorController()
    {
        newCrawlspaceFloor = new Crawlspace_Floor__c();
        
        recommendation = new Recommendation__c();
        recommendation.Status_Code__c = 'Recommended';
        recommendation.RecordTypeId = Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().get('Crawlspace Floor').getRecordTypeId();
        
        layerList = new list<Layer__c>();
    }
    
    public List<Layer__c> layerList{get;set;}
    public Map<Id,Layer__c> getlayerListMap(){
        crawFloorId = newCrawlspaceFloor.Id;
        layerList = new list<Layer__c>();
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ' RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Crawlspace_Floor__c =: crawFloorId';
        
        layerList = Database.query(query);
        return new Map<Id,Layer__c>(layerList);
    }
    public void savelayers(){
        if(layerList != null && layerList.size() > 0){
            UPSERT layerList;
        }
    }
    public void addNewLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE RecordTypeId =:recordTypeId AND Crawlspace_Floor__c =:crawFloorId];
        if(layerType != null && m.containsKey(layerType)){
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + (layers.size() + 1),
                Type__c = layerType,
                Crawlspace_Floor__c = crawFloorId,
                RecordTypeId = recordTypeId
            );
            INSERT newLayer;
            layerList.add(newLayer);
        }
    }
    public void deleteLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = layerList.get(index).Id);
        layerList.remove(index);
    }
    
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            fields += dfield.getName() + ', ';
        }
        return fields;
    }
    
    public void saveNewReccom() {
        
        recommendation.Crawlspace_Floor__c = newCrawlspaceFloor.Id;
        upsert recommendation;   
        
        recommendation = new Recommendation__c();
    }
    
    public list<Recommendation__c> getcrawWallRecom(){
        return [select id, Status_Code__c, Operation__c, Part__c, Description__c, CreatedDate
                     from Recommendation__c
                     where Crawlspace_Floor__c =: newCrawlspaceFloor.Id
                     and Crawlspace_Floor__c != null];
    }
    
    public Pagereference DeleteRecommendation(){
        List<Recommendation__c> recList = [select id from Recommendation__c where id =: RecommendationId];
        
        if(recList != null && recList.size() > 0){
            delete recList;
        }
        return null;
    }
    
    public void editReccom()
    {
        List<Recommendation__c> recList = [select id, Status_Code__c, Quantity__c, Operation__c, Part__c, Pricing_Effective_Date__c, Treat_From_Interior__c, Description__c,
                                           Type__c, Add_Depth__c, Remove_Exist__c, Added_Nominal_R_Value__c, Assembly_Nominal_R_Value__c, Include_in_total_R_Value__c, Total_R_Value__c
                                           from Recommendation__c where id =: RecommendationId];
        
        if(recList != null && recList.size() > 0){
            recommendation = recList[0];
        }
    }
}