@istest
public class dsmtUpdateRecommendationControllerTest {
     @istest
    static void test()
    {
       
        Energy_Assessment__c ea=Datagenerator.setupAssessment();
        
        
        dsmtEAModel.Surface newSurfaceObj = new dsmtEAModel.Surface();
        newSurfaceObj.EAObj = new Energy_Assessment__c();    
        newSurfaceObj.rcmd = new Recommendation__c();

        Wall__c wall =new Wall__c();
        
        Ceiling__c ceilling = new Ceiling__c();
        
        Recommendation_Scenario__c proj = new Recommendation_Scenario__c();
        insert proj;
        
        DSMTracker_Product__c  dsm = new DSMTracker_Product__c();
        insert dsm;
        
        Thermal_Envelope__c tenv = new Thermal_Envelope__c(Energy_Assessment__c = ea.Id);
        insert tenv;
        
        BuildingSpecificationTriggerHandler.stopProcessForBuildingModal = true;
        Thermal_Envelope_Type__c tet = new Thermal_Envelope_Type__c(Energy_Assessment__c = ea.Id,
                                                                    Thermal_Envelope__c = tenv.Id,
                                                                    RecordTypeId = Schema.SObjectType.Thermal_Envelope_Type__c.getRecordTypeInfosByName().get('Exterior Wall').getRecordTypeId());
        
        try{
            insert tet;
        }catch(Exception ex){}
        test.startTest();
        
        Recommendation__c rc = new Recommendation__c();
        rc.Recommendation_Scenario__c = proj.id;
        rc.DSMTracker_Product__c = dsm.id;
        rc.SubCategory__c ='Lighting';
        rc.DeemedSavings__c=true;
        rc.Pricing_Effective_Date__c = date.today();
        rc.Operation__c='Add Continuous Insulation';
        rc.InsulationType__c = 'InsulationType';
        rc.InsulationDepth__c = 100.00;
        rc.MaxInsulationDepth__c = 100;
        rc.IsEnclosed__c = true;
        rc.ItemCode__c = 'CONTR_WO';
        rc.Category__c = 'Insulation';
        rc.Thermal_Envelope_Type__c = tet.Id;
        insert rc;
        
        ApexPages.currentPage().getParameters().put('id', rc.Id);
        ApexPages.StandardController controller = new ApexPages.StandardController(rc);
        dsmtUpdateRecommendationController cntrl = new dsmtUpdateRecommendationController(controller);
        cntrl.updateRecommendation();
    }
}