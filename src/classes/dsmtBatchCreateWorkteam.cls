global class dsmtBatchCreateWorkteam implements Database.Batchable<sObject>, Schedulable
{
    @testVisible private static Date startDate;
    @testVisible private static Integer daysOfCoverageNeeded;

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id, Name, Schedule__c, Location__c, Territory__c, Active__c, Status__c FROM Employee__c WHERE Schedule__c != null AND Status__c = \'Active\'';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Employee__c> employees){
        startDate = startDate == null ? Date.today() : startDate;
        daysOfCoverageNeeded = daysofCoverageNeeded == null ? Integer.valueOf(Label.EmpDayLabel) : daysOfCoverageNeeded;
        System.debug('startDate: '+startDate);
        WorkTeamMaker workTeamCreator = new WorkTeamMaker(startDate, daysOfCoverageNeeded);
        workTeamCreator.createWorkTeamsForEmployees(employees);
    }  

    global void finish(Database.BatchableContext BC)
    {
    }
    
    global void execute(SchedulableContext sc){
        dsmtBatchCreateWorkteam cntrl = new dsmtBatchCreateWorkteam();
        database.executebatch(cntrl,1);
    }
}