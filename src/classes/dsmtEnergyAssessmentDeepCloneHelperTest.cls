@isTest
public class dsmtEnergyAssessmentDeepCloneHelperTest 
{
    @isTest
    public static void runTest()
    {
        
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        

        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        test.startTest();
        Building_Specification__c bs =Datagenerator.getBuildingSpecWithAppt(); 

        dsmtEnergyAssessmentDeepCloneHelper cnt = new dsmtEnergyAssessmentDeepCloneHelper();
        
        dsmtEnergyAssessmentDeepCloneHelper.DeepCloneEnergyAssessment(ea.Id, bs.Appointment__c);
        dsmtEnergyAssessmentDeepCloneHelper.cloneThermalEnvelope();
        dsmtEnergyAssessmentDeepCloneHelper.cloneLightning();
        dsmtEnergyAssessmentDeepCloneHelper.cloneAppliance();
        dsmtEnergyAssessmentDeepCloneHelper.cloneWaterFixtures();
        dsmtEnergyAssessmentDeepCloneHelper.cloneMiscellaneous();
        dsmtEnergyAssessmentDeepCloneHelper.cloneSafetyAspects();
        dsmtEnergyAssessmentDeepCloneHelper.cloneProposals();
        test.stoptest();      
    }
    /*@isTest
    public static void runTest1()
    {
        Energy_Assessment__c ea =Datagenerator.setupAssessment();
        
        Location__c loc = new Location__c();
        insert loc;
        
        employee__c em = new employee__c();
        em.location__c = loc.id;
        em.Employee_Id__c = 'test1234';
        em.status__c = 'Active';
        insert em;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, em.Id);
        insert wt;
        test.startTest();

        
        
        Workorder__c wo = new Workorder__c();
        wo.Scheduled_Start_Date__c = Datetime.now();
        wo.Scheduled_End_Date__c = Datetime.now().adddays(1);
        
        insert wo;
        
        Appointment__c app = new Appointment__c();
        app.Energy_Assessment__c = ea.id;
        app.Workorder__c = wo.id;
        app.Appointment_Start_Time__c = DateTime.now();
        app.Appointment_End_Time__c = DateTime.now().addDays(3);
        app.Appointment_Type__c = 'Home Energy Assessment';
        insert app;
        
        
        DSMTracker_Product__c dsm = new DSMTracker_Product__c();
        dsm.Sub_Category__c='Air Sealing' ;
        
        insert dsm;
        
        Recommendation__c rc = new Recommendation__c();
        rc.Energy_Assessment__c = ea.Id;
        //rc.Building_Specification__c=bs.Id;
        rc.Dsmtracker_Product__c =dsm.id;
        rc.Cost__c = 2;
        
        insert rc;        
        //test.startTest();
        dsmtEnergyAssessmentDeepCloneHelper cnt = new dsmtEnergyAssessmentDeepCloneHelper();
        //dsmtEnergyAssessmentDeepCloneHelper.DeepCloneEnergyAssessment(ec.Id, app.Id);
        //dsmtEnergyAssessmentDeepCloneHelper.cloneBuilding(bs.Id, app.Id);
        //dsmtEnergyAssessmentDeepCloneHelper.cloneEARecommendations(ec.Id, ec.Id, bs.Id);
        dsmtEnergyAssessmentDeepCloneHelper.cloneMechanical();
        //dsmtEnergyAssessmentDeepCloneHelper.cloneAirFlow(ec.Id, ec.Id, bs.Id);
        dsmtEnergyAssessmentDeepCloneHelper.cloneThermalEnvelope();
        dsmtEnergyAssessmentDeepCloneHelper.cloneLightning();
        dsmtEnergyAssessmentDeepCloneHelper.cloneAppliance();
        dsmtEnergyAssessmentDeepCloneHelper.cloneWaterFixtures();
        dsmtEnergyAssessmentDeepCloneHelper.cloneMiscellaneous();
        dsmtEnergyAssessmentDeepCloneHelper.cloneSafetyAspects();
        dsmtEnergyAssessmentDeepCloneHelper.cloneProposals();
        dsmtEnergyAssessmentDeepCloneHelper.getSObjectFields('Air_Flow_and_Air_Leakage__c');
        test.stopTest();
    }*/
    
    
}