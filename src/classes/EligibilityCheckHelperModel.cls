global class EligibilityCheckHelperModel {
    /* Instance Variables */
    public Eligibility_Check__c 					ecObj								{get;set;}
    public Call_List_Line_Item__c 					clliObj								{get;set;} 
    public List<Trade_Ally_Account__c> 				contractors							{get;set;}
    public Customer__c 								customerObj							{get;set;}
    public String	 								modelBreadcrumbTitle				{get;set;}
        
    /* Constructor */
    public EligibilityCheckHelperModel(Id cLLI_Id){
        this.clliObj = [
            SELECT 
            	Customer__c,What_is_the_home_heating_source__c,How_many_units__c,Do_you_own_or_rent__c,
                Do_you_live_there__c,How_did_you_hear_about_this_program__c,Promo_Code__c,Trade_Ally_Account__c,
                Home_Size__c,Home_Style__c,My_home_was_built_in__c,My_attic_have_ducts__c,My_home_has_thermostats__c,Workorder_Type__c,
                Customer__r.Gas_Account__r.First_Name__c,Customer__r.Gas_Account__r.Last_Name__c,Customer__r.Electric_Account__r.First_Name__c,
                Customer__r.Electric_Account__r.Last_Name__c,Customer__r.Service_Address_Full__c,customer__r.Electric_Account_Number__c,
                customer__r.gas_Account_Number__c
            FROM 
            	Call_List_Line_Item__c
            WHERE
            	Id =:cLLI_Id
            LIMIT 1
        ];
        System.debug('this.clliObj ' + this.clliObj);
        if(this.clliObj != null){
            this.initEligibilityCheck();
            this.loadCustomer(cLLI_Id);
        }
        System.debug('this.customerObj ' + this.customerObj);
    }
    
    /* Instance Methods */
    public void initEligibilityCheck(){
        this.ecObj = new Eligibility_Check__c();
        String heatWithNaturalGas = '';
        if(this.clliObj.What_is_the_home_heating_source__c == 'Gas'){
            heatWithNaturalGas = 'Yes';
        }else if(this.clliObj.What_is_the_home_heating_source__c != '' && this.clliObj.What_is_the_home_heating_source__c != null){
            heatWithNaturalGas = 'No';
        }
        this.ecObj = new Eligibility_Check__c(
        	Call_List_Line_Item__c 						= this.clliObj.Id,
            I_heat_my_home_with__c 						= this.clliObj.What_is_the_home_heating_source__c,
            Do_you_live_there__c 						= this.clliObj.Do_you_live_there__c,
            Do_you_own_or_rent__c 						= this.clliObj.Do_you_own_or_rent__c,
            How_many_units__c 							= this.clliObj.How_many_units__c,
            Trade_Ally_Account__c 						= this.clliObj.Trade_Ally_Account__c,
            How_did_you_hear_about_this_program__c 		= this.clliObj.How_did_you_hear_about_this_program__c,
            Promo_Code__c 								= this.clliObj.Promo_Code__c,
            Home_Size__c 								= this.clliObj.Home_Size__c,
            Home_Style__c 								= this.clliObj.Home_Style__c,
            Year_built_in__c 							= this.clliObj.My_home_was_built_in__c,
            Attic__c 									= this.clliObj.My_attic_have_ducts__c,
            Thermostats__c 								= this.clliObj.My_home_has_thermostats__c,
            Workorder_Type__c 							= this.clliObj.Workorder_Type__c,
            Customer__c 								= this.clliObj.Customer__c,
            Gas_Account_Holder_First_Name__c 			= this.clliObj.Customer__r.Gas_Account__r.First_Name__c,
            Gas_Account_Holder_Last_Name__c 			= this.clliObj.Customer__r.Gas_Account__r.Last_Name__c,
            Electric_Account_Holder_First_Name__c 		= this.clliObj.Customer__r.Electric_Account__r.First_Name__c,
            Electric_Account_Holder_Last_Name__c		= this.clliObj.Customer__r.Electric_Account__r.Last_Name__c,
            Service_Address__c 							= this.clliObj.Customer__r.Service_Address_Full__c,
            Electric_Account__c 						= this.clliObj.Customer__r.Electric_Account_Number__c,
            Gas_Account__c 								= this.clliObj.Customer__r.Gas_Account_Number__c,
            End_Date__c 								= Date.Today().Adddays(32),
            Start_Date__c 								= Date.Today().Adddays(2),
            Do_you_heat_with_natural_Gas__c				= heatWithNaturalGas 
        );
        upsert this.ecObj;
        this.modelBreadcrumbTitle = 'Eligibility Check >';
    }
    public void loadContractors(String programId){
        Program__c program =  [select id,name,Portfolio__r.Account__c from program__c where id =:programId];
        if(program != null && program.Portfolio__r.Account__c != null){
            this.contractors = [select id,name from Trade_Ally_Account__c where Account__c =: program.Portfolio__r.Account__c];
        }else{
            this.contractors = [select id,name from Trade_Ally_Account__c limit 1000];
        }
    }
    public void loadCustomer(String clli_Id){
        this.customerObj = new Customer__c();
        Id recordId = (this.clliObj.Customer__c != null) ? this.clliObj.Customer__c : clli_Id;
        if(recordId  != null){
            DescribeSObjectResult describeResult = recordId.getSObjectType().getDescribe();
            List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
            String query =
              ' SELECT ' +
                  String.join( fieldNames, ',' ) +
              ' FROM ' +
                  describeResult.getName() +
              ' WHERE ' +
                  ' id = :recordId ' +
              ' LIMIT 1 '
            ;
            List<SObject> records = Database.query(query);
            this.customerObj = (customer__c)records.get(0);
        }
    }
}