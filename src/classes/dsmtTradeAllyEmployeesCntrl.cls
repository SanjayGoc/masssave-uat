public class dsmtTradeAllyEmployeesCntrl {
    public String employeesForManagerJSON{get;set;} 
    public DSMTracker_Contact__c thisContact{get;set;} 
    
    public dsmtTradeAllyEmployeesCntrl(){
        loadLoggedInUserContactInfo();
    }
    
    public void loadEmployeesForManager(){
        List<Object> employeeRows = new List<Object>();
        if(thisContact != null && thisContact.Trade_Ally_Account__c != null){
         	List<DSMTracker_Contact__c> employeesForManager = [
                SELECT Id,Name,First_Name__c,Last_Name__c,Email__c,Phone__c,Title__c
                FROM Dsmtracker_Contact__c 
                WHERE (status__c = 'Approved' or Status__c = '') AND Trade_Ally_Account__c =: thisContact.Trade_Ally_Account__c AND Review__c = null
            ];  
            if(employeesForManager != null){
                for(DSMTracker_Contact__c emp :employeesForManager){
                    employeeRows.add(new Map<String,Object>{
                        	'Id'=>emp.Id,
                            'FirstName'=>emp.First_Name__c,
                            'LastName'=>emp.Last_Name__c,
                            'Email'=>emp.Email__c,
                            'Phone'=>emp.Phone__c,
                            'Title'=>emp.Title__c
                    });
                }
            }
        }
        this.employeesForManagerJSON = JSON.serialize(employeeRows);
    }
    
    public void loadLoggedInUserContactInfo(){
        User loggedInUser = [SELECT Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name FROM User WHERE Id =: UserInfo.getUserId()];
        if(loggedInUser != null && loggedInUser.ContactId != null){
            thisContact = [
                SELECT Id, Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Trade_Ally_Account__r.Id,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c 
                FROM DSMTracker_Contact__c 
                WHERE Contact__r.Id = : loggedInUser.ContactId AND Trade_Ally_Account__c != null 
                LIMIT 1
            ];
        }
    }
}