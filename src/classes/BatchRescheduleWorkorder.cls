public class BatchRescheduleWorkorder implements Database.batchable<SObject>,Database.Stateful,schedulable,Database.AllowsCallouts{ 
    
    public List<workOrderDetail> lstWOD ;
    public BatchRescheduleWorkorder(){
        lstWOD = new List<workOrderDetail>();
    }
    
    public database.querylocator start(Database.batchableContext batchableContext){
         Date myDate = Date.today();
         Time myTime = Time.newInstance(0, 0, 0, 0);
         Time myTime1 = Time.newInstance(23, 59,59, 0);
         DateTime stDt = DateTime.newInstanceGMT(myDate, myTime);
         DateTime endDt = DateTime.newInstanceGMT(myDate, myTime1);
         system.debug('@@End Date@@'+endDt );
         String sQuery ='select id,Scheduled_Date__c,Scheduled_Start_Date__c,Service_Date__c,Duration__c,Address_Formula__c,Early_Arrival_Time__c,Late_Arrival_Time__c,External_Reference_ID__c,Workorder_Type__c,Work_Team__r.Captain__c from Workorder__c where LastModifiedDate >= :stDt AND LastModifiedDate <= :endDt AND Status__c = \'Requires Review\'';
         System.debug('===sQuery===='+sQuery);
         return Database.getQueryLocator(sQuery);
    }
    
    public void execute(Database.BatchableContext bc, List<Workorder__c> scope){ 
        Set<Id> woTypeIdLst = new Set<Id>();
        
        for(Workorder__c wo:scope){
            Set<String> empIdLstNew = null;
            if(wo.Workorder_Type__c!=null){
                    
                    list<Required_Skill__c> rsLst = [select id, Skill__c,Skill__r.Name,Minimum_Score__c from Required_Skill__c where Workorder_Type__c = :wo.Workorder_Type__c ];
            
                    set<string> skillIds = new set<string>();
                    
                    for(Required_Skill__c r : rsLst){
                        skillIds.add(r.Skill__c);
                    }
                    
                    system.debug('--skillIds---'+skillIds);
                    
                    list<Employee_Skill__c> esLst = [select id, Skill__c,Employee__c,Employee__r.Name,Survey_Score__c from Employee_Skill__c where skill__c in : skillIds];
            
                    
                    Map<Id,list<Employee_Skill__c>> esMap = new Map<Id,list<Employee_Skill__c>>();
                    list<Employee_Skill__c> temp = null;
                    
                    for(Employee_Skill__c  es : esLst){
                            
                            if(esMap.get(es.Employee__c) != null){
                                temp = esMap.get(es.Employee__c);
                                temp.add(es);
                                esmap.put(es.Employee__c,temp);
                            }else{
                                temp = new list<Employee_Skill__c>();
                                temp.add(es);
                                esmap.put(es.Employee__c,temp);
                            }
                    }
                    system.debug('--esmap--'+esmap);
                    
                    Set<Id> empSkill = new Set<Id>();
                    boolean add = false;
                    Set<Id> empIdset = new Set<Id>();
                    boolean reqadd = false;
                    string strEmpId ='';
                    
                   
                     map<string, decimal> empSkillRateMap = new map<string, decimal>();

                    
                     for(Id empId : esMap.keyset()){
                        add = false;
                        temp = esmap.get(empId);
                        decimal count = 0;
                        for(Required_Skill__c r : rsLst){
                            reqadd = false;
                            for(Employee_Skill__c  es : temp){
                                if(es.Survey_Score__c >= r.Minimum_Score__c){
                                    if(r.Skill__c == es.Skill__c){
                                        count += es.Survey_Score__c;
                                        reqadd = true;
                                    }
                                }
                            }
                            if(reqadd == false){
                                break;
                            }
                        }
                        if(!reqadd){
                            add = false;
                        }else{
                            add = true;
                        }
                        
                        if(add){
                            empSkillRateMap.put(empId, count);
                        }
                    }
                    
                    system.debug('empSkillRateMap :::::'+ empSkillRateMap);
                    List<String> empIdLst = new List<String>();
                    if(empSkillRateMap.size() > 0){
                        list<decimal> rateList = empSkillRateMap.values();
                        rateList.sort();
                        system.debug('rateList :::::'+ rateList);
                        for(integer i = rateList.size()-1; i >= 0; i--){
                            for(string s : empSkillRateMap.keyset()){
                                if(rateList[i] == empSkillRateMap.get(s))
                                {
                                        empIdLst.add(s);
                                }
                            }
                        }
                    }
                
                if(empIdLst.size()>0){
                    Date d1= wo.Service_Date__c;
                    List<Work_Team__c> wtList = [select id, name,Captain__c  from work_team__c where Captain__c in : empIdLst and 
                                                Service_Date__c =:d1 limit 9999];
                
                    
                    empIdLstNew = new Set<string>();
                    
                    for(Work_Team__c wt : wtList){
                        if(wt.Captain__c != wo.Work_Team__r.Captain__c)
                            empIdLstNew.add(wt.Captain__c);
                    }
                }
                
            }
            String dtConverted = '';
            workOrderDetail wod = new workOrderDetail();
            wod.employeeId = empIdLstNew ;
            wod.serviceAddress = wo.Address_Formula__c;
            wod.serviceDuration = Integer.valueof(wo.Duration__c);
            system.debug('--dsmtCallOut.StaticSessionId---'+dsmtCallOut.StaticSessionId);
            List<Date> lstDate = new List<Date>();
            lstDate.add(wo.Service_Date__c);
            wod.PlanDate = lstDate;
            
            if(dsmtCallOut.StaticSessionId != null){
                wod.SessionId = dsmtCallOut.StaticSessionId;
            }else{
                wod.sessionId = String.valueOf(Datetime.now())+':'+string.valueof(DateTime.now().millisecond());
                dsmtCallOut.StaticSessionId = wod.SessionId;
            }
            
            if(wo.External_Reference_ID__c!=null)
                wod.SQLRefId = wo.External_Reference_ID__c;
            wod.workOrderId = wo.Id;
            
            if(wo.Early_Arrival_Time__c!=null){
                dtConverted = wo.Early_Arrival_Time__c.format('MM/dd/yyyy h:mm a');
                wod.earlyArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
            }
            system.debug('@@wo.Late_Arrival_Time__c@@'+wo.Late_Arrival_Time__c);
            if(wo.Late_Arrival_Time__c!=null){
                dtConverted = wo.Late_Arrival_Time__c.format('MM/dd/yyyy h:mm a');
                wod.lateArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
            }
            
            lstWOD.add(wod);
            system.debug('@@lstWOD@@'+lstWOD);
        }
    }
    
    public void finish(Database.BatchableContext batchableContext) {
        system.debug('@@lstWOD1@@'+lstWOD);
        if(lstWOD.size()>0){
            dsmtCallOut.loginDetail ldObj = new dsmtCallOut.loginDetail();
           
            ldObj.userName = Login_Detail__c.getInstance().UserName__c;
            ldObj.password = Login_Detail__c.getInstance().Password__c;
            ldObj.orgID = Login_Detail__c.getInstance().Org_Id__c;
            ldObj.securityToken = Login_Detail__c.getInstance().Security_Token__c;
            ldObj.serverURL = Login_Detail__c.getInstance().Server_URL__c;
            
            String lgnstr = JSON.serialize(ldObj); 
            system.debug('@@lgnstr @@'+lgnstr );
            String str = JSON.serialize(lstWOD);
            String body = '{"workOrderDetail":'+str+',"loginDetail":'+lgnstr+'}';
            system.debug('@@body@@'+ body );
            HttpRequest req = new HttpRequest();
            Http http = new Http();
            HTTPResponse res = null;
            
            req.setEndpoint(System_Config__c.getInstance().URL__c+'RescheduleWorkOrder?type=json');
            req.setMethod('POST');
            req.setBody(body);
            req.setHeader('content-type', 'application/json');
            // req.setHeader('content-length', '200');
            http = new Http();
            if(!Test.isRunningTest()){
                res = http.send(req);
                System.debug(res.getBody());
            }
        }
    }
    
    public void execute(SchedulableContext sc) {
        BatchRescheduleWorkorder obj = new BatchRescheduleWorkorder();
        database.executebatch(obj,1);
    }
    
    public class workOrderDetail{
        public Set<string> employeeId;
        public string serviceAddress;
        public List<Date> PlanDate;
        public Integer serviceDuration;
        public string sessionId;
        public string SQLRefId;
        public string workOrderId;
        public string earlyArrivalTime;
        public string lateArrivalTime;
    }
}