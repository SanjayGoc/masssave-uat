@isTest
Public class dsmtSyncWorkTeamsControllerTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        acc.Billing_Account_Number__c= 'Gas-~~~1234344';
        insert acc;
        Location__c loc= Datagenerator.createLocation();
        insert loc;
        Employee__c em = Datagenerator.createEmployee(loc.id);
        em.Status__c='Approved';
        insert em;
        Employee__c em2 = Datagenerator.createEmployee(loc.id);
        em2.Status__c='Approved';
        insert em2;
        
        Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
        insert wt;

        ApexPages.currentPage().getParameters().put('Id',em.id);
        dsmtSyncWorkTeamsController controller = new dsmtSyncWorkTeamsController();
        controller.autoRun();
        
        
    }
}