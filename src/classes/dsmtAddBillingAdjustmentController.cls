public class dsmtAddBillingAdjustmentController{

    public String reviewId {get;set;}
    public String recommXml {get;set;}
    public Review__c rev{get;set;}
    public String dataSave{get;set;}
    public String reviewType {get;set;}
    public decimal subTotal {get;set;}
    public decimal utilityIncentive {get;set;}
    public decimal customerContribution {get;set;}
    public decimal customerDeposit {get;set;}
    public decimal remainingCustomerContribution {get;set;}
    public string EAName{get;set;}
    public string TAName{get;set;}
    
    public dsmtAddBillingAdjustmentController(ApexPages.StandardController controller) {
        init();
    }
    
    public dsmtAddBillingAdjustmentController() {
        init();
    }
    
    private void init()
    {
        rev = new Review__c();
        reviewId = ApexPages.currentPage().getParameters().get('id');
        
        initJSON();
        checkReviewType();
    }
    
    public void initJSON()
    {
        fillRecommendationsJSON(fetchProjectIdSet());
    }
    
    private void checkReviewType()
    {
        List<Review__C> reviewList = [select Id, RecordType.Name,Trade_Ally_Account__r.Name, Energy_Assessment__r.Name
                                      from Review__C 
                                      where Id =: reviewId 
                                      limit 1];
        if(reviewList.size() > 0){
            reviewType = reviewList[0].RecordType.Name; 
            EAName = reviewList[0].Energy_Assessment__r.Name;
            TAName = reviewList[0].Trade_Ally_Account__r.Name;
        }
    }
    
    private set<String> fetchProjectIdSet()
    {
        set<String> projectIdSet = new Set<String>();
        
        List<Project_Review__c> revList = [select id, Name, Review__c, Project__c 
                                           from Project_Review__c 
                                           where Review__c =: reviewId
                                           and Review__c != null];
        for(Project_Review__c pr : revList)
        {
            projectIdSet.add(pr.Project__c);
        }
        
        return projectIdSet;
    }
    
    private void fillRecommendationsJSON(Set<String> projectIdSet)
    {
        remainingCustomerContribution = customerDeposit = utilityIncentive = subTotal = customerContribution = 0;
        map<string, decimal> projCollectedMap = new map<string, decimal>();
        
        List<Recommendation__c> recomList = [select id, Name, Recommendation_Scenario__c, Recommendation_Description__c, Quantity__c, Units__c,
                                             isChecked__c, Recommendation_Scenario__r.Name, Inspected_Quantity__c, Change_Order_Quantity__c,
                                             Installed_Date__c, Change_Order_Installed_Date__c, DSMTracker_Product__r.Name, Part_Cost__c,
                                             Recommendation_Scenario__r.Total_Collected__c, Unit_Cost__c
                                             from Recommendation__c 
                                             where Recommendation_Scenario__c in : projectIdSet];
        
        recommXml = '<Recommendations>';
        if(recomList.size() > 0)
        {
            Set<Id> recId = new Set<Id>();
            Map<Id, Change_Order_Line_Item__c> changeOrderMap = new Map<Id, Change_Order_Line_Item__c>();
            
            for (Recommendation__c ir: recomList) {
                recId.add(ir.Id);
            }
            
            if(recId.size() > 0){
                List<Review__c> changeRevList = [select id 
                                                 from Review__c 
                                                 where (Billing_Review__c =: reviewId 
                                                 OR Work_Scope_Review__c =: reviewId) 
                                                 and RecordType.Name = 'Billing Adjustment'
                                                 and Status__c = 'New'
                                                 order by CreatedDate Desc];
                
                if(changeRevList  != null && changeRevList.size() > 0){
                    
                    Set<Id> crId = new Set<Id>();
                    
                    for( Review__c c : changeRevList){
                        crId.add(c.Id);
                    }
                
                    List<Change_Order_Line_Item__c> chageorderList = [select id,Change_Order_Quantity__c,Recommendation__c from Change_Order_Line_Item__c
                                                                      where Recommendation__c in : recId
                                                                      and (Billing_Review__c =: reviewId 
                                                                      OR Review__c =: reviewId )
                                                                      and Change_Order_Review__c =: changeRevList.get(0).Id
                                                                      order by CreatedDate Desc];
                                                                      
                    for(Change_Order_Line_Item__c c : chageorderList){
                        if(changeOrderMap.get(c.Recommendation__c) == null && c.Change_Order_Quantity__c != null){
                            changeOrderMap.put(c.Recommendation__c, c);
                        }
                    }
                }
            }
            
            for (Recommendation__c ir: recomList) {
                if(ir.Recommendation_Scenario__r.Total_Collected__c != null)
                    projCollectedMap.put(ir.Recommendation_Scenario__c + '~~~' + ir.Recommendation_Scenario__r.Name, ir.Recommendation_Scenario__r.Total_Collected__c);
                    
                recommXml += '<Recommendation>';
                
                    recommXml += '<Checked>' + checkBox(ir.isChecked__c) + '</Checked>';
                    recommXml += '<ProjectId>' + ir.Recommendation_Scenario__c + '</ProjectId>';
                    recommXml += '<ProjectName><![CDATA[' + ir.Recommendation_Scenario__r.Name + ']]></ProjectName>';
                    recommXml += '<RecommendationId>' + ir.Id + '</RecommendationId>';
                    
                    if(ir.DSMTracker_Product__r.Name != null)
                        recommXml += '<RecommendationName><![CDATA[' + ir.DSMTracker_Product__r.Name + ']]></RecommendationName>';
                    else
                        recommXml += '<RecommendationName></RecommendationName>';
                    
                    if(ir.Quantity__c != null)
                        recommXml += '<Quantity><![CDATA[' + ir.Quantity__c+ ']]></Quantity>';
                    else
                        recommXml += '<Quantity></Quantity>';
                        
                    if(ir.Inspected_Quantity__c == null)
                        recommXml += '<InspectedQuanity></InspectedQuanity>';
                    else    
                        recommXml += '<InspectedQuanity><![CDATA[' + ir.Inspected_Quantity__c + ']]></InspectedQuanity>';
                    
                    if(changeOrderMap.containsKey(ir.Id) && changeOrderMap.get(ir.Id).Change_Order_Quantity__c != null)
                        recommXml += '<ChangeOrderQuantity><![CDATA[' + changeOrderMap.get(ir.Id).Change_Order_Quantity__c + ']]></ChangeOrderQuantity>';
                    else        
                        recommXml += '<ChangeOrderQuantity></ChangeOrderQuantity>';
                    
                    if(ir.Installed_Date__c != null)
                        recommXml += '<InstalledDate>' + ir.Installed_Date__c.format() + '</InstalledDate>';
                    else
                        recommXml += '<InstalledDate></InstalledDate>';
                        
                    if(ir.Change_Order_Installed_Date__c != null)
                        recommXml += '<ChangeOrderInstalledDate><![CDATA[' + ir.Change_Order_Installed_Date__c.format() + ']]></ChangeOrderInstalledDate>';
                    else
                        recommXml += '<ChangeOrderInstalledDate></ChangeOrderInstalledDate>';
                
                recommXml += '</Recommendation>';
                
                if(changeOrderMap.get(ir.Id) != null && changeOrderMap.get(ir.Id).Change_Order_Quantity__c != null){
                    subTotal += changeOrderMap.get(ir.Id).Change_Order_Quantity__c * ir.Unit_Cost__c;
                }else{
                    if(ir.Part_Cost__c != null)
                        subTotal += ir.Part_Cost__c;
                }
            }
        }
        
        if(subTotal > 0)
            utilityIncentive = (subTotal * .75);
        
        customerContribution = subTotal - utilityIncentive;
        
        for(String s : projCollectedMap.keyset())
        {
            customerDeposit += projCollectedMap.get(s);
        }
        
        remainingCustomerContribution = (customerContribution - customerDeposit);
        
        recommXml += '</Recommendations>';
    }
    
    boolean checkBox(boolean chk){
        return chk ? true : false;
    }
    
    public PageReference createReviewRec(){
        String projId = ApexPages.currentPage().getParameters().get('projid');
        
        if(reviewId != null)
        {
            loadChangeOrderReview();
        }
        else if(projId != null)
        {    
            List<Project_Review__c> prList = [select id, Review__c, Project__c 
                                              from Project_Review__c 
                                              where Project__c =: projId
                                              and Review__c != null
                                              and (Review__r.RecordType.Name = 'Billing Review'
                                              or Review__r.RecordType.Name = 'Billing Review Manual')];
            
            if(prList.size() > 0)
                return new PageReference('/apex/dsmtAddBillingAdjustment?id='+prList[0].Review__c+'&projid='+projId).setRedirect(true);
        }
        
        return null;
    }
    
    public void CreateReviewRecord(){
        if(reviewId  != null)
        {
            List<Review__c> reviewList = [select Id,Name,Project__c,Status__c,Work_Scope_Review__c,Trade_Ally_Account__r.Name,
                                          Energy_Assessment__r.Name 
                                          from Review__c 
                                          where Work_Scope_Review__c = :reviewId  
                                          and Type__c = 'Billing Adjustment'
                                          and Status__c = 'New'
                                          limit 1];
            
            if(reviewList.size() > 0)
            {
                rev = reviewList[0];
            }
            else
            {
                loadChangeOrderReview();
                upsert rev; 
            }
        }
    }
    
    private void loadChangeOrderReview()
    {
        List<Review__C> lstPrj = [select Id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                        Energy_Assessment__r.Trade_Ally_Account__r.Email__c,First_Name__c,
                                        Last_Name__c,Phone__c,Email__c,Address__c,City__c,State__c,
                                        Zipcode__c,Trade_Ally_Account__c from Review__C 
                                        where Id =: reviewId limit 1]; 
        rev.Status__c = 'New';
        Id devRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Billing Adjustment').getRecordTypeId();

        List<Dsmtracker_Contact__c> dsmtList = [select id,Dsmtracker_Contact__c,Trade_Ally_Account__c from Dsmtracker_Contact__c where Portal_User__c =: userinfo.getUSerID()
                                                    and Trade_Ally_Account__c  != null];
        if(dsmtList.size() > 0){
            rev.Trade_Ally_Account__c = dsmtList.get(0).Trade_Ally_Account__c;
        }                                   
        if(lstPrj.get(0).Energy_Assessment__c != null)    
            rev.Energy_Assessment__c = lstPrj.get(0).Energy_Assessment__c;
        
        rev.Type__c = 'Billing Adjustment';
        rev.First_Name__c = lstPrj.get(0).First_Name__c;
        rev.Last_Name__c = lstPrj.get(0).Last_Name__c;
        rev.Phone__c = lstPrj.get(0).Phone__c;
        rev.Email__c = lstPrj.get(0).Email__c;
        rev.Address__c = lstPrj.get(0).Address__c;
        rev.City__c = lstPrj.get(0).City__c;
        rev.State__c = lstPrj.get(0).State__c;
        rev.Zipcode__c = lstPrj.get(0).Zipcode__c;
        rev.Billing_Review__c = lstPrj.get(0).Id;
        
        if(lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c != null)
            rev.Trade_Ally_Email__c = lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c;
        
        rev.RecordTypeId =  devRecordTypeId; 
    }
    
    public PAgereference CreateChagneOrderItems()
    {
        system.debug('sddsfsdf ::::::');
        CreateReviewRecord();
        
        List<Recommendation__c> lstRec = new List<Recommendation__c>();
        List<Change_Order_Line_Item__c> lstChangeOrderItems = new List<Change_Order_Line_Item__c >();
        
        system.debug('dataSave ::::::' + dataSave);
        if(dataSave != null && dataSave != ''){
            map<String, String> recQMap = new map<String, String>();
            list<String> splitedData = dataSave.split(';');
            
            for(string s: splitedData){
                recQMap.put(s.split('-')[0], s.split('-')[1]);
            }
            
            system.debug('recQMap :::::' + recQMap);
            
            List<Recommendation__c> recomList = [select id, Name, Recommendation_Scenario__c, Recommendation_Description__c, Quantity__c, Units__c,
                                                 isChecked__c, Recommendation_Scenario__r.Name, Inspected_Quantity__c, Change_Order_Quantity__c,
                                                 Change_Order_Installed_Date__c
                                                 from Recommendation__c 
                                                 where id in : recQMap.keyset()];
            
            Set<Id> recId = new Set<Id>();
            Map<Id, Change_Order_Line_Item__c> changeOrderMap = new Map<Id, Change_Order_Line_Item__c>();
            
            for (Recommendation__c ir: recomList) {
                recId.add(ir.Id);
            }
            
            if(recId.size() > 0){
                List<Review__c> changeRevList = [select id 
                                                 from Review__c 
                                                 where (Billing_Review__c =: reviewId
                                                 OR Work_Scope_Review__c =: reviewId) 
                                                 and RecordType.Name = 'Billing Adjustment'
                                                 and Status__c = 'New'
                                                 order by CreatedDate Desc];
                
                if(changeRevList  != null && changeRevList.size() > 0){
                    
                    Set<Id> crId = new Set<Id>();
                    
                    for( Review__c c : changeRevList){
                        crId.add(c.Id);
                    }
                
                    List<Change_Order_Line_Item__c> chageorderList = [select id,Change_Order_Quantity__c,Recommendation__c from Change_Order_Line_Item__c
                                                                      where Recommendation__c in : recId 
                                                                      and (Billing_Review__c =: reviewId
                                                                      OR Review__c =: reviewId)
                                                                      and Change_Order_Review__c =: changeRevList.get(0).Id
                                                                      and Status__c = 'New'
                                                                      order by CreatedDate Desc];
                                                                      
                    for(Change_Order_Line_Item__c c : chageorderList){
                        if(changeOrderMap.get(c.Recommendation__c) == null && c.Change_Order_Quantity__c != null){
                            changeOrderMap.put(c.Recommendation__c, c);
                        }
                    }
                }
            }
                                                 
            for(Recommendation__c r : recomList){
                Change_Order_Line_Item__c rec = new Change_Order_Line_Item__c();
                
                List<String> splitedQnty = recQMap.get(r.Id).split(',');
                
                decimal changeOrderQn = r.Change_Order_Quantity__c;
                
                if(changeOrderMap.containsKey(r.Id))
                    changeOrderQn = changeOrderMap.get(r.Id).Change_Order_Quantity__c;
                
                try{
                    if(integer.valueof(changeOrderQn) != integer.valueof(splitedQnty[0]) ||
                       r.Change_Order_Installed_Date__c != convertStrToDate(splitedQnty[1])){
                       
                        rec.Recommendation__c = r.Id;
                        
                        if(changeOrderMap.containsKey(r.Id))
                            rec.Id = changeOrderMap.get(r.Id).Id;
                            
                        rec.Change_Order_Quantity__c = splitedQnty[0] == null ? 0: integer.valueof(splitedQnty[0]);
                        rec.Change_Order_Installed_Date__c = convertStrToDate(splitedQnty[1]);
                        rec.Review__c = reviewId;
                        system.debug('--rev.Id--'+rev.Id);
                        rec.Change_Order_Review__c = rev.Id;
                        rec.Project__c = r.Recommendation_Scenario__c;
                        rec.Original_Quantity__c = r.Quantity__c;
                        rec.Status__c = 'New';
                        
                        lstChangeOrderItems.add(rec);
                        
                        r.Change_Order_Quantity__c = splitedQnty[0] == null ? 0: integer.valueof(splitedQnty[0]);
                        r.Change_Order_Installed_Date__c = convertStrToDate(splitedQnty[1]);
                    }
                }
                catch(Exception ex){
                    system.debug('Exception :::::' + ex.getMessage());
                    
                    if(splitedQnty[0].trim() == ''){
                        r.Change_Order_Quantity__c = null;
                        r.Change_Order_Installed_Date__c = convertStrToDate(splitedQnty[1]);
                        
                        if(changeOrderMap.containsKey(r.Id)){
                            rec.Change_Order_Quantity__c = null;  
                            rec.Id = changeOrderMap.get(r.Id).Id;  
                            rec.Change_Order_Installed_Date__c = convertStrToDate(splitedQnty[1]);
                            
                            lstChangeOrderItems.add(rec);
                        }
                    }
                    
                    continue;
                } 
            }
            
            if(lstChangeOrderItems.size()>0){
                upsert lstChangeOrderItems;
            }
            
            update recomList;
        }
        
        String navigate = ApexPages.currentPage().getParameters().get('navigate');
        
        if(navigate == 'yes')
            return new Pagereference('/'+rev.Id);
        else
            initJSON();
            
        return null;
    }
    
    date convertStrToDate(String dt)
    {
        if(dt != null && !dt.contains('NaN'))
        {
            list<String> sList = dt.split('/');
            if(sList.size() == 3)
                return date.newInstance(integer.valueOf(sList[2]), integer.valueOf(sList[0]), integer.valueOf(sList[1]));
        }
        
        return null;
    }
}