public class dsmtSyncTerritoryController{
 
    // Constructor - this only really matters if the autoRun function doesn't work right
    public dsmtSyncTerritoryController() {
    }
     
    // Code we will invoke on page load.
    public PageReference autoRun() {
 
        String LocId = ApexPages.currentPage().getParameters().get('Id');
 
        dsmtCallOut.SyncTerritory(LocId);
 
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/' + LocId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}