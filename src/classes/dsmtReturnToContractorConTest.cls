@istest
public class dsmtReturnToContractorConTest 
{
	@istest
    static void runtest()
    {
        Trade_Ally_Account__c ta =new Trade_Ally_Account__c();
        ta.Name='test';
        insert ta;
        
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Contact_First_Name__c='marwel';
        ir.Contact_Last_Name__c='stadio';
        ir.Contractor_Name__c=ta.id;
        insert ir;
        
        /*Review__c rv =new Review__c();
        rv.Type__c='Post-Inspection Result';
        rv.Inspection_Request__c=ir.id;
        rv.Trade_Ally_Account__c=ir.Contractor_Name__c;
        rv.Trade_Ally_Email__c = Ir.Contractor_Name__r.Email__c;
        rv.Status__c = 'Pending Contractor Acceptance';
        insert rv;*/
        
        ApexPages.StandardController sc = new ApexPages.StandardController(ir);
        dsmtReturnToContractorCon cntrl = new dsmtReturnToContractorCon(sc);
        cntrl.createReviewRec();
    }
}