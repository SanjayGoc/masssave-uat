@isTest
Public class dsmtbatchScheduledWorkTeamTest{
    @isTest
    public static void runTest(){
            Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        wt.unavailable__c = true;
        insert wt;
        
        dsmtbatchScheduledWorkTeam cntrl = new dsmtbatchScheduledWorkTeam ();
        cntrl.execute(null);
    }
    
}