@isTest
Public class TriggersTest{
    @isTest
    public static void runTest(){
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        obj.Account__c = 'Gas-~~~1234';
        insert obj;
        
       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Trade_Ally_Type__c='HPC';
        update Tacc;
        
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        app.Customer_Reference__c = cust.id;
        app.Appointment_Status__c  = 'Scheduled';
        app.Employee__c  = null;
        update app;
       
        
        
        
    }
       static testMethod void case1(){
           Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
          
       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Trade_Ally_Type__c='HPC';
        update Tacc;
        Trade_Ally_Account__c Tacc2= Datagenerator.createTradeAccount();
        Tacc2.Trade_Ally_Type__c='HPC';
        update Tacc2;
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        Email_Custom_Setting__c emailset= Datagenerator.createEmailCustSetting();
        emailset.RR_Submitted_Owner_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Submitted_TA_ThankYou_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_TA_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_DSMTContact_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        update emailset;
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        update dsmt;
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        update app;
        
        Location__c loc = Datagenerator.createLocation();
        insert loc;
        
       /* Employee__c emp = Datagenerator.createEmployee(Loc.Id);
       // emp.DSMTracker_Contact__c = c.Id;
        insert emp;
        app.employee__c = emp.Id;
        app.DSMTracker_Contact__c = null;
        update app;*/
        
        app.Appointment_Start_Time__c = datetime.now().AddSeconds(10);
        app.employee__c = null;
        app.Work_Team_Member__c = null;
        app.DSMTracker_Contact__c=dsmt.id;
        update app;

        
     }
       
       
       static testMethod void case2(){
           Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
          
       
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        Program_Eligibility__c PE= Datagenerator.createProgramEL();
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Tacc.Trade_Ally_Type__c='HPC';
        Tacc.Internal_Account__c = true;
        update Tacc;
        
        Trade_Ally_Account__c Tacc2= Datagenerator.createTradeAccount();
        Tacc2.Trade_Ally_Type__c='HPC';
        update Tacc2;
        
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        insert cust;
        
        Email_Custom_Setting__c emailset= Datagenerator.createEmailCustSetting();
        emailset.RR_Submitted_Owner_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Submitted_TA_ThankYou_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_TA_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        emailset.RR_Approved_DSMTContact_Email_Template__c='Notification_to_Requester_For_Approved_Status';
        update emailset;
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        dsmt.Status__c = 'Approved';
        dsmt.Trade_Ally_Type_PL__c = 'HPC';
        dsmt.Email__c = 'test@ucs.com';
        update dsmt;
        
        Appointment__c app= Datagenerator.createAppointment(EL.Id);
        app.DSMTracker_Contact__c=dsmt.id;
        app.Customer_Reference__c = cust.Id;
        app.Appointment_Status__c = 'Scheduled';
        Workorder__c wo = new Workorder__c(Status__c='Unscheduled');
        wo.Requested_Start_Date__c  = Datetime.now();
        wo.Requested_End_Date__c  = Datetime.now().Addhours(2);
        wo.Scheduled_Start_Date__c = Datetime.now();
        wo.Scheduled_End_Date__c= Datetime.now().Addhours(2);
        Workorder_Type__c woType = new Workorder_Type__c(Name='Wifi');
        woType.Qualifying_Audit__c  = true;
        insert woType;
        wo.workorder_Type__c = woType.Id;
        insert wo;
        app.Workorder__c = wo.Id;
        update app;
        
      }
       
       @isTest
       public static void TA_DSMT_RR_Triggers(){
            
            Email_Custom_Setting__c setting = new Email_Custom_Setting__c();
            setting.DSMT_ES_Registration_Email_Template__c = 'Send_Registration_Request_for_DSMT_Contact';
            insert setting;
            
           
            
            List<Exception_Template__c> explist = new List<Exception_Template__c>();
            
            Exception_Template__c emailTemp= new Exception_Template__c(Object_Sub_Type__c='HPC', Reference_ID__c='ETN-0000001', Automatic__c = true, Active__c = true,Registration_Request_Level_Message__c= true);
            explist.add(emailTemp);
            
            Exception_Template__c emailTemp2 = emailTemp.clone();
            emailTemp2.Reference_Id__c = 'ETN-0000002';
            explist.add(emailtemp2);
            
            Exception_Template__c emailTemp3 = emailTemp.clone();
            emailTemp3.Reference_Id__c = 'ETN-0000003';
            explist.add(emailtemp3);
            
            Exception_Template__c emailTemp4 = emailTemp.clone();
            emailTemp4.Reference_Id__c = 'ETN-0000004';
            explist.add(emailtemp4);
            
            Exception_Template__c emailTemp5 = emailTemp.clone();
            emailTemp5.Reference_Id__c = 'ETN-0000005';
            explist.add(emailtemp5);
            
            Exception_Template__c emailTemp6 = emailTemp.clone();
            emailTemp6.Reference_Id__c = 'ETN-0000006';
            explist.add(emailtemp6);
            
            Exception_Template__c emailTemp7 = emailTemp.clone();
            emailTemp7.Reference_Id__c = 'ETN-0000007';
            explist.add(emailtemp7);
            
            Exception_Template__c emailTemp8 = emailTemp.clone();
            emailTemp8.Reference_Id__c = 'ETN-0000008';
            explist.add(emailtemp8);
            
            Exception_Template__c emailTemp9 = emailTemp.clone();
            emailTemp9.Reference_Id__c = 'ETN-0000009';
            explist.add(emailtemp9);
            
            Exception_Template__c emailTemp10 = emailTemp.clone();
            emailTemp10.Reference_Id__c = 'ETN-0000010';
            explist.add(emailtemp10);
            
            Exception_Template__c dsmttemp = emailTemp.clone();
            dsmttemp.Reference_Id__c = 'ETN-0000011';
            dsmttemp.Registration_Request_Level_Message__c = false;
            dsmttemp.DSMTracker_Contact_Level_Message__c = true;
            explist.add(dsmttemp);
            
            Exception_Template__c dsmttemp1 = dsmttemp.clone();
            dsmttemp1.Reference_Id__c = 'ETN-0000012';
            explist.add(dsmttemp1);
            
            Exception_Template__c dsmttemp2 = dsmttemp.clone();
            dsmttemp2.Reference_Id__c = 'ETN-0000013';
            explist.add(dsmttemp2);
            
            Exception_Template__c dsmttemp3 = dsmttemp.clone();
            dsmttemp3.Reference_Id__c = 'ETN-0000014';
            explist.add(dsmttemp3);
            
            Exception_Template__c dsmttemp4 = dsmttemp.clone();
            dsmttemp4.Reference_Id__c = 'ETN-0000015';
            explist.add(dsmttemp4);
                                            
            insert explist;
            
            Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
            Tacc.Trade_Ally_Type__c='HPC';
            update Tacc;
            
            DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
            dsmt.Trade_Ally_Account__c =Tacc.id; 
            dsmt.Trade_Ally_Type_PL__c = 'HPC';
            dsmt.Title__c = 'Crew Chief';
            update dsmt;
            
            dsmt.Title__c = 'Energy specialist';
            update dsmt;
            
            Registration_Request__c reg= Datagenerator.createRegistration();
            reg.DSMTracker_Contact__c=dsmt.id; 
            reg.Trade_Ally_Account__c =Tacc.ID;
            reg.status__c ='Submitted';
            update reg;
            
            
            List<Attachment__c> lstAttch = new List<Attachment__c>();
        
        
            Attachment__c att2 = new Attachment__c(Status__c='Uploaded',Registration_Request__c=reg.id,Attachment_Type__c='HIC');
            lstAttch.add(att2);
        
            Attachment__c att3 = att2.clone();
            att3.Attachment_Type__c = 'W-9';
            lstAttch.add(att3);
            
            Attachment__c att4 = att2.clone();
            att4.Attachment_Type__c = 'BPI Gold Star';
            lstAttch.add(att4);
            
            Attachment__c att5 = att2.clone();
            att5.Attachment_Type__c = 'Liability $1M/$2M';
            lstAttch.add(att5);
            
            Attachment__c att6 = att2.clone();
            att6.Attachment_Type__c = 'Lead Safe';
            lstAttch.add(att6);
            
            Attachment__c att7 = att2.clone();
            att7.Attachment_Type__c = 'Auto $1M';
            lstAttch.add(att7);
            
            Attachment__c att8 = att2.clone();
            att8.Attachment_Type__c = 'Excess Liability $1M/$2M';
            lstAttch.add(att8);
            
            Attachment__c att9 = att2.clone();
            att9.Attachment_Type__c = 'Workers Comp $500K';
            lstAttch.add(att9);
            
            Attachment__c att10 = att2.clone();
            att10.Attachment_Type__c = 'Logo for Online Profile';
            lstAttch.add(att10);
            
            Attachment__c att11 = att2.clone();
            att11.Attachment_Type__c = 'Accurate Summary background check';
            lstAttch.add(att11);
            
            Attachment__c att12 = att2.clone();
            att12.Attachment_Type__c = 'Personal Lead Safe';
            att12.Dsmtracker_contact__c = dsmt.id;
            lstAttch.add(att12);
            
            Attachment__c att13 = att12.clone();
            att13.Attachment_Type__c = 'Crew Chief Certification';
            lstAttch.add(att13);
            
            Attachment__c att14 = att12.clone();
            att14.Attachment_Type__c = 'BPI Building Analyst';
            lstAttch.add(att14);
            
            Attachment__c att15 = att12.clone();
            att15.Attachment_Type__c = 'BPI Envelope';
            lstAttch.add(att15);
            
            Attachment__c att16 = att12.clone();
            att16.Attachment_Type__c = 'CSL';
            lstAttch.add(att16);
            
            insert lstAttch;
        
        
       }
    
    testmethod static void apptStatusChangeTrigger()
    {
        Trade_Ally_Account__c ta = Datagenerator.createTradeAccount();
        Trade_Ally_Account__c ta2 = Datagenerator.createTradeAccount();
        
        Account ac = Datagenerator.createAccount();
        insert ac;
        
        Premise__c pr = Datagenerator.createPremise();
        insert pr;
        
        Customer__c c = Datagenerator.createCustomer(ac.Id, pr.Id);
        c.Trade_Ally_Account__c = ta.Id;
        insert c;
        
        Eligibility_Check__c el = Datagenerator.createELCheck();
        
        Appointment__c app = Datagenerator.createAppointment(el.Id);
        app.Customer_Reference__c = c.Id;
        
        upsert app;
        
        c.Trade_Ally_Account__c = ta2.Id;
        update c;
        
        
        app.Trade_Ally_Account__c=ta2.Id;
        app.Appointment_Status__c = 'Pending';
        upsert app;
        
        c.Trade_Ally_Account__c = ta.Id;
        update c;
        
         app.Trade_Ally_Account__c=ta.Id;
        app.Appointment_Status__c = 'Cancelled';
        upsert app;
        
        c.Trade_Ally_Account__c = ta2.Id;
        update c;
        
    }
    
    testmethod static void AfterInsertUpdateSRTrigger()
    {
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Call_List_Type__c = 'Requires Reschedules';
        insert obj;
        
        Account ac = Datagenerator.createAccount();
        insert ac;
        
        Premise__c pr = Datagenerator.createPremise();
        insert pr;
        
        Customer__c con = Datagenerator.createCustomer(ac.Id, pr.Id);
        insert con;
        
        Service_Request__c sr = new Service_Request__c(Status__c = 'Approved');
        sr.Call_List_Line_Item__c = obj.Id;
        sr.Customer__c=con.Id;
        insert sr;
        
        sr.Status__c = 'Rejected';
        update sr;
        
        sr.Status__c = 'Submitted';
        Id EST_RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Energy Specialist Ticket').getRecordTypeId();
        sr.RecordtypeId = EST_RecordTypeId;
        sr.Sub_Type__c='Request PTO';
        update sr;
        
        sr.Status__c = 'Rejected';
        update sr;
        
        sr.Status__c = 'Submitted';
        sr.RecordtypeId = EST_RecordTypeId;
        sr.Sub_Type__c='Customer Cancellation/No Show';
        update sr;
        sr.Status__c = 'Rejected';
        update sr;
        
        sr.Status__c = 'Submitted';
        sr.RecordtypeId = EST_RecordTypeId;
        sr.Sub_Type__c='Request Return Visit/Combustion Safety Test';
        update sr;
    }
    
    testmethod static void AfterUpdateDSMTCUpdateEmployeeTrigger()
    {
        DSMTracker_Contact__c c = Datagenerator.createDSMTracker();
        c.Active__c = true;
        upsert c;
        
        Location__c loc = Datagenerator.createLocation();
        insert loc;
        Employee__c emp = Datagenerator.createEmployee(Loc.Id);
        emp.DSMTracker_Contact__c = c.Id;
        insert emp;
        
        emp.DSMTracker_Contact__c = c.Id;
        upsert emp;
        
        Eligibility_Check__c el = Datagenerator.createELCheck();
        
        
        Appointment__c app = Datagenerator.createAppointment(el.Id);
        app.DSMTracker_Contact__c = c.Id;
        upsert app;        
        
        c.Active__c = false;
        update c;
        
        c.Active__c = true;
        update c;
    }
    
     testmethod static void CreateWorkOrderBITrigger()
     {
         Account ac = Datagenerator.createAccount();
        insert ac;
        
        Premise__c pr = Datagenerator.createPremise();
        insert pr;
        
        Customer__c con = Datagenerator.createCustomer(ac.Id, pr.Id);
        insert con;
         
         DSMTracker_Contact__c c = Datagenerator.createDSMTracker();
         
         Eligibility_Check__c el = Datagenerator.createELCheck();
         
         Workorder_Type__c woType = new Workorder_Type__c(Name='Wifi');
         insert woType;
        
        Appointment__c app1 = new Appointment__c(Appointment_Status__c='Pending',Eligibility_Check__c=el.Id,Appointment_Start_Time__c=date.today(),Appointment_End_Time__c= date.today(),Appointment_Type__c='Wifi',DSMTracker_Contact__c = c.Id) ;
         app1.Customer_Reference__c = con.Id;
        upsert app1;
     }
    
    testmethod static void AssignCallListItemToCallistTrigger()
    {
        User u = Datagenerator.CreatePortalUser();
        
            
            Call_List__c callListObj1 = new Call_List__c();
            callListObj1.Name='Primary Call List';
            callListObj1.Status__c = 'Active';
            insert callListObj1;
        
        System.runAs(u)
        {
            Lead l = new Lead();
            l.LastName = 'test';
            l.Company = 'test';
            l.Read_date__c = Date.Today()-2;
            insert l;
            
            Call_List__c callListObj = new Call_List__c();
            callListObj.Name='Requires Reschedules';
            callListObj.Status__c = 'Active';
            callListObj.OwnerId=u.Id;
            insert callListObj;
            
            Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            obj.Call_List_Type__c = 'Requires Reschedules';
            insert obj;
            
            Call_List_Line_Item__c Obj1 = new Call_List_Line_Item__c();
            obj1.Status__c = 'Ready To Call';
            obj1.First_Name_New__c = 'Test1';
            obj1.Last_Name_New__c = 'Test1';
            obj1.Next_Followup_date__c  = Date.Today();
            obj1.Lead__c = l.Id;
            obj1.Phone_New__c  = '9909240665';
            obj1.Call_List_Type__c = 'Primary Call List';
            insert obj1;
        }
    }
    
    testmethod static void createAndSetAccountsBITrigger()
    {
        Id utilityRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Utility').getRecordTypeId();
        
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account  acc1= Datagenerator.createAccount();
        acc1.Name='National Grid Gas';
        insert acc1;
        
        Account  acc2= Datagenerator.createAccount();
        acc2.Name='National Grid Electric';
        insert acc2;
        
        Account  acc3= Datagenerator.createAccount();
        acc3.Name='National Grid Electric';
        acc3.RecordTypeId = utilityRecordTypeId;
        insert acc3;
                
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        cust.Customer_Not_Found__c=true;
        cust.Name=null;
        cust.Gas_Provider_Name__c='National Grid Gas';
        cust.Electric_Provider_Name__c='National Grid Electric';
        cust.Electric_Account_Number__c='112322221';
        cust.Gas_Account_Number__c='2323232323';
        cust.Electric_provider__c = acc3.Id;
        cust.Gas_provider__c = acc3.Id;
        insert cust;
        
        Customer__c cust1 = Datagenerator.createCustomer(acc1.id,null);
        cust1.Customer_Not_Found__c=true;
        cust1.First_Name__c=null;
        cust1.Name=null;
        insert cust1;
    }
    
    testmethod static void beforeInsertUpdateSRTrigger()
    {
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Call_List_Type__c = 'Requires Reschedules';
        insert obj;
        
        Account ac = Datagenerator.createAccount();
        insert ac;
        
        Premise__c pr = Datagenerator.createPremise();
        insert pr;
        
        Customer__c con = Datagenerator.createCustomer(ac.Id, pr.Id);
        insert con;
        
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        insert loc;
        
        
        
        Employee__c emp = new Employee__c ();
        emp.Name = 'emp name';
        emp.Location__c = Loc.Id;
        emp.Employee_Id__c = '123546';
        emp.Status__c = 'Active';
        emp.Email__c = 'test@pliant.com';
        
        insert emp;
                
        DSMTracker_Contact__c dsmt = new DSMTracker_Contact__c();
        dsmt.Is_TA_Internal_Account__c = true;
        insert dsmt;
        
        Service_Request__c sr = new Service_Request__c();
        sr.Call_List_Line_Item__c = obj.Id;
        sr.Customer__c=con.Id;
        sr.Employee__c = emp.id;
        insert sr;
        
        Trade_Ally_Account__c ta= new Trade_Ally_Account__c();
        ta.Name='test';
        insert ta;
        
        Service_Request__c sr2 = new Service_Request__c();
        sr2.Customer__c=con.Id;
        sr2.Trade_Ally_Account__c=ta.id;
        insert sr2;
        
        
        
        Service_Request__c sr3 = new Service_Request__c();
        sr3.Customer__c=con.Id;
        sr3.DSMTracker_Contact__c = dsmt.id;
        insert sr3;
        
    }
    testmethod static void cpyempToAppTrigger(){
        
        Energy_Specialist__c es = new Energy_Specialist__c();
        es.Name = 'test ES';
        insert es;
        
        Service_Request__c sr = new Service_Request__c();
        sr.Energy_Specialist__c=es.Id;
        insert sr;            
    }
    
    testmethod static void AfterInsertEmployeeCreateWorkTeam(){
        
        Schedules__c sc = new Schedules__c();
        sc.Name = 'Test SC';
        insert sc;
        
        List<Schedule_Line_Item__c> slilist = new list<Schedule_Line_Item__c>();
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c();
        sli.Schedules__c = sc.Id;
        sli.Schedule_Week__c='WEEK A';
        sli.WeekDay__c = 'Sunday';
        slilist.add(sli);
        
        Schedule_Line_Item__c sli1 = sli.clone();
        sli1.WeekDay__c = 'Monday';
        slilist.add(sli1);
        
        Schedule_Line_Item__c sli2 = sli.clone();
        sli2.WeekDay__c = 'Tuesday';
        slilist.add(sli2);
        
        Schedule_Line_Item__c sli3 = sli.clone();
        sli3.WeekDay__c = 'Wednesday';
        slilist.add(sli3);
        
        Schedule_Line_Item__c sli4 = sli.clone();
        sli4.WeekDay__c = 'Thursday';
        slilist.add(sli4);
        
        Schedule_Line_Item__c sli5 = sli.clone();
        sli5.WeekDay__c = 'Friday';
        slilist.add(sli5);
        
        Schedule_Line_Item__c sli6 = sli.clone();
        sli6.WeekDay__c = 'Saturday';
        slilist.add(sli6);
        
        insert slilist;
        
        List<Schedule_Line_Item__c> slilist1 = new list<Schedule_Line_Item__c>();
        
        Schedule_Line_Item__c sli7 = new Schedule_Line_Item__c();
        sli7.Schedules__c = sc.Id;
        sli7.Schedule_Week__c='WEEK B';
        sli7.WeekDay__c = 'Sunday';
        slilist1.add(sli7);
        
        Schedule_Line_Item__c sli8 = sli7.clone();
        sli8.WeekDay__c = 'Monday';
        slilist1.add(sli8);
        
        Schedule_Line_Item__c sli9 = sli7.clone();
        sli9.WeekDay__c = 'Tuesday';
        slilist1.add(sli9);
        
        Schedule_Line_Item__c sli10 = sli7.clone();
        sli10.WeekDay__c = 'Wednesday';
        slilist1.add(sli10);
        
        Schedule_Line_Item__c sli11 = sli7.clone();
        sli11.WeekDay__c = 'Thursday';
        slilist1.add(sli11);
        
        Schedule_Line_Item__c sli12 = sli7.clone();
        sli12.WeekDay__c = 'Friday';
        slilist1.add(sli12);
        
        Schedule_Line_Item__c sli13 = sli7.clone();
        sli13.WeekDay__c = 'Saturday';
        slilist1.add(sli13);
        
        insert slilist1;
        
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        insert loc;
        
        Employee__c emp = new Employee__c ();
        emp.Name = 'emp name';
        emp.Location__c = Loc.Id;
        emp.Employee_Id__c = '123546';
        emp.Status__c = 'Active';
        emp.Schedule__c = sc.id;
        insert emp;
        
        Work_Team__c wt = new Work_Team__c();
        wt.Deleted__c = true;
        wt.Captain__c = emp.Id;
        insert wt;
        
        Workorder__c w = new Workorder__c();
        w.Work_Team__c = wt.id;
        w.Status__c = 'Scheduled';
        w.Call_me_back_to_reschedule__c = true;
        w.Flexible__c = true;
        w.Scheduled_Start_Date__c = Datetime.now();
        w.Scheduled_End_Date__c = DAtetime.now().Addhours(2);
        insert w;
        w.Work_Team__c = null;
        update w;
        
        
        
        Schedules__c sc1 = new Schedules__c();
        sc1.Name = 'Test SC';
        insert sc1;
        
        emp.Schedule__c = sc1.id;
        update emp;
        
    }    
    
    testmethod static void AIScheduleCreateItems(){
        
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        insert loc;
        
        Schedules__c sc = new Schedules__c();
        sc.Name = 'Test SC';
        sc.Location__c = loc.id;
        insert sc;
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c();
        sli.Schedule_Week__c='WEEK A';
        sli.WeekDay__c = 'Sunday';
        sli.Location__c = loc.id;
        sli.Schedules__c = sc.Id;
        insert sli;
        
    }
    testmethod static void AttachmentBIBUTrigger(){
        Exception__c exp = new Exception__c();
        exp.Application_Type__c='Custom';
        insert exp;
            
        Attachment__c atc = new Attachment__c();
        atc.Exception__c=exp.Id;
        insert atc;
    }
    
    testmethod static void cretaeCallListforUserTrigger(){
        
        Call_List__c cl = new Call_List__c();
        cl.Name = 'test';
        insert cl;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='DSMT CSR']; 
        
       String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        
        User tuser = new User(  firstname = 'tuserFname',
                            lastName = 'tuserLastname',
                            email = 'tuser@test.org',
                            
                            profileId = p.id,
                            Username = hexDigest+'@test.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias ='tuser',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US'
                            );
        insert tuser;
    }
    
    testmethod static void CreateCustomerInteractionAndActivityOnCallItem(){
        Customer__c cr = new Customer__c();
        cr.Name='test';
        insert cr;
        
        Call_List_Line_Item__c cli = new Call_List_Line_Item__c();
        cli.Status__c = 'Transferred';
        insert cli;
        
        cli.Status__c = 'Completed';
        cli.Customer__c = cr.id;
        cli.Call_Notes__c ='test CLI';
        update cli;
      }
    
    testmethod static void dsmtAIETAssignmentTrigger(){
        Schedules__c sc = new Schedules__c(Name='test',Start_Time__c='08am',End_Time__c='06pm');
        insert sc;
        
        Location__c loc = new Location__c(Name='test');
        insert loc;
        
        Employee__c ec = new Employee__c(Schedule__c=sc.Id, Location__c=loc.Id,Employee_Id__c='EMP0010',Active__c=true,Status__c = 'Active');
        insert ec;
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c();
        sli.Schedule_Week__c='WEEK A';
        insert sli;
        
        Work_Team__c wt = new Work_Team__c(Captain__c=ec.Id,
                                           Schedule_Line_Item__c=sli.Id
                                          );
        insert wt;
        
        Workorder__c wo = new Workorder__c(work_team__c=wt.Id,Status__c='Unscheduled');
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();
        insert wo;
        
       Appointment__c ap = new Appointment__c();
       ap.Employee__c = ec.id; 
       ap.Appointment_Status__c = 'Eligible';  
       ap.Work_Team__c = wt.id;  
       insert ap;
        
        Employee_Territory_Assignment__c eta = new Employee_Territory_Assignment__c();
        eta.Assignment_Type__c = 'Recurring';
        eta.Assignment_Start_Date__c =date.today();
        eta.Assignment_End_Date__c = date.today()+2;
        eta.Schedule_Line_Item__c=sli.id;
        insert eta;
        
        Employee_Territory_Assignment__c eta1 = new Employee_Territory_Assignment__c();
        eta1.Assignment_Type__c = 'One Time';
        eta1.Assignment_Start_Date__c =date.today();
        eta1.Assignment_End_Date__c = date.today()+2;
        eta1.Employee__c = ec.Id;        
        insert eta1; 
        
        Employee_Territory_Assignment__c eta2 = new Employee_Territory_Assignment__c();
        eta2.Assignment_Type__c = 'One Time';
        eta2.Assignment_Start_Date__c =date.today();
        eta2.Assignment_End_Date__c = date.today()+2;
        eta2.Employee__c = ec.Id;
        eta2.Schedule_Line_Item__c=sli.id;
        insert eta2;
        
    }
    testmethod static void dsmtAUScheduleLineItemTrigger(){
        
        Location__c loc = new Location__c(Name='test');
        insert loc;
        
        Employee__c ec = new Employee__c(Location__c=loc.Id,Employee_Id__c='EMP0010',Active__c=true,Status__c = 'Active');
        insert ec;
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c();
        sli.Schedule_Week__c='WEEK A';
        sli.Unavailable__c=false;
        insert sli;
        
        Work_Team__c wt = new Work_Team__c(Captain__c=ec.Id,
                                           Schedule_Line_Item__c=sli.Id
                                          );
        insert wt;
        
        sli.Unavailable__c=true;
        update sli;
                
        Appointment__c app = new Appointment__c();
        app.Appointment_Type__c = 'WiFi';
        app.Employee__c = ec.id;
        app.Appointment_Status__c = 'Eligible';
        insert app;
        
        app.Appointment_Status__c = 'Cancelled';
        update app;
    }
    testmethod static void dsmtBIBUCEPopulateBUTrigger(){
        
        Customer__c cr = new Customer__c();
        cr.Name='testasdfghjklqw';
        cr.First_Name__c = 'test FN';
        insert cr;
        
        Customer_Eligibility__c ce = new Customer_Eligibility__c();
        ce.Attic__c='Yes';    
        ce.Unit__c='test';
        insert ce;
        
        ce.Unit__c='test update';
        ce.Primary_Provider__c='Gas';
        ce.Gas_Customer__c = cr.id;
        update ce;
        
        Customer_Eligibility__c ce1 = new Customer_Eligibility__c();
        ce1.Attic__c='Yes';    
        ce1.Unit__c='test';
        insert ce1;
        
        ce1.Unit__c='test update';
        ce1.Primary_Provider__c='Electric';
        ce1.Electric_Customer__c = cr.id;
        update ce1;
       }
       
       testmethod static void dsmtCreateAppointmentOnWTMTrigger(){
        
        Location__c loc = new Location__c(Name='test');
        insert loc;
        
        Employee__c ec = new Employee__c(Location__c=loc.Id,Employee_Id__c='EMP0010',Active__c=true,Status__c = 'Active');
        insert ec;
        
        Employee__c ec1 = new Employee__c(Location__c=loc.Id,Employee_Id__c='EMP0011',Active__c=true,Status__c = 'Active');
        insert ec1;
        
        Employee__c ec2 = new Employee__c(Location__c=loc.Id,Employee_Id__c='EMP0012',Active__c=true,Status__c = 'Active');
        insert ec2;
        
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c();
        sli.Schedule_Week__c='WEEK A';
        sli.Unavailable__c=false;
        insert sli;
        
        Work_Team__c wt = new Work_Team__c(Captain__c=ec.Id,
                                           Schedule_Line_Item__c=sli.Id
                                          );
        insert wt;
        
        Workorder__c wo = new Workorder__c();
        wo.Status__c = 'Scheduled';
        wo.Work_Team__c = wt.Id;
        insert wo;
        
        List<Work_Team_Member__c> lstWTM = new List<Work_Team_Member__c>();
        Work_Team_Member__c wtm = new Work_Team_Member__c(Employee__c=ec1.Id,Work_Team__c=wt.Id);
        
        lstWTM.add(wtm);
        
        Work_Team_Member__c wtm1 = new Work_Team_Member__c(Employee__c=ec2.Id,Work_Team__c=wt.Id);
        lstWTM.add(wtm1);
        
        insert lstWTM;
        
        wt.Start_Date__c = DateTime.now().addMinutes(10);
        update wt;
        
        
    }
    
    testmethod static void dsmtUpdateAppointmentEmpIdsTrigger()
    {
        DSMTracker_Contact__c c = Datagenerator.createDSMTracker();
        c.Active__c = true;
        upsert c;
        
        Location__c loc = Datagenerator.createLocation();
        insert loc;
        Employee__c emp = Datagenerator.createEmployee(Loc.Id);
        emp.DSMTracker_Contact__c = c.Id;
        insert emp;
        
        emp.DSMTracker_Contact__c = c.Id;
        upsert emp;
        
        Eligibility_Check__c el = Datagenerator.createELCheck();
        
        
        Appointment__c app = Datagenerator.createAppointment(el.Id);
        app.DSMTracker_Contact__c = c.Id;
        app.Employee__c = emp.Id;
        upsert app;        
        
        emp.Employee_Id__c = 'test123';
        update emp;
    }
}