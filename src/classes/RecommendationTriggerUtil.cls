public class RecommendationTriggerUtil {
    public static List<Enrollment_Application__c> fillEnrollmentApplication(RecommendationTriggerHandler.RModel model){
        List<Enrollment_Application__c> retLst = new List<Enrollment_Application__c>();
        List<Recommendation__c> lstRS = model.lstRs;
        Map<String, String> mapEAToWO = model.mapEAToWO;
        Map<String, String> mapEAToAppt = model.mapEAToAppt;
        Map<String, Appointment__c> mapWOToApptObj = model.mapWOToApptObj;
        Map<Id, Appointment__c> mapApptIdAppt = model.mapApptIdAppt;
        
        for(Recommendation__c rs: lstRS){
            String energyAssesmentId = rs.Energy_Assessment__c;
            Appointment__c appt = null;
            if(mapEAToAppt.containsKey(energyAssesmentId)){
                String apptId = mapEAToAppt.get(energyAssesmentId);
                if(mapApptIdAppt.containsKey(apptId)){
                    appt = mapApptIdAppt.get(apptId);
                }
            }else if(mapEAToWO.containsKey(energyAssesmentId)){
                String woId = mapEAToWO.get(energyAssesmentId);
                if(mapWOToApptObj.containsKey(woId)){
                    appt = mapWOToApptObj.get(woId);
                }
            }
            if(appt!=null){
                String custFirstname = appt.Customer_Reference__r.First_Name__c;
                String custLastName = appt.Customer_Reference__r.Last_Name__c;
                String custPhone = appt.Customer_Reference__r.Phone__c;
                String custEmail = appt.Customer_Reference__r.Email__c;
                
                String custServiceAddress = appt.Customer_Reference__r.Service_Address__c; 
                String custServiceState = appt.Customer_Reference__r.Service_State__c;
                String custServiceCity = appt.Customer_Reference__r.Service_City__c;
                String custServiceZipcode = appt.Customer_Reference__r.Service_Zipcode__c;
                String custElecProviderName = appt.Customer_Reference__r.Electric_Provider_Name__c;
                String custGasProviderName = appt.Customer_Reference__r.Gas_Provider_Name__c;
                String custGasAccNo = appt.Customer_Reference__r.Gas_Account_Number__c;
                String custElecAccNo = appt.Customer_Reference__r.Electric_Account_Number__c;
                
                
                String taFirstName=appt.Trade_Ally_Account__r.First_Name__c;
                String taLastName=appt.Trade_Ally_Account__r.Last_Name__c;
                String taPhone = appt.Trade_Ally_Account__r.Phone__c;
                String taEmail = appt.Trade_Ally_Account__r.Email__c;
                
                String taStreetAddress = appt.Trade_Ally_Account__r.Street_Address__c; 
                String taStreetState = appt.Trade_Ally_Account__r.Street_State__c;
                String taStreetCity = appt.Trade_Ally_Account__r.Street_City__c;
                String taStreetZipcode = appt.Trade_Ally_Account__r.Street_Zip__c;
                
                String taMailAddress = appt.Trade_Ally_Account__r.Mailing_Address__c; 
                String taMailState = appt.Trade_Ally_Account__r.Mailing_City__c;
                String taMailCity = appt.Trade_Ally_Account__r.Mailing_City__c;
                String taMailZipcode = appt.Trade_Ally_Account__r.Mailing_Zip__c; 
                
                Enrollment_Application__c ea = new Enrollment_Application__c();
                ea.Recommendation__c = rs.Id;
                ea.Recommendation_Scenario__c =rs.Recommendation_Scenario__c; 
                ea.Who_should_receive_incentive_payment__c = 'Trade Ally';
                ea.Status__c = 'Complete';
                ea.Stage__c='Complete';
                ea.Expected_Incentive__c =rs.ContractorIncentive__c;
                ea.Final_Customer_Incentive__c =rs.CustomerIncentive__c;
                /* From Customer */
                ea.Customer__c = appt.Customer_Reference__c;
                ea.Service_Address__c = custServiceAddress;
                ea.Service_State__c = custServiceState;
                ea.Service_City__c = custServiceCity;
                ea.Service_Zip_Code__c = custServiceZipcode;
                ea.Electric_account_number__c = custElecAccNo;
                ea.Gas_account_number__c = custGasAccNo;
                
                ea.Contact_First_Name__c = custFirstname;
                ea.Contact_Last_Name__c = custLastName;
                ea.Applicant_Email_Address__c = custEmail;
                ea.Applicant_Phone_Number__c =custPhone;
                ea.Contact_Address__c = custServiceAddress; 
                ea.Contact_State__c = custServiceState; 
                ea.Contact_City__c = custServiceCity; 
                ea.Contact_Zip_Code__c = custServiceZipcode; 
                
                /* From Customer DSMTContact */
                Map<String, DSMTracker_Contact__c> mapCustIdToDC = model.mapCustIdToDC;
                if(mapCustIdToDC.containsKey(appt.Customer_Reference__c)){
                    /*ea.Contact_First_Name__c = mapCustIdToDC.get(appt.Customer_Reference__c).First_Name__c;
                    ea.Contact_Last_Name__c = mapCustIdToDC.get(appt.Customer_Reference__c).Last_Name__c;
                    ea.Applicant_Email_Address__c = mapCustIdToDC.get(appt.Customer_Reference__c).Email__c;
                    ea.Applicant_Phone_Number__c = mapCustIdToDC.get(appt.Customer_Reference__c).Phone__c;*/
                    /*ea.Contact_Address__c = mapCustIdToDC.get(appt.Customer_Reference__c).Address__c; 
                    ea.Contact_State__c = mapCustIdToDC.get(appt.Customer_Reference__c).State__c; 
                    ea.Contact_City__c = mapCustIdToDC.get(appt.Customer_Reference__c).City__c; 
                    ea.Contact_Zip_Code__c = mapCustIdToDC.get(appt.Customer_Reference__c).Zip__c; */
                    ea.Customer_Contact_DSMT__c = mapCustIdToDC.get(appt.Customer_Reference__c).Id; 
                }
                
                /* From TradAlly Account */
                ea.Trade_Ally_Street_Address__c = taStreetAddress;
                ea.Trade_Ally_State__c = taStreetState;
                ea.Trade_Ally_City__c = taStreetCity;
                ea.Trade_Ally_Zip_Code__c = taStreetZipcode; 
                ea.Trade_Ally_Account__c = appt.Trade_Ally_Account__c;
                
                
                /* From TradAlly DSMTContact */
                Map<String, DSMTracker_Contact__c> mapTAIdToDC = model.mapTAIdToDC;
                if(mapTAIdToDC.containsKey(appt.Trade_Ally_Account__c)){
                    ea.Trade_Ally_Contact_First_Name__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).First_Name__c;
                    ea.Trade_Ally_Contact_Last_Name__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Last_Name__c;
                    ea.Trade_Ally_Contact_DSMT__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Id;
                    ea.Email_Address__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Email__c;
                    ea.Phone_Number__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Phone__c;
                    
                    ea.Mailing_Contact_First_Name__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).First_Name__c;
                    ea.Mailing_Contact_Last_Name__c =  mapTAIdToDC.get(appt.Trade_Ally_Account__c).Last_Name__c;
                    ea.Payment_Mailing_Email_Address__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Email__c;
                    ea.Payment_Mailing_Phone_Number__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Phone__c; 
                    
                    ea.Mailing_Address__c = '50 Washington St';
                    ea.Mailing_City__c = 'Westborough';
                    ea.Mailing_State__c = 'MA';
                    ea.Mailing_Zip_Code__c = '01581';
                    /*ea.Mailing_Address__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Address__c;
                    ea.Mailing_City__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).City__c;
                    ea.Mailing_State__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).State__c;
                    ea.Mailing_Zip_Code__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Zip__c; 
                    */
                    ea.Payee_Contact__c =  mapTAIdToDC.get(appt.Trade_Ally_Account__c).Id; 
                    if(mapTAIdToDC.get(appt.Trade_Ally_Account__c).Contact__c!=null && String.valueOf(mapTAIdToDC.get(appt.Trade_Ally_Account__c).Contact__c)!=''){
                        ea.Trade_Ally_Contact__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Contact__c;  
                    }
                    if(mapTAIdToDC.get(appt.Trade_Ally_Account__c).Account__c!=null && String.valueOf(mapTAIdToDC.get(appt.Trade_Ally_Account__c).Account__c)!=''){
                        ea.Trade_Ally_Business_Name__c = mapTAIdToDC.get(appt.Trade_Ally_Account__c).Account__c;  
                    } 
                }
                retLst.add(ea);    
            } 
        }
        return retLst;
    }
    
    public static List<Measure_Line_Item__c> fillMeasureLineItem(RecommendationTriggerHandler.RModel model){
        List<Measure_Line_Item__c> retLst = new List<Measure_Line_Item__c>();
        Map<String, Eligible_Measure__c> mapNameEM = new Map<String, Eligible_Measure__c>();
        for(Eligible_Measure__c em: model.mapIdEligMesu.values()){
            mapNameEM.put(em.Name,em);
        }
        Map<Id, Recommendation_Scenario__c> mapRSMap = model.mapRSMap;
        Map<Id,Recommendation__c> mapRMap = model.mapRMap;
        List<Enrollment_Application__c> lstEnrollmentApplication = model.lstEnrollmentApplication;
        for(Enrollment_Application__c ea: lstEnrollmentApplication){
            Measure_Line_Item__c mli = new Measure_Line_Item__c();
            mli.Enrollment_Application__c = ea.Id; 
            mli.Application_Type__c = 'Custom';
            mli.Stage__c = 'Completed';
            if(mapRMap.containsKey(ea.Recommendation__c)){     
                mli.Equipment_Cost__c = mapRMap.get(ea.Recommendation__c).Cost__c;
                mli.Quantity__c= mapRMap.get(ea.Recommendation__c).Quantity__c;
                mli.Make__c = mapRMap.get(ea.Recommendation__c).Recommended_Product__c; 
                mli.Model__c = mapRMap.get(ea.Recommendation__c).Recommended_Product__c; 
                mli.Insulation_location__c = mapRMap.get(ea.Recommendation__c).Location__c;
                mli.SIR__c = mapRMap.get(ea.Recommendation__c).Sir__c;
                String rsId = mapRMap.get(ea.Recommendation__c).Recommendation_Scenario__c!=null ? mapRMap.get(ea.Recommendation__c).Recommendation_Scenario__c : '';
                if(rsId!=''){
                    String rsName = mapRSMap.get(rsId).Name;
                    if(rsName!=null && rsName.contains('ISM')){
                        if(mapNameEM.containsKey('EM-0000')){
                            mli.Eligible_Measure__c=mapNameEM.get('EM-0000').Id;    
                            mli.Incentive_Amount_Trigger__c = mapNameEM.get('EM-0000').Incentive_per_unit__c;
                        } 
                    }else if(rsName!=null && rsName.contains('ASEAL')){
                        if(mapNameEM.containsKey('EM-0001')){
                            mli.Eligible_Measure__c=mapNameEM.get('EM-0001').Id;    
                            mli.Incentive_Amount_Trigger__c = mapNameEM.get('EM-0001').Incentive_per_unit__c;
                        }
                    }
                }
                
            } 
            retLst.add(mli);
        }
        return retLst;
    }
    
    public static List<Payment__c> fillPayments(RecommendationTriggerHandler.RModel model){ 
        List<Payment__c> lstRetPayment = new List<Payment__c>();
        for(Enrollment_Application__c ea : model.lstEnrollmentApplication){
            Payment__c pay = new Payment__c();
            pay.Paid_To__c = 'Customer';
            pay.Status__c = 'Approval Required';
            pay.Enrollment_Application__c = ea.id;
            pay.Requested_Date__c = Date.Today();
            lstRetPayment.add(pay);   
        }
        return lstRetPayment;
    } 
    
    
    public static List<Payment_Line_Item__c> fillPaymentLineItems(RecommendationTriggerHandler.RModel model){
        Map<String, Measure_Line_Item__c> mapEAIdToMLI = new Map<String, Measure_Line_Item__c>();
        for(Measure_Line_Item__c mli: model.lstMLI){
            mapEAIdToMLI.put(mli.Enrollment_Application__c, mli);
        }
            
        List<Payment_Line_Item__c> lstRetPLI = new List<Payment_Line_Item__c>();
        for(Payment__c pay: model.lstPayments){
            Payment_Line_Item__c pli = new Payment_Line_Item__c();
            pli.Enrollment_Application__c = pay.Enrollment_Application__c;
            pli.Measure_Line_Item__c = mapEAIdToMLI.get(pay.Enrollment_Application__c).Id;
            pli.Status__c = pay.Status__c;
            pli.Paid_To__c = pay.Paid_To__c;
            pli.payment__c = pay.id;
            //pli.Amount__c =  mapEAIdToMLI.get(pay.Enrollment_Application__c).Total_Incentive__c;
            lstRetPLI.add(pli);
        }
        return lstRetPLI;
    }
}