public  class dsmtAllAppointmentsCntrlHPC{

     public string apptJSON{get;set;}

     public Set<Id> dsmtcId {get;set;}
     public Dsmtracker_contact__c dsmtCon{get;set;}
     public Trade_Ally_Account__c ta{get;set;}
     public String portalRole{get;set;}
     public Appointment__c editApp{get;set;}
     public boolean overlap {get;set;}
     public boolean isIIC{get;set;}
     public String AppointmentId{get;set;}
     public String CancelNotes{get;set;}
     public List<SelectOption> dsmtContactOption{get;set;}
     public String CustId{get;set;}
     
     //Filters
     public string status {get;set;}
     public Appointment__c filterApp {get;set;}
     
     public String customerId;
     public string TaId;
     
     public boolean ISHPCScheudler{get;set;}
     
     public dsmtAllAppointmentsCntrlHPC(){
         
         ISHPCScheudler = false;
         dsmtcId = new Set<Id>();
         GetContactInfo();
        //portalRole = dsmtCon.Portal_Role__c;
         
         filterApp = new Appointment__c();
         filterApp.Appointment_Start_Time__c = System.now().addMonths(-3);
     }
     
     
       public void GetContactInfo(){
            
             dsmtContactOption = new List<SelectOption>(); 
             dsmtContactOption.add(new SelectOption('', '--Select Team Member--'));     
            
            String usrid = UserInfo.getUserId();
            List<User> usrList  = [select Id,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
            List < DSMTracker_Contact__c > dsmtList = [Select Id,Portal_User__r.Profile.Name, Name,Super_User__c,Portal_Role__c, Trade_Ally_Account__c,Email__c,Phone__c,Address__c,City__c,State__c,Zip__c,First_Name__c,Last_Name__c from DSMTracker_Contact__c 
                            where  Portal_User__c =: usrList.get(0).Id  and Trade_Ally_Account__c != null limit 1];
            if(dsmtList != null && dsmtList.size() > 0){
                dsmtCon = dsmtList.get(0);
                dsmtcId.add(dsmtCon.Id);
                TaID = dsmtList.get(0).Trade_Ally_Account__c;
                if(dsmtList.get(0).Portal_User__r.Profile.Name == 'HPC Energy Specialist'){
                    ISHPCScheudler = true;
                }
                
                List<Trade_Ally_Account__c> taList = [select id,name,Account__c,owner.Name,Owner.Phone,Owner.Email,Account_Manager__r.Name,Account_Manager__r.phone,Account_Manager__r.email,
                                                        Address__c,City__c,State__c,Zip_Code__c,Website__c,Phone__c,
                                                        Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,
                                                        Outreach_rep__r.Phone,Outreach_rep__r.Email,Outreach_rep__r.Name,(select id,name from DSMTracker_Contacts__r)
                                                        from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                if(taList != null && taList.size() > 0){
                    ta = taList.get(0);
                    for(Dsmtracker_Contact__c dsmt : ta.DSMTracker_Contacts__r){
                        if(dsmtList.get(0).Portal_User__r.Profile.Name != 'HPC Energy Specialist'){
                            dsmtcId.add(dsmt.Id);
                        }
                        dsmtContactOption.add(new SelectOption(dsmt.Id, dsmt.Name));
                    }
                }
            }else{
                dsmtCon = new Dsmtracker_Contact__c();
            }
                
            
     }
     
     
     public void initAllAppointments(){
            
        String filterquery =  ' Where Appointment_Start_Time__c != null and Appointment_End_Time__c != null and Customer_Reference__c != null and Appointment_Status__c not in (\'Pending\',\'Incomplete\',\'Ineligible\') and DSmtracker_Contact__c in: dsmtcId ';
        
        string firstNameF = ApexPages.currentPage().getParameters().get('firstNameF');
        string lastNameF = ApexPages.currentPage().getParameters().get('lastNameF');
        string addressF = ApexPages.currentPage().getParameters().get('addressF');
        string sdF = ApexPages.currentPage().getParameters().get('sdF');
        string edF = ApexPages.currentPage().getParameters().get('edF');
        
        if(firstNameF != null && firstNameF.trim().length() > 0){
            firstNameF = '%'+firstNameF+'%';
            filterquery += ' and Customer_Reference__r.First_Name__c like : firstNameF '; 
        }
        
        if(lastNameF != null && lastNameF.trim().length() > 0){
            lastNameF = '%'+lastNameF+'%';
            filterquery += ' and Customer_Reference__r.Last_Name__c like : lastNameF '; 
        }
        
        if(addressF != null && addressF.trim().length() > 0){
            addressF = '%'+addressF+'%';
            filterquery += ' and Customer_Reference__r.Service_Address__c like : addressF '; 
        }
        
        datetime dt, edt;
        if(sdF != null && sdF.trim().length() > 0){
            dt = datetime.parse(sdF);
            filterquery += ' and Appointment_Start_Time__c >=: dt '; 
        }
        
        if(edF != null && edF.trim().length() > 0){
            edt = datetime.parse(edF);
            filterquery += ' and Appointment_End_Time__c <=: edt '; 
        }
        
        if(filterquery.endswith('And')){
            filterquery = filterquery.substring(0,filterquery.length() - 3);
        }
        
        system.debug('filterquery :::::'+ filterquery);
        
        String query = '';
        
        //query = 'select id,DSMTracker_Contact__r.Name, Name,Customer_Reference__c,Customer_Reference__r.Name,Appointment_Status__c ,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,Employee__r.Name,CreatedDate from Appointment__c';
        query += 'Select id,Name,Phone__c,Email__c,Appointment_Status__c,COSP_status__c,COSP_Inactivated__c,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,CreatedDate,';
        query += 'DSMTracker_Contact__r.Name,Customer_Reference__c,Customer_Reference__r.Name,Customer_Reference__r.First_Name__c,Customer_Reference__r.Last_Name__c,Customer_Reference__r.Phone__c,Customer_Reference__r.Email__c,Customer_Reference__r.Service_Address__c,Customer_Reference__r.Service_City__c,Customer_Reference__r.Service_State__c,Customer_Reference__r.Service_Zipcode__c,';
        query += 'Customer_Reference__r.Electric_Provider_Name__c,Customer_Reference__r.Electric_Account_Number__c,Customer_Reference__r.Gas_Provider_Name__c,Customer_Reference__r.Gas_Account_Number__c,(select id,name from Energy_Assessment__r)'; 
        query += ' from Appointment__c ';
        
        query += filterquery ;
    
        query += ' order by Appointment_Start_Time__c desc limit 10000';
        
        if(test.isrunningTest()){
            query = 'Select id,Name,Phone__c,Email__c,Appointment_Status__c,COSP_status__c,COSP_Inactivated__c,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,CreatedDate,';
            query += 'DSMTracker_Contact__r.Name,Customer_Reference__c,Customer_Reference__r.Name,Customer_Reference__r.First_Name__c,Customer_Reference__r.Last_Name__c,Customer_Reference__r.Phone__c,Customer_Reference__r.Email__c,Customer_Reference__r.Service_Address__c,Customer_Reference__r.Service_City__c,Customer_Reference__r.Service_State__c,Customer_Reference__r.Service_Zipcode__c,';
            query += 'Customer_Reference__r.Electric_Provider_Name__c,Customer_Reference__r.Electric_Account_Number__c,Customer_Reference__r.Gas_Provider_Name__c,Customer_Reference__r.Gas_Account_Number__c,(select id,name from Energy_Assessment__r)'; 
            query += ' from Appointment__c ';
        }
        
        list<AppointmentModal> listAppoint = new List<AppointmentModal>();
        for(Appointment__c apt : Database.query(query)){
             
            
            AppointmentModal mod = new AppointmentModal();
            String EnergyAssesName = '';
            mod.Id = apt.Id;
            
            mod.cId = apt.Customer_Reference__c;
            mod.cNm= apt.Customer_Reference__r.Name;
            mod.fNM = apt.Customer_Reference__r.First_Name__c;
            mod.lNm = apt.Customer_Reference__r.Last_Name__c;
            mod.Pn = apt.Phone__c;
            mod.Em = apt.Email__c;
            mod.SS= apt.Customer_Reference__r.Service_Address__c;
            mod.SC = apt.Customer_Reference__r.Service_City__c;
            mod.SSt = apt.Customer_Reference__r.Service_State__c;
            mod.SZ = apt.Customer_Reference__r.Service_Zipcode__c;
            mod.EP = apt.Customer_Reference__r.Electric_Provider_Name__c;
            mod.EA = apt.Customer_Reference__r.Electric_Account_Number__c;
            mod.GP = apt.Customer_Reference__r.Gas_Provider_Name__c;
            mod.GA = apt.Customer_Reference__r.Gas_Account_Number__c;
            if(apt.COSP_Inactivated__c){
                mod.CI ='true';
            }else{
                mod.CI ='false';
            }
            
            List<Energy_Assessment__c> EnAsesList = apt.Energy_Assessment__r;
            if(EnAsesList.size()>0){
                
                for(Energy_Assessment__c EA : EnAsesList){
                    EnergyAssesName += '<a href="/'+EA.id+'" target="_blank">'+EA.name+'</a>'+' , '; 
                    //EnergyAssesName += EA.id+'@@@'+EA.name+','; 
                }
                if(EnergyAssesName.endsWith(' , ')){
                    EnergyAssesName = EnergyAssesName.substring(0,EnergyAssesName.length() - 3);
                } 
            }   
            
            mod.EnA = EnergyAssesName;
            mod.AT = apt.Appointment_Type__c;
            mod.ST =  apt.Appointment_Start_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
            mod.ET = apt.Appointment_End_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
            mod.Emp = apt.DSMTracker_Contact__r.Name;
            mod.Sts = apt.COSP_status__c;
            mod.AN = apt.Name;
            mod.CD = apt.CreatedDate.format('MM/dd/yyyy');
            if(apt.Appointment_End_Time__c != null && apt.Appointment_End_Time__c.date() < date.Today()){
                mod.hideResbutton = 'true';
            }else{
                mod.hideResbutton = 'false';
            }
            listAppoint.add(mod);
        }
        
        List<User> profileLst = [select id,profile.Name from User where id =: userinfo.getUSerId()];
            
        if(profileLst != null && (profileLst.get(0).profile.Name.Contains('IIC') || profileLst.get(0).profile.Name == 'HPC Office')){
            query = '';
            String workStopStatus = 'Work Stoppage';
            isIIC = true;
            //query = 'select id,DSMTracker_Contact__r.Name, Name,Customer_Reference__c,Customer_Reference__r.Name,Appointment_Status__c ,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,Employee__r.Name,CreatedDate from Appointment__c';
            query += 'Select id,Status__c,Phone__c,Name,Email__c,Pre_Work_Review__r.Energy_Assessment__c,Pre_Work_Review__r.Energy_Assessment__r.Name,Schedule_Date__c,Type__c,CreatedDate,';
            query += 'Pre_Work_Review__r.Energy_Assessment__r.Customer__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Name,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.First_Name__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Last_Name__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Phone__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Email__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_Address__c';
            query += ',Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_City__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_State__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_Zipcode__c,';
            query += 'Schedule_Duration__c,Schedule_Window__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Electric_Provider_Name__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Electric_Account_Number__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Gas_Provider_Name__c,Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Gas_Account_Number__c'; 
            
             // DSST-15489 - PP - START
            if(isIIC){
                query += ' from Review__c where Trade_Ally_Account__c =: TaID and status__c != \'Unassigned\' and status__c != \'Rejected and Reassigned\' and ((status__c !=: workStopStatus and status__c != \'Work Scope Approved\' and status__c != \'Complete Work Order\') OR (status__c = \'Work Scope Approved\' And Type__c != \'Pre Work Review\') OR (status__c = \'Complete Work Order\' And Type__c != \'Work Scope Review\') ) limit 10000';
            }else{
                query += ' from Review__c where Trade_Ally_Account__c =: TaID and ((status__c !=: workStopStatus and status__c != \'Work Scope Approved\' and status__c != \'Complete Work Order\') OR (status__c = \'Work Scope Approved\' And Type__c != \'Pre Work Review\') OR (status__c = \'Complete Work Order\' And Type__c != \'Work Scope Review\') ) limit 10000';
            }
             // DSST-15489 - PP - END
            
            system.debug('--query ---'+query);
            
            for(Review__c apt : database.query(query)){
                if(apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__c != null){
                AppointmentModal mod = new AppointmentModal();
                String EnergyAssesName = '';
                mod.Id = apt.Id;
                
                mod.cId = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__c;
                mod.cNm = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Name;
                mod.Action = apt.Name;
                mod.fNM = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.First_Name__c;
                mod.lNm = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Last_Name__c;
                mod.Pn = apt.Phone__c;
                mod.Em = apt.Email__c;
                mod.SS= apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_Address__c;
                mod.SC = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_City__c;
                mod.SSt = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_State__c;
                mod.SZ = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Service_Zipcode__c;
                mod.EP = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Electric_Provider_Name__c;
                mod.EA = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Electric_Account_Number__c;
                mod.GP = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Gas_Provider_Name__c;
                mod.GA = apt.Pre_Work_Review__r.Energy_Assessment__r.Customer__r.Gas_Account_Number__c;
                
                mod.CI ='false';
                
                EnergyAssesName += '<a href="/'+apt.Pre_Work_Review__r.Energy_Assessment__c+'" target="_blank">'+apt.Pre_Work_Review__r.Energy_Assessment__r.name+'</a>'+' , '; 
                        //EnergyAssesName += EA.id+'@@@'+EA.name+','; 
                    
                if(EnergyAssesName.endsWith(' , ')){
                    EnergyAssesName = EnergyAssesName.substring(0,EnergyAssesName.length() - 3);
                } 
                
                if(profileLst.get(0).profile.Name == 'HPC Office')
                    mod.EnA = '<a href="/'+apt.Id+'" target="_blank">'+apt.Name+'</a>';
                else
                    mod.EnA = EnergyAssesName;
                
                mod.AT = apt.Type__c;
               // mod.ST =  apt.Appointment_Start_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
               // mod.ET = apt.Appointment_End_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
               // mod.Emp = apt.DSMTracker_Contact__r.Name;
               if(apt.Schedule_Date__c != null){
                mod.ScheduleDate = apt.Schedule_Date__c.Format();
                }
                mod.ScheduleDuration = apt.Schedule_Duration__c;
                mod.ScheduleWindow = apt.Schedule_Window__c;
                mod.Sts = apt.status__c;
                mod.AN = apt.Name;
                mod.CD = apt.CreatedDate.format('MM/dd/yyyy');
                
                 if(apt.Schedule_Date__c != null && apt.Schedule_Date__c < date.Today()){
                        mod.hideResbutton = 'true';
                    }else{
                        mod.hideResbutton = 'false';
                    }
                    
                listAppoint.add(mod);
                }
            }
        
        }
            
        system.debug('--listAppoint ---'+listAppoint );
        apptJSON = JSON.serialize(listAppoint );

    }
    
    @RemoteAction
    public static list<AppointmentModal> queryAppList(string dsmtIds)
    {
        list<string> dsmtcId = new list<string>();
        dsmtcId = dsmtIds.split('~~~');
        
        String filterquery =  ' Where Appointment_Start_Time__c != null and Appointment_End_Time__c != null And DSMTracker_Contact__c in : dsmtcId and Appointment_Status__c not in (\'Pending\',\'Incomplete\',\'Ineligible\')';
        
        string firstNameF = ApexPages.currentPage().getParameters().get('firstNameF');
        string lastNameF = ApexPages.currentPage().getParameters().get('lastNameF');
        string addressF = ApexPages.currentPage().getParameters().get('addressF');
        string sdF = ApexPages.currentPage().getParameters().get('sdF');
        string edF = ApexPages.currentPage().getParameters().get('edF');
        
        if(firstNameF != null && firstNameF.trim().length() > 0)
            filterquery += ' and Customer_Reference__r.First_Name__c =: firstNameF '; 
        
        if(lastNameF != null && lastNameF.trim().length() > 0)
            filterquery += ' and Customer_Reference__r.Last_Name__c =: lastNameF '; 
        
        if(addressF != null && addressF.trim().length() > 0)
            filterquery += ' and Customer_Reference__r.Service_Address__c =: addressF '; 
        
        /*if(firstNameF != null && firstNameF.trim().length() > 0)
            filterquery += ' and Customer_Reference__r.First_Name__c =: firstNameF '; 
        
        if(firstNameF != null && firstNameF.trim().length() > 0)
            filterquery += ' and Customer_Reference__r.First_Name__c =: firstNameF '; */
        
        if(filterquery.endswith('And')){
            filterquery = filterquery.substring(0,filterquery.length() - 3);
        }
        
        system.debug('firstNameF :::::'+ firstNameF);
        
        String query = '';
        
        //query = 'select id,DSMTracker_Contact__r.Name, Name,Customer_Reference__c,Customer_Reference__r.Name,Appointment_Status__c ,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,Employee__r.Name,CreatedDate from Appointment__c';
        query += 'Select id,Name,Phone__c,Email__c,Appointment_Status__c,COSP_status__c,COSP_Inactivated__c,Appointment_Start_Time__c,Appointment_End_Time__c,Appointment_Type__c,CreatedDate,';
        query += 'DSMTracker_Contact__r.Name,Customer_Reference__c,Customer_Reference__r.Name,Customer_Reference__r.First_Name__c,Customer_Reference__r.Last_Name__c,Customer_Reference__r.Phone__c,Customer_Reference__r.Email__c,Customer_Reference__r.Service_Address__c,Customer_Reference__r.Service_City__c,Customer_Reference__r.Service_State__c,Customer_Reference__r.Service_Zipcode__c,';
        query += 'Customer_Reference__r.Electric_Provider_Name__c,Customer_Reference__r.Electric_Account_Number__c,Customer_Reference__r.Gas_Provider_Name__c,Customer_Reference__r.Gas_Account_Number__c'; 
        query += ' from Appointment__c ';
        
        query += filterquery ;
    
        query += ' order by Appointment_Start_Time__c desc limit 25000';
        system.debug('==dsmtcid========'+dsmtcid);
        system.debug('=======query======='+query);
        list<AppointmentModal> listAppoint = new List<AppointmentModal>();
        for(Appointment__c apt : Database.query(query)){
             
            
            AppointmentModal mod = new AppointmentModal();
                
            mod.Id = apt.Id;
            
            mod.cId = apt.Customer_Reference__c;
            mod.cNm= apt.Customer_Reference__r.Name;
            mod.fNM = apt.Customer_Reference__r.First_Name__c;
            mod.lNm = apt.Customer_Reference__r.Last_Name__c;
            mod.Pn = apt.Phone__c;
            mod.Em = apt.Email__c;
            mod.SS= apt.Customer_Reference__r.Service_Address__c;
            mod.SC = apt.Customer_Reference__r.Service_City__c;
            mod.SSt = apt.Customer_Reference__r.Service_State__c;
            mod.SZ = apt.Customer_Reference__r.Service_Zipcode__c;
            mod.EP = apt.Customer_Reference__r.Electric_Provider_Name__c;
            mod.EA = apt.Customer_Reference__r.Electric_Account_Number__c;
            mod.GP = apt.Customer_Reference__r.Gas_Provider_Name__c;
            mod.GA = apt.Customer_Reference__r.Gas_Account_Number__c;
            if(apt.COSP_Inactivated__c){
                mod.CI ='true';
            }else{
                mod.CI ='false';
            }
            
            mod.AT = apt.Appointment_Type__c;
            mod.ST =  apt.Appointment_Start_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
            mod.ET = apt.Appointment_End_Time__c.format('MM/dd/yyyy HH:mm:ss', 'America/New_York');
            mod.Emp = apt.DSMTracker_Contact__r.Name;
            mod.Sts = apt.COSP_status__c;
            mod.AN = apt.Name;
            mod.CD = apt.CreatedDate.format('MM/dd/yyyy');
            
             if(apt.Appointment_End_Time__c.date() < date.Today()){
                mod.hideResbutton = 'true';
            }else{
                mod.hideResbutton = 'false';
            }
            
            listAppoint.add(mod);
        }
        
        return listAppoint;
    }
    
    public class AppointmentModal{
        public string Id{get;set;}
        
        public String cId{get;set;}
        public string cNm{get;set;}
        public String fNm{get;set;}
        public String lNm{get;set;}
        public String Pn{get;set;}
        public String Em{get;set;}
        public String SS{get;set;}
        public String SC{get;set;}
        public String SSt{get;set;}
        public String SZ{get;set;}
        public String EP{get;set;}
        public String EA{get;set;}
        public String GP{get;set;}
        public String GA{get;set;}
        
        public string AT{get;set;}
        public String AN{get;set;}
        public String ST{get;set;}
        public String ET{get;set;}
        public string Emp{get;set;}
        public string Sts{get;set;}
        public String CD{get;set;}
        public string CI{get;set;}
        public String EnA{get;set;}
        
        public String ScheduleDate{get;set;}
        public string ScheduleDuration{get;set;}
        public string ScheduleWindow{get;set;}
        public string hideResbutton{get;set;}
        public String Action {get;set;}

    }
    
    public void updateAppoinment(){
        Date dt = editApp.Appointment_Start_Time__c.Date();
        List<Appointment__c> appList = [select id,Appointment_Start_Time__c,Appointment_End_Time__c 
                                        from appointment__c 
                                        where Appointment_Start_Date__c =: dt 
                                        and dsmTracker_Contact__c =: editApp.DSMTracker_Contact__c and Id !=: editApp.id];
        
        overlap = false;
        
        for(Appointment__c app : appList){
            
            if(editApp.Appointment_Start_Time__c >= app.Appointment_Start_Time__c &&
                editApp.Appointment_Start_Time__c <= app.Appointment_End_Time__c ){
                    overlap = true;
            }
            
            if(editApp.Appointment_End_Time__c >= app.Appointment_Start_Time__c &&
                editApp.Appointment_End_Time__c <= app.Appointment_End_Time__c ){
                    overlap = true;
            }
            if(editApp.Appointment_Start_Time__c <= app.Appointment_Start_Time__c &&
                editApp.Appointment_End_Time__c  >= app.Appointment_End_Time__c){
                
                overlap = true;    
            } 
        }
        
        if(!overlap){
            editApp.Appointment_Status__c = 'Rescheduled';
            editApp.Notes__c = editApp.Description__c;
            if(editApp.workorder__c  != null){
                List<workorder__c> woList = [select id,notes__c,Status__c  from workorder__c where id =: editApp.workorder__c];
                
                if(woList != null && woList.size() > 0){
                    woList.get(0).Notes__c = editApp.notes__c;
                    woList.get(0).Status__c = 'Rescheduled';
                    update woList;
                }
            }
            update editApp;
            editApp = new Appointment__c();
            
        }else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.AppointmentOverlap));
        }
    }
    
    public Integer minutesToAdd{get;set;}
    public void queryAppointment(){
        Integer duration = 0;
        editApp = new Appointment__c();
        list<Appointment__c> appList = [select 
                                   id, 
                                   Name,
                                   Appointment_Status__c ,
                                   Description__c,
                                   DSMTracker_Contact__c,
                                   Appointment_Start_Time__c,
                                   Appointment_End_Time__c,
                                   Workorder__r.Workorder_Type__r.Est_Total_Time__c,
                                   workorder__C
                               from 
                                   Appointment__c
                                   where id =: AppointmentId];
                                   
        if(appList.size() > 0){
            editApp = appList[0];
            if(editApp.Workorder__r.Workorder_Type__r.Est_Total_Time__c != null)
                minutesToAdd = integer.valueOf(editApp.Workorder__r.Workorder_Type__r.Est_Total_Time__c);
            else
                minutesToAdd = 120;
        }
    }
    
    public void CancenAppointment(){
         //AppointmentId
        List<Appointment__c> appList  = [select Id,Cancel_Notes__c,Appointment_Status__c,Workorder__c from Appointment__c where id =: AppointmentId];
        
        if(appList != null && appList.size() > 0){
            if(appList.get(0).Cancel_Notes__c == null){
                appList.get(0).Cancel_Notes__c = '';
            }
            appList.get(0).Cancel_Notes__c = CancelNotes;
            appList.get(0).Appointment_Status__c = 'Cancelled';
        }
        
        update appList;
        
        Workorder__c wo = new Workorder__c ();
        wo.Id = appList.get(0).workorder__c;
        wo.Status__c = 'Cancelled';
        update wo;
        
    }
    
    public String eligibleCheckId{get;set;}
    public void addNewAppointment(){
        system.debug('--custId --'+custId);
      
        String selects = '';
        
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get('Eligibility_Check__c').getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
        }   
        if(selects.trim().length() > 0){
            selects = ','+selects;
        }
        
        String query = 'select id '+selects+' from Eligibility_Check__c where Customer__c =: custId order by createddate desc limit 1';
        List<Eligibility_Check__c> eclist = database.query(query);
        Eligibility_Check__c EC = null;
        if(eclist.size() > 0){
           EC = eclist.get(0).clone();
           EC.Dsmtracker_Contact__c = dsmtCon.id;
        }else{
           List<Customer__c> custlist = [select id,First_Name__c,Last_Name__c,Phone__c,Email__c,Service_Address__c,Service_City__c,Service_Zipcode__c,Electric_Account_Number__c,Gas_Account_Number__c from Customer__c where id =: custid];
           if(custlist.size() > 0){
              customer__c cust = custlist.get(0);
              EC = new Eligibility_Check__c();
              EC.First_Name__c = cust.First_Name__c;
              EC.Last_Name__c = cust.Last_Name__c;
              EC.Phone__c = cust.Phone__c;
              EC.Email__c = cust.Email__c;
              EC.Zip__c = cust.Service_Zipcode__c;
              EC.customer__c = custId;  
              EC.Dsmtracker_Contact__c = dsmtCon.id;
              EC.Electric_Account__c = cust.Electric_Account_Number__c;
              EC.Gas_Account__c = cust.Gas_Account_Number__c; 
              EC.Service_Address__c = cust.Service_Address__c;
              EC.City__c = cust.Service_City__c;
              EC.Electric_Account_Holder_First_Name__c = cust.First_Name__c;
              EC.Electric_Account_Holder_Last_Name__c = cust.Last_Name__c;
              if(EC.Gas_Account__c != null){
                 EC.Gas_Account_Holder_First_Name__c = cust.First_Name__c;
                 EC.Gas_Account_Holder_Last_Name__c = cust.Last_Name__c;
              }
           }
           
        }
        
        if(EC != null){
           insert EC;
           eligibleCheckId = EC.id;
        }            
    }
}