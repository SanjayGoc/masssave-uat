@istest
public class AtticWallComponentCntrlTest
{
     @istest
    static void runTest()
    {
        Wall__c wl =new Wall__c();
        wl.Name='test';
        insert wl;
        
        Layer__c ly =new Layer__c();
        ly.Name='test';
        ly.Type__c='Siding';
        ly.HideLayer__c = false;
        ly.Wall__c=wl.id;
        insert ly;
        
        AtticWallComponentCntrl awcc = new AtticWallComponentCntrl ();
        String newLayer = awcc.newLayerType;
        awcc.atticWallId=wl.Id;
        awcc.getLayerTypeOptions();
        awcc.getWallLayersMap();
        awcc.saveWall();
        
      
        ApexPages.currentPage().getParameters().put('layerType','Flooring');
        awcc.addNewAtticWallLayer();
        
        ApexPages.currentPage().getParameters().put('index','0');
        awcc.deleteWallLayer();
        
    }
}