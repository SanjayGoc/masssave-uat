@isTest
Public class dsmtWorkOrderHelperControllerTest{
    @isTest
    public static void runTest(){
        
        Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.Createwo();
        wo.Location__c = locObj.Id;
        wo.Work_Team__c = wt.Id;
        wo.Early_Arrival_Time__c = DateTime.newInstance(2018, 1, 1, 10, 30, 0); 
        wo.Late_Arrival_Time__c = Datetime.newInstance(2018, 1, 1, 11, 0, 0);
        insert wo;
        
        dsmtWorkOrderHelperController.CancelWorkOrder(wo.Id);
        dsmtWorkOrderHelperController.updateWorkOrder(wo.Id,'Reschedulled');
        dsmtWorkOrderHelperController.OptimizeRoute(wo.Id,date.Today(),Date.Today());
        
        Workorder_Type__c wtype = new Workorder_Type__c();
        wtype.Friendly_Name__c = 'Test';
        insert wtype;
        
        wo.Workorder_Type__c = wtype.Id;
        update wo;
        
    }
    
    @isTest
    public static void runTest1(){
        
        Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.Createwo();
        wo.Location__c = locObj.Id;
        wo.Work_Team__c = wt.Id;
        wo.Early_Arrival_Time__c = DateTime.newInstance(2018, 1, 1, 10, 30, 0); 
        wo.Late_Arrival_Time__c = Datetime.newInstance(2018, 1, 1, 11, 0, 0);
        insert wo;
        
        dsmtWorkOrderHelperController.CancelWorkOrder(wo.Id);
        dsmtWorkOrderHelperController.updateWorkOrder(wo.Id,'Reschedulled');
        dsmtWorkOrderHelperController.OptimizeRoute(wo.Id,date.Today(),Date.Today());
        
        Workorder_Type__c wtype = new Workorder_Type__c();
        wtype.Friendly_Name__c = 'Test';
        insert wtype;
        
        wo.Scheduled_Start_Date__c  = datetime.now().Adddays(2);
        wo.Scheduled_End_Date__c  = datetime.now().Adddays(2).Addminutes(90);
        update wo;
       
        
    }
}