public class dsmtEmployeeRegionController {
    public list<Employee__c> allLocationEmps{get;set;}
    public Location__c location {get;set;}
    public list<SelectOption> empOptions {get;set;}
    public list<SelectOption> empDeleteOptions {get;set;}
    public list<Territory__c> territoryList {get;set;}
    
    public Region__c region {get;set;}
    
    public string empId {get;set;}
    public string empDeleteId {get;set;}
    public string coordinates {get;set;}
    public string shapeType {get;set;}

    private string regionId;
    
    public dsmtEmployeeRegionController (ApexPages.StandardController controller) {
        init();
    }

    public dsmtEmployeeRegionController ()
    {
        init();
    }
    
    private void init()
    {
        
        allLocationEmps = new list<Employee__c>();
        location        = new Location__c();
        empOptions      = new list<SelectOption>();
        empDeleteOptions= new list<SelectOption>();
        region          = new Region__c();
        territoryList   = new list<Territory__c>();
        
        regionId        = ApexPages.currentPage().getParameters().get('id');
        
        region          = QueryRegion(regionId);
        territoryList   = QueryTerritories();
        
        allLocationEmps = QueryAllEmployees(region.Location__c);
        allLocationEmps.sort();
        
        location        = FetchLocation(region.Location__c);
        
        set<string> empIdSet = new set<string>();
        for(Territory__c t : [select id, Employee__c from Territory__c where Employee__c in : allLocationEmps and Region__c =: regionId]){
            empIdSet.add(t.Employee__c);
        }
        
        empOptions.add(new SelectOption('', '--None--'));
        empDeleteOptions.add(new SelectOption('', '--None--'));
        for(Employee__c emp : allLocationEmps)
        {
            if(!empIdSet.contains(emp.Id))
                empOptions.add(new SelectOption(emp.Id, emp.Name));
            else
                empDeleteOptions.add(new SelectOption(emp.Id, emp.Name));
        }
    } 
    
    //
    private list<Territory__c> QueryTerritories(){
        return [select id, Name, Employee__c, ShapeType__c, Employee__r.Team_Color__c,
               (select id, Sequence__c, Coordinates__c, Coordinates__latitude__s, Coordinates__longitude__s
                from Territory_Details__r
                order by Sequence__c)
                from Territory__c 
                where Region__c =: regionId
                and ShapeType__c != null];
    }
    
    //Get current Location record
    private Location__c FetchLocation(string locationId) {
        return [select id, Name, Address__c, City__c, Country__c, 
                Zip__c from Location__c where id =: locationId
                order by name limit 1];
    }
    
    private static Region__c QueryRegion(string regionId)
    {
        return [select id, Name, Location__c from Region__c where id =: regionId limit 1];
    }
    
    private static list<Employee__c> QueryAllEmployees(string selectedLocation)
    {
        
        string employeeString = ' Select Id, Name, Team_Color__c, Active__c, Address__c, City__c, Date_of_Birth__c, Department_ID__c, Email__c, '+
                                ' Employee_Id__c, Home_Phone__c, Job_Code__c, Location__c, Mobile_Phone__c, Start_Date__c, '+
                                ' State__c, Status__c, Work_Teams__c, Zip__c'+
                                ' FROM Employee__c e '+
                                ' WHERE Location__c = \'' + selectedLocation +'\''+
                                ' AND Status__c != \'' + 'I' + '\'';
        
        return database.query(employeeString);
    }
    
    public void SaveTerritory()
    {
        Territory__c territory = new Territory__c(Employee__c = empId,
                                                  Region__c = regionId,
                                                  ShapeType__c = shapeType);
        
        insert territory;
        
        list<Territory_Detail__c> territoryDetailList = new list<Territory_Detail__c>();
        
        list<string> splitedCoordinates = coordinates.replace('(','').replace(')','').split('~~~');
        
        integer sequence = 0;
        for(string coordinate : splitedCoordinates)
        {
            sequence++;
            string cordinate = coordinate.replace(',','~~~');
            list<string> lanLongList = cordinate.split('~~~');
            
            Territory_Detail__c territoryDetail = new Territory_Detail__c(Coordinates__latitude__s = decimal.valueOf(lanLongList[0].trim()),
                                                                          Coordinates__longitude__s = decimal.valueOf(lanLongList[1].trim()),
                                                                          Territory__c = territory.Id,
                                                                          Sequence__c = sequence);
        
            territoryDetailList.add(territoryDetail);
        }
        
        if(territoryDetailList.size() > 0)
            insert territoryDetailList;
    }
    
    public void DeleteEmpTerritory()
    {
        list<Territory__c> territoryDeleteList = [select id from Territory__c where Employee__c =: empDeleteId];
        
        if(territoryDeleteList.size() > 0)
            delete territoryDeleteList;
    }
}