public class BatchAvailableTimeSlot implements Database.batchable<SObject>,Database.Stateful,schedulable,Database.AllowsCallouts{  
    
    public static Set<Id> setWTId = new Set<Id>();
    public database.querylocator start(Database.batchableContext batchableContext){
        String sQuery ='select id,Work_Team__c,Work_Team__r.Service_Date__c from Work_Team_Member__c where Work_Team__c != null '+(Test.isRunningTest()?' LIMIT 199':'');
        //System.debug('===sQuery===='+sQuery);
        return Database.getQueryLocator(sQuery);
        
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> scope){ 
        Set<Id> wtId = new Set<Id>();
        Set<id> wtmIds = new Set<Id>();
        Map<Id,Id> mapWorkTM = new Map<Id,Id>();
        Map<Id,Work_Team__c> mapWT = new Map<Id,Work_Team__c>();
        Map<Id,DateTime> mapavailableSlot = new Map<Id,DateTime>();
        List<Available_Slot__c> lstASlot = new List<Available_Slot__c>();
        Map<Id,Appointment__c> mapTravelTime = new Map<Id,Appointment__c>();
        
        Work_Team_Member__c wtm = null;
        
        for(sObject o : scope){
            wtm = (Work_Team_Member__c)o;
            wtmIds.add(wtm.Id);
            wtId.add(wtm.Work_Team__c);
            //setDate.add(wtm.Service_Date__c);
            mapWorkTM.put(wtm.Work_Team__c,wtm.Id);
            
        }
        List<Available_Slot__c> dltAVLst = [select Id from Available_Slot__c where Work_Team_Member__c In :wtmIds];
        Set<Id> setAVId = new Set<Id>();
        if(dltAVLst.size()>0){
            for(Available_Slot__c a: dltAVLst ){
                setAVId.add(a.Id);
            }
            List<Possible_Workorder_Types__c> dltPWLst = [select Id from Possible_Workorder_Types__c where Available_Slot__c In :setAVId];
            if(dltPWLst.size()>0){
                delete dltPWLst;
            }
            delete dltAVLst;
        }
        for(Work_Team__c w: [select Id,Start_Date__c,End_Date__c,Name from Work_Team__c where Id In :wtId and unavailable__c = false
                                and Service_Date__c >=: DAte.Today()]){
            mapWT.put(w.Id,w);
        }
        for(Appointment__c app: [select Id,Appointment_Start_Time__c,Workorder__c,Appointment_End_Time__c,Work_Team__c,Work_Team__r.Name,Workorder__r.Address_Formula__c,Work_Team__r.Captain_Address__c from Appointment__c where Work_Team__c In :mapWT.keyset() AND Appointment_Status__c='Scheduled' AND Work_Team__c!=null Order by Appointment_Start_Time__c ASC]){
            setWTId.add(app.Work_Team__c);
            if(mapavailableSlot.get(app.Work_Team__c)!=null){
                if(mapavailableSlot.get(app.Work_Team__c) < app.Appointment_Start_Time__c){
                    //decimal minutes = Decimal.valueof(EligibilityCheckUtility.dsmtGetDistance(mapTravelTime.get(app.Work_Team__c).Workorder__r.Address_Formula__c,app.Work_Team__r.Captain_Address__c ));
                    
                    Available_Slot__c objas = new Available_Slot__c();
                    objas.Work_Team_Member__c = mapWorkTM.get(app.Work_Team__c);
                    objas.Start_Time__c = mapavailableSlot.get(app.Work_Team__c);
                    objas.End_Time__c = app.Appointment_Start_Time__c;
                    objas.Duration__c = ((objas.End_Time__c.getTime())/1000/60 - (objas.Start_Time__c.getTime())/1000/60);
                    objas.Slot_Time_and_Duration__c = objas.Start_Time__c.format('dd-MM-YYYY hh:mm a')+' - '+objas.End_Time__c.format('hh:mm a')+' '+objas.Duration__c+' min';
                    objas.Work_Team_Member_Name__c = app.Work_Team__r.Name;
                    if(app.Workorder__r.Address_Formula__c!=null)
                        objas.Next_Address__c = app.Workorder__r.Address_Formula__c;
                        
                    if(mapTravelTime.get(app.Work_Team__c)!=null){
                        system.debug('@@mapTravelTime.get(app.Work_Team__c)@@'+mapTravelTime.get(app.Work_Team__c));
                        system.debug('@@mapTravelTime.get(app.Work_Team__c).Workorder__r@@'+mapTravelTime.get(app.Work_Team__c).Workorder__c);
                        system.debug('@@mapTravelTime.get(app.Work_Team__c).Workorder__r.Address_Formula__c@@'+mapTravelTime.get(app.Work_Team__c).Workorder__r.Address_Formula__c);
                        if(app.Work_Team__c!=null && mapTravelTime.get(app.Work_Team__c).Workorder__c !=null && mapTravelTime.get(app.Work_Team__c).Workorder__r.Address_Formula__c!=null)
                            objas.Current_Address__c= mapTravelTime.get(app.Work_Team__c).Workorder__r.Address_Formula__c;
                    }
                    
                    /*if(minutes!=null)
                        objas.Travel_Time_to_Next__c = minutes;*/
                        
                    mapTravelTime.put(app.Work_Team__c,app);
                    
                    lstASlot.add(objas);
                }
                
                if(mapWT.get(app.Work_Team__c).End_Date__c > app.Appointment_End_Time__c){
                    mapavailableSlot.put(app.Work_Team__c,app.Appointment_End_Time__c);
                    mapTravelTime.put(app.Work_Team__c,app);
                }
            }
            else{
                if(mapWT.get(app.Work_Team__c).Start_Date__c < app.Appointment_Start_Time__c ){
                    //decimal minutes = Decimal.valueof(EligibilityCheckUtility.dsmtGetDistance(app.Work_Team__r.Captain_Address__c,app.Workorder__r.Address_Formula__c));
                    
                    Available_Slot__c objas = new Available_Slot__c();
                    objas.Work_Team_Member__c = mapWorkTM.get(app.Work_Team__c);
                    objas.Start_Time__c = mapWT.get(app.Work_Team__c).Start_Date__c;
                    objas.End_Time__c = app.Appointment_Start_Time__c;
                    objas.Duration__c = ((objas.End_Time__c.getTime())/1000/60 - (objas.Start_Time__c.getTime())/1000/60);
                    objas.Slot_Time_and_Duration__c = objas.Start_Time__c.format('dd-MM-YYYY hh:mm a')+' - '+objas.End_Time__c.format('hh:mm a')+' '+objas.Duration__c+' min';
                    objas.Work_Team_Member_Name__c = app.Work_Team__r.Name;
                    if(app.Work_Team__r.Captain_Address__c!=null)
                        objas.Current_Address__c = app.Work_Team__r.Captain_Address__c;
                    if(app.Workorder__r.Address_Formula__c!=null)
                        objas.Next_Address__c= app.Workorder__r.Address_Formula__c;
                    /*if(minutes!=null)
                        objas.Travel_Time_to_Next__c = minutes;*/
                    
                    mapTravelTime.put(app.Work_Team__c,app);
                    
                    lstASlot.add(objas);
                    
                }
                
                if(mapWT.get(app.Work_Team__c).End_Date__c > app.Appointment_End_Time__c){
                    mapavailableSlot.put(app.Work_Team__c,app.Appointment_End_Time__c);
                    mapTravelTime.put(app.Work_Team__c,app);
                }
            }
        }
        
        for(Id key: mapavailableSlot.keyset()){
            if(mapWT.get(key).End_Date__c > mapavailableSlot.get(key)){
                    Available_Slot__c objas = new Available_Slot__c();
                    objas.Work_Team_Member__c = mapWorkTM.get(key);
                    objas.Start_Time__c = mapavailableSlot.get(key);
                    objas.End_Time__c = mapWT.get(key).End_Date__c;
                    objas.Duration__c = ((objas.End_Time__c.getTime())/1000/60 - (objas.Start_Time__c.getTime())/1000/60);
                    objas.Slot_Time_and_Duration__c = objas.Start_Time__c.format('dd-MM-YYYY hh:mm a')+' - '+objas.End_Time__c.format('hh:mm a')+' '+objas.Duration__c+' min';
                    objas.Work_Team_Member_Name__c = mapWT.get(key).Name;
                    
                    lstASlot.add(objas);
                    
            }
        }
        
        
        if(lstASlot.size()>0){
            system.debug('@@size of lstASlot@@'+lstASlot.size());
            //insert lstASlot;
            System.enqueueJob(new QueueClass1(lstASlot));
        }
        
    }
    
    public void finish(Database.BatchableContext batchableContext) {
        BatchAvailableTimeSlotWT btchWT = new BatchAvailableTimeSlotWT(setWTId);
        database.executebatch(btchWT ,200);
    }
    
    public void execute(SchedulableContext sc) {
        BatchAvailableTimeSlot obj = new BatchAvailableTimeSlot();
        database.executebatch(obj,200);
    }
}