@isTest
Public class dsmtRegistrationRequestHelperCntrlTest{
    @isTest
    public static void runTest(){
       
        User u = Datagenerator.CreatePortalUser();
        
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
        dsmt.Trade_Ally_Account__c =Tacc.id; 
        dsmt.Contact__c = u.contactid;
        dsmt.Title__c = 'COO';
        dsmt.Status__c = 'Approved';
        update dsmt;
        
        Registration_Request__c reg= Datagenerator.createRegistration();
        reg.Legal_First_Name__c = 'Hemanshu';
        reg.Legal_Last_Name__c = 'Patel';
        reg.Legal_Email__c='test@test.com';
        reg.Legal_Phone__c = '1234567890';
        reg.DSMTracker_Contact__c=dsmt.id; 
        reg.Trade_Ally_Account__c= Tacc.id;
        reg.Status__c = 'Submitted';
        update reg;
        
        
        Dsmtracker_Contact__c dsmt1 = dsmt.clone();
        dsmt1.Registration_request__c = reg.id;
        insert dsmt1;
        
        
        
        
        Email_Custom_Setting__c emailTempSet= new Email_Custom_Setting__c(name='test',RR_Submitted_Owner_Email_Template__c='Notification_To_Owner_for_Registration_Request_Submitted');
        insert emailTempSet;
       
        dsmtRegistrationRequestHelperCntrl controller = new dsmtRegistrationRequestHelperCntrl(reg);
        
        Set<Id> regRequestIds = new set<id>();
        regRequestIds.add(reg.id);
      
        dsmtRegistrationRequestHelperCntrl.sendEmail(regRequestIds);
        
        reg.Status__c = 'Approved';
        update reg;
        
        dsmtRegistrationRequestHelperCntrl.sendEmail(regRequestIds);
        
        reg.Status__c = 'Rejected';
        update reg;
        dsmtRegistrationRequestHelperCntrl.sendEmail(regRequestIds);
        
         
    }
      
}