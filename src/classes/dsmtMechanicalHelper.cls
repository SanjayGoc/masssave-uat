public class dsmtMechanicalHelper
{

    @TestVisible
    private static boolean hasActualLoadFraction( Mechanical_Sub_Type__c cs, dsmtEAModel.Surface bp )
    {
        boolean retval = cs.ActualLoadFraction__c !=null || bp.RecordTypeMap.get(cs.RecordTypeId) == 'Evaporative Cooler';

        return retval;
    }
      
    public static decimal scanIntDecimalDictionary( Map<integer, decimal> idd, integer ival )
    {
        decimal retval = null;

        ////System.debug((' idd ' + idd);
        ////System.debug((' ival ' + ival);
        
        Set<integer> yearSet = idd.keySet();

        //////System.debug(('yearSet ' + yearSet);
        
        for ( integer kvp : yearSet ) 
        {
            // initialize with first entry...
            if ( retval == null ) 
            {
                retval = idd.get(kvp);
            }
            
            // get last entry that's smaller or equal to the number looked for...
            if ( ival >= kvp ) 
            {
                retval = idd.get(kvp);
            }
        }

        return retval;
    }
    
    public static boolean HasDistributionSystem(dsmtEAModel.Surface bp)
    {
        boolean hasDS =false;
        
        List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c != bp.mstObj.Mechanical_Type__c)  continue;

                if(dsmtEAModel.GetDistributionSystemRecordTypes().contains(bp.RecordTypeMap.get(mst.RecordTypeId)))
                {
                    hasDS = true;
                    break;
                }
            }
        }
        
        return hasDS;
    }
    
    public static boolean HasDistributionSystemWithType(dsmtEAModel.Surface bp, string dsType)
    {
        boolean hasDS =false;

        if(bp==null) return hasDS;
        
        List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mechType.Id && 
                dsmtEAModel.GetDistributionSystemRecordTypes().contains(bp.RecordTypeMap.get(mst.RecordTypeId)))
                {
                    if(mst.DistributionSystemType__c == dsType)
                    {
                        hasDS = true;
                        break;
                    }
                }
            }
        }
        
        return hasDS;
    }
    
    @TestVisible
    private static boolean HasThermostate(dsmtEAModel.Surface bp)
    {
        boolean hasDS =false;
        
        List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mechType.Id && bp.RecordTypeMap.get(mst.RecordTypeId)=='Thermostat')
                {
                    hasDS = true;
                    break;
                }
            }
        }
        
        return hasDS;
    }

    @TestVisible
    private static boolean hasActualLoadFractionOrActualCapacity( dsmtEAModel.Surface bp )
    {
        boolean retval = hasActualLoadFraction( bp ) || hasActualCapacity( bp );
        return retval;
    }

    private static boolean hasActualCapacity( dsmtEAModel.Surface bp )
    {
        Mechanical_Sub_Type__c cs = bp.mstObj;
        
        boolean retval = cs.ActualCapacityInCapacityUnits__c!=null ||
                            cs.ActualSensibleCapacityInCapacityUnits__c !=null ||
                            cs.CoolingSystemType__c ==  'Window/Wall A/C' ||
                            (cs.CoolingSystemType__c == 'GSHP' && 
                            (cs.ActualGSHPCap32__c !=null || 
                             cs.ActualGSHPCap50__c!=null ||
                             cs.ActualGSHPCap59__c !=null ||
                             cs.ActualGSHPCap77__c !=null));

        return retval;
    }

    private static boolean hasActualLoadFraction( dsmtEAModel.Surface bp )
    {
        boolean retval = bp.mstObj.ActualLoadFraction__c !=null || bp.mstObj.CoolingSystemType__c == 'Evaporative Cooler';

        return retval;
    }
    
    public static void ResetActualsToNull(Mechanical_Sub_Type__c mst)
    {
        mst.ActualASHPCap47InCapacityUnits__c = null;
        mst.ActualBTUsPerHour__c = null;
        mst.ActualDFHPLoadFraction__c = null;
        mst.ActualDHWLoadFraction__c = null;
        mst.ActualDistributionFractionC__c = null;
        mst.ActualDistributionFractionH__c = null;
        mst.ActualEER__c = null;
        mst.ActualEquipEffInEffUnits__c = null;
        mst.ActualEquipmentEfficiency__c = null;
        mst.ActualEquipEffInEffUnits__c = null;
        mst.ActualHasRampRecovery__c = null;
        mst.ActualHeatinggSetBackHours__c = null;
        mst.ActualHeatingSetBackHours__c = null;
        mst.ActualHeatingSetBackTemp__c = null;
        mst.ActualHeatingSetTemp__c = null;
        mst.ActualInput__c = null;
        mst.ActualLoadFraction__c = null;
        mst.ActualPipeInsulationAmountValue__c = null;
        mst.ActualRecoveryEfficiency__c = null;
        mst.ActualReturnLeakage__c = null;
        mst.ActualSupplyLeakage__c = null;
        mst.ActualTankTemp__c = null;
        mst.ActualTotalLeakage__c = null;
        mst.ActualVoltage__c = null;
        mst.ActualVolume__c = null;
        mst.ActualYear__c = null;
        mst.ActualFuelType__c = null;
        mst.ActualFuelType__c = null;
        mst.ActualVolume__c = null;
        
        mst.ActualYear__c=null;
        mst.ActualVoltage__c=null;
        mst.ActualHeatingSetTemp__c =null;
        mst.ActualCoolingSetBackHours__c = null;
        mst.ActualCapacityInCapacityUnits__c =null;
        mst.ActualCoolingAirFlow__c =null;
        
        mst.ActualCoolingSetBackHours__c=null;
        mst.ActualCoolingSetBackTemp__c=null;
        mst.ActualCoolingSetTemp__c=null;
        mst.ActualEER__c= null;
        mst.ActualEquipEffInEffUnits__c=null;
        mst.ActualEquipmentEfficiency__c=null;
        
        mst.ActualGSHPCap32__c=null;
        mst.ActualGSHPCap50__c=null;
        mst.ActualGSHPCap59__c=null;
        mst.ActualGSHPCap77__c= null;
        mst.ActualGSHPEER59__c=null;
        mst.ActualGSHPEER77__c=null;
        
        mst.ActualHeatingSetBackHours__c=null;
        mst.ActualHeatingSetBackTemp__c=null;
        mst.ActualHeatingSetTemp__c=null;
        mst.ActualLoadFraction__c= null;
        mst.ActualNominalTons__c=null;
        mst.ActualNumTerminals__c=null;
        
        mst.ActualSEER__c=null;
        mst.ActualSensibleCapacityInCapacityUnits__c=null;
        mst.ActualSetBacksPerDay__c=null;
        mst.ActualEnergyFactor__c =null;
        
        mst.ActualHeatinggSetBackHours__c =null;
    }
    
    //////////////////////////////////////// DISTRIBUTION SYSTEM ///////////////////////////////////////////////////
    

    public static Mechanical_Sub_type__c GetMechanicalCoolingSystem(dsmtEAModel.Surface bp)
    {
        Mechanical_Sub_type__c result = null;
        
       List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if(dsmtEAmodel.GetCoolingSystem().contains(bp.RecordTypeMap.get(mst.RecordTypeId)))
                    {
                        result = mst;
                        break;
                    }
                }
            }
        }
        
        
        return result;
    }

    public static Mechanical_Sub_type__c GetMechanicalDHWSystem(dsmtEAModel.Surface bp)
    {
        Mechanical_Sub_type__c result = null;
        List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if(dsmtEAmodel.GetDHWSystem().contains(bp.RecordTypeMap.get(mst.RecordTypeId)))
                    {
                        result = mst;
                        break;
                    }
                }
            }
        }        
        
        return result;
    }

    public static Mechanical_Sub_type__c GetMechanicalThermostatSystem(dsmtEAModel.Surface bp)
    {
        Mechanical_Sub_type__c result = null;
        List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if(bp.RecordTypeMap.get(mst.RecordTypeId) == 'Thermostat')
                    {
                        result = mst;
                        break;
                    }
                }
            }
        }
        
        
        return result;
    }

    public static List<Mechanical_Sub_type__c> GetMechanicalThermostatSystemList(dsmtEAModel.Surface bp)
    {
        List<Mechanical_Sub_type__c> result = null;
        
        if(bp.mst !=null)
        {
            result = new List<Mechanical_Sub_type__c>();
            integer mstSize = bp.mst.size();

            For(integer i=0; i< mstSize; i++)            
            {
                Mechanical_Sub_Type__c mst = bp.mst[i];

                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if(bp.RecordTypeMap.get(mst.RecordTypeId) == 'Thermostat')
                    {
                        result.add(mst);
                        break;
                    }
                }
            }
        }        
        
        return result;
    }
    
    public static Mechanical_Sub_type__c GetMechanicalHeatingSystem(dsmtEAModel.Surface bp)
    {
        Mechanical_Sub_type__c result = null;
        
       List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                //////System.debug(('mst.Mechanical_Type__c>>>' + mst.Mechanical_Type__c);
                //////System.debug(('bp.mstObj.Mechanical_Type__c>>>' + bp.mstObj.Mechanical_Type__c);

                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if(dsmtEAModel.GetHeatingSystem().contains(bp.RecordTypeMap.get(mst.RecordTypeId)))
                    {
                        result = mst;
                        break;
                    }
                }
            }
        }        
        
        return result;
    }

    public static List<Mechanical_Sub_type__c> GetMechanicalDistributionSystem(dsmtEAModel.Surface bp)
    {
        List<Mechanical_Sub_type__c> result = new List<Mechanical_Sub_type__c>();
        
       List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if(dsmtEAModel.GetDistributionSystemRecordTypes().contains(bp.RecordTypeMap.get(mst.RecordTypeId)))
                    {
                        result.add(mst);
                        break;
                    }
                }
            }
        }
        
        
        return result;
    }

    public static decimal ComputeCoolingFanOnCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c ds = bp.mstObj;
        Mechanical_Sub_Type__c cs = GetMechanicalCoolingSystem(bp);
        
        if ( cs != null ) 
        {
            decimal coolingFanOnKWH = cs.CoolingFanOnKWH__c;
            if ( coolingFanOnKWH!=null ) 
            {
                retval = (decimal)coolingFanOnKWH * SurfaceConstants.CommonBtusByFuelType.get('Electricity').get('kWh') / 1000000;
            }
        }

        return retval;
    }

    public static string getEndUseType( dsmtEAModel.Surface bp )
    {
        string retval = null;
        
        boolean isHeatingSystem  =false;
        boolean isCoolingSystem =false;
        boolean isDistributionSystem =false;
        boolean isDHW=false;
        boolean isAppliance =false;
        boolean IsMechanicalVentilationSystem=false;            
        boolean isWaterFixture =false; 
        boolean IsDefaultLighting =false; 
        boolean IsLightBulbSet=false;
        
        if(bp.waterObj !=null)
        {
            isWaterFixture=true;
        }
        else if(bp.applObj !=null)
        {
            isAppliance=true;
        }
        else if(bp.genLigtObj !=null)
        {
            isDefaultLighting=true;
        }
        
        if(bp.lightLocObj !=null)
        {
            IsLightBulbSet=true;
        }

        if (isAppliance ) 
        {
            retval = bp.applObj.Type__c;
        }
        else if ( isWaterFixture ) 
        {
            retval = bp.RecordTypeMap.get(bp.waterObj.RecordTypeId);
        }
        else if ( isDefaultLighting || isLightBulbSet ) {
            retval = 'Lighting';
        }

        return retval;
    }  
     
    public static decimal splitIntensValue( dsmtEAModel.Surface bp, string ft, decimal inValue, string intens, boolean isWater)
    {
        decimal retval = null;
        string systemType = bp.BuildingProfileObject;

        boolean isHeatingSystem  =(systemType=='HeatingSystem');
        boolean isCoolingSystem =(systemType=='CoolingSystem');
        boolean isDistributionSystem =(systemType=='DistributionSystem');
        boolean isDHW=(systemType=='DHW');
        boolean isAppliance =(systemType=='Appliance');
        boolean IsMechanicalVentilationSystem=false;            
        boolean isWaterFixture =(systemType=='WaterFixture'); 
        boolean IsDefaultLighting =(systemType=='DefaultLighting'); 
        boolean IsLightBulbSet=(systemType=='LightBulbSet');

        if ( isHeatingSystem ) 
        {
            if ( intens == 'IntensH' ) 
            {
                retval = inValue;
            }
            else {
                retval = 0;
            }
        }
        else if ( isCoolingSystem ) 
        {
            if ( intens == 'IntensC' ) 
            {
                retval = inValue;
            }
            else {
                retval = 0;
            }
        }
        else if ( isDistributionSystem ) 
        {
            if ( intens == 'IntensH' ) 
            {
                retval = inValue;
            }
            else {
                retval = 0;
            }
        }
        else if ( isDHW ) 
        {
            // do a straight split...
            decimal fractionDays = dsmtTempratureHelper.getFractionDays( bp, intens );
            
            if ( fractionDays!=null && bp.temp.FractionDaysH!=null && bp.temp.FractionDaysC!=null && bp.temp.FractionDaysSh!=null ) 
            {
                retval = inValue * fractionDays / (bp.temp.FractionDaysH + bp.temp.FractionDaysC + bp.temp.FractionDaysSh);
            }
        }
        else if ( IsMechanicalVentilationSystem ) 
        {
            /*
            // use IntensH/C/Sh
            decimal fractionDays = dsmtSurfaceHelper.getFractionDays( bp, intens );
            MechanicalVentilationSystem mvs = bpo as MechanicalVentilationSystem;
            MechanicalVentilationSystemType mvst = mvs.MechanicalVentilationSystemType ?? MechanicalVentilationSystemType.Unknown;

            if ( fractionDays!=null ) {
                IDictionary<IntensGainUse, decimal> iguTable = Constants.MVIntensGainUseTable[mvst];

                decimal? dotIntens = computeDotIntens( iguTable, mi );
                decimal intensFactor = (getIntens( iguTable, intens ) ?? 1);
                if ( dotIntens!=null && dotIntens.Value != 0 ) {
                    retval = inValue.Value * fractionDays.Value * intensFactor / dotIntens.Value;
                }
                else {
                    retval = inValue.Value * fractionDays.Value;
                }
            }*/
        }
        else if ( isAppliance || isWaterFixture || IsDefaultLighting || IsLightBulbSet ) 
        {
            //Example: Dryer.MBtu * LU[ Dryer X GasIntensH ] * Bldg.PctH / LU[ Dryer X Rescale ]
            if ( inValue!=null ) 
            {
                string et = getEndUseType( bp );
                if ( et!=null ) 
                {
                    // first, apply seasonality
                    decimal fractionDays = dsmtTempratureHelper.getFractionDays( bp, intens );

                    // water is a gallons/day computation, so don't scale to the fraction of the year
                    if ( isWater ) fractionDays = 1;

                    if ( fractionDays!=null ) 
                    {
                        Map<string, decimal> iguTable = null;
                        if ( isWater ) 
                        {
                            if ( SurfaceConstants.CommonHWIntensGainUseTable.containsKey(et)) 
                            {
                                iguTable = SurfaceConstants.CommonHWIntensGainUseTable.get(et);
                            }
                        }
                        else 
                        {
                            if ( ft!=null && SurfaceConstants.CommonIntensGainUseTable.containsKey(ft) && SurfaceConstants.CommonIntensGainUseTable.get(ft).containsKey(et)) 
                            {
                                iguTable = SurfaceConstants.CommonIntensGainUseTable.get(ft).get(et);
                            }
                        }
                        decimal dotIntens = dsmtEAModel.ISNULL(computeDotIntens( iguTable, bp ) , 1);
                        decimal intensFactor = dsmtEAModel.ISNULL(getIntens( iguTable, intens ) , 1);

                        ////System.debug(( 'invalue ' + inValue + ' fractionDays ' + fractionDays + ' intensFactor ' + intensFactor + ' dotIntense ' + dotIntens);

                        if ( dotIntens != 0 ) 
                        {
                            retval = inValue * fractionDays * intensFactor / dotIntens;
                        }
                        else {
                            retval = inValue * fractionDays;
                        }

                        ////System.debug(('retval ' + dsmtEAModel.ISNULL(retVal).setScale(7, roundingMode.HALF_UP));
                    }
                    
                    // next, adjust distribution of load based on operating months
                    string opMonths = null;
                    integer numOpMonths = null;

                    if ( isAppliance ) 
                    {
                        Appliance__c app = bp.applObj;
                        opMonths = app.Months_of_Operation__c;
                        numOpMonths = (integer)app.ApplianceNumberOperatingMonths__c;
                    }
                    else if ( isWaterFixture ) 
                    {
                        opMonths = bp.waterObj.Months_Used__c;
                        if(opMonths !=null)
                            numOpMonths = bp.waterObj.Months_Used__c.split(';').size();
                    }
                    if ( numOpMonths!=null ) 
                    {
                        if ( opMonths != null && opMonths != 'None' && opMonths != '' ) 
                        {
                            // CSGENG-4159: We have a months bit-rack, apply it.
                            decimal monthsAdjust = getMonthsFactor( opMonths, intens, bp );
                            retval *= monthsAdjust;
                        }
                        else if ( numOpMonths != null ) 
                        {
                            // CSGENG-4159: We don't have a months bit-rack, scale by numOpMonths/12
                            retval = dsmtEAModel.ISNULL(retval) * (decimal)numOpMonths / 12;
                        }
                    }
                }
            }

            if (IsDefaultLighting || IsLightBulbSet ) 
            {
                if (bp.ws != null ) 
                {
                    decimal lightAdjust = getLightFactor( bp, intens );
                    ////System.debug((' lightAdjust ' + lightAdjust);
                    if ( lightAdjust!=null ) 
                    {
                        //WS.LightFactorH
                        decimal LightAdjustTotal = bp.temp.LightAdjustTotal;
                        retval = dsmtEAModel.ISNULL(retval) * lightAdjust / dsmtEAModel.ISNULL(LightAdjustTotal , 1);
                    }
                }
            }
            else if ( isAppliance ) 
            {
                Appliance__c app = bp.applObj;
                if(app.Type__c == 'Refrigeration')
                {
                    //1 + 0.025 * ( Refrig.RoomTempH - Refrig.RoomTemp )
                    decimal roomTempx = dsmtTempratureHelper.GetRefrigeratorRoomTemp( app, intens );
                    ////System.debug((' roomTempx ' + roomTempx );
                    
                    if ( app.Room_Temperature__c!=null && roomTempx!=null ) 
                    {
                         retval = dsmtEAModel.ISNULL(retval) * (1 + 0.025 * (roomTempx - app.Room_Temperature__c)) / dsmtEAModel.ISNULL(dsmtTempratureHelper.ComputeRefrigeratorTempAdjustTotal( bp ), 1);
                    }
                }
                else if(app.Type__c == 'TV' || app.Type__c == 'Game Console'
                || app.Type__c == 'Home Office')
                {
                    decimal lightAdjust = dsmtTempratureHelper.getLightFactor( bp, intens );
                    if ( lightAdjust!=null) 
                    {
                        //1 + ( LU[Lighting: Indoor X PlugIntensH] - 1 ) / 2
                        retval = (dsmtEAModel.ISNULL(retval) * (1 + (lightAdjust - 1) / 2)) / dsmtEAModel.ISNULL(bp.temp.CELightAdjustTotal,1);
                    }
                }
                
            }
        }

        return retval;
    }

    @TestVisible
    private static decimal getMonthsFactor( string months, string intens, dsmtEAModel.Surface bp )
    {
        decimal retval = 0;

        decimal numMonths = 0;
        decimal seasonMonths = 0;
        bp.Months = months;
        For ( string month : months.split(';')) 
        {
            if ( month == null || month == 'None' || month == '' ) continue;

            // add up total months for this season for this appliance
            numMonths += dsmtBuildingModelHelper.ComputeMonthsSeasonFractionCommon(bp).get(month).get(intens);              

            // add up total months for this season
            seasonMonths += dsmtBuildingModelHelper.ComputeMonthsSeasonFractionCommon(bp).get(month).get(intens);
        }

        if ( seasonMonths != 0 ) {
            retval = numMonths / seasonMonths;
        }

        return retval;
    }
        
    private static decimal computeDotIntens( Map<string, decimal> iguTable, dsmtEAModel.Surface mo )
    {
        decimal retval = null;

        //Example: LU[ Clothes Washer X WHIntensH] * Bldg.PctH + LU[ Clothes Washer X WHIntensC] * Bldg.PctC + LU[ Clothes Washer X WHIntensSh] * Bldg.PctSh
        if ( iguTable != null && mo.temp.FractionDaysH!=null && mo.temp.FractionDaysC!=null&& mo.temp.FractionDaysSh!=null) 
        {
            retval = iguTable.get('IntensH') * mo.temp.FractionDaysH +
                        iguTable.get('IntensC') * mo.temp.FractionDaysC +
                        iguTable.get('IntensSh') * mo.temp.FractionDaysSh;
        }

        return retval;
    }
        
    private static decimal getIntens( Map<string, decimal> iguTable, string intens )
    {
        decimal retval = null;

        if ( iguTable != null ) 
        {
            retval = iguTable.get(intens);
        }

        return retval;
    }
        
    @TestVisible
    private static decimal getLightFactor( dsmtEAModel.Surface bp, string intens )
    {
        decimal retval = null;

        if ( bp.ws != null ) 
        {
            if(intens =='IntensH') 
            {
                retval = bp.ws.LightH__c;
            }
            else if (intens == 'IntensC')
            {
                retval = bp.ws.LightC__c;
            }
            else if (intens == 'IntensSh')
            {
                retval = bp.ws.LightSh__c;
            }
        }

        return retval;
    }

    public static decimal PercentToFraction(decimal value)
    {
        return dsmtEAModel.ISNULL(value) * .01;
    }

    public static List<Mechanical_Sub_Type__c> GetThermostatRecordList(dsmtEAModel.Surface bp)
    {
        List<Mechanical_Sub_Type__c> retval = new List<Mechanical_Sub_Type__c>();

        List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if (bp.RecordTypeMap.get(mst.RecordTypeId) == 'Thermostat')
                    {
                        retval.add(mst);
                    }
                }
            }
        }
        return retval;
    }

    public static List<Mechanical_Sub_Type__c> GetDistributionSystemRecordList(dsmtEAModel.Surface bp)
    {
        List<Mechanical_Sub_Type__c> retval = new List<Mechanical_Sub_Type__c>();

        List<Mechanical_Sub_Type__c> mstList = bp.mst;
        if(mstList !=null)
        {
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Mechanical_Type__c == bp.mstObj.Mechanical_Type__c)
                {
                    if(dsmtEAModel.GetDistributionSystemRecordTypes().contains(bp.RecordTypeMap.get(mst.RecordTypeId)))
                    {
                        retval.add(mst);
                    }
                }
            }
        }
        return retval;
    }

    public static Mechanical_Sub_Type__c GetDistributionSystemMechanicalSystem(dsmtEAModel.Surface bp)
    {
        Mechanical_Sub_Type__c result = null;

        if(bp.mst ==null || bp.mst.size() ==0) return null;

        if(bp.mstObj.HeatingSystem__c!=null)
        {
            List<Mechanical_Sub_Type__c> mstList = bp.mst;
        
            For(integer i=0, j = mstList.size(); i < j; i++)
            {
                Mechanical_Sub_Type__c mst = mstList[i];
                if(mst.Id==bp.mstObj.HeatingSystem__c)
                {
                    return mst;
                }
            }
        }
        else {
            result = GetMechanicalHeatingSystem(bp);
            if(result ==null)
                result = GetMechanicalCoolingSystem(bp);
        }

        return result;
    }

    
}