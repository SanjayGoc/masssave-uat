public class newMailTemplatePageController {

    public String toAddress {
        get;
        set;
    }
    public String toAddressName {
        get;
        set;
    }
    public String subject {
        get;
        set;
    }
    public String ccAddress {
        get;
        set;
    }
    public String body {
        get;
        set;
    }
    public String fromAddress {
        get;
        set;
    }
    public List < Attachment > attachmentList {
        get;
        set;
    }
    public boolean isShowSelectTemp {
        get;
        set;
    }
    public String selTemplateId {
        get;
        set;
    }
    public String invId {
        get;
        set;
    }
    public List < AttachmentModel > listAttmod {
        set;
        get;
    }
    public string bccAddress {
        get;
        set;
    }
    public string server_url {
        get;
        set;
    }
    public string GUID {
        get;
        set;
    }
    public Invoice__c INV {
        get;
        set;
    }
    public List < selectOption > toContactOptions {
        get;
        set;
    }
    public string toId {
        get;
        set;
    }
    public Boolean showSendEmailButton {
        get;
        set;
    }
    public boolean isError;
    public boolean isSendEmail{get;set;}
    
    private String todoEmail;
    String paymentRequestId = '';
    boolean isRejection = false;
    String msgId = '';
    boolean IsForward = false;
    public String OrgId{get;set;}
    public string rplyType {set;get;}
    
    public List<SelectOption> folderList{get;set;}
    public String selFolderId{get;set;}
    
    public newMailTemplatePageController (){
        OrgId = UserInfo.getOrganizationId();
        OrgId = OrgId.substring(0,15);
        server_url = System.URL.getSalesforceBaseUrl().toExternalForm();
        toId       = todoEmail = ccAddress = '';
        listAttmod = new List < AttachmentModel > ();
        INV         = new Invoice__c();
        toAddressName = '';
        
            showSendEmailButton = true;
        
        
        attachmentList = new List < Attachment > ();
        attachmentList.add(new Attachment());
        attachmentList.add(new Attachment());
        attachmentList.add(new Attachment());
        
        selTemplateId = ApexPages.Currentpage().getParameters().get('TemplateID');
        invId          = ApexPages.Currentpage().getParameters().get('invid');
        
        String sendemail = ApexPages.Currentpage().getParameters().get('sendEmail');
        rplyType = ApexPages.Currentpage().getParameters().get('rplytype'); 
        if(ApexPages.Currentpage().getParameters().get('forward') != null){
            IsForward = true;
        }
        
        
        selFolderId = ApexPages.currentPage().getParameters().get('folderId');
        selTemplateId = ApexPages.currentPage().getParameters().get('TemplateID');
       
        
         toId = ApexPages.Currentpage().getParameters().get('toId');
        string ccEmails = ApexPages.Currentpage().getParameters().get('ccEmails');
        
        if(selTemplateId == null || (sendemail != null && Boolean.valueOf(sendemail) == true)){
           isSendEmail = true;
           getAllEmailTemplateFolders();
        }else{
           
           if(msgId != null && msgId != ''){
               isSendEmail = true;
               getAllEmailTemplateFolders();
           }else{
               isSendEmail = false;
           }
        }
        
        //isSendEmail = true;
         if(selFolderId != null && selTemplateId == null){
           getEmailTemplatesByfolder();
        }
        system.debug('---selTemplateId--1-'+selTemplateId);
        if (invId != null && invId.trim().length() > 0) {
            List < Invoice__c> lstINVs = [select id, DSMTracker_Contact__r.Contact__c,DSMTracker_Contact__r.Contact__r.Email,
                                                         (select id, name, Description from Attachments) 
                                                         FROM Invoice__c WHERE Id = : invId];
            if (lstINVs.size() > 0) {
                INV = lstINVs[0];
                toId = INV.DSMTracker_Contact__r.Contact__c;
                
                /*if(ApexPages.Currentpage().getParameters().get('Pagename') == 'sendemail'){
                    
                    
                    selTemplateId = FetchTemplateId('sendemail',INV);
                    ccEmails = PopulateToAndCCForSendEmail(INV);
                }*/
        
                }
            }
            
            system.debug('--selTemplateId ---'+selTemplateId );
            system.debug('--selTemplateId123 ---'+ApexPages.Currentpage().getParameters().get('TemplateID') );
            if(selTemplateId == null && ApexPages.Currentpage().getParameters().get('TemplateID') == null){
                //selTemplateId = FetchTemplateId('sendemail',INV);
            }
        
        
      
        if (toId!= null && toId.trim().length() > 0) {
            todoEmail = fetchTargetObject(ApexPages.Currentpage().getParameters().get('invid'), false);
        } else {
            todoEmail = fetchTargetObject(ApexPages.Currentpage().getParameters().get('invid'), false);
        }
        
        
        if (ccEmails != null && ccEmails.trim().length() > 0) {
            set < string > emlSet = new set < String > ();
            for (String eml: ccEmails.split(',')) {
                if (eml != todoEmail && eml != null && eml.length() > 0 && !emlSet.contains(eml)) {
                    ccAddress += eml + ',';
                    emlSet.add(eml);
                }
            }
        }
        
        bccAddress = Userinfo.getUserEmail();
        if (selTemplateId != null && selTemplateId.length() > 0) {
            isShowSelectTemp = false;
        }
        
        //Methods
        fillAttachmentModel();
        fetchToContactOptions();

        if (( toId == null || toId.trim().length() == 0 )) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '<li>This Enrollment Application does not have primary contact.</li>'));
        }    
        
        if (toId != null && toId.length() > 0) {
            list<Contact> conlist = [select id, Email from Contact where id =: toId];
            if(conList.size() == 0 || (conList.size() > 0 && conlist[0].Email == null)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, '<li>This Enrollment Application does not have primary contact or primary contact does not have valid Email.!</li>'));
                toId = null;
            }
            toAddressName = todoEmail + ',';
        }
    }
    
    
    public void fillAttachmentModel() {
        listAttmod = new List < AttachmentModel > ();
        if(rplyType!='msg'){
        for (Attachment__c attach: [select id, Name, Attachment_Name__c, Attachment_Type__c, File_Download_Url__c, File_Url__c, CreatedDate, CreatedBy.Name,Email_File_Download_URL__c from Attachment__c
            WHERE Invoice__c = : INV.id
        ]) {
            AttachmentModel mod = new AttachmentModel();
            mod.Attachment = attach;
            listAttmod.add(mod);
        }
        }
    }
    
    //Method
    private void fetchToContactOptions() {
        toContactOptions = new List < SelectOption > ();
        
        toContactOptions.add(new SelectOption('', '--NONE--'));
        
    }
    
    private String FetchTemplateId(String PageName,Invoice__c invObj){
            String tplId = '';
            String TemplName = 'Invoice Template';
            list<EmailTemplate>tplList = new list<EmailTemplate>();
          
            if(TemplName != null && TemplName.trim().length()>0)
                tplList = [select id from EmailTemplate where DeveloperName =: TemplName];
           
            
            system.debug('####' + tplList);
            
            if(tplList.size() > 0)
             tplId = tplList[0].Id;
             
            return tplId;
    }
    
    private String PopulateToAndCCForSendEmail(Invoice__c invObj){
        boolean TradAlly = false;
        boolean Custtomer = false;
        String toCon = '';
        String ccEmails1 = '';
        
        toCon = invObj.DSMTracker_Contact__r.Contact__c ; 
        
        
        toId = tocon;
        return  ccEmails1;
    }
    
    /**
     * Get Email Templates By Folder to fill Folder dropdown 
     *
     * @param Nothing 
     * @return Nothing.
     */
    
    public void getAllEmailTemplateFolders(){
       folderList = new List<SelectOption>();
       templateList = new List<SelectOption>();
       
       Map<String,String> folderIdtoName = new Map<String,String>();
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true order by FolderId];
       
       for(EmailTemplate et : emailTemplatelist){
            
           
               folderIdtoName.put(et.folderId,et.Folder.Name);
           
       }
       system.debug('--folderIdtoName--'+folderIdtoName);
        folderList.add(new SelectOption('','--Select--'));
        templateList.add(new SelectOption('','--Select--'));
        for(String folderid : folderIdtoName.keySet()){
           system.debug('--folderid--'+folderid);
           system.debug('--folderIdtoName.get(folderid)--'+folderIdtoName.get(folderid));
           if(folderIdtoName.get(folderid) != null)
             folderList.add(new SelectOption(folderid, folderIdtoName.get(folderid)));
        }
    }
    
     //Modal class
    public class AttachmentModel {
        public Attachment__c attachment {
            set;
            get;
        }
        public Boolean isAttach {
            set;
            get;
        }
        public AttachmentModel() {
            attachment = new Attachment__c();
            isAttach = false;
        }
    }
    
    public List<SelectOption> templateList{get;set;}
    
    /**
     * Get Email Templates By Folder to fill  template dropdown
     *
     * @param Nothing 
     * @return Nothing.
     */
   
    public void getEmailTemplatesByfolder(){
       templateList = new List<SelectOption>();
       templateList.add(new SelectOption('','--Select--'));
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true and FolderId =: selFolderId order by Name];
       for(EmailTemplate et : emailTemplatelist){
          templateList.add(new SelectOption(et.Id, et.Name));
       }
    }
    
    private String htmlToText(String htmlString) {
        String RegEx = '(</{0,1}[^>]+>)';
        if (htmlString == null)
            htmlString = '';
        return htmlString.replaceAll('&nbsp', '').replaceAll(';', '').replaceAll(RegEx, '');
    }
    
    /**
        This Method send Email with attachments.
    */
    private String fetchTargetObject(string invid, boolean bl) {
        string id = '';
         for (Invoice__c inv: [select Id, DSMTracker_Contact__r.Contact__c, DSMTracker_Contact__r.Contact__r.Email 
                                       from Invoice__c
                                       where Id =: invid
                                       ]) {
                
                id = bl ? ('' + inv.DSMTracker_Contact__r.Contact__c) : inv.DSMTracker_Contact__r.Contact__r.Email;
                break;
            }
        return id;
    }
    
    public void addMessage(String Msg) {
        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, Msg));
    }
    
    //Method that will send Emails to the selected ContactLists
    public pagereference sendEmail() {
        boolean isErr = false;
        string errMsg = '';
        string trgetObjId = fetchTargetObject(ApexPages.Currentpage().getParameters().get('invid'), true);

        system.debug('---toAddressName ---'+toAddressName );
        
           
        if (toAddressName == null || toAddressName.trim().length() == 0) {
            errMsg += '<li>Email must contain an email address in the To field.</li>';
            isErr = true;
        }
        
        if (subject == null || subject.trim().length() == 0) {
            errMsg += '<li>Subject is required for send email!</li>';
            addMessage(errMsg);
            isErr = true;
        }
        
        

        if (isErr == false) {
            //Initialize/reset variables
            isError = false;
            string errorMsg = '';
          
            list < string > additionalList = new list < string > ();
            if (toAddressName != null && toAddressName.length() > 0) {
                additionalList = toAddressName.deletewhitespace().split(',');
            }

            list < string > ccAddressList = new list < string > ();
            if (ccAddress != null && ccAddress.length() > 0) {
                ccAddressList = ccAddress.deletewhitespace().split(',');
            }
            
            ccAddressList.add(Label.Bounce_Tracking_Email_Address);

            list < string > bccAddressList = new list < string > ();
            if (bccAddress != null && bccAddress.length() > 0) {
                bccAddressList = bccAddress.deletewhitespace().split(',');
                bccAddressList.add(UserInfo.getUserEmail());
            }

            // Attach Attachments
            list < Messaging.Singleemailmessage > emails = new list < Messaging.Singleemailmessage > ();
            list < Messaging.Emailfileattachment > listAttach = new list < Messaging.Emailfileattachment > ();
            
            string attachmentNamesList = 'Attachments:\n';
            set < Id > attIds = new set < Id > ();
            String attachmentUrl = '';
            if(rplyType!='msg'){
                for (AttachmentModel mod: listAttmod) {
                    if (mod.isAttach) {
                        String newURL = '';
                         String oldURL =  mod.attachment.Email_File_Download_URL__c;
                         String[] questionsplit = oldURL.split('\\?');
                         String[] ampSplit = questionsplit[1].split('&');
                         newURL = questionsplit[0]+'?';
                         system.debug('--newURL--'+newURL);
                         system.debug('--ampSplit'+ampSplit);
                         for(String parameter : ampSplit){
                           String[] equalsplit = parameter.split('=');
                           newURL += equalsplit[0]+'='+EncodingUtil.urlEncode(equalsplit[1],'UTF-8')+'&'; 
                         }
                         newURL = newURL.subString(0,newURL.length() - 1);
                         system.debug('--newURL--'+newURL);
                        attachmentUrl += '<li><a target="_self" href="' + newURL + '">' + mod.attachment.Attachment_Name__c + '</a></li>';
                        attIds.add(mod.attachment.id);
                    }
                }
            }

            //Add to,Cc,AttionalTo
            Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
            
            system.debug('---additionalList---'+additionalList);
            
            if (additionalList != null && additionalList.size() > 0)
                email.setToAddresses(additionalList);
            if (ccAddressList != null && ccAddressList.size() > 0)
                email.setCcAddresses(ccAddressList);
            if (bccAddressList != null && bccAddressList.size() > 0)
                email.setBCcAddresses(bccAddressList);
            
            system.debug('---email.setToAddresses---'+email.getToAddresses());
               
            email.setSubject(subject);
            email.setSaveAsActivity(false);
            List<OrgWideEmailAddress >lstOrgWideEmailAddress =[select id, Address, DisplayName from OrgWideEmailAddress
                                                          WHERE DisplayName='DSMT Support']; 
           if(lstOrgWideEmailAddress.size()>0){
                email.setOrgWideEmailAddressId(lstOrgWideEmailAddress[0].id);
                fromAddress = lstOrgWideEmailAddress[0].Address;
            }
            
            String currUserId = UserInfo.getUserId(); 
            
             
            
            
            system.debug('---email.setOrgWideEmailAddressId---'+email.getOrgWideEmailAddressId());
            
            //Fill in the BODY        
            String taskDescription = body;
            if (attachmentUrl != '') {
                attachmentUrl = '<br/><b>Download Attachments Here:</b><br/>' + attachmentUrl;
            }
            
            email.setHtmlBody(body + attachmentUrl);
            if (listAttach != null && listAttach.size() > 0) {
                email.setFileAttachments(listAttach);
            }
            
            emails.add(email);
            system.debug('---emails----'+emails);
            
           

            List<User> UserList = [select Id from User where id =: UserInfo.getUserId()];
            if(rplyType=='msg'){
                system.debug('----------> MSG');
                Messaging.sendEmail(emails);
            }
            if(fromAddress == '' || fromAddress == null)
                fromAddress = userinfo.getUserEmail();
            try {
                if(!Test.isRunningTest()){
                
                      if(UserList != null ){
                        Messaging.sendEmail(emails);
                    }
                    
                
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Message Sent Successfully'));
                
                string exp_ids = ApexPages.currentPage().getParameters().get('exp_ids');
                if(exp_ids != null && exp_ids.trim().length() > 0){
                    list<Exception__c> exceptions = [select Id, Disposition__c from Exception__c where id in : exp_ids.split(',')];
                    if(exceptions != null && exceptions.size() > 0){
                        for(Exception__c e : exceptions){
                            e.Disposition__c = 'Awaiting Customer Information';
                        }
                        if(!Test.isRunningTest()){
                            update exceptions;
                        }
                    }
                }
                
                for (Attachment att: attachmentList) {
                    att.body = null;
                }
                
                string NewMsgId = Message.CreateEmailMessage(fromAddress,toAddressName,ccAddress,bccAddress,invid,subject,body+attachmentUrl,'Send Email',trgetObjId,MsgId);
                        system.debug('@@Body111@@'+body);
                
            } catch (Exception ex) {
                system.debug('Exception is -->:'+ex.getMessage());
                system.debug('Line number is--> :'+ex.getLinenumber());
                addMessage(ex.getMessage());
                for (Attachment att: attachmentList) {
                    att.body = null;
                }
                return null;
            }
        } else {
            addMessage(errMsg);
            return null;
        }
        return new PageReference('/' + invId);
    }
    
     // This method is for Cancel
    public pagereference cancel() {
        if(IsForward){
            return new PageReference('/' + msgId);
        }else{
            if(rplyType!='msg'){
            return new PageReference('/' + invId);
            }
            else{
                return new PageReference('/' + msgId);
            }
        }
    }
    
     /**
     * Reload page based on Program
     *
     * @param Nothing 
     * @return Pagereference .
     */
   
    public PageReference reloadPage(){
       PageReference pg = null;
       /* @author Hemanshu Patel - GTES
        * @date 20/02/2017
        *
        * @group Ticket#-000655 
        * @group-content https://na16.salesforce.com/a0vj0000004vrkz?srPos=0&srKp=a0v
        *
        * @description Added code for new prorgam Smart $aver custom performance.
        */
       
           pg = Page.dsmtNewEmail;
       
       pg.getParameters().put('invId', invId);
       pg.getParameters().put('TemplateID', selTemplateId);
       pg.getParameters().put('sendEmail', String.valueOf('false'));
       pg.getParameters().put('folderId',selFolderId);
       pg.getParameters().put('Pagename','sendemail');
       pg.setRedirect(true);
       return pg;
    }
    
    // This Method Fill HTML Body using Template    
    //***************************** IMPORTANT ***************************
    //** Must make sure that a contact exists with the last name 'Do Not Delete' in order for this to work.
    public PageReference fillHtmlBody() {
         if (selTemplateId != null && selTemplateId.trim().length() > 0) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {
                 Label.Email_Address_for_Mail
            };
            mail.setToAddresses(toAddresses);
           
            mail.setUseSignature(false);
            mail.setSaveAsActivity(true);
            mail.setSenderDisplayName('MMPT');
            
            system.debug('===invId=='+invId);
            //if(invId!=null && invId !='')
                //mail.setWhatId(invId);
            
                
            List < Contact > lstContact = [select id, name, email from contact where Name = 'Do Not Delete'];
            mail.setTargetObjectId(lstContact[0].id);
            mail.setTemplateId(selTemplateId);
            Savepoint sp = Database.setSavepoint();

            try {
                if(!Test.isRunningTest()){
                List<Messaging.SendEmailResult> results =
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                            mail
                            });
                system.debug('--mail---'+mail);
                system.debug('--results --'+results);
                }

                List < EmailTemplate > emailTemp = [select TemplateType, Id from EmailTemplate where id = : selTemplateId limit 1];
                if (emailTemp != null && emailTemp.size() > 0) {
                    if (emailTemp[0].TemplateType.equalsIgnoreCase('text')) {
                        body = mail.getPlainTextBody();
                    } else {
                        body = mail.getHtmlBody();
                        body = body.replace('<meta charset="utf-8">', '').replace('<br>', '<br/>');
                        body += '</body>';
                    }

                }
                subject = mail.getSubject();
                system.debug('--body ---'+body );
                system.debug('--subject ---'+subject );
                
                
                
                Database.rollback(sp);
            }
            Catch(Exception e) {
                system.debug('==e=='+e);
                addMessage(e.getMessage());
                Database.rollback(sp);
            }

        }
        
        
        
        return null;
    }
}