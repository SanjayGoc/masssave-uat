Public Class dsmtKickoffReviewProcessCntroller{

    public String assessId {get;set;}
    Public Set<String> custId = new set<String>();
    public String AssessmentName{get;set;}
    public list<Proposal__c> selectedProposalList{get;set;}
    public String proposalId {get;set;}
    public Boolean isError{get;set;}
    public List<ProposalModal> proposalModallist{get;set;}
    public List<Recommendation_Scenario__c> projectListStep2{get;set;}
    public Deposit_and_Payment__c dp{get;set;}
    public Boolean showSetp2{get;set;}
    public Id DespositPaymentRecodType {get;set;}
    public String DespositPaymentRecodTypeName {get;set;}
    public String Customer {get;set;}
    public String ProjectName {get;set;} 
    
    Public dsmtKickoffReviewProcessCntroller(){
       
        assessId = ApexPages.currentPage().getParameters().get('assessId');
        isError = false;
        fillProposalModallist();
        DespositPaymentRecodType = Schema.SObjectType.Deposit_and_Payment__c.getRecordTypeInfosByName().get('Deposit').getRecordTypeId();
        DespositPaymentRecodTypeName = 'Deposit';
    }
    
    Public String getCustomerInfo(){
        
        List<Energy_Assessment__c> EAList = [Select id,name,Customer__c from Energy_Assessment__c where id =: assessId];
        if(EAList.size() > 0){
            for(Energy_Assessment__c ea: EAList){
                custId.add(ea.Customer__c);
                AssessmentName = ea.name;
            }
        }        
        
        if(custId != null){
            List<Customer__c> customerList = [select id, Name, Service_Address__c,
                                              Service_City__c, Service_State__c, Service_Zipcode__c
                                              from Customer__c
                                              where id =: custId
                                              limit 1];
            
            if(customerList.size() > 0){
                String custInfo = customerList[0].Name + ' | ';
                
                custInfo += customerList[0].Service_Address__c + ', ' + customerList[0].Service_City__c + ', ' + customerList[0].Service_State__c
                            + ', ' + customerList[0].Service_Zipcode__c;
                
                return custInfo;
            }
        }
        return null;
    }
 
    public void fillProposalModallist(){
       proposalModallist = new List<ProposalModal>();
       List<Proposal__C> ProposalList = [select id, Name, Type__c, Status__c, Calc_Deposit_Amount__c, Customer_Contribution__c, CreatedDate, Total_Cost__c, Name_Type__c ,
                Total_Incentive__c, Total_Annual_Energy_Savings__c, Total_Payback__c,Final_Proposal_Incentive__c,Signature_Date__c,
                Deposit_Amount__c,Deposit_Type__c
                from Proposal__c
                where Energy_Assessment__c =: assessId
                and Energy_Assessment__c != null and status__c != 'Inactive'
                order by CreatedDate desc];
      for(Proposal__c prop : proposalList){
          ProposalModal modal = new ProposalModal();
          modal.proposal = prop;
          modal.isSelect = false;
          proposalModallist.add(modal);
      }          
    }
    
    public pageReference step1Next(){
        isError = false;
        Set<String> propIds = new Set<String>();
        
        for(ProposalModal modal : proposalModallist){
           system.debug('--modal--'+modal);
           
           if(modal.isSelect){
               Proposal__c prop = modal.Proposal;
               if(prop.Signature_Date__c == null){
                  ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Please add signature date for proposal :'+prop.Name);
                  ApexPages.addMessage(msg);
                  isError = true; 
               }
               propIds.add(prop.id);
               system.debug('propIds---'+propIds);
           }
        } 
        
        if(!isError){
            Set<String> recommIds = new Set<String>();
            List<Proposal_Recommendation__c> propreclist = [select id,Recommendation__c,proposal__c from proposal_recommendation__c where proposal__c in: propIds];
            system.debug('propreclist----'+propreclist);
            for(Proposal_Recommendation__c proprec : propreclist){
                 if(!recommIds.add(proprec.Recommendation__c)){
                     ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'One or more of the selected Proposals contain the same recommendations. The Project(s) and Pre Work Review could not be created.');
                      ApexPages.addMessage(msg);
                      isError = true;
                 }
            }
        }
        
        if(!isError){
           List<Proposal__c> updateProposal = new List<Proposal__c>(); 
           boolean isProposal = false;
           for(ProposalModal modal : proposalModallist){
               system.debug('--modal--'+modal);
           
               if(modal.isSelect){
                   Proposal__c prop = modal.Proposal;
                   if(prop.status__c == 'Proposed'){
                       prop.Status__c = 'Signed';
                       updateProposal.add(prop);
                       isProposal = true;
                   }
                   if(prop.status__c == 'Contracted'){
                       isProposal = true;
                   }
               }
           }
           if(updateProposal.size() > 0){
               update updateProposal;
           }
           
           List<Recommendation_Scenario__c> projectList = [SELECT id,Proposal__c,Energy_Assessment__r.Customer__c,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c,
                                                           Energy_Assessment__r.Customer__r.First_Name__c,Energy_Assessment__r.Customer__r.Last_Name__c,Is_Heat_Loan__c,Address__c,City__c,Zip__c,State__c,Phone__c,Email__c
                                                           from Recommendation_Scenario__c WHERE Proposal__c IN : propIds];
                                                           
           if(projectList.size() > 0){
               if(isProposal){
                   Recommendation_Scenario__c Proj = projectList.get(0);
                   
                   List<Project_Review__c> projRevieeList = [Select id,Project__c from Project_Review__c where Project__c =: Proj.id];
                   if(projRevieeList.size() > 0){
                       ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Pre work review is already created so you can not create pre work review for it.');
                       ApexPages.addMessage(msg);
                     
                       isError = true;
                   }
                   if(!isError){
                       Id preRevRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Pre Work Review').getRecordTypeId();
                       Review__c rev = new Review__c();
                       rev.Type__c = 'Pre Work Review';
                       rev.Review_Type__c = 'Pre Work Review';
                       rev.Requested_By__c = userinfo.getUSerId();
                       rev.RecordTypeId = preRevRecordTypeId ;
                       rev.Status__c = 'Pending Review';
                       rev.Address__c = Proj.Address__c;
                       rev.City__c = Proj.City__c ;
                       rev.Zipcode__c = Proj.Zip__c;
                       rev.State__c = Proj.State__c;
                       rev.Phone__c = proj.Phone__c;
                       rev.Email__c = Proj.Email__c;
                       rev.Trade_Ally_Account__c = proj.Energy_Assessment__r.Trade_Ally_Account__c;
                       rev.DSMTracker_Contact__c= proj.Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c;
                       rev.First_Name__c = proj.Energy_Assessment__r.Customer__r.First_Name__c;
                       rev.Last_Name__c = proj.Energy_Assessment__r.Customer__r.Last_Name__c;
                       rev.Energy_Assessment__c = Proj.Energy_Assessment__c;
                       rev.Customer__c = proj.Energy_Assessment__r.Customer__c;
                       rev.Heat_Loan__c = proj.Is_Heat_Loan__c ;
                       rev.Projects__c = proj.id;
                       insert rev; 
                       
                       showSetp2 = true;
                   }
               }         
           }
        }
        
        return null;
        
    }
    
    public List<Recommendation_Scenario__c> getStep2ProjectList(){
        projectListStep2 = [SELECT id,Name,Deposit_Not_Required__c,Deposit_Note__c,Deposit_Type__c,Energy_Assessment__r.Customer__c,Energy_Assessment__r.Customer__r.name,Deposit_Amount__c,Energy_Assessment__c,Proposal__c,Proposal__r.Name_type__c,
                            recordtype.name from Recommendation_Scenario__c WHERE Energy_Assessment__c =: assessId and Proposal__r.Name_type__c != 'Air Sealing'];
        for(Recommendation_Scenario__c proj : projectListStep2){
            ProjectName = proj.name;        
        }                    

        return projectListStep2;                   

    }
   
    public class proposalModal{
        public proposal__c proposal{get;set;}
        public Boolean isSelect{get;set;}
        public proposalModal(){
           proposal = new Proposal__c();
           isSelect = false;
        }
    }
}