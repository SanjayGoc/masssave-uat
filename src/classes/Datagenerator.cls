public class Datagenerator {

    
     //Create Loacation
     public static Location__c  createLocation(){
        Location__c loc = new Location__c();
        loc.Name = 'test loc';
        return loc;
     }
     
     public static event createEvent(Id WhatId){
      //Create Location Event 
        Event event = new Event();
        event.Subject = 'Holiday';
        event.StartDateTime = DateTime.now().addDays(-5);
        event.EndDateTime = DateTime.now().addDays(5);
        event.WhatId = WhatId;
        return event;
    }
    
    public static Employee__c createEmployee(Id LocationId){
        Employee__c emp = new Employee__c ();
        emp.Name = 'emp name';
        emp.Location__c = LocationId;
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        emp.Employee_Id__c = hexDigest.substring(0,11);
        emp.Status__c = 'Active';
        return emp;
    }
    
    public static Work_Team__c CreateWorkTeam(Id locId,Id EmpId){
        Work_Team__c workTeam = new Work_Team__c();
        workTeam.Name = 'wrokteam';
        workTeam.Location__c = locId;
        workTeam.Service_Date__c = System.today();
        workTeam.Deleted__c = false;
        workTeam.Captain__c = empId;
        return workTeam;
    }
    
   
    
   
    public static Region__c CreateRegion(Id LocationId){
        Region__c reg = new Region__c();
        reg.Name = '88220';
        reg.Type__c = 'Zipcode';
        reg.Location__c = locationId;
        return reg;
    }
    
    public static Territory__c createTerritory(Id RegId,Id EmpId,Id wtId){
        Territory__c ter = new Territory__c();
        ter.Employee__c = empId;
        ter.Region__c = regId;
        ter.Work_Team__c = wtId;
        ter.Territory_Name__c = 'test';
        return ter;
    }
    
    public static Territory_Detail__c createTerritoryDetail(Id terId){
        Territory_Detail__c td = new Territory_Detail__c();
        td.Territory__c = terId;
        td.Coordinates__latitude__s = 32.010404958190534;
        td.Coordinates__longitude__s = -104.512939453125;
       // td.Coordinates__c = '32.010404958190534 -104.512939453125';
        return td;
    }
    
    public static Warehouse__c createWarehouse(){
        Warehouse__c wh = new Warehouse__c();
        wh.Name = 'whname';
        return wh;
    }
    
    public static Product__c createProduct(){
        
        Product__c prod = new Product__c();
        prod.Name = 'test';
        prod.Type__c = 'Inventory';
        prod.Description__c = 'test';
        return prod;
    }
    
    public static Employee_Skill__c createempSkill(Id empId,Id skillId){
    
        Employee_Skill__c es = new Employee_Skill__c();
        es.Employee__c = empId;
        es.Skill__c = skillId;
        es.Survey_Score__c = 10;
        return es;
    }
    
    public static Skill__c createSkill(){
    
        Skill__c s = new Skill__c();
        s.Description__c = 'test';
        s.Name = 'Test';
        s.Maximum_Score_Level__c = 10;
        s.Minimum_Score_Level__c = 5;
        s.Watch_List_Threshold__c = 7;
        return s;
    }
    
    public static Workorder__c createWo(){
        
        Workorder__c wo = new Workorder__c();
        return wo;
    }
    
    public static Account createAccount(){
        Account acc = new Account();
        acc.Name = 'Xcel Energy NM';
        String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        acc.Billing_Account_Number__c= hexDigest.substring(0,11);
        return acc;
    }
    
    public static Premise__c createPremise(){
        Premise__c prem = new Premise__c ();
        prem.Address__c = 'test';
        prem.City__c = 'test';
        prem.State__c = 'test';
        prem.Zip_Code__c = 'test';
        return prem;
    }
    
    public static Customer__c createCustomer(Id accId,Id premId){
        Customer__c cust = new Customer__c();
        cust.Account__c = accId;
        cust.Name = 'test';
        cust.First_Name__c='hemanshu';
        cust.Last_Name__c='patel';
        cust.Service_Zipcode__c='1234';
        cust.Account__c = accId;
        cust.Premise__c = PremId;
        return cust;
    }
    
     public static Eligibility_Check__c createELCheck(){
        Eligibility_Check__c elCheck = new  Eligibility_Check__c( Zip__c='1234',First_Name__c='hemanshu',Last_Name__c='patel',Gas_Account_Holder_First_Name__c ='Gas-123',Gas_Account_Holder_Last_Name__c='test-123');
         insert elCheck;
        return elCheck;
    }
    public static Program_Eligibility__c createProgramEL(){
        Program_Eligibility__c progEl = new Program_Eligibility__c(Zip_Code__c='1234',City__c='US');
        progEl.Heating_Fuel_Eligibility__c = 'Y';
        progEl.Fuel__c  = 'Gas';
         insert progEl;
        return progEl;
    }
     public static Trade_Ally_Account__c createTradeAccount(){
       Trade_Ally_Account__c TAcc = new Trade_Ally_Account__c(name='test');
       insert TAcc;
        return TAcc;
    }
    
   public static Call_List_Line_Item__c createLineItem(id custid){
       Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
       // obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        //obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        obj.customer__c=custid;
        insert obj;
        return obj;
    } 
    
     public static customer_Eligibility__c createCustomerEL(id Elcheckid){
       customer_Eligibility__c  CustEl = new Customer_Eligibility__c (Eligibility_Check__c =Elcheckid,Electric_Account__c='Electric',Gas_Account__c='Gas');
        insert CustEl;
        return CustEl;
    }
    public static Appointment__c createAppointment(id ELcheck){
       Appointment__c app = new Appointment__c(Appointment_Status__c='Pending',Appointment_Type__c='test',Eligibility_Check__c=ELcheck,Appointment_Start_Time__c=date.today(),Appointment_End_Time__c= date.today()) ;
        insert app;
        return app;
    }
    public static DSMTracker_Contact__c createDSMTracker(){
       DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        insert dsmt;
        return dsmt;
    }
    
     public static Review__c createReview(Id TaccID){
       Review__c rev = new Review__c(Email__c='test@test.com',Trade_Ally_Account__c=TaccID,Status__c = 'Draft', Requested_By__c = Userinfo.getuserid(),Review_Type__c='Document');
        insert rev;
        return rev;
    }
   
     public static Attachment__c createAttachment(Id TaccID){
       Attachment__c att = new Attachment__c(Trade_Ally_Account__c=TaccId,Status__c='New');
        insert att;
        return att;
    }
    public static Registration_Request__c createRegistration(){
       Registration_Request__c reg = new Registration_Request__c(First_Name__c='hemanshu',Last_Name__c='patel',Email__c='test@test.com',Status__c='test');
        insert reg;
        return reg;
    }
     public static Document createDocument(){
       Document doc= new Document(name='test',Body=blob.valueOf('test'));
      //  insert doc;
        return doc;
    }
     public static Email_Custom_Setting__c createEmailCustSetting(){
       Email_Custom_Setting__c emailTempSet= new Email_Custom_Setting__c(name='test',Registration_Documents_Folder__c='General Templates',Email_Template__c='New_Message_Created_From_Portal');
        insert emailTempSet;
        return emailTempSet;
    }
    public static Messages__c createMessage(){
      Messages__c msg =new Messages__c(Message_Date__c=date.today(),Sent__c=date.today(),From__c='test@test.com',CC__c='test@test.com',BCC__c='test@test.com',To__c='test@test.com'
                    ,Subject__c='test',Message__c='test');
        insert msg;
        return msg;
    }
    
    public static UCS_AgilePMO__Ticket__c createUCSAgilePMOTk (){
      UCS_AgilePMO__Ticket__c lstTickets =new UCS_AgilePMO__Ticket__c(Name='test',UCS_AgilePMO__Type__c='test',UCS_AgilePMO__Status__c='test',UCS_AgilePMO__Priority__c='high',UCS_AgilePMO__Description__c='test');
      insert lstTickets;
      return lstTickets;
    }
    
    Public Static User CreatePortalUser(){
        
        Account a = new Account(name= 'test');
        insert a;
        
        Contact c = new Contact(lastname = 'test',email = 'test@test.com',AccountId = a.id);
        insert c;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Portal Manager Custom']; 
        
        User tuser = new User(  firstname = 'tuserFname',
                            lastName = 'tuserLastname',
                            email = 'tuser@test.org',
                            contactid = c.id,
                            profileId = p.id,
                            MobilePhone='1234567890',
                            Send_SMS_Notification__c=true,
                            Username = 'tuser@test.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias ='tuser',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US'
                            );
        insert tuser;                    
        return tuser;
    }
    
    public static Energy_Assessment__c createEnergyAssessment(){
        Energy_Assessment__c ea = new Energy_Assessment__c();
        insert ea;
        return ea;
    }
    
    public static Energy_Assessment__c setupAssessment(){
        
        LightingAndApplianceConstant__c lac = new LightingAndApplianceConstant__c();
        lac.DefaultIndoorLightingPerSqFt__c = 10;
        lac.DefaultOutdoorLightingUsage__c = 'Medium';
        lac.DefaultOutdoorLightingPerSqFt__c = 10;
        insert lac;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        Workorder__c wo = new Workorder__c();
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();
        insert wo;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Year_Built__c='1955';
        bs.Bedromm__c = 3;
        bs.Occupants__c = 3;
        bs.Orientation__c = 'South';
        bs.Ceiling_Heights__c = 8;
        bs.Floors__C=1.0;
        bs.Floor_Area__c = 1800;
        
        bs.Appointment__c = appt.id;        
       
        insert bs;
        
        WallRatio__c wr = new WallRatio__c();
        wr.Name = 'Rectangular';
        wr.Value__c = 1;
        insert wr;
        
        FloorWallExp__c fw = new FloorWallExp__c();
        fw.Name = 'Rectangular';
        fw.Value__c = 1;
        insert fw;
        
        Saving_Constant__c sc = new Saving_Constant__c();     
        sc.FrontBackToLeftRightWindowAreaFactor__c = 10;
        sc.DefaultWindowFilmTotalSolarRejection__c = 10;
        sc.InsulationQuiltLengthOfNightFactor__c = 10;
        insert sc;
        
        List<WallInsulationAmountRValue__c> wiavList = new List<WallInsulationAmountRValue__c>();
        
        WallInsulationAmountRValue__c wiav = new WallInsulationAmountRValue__c();
        wiav.Name = 'None';
        wiav.Value__c = 11;
        wiavList.add(wiav);
        
        WallInsulationAmountRValue__c wiav1 = new WallInsulationAmountRValue__c();
        wiav1.Name = 'Fills Cavity'; 
        wiav1.Value__c = 11;
        wiavList.add(wiav1);
        
        WallInsulationAmountRValue__c wiav2 = new WallInsulationAmountRValue__c();
        wiav2.Name = 'Lots';
        wiav2.Value__c = 11;
        wiavList.add(wiav2);
        
        WallInsulationAmountRValue__c wiav3 = new WallInsulationAmountRValue__c();
        wiav3.Name = 'Some';
        wiav3.Value__c = 11;
        wiavList.add(wiav3);
        
        WallInsulationAmountRValue__c wiav4 = new WallInsulationAmountRValue__c();
        wiav4.Name = 'Standard';
        wiav4.Value__c = 11;
        wiavList.add(wiav4);
        
        WallInsulationAmountRValue__c wiav5 = new WallInsulationAmountRValue__c();
        wiav5.Name = 'Super';
        wiav5.Value__c = 11;
        wiavList.add(wiav5);
        
        WallInsulationAmountRValue__c wiav6 = new WallInsulationAmountRValue__c();
        wiav6.Name = 'Unknown';
        wiav6.Value__c = 11;
        wiavList.add(wiav6);
        
        insert wiavList;
    
        WeatherStation__c ws = new WeatherStation__c();
        ws.CDLatentMult__c = 4.02;
        ws.CDMultPCFM__c = 0.6568881;
        ws.CDMult__c = 1.125;
        ws.CExposeMult__c = 1.860;
        ws.CHeightExp__c = 0.326;
        ws.CNFactor__c= 38.31;
        ws.CSolarH__c= 115237.00;
        ws.CZ1_2__c= 0;
        ws.Clg1PctDesignTempDryBulb__c = 86.50;
        ws.Clg1PctDesignTempWetBulb__c = 71.60;
        ws.ClgGrDiff__c = 27.00;
        ws.Elevation__c = 62.32;
        ws.HDMultPCFM__c = 1.084081;
        ws.HDMult__c = 1.175;
        ws.HNFactor__c = 23.48;
        ws.HSolarH__c = 208026;
        ws.Htg99PctDesignTemp__c =  10.80;        
        ws.LightC__c = 0.8900;
        ws.LightH__c =1.06000;
        ws.LightSh__c = 0.90000;
        ws.TGround__c = 50;        
        ws.NormalNFactor__c = 18.00;
        insert ws;
    
        List<WeatherStationTemprature__c> wstList = new List<WeatherStationTemprature__c>();
        WeatherStationTemprature__c wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 58;
        wst.TempValues__c = '170,162,154,152,143,139,135,132,123,119,115,110,101,94,85,79,69,61,56,51,45,39,30,24,14,13,11';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CD';
        wstList.add(wst);

        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 40;
        wst.TempValues__c = '1113,1233,1359,1488,1623,1763,1910,2069,2233,2400,2572,2749,2933,3130,3331,3535,3752,3971,4198,4429,4664,4907,5159,5417,5690,5967,6249,6541,6837,7138,7446,7755,8074';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'HDD';
        wstList.add(wst);

        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 58;
        wst.TempValues__c = '3101,2942,2758,2636,2505,2374,2257,2120,1976,1832,1675,1552,1379,1241,1106,956,830,682,590,515,440,378,296,245,185,129,100';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CH';
        wstList.add(wst);

        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 50;
        wst.TempValues__c = '2701,2514,2335,2165,2004,1849,1701,1557,1420,1289,1161,1041,930,822,719,626,539,460,386,318,257,203,155,116,86,61,41,28,21,15,10';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CDD';
        wstList.add(wst);

        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 40;
        wst.TempValues__c = '2302,2513,2646,2760,2895,3041,3203,3422,3572,3667,3807,3941,4081,4297,4438,4537,4795,4907,5060,5200,5313,5487,5659,5813,6049,6157,6292,6447,6567,6722,6862,6947,7164';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'HH';
        wstList.add(wst);

        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 68;
        wst.TempValues__c = '12575,10900,9348,7969,6728,5622,4666,3836,3154,2564,2049,1609,1231,935,690,505,376';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'CDH';
        wstList.add(wst);

        wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 5;
        wst.TempStart__c = -5;
        wst.TempValues__c = '0,0.000,0.000,0.012,0.042,0.125,0.230,0.370,0.507,0.723,0.828,0.920,0.977,1.000';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'HL';
        wstList.add(wst);

        insert wstList;

        List<SolarGainByOperation__c> sgList = new List<SolarGainByOperation__c>();
        SolarGainByOperation__c sg = new SolarGainByOperation__c();
        sg.WeatherStation__c = ws.Id;
        sg.Orientation__c = 'South';
        sg.SolarGain__c = 53605;        
        sgList.add(sg);

        sg = new SolarGainByOperation__c();
        sg.WeatherStation__c = ws.Id;
        sg.Orientation__c = 'East';
        sg.SolarGain__c = 60520;        
        sgList.add(sg);

        insert sgList;
        
    /*Safety_Aspect__c sf =new Safety_Aspect__c();      
    insert sf;*/
        
        Energy_Assessment__c ea =new Energy_Assessment__c();
        //ea.Safety_Aspect__c=sf.id;
        ea.Building_Specification__c = bs.id;   
        ea.Building_Shape__c = 'Rectangular';
        ea.WeatherStation__c = ws.id;                
        insert ea;
        
        return ea;
    }
    
    // this method is not acchully ea to database developer has to call insert method explicity where it used.
     public static Energy_Assessment__c setupAssessmentWithWorkOrder(){
        
        LightingAndApplianceConstant__c lac = new LightingAndApplianceConstant__c();
        lac.DefaultIndoorLightingPerSqFt__c = 10;
        lac.DefaultOutdoorLightingUsage__c = 'Medium';
        lac.DefaultOutdoorLightingPerSqFt__c = 10;
        insert lac;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        Workorder__c wo = new Workorder__c();
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();         
        insert wo;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Floor_Area__c = 1;
        bs.Orientation__c = 'South';
        bs.Floors__C=1.0;
        bs.Bedromm__c = 1;
        bs.Ceiling_Heights__c = 10;
        bs.Appointment__c = appt.id;
        bs.Occupants__c = 10;
        bs.Year_Built__c='2018';
        insert bs;
        
        WallRatio__c wr = new WallRatio__c();
        wr.Name = 'Rectangular';
        wr.Value__c = 1;
        insert wr;
        
        FloorWallExp__c fw = new FloorWallExp__c();
        fw.Name = 'Rectangular';
        fw.Value__c = 1;
        insert fw;
        
        Saving_Constant__c sc = new Saving_Constant__c();
        sc.DefaultDoorWidth__c = 10;
        sc.DefaultDoorheight__c = 10;
        sc.FrontBackToLeftRightWindowAreaFactor__c = 10;
        sc.DefaultWindowFilmTotalSolarRejection__c = 10;
        sc.InsulationQuiltLengthOfNightFactor__c = 10;
        insert sc;
        
        List<WallInsulationAmountRValue__c> wiavList = new List<WallInsulationAmountRValue__c>();
        
        WallInsulationAmountRValue__c wiav = new WallInsulationAmountRValue__c();
        wiav.Name = 'None';
        wiav.Value__c = 11;
        wiavList.add(wiav);
        
        WallInsulationAmountRValue__c wiav1 = new WallInsulationAmountRValue__c();
        wiav1.Name = 'Fills Cavity'; 
        wiav1.Value__c = 11;
        wiavList.add(wiav1);
        
        WallInsulationAmountRValue__c wiav2 = new WallInsulationAmountRValue__c();
        wiav2.Name = 'Lots';
        wiav2.Value__c = 11;
        wiavList.add(wiav2);
        
        WallInsulationAmountRValue__c wiav3 = new WallInsulationAmountRValue__c();
        wiav3.Name = 'Some';
        wiav3.Value__c = 11;
        wiavList.add(wiav3);
        
        WallInsulationAmountRValue__c wiav4 = new WallInsulationAmountRValue__c();
        wiav4.Name = 'Standard';
        wiav4.Value__c = 11;
        wiavList.add(wiav4);
        
        WallInsulationAmountRValue__c wiav5 = new WallInsulationAmountRValue__c();
        wiav5.Name = 'Super';
        wiav5.Value__c = 11;
        wiavList.add(wiav5);
        
        WallInsulationAmountRValue__c wiav6 = new WallInsulationAmountRValue__c();
        wiav6.Name = 'Unknown';
        wiav6.Value__c = 11;
        wiavList.add(wiav6);
        
        insert wiavList;
    
        WeatherStation__c ws = new WeatherStation__c();
        ws.NormalNFactor__c = 10;
        insert ws;
    
        WeatherStationTemprature__c wst = new WeatherStationTemprature__c();
        wst.TempIncr__c = 1;
        wst.TempStart__c = 1;
        wst.TempValues__c = '1,2';
        wst.WeatherStation__c = ws.id;
        wst.WSProperty__c = 'HH';
        insert wst;
        
    /*Safety_Aspect__c sf =new Safety_Aspect__c();      
    insert sf;*/
        
        Energy_Assessment__c ea =new Energy_Assessment__c();
        //ea.Safety_Aspect__c=sf.id;
        ea.Building_Specification__c = bs.id;   
        ea.Building_Shape__c = 'Rectangular';
        ea.WeatherStation__c = ws.id;                        
        ea.Workorder__c = wo.Id;      
        ea.Appointment__c = appt.Id;
        return ea;
    }
   
    public static Building_Specification__c createBuildingSpecification()
    {
        Building_Specification__c bs = new Building_Specification__c();
        bs.Floor_Area__c = 1;
        bs.Orientation__c = 'South';
        bs.Floors__C=1.0;
        bs.Bedromm__c = 1;
        bs.Ceiling_Heights__c = 10;
        //bs.Appointment__c = appt.id;
        bs.Occupants__c = 10;
        bs.Year_Built__c='2018';
        
        return bs;
    }
    
    public static Inspection_Request__c createInspectionReqest(Customer__c cust,Review__c breview){
        
        Inspection_Request__c inspReq = new Inspection_Request__c();
        inspReq.Name = cust.Name;
        inspReq.Review__c = breview.id;
        inspReq.Customer__c = cust.id;
        inspReq.Account__c = cust.Account__c;
        inspReq.Inspection_Type__c = '';
        inspReq.Start_Date__c = Date.today();
        inspReq.Notes_for_Inspector__c = 'Testing';
        return inspReq;
    }
    
    public static Review__c createBillingReview(){
        
         Review__c newrev = new Review__c();
         newrev.Type__c = 'Billing Review';
         newrev.Review_Type__c = 'Billing Review';
         newrev.Status__c = 'Pending Completion Review';
         newRev.RecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Billing Review').getRecordTypeId();
         
         return newRev;
        
    }
    
    public static Building_Specification__c  getBuildingSpecWithAppt()
    {
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;
        
        Workorder__c wo = new Workorder__c();
        wo.Eligibility_Check__c = ec.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();        
        wo.Scheduled_Start_Date__c = datetime.now();
        wo.Scheduled_End_Date__c = datetime.now();         
        insert wo;
        
        Appointment__c appt = new Appointment__c();
        appt.Workorder__c = wo.id;
        appt.Appointment_Start_Time__c = datetime.now();
        appt.Appointment_End_Time__c = datetime.now();
        insert appt;
        
        Building_Specification__c bs = new Building_Specification__c();
        bs.Floor_Area__c = 1;
        bs.Orientation__c = 'South';
        bs.Floors__C=1.0;
        bs.Bedromm__c = 1;
        bs.Ceiling_Heights__c = 10;
        bs.Appointment__c = appt.id;
        bs.Occupants__c = 10;
        bs.Year_Built__c='2018';
        insert bs;
        return bs;
    }
    
    /**
     * Create Mechanical sub type along with EA, Mechanical and Mechanical Type.
     */
    public static Mechanical_Sub_Type__c createMechanicalSubType()
    {
        return createMechanicalSubType(null, null, null);
    }
    
    /**
     * Create Mechanical sub type if EA, Mechanical and Mechanical Type is aleady created.
     */
    public static Mechanical_Sub_Type__c createMechanicalSubType(Energy_Assessment__c ea, Mechanical__c mech, Mechanical_Type__c mt)
    {
        //Create new EA if it is null;
        if(ea == null)
          ea=Datagenerator.setupAssessment();
        
        //create mechanical if null
        if(mech == null)
        {
            mech = new Mechanical__c();
            mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
            mech.Mechanical_Types__c = 'Custom';
            insert mech;    
        }
        
        //create mechanical type if null
        if(mt == null)
        {
            mt = new Mechanical_Type__c();
            mt.Mechanical__c = mech.id;
            mt.Location__c = 'Basement';
            mt.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Mini-Split A/C').getRecordTypeId();
            insert mt;    
        }
        
        
        Id RecordTypeIdMechanicalSub_Type1 = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId(); 
        Mechanical_Sub_Type__c mst=new Mechanical_Sub_Type__c();
        mst.RecordTypeId=RecordTypeIdMechanicalSub_Type1;
        mst.Tank_Gallons__c=24;
        mst.Mechanical__c=mech.Id;
        mst.Total_Leakage__c = 10;
        mst.Fuel_Type__c='Electricity';
        mst.Name = 'test';
        mst.Energy_Assessment__c = ea.id;
        mst.Voltage__c = 'Low';
        mst.Name = 'newRT';   
        mst.Mechanical_Type__c = mt.id;
        insert mst;
        
        return mst;
    }
}