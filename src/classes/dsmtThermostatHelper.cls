public class dsmtThermostatHelper
{
	private static decimal ISNULL(decimal value)
    {
        if(value==null)return 0;
        return value;
    }

	public static decimal ComputeThermostatCoolingSetTempCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if ( bp.ws != null ) 
        {            
            if ( bp.ws.ClgGrDiff__c !=null ) 
            {
                //78 - (MAX (MIN (50, [lookup in table W.Tdes, row=Weather.Site, col = ClgGrDiff]) /10 , 0))
                retval = Math.Round( 78 - Math.Max( Math.Min( 50, bp.ws.ClgGrDiff__c ) / 10, 0 ) );
            }
        }

        return retval;
    }

    public static decimal ComputeThermostatHeatingSetTempCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        
        decimal HDD60 = bp.temp.HDD60;
        //system.debug(' Heating+DHW :: Thermostat : HDD60 : ' + HDD60);
        
        if ( HDD60 !=null ) 
        {
            retval = Math.Max( 66, 74 - HDD60 / 1000 ).setScale(0,roundingmode.HALF_UP);
        }
        
        // CSGENG-4273: reduce ElectricResistance heater thermostat by 3 degrees F.
        if ( bp.RecordTypeMap.get(bp.mechType.RecordTypeId) == 'Electric Resistance' ) 
        {
            retval -= 3;
        }

        return retval;
    }
    
    //ComputeThermostatHeatingSetbackDecayHours
    /// <summary>
    /// Compute Thermostat ActualSBDecayHrs for a Heating System
    /// </summary>
    // Algorithms: = MINIMUM [ Equip(n).ThermostatHoursToReachSBH , Equip(n).ThermostatSBH ]    
    public static decimal ComputeThermostatHeatingSetbackDecayHoursCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;

        if ( bp.mstObj.HeatingSetBackHoursToReach__c !=null && bp.mstObj.HeatingSetBackHours__c!=null) 
        {
            retval = Math.Min( bp.mstObj.HeatingSetBackHoursToReach__c, bp.mstObj.HeatingSetBackHours__c );
        }

        return retval;
    }
    
    // Algorithms: = Equip(n).TstatSetH - Bldg.TempExtH60
    public static decimal ComputeThermostatHeatingDTHouseNominalCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;        

        decimal exteriorTempH60 = bp.temp.ExtTempH60;

        if ( bp.mstObj.HeatingSetTemp__c !=null && exteriorTempH60!=null) 
        {
            retval = Math.Max( bp.mstObj.HeatingSetTemp__c - exteriorTempH60, .01 );
        }
        return retval;
    }
    
    // Algorithms: = -Ln( (Equip(n).TstatSBH - Bldg.TempExtH60) / Equip(n).DTHouseNominalH ) * Bldg.TimeConstH    
    public static decimal ComputeThermostatHeatingSetbackHoursToReachCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        
        decimal exteriorTempH60 = dsmtEAModel.SetScale(bp.temp.ExtTempH60);
        decimal TimeConstH =dsmtEAModel.SetScale(bp.mo.TimeConstH);

        Mechanical_Sub_Type__c thermostat = bp.mstObj;

        //system.debug(' exteriorTempH60 ' + exteriorTempH60 + ' TimeConstH ' + TimeConstH);
        
        if ( exteriorTempH60!=null &&
            thermostat.HeatingSetBackTemp__c !=null &&
            thermostat.HeatingSetBackHours__c!=null &&
            thermostat.HeatingDTHouseNominal__c !=null &&
            thermostat.HeatingDTHouseNominal__c != 0 && TimeConstH !=null ) 
            {

                if ( thermostat.HeatingSetBackHours__c == 0 ) 
                {
                    retval = 0;
                }
                else 
                {
                    decimal logValue = Math.Log((double)(Math.Max( (thermostat.HeatingSetBackTemp__c - exteriorTempH60) / 
                                thermostat.HeatingDTHouseNominal__c, .01 )) );
                    logValue *=-1;

                    //system.debug('logValue >>> ' + logValue );

                    retval = Math.Max( .01,logValue) * TimeConstH;

                    //system.debug(' log value >>> ' + Math.Log( (double)(Math.Max( (thermostat.HeatingSetBackTemp__c - exteriorTempH60) /                                 thermostat.HeatingDTHouseNominal__c, .01 )) ));
                }
            }

        //system.debug(' decay horus ' + retval);
        //retval = 1.4061889;

        return retval;
    }
    
    // Algorithms: = Bldg.TempExtH60 + (Equip(n).DTHouseNominalH * (Bldg.TimeConstH/Equip(n).TstatActualSBDecayHrsH) * (1 - e^(-1 * Equip(n).TstatActualSBDecayHrsH/Bldg.TimeConstH))
    public static decimal ComputeThermostatHeatingSetbackTinDuringDecayCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        decimal exteriorTempH60 =  bp.temp.ExtTempH60;
        decimal TimeConstH =bp.mo.TimeConstH;
        
        if ( exteriorTempH60!=null &&
            thermostat.HeatingDTHouseNominal__c !=null &&
            TimeConstH!=null &&
            thermostat.HeatingSetBackDecayHours__c!=null) 
        {
            //= Bldg.TempExtH60 + 
            retval = exteriorTempH60;

            if ( thermostat.HeatingSetBackDecayHours__c != 0 ) 
            {
                /*
                //(Equip(n).DTHouseNominalH *
                retval += (thermostat.HeatingDTHouseNominal__c *

                //(Bldg.TimeConstH/Equip(n).TstatActualSBDecayHrsH) * 
                (TimeConstH / thermostat.HeatingSetBackDecayHours__c) *

                //(1 - e^(-1 * Equip(n).TstatActualSBDecayHrsH/Bldg.TimeConstH))
                (1 - (decimal)Math.Exp( -1 * (double)(thermostat.HeatingSetBackDecayHours__c / 
                TimeConstH) )));*/

                //TimeConstH = TimeConstH.SetScale(7,roundingmode.HALF_UP);
                decimal heatingSetBackDecayHours = thermostat.HeatingSetBackDecayHours__c.setScale(7,roundingmode.HALF_UP);
                decimal dtHouseNominal = thermostat.HeatingDTHouseNominal__c; //.setScale(7,roundingmode.HALF_UP);

                decimal a = thermostat.HeatingDTHouseNominal__c;
                decimal b = TimeConstH/thermostat.HeatingSetBackDecayHours__c;
                decimal c = 1 - Math.exp(-1 * thermostat.HeatingSetBackDecayHours__c/TimeConstH);

                //system.debug('a >>> ' + a);
                //system.debug('b >>> ' + b);
                //system.debug('c >>> ' + c);
                //system.debug('alternate c ' + (1 - Math.exp(-1 * (double) (thermostat.HeatingSetBackDecayHours__c/TimeConstH))));
                //system.debug('a * b * c >>>' + (a * b * c));
                ////system.debug('K7 * (K8/K9) * (1 - EXP(-1 * K9/K8)) ' + (dtHouseNominal * (K8/K9) * (1 - EXP(-1 * K9/K8))));
                retval = exteriorTempH60 + (a * b * c);
                //retval += (dtHouseNominal * (TimeConstH/heatingSetBackDecayHours) * a);

                //system.debug(' result ' + retval);

            }
            else 
            {
                retval += thermostat.HeatingDTHouseNominal__c;
            }
        }


        return retval;
    }
    
    // Algorithms: = Bldg.TempExtH60 + Equip(n).DTHouseNominalH* e^(-1 * Equip(n).TstatActualSBDecayHrsH/Bldg.TimeConstH)
    public static decimal ComputeThermostatHeatingSetbackTinAtEndCommon( dsmtEAModel.Surface bp)
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        decimal exteriorTempH60 = bp.temp.ExtTempH60;
        decimal TimeConstH =bp.mo.TimeConstH;

        if ( exteriorTempH60 !=null &&
            thermostat.HeatingDTHouseNominal__c !=null &&
            TimeConstH!=null &&
            TimeConstH != 0 &&
            thermostat.HeatingSetBackDecayHours__c !=null ) 
        {
            retval = exteriorTempH60 + dsmtEAModel.SetScale(thermostat.HeatingDTHouseNominal__c) * 
            (decimal)Math.Exp( -1 * (double)(dsmtEAModel.SetScale(thermostat.HeatingSetBackDecayHours__c) / 
            TimeConstH));
        }
        

        return retval;
    }
    
    public static decimal ComputeThermostatHeatingSetBackTempCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if(thermostat.HeatingSetTemp__c !=null)
        {
            retval = thermostat.HeatingSetTemp__c + SurfaceConstants.DefaultHeatingSystemThermostatSetBack - SurfaceConstants.DefaultHeatingSystemThermostatSet;
        }

        return retval;
    }
    
    
    
    // Algorithms:= Bldg.TempExtC + (Equip(n).DTHouseNominalC * (Bldg.TimeConstC/Equip(n).TstatActualSBDecayHrsC) * (1 - e^(-1 * Equip(n).TstatActualSBDecayHrsC/Bldg.TimeConstC))    
    public static decimal ComputeThermostatCoolingSetbackTinDuringDecayCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;

        decimal exteriorTempC74 = bp.temp.ExtTempC74;
        decimal TimeConstC = bp.mo.TimeConstC;
        
        if ( exteriorTempC74 !=null &&
            thermostat.CoolingDTHouseNominal__c !=null &&
            TimeConstC !=null &&
            thermostat.CoolingSetBackDecayHours__c !=null) 
        {
            //Bldg.TempExtC + 
            retval = exteriorTempC74;

            if ( thermostat.CoolingSetBackDecayHours__c != 0 ) 
            {
                //(Equip(n).DTHouseNominalC * 
                retval += (thermostat.CoolingDTHouseNominal__c *

                //(Bldg.TimeConstC/Equip(n).TstatActualSBDecayHrsC) *
                (TimeConstC / thermostat.CoolingSetBackDecayHours__c) *

                //1 - e^(-1 * Equip(n).TstatActualSBDecayHrsC/Bldg.TimeConstC))
                (1 - (decimal)Math.Exp( -1 * (double)(thermostat.CoolingSetBackDecayHours__c / TimeConstC) )));
            }
            else 
            {
                retval += thermostat.CoolingDTHouseNominal__c;
            }
        }

        return retval;
    }
    
    // Algorithms: = Bldg.TempExtC74 + Equip(n).DTHouseNominalC* e^(-1 * Equip(n).TstatActualSBDecayHrsC/Bldg.TimeConstC)    
    public static decimal ComputeThermostatCoolingSetbackTinAtEndCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;        
        decimal TimeConstC = bp.mo.TimeConstC;
        decimal exteriorTempC74 = bp.temp.ExtTempC74;

        if ( exteriorTempC74 !=null &&
            thermostat.CoolingDTHouseNominal__c !=null &&
            TimeConstC !=null &&
            TimeConstC != 0 &&
            thermostat.CoolingSetBackDecayHours__c !=null ) 
        {
            retval = exteriorTempC74 + thermostat.CoolingDTHouseNominal__c * 
            (decimal)Math.Exp( -1 * (double)(thermostat.CoolingSetBackDecayHours__c / 
            TimeConstC));
        }


        return retval;
    }
    
    public static decimal ComputeThermostatCoolingSetBackTempCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        retval = ISNULL(thermostat.CoolingSetTemp__c) + SurfaceConstants.DefaultCoolingSystemThermostatSetBack - SurfaceConstants.DefaultCoolingSystemThermostatSet;

        return retval;
    }
    
    // Algorithms: = MAX (-Ln( MAX ((Equip(n).TstatSBC - Bldg.TempExtC74) / Equip(n).DTHouseNominalC, 0.01) ) * Bldg.TimeConstC, 0.01)
    public static decimal ComputeThermostatCoolingSetbackHoursToReachCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        //dsmLog.add(dsmtSurfaceHelper.AddLogMsg('Saving Calculation','Thermostat Calc.','Computing Cooling Set back hours to reach'));
        //system.debug(' Computing cooling set back hours to reach');
        
        decimal exteriorTempC74 = bp.temp.ExtTempC74;
        decimal TimeConstC = bp.mo.TimeConstC;
        
        //system.debug(' exteriorTempC74 ' + exteriorTempC74 + ' TimeConstC ' + TimeConstC );
        
        if ( exteriorTempC74!=null &&
            thermostat.CoolingSetBackTemp__c!=null &&
            thermostat.CoolingSetBackHours__c !=null &&
            thermostat.CoolingDTHouseNominal__c !=null &&
            TimeConstC !=null &&
            thermostat.CoolingDTHouseNominal__c != 0) 
            {
                if ( thermostat.CoolingSetBackHours__c == 0 ) 
                {
                    retval = 0;
                }
                else 
                {
                    // bulletproof this against the case where the setback is higher than the exteriorTempC and the algorithm starts oscillating
                    retval = Math.Max( .01, -(decimal)Math.Log( (double)(Math.Max( (thermostat.CoolingSetBackTemp__c - exteriorTempC74) / thermostat.CoolingDTHouseNominal__c, .01 )) ) *
                                                        TimeConstC
                                                );
                }
            }
            else 
            {
                retval = thermostat.CoolingSetBackHours__c ;
            } 

        return retval;
    }
    
    // Algorithms: == Equip(n).ThermostatSBHoursC - Equip(n).ThermostatActualSBDecayHrsC    
    public static decimal ComputeThermostatCoolingSetbackHoursRemainderCommon( dsmtEAModel.Surface bp  )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        if ( thermostat.CoolingSetBackHours__c !=null && thermostat.CoolingSetBackDecayHours__c !=null ) 
        {
            if ( thermostat.CoolingSetBackHours__c  == 0 ) 
            {
                retval = 0;
            }
            else 
            {
                retval = thermostat.CoolingSetBackHours__c  - thermostat.CoolingSetBackDecayHours__c;
            }
        }

        return retval;
    }
    
    // Algorithms: = MINIMUM [ Equip(n).ThermostatHoursToReachSBC , Equip(n).ThermostatSBC ]
    public static decimal ComputeThermostatCoolingSetbackDecayHoursCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if ( thermostat.CoolingSetBackHoursToReach__c !=null && thermostat.CoolingSetBackHours__c !=null ) 
        {
            retval = Math.Min( thermostat.CoolingSetBackHoursToReach__c, thermostat.CoolingSetBackHours__c );
        }

        return retval;
    }
    
    
    
    // Algorithms:  = IF [User.Model.ThermostatSetLiteral = "Yes": Equip(n).T.IndoorLiteralC; ELSE (Dft.Bldg.ThermostatSet.C - Equip(n).T.IndoorLiteralC) * (1 - Model.ThermostatUserWeight.C) + Equip(n).T.IndoorLiteralC
    public static decimal ComputeThermostatCoolingIndoorTempUsedCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if ( thermostat.CoolingSetLiteral__c == true ) 
        {
            retval = thermostat.CoolingIndoorTempLiteral__c;
        }
        else {
            if (thermostat.CoolingIndoorTempLiteral__c !=null) 
            {
                retval = (SurfaceConstants.DefaultCoolingSystemThermostatSet - thermostat.CoolingIndoorTempLiteral__c) 
                * (1 - SurfaceConstants.DefaultCoolingSystemThermostatUserWeight) + thermostat.CoolingIndoorTempLiteral__c;
            }
        }

        return retval;
    }
    
    // Algorithms: = (TSetH*(24-TstatSBHoursH)+TstatSBH*TstatSBHoursH)/24    
    public static decimal ComputeThermostatCoolingIndoorTempSimpleCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if ( thermostat.CoolingSetBackHours__c !=null &&
                thermostat.CoolingSetBackHours__c != 0 &&
                thermostat.CoolingSetBackTemp__c !=null && thermostat.CoolingSetTemp__c !=null ) 
        {

            decimal setbackHours = thermostat.CoolingSetBackHours__c;
            decimal hoursToReachSetback = setbackHours / 2; // thermostat.CoolingSetBackHoursToReach.Value recurses, so we can't use it here;  Assume we reach setback in 1/2 the time.
            decimal decayHours = Math.Max( hoursToReachSetback, setbackHours );
            decimal remainingHours = setbackHours - decayHours;
            decimal setTemp = thermostat.CoolingSetTemp__c;
            decimal setbackTemp = thermostat.CoolingSetBackTemp__c;

            retval = ((24 - setbackHours) * setTemp +          // non-setback hours
                decayHours * (setTemp + setbackTemp) / 2 * decayHours / Math.Max( decayHours, hoursToReachSetback ) +  // linear ramp up to setback temp; account for setback is never reached
                remainingHours * setbackTemp) / 24;                // hours after reaching setback
        }
        else {
            retval = thermostat.CoolingSetTemp__c ;
        }

        return retval;
    }
    
    // MIN ((Equip(n).TstatSetC*(24 - Equip(n).TstatActualSBDecayHrsC - Equip(n).TstatHoursRemainC) + 
    //          (Equip(n).TstatActualSBDecayHrsC * Equip(n).TstatTinDuringSBDecayC) + 
    //          (Equip(n).TstatTinAtSBEndC * Equip(n).TstatHoursRemainC) ) / 24 , 
    //      Bldg.TempExtC74 )
    public static decimal ComputeThermostatCoolingIndoorTempLiteralCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        decimal TempC74 = bp.temp.ExtTempC74;
        
        if ( thermostat.CoolingSetTemp__c !=null &&
            thermostat.CoolingSetBackDecayHours__c !=null &&
            thermostat.CoolingSetBackHoursRemainder__c !=null &&
            thermostat.CoolingSetBackTinDuringDecay__c !=null &&
            thermostat.CoolingSetBackTinAtEnd__c !=null &&
            TempC74 !=null) 
        {
            if ( thermostat.CoolingSetBackHours__c == 0 ) 
            {
                retval = thermostat.CoolingSetTemp__c ;
            }
            else 
            {
                decimal temp1 = (
                    (thermostat.CoolingSetTemp__c 
                    * (24 - thermostat.CoolingSetBackDecayHours__c - thermostat.CoolingSetBackHoursRemainder__c)
                    + (thermostat.CoolingSetBackDecayHours__c * thermostat.CoolingSetBackTinDuringDecay__c)
                    + (thermostat.CoolingSetBackTinAtEnd__c * thermostat.CoolingSetBackHoursRemainder__c)
                ) / 24) ;
                if(temp1==null) temp1=0;
                
                retval = Math.Min(temp1 , TempC74);
            }
        }

        return retval;
    }
    
    // Algorithms: = Equip(n).TstatSetH - Bldg.TempExtH60
    public static decimal ComputeThermostatCoolingDTHouseNominalCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c ts = bp.mstObj;
        decimal exteriorTempC74 = bp.temp.ExtTempC74;
        
        if ( ts.CoolingSetTemp__c  !=null && exteriorTempC74 !=null) 
        {
            retval = Math.Min( ts.CoolingSetTemp__c  - exteriorTempC74, -.01 );
        }

        return retval;
    }

    // Algorithms: = Equip(n).ThermostatSBHoursH - Equip(n).ThermostatActualSBDecayHrsH     
    public static decimal ComputeThermostatHeatingSetbackHoursRemainderCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if ( thermostat.HeatingSetBackHours__c!=null && thermostat.HeatingSetBackDecayHours__c!=null ) 
        {
            if ( thermostat.HeatingSetBackHours__c== 0 ) 
            {
                retval = 0;
            }
            else {
                retval = thermostat.HeatingSetBackHours__c- thermostat.HeatingSetBackDecayHours__c;
            }
        }

        return retval;
    }
    
    public static decimal ComputeThermostatHeatingIndoorTempLiteralCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if ( thermostat.HeatingSetTemp__c!=null &&
                thermostat.HeatingSetBackDecayHours__c!=null &&
                thermostat.HeatingSetBackHoursRemainder__c!=null &&
                thermostat.HeatingSetBackTinDuringDecay__c!=null &&
                thermostat.HeatingSetBackTinAtEnd__c!=null &&                   
                bp.temp.ExtTempH60!=null ) 
        {

            if ( thermostat.HeatingSetBackHours__c == 0 ) {
                retval = thermostat.HeatingSetTemp__c;
            }
            else 
            {
                decimal heatingSetTemp = dsmtEAModel.SetScale(thermostat.HeatingSetTemp__c);
                decimal heatingSetBackDecayHours = dsmtEAModel.SetScale(thermostat.HeatingSetBackDecayHours__c);
                decimal heatingSetBackHoursRemainder = dsmtEAModel.SetScale(thermostat.HeatingSetBackHoursRemainder__c);
                decimal heatingSetBackTinDuringDecay = dsmtEAModel.SetScale(thermostat.HeatingSetBackTinDuringDecay__c);
                decimal heatingSetBackTinAtEnd = dsmtEAModel.SetScale(thermostat.HeatingSetBackTinAtEnd__c);
                
                retval = Math.Max( ISNULL (
                             dsmtEAModel.SetScale(heatingSetTemp * (24 - heatingSetBackDecayHours - heatingSetBackHoursRemainder) +
                             (heatingSetBackDecayHours * heatingSetBackTinDuringDecay) +
                             (heatingSetBackTinAtEnd * heatingSetBackHoursRemainder)
                            ) / 24), bp.temp.ExtTempH60);
            }
        }

        return retval;
    }
    
    // Algorithms: = (TSetH*(24-TstatSBHoursH)+TstatSBH*TstatSBHoursH)/24       
    public static decimal ComputeThermostatHeatingIndoorTempSimpleCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
    
        if ( thermostat.HeatingSetBackHours__c !=null &&
                thermostat.HeatingSetBackHours__c != 0 &&
                thermostat.HeatingSetBackTemp__c !=null && thermostat.HeatingSetTemp__c !=null ) 
        {

            decimal setbackHours = thermostat.HeatingSetBackHours__c.setScale(7);
            decimal hoursToReachSetback = setbackHours / 2; // thermostat.HeatingSetBackHoursToReach.Value recurses, so we can't use it here;  Assume we reach setback in 1/2 the time.
            decimal decayHours = Math.Max( hoursToReachSetback, setbackHours );
            decimal remainingHours = setbackHours - decayHours;
            decimal setTemp = thermostat.HeatingSetTemp__c;
            decimal setbackTemp = thermostat.HeatingSetBackTemp__c;

            retval = ((24 - setbackHours) * setTemp +          // non-setback hours
                decayHours * (setTemp + setbackTemp) / 2 * decayHours / Math.Max( decayHours, hoursToReachSetback ) +  // linear ramp up to setback temp; account for setback is never reached
                remainingHours * setbackTemp) / 24;                // hours after reaching setback
        }
        else {
            retval = thermostat.HeatingSetTemp__c ;
        }

        return retval;
    }
    
    public static decimal ComputeThermostatHeatingIndoorTempUsedCommon( dsmtEAModel.Surface bp )
    {
        decimal retval = null;
        Mechanical_Sub_Type__c thermostat = bp.mstObj;
        
        if ( thermostat.HeatingSetLiteral__c == true ) 
        {
            retval = thermostat.HeatingIndoorTempLiteral__c;
        }
        else 
        {
            if (thermostat.HeatingIndoorTempLiteral__c !=null) 
            {
                retval = (SurfaceConstants.DefaultHeatingSystemThermostatSet - thermostat.HeatingIndoorTempLiteral__c) 
                * (1 - SurfaceConstants.DefaultHeatingSystemThermostatUserWeight) + thermostat.HeatingIndoorTempLiteral__c;
            }
        }

        return retval;
    }
}