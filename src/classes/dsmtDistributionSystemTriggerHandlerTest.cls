@isTest
private class dsmtDistributionSystemTriggerHandlerTest {
	
	@isTest static void test_method_one() 
	{
		Energy_Assessment__c testEA=dsmtEAModel.setupAssessment();       

        /// create mechnical records
		Mechanical__c mech = new Mechanical__c();		
		mech.Energy_Assessment__c = testEA.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = testEA.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = testEA.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();
	}
	
	
}