@isTest
Public class dsmtHelperClassTest{
    @isTest
    public static void runTest(){
        Eligibility_Check__c ec = datagenerator.createELCheck();
        ec.Purchase_a_discounted_smart_thermostat__c  = false;
        ec.No_cost_home_energy_assessment__c = true;
        ec.How_many_units__c  = 'Single Family';
        ec.Do_you_own_or_rent__c  = 'Own';
        update ec;
        
        dsmtHelperClass.GetWorkOrderType(ec.Id);
        
        ec.Purchase_a_discounted_smart_thermostat__c  = true;
        ec.No_cost_home_energy_assessment__c = true;
        ec.How_many_units__c  = 'Single Family';
        ec.Do_you_own_or_rent__c  = 'Own';
        update ec;
        
        dsmtHelperClass.GetWorkOrderType(ec.Id);
        
        customer_Eligibility__c  ce = Datagenerator.createCustomerEL(ec.Id);
        
        dsmtHelperClass.GetWorkOrderTypeCE(ce.Id);
    }
}