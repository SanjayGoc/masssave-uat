global class dsmtbatchEmailNotify implements database.Batchable<sobject> {
    
    List<Trade_Ally_Account__c> cancelApplication = new List<Trade_Ally_Account__c>();
    List<Trade_Ally_Account__c> inactiveApplication = new List<Trade_Ally_Account__c>();
    Set<Id> AccId = new Set<Id>();
    global database.QueryLocator start(database.BatchableContext bc){
        //may be this query may need to be on Registration_Request__c.
        if(!Test.isRunningTest()){
            return Database.getQueryLocator('Select Id, Name, LastModifiedDate,Email__c,Owner.Email,Account_Manager__c,Stage__c,Status__c,Account__c from Trade_Ally_Account__c where Status__c = \'Prospecting\' and  LastModifiedDate != LAST_N_DAYS:30  and Stage__c=\'Prospecting\'' );
        }else{
            return Database.getQueryLocator('Select Id, Name, LastModifiedDate,Email__c,Owner.Email,Account_Manager__c,Stage__c,Status__c,Account__c from Trade_Ally_Account__c where Status__c = \'Prospecting\'  and Stage__c=\'Prospecting\'' );
        }
        /*
return Database.getQueryLocator('Select Id, Name,OwnerId, Owner.Email,LastModifiedDate,Email__c,Account_Manager__c,Stage__c,Status__c,Account__c from Trade_Ally_Account__c where Status__c = \'Prospecting\'  and Stage__c=\'Prospecting\' and Id=\'a0w4D0000001URB\'' );
*/
    }
    
    global void execute(database.BatchableContext bc,List<Trade_Ally_Account__c> scope){
        system.debug('=====>> ' + scope);
        for(Trade_Ally_Account__c ta : scope){
            AccId.add(ta.Account__c);
        }
        Contact conId = [Select Id from Contact where AccountId IN :AccId Limit 1];
        
        List<String> to60EmailList = new List<String>();
        List<String> to30EmailList = new List<String>();
        String objType = 'Trade_Ally_Account__c';
        for(Trade_Ally_Account__c ta : scope){
            String fromaddress = ta.Owner.Email;
            DateTime dT = System.now();
            Date currDate = date.newinstance(dT.year(), dT.month(), dT.day());
            Date lastModifiedDate = date.newinstance(ta.LastModifiedDate.year(), ta.LastModifiedDate.month(), ta.LastModifiedDate.day());
            Integer numberDaysDue;
            if(!Test.isRunningTest()){
                numberDaysDue = LastModifiedDate.daysBetween(currDate);
            }else{
                numberDaysDue=70;
            }
            system.debug('>>>>>>>>>>>>> '+numberDaysDue);
            
            //String targetObjectId = '0034D000005RBeC' ;
            String targetObjectId = conId.id;
            if(numberDaysDue>=60){              
                to60EmailList.add(ta.Email__c);             
                
                ta.Status__c = 'Rejected';
                ta.Stage__c  = 'Inactive';
                cancelApplication.add(ta);
                
                
                String emailTemplate = Email_Custom_Setting__c.getorgDefaults().    
                    RR_Cancelled_application_after_60_days__c;
                dsmtRegistrationRequestUtil.sendEmail(targetObjectId,ta,objType ,to60EmailList,new List<String>(),new List<String>(),emailTemplate,null,fromaddress,'Outbound');
                
            } else if(numberDaysDue>=30){
                inactiveApplication.add(ta);
                to30EmailList.add(ta.Email__c);
                
                String emailTemplate = Email_Custom_Setting__c.getorgDefaults().RR_Inactive_application_after_30_days__c;
                dsmtRegistrationRequestUtil.sendEmail(targetObjectId,ta,objType ,to30EmailList,new List<String>(),new List<String>(),emailTemplate,null,fromaddress,'Outbound');
            }
            
            
        }
        if(cancelApplication.size() > 0)
        {
            //update cancelApplication;
            system.debug('>>>>>>>>>>>>> '+cancelApplication);
        }
        system.debug('>>>>>>>>>>>>> '+inactiveApplication);     
        //update scope;
    }
    
    global void finish(Database.BatchableContext BC)
    {
        AsyncApexJob ajob = [select id,status,NumberOfErrors,JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddress = new String[] {'baji.sharief+99@neelblue.com'};
            mail.setToAddresses(toAddress);
        mail.setSubject('dsmtbatchEmailNotify : ' + ajob.Status);
        mail.setPlainTextBody('The batch Apex job dsmtbatchEmailNotify completed');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }  
}