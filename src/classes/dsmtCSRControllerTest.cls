@isTest
Public class dsmtCSRControllerTest{
    @isTest
    public static void runTest(){
        
        Trigger_Configuration__c tc = new Trigger_Configuration__c();
        tc.Deactivate_Customer_Triggers__c = true;
        tc.Deactivate_WorkOrder_Triggers__c = true;
        tc.Deactivate_Appointment_Triggers__c = true;
        tc.Deactivate_WorkTeam_Triggers__c = true;
        tc.Disable_Employee_Trigger__c = true;
        insert tc;
        
        Account  acc= Datagenerator.createAccount();
        insert acc;
        
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        Customer__c cust = Datagenerator.createCustomer(acc.id,null);
        cust.Gas_Account__c = acc.Id;
        insert cust;
        Call_List__c callListObj = new Call_List__c();
        callListObj.Status__c = 'Active';
        insert callListObj;
        
        Lead l = new Lead();
        l.LastName = 'test';
        l.Company = 'test';
        l.Read_date__c = Date.Today()-2;
        insert l;
        
        Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today();
        obj.Call_Notes__c='test<br>test';
        obj.Customer__c=cust.Id;
        insert obj;
        
        Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        obj.Phone_New__c  = '9909240666';
        obj.Followup_Date__c = date.Today()-1;
        insert obj;

        
        Obj = new Call_List_Line_Item__c();
        obj.Call_List__c = callListObj.Id;
        obj.Status__c = 'Ready To Call';
        obj.First_Name_New__c = 'Test';
        obj.Last_Name_New__c = 'Test';
        obj.Next_Followup_date__c  = Date.Today();
        obj.Lead__c = l.Id;
        
        obj.Phone_New__c  = '9909240666';
        insert obj;
        
        Task tsk = new Task();    
        tsk.WhatId = obj.Id;
       // tsk.WhoId = l.Id;
        tsk.CallDisposition = 'Outbound';
        tsk.Status = 'Completed';
        tsk.Subject = ' Call To ';
        tsk.Description = 'test';
        tsk.ActivityDate = Date.Today();
        tsk.Type = 'Call';
        insert tsk;
        
        Note n = new Note();
        n.ParentId = obj.Id;
        n.Title = 'test';
        n.Body = 'test';
        insert n;
        Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        Portfolio__c port = new Portfolio__c();
        port.Account__c= acc.id;
        insert port;
        program__c pg= new program__c();
        pg.Portfolio__c= port.id;
        //pg.GL_String__c = 'External ID';
        insert pg;

        Service_Request_Parameters__c srp = new Service_Request_Parameters__c();
        srp.name= 'test';
        srp.Customer__c=cust.id;
        insert srp;
        Skill__c sk= Datagenerator.createSkill();
        insert sk;
        Workorder__c work= Datagenerator.createWo();
        work.customer__c=cust.id;
        work.Early_Arrival_Time__c = DateTime.newInstance(2018, 1, 1, 10, 30, 0); 
        work.Late_Arrival_Time__c = Datetime.newInstance(2018, 1, 1, 11, 0, 0);
        insert work;

        /* Service_Request__c sr = new Service_Request__c();
        //  sr.Name = 'test';
        insert sr;*/
        
        Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
        insert woTypeList;
        Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
        insert rsk;
        Eligibility_Check__c EL= Datagenerator.createELCheck();
        EL.Workorder_Type__c=woTypeList.Id;
        EL.Service_Address__c='test';
        EL.City__c='test';
        EL.Start_Date__c=date.today().addDays(-5);
        EL.End_Date__c=date.today().addDays(5);
        update EL;
        EL.How_many_units__c='Single family';
        EL.How_many_do_you_own__c  = '1';
        update EL;
        
        List<Lead_Contact_Mapping__c> lstLCM = new List<Lead_Contact_Mapping__c>();
        Lead_Contact_Mapping__c  lcm = new Lead_Contact_Mapping__c ();
        lcm.Call_list_line_item_field__c='test';
        lcm.Call_list_line_item_field_Label__c='test';
        lcm.Call_list_line_item_field_Renamed_Label__c='test';
        lcm.Contact_Field__c='test';
        lcm.Index__c=2;
        lstLCM.add(lcm);
        
        Lead_Contact_Mapping__c  lcm1 = new Lead_Contact_Mapping__c ();
        lcm1.Call_list_line_item_field__c='test1';
        lcm1.Call_list_line_item_field_Label__c='test1';
        lcm1.Call_list_line_item_field_Renamed_Label__c='test1';
        lcm1.Contact_Field__c='test1';
        lcm1.Index__c=3;
        lstLCM.add(lcm1);
        insert lstLCM;
        
        ApexPages.currentPage().getParameters().put('clliId',obj.Id);
        //ApexPages.currentPage().getParameters().put('clId',obj.Id);
        test.startTest();
        dsmtCSRController controller = new dsmtCSRController();
        controller.skipitemids = obj.Id;
        controller.callListLineItemId = obj.Id;
        controller.queryCallScriptLineItem();
        controller.skipCallScriptLineItems();
        controller.updateCallScriptLineItem();
        controller.goToNewContact = true;
        controller.newCallListLineItem  = new Call_List_Line_Item__c();
        controller.newCallListLineItem.Last_Name_New__c = 'test';
        controller.leadContactMappings[0].Call_list_line_item_field__c='test';
        controller.addNewContact();
        controller.ReloadPage();
        dsmtCSRController.SearchModel search = new dsmtCSRController.SearchModel();
        search.searchTypeChanged();
        //search.invokeSearch();
        search.CreateCustomer();
        search.CreateCustomer2();
        search.sObjType.put('First_Name__c','Test');
        search.invokeSearch();
        controller.getHeatingSource();
        controller.getUnits();
        controller.queryRescWo();
        controller.rescduleWorder();
        controller.getFollowupDateOptions();
        String temp = controller.worderColumns;
        list<Schema.FieldSetMember> temp2 = search.sObjTypeSearchCriteriaFields;
        String tempcol = search.searchResultColumns;
        String tempcolCom = search.searchResultColumnsCommercial;
        test.stopTest();
        // updatePinSetting
        dsmtCSRController.updatePinSetting('test','test',true);
        controller.createFieldDataXML();

        controller.getBusinessDays();
        controller.QA();
        controller.ChangeCallList();
        controller.SendEmail();
        controller.convertLead();
        controller.ChangeFollowupType();
        controller.getApartmentList();
        dsmtCSRController.getContractors(pg.id);
        controller.checksForLandlord();
        controller.checkGasUtility();
        controller.checkElectricUtility();
        dsmtCSRController.fetchCustomer(obj.Id);
        controller.customerId=cust.id;
        controller.saveEligibility();
        dsmtCSRController.getContactValues(cust.id);
        dsmtCSRController.checkZipEligibility('1234','','5+ unit','Yes');
        controller.initNewCallListLineItem();
        controller.custId=cust.id;
        controller.customerId = cust.id;
        // controller.createCallList();
        dsmtCSRController.goToServiceRequest(cust.id);
        controller.customerId= cust.id;
        controller.FillItemFromCustomer();
        controller.RefreshcallItem();
        //Added By Sanjay
        controller.SearchCustomer();
        controller.CustNotFound.Electric_Account__c = null;
        controller.CustNotFound.Gas_Account__c = null;
        controller.CreateCustomer2();//error
        //Added By Sanjay
        controller.cancelCustomer();
        controller.callLineItemId=obj.id;
        //controller.queryCallLineItem();//error
        controller.copyHeatingvalues();//error
        controller.worderId=work.id;
        // controller.CancelWorkOrder();web service call
        controller.getEmployee();
        controller.WoTypeId=woTypeList.Id;
        dsmtCSRController.closeServiceRequest(null,'test<br>test');
        //controller.eleCheckId=EL.ID;
        controller.WoTypeId=woTypeList.Id;
     
        controller.custInteraction =EL;
        controller.getAppt();//commented
        //controller.CreateWorkOrder();
        controller.outboundCallListItemCreate();
        controller.callListLineItem=obj;
        controller.acceptOwnership();
        controller.verifyAddress();
        dsmtCSRController.ValidateAddress('test','test','1234');
        
        //  controller.cloneCallScriptLineItem(obj.Id);
        
        dsmtCSRController.HistoryModal historyMod = new dsmtCSRController.HistoryModal('test','test','test','test','test');
        historyMod.getCommentValue();
        historyMod.getcommentTextValue();
        obj.Customer__c = cust.Id;
        //obj.Lead__c = null;
        controller.transferToId = userinfo.getUserId();
        update obj;

        controller.updateCallScriptLineItem();
        controller.callListLineItem = null;
        controller.addNewContact();  
       // controller.addNewLead();
       // controller.addNewContactForCli(); 
        controller.CreateWorkOrder();
        controller.cancelWO = work;
        controller.cancelWO.Cancel_Reason__c = 'test';
        controller.cancelWo.Cancellation_Notes__c = 'test';
        controller.worderId = work.Id;
        controller.CancelWorkOrder(); 
        controller.customerId= '';
       // controller.FillItemFromCustomer();
       // controller.callLineItemId  = obj.Id;
      //  controller.queryCallLineItem();
        
        //controller.createFieldDataXML();
        
        
         
    }
}