@isTest
public class ProjectExceptionstest
{
     public static testmethod void test()
     {
     
          Exception_Template__c et = new Exception_Template__c ();
          et.Automatic__c = true;
          et.Active__c = true;
          et.Project_Level_Message__c = true;
          et.Reference_Id__c ='Project-002';
          insert et;
          
          Building_Specification__c bs = new Building_Specification__c();
          bs.Year_Built__c='';
          insert bs;
          
          /*Energy_Assessment__c ea = new Energy_Assessment__c();
          ea.Building_Specification__c=bs.id;
          insert ea;*/
          
          Recommendation_Scenario__c rs = new Recommendation_Scenario__c();
          //rs.Energy_Assessment__c =ea.id;
          rs.Status__c='Approved For Invoice Submission';
          rs.Signature_Date__c=null;
          insert rs;
         
          Recommendation__c rc = new Recommendation__c(Recommendation_Scenario__c = rs.id);
          insert rc;
                             
          Exception__C ex = new Exception__c();
          ex.Disposition__c='Rejected';
          ex.Project__c= rs.id;    
          ex.Exception_Template__c = et.id;
          insert ex;      
          
          
         
          ProjectExceptions cntrl= new ProjectExceptions();
          et.Reference_Id__c ='Project-003';
          update et;         
        
          ProjectExceptions cntrl1= new ProjectExceptions();
          et.Reference_Id__c ='Project-004';
          update et; 
         
          ProjectExceptions cntrl2= new ProjectExceptions();
          et.Reference_Id__c ='Project-005';
          update et;     
         }
 }