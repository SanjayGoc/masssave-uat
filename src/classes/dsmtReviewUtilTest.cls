@isTest
public class dsmtReviewUtilTest 
{
    testMethod static void testApproveReview()
    {
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        DSMTracker_Contact__c dsmt = new  DSMTracker_Contact__c(name='test',email__c='test@test.com',phone__c='12345',First_Name__c='hemanshu',Last_Name__c='patel',OAP_Profile__c=true);
        dsmt.Trade_Ally_Account__c = ta.Id;
        dsmt.Status__c='Submitted';
        insert dsmt;

        Review__c review = new Review__c();
        review.Review_Type__c='Document';
        review.State__c = 'Approved';
        review.status__c='New';
        review.DSMTracker_Contact__c = dsmt.Id;
        review.Trade_Ally_Account__c =ta.Id;
        insert review;

        Attachment__c at1 = new Attachment__c();
        at1.Status__c='Submitted';       
        insert at1;
        
        Dsmtracker_contact__c cont = new Dsmtracker_contact__c();
        cont.Review__c = review.Id;
        cont.Status__c='Approved';
        insert cont;
        
        Dsmtracker_contact__c cont2 = new Dsmtracker_contact__c();
        cont2.Review__c = review.Id;
        cont2.DSMTracker_Contact__c = cont.Id;
        insert cont2;

        Attachment__c att = new Attachment__c();
        att.Shadow_Attachment__c = at1.Id;
        att.Status__c='Approved';
        att.Review__c = review.Id;
        att.DSMTracker_Contact__c = cont2.Id;
        insert att;
        
        Recommendation_Scenario__c prg = new Recommendation_Scenario__c();
        prg.Type__c='Insulation';
        insert prg;

        Recommendation__c rc =new Recommendation__c();
        rc.Quantity__c=5;
        rc.Recommendation_Scenario__c=prg.id;
        insert rc;

        Project_Review__c pr =new Project_Review__c();
        pr.Project__c=prg.id;
        pr.Review__c = review.Id;
        insert pr;

        Inspection_request__c ins = new Inspection_request__c();
        insert ins;
        
        Recommendation_Scenario__c  rec = new Recommendation_Scenario__c();
        insert rec;
        
        Change_Order_Line_Item__c cha = new Change_Order_Line_Item__c ();
        cha.Review__c = review.Id;
        cha.Change_Order_Review__c =review.id;
        insert cha;
		Test.startTest();
        dsmtReviewUtil.approveReview(review.id);
        dsmtReviewUtil.RejectReview(review.Id);  
        dsmtReviewUtil.getTotalPaymentamount(review.Id);
        dsmtReviewUtil.submitreview(review.Id);
        dsmtReviewUtil.submitreviewForBillingAdjustment(review.Id);
        dsmtReviewUtil.VoidReview(review.Id); 
        dsmtReviewUtil.createPaymentRecord(review.Id); 
        dsmtReviewUtil.createPostInspectionResultReview(ins.Id);
        dsmtReviewUtil.callApprovalProcessForIR(ins.Id);
        dsmtReviewUtil.PreWorkReviewCheck(rec.Id);
        Test.stopTest();
    }
}