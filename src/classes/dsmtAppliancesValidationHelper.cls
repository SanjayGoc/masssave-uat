public class dsmtAppliancesValidationHelper extends dsmtEnergyAssessmentValidationHelper{
    public ValidationCategory validate(String categoryTitle,String uniqueName,Id eaId,ValidationCategory otherValidations){
        ValidationCategory validations= new ValidationCategory(categoryTitle,uniqueName);
        
        Set<String> applianceTypes = new Set<String>{
            'Dehumidifier','Dishwasher','Dryer','Game Console','Other','Oven','Range','Refrigeration','Sump Pump','TV','Washer','Well Pump'
        };
        
        if(eaId != null){
            Integer i = 0,refrigeratorCount = 0,primaryRefrigeratorCount = 0;
            for(Appliance__c apl : Database.query('SELECT ' + getSObjectFields('Appliance__c') + ' FROM Appliance__c WHERE Energy_Assessment__c=:eaId')){
                i++;
                if(apl.Category__c != null && apl.Category__c.trim() != ''){
                    ValidationCategory aplValidations = new ValidationCategory(apl.Category__c + ' > ' + apl.Name,apl.Id);
                    if(apl.Type__c != null && apl.Type__c.trim() != '' && applianceTypes.contains(apl.Type__c)){
                        if(apl.Year__c == null || String.valueOf(apl.Year__c).trim() == '' || apl.Year__c <= 0 || String.valueOf(apl.Year__c).trim().length() < 4){
                            aplValidations.errors.add(new ValidationMessage('Year requires an explicit value'));
                        }else if(apl.Year__c != null && Integer.valueOf(apl.Year__c) > 0 && Integer.valueOf(apl.Year__c) > (System.Today().year() + 1)){
                            aplValidations.errors.add(new ValidationMessage('Year must be less than or equal to '+(System.Today().year() + 1)));
                        }
                    }
                    if(apl.Type__c.equalsIgnoreCase('Washer')){
                        validateWasherAppliance(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Dryer')){
                        validateDryerAppliance(aplValidations,apl,othervalidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Dishwasher')){
                        validateDishwasherAppliance(aplValidations,apl,othervalidations);
                    }else if(apl.Type__c == 'Range'){
                        validateRangeAppliance(aplValidations,apl,othervalidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Refrigeration')){
                        validateRefrigeratorAppliance(aplValidations,apl,othervalidations);
                        refrigeratorCount++;
                        if(apl.Primary_Refrigerator__c == true){
                            primaryRefrigeratorCount++;
                        }
                    }else if(apl.Type__c.equalsIgnoreCase('Cooktop')){
                        validateCooktopAppliance(aplValidations,apl,othervalidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Oven')){
                        validateOvenAppliance(aplValidations,apl,othervalidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Dehumidifier')){
                        validateDehumidifier(aplValidations,apl,othervalidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Aquarium')){
                        validateAquarium(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Water Bed Heater')){
                        validateWaterBedHeater(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Well Pump')){
                        validateWellPump(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Sump Pump')){
                        validateSumpPump(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Holiday Lights')){
                        validateHolidayLights(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('TV')){
                        validateTV(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Game Console')){
                        validateGameConsole(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('Other')){
                        validateOther(aplValidations,apl,otherValidations);
                    }else if(apl.Type__c.equalsIgnoreCase('General')){
                        validateGeneral(aplValidations,apl,otherValidations);
                    }
                        
                    if(apl.Year__c != null && Integer.valueOf(apl.Year__c) > 0 && Integer.valueOf(apl.Year__c) < (System.Today().year() - 10) ){
                        aplValidations.warnings.add(new ValidationMessage('The appliance that you have entered is more than 10 years old. It should be assessed for replacement possibility.'));
                    }
                    validations.addCategory(aplValidations);
                }
            } 
            if(primaryRefrigeratorCount == 0 && RefrigeratorCount == 1){
                validations.errors.add(new ValidationMessage('If there is only 1 refrigerator, it should be marked as Primary'));
            }else if(RefrigeratorCount > 1 && (primaryRefrigeratorCount > 1 || primaryRefrigeratorCount == 0 )){
                validations.errors.add(new ValidationMessage('Exactly 1 of the {0} refrigerators should be marked as Primary'.replace('{0}',String.valueOf(RefrigeratorCount))));
            } 
        }
        
        return validations;
    }
    
    private void validateGeneral(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Usage__c== null || apl.Usage__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Usage is required'));
        }
    }
    
    private void validateOther(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Fuel_Type__c == null || apl.Fuel_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Fuel Type is required'));
        }
    }
    
    private void validateHomeOffice(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Usage__c== null || apl.Usage__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Usage is required'));
        }
        if(apl.Desktop_PCs__c != null && apl.Desktop_PCs__c > 10){
            validations.errors.add(new ValidationMessage('Desktop PCs must be less than equal to 10'));
        }
        if(apl.Notebook_PCs__c != null && apl.Notebook_PCs__c > 10){
            validations.errors.add(new ValidationMessage('Notebook PCs must be less than equal to 10'));
        }
        if(apl.Other_Equipment__c != null && apl.Other_Equipment__c > 10){
            validations.errors.add(new ValidationMessage('Other Equipments must be less than equal to 10'));
        }
        if(apl.Routers__c != null && apl.Routers__c > 10){
            validations.errors.add(new ValidationMessage('Routers must be less than equal to 10'));
        }
    }
    
    private void validateGameConsole(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Usage__c== null || apl.Usage__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Usage is required'));
        }
    }
    
    private void validateTV(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Usage__c== null || apl.Usage__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Usage is required'));
        }
        if(apl.Quantity__c != null && apl.Quantity__c > 10){
            validations.errors.add(new ValidationMessage('Quantity must be less than equal to 10'));
        }
        if(apl.Diagonal__c == null || apl.Quantity__c < 10){
            validations.errors.add(new ValidationMessage('Quantity must be less than equal to 10'));
        }else if(apl.Diagonal__c != null && apl.Quantity__c > 120){
            validations.errors.add(new ValidationMessage('Diagonal must be less than equal to 120'));
        }
        if(apl.SetTop_Box_Quantity__c != null && apl.SetTop_Box_Quantity__c > 10){
            validations.errors.add(new ValidationMessage('Settop box must be less than equal to 10'));
        }
        if(apl.Number_of_DVRs__c != null && apl.Number_of_DVRs__c > 10){
            validations.errors.add(new ValidationMessage('Number of DVRs must be less than equal to 10'));
        }
    }
    
    private void validateHolidayLights(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Fraction_of_LED_Lights__c != null && apl.Fraction_of_LED_Lights__c > 1){
            validations.errors.add(new ValidationMessage('Fraction of LED Lights must be less than equal to 1'));
        }
    }
    
    private void validateSumpPump(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
    }
    
    private void validateWellPump(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
    }
    
    private void validateWaterBedHeater(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
    }
    
    private void validateAquarium(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
    }
    
    private void validateDehumidifier(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Usage__c== null || apl.Usage__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Usage is required'));
        }
        if(apl.Dehumidifier_Type__c == null || apl.Dehumidifier_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Dehumidifier Type is required'));
        }
    }
    
    private void validateOvenAppliance(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Usage__c== null || apl.Usage__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Usage is required'));
        }
        if(apl.Fuel_Type__c == null || apl.Fuel_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Fuel Type is required'));
        }else if(apl.Fuel_Type__c != null && apl.Fuel_Type__c != 'Electricity' && (apl.Pilot_Type__c == null || apl.Pilot_Type__c.trim() == '')){
            validations.errors.add(new ValidationMessage('Pilot type is required'));
        }
    }
    
    private void validateCooktopAppliance(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Usage__c== null || apl.Usage__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Usage is required'));
        }
        if(apl.Fuel_Type__c == null || apl.Fuel_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Fuel Type is required'));
        }else if(apl.Fuel_Type__c != null && apl.Fuel_Type__c != 'Electricity' && (apl.Pilot_Type__c == null || apl.Pilot_Type__c.trim() == '')){
            validations.errors.add(new ValidationMessage('Pilot type is required'));
        }
    }
    
    private void validateRefrigeratorAppliance(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        Map<String,String> reqFieldsMap = new Map<String,String>{
            'Location__c' => 'Location',
            'Refrigerator_Type__c' => 'Refrigerator Type',
            'Refrigerator_Style__c'=> 'Refrigerator Style',
            'Defrost_Type__c' => 'Defrost Type',
            'Volume__c' => 'Volume (cu. ft.)',
            'Rated_kWh__c' => 'Rated kWh'
        };
        if(apl.Refrigerator_Type__c != null && apl.Refrigerator_Type__c == 'Refrigerator Only'){
            reqFieldsMap.remove('Refrigerator_Style__c');
        }
        validateRequiredFields(apl,reqFieldsMap,validations);
        
        if(apl.Volume__c != null && apl.Volume__c < 2){
            validations.errors.add(new ValidationMessage('Volume (cu. ft.) must be greater than euqal to 2'));
        }else if(apl.Volume__c != null && apl.Volume__c > 35){
            validations.errors.add(new ValidationMessage('Volume (cu. ft.) must be less than euqal to 35'));
        }
        
        if((apl.Refrigerator_Type__c == 'Refrigerator Only' && apl.Ice_in_Door__c == true) ||
            (apl.Refrigerator_Type__c == 'Refrigerator' && apl.Refrigerator_Style__c == 'Single Door' && apl.Ice_in_Door__c == true) || 
            (apl.Refrigerator_Type__c == 'Freezer' && apl.Refrigerator_Style__c == 'Chest' && apl.Ice_in_Door__c == true )){
            validations.errors.add(new ValidationMessage('Ice In Door  is not a valid option for {0} refrigerators'.replace('{0}',apl.Refrigerator_Type__c)));
        }
        
        if(apl.Metering_Start_Time__c != null && apl.Metering_Stop_Time__c != null && apl.Metering_Stop_Time__c < apl.Metering_Start_Time__c){
            validations.errors.add(new ValidationMessage('Refrigerator meetering stop time must be later than start time'));
        }
    }
    
    private void validateRangeAppliance(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Fuel_Type__c == null || apl.Fuel_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Fuel Type is required'));
        }else if(apl.Fuel_Type__c != null && apl.Fuel_Type__c != 'Electricity' && (apl.Pilot_Type__c == null || apl.Pilot_Type__c.trim() == '')){
            validations.errors.add(new ValidationMessage('Pilot type is required'));
        }
    }
    
    private void validateDishwasherAppliance(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Dishwasher_Type__c == null || apl.Dishwasher_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Dishwasher type is required'));
        }
        if(apl.Dishwasher_Loads_Per_Week__c == null || String.valueOf(apl.Dishwasher_Loads_Per_Week__c).trim() == '' || apl.Dishwasher_Loads_Per_Week__c > 20){
            validations.errors.add(new ValidationMessage('Dishwasher Loads/ Week must be less than or equal to 20'));
        }
        if(apl.Dishwasher_Energy_Factor__c == null || String.valueOf(apl.Dishwasher_Energy_Factor__c).trim() == '' || apl.Dishwasher_Energy_Factor__c < 0.2){
            validations.errors.add(new ValidationMessage('Energy factor must be greater than equal to 0.2'));
        }else if(apl.Dishwasher_Energy_Factor__c != null && apl.Dishwasher_Energy_Factor__c > 2){
            validations.errors.add(new ValidationMessage('Energy factor must be less than equal to 2'));
        }
    }
    
    private void validateDryerAppliance(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Indoor_HangLoads_Week__c != null && apl.Indoor_HangLoads_Week__c > 30){
            validations.errors.add(new ValidationMessage('Indoor hang drying (Loads/Week) must be less than or equal to 30.'));
        }
        if(apl.Dryer_Loads_Per_Week__c != null && apl.Dryer_Loads_Per_Week__c > 30){
            validations.errors.add(new ValidationMessage('Dryer loads (Loads/Week) must be less than or equal to 30'));
        }
        if(apl.Location__c == null || apl.Location__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Location is required'));
        }
        if(apl.Fuel_Type__c == null || apl.Fuel_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Fuel Type is required'));
        }else if(apl.Fuel_Type__c != null && apl.Fuel_Type__c != 'Electricity' && (apl.Pilot_Type__c == null || apl.Pilot_Type__c.trim() == '')){
            validations.errors.add(new ValidationMessage('Pilot type is required'));
        }
    }
    
    private void validateWasherAppliance(ValidationCategory validations,Appliance__c apl,ValidationCategory otherValidations){
        if(apl.Hot_Loads_Week__c == null){
            validations.errors.add(new ValidationMessage('Hot Loads/Week is required'));
        }else if(apl.Hot_Loads_Week__c > 30 ){
            validations.errors.add(new ValidationMessage('Loads/Week (Hot) must be less than or equal to 30.'));
        }
        
        if(apl.Warm_Loads_Week__c == null){
            validations.errors.add(new ValidationMessage('Warm Loads/Week is required'));
        }else if(apl.Warm_Loads_Week__c > 30 ){
            validations.errors.add(new ValidationMessage('Loads/Week (Warm) must be less than or equal to 30.'));
        }
        
        if(apl.Cold_Loads_Week__c == null){
            validations.errors.add(new ValidationMessage('Cold Loads/Week is required'));
        }else if(apl.Cold_Loads_Week__c > 30 ){
            validations.errors.add(new ValidationMessage('Loads/Week (Cold) must be less than or equal to 30.'));
        }
        
        if(apl.Capacity__c == null || String.valueOf(apl.Capacity__c).trim() == ''){
            validations.errors.add(new ValidationMessage('Washer capacity requires an explicit value.'));
        }else if(apl.Capacity__c < 1.5){
            validations.errors.add(new ValidationMessage('Washer capacity must be greater than or equal to 1.5'));
        }else if(apl.Capacity__c > 5){
            validations.errors.add(new ValidationMessage('Washer capacity must be less than or equal to 5.'));
        }
        
        if(apl.Water_Factor__c == null || String.valueOf(apl.Water_Factor__c).trim() == ''){
            validations.errors.add(new ValidationMessage('Water factor requires an explicit value.'));
        }else if(apl.Water_Factor__c < 2){
            validations.errors.add(new ValidationMessage('Water factor must be greater than or equal to 2'));
        }else if(apl.Water_Factor__c > 20){
            validations.errors.add(new ValidationMessage('Water factor must be less than or equal to 20.'));
        }
        
        if(apl.Remaining_Moisture__c == null || String.valueOf(apl.Remaining_Moisture__c).trim() == ''){
            validations.errors.add(new ValidationMessage('Remaining moisture content requires an explicit value.'));
        }else if(apl.Remaining_Moisture__c < 0.2){
            validations.errors.add(new ValidationMessage('Remaining moisture content must be greater than or equal to 0.2'));
        }else if(apl.Remaining_Moisture__c > 0.8){
            validations.errors.add(new ValidationMessage('Remaining moisture content must be less than or equal to 0.8.'));
        }
        
        if(apl.MEF__c == null || String.valueOf(apl.MEF__c).trim() == ''){
            validations.errors.add(new ValidationMessage('Modified energy factor requires an explicit value.'));
        }else if(apl.MEF__c < 0.5){
            validations.errors.add(new ValidationMessage('Modified energy factor must be greater than or equal to 0.5'));
        }else if(apl.MEF__c > 3.5){
            validations.errors.add(new ValidationMessage('Modified energy factor must be less than or equal to 3.5'));
        }
        
        if(apl.Washer_Type__c == null || apl.Washer_Type__c.trim() == ''){
            validations.errors.add(new ValidationMessage('Washer type is required'));
        }
    }
}