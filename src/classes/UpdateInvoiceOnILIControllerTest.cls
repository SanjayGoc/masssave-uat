@isTest
public class UpdateInvoiceOnILIControllerTest
{
    public testmethod static void test1()
    {
        Invoice__c inv = new Invoice__c();
        insert inv;
        
        Invoice_Line_Item__c ili = new Invoice_Line_Item__c();
        ili.Invoice__c=inv.id;
        insert ili;
        
         ApexPages.currentPage().getParameters().put('id',inv.id);
         
         UpdateInvoiceOnILIController cntrl = new UpdateInvoiceOnILIController();
         cntrl.iliIds=ili.id+'~~~';
         cntrl.saveILI();
    }
}