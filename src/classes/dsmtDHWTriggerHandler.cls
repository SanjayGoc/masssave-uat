public class dsmtDHWTriggerHandler
{
    public static boolean forRecommendation = false;

    public static void calculateDefaultValues(list<Mechanical_Sub_Type__c> newList, Map<Id, Mechanical_Sub_Type__c> oldMap, dsmtEAModel.Surface bp, boolean isInsert)
    {
        updateDHWSystem(bp, isInsert);
    }

    private static void ManageActuals(Mechanical_Sub_Type__c mst)
    {
        if(mst.ActualTankTemp__c <=0) mst.ActualTankTemp__c=null;
        if(mst.ActualCAE__c <=0)mst.ActualCAE__c =null;
        if(mst.ActualPipeInsulation__c <=0) mst.ActualPipeInsulation__c =null;
        if(mst.ActualBTUsPerHour__c <=0)mst.ActualBTUsPerHour__c =null;
        if(mst.ActualVolume__c <=0)mst.ActualVolume__c =null;
        if(mst.ActualLoadFraction__c <=0)mst.ActualLoadFraction__c =null;
        if(mst.ActualTankTemp__c <=0)mst.ActualTankTemp__c=null;
        if(mst.ActualEnergyFactor__c<=0)mst.ActualEnergyFactor__c=null;
        if(mst.ActualRecoveryEfficiency__c<=0)mst.ActualRecoveryEfficiency__c=null;
        if(mst.ActualDHWVentType__c=='') mst.ActualDHWVentType__c = null;
        if(mst.ActualPilotType__c =='') mst.ActualPilotType__c =null;
    }
    
    public static void updateDHWSystem(dsmtEAModel.Surface bp, boolean isInsert)
    {
        Mechanical_Sub_Type__c mst = bp.mstObj;

        ManageActuals(mst);

        mst.Efficiency__c = dsmtDHWHelper.ComputeDHWEfficiencyCommon(bp);

        bp.Efficiency = mst.Efficiency__c;
        bp.Location = mst.Location__c;
        bp.LocationType = mst.LocationType__c;
        bp.LocationSpace = mst.LocationSpace__c;
        bp.Load = mst.Load__c;
        bp.FuelType = mst.Fuel_Type__c;
        bp.FuelUnits = mst.FuelUnits__c;
        bp.EnergyConsumptionH = mst.EnergyConsumptionH__c;
        bp.EnergyConsumptionC = mst.EnergyConsumptionC__c;
        bp.EnergyConsumptionSh = mst.EnergyConsumptionSh__c;
        
        mst.DHWType__c = bp.recordTypeMap.get(mst.RecordTypeId);
        ////System.debug('HOT_WATER_REVIEW : mst.DHWType__c :: ' + mst.DHWType__c);
         
        mst.CanHavePilot__c = dsmtDHWHelper.ComputeDHWCanHavePilotCommon(bp);
        
        if(mst.ActualPilotType__c ==null)
            mst.Pilot_Type__c = dsmtDHWHelper.ComputeDHWPilotTypeCommon(bp);
         
        if(mst.ActualYear__c ==null)
            mst.Year__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectYearCommon(bp);


        //#Code Block Start
		//DSST-10104
        Mechanical_Sub_Type__c hs =null;
		if(mst.DHWType__c == 'Combo DHW' || mst.DHWType__c == 'Tankless Coil')
		{
			hs = dsmtMechanicalHelper.GetMechanicalHeatingSystem(bp);
			mst.ActualYear__c = hs.ActualYear__c;
			if(hs.ActualYear__c !=null)
				mst.Year__c = hs.ActualYear__c;
		}
		//#Code Block End

        ///DSST -10714
        /// Add more condition to make sure tankless coil location is calculated
        if(mst.Location__c ==null || (hs!=null && hs.Location__c != mst.Location__c))
            mst.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);

        /*
        //TEMPORARY CODE FOR Heat Pump and On Demand ------------------
        if(mst.DHWType__c.equalsIgnoreCase('Heat Pump')){
            mst.Location__c = 'Basement';
        }else if(mst.DHWType__c.equalsIgnoreCase('On Demand')){
            mst.Location__c = 'Living Space';
        }
        //----------------------------------------------*/

        if(mst.Fuel_Type__c==null || 
            mst.DHWType__c == 'Indirect Storage Tank' ||
            mst.DHWType__c == 'Tankless Coil' ||
            mst.DHWType__c == 'Combo DHW')
        {
            mst.Fuel_Type__c = dsmtEnergyConsumptionHelper.ComputeFuelTypeCommon(bp);
        }

        /// Invoice correction DHW measures
        /// Rohit Sharma
        if(mst.DHWType__c == 'Combo DHW') mst.DHWType__c = 'Combo';

        bp.FuelType = mst.Fuel_Type__c;

        mst.FuelUnits__c = dsmtEnergyConsumptionHelper.LookupBuildingProfileObjectFuelUnitsCommon(bp);
        
        bp.FuelUnits = mst.FuelUnits__c;
        
        mst.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
        
        bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
        bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
        bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

        mst.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
        bp.LocationType = mst.LocationType__c;
        
        mst.HasInsulatedIndirectConnectPipes__c = dsmtDHWHelper.ComputeDHWHasInsulatedIndirectConnectPipesCommon(bp);
        mst.Has_Purge_Control__c = dsmtDHWHelper.ComputeDHWHasPurgeControlCommon();
        mst.MixTemp__c = dsmtDHWHelper.ComputeDHWMixTempCommon(bp);

        mst.RegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');

        mst.RegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');

        mst.ExpectedUsefuleLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedUsefulLifeCommon('Thermostat');

        mst.ExpectedRemainingLife__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectExpectedRemainingLifeCommon(mst.ExpectedUsefuleLife__c, mst.Year__c);
        
        mst.TRoomC__c = dsmtDHWHelper.ComputeDHWTRoomCCommon(bp);
        mst.TRoomH__c = dsmtDHWHelper.ComputeDHWTRoomHCommon(bp);
        mst.TRoomSh__c = dsmtDHWHelper.ComputeDHWTRoomShCommon(bp);

        mst.CanHavePilot__c = dsmtDHWHelper.ComputeDHWCanHavePilotCommon(bp);

        if(mst.ActualCAE__c ==null)
            mst.CAE__c = SurfaceConstants.DefaultCAE;

        // There are some field which only defaut to when insert
        if(isInsert)
        {
             mst.Primary_DHW__c = dsmtDHWHelper.ComputeIsPrimaryDHWCommon(bp);
             mst.Collector_Area__c = 64;
        }

        if(mst.ActualPipeInsulation__c ==null)
            mst.Pipe_Insulation__c =0;

        if(mst.ActualBTUsPerHour__c ==null)
        {
            mst.BTUsPerHour__c = dsmtDHWHelper.ComputeDHWBTUsPerHourCommon(bp); 
            mst.Input_Kw__c = dsmtEAModel.SetScale(dsmtEAModel.ISNULL(mst.BTUsPerHour__c) * 0.0002931);
            mst.Input_kBtu_h__c = dsmtEAModel.SetScale(dsmtEAModel.ISNULL(mst.BTUsPerHour__c) * .001);
        }
        else {
            if(mst.Fuel_Type__c !='Electricity')
            {
                mst.BTUsPerHour__c = dsmtEAModel.SetScale(mst.ActualBTUsPerHour__c / .001);
            }
            else {
                mst.BTUsPerHour__c = dsmtEAModel.SetScale(mst.ActualBTUsPerHour__c / 0.0002931);
            }                
        }
        
        mst.ASHL__c = dsmtDHWHelper.ComputeASHLCommon(bp);

        if(mst.ActualVolume__c ==null)
            mst.Volume__c = dsmtDHWHelper.ComputeDHWVolumeCommon(bp);

        mst.HourRating__c = dsmtDHWHelper.ComputeDHWHourRatingCommon(bp);

        /// DSST-7257 Move to  here
        mst.StandbyLoss__c = dsmtDHWHelper.ComputeDHWStandbyLossCommon(bp);

        if(mst.ActualLoadFraction__c ==null)
            mst.LoadFraction__c = dsmtDHWHelper.ComputeDHWLoadFractionCommon(bp);

        if(mst.ActualDHWVentType__c ==null)
            mst.DHWVentType__c = dsmtDHWHelper.ComputeDHWVentTypeCommon(bp); 

        if(mst.ActualTankTemp__c==null)
            mst.TankTemp__c =dsmtDHWHelper.ComputeDHWTankTempCommon(bp);
        
        //mst.Tank_Wrap__c =dsmtDHWHelper.ComputeDHWTankWrapCommon(bp);
        
        mst.TColdWaterC__c = dsmtDHWHelper.ComputeDHWTColdWaterCCommon(bp);
        mst.TColdWaterH__c = dsmtDHWHelper.ComputeDHWTColdWaterHCommon(bp);
        mst.TColdWaterSh__c = dsmtDHWHelper.ComputeDHWTColdWaterShCommon(bp);
        mst.TColdWaterAvg__c = dsmtDHWHelper.ComputeDHWTColdWaterAvgCommon(bp);

        if(mst.ActualRecoveryEfficiency__c==null)
            mst.Recovery_Efficiency__c = dsmtDHWHelper.ComputeDHWRecoveryEfficiencyCommon(bp);
        
        if(!forRecommendation)
            dsmtBuildingModelHelper.UpdateBuildingProfileForDHWInsert(bp);
        
        mst.ShowerAdjustedC__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'C','Shower');
        mst.ShowerAdjustedH__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'H','Shower');
        mst.ShowerAdjustedSh__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'Sh','Shower');
        
        mst.SinkAdjustedC__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'C','Sink');
        mst.SinkAdjustedH__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'H','Sink');
        mst.SinkAdjustedSh__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'Sh','Sink');
        
        mst.BathAdjustedC__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'C','Bath');
        mst.BathAdjustedH__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'H','Bath');
        mst.BathAdjustedSh__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'Sh','Bath');
        
        mst.HandDishwashingAdjustedC__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'C','Hand Dishwasher');
        mst.HandDishwashingAdjustedH__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'H','Hand Dishwasher');
        mst.HandDishwashingAdjustedSh__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'Sh','Hand Dishwasher');

        mst.WasherAdjustedC__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'C','Washer');
        mst.WasherAdjustedH__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'H','Washer');
        mst.WasherAdjustedSh__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'Sh','Washer');         

        mst.DishwasherAdjustedC__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'C','Dishwasher');
        mst.DishwasherAdjustedH__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'H','Dishwasher');
        mst.DishwasherAdjustedSh__c = dsmtDHWHelper.ComputeDHWWaterFixtureAdjustedCommon(bp,'Sh','Dishwasher');
        
        mst.GallonsPerDayC__c = dsmtDHWHelper.ComputeDHWGallonsPerDayCCommon(bp); //33.758065;//
        mst.GallonsPerDayH__c = dsmtDHWHelper.ComputeDHWGallonsPerDayHCommon(bp); //44.283751;//
        mst.GallonsPerDaySh__c = dsmtDHWHelper.ComputeDHWGallonsPerDayShCommon(bp); //38.9780112; //
        
        if(mst.ActualEnergyFactor__c==null)
            mst.Energy_Factor__c = dsmtDHWHelper.ComputeDHWEnergyFactorCommon(bp); //.5920373;// 
        
        mst.HPWHIntLoadAdjC__c = dsmtDHWHelper.ComputeDHWHPWHIntLoadAdjCCommon(bp);
        mst.HPWHIntLoadAdjH__c = dsmtDHWHelper.ComputeDHWHPWHIntLoadAdjHCommon(bp);
        
        mst.NormalizedLoadFraction__c = dsmtDHWHelper.ComputeDHWNormalizedLoadFractionCommon(bp);
        mst.UA__c = dsmtDHWHelper.ComputeDHWUACommon(bp);
        mst.UAWrapDiff__c = dsmtDHWHelper.ComputeDHWUAWrapDiffCommon(bp);
        mst.UAAdj__c = dsmtDHWHelper.ComputeDHWUAAdjCommon(bp);

        mst.AdjustC__c = dsmtDHWHelper.ComputeDHWAdjustCCommon(bp);
        mst.AdjustH__c = dsmtDHWHelper.ComputeDHWAdjustHCommon(bp);
        mst.AdjustSh__c = dsmtDHWHelper.ComputeDHWAdjustShCommon(bp);

        mst.EnergyUseC__c = dsmtDHWHelper.ComputeDHWEnergyUseCCommon(bp);
        mst.EnergyUseH__c = dsmtDHWHelper.ComputeDHWEnergyUseHCommon(bp);
        mst.EnergyUseSh__c = dsmtDHWHelper.ComputeDHWEnergyUseShCommon(bp);
        mst.EnergyUse__c = dsmtDHWHelper.ComputeDHWEnergyUseCommon(bp);

        mst.StandbyDailyC__c = dsmtDHWHelper.ComputeDHWStandbyDailyCCommon(bp);
        mst.StandbyDailyH__c = dsmtDHWHelper.ComputeDHWStandbyDailyHCommon(bp);
        mst.StandbyDailySh__c = dsmtDHWHelper.ComputeDHWStandbyDailyShCommon(bp);
        
        mst.StandbyUseC__c = dsmtDHWHelper.ComputeDHWStandbyUseCCommon(bp);
        mst.StandbyUseH__c = dsmtDHWHelper.ComputeDHWStandbyUseHCommon(bp);
        mst.StandbyUseSh__c = dsmtDHWHelper.ComputeDHWStandbyUseShCommon(bp);
        mst.StandbyUse__c = dsmtDHWHelper.ComputeDHWStandbyUseCommon(bp);
        
        mst.StandbyUseAdjC__c = dsmtDHWHelper.ComputeDHWStandbyUseAdjCCommon(bp);
        mst.StandbyUseAdjH__c = dsmtDHWHelper.ComputeDHWStandbyUseAdjHCommon(bp);
        mst.StandbyUseAdjSh__c = dsmtDHWHelper.ComputeDHWStandbyUseAdjShCommon(bp);
        mst.StandbyUseAdj__c = (mst.StandbyUseAdjC__c != null ? mst.StandbyUseAdjC__c : 0) +
                               (mst.StandbyUseAdjH__c != null ? mst.StandbyUseAdjH__c : 0) +
                               (mst.StandbyUseAdjSh__c != null ? mst.StandbyUseAdjSh__c : 0);
        
        
        mst.Load__c = dsmtDHWHelper.ComputeDHWLoadCommon(bp);
        
        bp.Load = mst.Load__c;
        bp.FuelType = mst.Fuel_Type__c;
        bp.FuelUnits = mst.FuelUnits__c;
                
        mst.EnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'H');
        mst.EnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'C');
        mst.EnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'Sh');

        mst.AnnualEnergyConsumption__c = bp.AnnualEnergyConsumption;
        mst.AnnualFuelConsumption__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualFuelConsumptionCommon(bp);
        
        mst.IndoorGainPlugH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp, 'H');
        mst.IndoorGainPlugC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp, 'C');
        mst.IndoorGainPlugCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp, 'CLat');
        
        mst.IndoorGainGasH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainGasCommon(bp, 'H');
        mst.IndoorGainGasC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainGasCommon(bp, 'C');
        mst.IndoorGainGasCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainGasCommon(bp, 'CLat');
        
        mst.IndoorGainOtherH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainOtherCommon(bp, 'H');
        mst.IndoorGainOtherC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainOtherCommon(bp, 'C');
        mst.IndoorGainOtherCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainOtherCommon(bp, 'CLat');
        
        mst.IndoorGainHWH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainHWCommon(bp, 'H');
        mst.IndoorGainHWC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainHWCommon(bp, 'C');
        mst.IndoorGainHWCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainHWCommon(bp, 'CLat');
        
        mst.IndoorGainH__c = (mst.IndoorGainPlugH__c != null ? mst.IndoorGainPlugH__c : 0) +
                           (mst.IndoorGainGasH__c != null ? mst.IndoorGainGasH__c : 0) +
                           (mst.IndoorGainOtherH__c != null ? mst.IndoorGainOtherH__c : 0) +
                           (mst.IndoorGainHWH__c != null ? mst.IndoorGainHWH__c : 0);
                           
        mst.IndoorGainC__c = (mst.IndoorGainPlugC__c != null ? mst.IndoorGainPlugC__c : 0) +
                           (mst.IndoorGainGasC__c != null ? mst.IndoorGainGasC__c : 0) +
                           (mst.IndoorGainOtherC__c != null ? mst.IndoorGainOtherC__c : 0) +
                           (mst.IndoorGainHWC__c != null ? mst.IndoorGainHWC__c : 0);
        
        mst.IndoorGainCLat__c = (mst.IndoorGainPlugCLat__c != null ? mst.IndoorGainPlugCLat__c : 0) +
                              (mst.IndoorGainGasCLat__c != null ? mst.IndoorGainGasCLat__c : 0) +
                              (mst.IndoorGainOtherCLat__c != null ? mst.IndoorGainOtherCLat__c : 0) +
                              (mst.IndoorGainHWCLat__c != null ? mst.IndoorGainHWCLat__c : 0);

        ////System.debug('mst.FuelUnits__c ' + mst.FuelUnits__c);

        mst.FuelConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp,'H');
        mst.FuelConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp,'C');
        mst.FuelConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp,'Sh');

        if(!forRecommendation)
        {
            Date dt = Date.today();
            bp.FuelUsage = dsmtEnergyConsumptionHelper.GetFuelUsageList(bp, dt);
           
            mst.AnnualOperatingCost__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualOperatingCostCommon(bp);
        }
    }
}