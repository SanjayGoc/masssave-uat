public class AtticWallComponentCntrl {
    public Id atticWallId{get;set;}
    public String newLayerType{get;set;}
    public Wall__c atticWall{get{
        if(atticWall == null){
            List<Wall__c> walls = Database.query('SELECT ' + getSObjectFields('Wall__c') + ',Thermal_Envelope_Type__r.SpaceConditioning__c FROM Wall__c WHERE Id=:atticWallId'); 
            if(walls.size() > 0){
                atticWall = walls.get(0);
            }
        }
        return atticWall;
    }set;}
    public void saveWall(){
        System.debug('-------------------------- SAVING WALL ------------------------------------');
        saveWallLayers();
        if(atticWall != null && atticWall.Id != null){
            UPDATE atticWall;
            atticWall = null;
        }
    }
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    public List<Layer__c> wallLayers{get;set;}
    public Map<Id,Layer__c> getWallLayersMap(){
        Id wallId = atticWall.Id;
        System.debug('wallId :: ' + wallId);
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Wall__c =:wallId AND HideLayer__c = false ';
        System.debug('@@QUERY' + query);
        wallLayers = Database.query(query);
        return new Map<Id,Layer__c>(wallLayers);
    }
    public void saveWallLayers(){
        if(wallLayers != null && wallLayers.size() > 0){
            UPSERT wallLayers;
        }
    }
    public void addNewAtticWallLayer(){
        String layerType = newLayerType;// ApexPages.currentPage().getParameters().get('layerType');
        System.debug('addNewAtticWallLayer.layerType :: ' + layerType);
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        if(layerType != null && m.containsKey(layerType)){
            
            Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
            List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Wall__c =:atticWall.Id AND RecordTypeId =:recordTypeId];
            
            Decimal nextNumber= layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Wall__c = atticWall.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            System.debug('newLayer :: ' + newLayer);
            if(newLayer.Id != null){
                atticWall.Next_Attic_Wall_Layer_Number__c = nextNumber + 1;
                UPDATE atticWall;
            }
            wallLayers.add(newLayer);
        }
        System.debug('wallLayers :: ' + wallLayers);
    }
    public void deleteWallLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = wallLayers.get(index).Id);
        wallLayers.remove(index);
        saveWall();
    }
    
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}