@isTest 
private class UpdateCustomerAITest 
{
    static testMethod void Method1() {
        
        Customer__c cust = new Customer__c();
        cust.Name = 'test';
        cust.First_Name__c = 'test1';
        cust.Last_Name__c = 'test2';
        cust.Gas_Provider_Name__c ='National Grid Gas';
        cust.Electric_Provider_Name__c = 'National Grid Electric';
        cust.Service_Address__c = 'test1';
        cust.Service_State__c = 'test3';
        cust.Service_Zipcode__c = '60001';
        cust.Primary_Email__c ='test@test.com';
        cust.Electric_Account_Number__c = '123654122';
        cust.Gas_Account_Number__c = '56982332';
        insert cust;
        
        DSMTracker_Contact__c dsmt = new DSMTracker_Contact__c();
        insert dsmt;
       
        Appointment__c app = new Appointment__c();
        app.Customer_Reference__c = cust.id;
        app.Appointment_Status__c = 'Scheduled';
        app.Employee__c = null;
        app.DSMTracker_Contact__c = dsmt.id;
        app.Is_Dataload__c = false;
        app.Appointment_Start_Time__c = DateTime.now();
        insert app;
        
        Workorder_Type__c woType = new Workorder_Type__c();
        woType.Qualifying_Audit__c = true;
        insert woType;
        
        Workorder__c wo = Datagenerator.Createwo();
        wo.Workorder_Type__c = woType.id;
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();
        insert wo;
        
        app.DSMTracker_Contact__c = null;
        app.workorder__c = wo.id;
        update app;
    }
    
}