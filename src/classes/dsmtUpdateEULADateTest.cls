@isTest
Public class dsmtUpdateEULADateTest
{
    @isTest
    public static void runTest()
    {
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User tuser = new User(  firstname = 'tuserFname',
                            lastName = 'tuserLastname',
                            email = 'tuser@test.org',
                            IsActive=false,
                            ProfileId=p.id,
                            MobilePhone='1234567890',
                            Send_SMS_Notification__c=true,
                            Username = 'tuse234234223334234r@test.org',
                            EmailEncodingKey = 'ISO-8859-1',
                            Alias ='tuser',
                            TimeZoneSidKey = 'America/Los_Angeles',
                            LocaleSidKey = 'en_US',
                            LanguageLocaleKey = 'en_US'
                            );
        insert tuser;      
        tuser.IsActive=true;
        update tuser;
        
    }
}