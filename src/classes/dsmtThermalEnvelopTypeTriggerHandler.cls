public class dsmtThermalEnvelopTypeTriggerHandler extends TriggerHandler implements ITrigger 
{
    public void bulkBefore()
    {
        if(Trigger.isInsert)
        {
          CalculateThermalEnvelopeType(Trigger.new, (Map<Id,Thermal_Envelope_Type__c>) Trigger.oldMap, true);
        }
        else if(Trigger.isUpdate)
        {
           CalculateThermalEnvelopeType(Trigger.new, (Map<Id,Thermal_Envelope_Type__c>) Trigger.oldMap, false);
        }
        else if(Trigger.isDelete)
        {
           deleteLayers(trigger.oldMap.keyset());
        }
    }

    public void bulkAfter() 
    {
        if (trigger.isInsert) 
        {
            CreateDefaultLayerAndChild(Trigger.new);    

        } else if (trigger.isUpdate) {
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }

    public static void CreateDefaultLayerAndChild(List<Thermal_Envelope_Type__c> teList)
    {
        //system.debug('after insert call');
        Map<Id,string> recTypeMap = dsmtHelperClass.GetRecordType();

        dsmtEAModel.Surface bp = new dsmtEAModel.Surface();        
        bp.recordTypeMap = recTypeMap;

        for(Thermal_Envelope_Type__c tet: teList)
        {
            //system.debug('tet>>>'+tet.id);
            //system.debug('tet>>>'+tet.IsFromFieldTool__c);
            if(tet.IsFromFieldTool__c) continue;

            bp.teType = tet;
            String Type = recTypeMap.get(tet.RecordTypeId);

            //system.debug('type>>>'+type);
            
            if(dsmtFuture.ThermalEnvTypeforChildRecord == false)
            {                
                //dsmtSurfaceHelper.CreateNewLayers(bp);
                if(Type == 'Attic' && !tet.Change_Order__c)
                {
                    dsmtFuture.CreateFloorfuture(tet.Id,Type);
                    dsmtFuture.CreateCeilingfuture(tet.Id,Type);
                    dsmtFuture.CreateWallfuture(tet.Id,Type);
                    //dsmtFuture.CreateAtticVentingFuture(tet.Id,Type);
                }
                else if(Type == 'Basement' || Type == 'Crawlspace')
                {
                    dsmtFuture.CreateCeilingfuture(tet.Id,Type);
                    dsmtFuture.CreateFloorfuture(tet.Id,Type);
                    dsmtFuture.CreateWallfuture(tet.Id,Type);
                }
                else if(Type == 'Windows & Skylights')
                {
                   dsmtFuture.Createwindowset(tet.Thermal_Envelope__c,tet.Id);
                }
            }
            
            if(dsmtEAModel.GetWallRecordTypes().contains(type) ||
            dsmtEAModel.GetFloorCeilingRecordType().contains(type))
            {
                //system.debug('creating default layers');
                dsmtSurfaceHelper.CreateNewLayers(bp);
            }
        }
    }

    public static void CalculateThermalEnvelopeType(List<Thermal_Envelope_Type__c> teTypeList, Map<Id,Thermal_Envelope_Type__c> oldMap, boolean isInsert)
    {
        Set<Id> EaId = new Set<Id>();
        integer mobileRecCount =0;

        for(Thermal_Envelope_Type__c te : teTypeList)
        {
            if(te.IsFromFieldTool__c) mobileRecCount +=1;

            if(te.Energy_Assessment__c !=null)
                EaId.add(te.Energy_Assessment__c);
        }

        if(EaId ==null || EaId.size()==0) return;
        if(mobileRecCount == teTypeList.size()) return;

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(EaId);

        if(bp==null) return;

        // Assign current record to list to make sure all value in list also get updated
        for(Thermal_Envelope_Type__c te : teTypeList)
        {
            if(isInsert)
            {
                if(bp.telist ==null) bp.telist = new List<Thermal_Envelope_Type__c>();
                bp.telist.add(te);
            }
            else 
            {
                if(bp.telist==null || bp.telist.size()==0)
                {
                    bp.telist = new List<Thermal_Envelope_Type__c>();
                    bp.telist.add(te);
                }
                else {

                    integer idx =0;
                    for(Thermal_Envelope_Type__c t : bp.telist)
                    {
                        if(t.Id==te.Id)
                        {
                            bp.telist[idx]=te;
                            break;
                        }

                        idx++;
                    }
                }
            }
        }

        // consutruct temprature and building model
        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        /// update base model first
       // bp = dsmtBuildingModelHelper.UpdateBaseBuildingThermal(bp);
        bp = dsmtBuildingModelHelper.UpdateThermalAreaModel(bp);
        bp.ai = dsmtBuildingModelHelper.UpdateAirFlowModel(bp.ai, bp).clone(true,true,true,true);

        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        for(Thermal_Envelope_Type__c tet: teTypeList)
        {
            if(tet.IsFromFieldTool__c) continue;
            
            string recTypeName = '';
            if(bp.recordTypeMap.get(tet.RecordTypeId) != null)
                recTypeName = bp.recordTypeMap.get(tet.RecordTypeId);

            if(recTypeName==null || recTypeName== '') continue;
                
            bp.teType = tet;            

            bp.layers =null;

            if(tet.Id !=null)
            {
                if(bp.allLayerMap !=null && bp.allLayerMap.size()>0)
                {
                    bp.layers = bp.allLayerMap.get(tet.Id);
                }
            }

            bp.layers = dsmtBuildingModelHelper.UpdateLayerModel(bp.layers,bp);

            integer builtYear =0;
            if(bp.bs.Year_Built__c != null && bp.bs.Year_Built__c !='') 
            {
               builtYear = integer.valueof(bp.bs.Year_Built__c);
            }

            ManageActuals(tet);

            tet.Year__c = dsmtBuildingModelHelper.ComputeBuildingProfileObjectYearCommon(bp);

            /// There are some common field that need to update every time
            tet.ChangeType__c = false;
           
            tet.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);

            bp.Location = tet.Location__c;
            tet.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
            bp.LocationSpace = tet.LocationSpace__c;
            bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
            bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
            bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

            tet.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
            bp.LocationType = tet.LocationType__c;

             // its a wall record type
            if(dsmtEAModel.GetWallRecordTypes().contains(recTypeName))
            {
                UpdateWallRecord(bp,isInsert,false);                
            }
            else if(dsmtEAModel.GetFloorCeilingRecordType().contains(recTypeName))
            {
                UpdateFloorCeilingRecord(bp,isInsert);
            }
            else if(recTypeName == 'Door')
            {
                UpdateDoorRecord(bp, tet);
            }
            else if(recTypeName == 'Attic')
            {
                UpdateAtticRecord(bp, isInsert);
            }
            else if(recTypeName == 'Basement' || recTypeName == 'Crawlspace' )
            {
                UpdateFoundationRecord(bp, isInsert);
            }
            else if(recTypeName == 'Windows & Skylights')
            {
                tet.FrontWindowQualitativeAmount__c = dsmtSurfaceHelper.ComputeWindowAmountCommon(bp,'Front');
                tet.BackWindowQualitativeAmount__c = dsmtSurfaceHelper.ComputeWindowAmountCommon(bp,'Back');
                tet.LeftWindowQualitativeAmount__c = dsmtSurfaceHelper.ComputeWindowAmountCommon(bp,'Left');
                tet.RightWindowQualitativeAmount__c = dsmtSurfaceHelper.ComputeWindowAmountCommon(bp,'Right');
                
                tet.FrontWindowFraction__c = dsmtSurfaceHelper.ComputeWindowFractionCommon(bp,'Front');
                tet.BackWindowFraction__c = dsmtSurfaceHelper.ComputeWindowFractionCommon(bp,'Back');    
                tet.RightWindowFraction__c = dsmtSurfaceHelper.ComputeWindowFractionCommon(bp,'Right');    
                tet.LeftWindowFraction__c = dsmtSurfaceHelper.ComputeWindowFractionCommon(bp,'Left');
                
                tet.FrontWindowAreaSQFT__c = dsmtSurfaceHelper.ComputeWindowAreaByFace(bp,'Front');
                tet.BackWindowAreaSQFT__c = dsmtSurfaceHelper.ComputeWindowAreaByFace(bp,'Back');    
                tet.LeftWindowAreaSQFT__c = dsmtSurfaceHelper.ComputeWindowAreaByFace(bp,'Left');    
                tet.RightWindowAreaSQFT__c = dsmtSurfaceHelper.ComputeWindowAreaByFace(bp,'Right');    
            }

            ///DSST-14896
            /// Make sure the inslation amount will be null if its none
            if(tet.Insul_Amount__c!=null && tet.Insul_Amount__c.equalsIgnoreCase('none'))
            {
                tet.Insul_Amount__c = '';
            }
        }
    }

    public static void ManageActuals(Thermal_Envelope_Type__c tet)
    {
        if(tet.Actual_Area__c <=0) tet.Actual_Area__c=null;
        if(tet.ActualSlope__c <=0) tet.ActualSlope__c=null;
        if(tet.ActualProjectedArea__c <=0) tet.ActualProjectedArea__c=null;
        if(tet.ActualSlope__c <=0) tet.ActualSlope__c=null;
        if(tet.ActualPerimeter__c <=0)tet.ActualPerimeter__c=null;
        if(tet.ActualHeight__c <=0)tet.ActualHeight__c=null;
        if(tet.ActualWidth__c <=0)tet.ActualWidth__c=null;
        if(tet.ActualInsulationAmount__c == '') tet.ActualInsulationAmount__c = null;
    }

    public static void UpdateFloorCeilingRecord(dsmtEAModel.Surface bp, boolean isInsert)
    {
        Thermal_Envelope_Type__c tet = bp.teType;
        string recTypeName = '';
        if(bp.recordTypeMap.get(tet.RecordTypeId) != null)
            recTypeName = bp.recordTypeMap.get(tet.RecordTypeId);

        tet.Exposed_To__c = dsmtSurfaceHelper.GetSurfaceExposedArea().get(recTypeName);
        tet.Space_Type__c = dsmtSurfaceHelper.GetSurfaceSpaceType().get(recTypeName);
        tet.FloorType__c = dsmtSurfaceHelper.ComputeFloorTypeCommon(bp);
        tet.CeilingType__c = dsmtSurfaceHelper.ComputeCeilingTypeCommon(bp);
        tet.FloorCeilingType__c = dsmtSurfaceHelper.ComputeFloorCeilingTypeCommon(bp);        
        tet.SpaceConditioning__c = dsmtSurfaceHelper.ComputeSpaceConditioningCommon(bp);                                        
        tet.Height__c = 0;                    
        tet.Width__c = dsmtEAModel.SetScale(bp.EaObj.FrontLength__c);
        tet.Length__c = dsmtSurfaceHelper.ComputeFloorCeilingLengthCommon(bp);

        // DSST-6348 - Cavity Insulation Depth error
        // #Code Block Start
        if(bp.layers !=null && bp.layers.size() > 0)
        {
            Layer__c wfLayer = dsmtSurfaceHelper.GetFirstFramingLayer(bp.layers,1);
            if(wfLayer !=null)
            {
                if(wfLayer.ActualCavityInsulationDepth__c !=null && wfLayer.ActualCavityInsulationDepth__c !=0)
                {
                    tet.ActualInsulationAmount__c = null;
                }
            }
        }
        
        if(tet.ActualInsulationAmount__c == null)
        {
            tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
        }
        // #Code Block End

        if(Trigger.isInsert)
        {
            //tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
            tet.Gaps__c = Saving_Constant__c.getOrgDefaults().DefaultInsulationGaps__c;
            tet.Reflectivity__c = dsmtSurfaceHelper.ComputeFloorCeilingReflectivityCommon(tet.FloorType__c);
            tet.Venting_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingVentingAmountCommon(bp);
        }
        
        decimal slope=0;
        if(recTypeName == 'Flat Roof')  
        {
            slope =0;        
        }
        else {
            slope = dsmtSurfaceHelper.ComputeFloorCeilingSlopeCommon(bp);  
        }
        
        if(tet.ActualSlope__c == null)
            tet.Slope__c = slope;
        
        if(tet.Actual_Area__c == null)
        {
            tet.Exterior_Area__c = dsmtSurfaceHelper.ComputeSurfaceAreaCommon(bp);
        }

        //DSST-15257 (Shift this calculation after area calculation)
        // Rohit Sharma
        if(tet.ActualPerimeter__c == null)
            tet.Perimeter__c = dsmtSurfaceHelper.ComputeFloorCeilingPerimeterCommon(bp);
        
        if(tet.ActualProjectedArea__c==null)
        {
            tet.Projected_Area__c = dsmtSurfaceHelper.ComputeFloorCeilingProjectedAreaCommon(bp);
        }                                    
        
        tet.ReradiationFactor__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofReradiationFactorCommon(bp);
        tet.ReflectivityFactor__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofReflectivityFactorCommon(tet.Reflectivity__c);
        tet.RoofVentCFMNat__c =dsmtSurfaceHelper.ComputeFloorCeilingRoofVentCFMNatCommon(bp);
        tet.RoofVentCFMH__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofVentCFMHCommon(bp);
        tet.SolarGainTempAddH__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofSolarGainTempAddCommon(bp,'H');
        tet.SolarGainTempAddC__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofSolarGainTempAddCommon(bp,'C');
        tet.TSolairH__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofTSolairCommon(bp,'H');
        tet.TSolairC__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofTSolairCommon(bp,'C');
        tet.SlabInsulationDepth__c = dsmtSurfaceHelper.ComputeFloorCeilingSlabInsulationDepthCommon(bp);
        tet.SlabInsulationDepthFactor__c  = dsmtSurfaceHelper.ComputeFloorCeilingSlabInsulationDepthFactorCommon(bp);

        tet.Net_Area__c = dsmtSurfaceHelper.ComputeAreaNet(bp);
        tet.RValueConst__c = dsmtSurfaceHelper.ComputeRValueConst(bp);
        tet.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(bp);  
        tet.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveRValueCommon(bp);   
        tet.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveNominalRValueCommon(bp); 
        tet.UA__c = dsmtSurfaceHelper.ComputeFloorCeilingUACommon(bp); 
        tet.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(tet.R_Value__c);
        tet.SlabRValNetLin__c = dsmtSurfaceHelper.ComputeFloorCeilingSlabRValNetLinCommon(bp);
        tet.SlabUALin__c = dsmtSurfaceHelper.ComputeFloorCeilingSlabUALinCommon(bp);
        if(tet.Venting_Amount__c == null || tet.Venting_Amount__c == '') 
           tet.Venting_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingVentingAmountCommon(bp);
       
        tet.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'H');
        tet.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'C');
        tet.UAEffH__c = dsmtSurfaceHelper.ComputeFloorCeilingUAEffCommon(bp, 'H');
        tet.UAEffC__c = dsmtSurfaceHelper.ComputeFloorCeilingUAEffCommon(bp, 'C');
        tet.LoadH__c = dsmtSurfaceHelper.ComputeFloorCeilingLoadCommon(bp, 'H');
        tet.LoadC__c = dsmtSurfaceHelper.ComputeFloorCeilingLoadCommon(bp, 'C');
    }

    public static void UpdateAtticRecord(dsmtEAModel.surface bp, boolean isInsert)
    {
        Thermal_Envelope_Type__c tet = bp.teType;
        if(isInsert)
        {
            tet.Trusses__c = Saving_Constant__c.GetOrgDefaults().DefaultAtticHasTrusses__c;    
            tet.SpaceConditioning__c = 'Unheated/Vented';
        }
        
    }

     public static void UpdateFoundationRecord(dsmtEAModel.surface bp, boolean isInsert)
    {
        Thermal_Envelope_Type__c tet = bp.teType;        
        string recTypeName =  bp.recordTypeMap.get(tet.RecordTypeId);
        if(isInsert)
        {
            tet.SpaceConditioning__c = 'Unconditioned';
            
            if(bp.recordTypeMap.get(tet.RecordTypeId) == 'Basement')
            {
                tet.Finished__c = Saving_Constant__c.GetOrgDefaults().DefaultBasementIsFinished__c;
                tet.Ceiling_Height__c = bp.bs.Ceiling_Heights__c;
            }
            else if(bp.recordTypeMap.get(tet.RecordTypeId) == 'Crawlspace')
            {
                tet.Ceiling_Height__c =Saving_Constant__c.GetOrgDefaults().DefaultCrawlspaceHeight__c;
            }
        }
        else 
        {
            Set<Id> keySet = bp.recordTypeMap.keySet();

            if(recTypeName == 'Basement') 
            {   
                /// DSST -9092
                if(bp.bs.Ceiling_Heights__c != tet.Ceiling_Height__c)
                //if(tet.Ceiling_Height__c == null || tet.Ceiling_Height__c ==0)
                {
                    tet.Ceiling_Height__c = bp.bs.Ceiling_Heights__c;
                }
                else if(tet.ceiling_Height__c <= 5)
                {
                    for(Id i:keySet)
                    {
                        if(bp.recordTypeMap.get(i)=='Crawlspace')
                        {
                            tet.RecordTypeId=i;
                            tet.ChangeType__c = true;
                            tet.Name = bp.recordTypeMap.get(i);
                            break;
                        }
                    }
                }
            }
            
            if(recTypeName == 'Crawlspace') 
            {
                if(tet.Ceiling_Height__c == null || tet.Ceiling_Height__c ==0)
                {
                    tet.Ceiling_Height__c =Saving_Constant__c.GetOrgDefaults().DefaultCrawlspaceHeight__c;
                }
                else if(tet.ceiling_Height__c > 5)
                {
                    for(Id i:keySet)
                    {
                        if(bp.recordTypeMap.get(i)=='Basement')
                        {
                            tet.RecordTypeId=i;
                            tet.ChangeType__c = true;
                            tet.Name = bp.recordTypeMap.get(i);
                            break;
                        }
                    }
                }
            }
        }
        
    }

    public static dsmtEAModel.Surface UpdateFloorForRecommendation(dsmtEAModel.Surface bp)
    {
        // First Clone the curernt bp to new to update wall
        dsmtEAModel.surface newBP = dsmtEAModel.CloneSurfaceModel(bp);

        // call temprature
        newBP.temp = dsmtTempratureHelper.ComputeTemprature(newBP);
        newBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(newBP);

        /// clone tet from old bp
        newBP.teType = bp.teType.clone(true,true,true,true);
        if(bp.floor !=null)
        {
            newBP.floor = bp.floor.clone(true,true,true,true);
        }

        integer idx=0;
        Id floor = bp.teType.Id;
        if(newBP.floor !=null)
            floor = bp.floor.Id;

        /// Update exterior wall ref in thermal list        
        if(bp.floor ==null)
        {
            if(newBP.telist !=null)
            {
                idx =0;
                For(Thermal_Envelope_Type__c t : newBP.telist)
                {
                    if(t.Id== newBP.tetype.Id)
                    {
                        newBP.telist[idx]= newBP.tetype;
                        break;
                    }
                    idx++;
                }
            }
        }
        else
        {
            if(newBP.floorList !=null)
            {
                idx =0;
                For(Floor__c t : newBP.floorList)
                {
                    if(t.Id== newBP.floor.Id)
                    {
                        newBP.floorList[idx]= newBP.floor;
                        break;
                    }
                    idx++;
                }
            }    
        }
        
        if(dsmtEAModel.GetFloorCeilingRecordType().contains(newBP.recordTypeMap.get(newBP.teType.RecordTypeId)))
        {
            newBP.teType.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(newBP);  
            newBP.teType.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveRValueCommon(newBP);   
            newBP.teType.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveNominalRValueCommon(newBP); 
            newBP.teType.UA__c = dsmtSurfaceHelper.ComputeFloorCeilingUACommon(newBP); 
            newBP.teType.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon( newBP.teType.R_Value__c);
            
            if(newBP.teType.Venting_Amount__c == null || newBP.teType.Venting_Amount__c == '') 
               newBP.teType.Venting_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingVentingAmountCommon(newBP);
           
            newBP.teType.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'H');
            newBP.teType.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'C');
        }
        else 
        {
            newBP.floor.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(newBP);  
            newBP.floor.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveRValueCommon(newBP);   
            newBP.floor.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveNominalRValueCommon(newBP); 
            newBP.floor.UA__c = dsmtSurfaceHelper.ComputeFloorCeilingUACommon(newBP); 
            newBP.floor.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(newBP.floor.R_Value__c);
            
            newBP.floor.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'H');
            newBP.floor.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'C');
        }

        newBP = dsmtBuildingModelHelper.UpdateBuildingMechanicalApplianceLoadModel(newBP);        
        newBP = dsmtBuildingModelHelper.UpdateBuildingLoadModel(newBP);

        newBP = dsmtBuildingModelHelper.UpdateBuildingMechanicalApplianceLoadModel(newBP);        
        newBP = dsmtBuildingModelHelper.UpdateBuildingLoadModel(newBP);

        //system.debug(' recommended model ' + JSON.serialize(newBP));
        
        return newBP;

    }

    public static dsmtEAModel.Surface UpdateCeilingForRecommendation(dsmtEAModel.Surface bp)
    {
        // First Clone the curernt bp to new to update wall
        dsmtEAModel.surface newBP = dsmtEAModel.CloneSurfaceModel(bp);

        // call temprature
        newBP.temp = dsmtTempratureHelper.ComputeTemprature(newBP);
        newBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(newBP);

        /// clone tet from old bp
        newBP.teType = bp.teType.clone(true,true,true,true);
        if(bp.ceiling !=null)
        {
            newBP.ceiling = bp.ceiling.clone(true,true,true,true);
        }

        integer idx=0;
        Id ceiling = bp.teType.Id;
        if(newBP.ceiling !=null)
            ceiling = bp.ceiling.Id;

        /// Update exterior wall ref in thermal list        
        if(bp.ceiling ==null)
        {
            if(newBP.telist !=null)
            {
                idx =0;
                For(Thermal_Envelope_Type__c t : newBP.telist)
                {
                    if(t.Id== newBP.tetype.Id)
                    {
                        newBP.telist[idx]= newBP.tetype;
                        break;
                    }
                    idx++;
                }
            }
        }
        else
        {
            if(newBP.ceilList !=null)
            {
                idx =0;
                For(Ceiling__c t : newBP.ceilList)
                {
                    if(t.Id== newBP.ceiling.Id)
                    {
                        newBP.ceilList[idx]= newBP.ceiling;
                        break;
                    }
                    idx++;
                }
            }    
        }
        
        if(dsmtEAModel.GetFloorCeilingRecordType().contains(newBP.recordTypeMap.get(newBP.teType.RecordTypeId)))
        {
            newBP.teType.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(newBP);  
            newBP.teType.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveRValueCommon(newBP);   
            newBP.teType.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveNominalRValueCommon(newBP); 
            newBP.teType.UA__c = dsmtSurfaceHelper.ComputeFloorCeilingUACommon(newBP); 
            newBP.teType.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon( newBP.teType.R_Value__c);
            
            if(newBP.teType.Venting_Amount__c == null || newBP.teType.Venting_Amount__c == '') 
               newBP.teType.Venting_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingVentingAmountCommon(newBP);
           
            newBP.teType.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'H');
            newBP.teType.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'C');
        }
        else 
        {
            newBP.ceiling.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(newBP);  
            newBP.ceiling.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveRValueCommon(newBP);   
            newBP.ceiling.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveNominalRValueCommon(newBP); 
            newBP.ceiling.UA__c = dsmtSurfaceHelper.ComputeFloorCeilingUACommon(newBP); 
            newBP.ceiling.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(newBP.floor.R_Value__c);
            
            newBP.ceiling.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'H');
            newBP.ceiling.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(newBP, null, 'C');
        }
        

        newBP = dsmtBuildingModelHelper.UpdateBuildingMechanicalApplianceLoadModel(newBP);        
        newBP = dsmtBuildingModelHelper.UpdateBuildingLoadModel(newBP);

        newBP = dsmtBuildingModelHelper.UpdateBuildingMechanicalApplianceLoadModel(newBP);        
        newBP = dsmtBuildingModelHelper.UpdateBuildingLoadModel(newBP);

        //newBP.temp = dsmtTempratureHelper.ComputeTemprature(newBP);
        //newBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(newBP);

        //system.debug(' recommended model ' + JSON.serialize(newBP));

        /// After UA Update update current thermostate and recalculate temp and mo        
        //dsmtEAModel.UpdateThermostateModel(newBP);       
        //////////////////////////////////////////

        //newBP = dsmtBuildingModelHelper.UpdateBuildingLoadModel(newBP);
        
        return newBP;

    }

    public static dsmtEAModel.Surface UpdateWallForRecommendation(dsmtEAModel.Surface bp)
    {
        // First Clone the curernt bp to new to update wall
        //system.debug(' before recommended model ' + JSON.serialize(bp));
        dsmtEAModel.surface newBP = dsmtEAModel.CloneSurfaceModel(bp);
        //system.debug(' after recommended model ' + JSON.serialize(newBP));

        // call temprature
        newBP.temp = dsmtTempratureHelper.ComputeTemprature(newBP);
        newBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(newBP);

        /// clone tet from old bp
        newBP.teType = bp.teType.clone(true,true,true,true);
        if(bp.wall !=null)
        {
            newBP.wall = bp.wall.clone(true,true,true,true);
        }

        integer idx=0;
        Id wall = bp.teType.Id;
        if(newBP.wall !=null)
            wall = bp.wall.Id;

        /// Update  wall ref in thermal list        
        if(bp.wall ==null)
        {
            List<Thermal_Envelope_Type__c> teList = newBP.teList;
            if(telist !=null)
            {
                idx =0;
                For(integer i=0, j= telist.size(); i < j; i++)
                {
                    Thermal_Envelope_Type__c t = teList[i];
                    if(t.Id== newBP.tetype.Id)
                    {
                        newBP.telist[idx]= newBP.tetype;
                        break;
                    }
                    idx++;
                }
            }
        }
        else
        {
            List<Wall__c> wallList = newBP.wallList;
            if(wallList !=null)
            {
                idx =0;
                For(integer i=0, j = wallList.size(); i < j; i++)
                {
                    Wall__c t = wallList[i];

                    if(t.Id== newBP.wall.Id)
                    {
                        newBP.wallList[idx]= newBP.wall;
                        break;
                    }
                    idx++;
                }
            }    
        }
        
        if(dsmtEAModel.GetWallRecordTypes().contains(newBP.recordTypeMap.get(newBP.teType.RecordTypeId)))
        {
            newBP.teType.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(newBP);                
            newBP.teType.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(newBP.teType.R_Value__c);
            newBP.teType.ExposedRValue__c = dsmtSurfaceHelper.ComputeWallExposedRValueCommon(newBP);
            newBP.teType.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeWallEffectiveRValueCommon(newBP);
            newBP.teType.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeWallNominalRValueCommon(newBP);
            newBP.teType.UA__c = dsmtSurfaceHelper.ComputeWallUACommon(newBP);
            //newBP.teType.UA__c = dsmtSurfaceHelper.ComputeWallUACommon(newBP);            
        }
        else 
        {
            //system.debug('updating wall r value after recom ' + newBP.wall.R_Value__c);
            newBP.wall.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(newBP);
            //system.debug('updated wall r value after recom ' + newBP.wall.R_Value__c);

            newBP.wall.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(newBP.wall.R_Value__c);
            newBP.wall.ExposedRValue__c = dsmtSurfaceHelper.ComputeWallExposedRValueCommon(newBP);
            newBP.wall.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeWallEffectiveRValueCommon(newBP);
            newBP.wall.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeWallNominalRValueCommon(newBP);
            newBP.wall.UA__c = dsmtSurfaceHelper.ComputeWallUACommon(newBP);            
        }
        

        newBP = dsmtBuildingModelHelper.UpdateBuildingMechanicalApplianceLoadModel(newBP);        
        newBP = dsmtBuildingModelHelper.UpdateBuildingLoadModel(newBP);

        newBP = dsmtBuildingModelHelper.UpdateBuildingMechanicalApplianceLoadModel(newBP);        
        newBP = dsmtBuildingModelHelper.UpdateBuildingLoadModel(newBP);

        //newBP = dsmtBuildingModelHelper.UpdateBuildingThermalLoadModel(newBP);

        newBP.temp = dsmtTempratureHelper.ComputeTemprature(newBP);
        newBP.mo = dsmtBuildingModelHelper.ComputeBuildingModel(newBP);

        //system.debug(' recommended model ' + JSON.serialize(newBP));
        
        return newBP;

    }

    public static void UpdateThermostateForThermalLoad(dsmtEAModel.Surface bp)
    {
         /// After UA Update update current thermostate and recalculate temp and mo
        dsmtEAModel.UpdateThermostateModel(bp);
        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);
        //////////////////////////////////////////
    }

    public static void UpdateDoorRecord(dsmtEAModel.Surface bp, Thermal_Envelope_Type__c tet)
    {
        tet.DoorType__c = SurfaceConstants.DefaultDoorType;
        tet.Width__c= SurfaceConstants.DefaultDoorWidth;
        tet.Height__c = SurfaceConstants.DefaultDoorHeight;
        tet.Exterior_Area__c = (tet.Width__c * tet.Height__c)/144;
        tet.HLF__c = dsmtSurfaceHelper.ComputeDoorHLFCLFCommon(bp, 'H');
        tet.CLF__c = dsmtSurfaceHelper.ComputeDoorHLFCLFCommon(bp, 'C');
        tet.R_Value__c = dsmtSurfaceHelper.ComputeDoorRvalueCommon(bp);
        tet.U_Factor__c = 1 / tet.R_Value__c;
        tet.UA__c = tet.Exterior_Area__c * tet.U_Factor__c;

        //UpdateThermostateForThermalLoad(bp);

        tet.LoadC__c =dsmtSurfaceHelper.ComputeDoorLoadCommon(bp,'C');
        tet.LoadH__c =dsmtSurfaceHelper.ComputeDoorLoadCommon(bp,'H');
    }

    public static void UpdateWallRecord(dsmtEAModel.Surface bp, boolean isInsert, boolean recmFlow)
    {
        Thermal_Envelope_Type__c tet = bp.tetype;

        string recTypeName = bp.recordTypeMap.get(tet.RecordTypeId);
        
        tet.Exposed_To__c = dsmtSurfaceHelper.GetSurfaceExposedArea().get(recTypeName);
        tet.Space_Type__c = dsmtSurfaceHelper.GetSurfaceSpaceType().get(recTypeName);

        if(tet.ActualWidth__c <=0)tet.ActualWidth__c=null;
        if(tet.ActualWidth__c ==null)
        {
            tet.Width__c = dsmtSurfaceHelper.ComputeWallWidthCommon(bp);    
        }
        else {
            tet.Width__c = tet.ActualWidth__c;   
        }

        if(tet.ActualHeight__c <=0) tet.ActualHeight__c=null;
        if(tet.ActualHeight__c !=null)
        {
            tet.Height__c = tet.ActualHeight__c /12;
        }
        else {
            tet.Height__c = dsmtSurfaceHelper.ComputeWallHeightCommon(bp);
            tet.HeightInch__c = dsmtEAModel.ISNULL(tet.Height__c) * 12;    
        }
        
        tet.Length__c = tet.Width__c;

        tet.SpaceConditioning__c = dsmtSurfaceHelper.ComputeSpaceConditioningCommon(bp);

        if(recTypeName == 'Band Joist' || recTypeName == 'Rim Joist')
        {
          tet.IsRim__c = true;
          tet.Exposed_To__c = 'Exterior';
        }

        // DSST-6348 - Cavity Insulation Depth error
        // #Code Block Start
        if(bp.layers !=null && bp.layers.size() > 0)
        {
            Layer__c wfLayer = dsmtSurfaceHelper.GetFirstFramingLayer(bp.layers,1);
            if(wfLayer !=null)
            {
                if(wfLayer.ActualCavityInsulationDepth__c !=null && wfLayer.ActualCavityInsulationDepth__c !=0)
                {
                    tet.ActualInsulationAmount__c = null;
                }
            }
        }

        if(tet.ActualInsulationAmount__c ==null)
        {
            tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeWallInsulationAmountCommon(bp);
        }        
        // #Code Block End
        if(isInsert)
        {
            //tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeWallInsulationAmountCommon(bp);
            tet.Gaps__c = SurfaceConstants.DefaultInsulationGaps;
        }
       

        if(tet.Actual_Area__c == null || tet.Actual_Area__c == 0)
            tet.Exterior_Area__c = dsmtSurfaceHelper.ComputeSurfaceAreaCommon(bp);

        if(recTypeName =='Band Joist' && dsmtEAModel.ISNULL(bp.bs.Floors__c) <= 1)
        {
            tet.Exterior_Area__c =0;
        }

        tet.Net_Area__c = dsmtSurfaceHelper.ComputeAreaNet( bp);
        tet.AreaBack__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Back');
        tet.AreaFront__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Front');
        tet.AreaLeft__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Left');
        tet.AreaRight__c = dsmtSurfaceHelper.ComputeWallAreaPartCommon(bp,'Right');                
        tet.RValueConst__c = dsmtSurfaceHelper.ComputeRValueConst(bp);        

        tet.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(bp);                
        tet.ExposedRValue__c = dsmtSurfaceHelper.ComputeWallExposedRValueCommon(bp);
        tet.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeWallEffectiveRValueCommon(bp);
        tet.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeWallNominalRValueCommon(bp);
        tet.UA__c = dsmtSurfaceHelper.ComputeWallUACommon(bp);                                
        tet.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(tet.R_Value__c);

       // if(!recmFlow)
            //UpdateThermostateForThermalLoad(bp);

        tet.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'H');
        tet.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'C');

        if(tet.UA__c !=null && tet.HLF__c !=null)
            tet.UAEffH__c = dsmtEAModel.SetScale(tet.UA__c) * dsmtEAModel.SetScale(tet.HLF__c);

        if(tet.UA__c !=null && tet.CLF__c !=null)
            tet.UAEffC__c = dsmtEAModel.SetScale(tet.UA__c) * dsmtEAModel.SetScale(tet.CLF__c);

        tet.LoadH__c = dsmtSurfaceHelper.ComputeWallLoadCommon(bp, 'H');
        tet.LoadC__c = dsmtSurfaceHelper.ComputeWallLoadCommon(bp, 'C');
    }
    
    public static void deleteLayers(Set<Id> tetIdSet)
    {
        List<Layer__c> layerList = [select id from Layer__c where Thermal_Envelope_Type__c in : tetIdSet];
        
        if(layerList.size() > 0)
        {
            delete layerList;
        }
    }
}