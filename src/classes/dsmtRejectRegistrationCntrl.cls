public with sharing class dsmtRejectRegistrationCntrl{

    public Registration_Request__c RR{get;set;}
    public string noteTitle{get;set;}
    public string noteBody{get;set;}
    public String selectedExcptions{set;get;}
    public string selectedStatus{get;set;}
    
    public List<ExceptionModel> listExceptions{set;get;}
    
    public dsmtRejectRegistrationCntrl(ApexPages.StandardController controller){
        listExceptions = new List<ExceptionModel>();
        selectedExcptions = '';
        RR = new Registration_Request__c();
        String rrid = controller.getId();
        List<Registration_Request__c >lstEas = [select id,Name, Rejected_Reasons__c,Status__c from Registration_Request__c WHERE Id=:rrid ];
        if(lstEas.size()>0)
            RR = lstEas[0];
        
        for(Exception__c exp : [select Id,Name,Exception_Message__c,Outbound_Message__c,Disposition__c,Short_Rejection_Reason__c from Exception__c where Registration_Request__c =: rrid]){
            ExceptionModel expMod = new ExceptionModel();
            expMod.exp = exp;
            listExceptions.add(expMod);
        }
    }
    
    public PageReference saveAndUpdate(){
       
       Set<String> setExpIds = new Set<String>();
        system.debug('--selectedExcptions--'+selectedExcptions);
        if(selectedExcptions != null && selectedExcptions.length() > 0){
            if(selectedExcptions.indexOf(',') != -1){
                selectedExcptions = selectedExcptions.substring(0,selectedExcptions.length()-1);
            }
            setExpIds.addAll(selectedExcptions.split(','));
        }
        system.debug('--setExpIds--'+setExpIds);
        
        String ShortRejReason = '';
        String message = '';
            
        for(ExceptionModel expMod : listExceptions){
           system.debug('---expMod.exp.Id---'+expMod.exp.Id);
           if(setExpIds.contains(expMod.exp.Id)){
                String desposition = expMod.exp.Outbound_Message__c != null ? expMod.exp.Outbound_Message__c : '';
                    
                if(ShortRejReason != null && ShortRejReason != ''){
                    ShortRejReason += ' , ' + expMod.exp.Short_Rejection_Reason__c;
                }else{
                    ShortRejReason = expMod.exp.Short_Rejection_Reason__c;
                }
                 if(desposition == null || desposition == ''){
                    desposition = expMod.exp.Exception_Message__c != null ? expMod.exp.Exception_Message__c : '';
                }
                
                if(desposition != ''){
                    message += '• ';
                    message +=   desposition;
                    message += '\n \n';
                }
                 system.debug('--message---'+message);
           }
        }
       
          if(noteBody != null && noteBody.trim().length() > 0){
               message += '• ' + noteBody;
                
                if(ShortRejReason != null && ShortRejReason != ''){
                    ShortRejReason += ' , '+noteBody.trim();
                }else{
                    ShortRejReason = noteBody.trim();
                }
          }
          
         RR.Rejected_Reasons__c = message;
         RR.Status__c = 'Rejected';  
         RR.Steps_Completed__c = '8';
        
        try{
            update RR;
            
        }catch(Exception ex){
            string err = '';
            string msg = ex.getMessage();
            if(msg.contains('VALIDATION_EXCEPTION')){
                msg = msg.substring(msg.indexOf('VALIDATION_EXCEPTION')+21, msg.length()-2);
                err = msg;
            }else{
                err = msg;
            }
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, err);
            ApexPages.addMessage(myMsg);
            
            return null;
        }
        return new PageReference('/'+rr.id);
    }
    
    public class ExceptionModel{
        public Exception__c exp{set;get;}
        public Boolean isCheck{set;get;}
        public ExceptionModel(){
            exp = new Exception__c();
            isCheck = false;
        }
    }
    
  
}