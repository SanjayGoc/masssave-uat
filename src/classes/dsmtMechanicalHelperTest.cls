@isTest 	
public class dsmtMechanicalHelperTest 
{	
	@testsetup static void SetupEnergyAssessment()
    {
        dsmtEAModel.setupAssessment(); 
    }

	@isTest
	static void CreateHeatingMechancial()
	{
    	List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];    	

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		/// create mechnical heating record record
		Mechanical__c mech = new Mechanical__c();
		mech.Energy_Assessment__c = ea.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = ea.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Furnace').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();

	}

	@isTest
	static void CreateCoolingMechancial()
	{
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];    	

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		/// create mechnical heating record record
		Mechanical__c mech = new Mechanical__c();
		mech.Energy_Assessment__c = ea.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = ea.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Central A/C').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(eaId);
		bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
		bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

		bp.mstobj = mstList[0];
		bp.mechType = mechType;

		dsmtMechanicalHelper.ResetActualsToNull(mstList[0]);
		dsmtMechanicalHelper.hasActualLoadFraction(mstList[0],bp);
		dsmtMechanicalHelper.HasDistributionSystem(bp);
		dsmtMechanicalHelper.HasDistributionSystemWithType(bp,'Duct');
		dsmtMechanicalHelper.HasThermostate(bp);
		dsmtMechanicalHelper.hasActualLoadFractionOrActualCapacity(bp);
		dsmtMechanicalHelper.GetMechanicalDHWSystem(bp);
		dsmtMechanicalHelper.GetMechanicalThermostatSystemList(bp);
		dsmtMechanicalHelper.ComputeCoolingFanOnCommon(bp);
		dsmtMechanicalHelper.GetThermostatRecordList(bp);
		dsmtMechanicalHelper.GetDistributionSystemMechanicalSystem(bp);
		dsmtMechanicalHelper.PercentToFraction(45.32);
		dsmtMechanicalHelper.ComputeCoolingFanOnCommon(bp);

		string months = 'January;February;March;April;May;June;July;August;September;October;November;December';
		dsmtMechanicalHelper.getMonthsFactor(months,'IntensH',bp);

		Test.stopTest();

	}

	@isTest
	static void CreateDHWMechancial()
	{
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];    	

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		/// create mechnical heating record record
		Mechanical__c mech = new Mechanical__c();
		mech.Energy_Assessment__c = ea.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Hot Water').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = ea.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Storage Tank').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();

	}

	@isTest
	static void CreateHeatingDHWMechancial()
	{
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];    	

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		/// create mechnical heating record record
		Mechanical__c mech = new Mechanical__c();
		mech.Energy_Assessment__c = ea.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating+DHW').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = ea.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('Boiler + Tankless Coil').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Boiler').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Tankless Coil').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Baseboard Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();

	}

	@isTest
	static void CreateHeatingCoolingMechancial()
	{
		List<Energy_Assessment__c> eaList = [Select Id From Energy_Assessment__c Limit 1];    	

		Energy_Assessment__c ea = eaList[0];
		
		Set<Id> eaId = new Set<Id>();
		eaId.add(ea.Id);

		/// create mechnical heating record record
		Mechanical__c mech = new Mechanical__c();
		mech.Energy_Assessment__c = ea.Id;
		mech.RecordTypeId = Schema.SObjectType.Mechanical__c.getRecordTypeInfosByName().get('Heating+Cooling').getRecordTypeId();
		insert mech;

		Mechanical_Type__c mechType = new Mechanical_Type__c();
		mechType.Energy_Assessment__c = ea.Id;
		mechType.Mechanical__c = mech.Id;
		mechType.RecordTypeId = Schema.SObjectType.Mechanical_Type__c.getRecordTypeInfosByName().get('GSHP').getRecordTypeId();
		insert mechType;

		List<Mechanical_Sub_Type__c> mstList = new List<Mechanical_Sub_Type__c>();

		Mechanical_Sub_Type__c mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('GSHP').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('GSHP Cooling').getRecordTypeId();
		mstList.add(mechSubType);		

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Thermostat').getRecordTypeId();
		mstList.add(mechSubType);

		mechSubType = new Mechanical_Sub_Type__c();
		mechSubType.Energy_Assessment__c = ea.Id;
		mechSubType.Mechanical__c = mech.Id;
		mechSubType.Mechanical_Type__c = mechType.Id;
		mechSubType.RecordTypeId = Schema.SObjectType.Mechanical_Sub_Type__c.getRecordTypeInfosByName().get('Duct Distribution System').getRecordTypeId();
		mstList.add(mechSubType);

		Test.startTest();

		insert mstList;

		Test.stopTest();

	}
	
}