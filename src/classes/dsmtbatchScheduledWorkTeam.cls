global class dsmtbatchScheduledWorkTeam implements Schedulable,database.batchable<sObject>,Database.AllowsCallouts,Database.Stateful {
    
   // global string myglobalId;
    global Set<Id> wtID = new Set<Id>();
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Date dt = Date.Today().Adddays(-1);
        String query = 'SELECT Id from Work_Team__c where unavailable__c=true';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Work_Team__c> scope)
    {
      
        for(Work_Team__c wt : scope){
           
         //  myglobalId +='~~~'+wt.Id;
           wtId.add(wt.Id);
        }
        
          
    }  
    global void finish(Database.BatchableContext BC)
    {
       // system.debug('--myglobalId ---'+myglobalId );
        List<dsmtCallOut.workTeamDeatil> wdList = new List<dsmtCallOut.workTeamDeatil>();
        dsmtCallOut.workTeamDeatil wd = null;
        system.debug('--wtId---'+wtId.size());
        for(Id wt : wtId){
            wd = new dsmtCallOut.workTeamDeatil();
            wd.isUnavailable = true;
            wd.workTeamId = wt;
            wdList.add(wd); 
            
        }
        
        String str = JSON.serialize(wdList);
            
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
    
        req.setEndpoint(System_Config__c.getInstance().URL__c+'UpdateWorkTeamAvailability?type=json');
     
        req.setMethod('POST');
        
        String body = '{"workTeamDeatil":'+str+'}';
        system.debug('--str--'+body);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        req.setTimeout(120000);
        http = new Http();
         if(!Test.isRunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
          }
    }
    
    global void  execute(SchedulableContext sc){
        dsmtbatchScheduledWorkTeam  obj = new dsmtbatchScheduledWorkTeam ();
        database.executebatch(obj,200);    
    }
}