public with sharing class AtticRoofComponentCntrl {
    public Id atticRoofId{get;set;}
    public String newLayerType{get;set;}
    public Ceiling__c atticRoof{get{
        if(atticRoof == null){
            List<Ceiling__c> roofs = Database.query('SELECT ' + getSObjectFields('Ceiling__c') + ',Thermal_Envelope_Type__r.SpaceConditioning__c FROM Ceiling__c WHERE Id=:atticRoofId'); 
            if(roofs.size() > 0){
                atticRoof = roofs.get(0);
            }
        }
        return atticRoof;
    }set;}
    public void saveRoof()
    {       
        saveRoofLayers();
        if(atticRoof != null && atticRoof.Id != null){
            atticRoof.Exposed_To__c = atticRoof.Roof_To__c;
            
            UPDATE atticRoof;
            
            atticRoof = null;
        }
    }
    
    public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    
    public List<Layer__c> roofLayers{get;set;}
    public Map<Id,Layer__c> getRoofLayersMap(){
        Id roofId = atticRoof.Id;
        System.debug('roofId :: ' + roofId);
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Ceiling__c =:roofId AND HideLayer__c = false';
        System.debug('@@QUERY' + query);
        roofLayers = Database.query(query);
        return new Map<Id,Layer__c>(roofLayers);
    }
    public void saveRoofLayers(){
        if(roofLayers != null && roofLayers.size() > 0){
            UPSERT roofLayers;
        }
    }
    public void addNewRoofLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Ceiling__c =:atticRoof.Id AND RecordTypeId =:recordTypeId];
        if(layerType != null && m.containsKey(layerType)){
            Decimal nextNumber= layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Ceiling__c = atticRoof.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            if(newLayer.Id != null){
                atticRoof.Next_Attic_Roof_Layer_Number__c = nextNumber + 1;
                UPDATE atticRoof;
            }
            roofLayers.add(newLayer);
        }
    }
    public void deleteRoofLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = roofLayers.get(index).Id);
        roofLayers.remove(index);
        saveRoof();
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}