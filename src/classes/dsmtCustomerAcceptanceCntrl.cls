public with sharing class dsmtCustomerAcceptanceCntrl {
    
    public String sectionTitle{get;set;}
    public Checklist__c pgCheckList{get;set;}
    public Checklist__c pgThankCheckList{get{return [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Customer_Acceptance_Form_Thank' LIMIT 1];}}
    public String agreementDocumentId{get;set;}
    public String projectId{get;set;}
    public String proposalId{get;set;}
    public String inspectionRequestId{get;set;}
    public String signatureDocName{get{return '';}}
    public String signatureAttachmentType{get;set;}
    public String attachmentId{get;set;}
    public String attachmentSignId{get;set;}
    public boolean enable_poller{get;set;}
    public String templateID{get;set;}
    public String query{get;set;}
    public String queryID{get;set;}
    public Attachment__C attachment{get;set;}
    public String SessionId{get;set;}
    public boolean isDocAvailableForSign{get;set;}
    public String ofn{get;set;}
    
    public dsmtCustomerAcceptanceCntrl(){
        projectId = ApexPages.currentPage().getParameters().get('id');
        proposalId = ApexPages.currentPage().getParameters().get('propid');
        inspectionRequestId = ApexPages.currentPage().getParameters().get('IRId');
        String documentName = 'Mass_Save_Customer_Acceptance_Form_pdf';

        isDocAvailableForSign = true;
        
        if(projectId != null && projectId.trim() != ''){
            signatureAttachmentType = 'Contract Document';
            ofn='Contract Document With Signature';
            List<Recommendation_Scenario__c> projectList  = [SELECT Id,Name, Customer_Acceptance_Signature_Id__c from Recommendation_Scenario__c where Id = :projectId and Status__c = 'Pending Customer Acceptance' limit 1];
            if(projectList.size() == 0){
                 isDocAvailableForSign = false;
            }
            List<Attachment__c> attachments = [SELECT Id, Name, File_Url__c FROM Attachment__c WHERE Project__c=:projectId AND Status__c='Completed' and attachment_Type__c =: signatureAttachmentType  ORDER BY CreatedDate DESC limit 1];
            if(attachments != null && attachments.size() > 0){
               attachment = attachments.get(0);
            }
            
            pgCheckList = [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Customer_Acceptance_Form' LIMIT 1];
            
            
        }else if(proposalId != null && proposalId.trim() != ''){
            signatureAttachmentType = 'Contract Document';
            ofn='Contract Document With Signature';
            List<Attachment__c> attachments = [SELECT Id,Name, File_Url__c FROM Attachment__c WHERE Proposal__c=:proposalId ORDER BY CreatedDate DESC];
            if(attachments != null && attachments.size() > 0){
                attachment = attachments.get(0);
            }
            
            pgCheckList = [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'Customer_Acceptance_Form' LIMIT 1];
        } else if(inspectionRequestId != null && inspectionRequestId.trim() != ''){
            signatureAttachmentType = 'InspectionRequestSignatureAttachment';
            ofn='COI Document With Signature';
            List<Attachment__c> attachments = [SELECT Id,Name, File_Url__c FROM Attachment__c WHERE Inspection_Request__c=:inspectionRequestId ORDER BY CreatedDate DESC];
            if(attachments != null && attachments.size() > 0){
                attachment = attachments.get(0);
            }
            
            pgCheckList = [SELECT Id,Name,Summary__c,Help_Url__c,(select id,Checklist_Information__c,Section__c from Checklist_Items__r where Help_text_for_public_portal__c = false and Special_instruction_for_public_portal__c = false order by Sequence__c) FROM Checklist__c WHERE Unique_Name__c = 'COI_Form' LIMIT 1];            
        }
        
        system.debug('--attachment--'+attachment);
        
        
    }
    
    @RemoteAction
    public static String saveSign(String projectId, String docname, String imgURI, String attachType, String sobjectType){
        system.debug('--projectId --'+projectId+'--imgURI--'+imgURI);
        system.debug('--docname--'+docname);
        try{
            if(sobjectType.equalsIgnoreCase('Recommendation_Scenario__c')){
                List<Recommendation_Scenario__c> projectList  = [SELECT Id,Name, Customer_Acceptance_Signature_Id__c, (Select id, Status__c From Recommendations__r Where Status__c = 'Pending Customer Acceptance') from Recommendation_Scenario__c where Id = :projectId limit 1];
                if(projectList.size() > 0){
                    Recommendation_Scenario__c enApp  = projectList.get(0);
                    
                    Datetime now = Datetime.now();
                    String datetimestr = now.format('MMddyyyyhhmm');
                    
                    docname = docname.replace('Pdf','Doc');
                    system.debug('--docname--'+docname);
                    
                    Attachment__c cAtt = new Attachment__c();
                    cAtt.Attachment_Name__c = 'Customer_Sign_'+datetimestr+'.png';
                    cAtt.Application_Type__c = 'Custom';//Prescriptive';
                    cAtt.Attachment_Type__c = 'Customer Signature'; //'Trade Ally Signature'; //
                    cAtt.Project__c = enApp.Id;
                    cAtt.Status__c = 'New';
                    insert cAtt;
                    if(enApp.Recommendations__r.size() > 0) {
                        for(Recommendation__c rec : enApp.Recommendations__r) {
                            rec.status__c = 'Contracted';
                        }
                        update enApp.Recommendations__r;
                    }
                    
                    Attachment sAttSign = new Attachment();
                    sAttSign.ParentID = cAtt.id;
                    sAttSign.Body = EncodingUtil.base64Decode(imgURI);
                    sAttSign.contentType = 'image/png';
                    sAttSign.Name = 'Customer_Sign_'+enApp.Name+'_'+datetimestr+'.png';
                    insert sAttSign;
                    
                    enApp.Customer_Acceptance_Signature_Id__c = sAttSign.id;
                    enApp.Status__c = 'Contracted';
                    enApp.Signature_Date__c = Date.Today();
                    UPDATE enApp;
                    
                    //String session_id = UserInfo.getSessionId();
                    //String session_id = 'USERNAME:' + Login_Detail__c.getOrgDefaults().Attachment_User_Name__c + ';PASSWORD:' +  + Login_Detail__c.getOrgDefaults().Attachment_User_Password__c+  + Login_Detail__c.getOrgDefaults().Attachment_User_Security_Token__c;
                    //String server_url = Dsmt_Salesforce_Base_Url__c.getOrgDefaults().PartnerURL__c;
                    //String environment_name = UserInfo.getOrganizationId(); 
              
                    Attachment__c cAtt1 = new Attachment__c();
                    cAtt1.Attachment_Name__c = 'Contract Documents - ' + enApp.Id + '.pdf';
                    cAtt1.Application_Type__c = 'Custom';
                    cAtt1.Attachment_Type__c = attachType;
                    cAtt1.Project__c = enApp.Id;
                    //cAtt1.Attachment_Offer_Template__c = 'Contract Documents - ' + enApp.Id + '.pdf';
                    cAtt1.Category__c = 'PRINT_COLLATERAL_PROJECT';
                    cAtt1.Attachment_Offer_Template__c = docname;
                    cAtt1.Status__c = 'New';
                    insert cAtt1;
                    
                    //system.debug('--webservice call init--');
                    //dsmtFuture.perform_callout(session_id, server_url, cAtt1.Id, enApp.Id, environment_name);
                    // invokeWebservice(enApp.Id, cAtt1.Id);
                    //system.debug('--webservice call intiated--');
                    return 'Success~~~'+cAtt1.Id+'~~~CongaGenerate';
                }else{
                    return 'Fail'+' : '+'Invalid Id for Project!!!';
                }     
            }else if(sobjectType.equalsIgnoreCase('Proposal__c')){
                List<Proposal__c> proposalList  = [SELECT Id,Name, Customer_Acceptance_Signature_Id__c from Proposal__c where Id = :projectId limit 1];
                if(proposalList.size() > 0){
                    Proposal__c proposal = proposalList.get(0);
                    
                    Datetime now = Datetime.now();
                    String datetimestr = now.format('MMddyyyyhhmm');
                    
                    //docname = docname.replace('Pdf','Doc');
                    //system.debug('--docname--'+docname);
                    
                    Attachment__c cAtt = new Attachment__c();
                    cAtt.Attachment_Name__c = 'Customer_Sign_'+datetimestr+'.png';
                    cAtt.Application_Type__c = 'Proposal';//Prescriptive';
                    cAtt.Attachment_Type__c = 'Customer Signature'; //'Trade Ally Signature'; //
                    cAtt.Proposal__c = proposal.Id;
                    cAtt.Status__c = 'New';
                    insert cAtt;
                    
                    Attachment sAttSign = new Attachment();
                    sAttSign.ParentID = cAtt.id;
                    sAttSign.Body = EncodingUtil.base64Decode(imgURI);
                    sAttSign.contentType = 'image/png';
                    sAttSign.Name = 'Customer_Sign_'+proposal.Name+'_'+datetimestr+'.png';
                    insert sAttSign;
                    
                    proposal.Customer_Acceptance_Signature_Id__c = sAttSign.id;
                    //proposal.Type__c = 'Signed';
                    proposal.Signature_Date__c = Date.Today();
                    UPDATE proposal;
                    
                    //String session_id = UserInfo.getSessionId();
                    //String session_id = 'USERNAME:' + Login_Detail__c.getOrgDefaults().UserName__c + ';PASSWORD:' +  + Login_Detail__c.getOrgDefaults().Password__c +  + Login_Detail__c.getOrgDefaults().Security_Token__c;
                    //String server_url = Dsmt_Salesforce_Base_Url__c.getOrgDefaults().PartnerURL__c;
                    //String environment_name = UserInfo.getOrganizationId(); 
                    
                    Attachment__c cAtt1 = new Attachment__c();
                    cAtt1.Attachment_Name__c = 'Contract Documents - ' + proposal.Id + '.pdf';
                    cAtt1.Application_Type__c = 'Custom';
                    cAtt1.Attachment_Type__c = attachType;
                    cAtt1.Proposal__c= proposal.Id;
                    //cAtt1.Attachment_Offer_Template__c = docname;
                    cAtt1.Category__c = 'PRINT_COLLATERAL_PROJECT';
                    cAtt1.Status__c = 'New';
                    insert cAtt1;
                    
                    //system.debug('--webservice call init--');
                    //dsmtFuture.perform_callout(session_id, server_url, cAtt1.Id, enApp.Id, environment_name);
                    //system.debug('--webservice call intiated--');
                    return 'Success~~~'+cAtt1.Id+'~~~Proposal Contract Generation';
                }else{
                    return 'Fail'+' : '+'Invalid Id for Proposal!!!';
                }
            } else if(sobjectType.equalsIgnoreCase('Inspection_Request__c')){
                List<Inspection_Request__c> projectList  = [SELECT Id,Name, Customer_Acceptance_Signature_Id__c from Inspection_Request__c where Id = :projectId limit 1];
                if(projectList.size() > 0){
                    Inspection_Request__c enApp  = projectList.get(0);
                    
                    Datetime now = Datetime.now();
                    String datetimestr = now.format('MMddyyyyhhmm');
                    
                    docname = docname.replace('Pdf','Doc');
                    system.debug('--docname--'+docname);
                    
                    Attachment__c cAtt = new Attachment__c();
                    cAtt.Attachment_Name__c = 'Customer_Sign_'+datetimestr+'.png';
                    cAtt.Application_Type__c = 'Custom';//Prescriptive';
                    cAtt.Attachment_Type__c = 'Customer Signature'; //'Trade Ally Signature'; //
                    cAtt.Inspection_Request__c = enApp.Id;
                    cAtt.Status__c = 'New';
                    insert cAtt;
                    
                    Attachment sAttSign = new Attachment();
                    sAttSign.ParentID = cAtt.id;
                    sAttSign.Body = EncodingUtil.base64Decode(imgURI);
                    sAttSign.contentType = 'image/png';
                    sAttSign.Name = 'Customer_Sign_'+enApp.Name+'_'+datetimestr+'.png';
                    insert sAttSign;
                    
                    enApp.Customer_Acceptance_Signature_Id__c = sAttSign.id;
                    enApp.Signature_Date__c = Date.Today();
                    enApp.COI_Signed__c = true;
                    UPDATE enApp;
                    
                    //String session_id = UserInfo.getSessionId();
                    //String session_id = 'USERNAME:' + Login_Detail__c.getOrgDefaults().Attachment_User_Name__c + ';PASSWORD:' +  + Login_Detail__c.getOrgDefaults().Attachment_User_Password__c+  + Login_Detail__c.getOrgDefaults().Attachment_User_Security_Token__c;
                    //String server_url = Dsmt_Salesforce_Base_Url__c.getOrgDefaults().PartnerURL__c;
                    //String environment_name = UserInfo.getOrganizationId(); 
              
                    Attachment__c cAtt1 = new Attachment__c();
                    cAtt1.Attachment_Name__c = docname;
                    cAtt1.Application_Type__c = 'Custom';
                    cAtt1.Attachment_Type__c = attachType;
                    cAtt1.Inspection_Request__c = enApp.Id;
                    //cAtt1.Attachment_Offer_Template__c = 'Contract Documents - ' + enApp.Id + '.pdf';
                    cAtt1.Category__c = 'INSPECTION_REQUEST';
                    cAtt1.Attachment_Offer_Template__c = docname;
                    cAtt1.Status__c = 'New';
                    insert cAtt1;
                    
                    //system.debug('--webservice call init--');
                    //dsmtFuture.perform_callout(session_id, server_url, cAtt1.Id, enApp.Id, environment_name);
                    // invokeWebservice(enApp.Id, cAtt1.Id);
                    //system.debug('--webservice call intiated--');
                    return 'Success~~~'+cAtt1.Id+'~~~COI Report';
                }else{
                    return 'Fail'+' : '+'Invalid Id for Inspection Report!!!';
                }     
            }
        }catch(Exception ex){
            system.debug('--ex--'+ex);
            return 'Fail'+' : '+ex.getmessage();
        }
        
        return 'Success~~~12112121';
    }
    
    public void getdataForContractDocument(){
        system.debug('--attachmentId--'+attachmentId);
        
        SessionId = UserInfo.getsessionId();
        system.debug('--Login SessionId --'+SessionId);
        if(SessionId == null){
          SessionId = dsmtFutureHelper.getGuestSession();
        }
        system.debug('--Login SessionId --'+SessionId);
        
        String TemplateName = 'Contract Document With Signature';
        
        List<APXTConga4__Conga_Template__c> Templatelist = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c =:TemplateName];
        
        if(Templatelist.size() > 0){
            templateID = Templatelist.get(0).Id;
        }
        
        List<APXTConga4__Conga_Merge_Query__c> querylist = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Project Acceptance Template Query']; 
        if(querylist.size() > 0){
            query = querylist.get(0).Id+'?pv0='+projectId;
        }
    }
    
    public void getdataForProposalContractDocument(){
        system.debug('--attachmentId--'+attachmentId);
        
        SessionId = UserInfo.getsessionId();
        system.debug('--Login SessionId --'+SessionId);
        if(SessionId == null){
          SessionId = dsmtFutureHelper.getGuestSession();
        }
        system.debug('--Login SessionId --'+SessionId);
        
        String TemplateName = 'Proposal Contract clearesult template';
        
        List<APXTConga4__Conga_Template__c> Templatelist = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c =:TemplateName];
        
        if(Templatelist.size() > 0){
            templateID = Templatelist.get(0).Id;
        }
        
        List<APXTConga4__Conga_Merge_Query__c> querylist = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Proposal_contract_Query1','Proposal_contract_Query2', 'Proposal_contract_Query3', 'Proposal_contract_Query4', 'Proposal_contract_Query5', 'Proposal_contract_Query6', 'Proposal_contract_Query7', 'Proposal_contract_Query8')]; 
        List<Proposal__c> proposals = [Select Id, Energy_Assessment__c FROM Proposal__c Where id=: proposalId];
        String assessId = proposals.get(0).Energy_Assessment__c;
        query = '';
        for(APXTConga4__Conga_Merge_Query__c q : querylist)
        {
            query += q.ID +'?pv0='+assessId+ ',';
        }
        query = query.removeEnd(',');
        
        
    }
    
     public void getdataForInspectionReportDocument(){
        system.debug('--attachmentId--'+attachmentId);
        
        SessionId = UserInfo.getsessionId();
        system.debug('--Login SessionId --'+SessionId);
        if(SessionId == null){
          SessionId = dsmtFutureHelper.getGuestSession();
        }
        system.debug('--Login SessionId --'+SessionId);
        
        String TemplateName = 'Inspection Report Template';
        
        List<APXTConga4__Conga_Template__c> Templatelist = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c =:TemplateName];
        
        if(Templatelist.size() > 0){
            templateID = Templatelist.get(0).Id;
        }
        
        List<APXTConga4__Conga_Merge_Query__c> querylist = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Inspection Report Query']; 
        if(querylist.size() > 0){
            query = querylist.get(0).Id+'?pv0='+inspectionRequestId;
        }       
    }

    Public String stdAttachmentId{get;set;}
    public PageReference checkCongaStatus(){
        try{
            system.debug('--attachmentId--'+attachmentId);
            enable_poller = true;
            List<Attachment__c> customAttachmentlist = [SELECT Id,Name, (SELECT Id,Name FROM Attachments) FROM Attachment__c WHERE Id =: attachmentId];
            
            if(customAttachmentlist.size() > 0 && customAttachmentlist.get(0).Attachments.size() > 0) {
                
                
                
                DocumentServiceWSFile.clsSalesForceLoginDetail loginDetail = new DocumentServiceWSFile.clsSalesForceLoginDetail();
                loginDetail.SessionID = SessionId;
                loginDetail.serverURL = Login_Detail__c.getInstance().Server_URL__c;
                loginDetail.orgID = Login_Detail__c.getInstance().Org_Id__c;
                loginDetail.userName = Login_Detail__c.getInstance().Attachment_User_Name__c;
                loginDetail.password = Login_Detail__c.getInstance().Attachment_User_Password__c;
                loginDetail.securityToken = Login_Detail__c.getInstance().Attachment_User_Security_Token__c;
                
                DocumentServiceWSFile.clsQueryStringParameters clsQueryStringParametersObj = new DocumentServiceWSFile.clsQueryStringParameters();
                clsQueryStringParametersObj.SessionID = SessionId;
                clsQueryStringParametersObj.ServerURL = Login_Detail__c.getInstance().Server_URL__c;
                clsQueryStringParametersObj.OrgId  = Login_Detail__c.getInstance().Org_Id__c;
                clsQueryStringParametersObj.AttachmentId = attachmentId;
                clsQueryStringParametersObj.SecurityKey = 'intelcore2duo';
                clsQueryStringParametersObj.AccountKey = 'nokia710lumia';
                clsQueryStringParametersObj.Replace = 'YES';

                if (projectId != null && projectId.trim().length() > 0) {
                    clsQueryStringParametersObj.RecordId = projectId ;
                    clsQueryStringParametersObj.AttachmentLookupCol = 'Project__c';
                    clsQueryStringParametersObj.AttachmentType = 'Contract Document';
                } else if (inspectionRequestId != null && inspectionRequestId.trim().length() > 0) {
                    clsQueryStringParametersObj.RecordId = inspectionRequestId;
                    clsQueryStringParametersObj.AttachmentLookupCol = 'Inspection_Request__c';
                    clsQueryStringParametersObj.AttachmentType = 'Certificate Of Inspection';
                } 

                stdAttachmentId = customAttachmentlist.get(0).Attachments.get(0).Id;
                DocumentServiceWSFile.DocumentServiceWSFileSoap documentServiceWSFile = new DocumentServiceWSFile.DocumentServiceWSFileSoap();
                DocumentServiceWSFile.ArrayOfClsUploadProcessResult result = documentServiceWSFile.MigrateAttachment(clsQueryStringParametersObj, stdAttachmentId, 'attachment', loginDetail);
                
                
                
                List<Attachment__c> list_pc_records = [SELECT Id, Status__c,Project__c,Attachment_Type__c FROM Attachment__c WHERE Id = :attachmentId AND Status__c = 'Completed' LIMIT 1];
                if(list_pc_records.size() > 0){
                     
                     enable_poller = false;
                     
                     List<Attachment__c> attlist = [SELECT Id FROM attachment__c WHERE Project__c = :projectId and attachment_Type__c = 'Customer Signature'];
                     DELETE attlist;
                     if(projectId != null) {
                         CreateReview();
                     }
                }
            }
        }catch(Exception ex) {
            system.debug('--ex--'+ex);
            enable_poller = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        return null;
    }

    public PageReference check_print_collateral_status() {
        system.debug('--projectId--'+projectId+'--attachmentId --'+attachmentId +',');
        try{
            enable_poller = true;
            List<Attachment__c> list_pc_records = [SELECT Id, Status__c,Project__c,Attachment_Type__c FROM Attachment__c WHERE Id = :attachmentId AND Status__c = 'Completed' LIMIT 1];
            if(list_pc_records.size() > 0){
                enable_poller = false;
                List<Recommendation_Scenario__c> projectList  = [SELECT Id,Name, Customer_Acceptance_Signature_Id__c, (Select id, Status__c From Recommendations__r Where Status__c = 'Pending Customer Acceptance') from Recommendation_Scenario__c where Id = :projectId limit 1];
                if(projectList.size() > 0){
                    //attachmentSignId = projectList.get(0).Customer_Acceptance_Signature_Id__c;
                    List<Attachment__c> attlist = [SELECT Id FROM attachment__c WHERE Project__c = :projectList.get(0).Id and attachment_Type__c = 'Customer Signature'];
                    DELETE attlist;
                }
            }
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        return null;
    }
    
    Public Void CreateReview(){
        List<Recommendation_Scenario__c> projList = [select id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                                            Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c,
                                                            Energy_Assessment__r.Customer__r.First_Name__c,Energy_Assessment__r.Customer__r.Last_Name__c,Address__c,City__c,State__c,Zip__c,Phone__c,Email__c from Recommendation_Scenario__c where id =: projectId];
        
        Recommendation_Scenario__c Proj = projList.get(0);
        Id preRevRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Pre Work Review').getRecordTypeId();
        Review__c rev = new Review__c();
        rev.Type__c = 'Pre Work Review';
        rev.Review_Type__c = 'Pre Work Review';
        rev.Requested_By__c = userinfo.getUSerId();
        rev.RecordTypeId = preRevRecordTypeId ;
        rev.Status__c = 'Pending Review';
        rev.Address__c = Proj.Address__c;
        rev.City__c = Proj.City__c ;
        rev.Zipcode__c = Proj.Zip__c;
        rev.State__c = Proj.State__c;
        rev.Phone__c = proj.Phone__c;
        rev.Email__c = Proj.Email__c;
        rev.Trade_Ally_Account__c = proj.Energy_Assessment__r.Trade_Ally_Account__c;
        rev.DSMTracker_Contact__c= proj.Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c;
        rev.First_Name__c = proj.Energy_Assessment__r.Customer__r.First_Name__c;
        rev.Last_Name__c = proj.Energy_Assessment__r.Customer__r.Last_Name__c;
        rev.Energy_Assessment__c = Proj.Energy_Assessment__c;
        insert rev;   
        
        List<Project_Review__c> prList = new List<Project_Review__c>();
            Project_Review__c pr = null;
            pr = new Project_Review__c();
            pr.Project__c = projList.get(0).Id;
            pr.Review__c = rev.Id;
            prList.add(pr);    
        
        
        if(prList.size() > 0){
            insert prList;
        }                                                 
    }
}