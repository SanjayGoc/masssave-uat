global class SR_AddNote_CtrlExtension {
    webservice static Service_Request__c getServiceRequestById(Id serviceRequestId){
        Service_Request__c serviceRequest = [SELECT Id, Name FROM Service_Request__c WHERE Id =:serviceRequestId];
        return serviceRequest;
    }
    
    webservice static Service_Request__c addNote(Id serviceRequestId, String note){
        String newNote = '<b>' + UserInfo.getName() + ' on '+ string.valueOf(system.today()).replace('00:00:00','')+' at '+ datetime.now().hour()+':'+datetime.now().minute() +' added</b><br>';
        newNote += '-----------------------<br>';
        newNote += (note != null || note != '') ? note+'<br>' : '<br>';
        Service_Request__c serviceRequest = [SELECT Id, Name, Notes__c FROM Service_Request__c WHERE Id =:serviceRequestId];
        serviceRequest.Notes__c = newNote + '<br>' + ((serviceRequest.Notes__c != null) ? serviceRequest.Notes__c : '');
        update serviceRequest; 
        return serviceRequest;
    }
}