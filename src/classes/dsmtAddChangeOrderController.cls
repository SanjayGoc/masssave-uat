public class dsmtAddChangeOrderController
{

    public String reviewId {get;set;}
    public String recommXml {get;set;}
    public Review__c rev{get;set;}
    public String dataSave{get;set;}
    
    
    public dsmtAddChangeOrderController(ApexPages.StandardController controller) {
        init();
    }
    
    public dsmtAddChangeOrderController() {
        init();
    }
    
    private void init()
    {
        rev = new Review__c();
        reviewId = ApexPages.currentPage().getParameters().get('id');
    }
    
    public void initJSON()
    {
        fillRecommendationsJSON(fetchProjectIdSet());
    }
    
    private set<String> fetchProjectIdSet()
    {
        set<String> projectIdSet = new Set<String>();
        
        List<Project_Review__c> revList = [select id, Name, Review__c, Project__c 
                                           from Project_Review__c 
                                           where Review__c =: reviewId
                                           and Review__c != null];
        for(Project_Review__c pr : revList)
        {
            projectIdSet.add(pr.Project__c);
        }
        
        return projectIdSet;
    }
    
    private void fillRecommendationsJSON(Set<String> projectIdSet)
    {
        List<Recommendation__c> recomList = [select id, Name, Recommendation_Scenario__c, Recommendation_Description__c, Quantity__c, Units__c,
                                             isChecked__c, Recommendation_Scenario__r.Name, Inspected_Quantity__c, Change_Order_Quantity__c
                                             from Recommendation__c 
                                             where Recommendation_Scenario__c in : projectIdSet];
        
        recommXml = '<Recommendations>';
        if(recomList.size() > 0)
        {
            for (Recommendation__c ir: recomList) {
                recommXml += '<Recommendation>';
                
                    recommXml += '<Checked>' + checkBox(ir.isChecked__c) + '</Checked>';
                    recommXml += '<ProjectId>' + ir.Recommendation_Scenario__c + '</ProjectId>';
                    recommXml += '<ProjectName><![CDATA[' + ir.Recommendation_Scenario__r.Name + ']]></ProjectName>';
                    recommXml += '<RecommendationId>' + ir.Id + '</RecommendationId>';
                    recommXml += '<RecommendationName><![CDATA[' + ir.Name + ']]></RecommendationName>';
                    
                    if(ir.Quantity__c == null)
                        ir.Quantity__c = 0;
                    recommXml += '<Quantity><![CDATA[' + ir.Quantity__c+ ']]></Quantity>';
                    
                    if(ir.Inspected_Quantity__c == null)
                        ir.Inspected_Quantity__c = 0;
                    recommXml += '<InspectedQuanity><![CDATA[' + ir.Inspected_Quantity__c + ']]></InspectedQuanity>';
                    
                    if(ir.Change_Order_Quantity__c == null)
                        ir.Change_Order_Quantity__c = 0;
                    recommXml += '<ChangeOrderQuantity><![CDATA[' + ir.Change_Order_Quantity__c+ ']]></ChangeOrderQuantity>';
                
                recommXml += '</Recommendation>';
            }
        }
        recommXml += '</Recommendations>';
    }
    
    boolean checkBox(boolean chk){
        return chk ? true : false;
    }
    
    public PageReference createReviewRec(){
        String projId = ApexPages.currentPage().getParameters().get('projid');
        
        if(reviewId != null)
        {
            List<Review__C> lstPrj = [select Id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                        Energy_Assessment__r.Trade_Ally_Account__r.Email__c,First_Name__c,
                                        Last_Name__c,Phone__c,Email__c,Address__c,City__c,State__c,
                                        Zipcode__c,Trade_Ally_Account__c from Review__C 
                                        where Id =: reviewId limit 1]; 
            rev.Status__c = 'New';
            Id devRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Change Order Review').getRecordTypeId();
    
            List<Dsmtracker_Contact__c> dsmtList = [select id,Dsmtracker_Contact__c,Trade_Ally_Account__c from Dsmtracker_Contact__c where Portal_User__c =: userinfo.getUSerID()
                                                        and Trade_Ally_Account__c  != null];
            if(dsmtList.size() > 0){
                rev.Trade_Ally_Account__c = dsmtList.get(0).Trade_Ally_Account__c;
            }                                   
            if(lstPrj.get(0).Energy_Assessment__c != null)    
                rev.Energy_Assessment__c = lstPrj.get(0).Energy_Assessment__c;
            
            rev.Type__c = 'Change Order Review';
            rev.First_Name__c = lstPrj.get(0).First_Name__c;
            rev.Last_Name__c = lstPrj.get(0).Last_Name__c;
            rev.Phone__c = lstPrj.get(0).Phone__c;
            rev.Email__c = lstPrj.get(0).Email__c;
            rev.Address__c = lstPrj.get(0).Address__c;
            rev.City__c = lstPrj.get(0).City__c;
            rev.State__c = lstPrj.get(0).State__c;
            rev.Zipcode__c = lstPrj.get(0).Zipcode__c;
            rev.Work_Scope_Review__c = lstPrj.get(0).Id;
            
            if(lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c != null)
                rev.Trade_Ally_Email__c = lstPrj.get(0).Energy_Assessment__r.Trade_Ally_Account__r.Email__c;
            
            rev.RecordTypeId =  devRecordTypeId;  
            upsert rev; 
     
            rev = [select Id,Name,Project__c,Status__c,Work_Scope_Review__c,Trade_Ally_Account__r.Name,Energy_Assessment__r.Name from Review__c where Id = :rev.Id limit 1];
        }
        else if(projId != null)
        {    
            List<Project_Review__c> prList = [select id, Review__c, Project__c 
                                              from Project_Review__c 
                                              where Project__c =: projId
                                              and Review__c != null
                                              and (Review__r.RecordType.Name = 'Work Scope Review'
                                              or Review__r.RecordType.Name = 'Work Scope Review Manual')];
            
            if(prList.size() > 0)
                return new PageReference('/apex/dsmtAddChangeOrder?id='+prList[0].Review__c+'&projid='+projId).setRedirect(true);
        }
        
        return null;
    }
    
    public PAgereference CreateChagneOrderItems()
    {
        List<Recommendation__c> lstRec = new List<Recommendation__c>();
        List<Change_Order_Line_Item__c> lstChangeOrderItems = new List<Change_Order_Line_Item__c >();
        
        
        if(dataSave != null && dataSave != ''){
            map<String, String> recQMap = new map<String, String>();
            list<String> splitedData = dataSave.split(';');
            
            for(string s: splitedData){
                recQMap.put(s.split('-')[0], s.split('-')[1]);
            }
            
            system.debug('recQMap :::::' + recQMap);
            
            List<Recommendation__c> recomList = [select id, Name, Recommendation_Scenario__c, Recommendation_Description__c, Quantity__c, Units__c,
                                                 isChecked__c, Recommendation_Scenario__r.Name, Inspected_Quantity__c, Change_Order_Quantity__c
                                                 from Recommendation__c 
                                                 where id in : recQMap.keyset()];
                                                 
            for(Recommendation__c r : recomList){
                Change_Order_Line_Item__c rec = new Change_Order_Line_Item__c();
                
                List<String> splitedQnty = recQMap.get(r.Id).split(',');
                
                if(rec.Change_Order_Quantity__c != decimal.valueOf(splitedQnty[0]) ||
                   rec.Original_Quantity__c     != decimal.valueOf(splitedQnty[1]) ||
                   rec.Inspected_Quantity__c    != decimal.valueOf(splitedQnty[2])){
                   
                    rec.Recommendation__c = r.Id;
                    rec.Change_Order_Quantity__c = splitedQnty[0] == null ? 0: integer.valueof(splitedQnty[0]);
                    rec.Original_Quantity__c     = splitedQnty[1] == null ? 0: integer.valueof(splitedQnty[1]);
                    rec.Inspected_Quantity__c    = splitedQnty[2] == null ? 0: integer.valueof(splitedQnty[2]);
                    rec.Review__c = reviewId;
                    rec.Change_Order_Review__c = rev.Id;
                    rec.Project__c = r.Recommendation_Scenario__c;
                    rec.Status__c = 'New';
                    
                    lstChangeOrderItems.add(rec);
                }
            }
            
            if(lstChangeOrderItems.size()>0)
                upsert lstChangeOrderItems;
        }
        
        return new Pagereference('/'+rev.Id);
    }
}