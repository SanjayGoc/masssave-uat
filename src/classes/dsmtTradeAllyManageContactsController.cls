public class dsmtTradeAllyManageContactsController {
    public static String REVIEW_TYPE = 'Employee';
    public String TAId{get;set;}
    public String DsmtConId{get;set;}
    public String RevId{get;set;}
    public Trade_Ally_Account__c taaccount{get;set;}
    public Review__c review{get;set;}
    public List<Attachment__c> attachlist{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}

    
    public Map<String,EmployeeModel> employeesMap{get{
        if(this.employeesMap == null){
            this.employeesMap = new Map<String,EmployeeModel>();
        }
        return this.employeesMap;
    }set;}
    public List<EmployeeModel> employees{get{
        return this.employeesMap.values();
    }}
    public DSMTracker_Contact__c newEmployee{get{ 
  
        if(this.newEmployee == null ){
            this.newEmployee = new DSMTracker_Contact__c(
                Trade_Ally_Type_PL__c = taaccount.Trade_Ally_Type__c,
                //Is_TA_Internal_Account__c = taaccount.Internal_Account__c,
                Name = null,
                First_Name__c=null,
                Last_Name__c=null,
                Title__c=null,
                Email__c=null
            );
        }
        return this.newEmployee;
    }set;}
    public boolean isEmailRequiredForNewEmployee{get{
            //Set<String> emailNotRequiredFor = new Set<String>{'Crew Member','Marketing','Electrician'};
            //return (this.newEmployee != null && this.newEmployee.Title__c != null) ? !emailNotRequiredFor.contains(this.newEmployee.Title__c) : false;
        if(this.newEmployee != null){
            this.newEmployee.recalculateFormulas();
            System.debug('--this.newEmployee.Portal_Role__c--' + this.newEmployee.Portal_Role__c);
        }
        Set<String> roles = new Set<String>{'Manager','Scheduler','Energy Specialist'};
        return (this.newEmployee != null && this.newEmployee.Title__c != null && this.newEmployee.Portal_Role__c != null && this.newEmployee.Portal_Role__c != '') ? roles.contains(this.newEmployee.Portal_Role__c) : false;  
    
    }}
     public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    
    public String attachType{get;set;}
    public boolean isError{get;set;}
    public boolean isSubmitReviewexist{get;set;}
    public boolean hasReviewAssigned{get;set;}
    public Review__c submittedReview{get;set;}
    public void rerender(){
        System.debug('TAId => '+TAId);
        System.debug('DsmtConId => '+DsmtConId);
        System.debug('RevId => '+RevId);
        System.debug('taaccount => '+taaccount);
        System.debug('review => '+review);
    }
    
    public dsmtTradeAllyManageContactsController(){
        
        isPortal = true;
        headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
        List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
            if(lstUser.size()>0){
                if(lstUser[0].IsPortalEnabled){
                    headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
                }
                else{
                    headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                    isPortal = false;
                }
            }
        
        
        TAId = ApexPages.currentPage().getParameters().get('taid');
        DsmtConId = ApexPages.currentPage().getParameters().get('dcid');
        RevId = ApexPages.currentPage().getParameters().get('id');
        
        init();
        System.debug('New Employee -- > ' + this.newEmployee);
    }
    
    public void init(){
        loadTradeallyAccount();
        isSubmitReviewexist = false;
        hasReviewAssigned = true;
        if(TAId != null){
            List<Review__c > reviewlist = [select id,name,status__c,OwnerId from Review__c where Trade_Ally_Account__c =: taaccount.Id and Dsmtracker_contact__c = null and Status__c = 'Submitted For Review' and Review_Type__c =:REVIEW_TYPE and Requested_By__c =: Userinfo.getuserid()];
            if(reviewlist.size() > 0){
                Review__c review = reviewlist.get(0);
                if(String.valueOf(review.OwnerId).startsWith('005')){ // UNDER REVIEW (Owner is USER)
                    isSubmitReviewexist = true;
                }else if(String.valueOf(review.OwnerId).startsWith('00G')){ // UNDER REVIEW (Owner is QUEUE)
                    //recallReview(review);
                    submittedReview = review;
                    hasReviewAssigned = false;
                }
            }
            System.debug('reviewList ---> ' + reviewlist);
        }
        loadReviewData();
    }
    
    public void recallReview(Review__c review){
        String revId = review.Id;
        String attachquery = dsmtFuture.getCreatableFieldsSOQL('Attachment__c','Status__c = \'Approved\'','');
        system.debug('--recallReview -> attachquery--'+attachquery);
        
        String additionalfields = '('+attachquery.replace(' Attachment__c',' Attachments__r')+')';
        system.debug('--recallReview -> additionalfields--'+additionalfields);
        
        String whereclause = 'status__c = \'Approved\' and Trade_Ally_Account__c = \''+taaccount.Id+'\'';
        String query = dsmtFuture.getCreatableFieldsSOQL('DSMTracker_Contact__c',whereclause,additionalfields);
        system.debug('--recallReview -> final query--'+query);
        List<DSMTracker_Contact__c > dsmtconlist = Database.query(query);
        System.debug('RECALL => dsmtconlist :: ' + dsmtconlist);
        
        attachquery = dsmtFuture.getCreatableFieldsSOQL('Attachment__c','Status__c = \'UnSubmitted\'','');
        additionalfields = '('+attachquery.replace(' Attachment__c',' Attachments__r')+')';
        whereclause = 'Review__c = \''+ review.Id+'\'';
        query = dsmtFuture.getCreatableFieldsSOQL('DSMTracker_Contact__c',whereclause,additionalfields);
        List<DSMTracker_Contact__c> submittedContactList = Database.query(query);
        Map<Id,DSMTracker_Contact__c> submittedContactMap = new Map<Id,DSMTracker_Contact__c>();
        for(DSMTracker_Contact__c submittedContact : submittedContactList){
            if(submittedContact.Dsmtracker_Contact__c != null){
                submittedContactMap.put(submittedContact.Dsmtracker_Contact__c , submittedContact);
            }
        }
        System.debug('RECALL => submittedContactMap :: ' + submittedContactMap);
        
        List<Dsmtracker_Contact__c> newdsmtlist = new List<Dsmtracker_Contact__c>();
        Map<String,List<Attachment__c>> newdsmtToAttachsMap = new Map<String,List<Attachment__c>>();
        for(Dsmtracker_Contact__c dsmt : dsmtconlist){
            if(submittedContactMap.containsKey(dsmt.Id)){
                Dsmtracker_Contact__c submittedContact = submittedContactMap.get(dsmt.Id);
                
                List<Attachment__c> newattachlist = newdsmtToAttachsMap.get(submittedContact.id);
                if(newattachlist == null){
                    newattachlist = new List<Attachment__c>();
                }
                List<Attachment__c> attachlist = dsmt.Attachments__r;
                
                List<Attachment__c> submittedAttachlist = submittedContact.Attachments__r;
                Map<String,Attachment__c> submittedAttachMap = new Map<String,Attachment__c>();
                for(Attachment__c submittedAttach : submittedAttachlist){
                    submittedAttachMap.put(submittedAttach.Shadow_attachment__c,submittedAttach);
                }
                
                if(attachlist.size() > 0){
                     for(Attachment__c attach : attachlist){
                         if(!submittedAttachMap.containsKey(attach.Id)){
                             Attachment__c newattach = attach.clone();
                             newattach.DSmtracker_Contact__c = null;
                             newattach.Status__c = 'UnSubmitted';
                             newattach.Trade_Ally_Account__c = null;
                             newattach.Review__c = revId;
                             newattach.Shadow_attachment__c = attach.Id;
                             newattach.OwnerId = Userinfo.getuserid();
                             
                             newattachlist.add(newattach);   
                         }
                     }   
                }
                newdsmtToAttachsMap.put(submittedContact.id,newattachlist);
            }
            else{
                DSmtracker_Contact__c newdsmt = dsmt.clone();
                newdsmt.Trade_Ally_Account__c = null;
                newdsmt.Review__c = revId;
                newdsmt.Contact__c = null;
                newdsmt.Account__c = null;
                newdsmt.Dsmtracker_Contact__c = dsmt.id;
                newdsmt.Status__c = 'UnSubmitted';
                newdsmt.Trade_Ally_Type_PL__c = this.taaccount.Trade_Ally_Type__c;
                newdsmt.Id = null;
                newdsmt.OwnerId = userinfo.getuserid();
                newdsmtlist.add(newdsmt);
                
                List<Attachment__c> newattachlist = newdsmtToAttachsMap.get(dsmt.id);
                if(newattachlist == null){
                    newattachlist = new List<Attachment__c>();
                }
                
                List<Attachment__c> attachlist = dsmt.Attachments__r;
                if(attachlist.size() > 0){
                     for(Attachment__c attach : attachlist){
                         Attachment__c newattach = attach.clone();
                         newattach.DSmtracker_Contact__c = null;
                         newattach.Status__c = 'UnSubmitted';
                         newattach.Trade_Ally_Account__c = null;
                         newattach.Review__c = revId;
                         newattach.Shadow_attachment__c = attach.Id;
                         newattach.OwnerId = Userinfo.getuserid();
                         
                         newattachlist.add(newattach);
                     }   
                }
                newdsmtToAttachsMap.put(dsmt.id,newattachlist);
            }
        }
        
        if(newdsmtlist.size() > 0){
            INSERT newdsmtlist;
            List<Attachment__c> newattachlist = new List<Attachment__c>();
            for(Dsmtracker_Contact__c newdsmt : newdsmtlist){  
                List<Attachment__c> attachlist = newdsmtToAttachsMap.get(newdsmt.dsmtracker_contact__c);
                if(attachlist != null){
                    for(Attachment__c newattach : attachlist){
                        newattach.Dsmtracker_Contact__c = newdsmt.id;
                        newattachlist.add(newattach);
                    }
                }
            }
            if(newattachlist.size() > 0){
                INSERT newattachlist;
            }
        }
        review.OwnerId = UserInfo.getUserId();
        review.Status__c = 'Draft';
        update review;
    }
    
    public void loadTradeallyAccount(){
        if(TAId != null){
            String query = dsmtFuture.getCreatableFieldsSOQL('Trade_Ally_Account__c','id=:TAId','');
            System.debug('loadTradeallyAccount -> query => ' + query);
            List<Trade_Ally_Account__c> talist = Database.query(query);
            if(talist.size() > 0){
                taaccount = talist.get(0);
            }
            System.debug('loadTradeallyAccount -> taaccount => ' + taaccount);
        }
    }
    public void loadReviewData(){
        if(RevId != null){
            String query = dsmtFuture.getCreatableFieldsSOQL('Review__c','id=:RevId','');
            List<Review__c> rlist = Database.query(query);
            if(rlist.size() > 0){
                review = rlist.get(0);
                TAId = review.Trade_Ally_Account__c;
                if(taaccount == null){
                    loadTradeallyAccount();
                }
            }else{
                review = new Review__c();
            }
            loadEmployees();   
        }
    }
    
    public PageReference checkReview(){
        if(hasReviewAssigned == false && submittedReview != null){
            recallReview(submittedReview);
        }
        if (!apexpages.currentPage().getParameters().containsKey('id') && isSubmitReviewexist == false) {
            String reviewId = null;
            
            List<Review__c> reviewlist;
            if(taaccount != null){
                reviewlist = [select id,name,status__c from Review__c where Trade_Ally_Account__c =: taaccount.Id and Dsmtracker_contact__c = null and Status__c = 'Draft' and Review_Type__c =:REVIEW_TYPE and Requested_By__c =: Userinfo.getuserid()];
                system.debug('--reviewlist--'+reviewlist);
                if(reviewlist.size() > 0){
                    reviewId = reviewlist.get(0).Id;
                    if(hasReviewAssigned == false){
                        recallReview(reviewlist.get(0));
                    }
                }   
            }
            
            if(reviewId == null){
                Schema.DescribeSObjectResult d = Schema.SObjectType.Review__c;
                Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
                
                Review__c newReview = new Review__c(status__c ='Draft',Review_Type__c = REVIEW_TYPE);
                newReview.Requested_by__c = Userinfo.getuserid();
                boolean isInsert = false;
                
                if(taaccount != null){
                    Schema.RecordTypeInfo rtByName =  rtMapByName.get('Trade Ally Account');
                    
                    newReview.Trade_Ally_Account__c = taaccount.id;
                    newReview.RecordTypeId = rtByName.getRecordTypeId();
                    isInsert =true;
                }
                
                if(isInsert == true){
                    insert newReview;
                    reviewId = newReview.id;
                    copyContacts(reviewId);
                }   
            }
            PageReference pg = Page.dsmtTradeAllyManageContacts;
            pg.getParameters().put('id', reviewId); 
            pg.setRedirect(true);
            return pg;
        }
        return null;
    }
    
    public void saveNewEmployee(){
        if(this.newEmployee != null){
            this.newEmployee.Name = this.newEmployee.First_Name__c +' '+ this.newEmployee.Last_Name__c;
            //this.newEmployee.Trade_Ally_Account__c = this.taaccount.Id;
            //this.newEmployee.Account__c = this.taaccount.Account__c;
            //this.newEmployee.Trade_Ally_Type_Text__c = this.taaccount.Trade_Ally_Type__c;
            this.newEmployee.Trade_Ally_Type_PL__c = this.taaccount.Trade_Ally_Type__c;
            this.newEmployee.Company_Name__c = this.taaccount.Name;
            this.newEmployee.Review__c = RevId;
            this.newEmployee.Status__c = 'Submitted';
            
            insert this.newEmployee;
            if(this.newEmployee.Id != null){
                this.newEmployee = null;
            }
            loadEmployees();
        }
    }
    
    public void copyContacts(String revId){
        if(revId != null){
             
             String attachquery = dsmtFuture.getCreatableFieldsSOQL('Attachment__c','Status__c = \'Approved\'','');
             system.debug('--attachquery--'+attachquery);
             
             String additionalfields = '('+attachquery.replace(' Attachment__c',' Attachments__r')+')';
             system.debug('--additionalfields--'+additionalfields);
             
             String whereclause = 'status__c = \'Approved\' and Trade_Ally_Account__c = \''+taaccount.Id+'\'';
             String query = dsmtFuture.getCreatableFieldsSOQL('DSMTracker_Contact__c',whereclause,additionalfields);
             system.debug('--final query--'+query);
             List<DSMTracker_Contact__c > dsmtconlist = Database.query(query);
             
             List<Dsmtracker_Contact__c> newdsmtlist = new List<Dsmtracker_Contact__c>();
             Map<String,List<Attachment__c>> newdsmtToAttachsMap = new Map<String,List<Attachment__c>>();
             for(Dsmtracker_Contact__c dsmt : dsmtconlist){
                 DSmtracker_Contact__c newdsmt = dsmt.clone();
                 newdsmt.Trade_Ally_Account__c = null;
                 newdsmt.Review__c = revId;
                 newdsmt.Contact__c = null;
                 newdsmt.Account__c = null;
                 
                 newdsmt.Dsmtracker_Contact__c = dsmt.id;
                 newdsmt.Status__c = 'UnSubmitted';
                 //newdsmt.trade_Ally_Type_Text__c = this.taaccount.Trade_Ally_Type__c;
                 newdsmt.Trade_Ally_Type_PL__c = this.taaccount.Trade_Ally_Type__c;
                 newdsmt.Id = null;
                 newdsmt.OwnerId = userinfo.getuserid();
                 newdsmtlist.add(newdsmt);
                 
                 List<Attachment__c> newattachlist = newdsmtToAttachsMap.get(dsmt.id);
                 if(newattachlist == null){
                    newattachlist = new List<Attachment__c>();
                 }
                 
                 List<Attachment__c> attachlist = dsmt.Attachments__r;
                 if(attachlist.size() > 0){
                     for(Attachment__c attach : attachlist){
                         Attachment__c newattach = attach.clone();
                         newattach.DSmtracker_Contact__c = null;
                         newattach.Status__c = 'UnSubmitted';
                         newattach.Trade_Ally_Account__c = null;
                         newattach.Review__c = revId;
                         newattach.Shadow_attachment__c = attach.Id;
                         newattach.OwnerId = Userinfo.getuserid();
                         
                         newattachlist.add(newattach);
                     }   
                 }
                 
                 newdsmtToAttachsMap.put(dsmt.id,newattachlist);
                 
             }
             system.debug('--newdsmtToAttachsMap--'+newdsmtToAttachsMap);
             
             if(newdsmtlist.size() > 0){
                 insert newdsmtlist;
                 
                 List<Attachment__c> newattachlist = new List<Attachment__c>();
                 for(Dsmtracker_Contact__c newdsmt : newdsmtlist){
                     
                     List<Attachment__c> attachlist = newdsmtToAttachsMap.get(newdsmt.dsmtracker_contact__c);
                     if(attachlist != null){
                         for(Attachment__c newattach : attachlist){
                             newattach.Dsmtracker_Contact__c = newdsmt.id;
                             newattachlist.add(newattach);
                         }
                     }
                 }
                 system.debug('--newattachlist--'+newattachlist);
                 
                 if(newattachlist.size() > 0){
                    insert newattachlist;
                 }
             }
        }
        
    }
    
    public void saveAsDraft(){
        if(review != null){
            update review;
        } 
    }
    
    public void loadEmployees(){
        if(RevId != null){
            List<DSMTracker_Contact__c > dsmtconlist = [
                SELECT Id,Name,First_Name__c, Last_Name__c, Email__c, Trade_Ally_Type_PL__c,Title__c,Title_Formula__c,CSL_Attached__c,BPI_Building_Analyst_Attached__c,BPI_Envelope_Attached__c,Active__c,Trade_Ally_Account__c,Trade_Ally_Account__r.Internal_Account__c,Portal_Role__c,
                        (
                            SELECT File_Url__c, File_Download_Url__c, CreatedDate, Attachment_Type__c,Attachment_Name__c, Application_Type__c,Status__c,Expires_On__c 
                            FROM Attachments__r 
                            ORDER BY createddate DESC
                        ),
                        (
                            SELECT Id,Is_Open__c,Name,Online_Portal_Title__c,Online_Portal_Text__c,Attachment_Needed__c,Required_Attachment_Type__c,Type__c,Soft_Exception__c,Lic_Number__c,Cert_Number__c,Expiry_date__c,Request_Date_Of_Hire__c,Disposition__c
                            FROM Exceptions__r 
                            WHERE Internal__c = FALSE AND Is_Open__c !=0 ORDER BY Priority__c NULLS Last
                        )
                FROM DSMTracker_Contact__c WHERE Review__c =:RevId];
            System.debug('dsmtconlist --> ' + dsmtconlist);
            if(dsmtconlist.size() > 0){
                employeesMap = new Map<String,EmployeeModel>();
                for(Dsmtracker_Contact__c dsmt : dsmtconlist){
                   EmployeeModel model = new EmployeeModel();
                    
                    model.Employee = new Dsmtracker_Contact__c(
                        Id = dsmt.Id,
                        Trade_Ally_Type_PL__c = dsmt.Trade_Ally_Type_PL__c ,
                        Is_TA_Internal_Account__c = dsmt.Trade_Ally_Account__r.Internal_Account__c,
                        Name = dsmt.Name,
                        First_Name__c=dsmt.First_Name__c,
                        Last_Name__c=dsmt.Last_Name__c,
                        Title__c=dsmt.Title__c,
                        Email__c=dsmt.Email__c,
                        CSL_Attached__c= dsmt.CSL_Attached__c,
                        BPI_Building_Analyst_Attached__c= dsmt.BPI_Building_Analyst_Attached__c,
                        Trade_Ally_Account__c= dsmt.Trade_Ally_Account__c,
                        BPI_Envelope_Attached__c=dsmt.BPI_Envelope_Attached__c,
                        Active__c = dsmt.Active__c
                    );
                    model.Employee.recalculateFormulas();
                    
                    
                    
                    System.debug('dsmt.Trade_Ally_Account__r.Internal_Account__c--'+dsmt.Trade_Ally_Account__r.Internal_Account__c);
                    System.debug('model.Employee--' + model.Employee);
                   List<Attachment__c> attachlist = dsmt.Attachments__r;
                   if(attachlist == null){
                       attachlist = new List<Attachment__c>();
                   }
                   List<Exception__c> exceptionList = dsmt.Exceptions__r;
                    if(exceptionList == null){
                       exceptionList = new List<Exception__c>();
                   }
                   model.attachmentList = attachlist;
                    model.exceptionList = exceptionList;
                   employeesMap.put(dsmt.Id,model);
               }
            }
            
        }
    }
    
    public PageReference saveAndSubmit(){
    
         isError = false;
         boolean isCrewchief = false;
         boolean isCSL = false;
         boolean isBPIbuildingAnalyst = false;
         boolean isbuildingEnvelop = false;
         boolean crewChiefRequired = taaccount.Trade_Ally_Type__c == 'HPC' || taaccount.Trade_Ally_Type__c == 'IIC';
         boolean buildingEnvelopRequired = taaccount.Trade_Ally_Type__c == 'HPC';
          Integer crewChiefCount = 0, buildingEnvelopCount = 0;
        
         for(EmployeeModel modal : employees){
             Dsmtracker_Contact__c employee = modal.Employee;
             system.debug('--employee --'+employee );
             List<Exception__c> explist = modal.exceptionList;
             if(taaccount.Trade_Ally_Type__c == 'HPC' || taaccount.Trade_Ally_Type__c == 'IIC'){
                 if(employee.Title_Formula__c == 'Crew Chief'){
                     isCrewchief = true;
                      crewChiefCount++;
                 }
                 
             }else{
                 isCrewchief = true;
                 
             }
             
             system.debug('--taaccount.Trade_Ally_Type__c--'+taaccount.Trade_Ally_Type__c);
             if(taaccount.Trade_Ally_Type__c == 'HPC'){
                 if(employee.BPI_Building_Analyst_Attached__c == false){
                    for(Exception__c exp : explist){
                       if(exp.Required_Attachment_Type__c == 'BPI Building Analyst'){
                          if(exp.Request_Date_Of_Hire__c == null){
                              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Request Date of hire must be required If document : '+exp.Online_Portal_Title__c+' is not upload for Employee : '+modal.Employee.Name);
                              ApexPages.addMessage(myMsg);
                              isError = true;
                          }else{
                              Integer monthdiff = exp.Request_Date_Of_Hire__c.monthsBetween(Date.Today());
                              system.debug('--monthdiff--:'+monthdiff);
                              if(monthdiff > 6 || monthdiff < -6){
                                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Request Date of hire must be within 6 month for document : '+exp.Online_Portal_Title__c+' for Employee : '+modal.Employee.Name);
                                  ApexPages.addMessage(myMsg);
                                  isError = true;
                              }
                          }
                       }
                    }
                 }
                 
                 if(employee.BPI_Envelope_Attached__c == true){
                    isbuildingEnvelop =true;
                    buildingEnvelopCount++;
                 }
                 if(employee.CSL_Attached__c){
                    isCSL = true;
                 }
             }else{
                isBPIbuildingAnalyst = true;
                isbuildingEnvelop = true;
                isCSL = true;
             }
             
             for(Exception__c exp : explist){
               if(exp.Is_Open__c != 0 && exp.soft_Exception__c == false){
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Required Document is missing for Employee : '+modal.Employee.Name);
                  ApexPages.addMessage(myMsg);
                  isError = true;
               }
             }
            
         }
         
         if(crewChiefRequired == true && crewChiefCount == 0){//if(isCrewchief == false){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Atleast one crew chief is required.');
             ApexPages.addMessage(myMsg);
             isError = true;
         }
         
         if(!isCSL){
            isError = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Atleast one CSL document is required.');
            ApexPages.addMessage(myMsg);
         }
         
         if(buildingEnvelopRequired == true && buildingEnvelopCount == 0){//if(!isbuildingEnvelop){
            isError = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Atleast one Building Envelope document is required.');
            ApexPages.addMessage(myMsg);
         }
         
         if(!isError){
             set<String> deleteattachids = new Set<String>();
             for(EmployeeModel emp :employees){
                for(Attachment__c attach :emp.attachmentList){
                   if(attach.Status__c == 'UnSubmitted'){
                      deleteattachids.add(attach.id);
                   }
                }
            }
            
            if(deleteattachids.size() > 0){
               delete [select id from attachment__c where id in: deleteattachids];
            }    
            
            String whereclause = 'Review__c=\''+review.Id+'\' AND Status__c!=\'UnSubmitted\'';
            String query = dsmtFuture.getCreatableFieldsSOQL('Attachment__c',whereclause,'');
            List<Attachment__c> submittedAttachments = database.query(query);
            Set<Id> contactIdsToSubmit = new Set<Id>();
            if(submittedAttachments.size() > 0){
                for(Attachment__c  attachment :submittedAttachments){
                    contactIdsToSubmit.add(attachment.DSMTracker_Contact__c);
                }
                if(contactIdsToSubmit.size() > 0){
                    List<DSMTracker_Contact__c> empToSubmit = [SELECT Id, Name, Status__c FROM DSMTracker_Contact__c WHERE Id IN:contactIdsToSubmit];
                    for(DSMTracker_Contact__c emp: empToSubmit){
                        emp.Status__c = 'Submitted';
                    }
                    update empToSubmit;
                }
            }
            
            delete [SELECT Id FROM DSMTracker_Contact__c WHERE Review__c =:review.Id AND Status__c != 'Submitted'];
            
            List<Dsmtracker_Contact__c> dsmtlist = [select id from Dsmtracker_contact__c where review__c =: review.id];
            if(dsmtlist.size() > 0){
                review.Status__c = 'Submitted For Review';
                review.Requested_By__c = Userinfo.getuserid();
                update review;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Your request has been successfully submitted for review..'));    
            }else{
               delete review;
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'There are no any updates in your request..'));    
            }
         }
         return null;
    }
    
    
    public String employeeid{get;set;}
    public void updateDSMTExceptions(){
        system.debug('--employeeid--'+employeeid);
        system.debug('--attachType--'+attachType);
        isError = false;
        for(EmployeeModel modal : employees){
            if(modal.Employee.id == employeeid){
                List<Exception__c> explist = modal.exceptionList;
                if(explist != null){
                    for(Exception__c exp : explist){
                      if(exp.Expiry_date__c != null && exp.Expiry_date__c < Date.Today()){
                           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Expiry date cannot be past date for document : '+exp.Online_Portal_Title__c+' for Employee : '+modal.Employee.Name);
                           ApexPages.addMessage(myMsg);
                           isError = true;
                      }
                      if(exp.Request_Date_Of_Hire__c != null){
                          Integer monthdiff = exp.Request_Date_Of_Hire__c.monthsBetween(Date.Today());
                          system.debug('--monthdiff--:'+monthdiff);
                          if(monthdiff > 6 || monthdiff < -6){
                              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Request Date of hire must be within 6 month for document : '+exp.Online_Portal_Title__c+' for Employee : '+modal.Employee.Name);
                              ApexPages.addMessage(myMsg);
                              isError = true;
                          }
                      }
                    }  
                    if(!isError)
                       update explist;
                }
            } 
        }
    }
    
    public String toggleEmployeeId{get;set;}
    public void toggleActiveInactiveEmployee(){
        if(this.toggleEmployeeId != null){
            EmployeeModel empModel = employeesMap.get(toggleEmployeeId);
            empModel.Employee.Active__c = !empModel.Employee.Active__c;
            empModel.Employee.Status__c = 'Submitted';
            update empModel.Employee;
        }
    }
    
    public class EmployeeModel{
        public Dsmtracker_Contact__c Employee{get;set;}
        public List<Exception__c> exceptionList{get;set;}
        public List<Attachment__c> attachmentList{get;set;}
        public EmployeeModel(){}
        public Integer getExceptionListSize(){
            return this.exceptionList.size();
        }
        public boolean isEmailRequired{get;set;}
        //public boolean isEmailRequired{get{
            //Set<String> emailNotRequiredFor = new Set<String>{'Crew Member','Marketing','Electrician'};
            //return (this.Employee != null && this.Employee.Title__c != null) ? !emailNotRequiredFor.contains(this.Employee.Title__c) : false;
            //if(this.Employee != null){
               // this.Employee.recalculateFormulas();
                //System.debug('--this.Employee.Portal_Role__c--' + this.Employee.Portal_Role__c);
           // }
           // Set<String> roles = new Set<String>{'Manager','Scheduler','Energy Specialist'};
           // return (this.Employee != null && this.Employee.Title__c != null && this.Employee.Portal_Role__c != null && this.Employee.Portal_Role__c != '') ? roles.contains(this.Employee.Portal_Role__c) : false;  
        
        //}}
        public void checkFieldValidations(){
            if(this.Employee != null){
                this.Employee.recalculateFormulas();
                System.debug('--this.Employee.Portal_Role__c--' + this.Employee.Portal_Role__c);
            }
            Set<String> roles = new Set<String>{'Manager','Scheduler','Energy Specialist'};
            this.isEmailRequired = (this.Employee != null && this.Employee.Title__c != null && this.Employee.Portal_Role__c != null && this.Employee.Portal_Role__c != '') ? roles.contains(this.Employee.Portal_Role__c) : false;  
        }
        public void save(){
            this.Employee.Status__c = 'Submitted';
            update this.Employee;
        }
        public void toggleActive(){            
            this.Employee.Active__c = !this.Employee.Active__c;
            this.Employee.Status__c = 'Submitted';
            system.debug('@@@@@Employee'+Employee);
            update this.Employee;
        }
    }
}