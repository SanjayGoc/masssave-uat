public class ProjectExceptions
{
   public static boolean isStopValidateException = false;
   public static void InsertExceptionsOnRR(List<Recommendation_Scenario__c> RRlist){
       
        Map<String,Exception_Template__c> mapExt = new Map<String,Exception_Template__c>();     
        //Gets all expection with that check box checked.
        for(Exception_Template__c ext : [select id,name,Reference_ID__c,Type__c,Online_Portal_Title__c,Online_Portal_Text__c,Online_Application_Section__c,Required_Attachment_Type__c,Internal__c,Exception_Message__c,Outbound_Message__c,Short_Rejection_Reason__c,Attachment_Needed__c from Exception_template__c where Automatic__c = true and Active__c = true and Project_Level_Message__c = true ]) {
          mapExt.put(ext.Reference_Id__c,ext);
        } 
        
        Set<Id> rrids = new Set<id>();
        for(Recommendation_Scenario__c rr :RRlist){
             rrids.add(rr.id);   
        }
        
        Map<String,Exception__c> mapExceptions = new Map<String,Exception__c>();
        for(Exception__c Ex :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Project__c, Project__r.id FROM Exception__c 
                                WHERE Project__r.id IN:rrids AND Disposition__c  != 'Resolved']){    
            mapExceptions.put(Ex.Project__c+'-'+Ex.Exception_Template__r.Reference_ID__c,Ex);     
        }      
        
        List<Exception__c> ExceptionsCreated = new List<Exception__c>();
          
        for(Recommendation_Scenario__c RRNew : RRlist) {
            
            List<string> AutoExceptionsList = new List<string>();
            
            for(String refId : mapExt.keyset()){
                Exception_Template__c temp = mapExt.get(refId);
               
                if(temp != null){
                      
                     /*if(refId == 'Project-001' && RRNew.Certification_of_Completion__c==false){
                        If(mapExceptions.get(RRNew.id+'-Project-001')==null) 
                            AutoExceptionsList.add('Project-001'); 
                     }*/
                     
                     if(refId == 'Project-002' && RRNew.Status__c=='Approved For Invoice Submission'){
                        If(mapExceptions.get(RRNew.id+'-Project-002')==null) 
                            AutoExceptionsList.add('Project-002'); 
                     }                     
                     
                     if(refId == 'Project-003' && RRNew.Signature_Date__c==null){
                        If(mapExceptions.get(RRNew.id+'-Project-003')==null) 
                            AutoExceptionsList.add('Project-003'); 
                     }  
                     if(refId == 'Project-004' && RRNew.Energy_Assessment__r.SMA__c && !RRNew.SMA_photo_Uploaded__c){
                        If(mapExceptions.get(RRNew.id+'-Project-004')==null) 
                            AutoExceptionsList.add('Project-004'); 
                     }
                     if(refId == 'Project-005' && RRNew.Energy_Assessment__r.Building_Specification__r.Year_Built__c != null &&
                             Integer.valueOf(RRNew.Energy_Assessment__r.Building_Specification__r.Year_Built__c) <= 1978 && !RRNew.Enclosed_Cavity_Uploaded__c){
                        If(mapExceptions.get(RRNew.id+'-Project-005')==null) 
                            AutoExceptionsList.add('Project-005'); 
                     }
                     if(refId == 'Project-006' && RRNew.Energy_Assessment__r.Building_Specification__r.Year_Built__c != null &&
                             Integer.valueOf(RRNew.Energy_Assessment__r.Building_Specification__r.Year_Built__c) <= 1978 && !RRNew.Lead_Safety_Uploaded__c){
                        If(mapExceptions.get(RRNew.id+'-Project-006')==null) 
                            AutoExceptionsList.add('Project-006'); 
                     }
                    
               }
                
           }
           
            system.debug('--AutoExceptionsList--'+AutoExceptionsList);
            for(string autolist :AutoExceptionsList){
                if(mapExt.get(autolist) !=null) {
                    Exception_Template__c Exceptiontemp = mapExt.get(autolist);
                    Exception__c ExceptionstoAdd = new Exception__c(); 
                    ExceptionstoAdd.Internal__c = Exceptiontemp.Internal__c; 
                    ExceptionstoAdd.Exception_Message__c= Exceptiontemp.Exception_Message__c; 
                    ExceptionstoAdd.Outbound_Message__c=Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Project__c=  RRNew.Id;
                    ExceptionstoAdd.Energy_Assessment__c = RRNew.Energy_Assessment__c ;
                    ExceptionstoAdd.Exception_Template__c = Exceptiontemp.id;
                    ExceptionstoAdd.FInal_Outbound_Message__c = Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Short_Rejection_Reason__c = Exceptiontemp.Short_Rejection_Reason__c ;
                    ExceptionstoAdd.Automated__c = true;
                    ExceptionstoAdd.OwnerId =RRNew.OwnerId;
                    ExceptionstoAdd.Attachment_Needed__c = Exceptiontemp.Attachment_Needed__c;
                    ExceptionstoAdd.Required_Attachment_Type__c = Exceptiontemp.Required_Attachment_Type__c;
                    ExceptionstoAdd.Online_Application_Section__c = Exceptiontemp.Online_Application_Section__c;
                    ExceptionstoAdd.Type__c = Exceptiontemp.Type__c;
                    //ExceptionstoAdd.Automatic_Communication__c = Exceptiontemp.Automatic_Communication__c;
                    //ExceptionstoAdd.Automatic_Rejection__c = Exceptiontemp.Automatic_Rejection__c;
                    //ExceptionstoAdd.Move_to_Incomplete_Queue__c = Exceptiontemp.Move_to_Incomplete_Queue__c;
                    ExceptionstoAdd.Online_Portal_Title__c  = Exceptiontemp.Online_Portal_Title__c;
                    ExceptionstoAdd.Online_Portal_Text__c = Exceptiontemp.Online_Portal_Text__c;
                    ExceptionsCreated.add(ExceptionstoAdd);    
                }
            }     
       }
        
        system.debug('--ExceptionsCreated--'+ExceptionsCreated);
        ProjectExceptions.isStopValidateException = true;
        if(ExceptionsCreated.size()>0){
           insert ExceptionsCreated;  
        }   
   }
   
   public static void CheckUpdateExceptions(List<Recommendation_Scenario__c> RRlist , Map<Id,Recommendation_Scenario__c> oldmap){
        Map<string,List<Exception__c>> ExceptionsMap = new Map<string,List<Exception__c>>();
        for(Exception__c Exceptions :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Project__c, Project__r.id FROM Exception__c 
                                WHERE Project__r.id=:oldmap.keyset() AND Disposition__c  != 'Resolved']){
                List<Exception__c> lsExceptions = ExceptionsMap.get(Exceptions.Project__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c);
                if(lsExceptions  == NULL)
                    lsExceptions  = new List<Exception__c>();
                lsExceptions.add(Exceptions );
                ExceptionsMap.put(Exceptions.Project__c +'-'+Exceptions.Exception_Template__r.Reference_ID__c,lsExceptions);        
        }
        
         List<Exception__c> ExceptionstoUpdate = new List<Exception__c>();
         Set<Id> RRId = new Set<Id>();
         for(Recommendation_Scenario__c RR : RRlist){
              RRId.add(RR.Id);        
         }
         
         for(Recommendation_Scenario__c rr : RRlist){
             
                //String keyValue = rr.id + '-Project-001';
                /* if(ExceptionsMap.containskey(rr.id + '-Project-001')){  
                     if(rr.Certification_of_Completion__c== true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-Project-001')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                } */
                
                if(ExceptionsMap.containskey(rr.id + '-Project-002')){  
                     if(rr.Status__c!='Approved For Invoice Submission'){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-Project-002')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.id + '-Project-003')){  
                     if(rr.Signature_Date__c!=null){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-Project-003')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                 if(ExceptionsMap.containskey(rr.id + '-Project-004')){  
                     if(rr.SMA_photo_Uploaded__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-Project-004')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                 if(ExceptionsMap.containskey(rr.id + '-Project-005')){  
                     if(rr.Enclosed_Cavity_Uploaded__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-Project-005')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                 if(ExceptionsMap.containskey(rr.id + '-Project-006')){  
                     if(rr.Lead_Safety_Uploaded__c == true){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-Project-006')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
            }
         
          ProjectExceptions.isStopValidateException = true;
            system.debug('--ExceptionstoUpdate--'+ExceptionstoUpdate);
            if(ExceptionstoUpdate.size()>0){
                update ExceptionstoUpdate;
            }
   }
}