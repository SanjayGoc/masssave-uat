public class dsmtSyncWorkTeamsController{
 
    // Constructor - this only really matters if the autoRun function doesn't work right
    public dsmtSyncWorkTeamsController() {
    }
     
    // Code we will invoke on page load.
    public PageReference autoRun() {
 
        String EmpId = ApexPages.currentPage().getParameters().get('Id');
 
        dsmtCallOut.SyncWorkTeams(EmpId);
 
        // Redirect the user back to the original page
        PageReference pageRef = new PageReference('/' + EmpId);
        pageRef.setRedirect(true);
        return pageRef;
 
    }
 
}