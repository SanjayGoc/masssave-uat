public class FloorTriggerHandler extends TriggerHandler implements ITrigger{
    
    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            calculateDefaultValues(trigger.new,trigger.isInsert);
            //DSST-6924: Changes made by DeveloperName – Begin
            dsmtRecursiveTriggerHandler.preventFloorUpdateAfterInsert = true;
        } 
        else if (trigger.isUpdate && dsmtRecursiveTriggerHandler.preventFloorUpdateAfterInsert == false)
        {
            //DSST-6924: Changes made by DeveloperName – End
            //Here we will call before update actions
             calculateDefaultValues(trigger.new,trigger.isInsert);
        } 
        else if (trigger.isDelete) 
        {
            deleteLayers(trigger.oldmap.keyset());
        } 
        else if (trigger.isUndelete) 
        {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) 
        {
            //Here we will call after insert actions
            //calculateDefaultValues(trigger.new, (Map<Id, Floor__c>) trigger.oldmap);
            dsmtFloorBAScfm50AIAU(trigger.new, (Map<Id, Floor__c>) trigger.oldmap, trigger.isInsert, trigger.isUpdate);
            CreateDefaultLayer(trigger.new);
        } 
        else if (trigger.isUpdate) 
        {
            //Here we will call after update actions
            //dsmtFloorBAScfm50AIAU(trigger.new, (Map<Id, Floor__c>) trigger.oldmap, trigger.isInsert, trigger.isUpdate);
        } 
        else if (trigger.isDelete) 
        {
            //Here we will call after delete actions
        } 
        else if (trigger.isUndelete) 
        {
            //Here we will call after undelete actions
        }
    }

    private static void ManageActuals(Floor__c floor)
    {
        if(floor.ActualArea__c <=0) floor.ActualArea__c=null;
        if(floor.ActualPerimeter__c <=0) floor.ActualPerimeter__c=null;
        if(floor.ActualProjectedArea__c<=0)floor.ActualProjectedArea__c=null;
        if(floor.ActualInsulationAmount__c == '') floor.ActualInsulationAmount__c = null;
    }

    private static void CreateDefaultLayer(List<Floor__c> floorList)
    {
        Map<Id,string> recTypeMap = dsmtHelperClass.GetRecordType();

        dsmtEAModel.Surface bp = new dsmtEAModel.Surface();        
        bp.recordTypeMap = recTypeMap;

        Set<Id> setCeilId = new Set<Id>();
        Set<Id> EaId = new Set<Id>();
        Set<Id> setTETId = new Set<Id>();
        
        integer skipCount =0;

        for(Floor__c te : floorList)
        {
            if(te.IsFromFieldTool__c) 
            {
                skipCount +=1;
                continue;
            }

            if(te.Thermal_Envelope_Type__c !=null)
                setTETId.add(te.Thermal_Envelope_Type__c);
        }

        if(setTETId ==null || setTETId.size() ==0) return;

        if(skipCount == floorList.size()) return;

        String query = 'select ' + dsmtHelperClass.sObjectFields('Thermal_Envelope_Type__c') +' Id from Thermal_Envelope_Type__c where Id In :setTETId order by CreatedDate Asc ';

        Map<Id,Thermal_Envelope_Type__c> tetMap = new Map<Id,Thermal_Envelope_Type__c> ((List<Thermal_Envelope_Type__c>)database.query(query));
        
        if(tetMap ==null || tetMap.size()==0)return;

        for(Floor__c te : floorList)
        {
            if(te.IsFromFieldTool__c) continue;
            if(te.Thermal_Envelope_Type__c ==null) continue;

            bp.teType = tetMap.get(te.Thermal_Envelope_Type__c);
            bp.floor = te;

            dsmtSurfaceHelper.CreateNewLayers(bp);
        }

    }
    
    private static void calculateDefaultValues(list<Floor__c> newList, boolean isInsert)
    {           
        Set<Id> EaId = new Set<Id>();
        Set<Id> setTETId = new Set<Id>();

        for(integer i=0, j= newList.size(); i < j; i++)
        {
            Floor__c te = newList[i];
            if(te.IsFromFieldTool__c) continue;

            if(te.Thermal_Envelope_Type__c !=null)
                setTETId.add(te.Thermal_Envelope_Type__c);     
        }

        if(setTETId ==null || setTETId.size() ==0) return;

        Map<Id,Thermal_Envelope_Type__c> tetMap = new Map<Id,Thermal_Envelope_Type__c> ([Select Id, Energy_Assessment__c From 
        Thermal_Envelope_Type__c Where Id in :setTETId]);

        if(tetMap ==null || tetMap.size() ==0) return;

        For(Id key : setTETId)
        {
            if(tetMap.get(key)==null)continue;
            if(tetMap.get(key).Energy_Assessment__c ==null)continue;
            EaId.add(tetMap.get(key).Energy_Assessment__c);
        }

        dsmtEAModel.Surface bp = dsmtEAModel.InitializeBuildingProfile(EaId);

        if(bp==null) return;
                
            
        if(bp.teList !=null && bp.teList.size() >0)
        {
            bp.AtticList  = new List<Thermal_Envelope_Type__c>();
            for(Thermal_Envelope_Type__c te : bp.teList)
            {   
                if(bp.recordTypeMap.get(te.RecordTypeID) == 'Attic')
                {
                    bp.AtticList.add(te);
                }
            }    
        }

         // consutruct temprature and building model
        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        /// update base model first
       // bp = dsmtBuildingModelHelper.UpdateBaseBuildingThermal(bp);
       
        bp = dsmtBuildingModelHelper.UpdateThermalAreaModel(bp);
        
        if(bp.ai !=null)
            bp.ai = dsmtBuildingModelHelper.UpdateAirFlowModel(bp.ai, bp).clone(true,true,true,true);

        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        //system.debug('current model>>>'+JSON.serialize(bp));

        integer floorCount = newList.size();
        if(isInsert)
        {
            if(bp.floorList ==null) bp.floorList = new List<Floor__c>();

            For(integer i=0 ;i< floorCount; i++)
            {
                bp.floorList.add(newList[i]);
            }
        }
        else 
        {
            For(integer i=0 ;i< floorCount; i++)
            {
                if(bp.floorList ==null) bp.floorList = new List<Floor__c>();

                if(bp.floorList.size() ==0)
                {
                    bp.floorList.add(newList[i]);
                }
                else 
                {
                    boolean isFound=false;
                    for(integer j=0; j<bp.floorList.size();j++)
                    {
                        if(bp.floorList[j].Id==newList[i].Id)
                        {
                            bp.floorList[j] = newList[i];
                            isFound=true;
                            break;
                        }
                    }

                    if(!isFound)
                    {
                        bp.floorList.add(newList[i]);
                    }
                }
            }    
        }

        tetMap = new Map<Id,Thermal_Envelope_Type__c>();

        if(bp.teList !=null && bp.teList.size()>0)
        {
            for(integer i=0; i<bp.teList.size();i++)
            {
                tetMap.put(bp.teList[i].Id,bp.teList[i]);
            }
        }

        for(integer i=0, j = newList.size(); i < j; i++)
        {
            Floor__c tet = newList[i];

            if(tet.IsFromFieldTool__c) continue;

            if(tet.Thermal_Envelope_Type__c ==null) continue;

            if(tetMap !=null && tetMap.size()>0)
            {
                bp.teType = tetMap.get(tet.Thermal_Envelope_Type__c);
            }

            if(bp.teType ==null)continue;

            
            if(tet.Id !=null)
            {
                if(bp.allLayerMap !=null && bp.allLayerMap.size()>0)
                {
                    bp.layers = bp.allLayerMap.get(tet.Id);
                }
            }

            bp.layers = dsmtBuildingModelHelper.UpdateLayerModel(bp.layers,bp);

            Id parentId = tet.Thermal_Envelope_Type__c;
            string recTypeName = bp.RecordTypeMap.get(bp.teType.RecordTypeId);
                
            bp.floor = tet;

            ManageActuals(tet);

            tet.Location__c = dsmtBuildingModelHelper.ComputeLocationCommon(bp);

            bp.Location = tet.Location__c;
            tet.LocationSpace__c = dsmtBuildingModelHelper.LookupLocationSpaceCommon(bp);
            bp.LocationSpace = tet.LocationSpace__c;
            bp.isConditioned = dsmtBuildingModelHelper.ComputeSpaceConditioningCommon(bp);
            bp.basementIsVented = dsmtBuildingModelHelper.ComputeBasementVented(bp);
            bp.crawlspaceIsVented = dsmtBuildingModelHelper.ComputeCrawlspaceVented(bp);

            tet.LocationType__c = dsmtEnergyConsumptionHelper.ComputeLocationTypeCommon(bp);
            bp.LocationType = tet.LocationType__c;
            
            tet.Space_Type__c = dsmtSurfaceHelper.GetSurfaceSpaceType().get(recTypeName + ' Floor');

            tet.FloorType__c = dsmtSurfaceHelper.ComputeFloorTypeCommon(bp);
            tet.CeilingType__c = dsmtSurfaceHelper.ComputeCeilingTypeCommon(bp);                
            tet.FloorCeilingType__c = dsmtSurfaceHelper.ComputeFloorCeilingTypeCommon(bp);
            tet.Type__c = tet.FloorCeilingType__c;

            if(tet.Exposed_To__c == null || tet.Exposed_To__c == '')
            {
                if(tet.Type__C=='Attic Floor')
                {
                    tet.Exposed_To__c = 'Living Space';
                }
                else {
                    tet.Exposed_To__c = 'Ground';   
                }

            }

            //DSST-7806 : Only Reset when name is blank
            if(tet.Name == ''|| tet.Name ==null)
            {
                tet.Name = tet.Type__c;
                if(bp.floorList !=null && bp.floorList.size()>1)
                {
                    integer xfloorCount =0;
                    For(integer k=0, p = bp.floorList.size(); k < p; k++)
                    {
                        if(bp.floorList[k].Thermal_Envelope_Type__c==tet.Thermal_Envelope_Type__c)
                            xfloorCount+=1;
                    }
                    if(xfloorCount > 1)
                        tet.Name = tet.Type__c + ' ' + string.valueof(xfloorCount);
                }    
            }

            integer buildingyear =0;
            if(bp.bs.Year_Built__c != null && bp.bs.Year_Built__c !='') 
            {
               buildingyear = integer.valueof(bp.bs.Year_Built__c);
            }

            
            tet.Has_Trusses__c = bp.teType.Trusses__c;

            if(tet.Name == '' || tet.Name == null)
            {
                tet.Name = tet.Type__c;
                if(recTypeName == 'Crawlspace')
                {
                    tet.Name = 'Crawlspace Floor';
                }
            }
            
            if(tet.Add__c==null || tet.Add__c == '') 
            {
                if(recTypeName == 'Attic')
                {
                    tet.Add__c = 'Unfloored';
                }
                else if(recTypeName == 'Basement' || recTypeName =='Crawlspace')
                {
                    tet.Add__c = 'Slab';
                }
            }

            tet.Width__c = bp.EaObj.FrontLength__c;
            tet.Length__c = dsmtSurfaceHelper.ComputeFloorCeilingLengthCommon(bp);

            // DSST-6348 - Cavity Insulation Depth error
            // #Code Block Start
            if(bp.layers !=null && bp.layers.size() > 0)
            {
                Layer__c wfLayer = dsmtSurfaceHelper.GetFirstFramingLayer(bp.layers,1);
                if(wfLayer !=null)
                {
                    if(wfLayer.ActualCavityInsulationDepth__c !=null && wfLayer.ActualCavityInsulationDepth__c !=0)
                    {
                        tet.ActualInsulationAmount__c = null;
                    }
                }
            }

            if(tet.ActualInsulationAmount__c == null)
            {
                tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
            }
            // #Code Block End
            
            if (isInsert) //Trigger.isInsert
            {
                //tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
                tet.Gaps__c = SurfaceConstants.DefaultInsulationGaps;
                tet.Reflectivity__c = dsmtSurfaceHelper.ComputeFloorCeilingReflectivityCommon(tet.FloorType__c);
            }

            /*if(bp.layers !=null && bp.layers.size() > 0)
            {
                Layer__c wfLayer = dsmtSurfaceHelper.GetFirstFramingLayer(bp.layers,1);
                if(wfLayer !=null)
                {
                    if(wfLayer.ActualCavityInsulationDepth__c !=null && wfLayer.ActualCavityInsulationDepth__c !=0)
                    {
                        tet.Insul_Amount__c = dsmtSurfaceHelper.ComputeFloorCeilingInsulationAmountCommon(bp);
                    }
                }
            }*/

            if(tet.ActualPerimeter__c ==null)
            {
                tet.Perimeter__c = dsmtSurfaceHelper.ComputeFloorCeilingPerimeterCommon(bp);
            }

            if(tet.ActualArea__c ==null || tet.ActualArea__c ==0)
            {
                tet.Area__c = dsmtSurfaceHelper.ComputeSurfaceAreaCommon(bp);
            }
            
            if(tet.ActualProjectedArea__c==null || tet.ActualProjectedArea__c==0) 
            {
                tet.Projected_Area__c = dsmtSurfaceHelper.ComputeFloorCeilingProjectedAreaCommon(bp);
            }

            if(buildingyear != tet.Year__c)
            {
                tet.Insul_Amount__c = dsmtSurfaceHelper.LookupDefaultInsulationAmountByYear(buildingyear);
                tet.Gaps__c = SurfaceConstants.DefaultInsulationGaps;
                tet.Year__c = buildingyear;
            }

            tet.Net_Area__c = dsmtSurfaceHelper.ComputeAreaNet(bp);
            tet.RValueConst__c = dsmtSurfaceHelper.ComputeRValueConst(bp);
            tet.R_Value__c = dsmtSurfaceHelper.ComputeRValueCommon(bp);  
            
            tet.Insul_Effective_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveRValueCommon(bp);   
            tet.Insul_Nominal_R_Value__c = dsmtSurfaceHelper.ComputeFloorCeilingEffectiveNominalRValueCommon(bp); 
            
            tet.UA__c = dsmtSurfaceHelper.ComputeFloorCeilingUACommon(bp); 
            
            tet.U_Value__c = dsmtSurfaceHelper.ComputeUValueFromRValueCommon(tet.R_Value__c);
            
            tet.SlabRValNetLin__c = dsmtSurfaceHelper.ComputeFloorCeilingSlabRValNetLinCommon(bp);
            tet.SlabUALin__c = dsmtSurfaceHelper.ComputeFloorCeilingSlabUALinCommon(bp);

            tet.RoofVentCFMNat__c =dsmtSurfaceHelper.ComputeFloorCeilingRoofVentCFMNatCommon(bp);
            tet.RoofVentCFMH__c = dsmtSurfaceHelper.ComputeFloorCeilingRoofVentCFMHCommon(bp);
            
            tet.HLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'H');
            tet.CLF__c = dsmtSurfaceHelper.ComputeSurfaceHLFCLF(bp, null, 'C');

            tet.UAEffH__c = dsmtSurfaceHelper.ComputeFloorCeilingUAEffCommon(bp,'H');
            tet.UAEffC__c = dsmtSurfaceHelper.ComputeFloorCeilingUAEffCommon(bp,'C');
            
            tet.LoadH__c = dsmtSurfaceHelper.ComputeFloorCeilingLoadCommon(bp, 'H');
            tet.LoadC__c = dsmtSurfaceHelper.ComputeFloorCeilingLoadCommon(bp, 'C');
        }
    }
    
    
    public static void dsmtFloorBAScfm50AIAU(list<Floor__c> newList, Map<Id, Floor__c> oldMap, boolean isInsert, boolean isUpdate)
    {
        try
        {
            Final String EMHomePartId = 'AirSealHoursAt_625_CFM50';
            Map<Id, Recommendation__c> recommendationEAMap = new Map<Id,Recommendation__c>();
            Map<Id, Thermal_Envelope_Type__c> thermalEnvTypeEAMap = new Map<Id,Thermal_Envelope_Type__c>();
            Map<Id, Floor__c> floorEAMap = new Map<Id,Floor__c>();
    
            List<Floor__c> floors = [SELECT Id,Thermal_Envelope_Type__c,Area__c,Thermal_Envelope_Type__r.Energy_Assessment__c
                                                                         FROM Floor__c
                                                                         WHERE Id In: newList
                                                                         AND Type__c = 'Basement Floor'];
            for(Floor__c f : floors)
            {
                //if(isInsert || (isUpdate && (f.Area__c != oldMap.get(f.Id).Area__c) ))
                    floorEAMap.put(f.Thermal_Envelope_Type__r.Energy_Assessment__c,f);    
            }
    
            
    
            if(!floorEAMap.isEmpty())
            {
    
                List<Thermal_Envelope_Type__c> thermalEnvTypes = [SELECT SpaceConditioning__c,Energy_Assessment__c                                                              
                                                                         FROM Thermal_Envelope_Type__c
                                                                         WHERE RecordType.Name = 'Basement' 
                                                                         AND Energy_Assessment__c In: floorEAMap.keyset()];
                for(Thermal_Envelope_Type__c t : thermalEnvTypes)
                {
                    thermalEnvTypeEAMap.put(t.Energy_Assessment__c,t);
                }
    
                List<Energy_Assessment__c> energyAssessments = [SELECT Id,Building_Specification__c,
                                                                             Building_Specification__r.Floor_Area__c
                                                                             FROM Energy_Assessment__c
                                                                             WHERE Id In: floorEAMap.keyset()];                                                       
                                                                             
                
    
                List<Recommendation__c> recomms = [Select Recommendation_Scenario__c, 
                                                        Recommendation_Scenario__r.Energy_Assessment__c,
                                                        Seal_Penetrations__c,
                                                        Seal_Rim_Joist_Area__c
                                                        FROM Recommendation__c
                                                        WHERE Recommendation_Scenario__r.Energy_Assessment__c In: floorEAMap.keyset()
                                                        AND DSMTracker_Product__r.EMHome_EMHub_PartID__c =: EMHomePartId];
                
    
                for( Recommendation__c recObj : recomms) 
                {  
                    recommendationEAMap.put(recObj.Recommendation_Scenario__r.Energy_Assessment__c, recObj);
                }
    
    
                List<Building_Specification__c> buildingSpecsToUpdate = new List<Building_Specification__c>();
                Decimal BAScfm50 = 0.0;
                Floor__c floor;
                Thermal_Envelope_Type__c thermalType;
                Recommendation__c recommendation;
                                                                     
                for(Energy_Assessment__c ea : energyAssessments)
                {
                    if(thermalEnvTypeEAMap.containskey(ea.Id) && recommendationEAMap.containskey(ea.Id)){
                        floor = floorEAMap.get(ea.Id);
                        thermalType = thermalEnvTypeEAMap.get(ea.Id);
                        recommendation = recommendationEAMap.get(ea.Id);
                        
                        BAScfm50 = calculateBAScf50(recommendation.Seal_Penetrations__c, thermalType.SpaceConditioning__c,recommendation.Seal_Rim_Joist_Area__c,ea.Building_Specification__r.Floor_Area__c,floor.Area__c);
                        buildingSpecsToUpdate.add(
                                                    new Building_Specification__c(
                                                                                    Id = ea.Building_Specification__c,
                                                                                    BAS_cfm50_Floor_Calculation__c = BAScfm50));
                        
                                
                    }
                }
                
                if(!buildingSpecsToUpdate.isEmpty())
                    update buildingSpecsToUpdate;
            }
            
        }
        catch(Exception ex)
        {
            system.debug('Error:- ' + ex.getMessage() + ' ' + ex.getStackTraceString());
        }  
    }
    
    private static decimal calculateBAScf50(String seal_Pen, String space, String seal_Rim, Decimal FA, Decimal A) {
        Decimal bas = 0.0;
        if(space == 'Conditioned')
           return calculateAdd(FA, A);
           
        if(space == 'Vented')
           return FA;
           
        if(space == 'Unconditioned'){
            if(seal_Pen == 'Ceiling' && (seal_Rim == null || seal_Rim == 'Wall'))  
                return FA;
            if(seal_Rim == 'Wall' && (seal_pen == null || seal_Pen == 'Wall'))
                return calculateAdd(FA ,A);
            
            if(seal_Pen == null)
                return FA;      
        
        }       
        
        return bas;
    }  
    
    private static Decimal calculateAdd(Decimal num1, Decimal num2){
        if(num1 != null && num2 != null){
            return (num1 + num2);
        }
        
        return 0;
    }  
    
    public static void deleteLayers(Set<Id> floorIdSet)
    {
        List<Layer__c> layerList = [select id from Layer__c where Floor__c in : floorIdSet];
        
        if(layerList.size() > 0)
        {
            delete layerList;
        }
    }
}