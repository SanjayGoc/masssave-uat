@isTest
Public class EmployeeAppointmentTest{
    @isTest
    public static void runTest(){
        User u = Datagenerator.createportaluser();
        
        DSMTracker_Contact__c dc = new DSMTracker_Contact__c(contact__c = u.ContactId);
        insert dc;
        
        System.runAs(u){
            Appointment__c a = new Appointment__c(Appointment_Type__c = 'df',
                                                  Appointment_Status__c = 'sd',
                                                  Appointment_Start_Time__c = system.now(),
                                                  Appointment_End_Time__c = system.now().addDays(1));
            insert a;
            
            EmployeeAppointment cntrl = new EmployeeAppointment();
            cntrl.editApp = a;
            cntrl.SaveUnAvailableAppointment();
            cntrl.updateAppoinment();
            cntrl.AppointmentId = a.Id;
            cntrl.CancenAppointment();
            cntrl.queryAppointment();
            cntrl.eventsJsonStr = '';
            cntrl.SelectedAppointmentType = 'df';
            cntrl.SelectedAppointmentStatus = 'sd';
            //cntrl.Selectedcontacts = 'asd';
            string s = cntrl.eventsJsonStr;
            
            EmployeeAppointment.EventsWrapper ew = new EmployeeAppointment.EventsWrapper('','','',2);
        }
    }
}