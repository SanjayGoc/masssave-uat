public class AddSkillScoreCntrl{

    public string empId {
        get;
        set;
    }
    
    public List < SkillsWrapper > lstSkillsWrap {
        get;
        set;
    }

    public string SkillXml {
        get;
        set;
    }
    public List <Skill__c> lstSkill{get;set;}
    public List <Skill__c> lstSkillRemain{get;set;}
    public integer skillCount {get;set;}
    public Set<String> setSkillsName;
    public List<SkillsWrapper> lstSkillRemainWrp{get;set;}
    Public Integer cntr;
    public Boolean isAdd{get;set;}
    
    public AddSkillScoreCntrl() {
        
        init();
    }

    private void init() {
        lstSkillsWrap = new List < SkillsWrapper > ();
        lstSkillRemainWrp = new List<SkillsWrapper>();
        lstSkillRemain = new List<Skill__C>();
        skillCount = 0;
        cntr = 0;
        setSkillsName = new Set<String>();
        empId = Apexpages.currentPage().getParameters().get('empId');
        lstSkill = new List<Skill__c>();
        
        lstSkill = [select Id,Name from Skill__c order by CreatedDate Desc];
        
        skillCount = lstSkill.size();

        system.debug('--empId ---' + empId );
        if (empId != null && empId.trim().length() > 0) {
            Employee__c opp = [select Id,Name from Employee__c where Id =: empId Limit 1];
            //Id = opp.AccountId;
            //getCon(accId );
            availableSkill();
            //isEdit = false;
            remainSkills();
        }
        addRow();
        
    }
    
    public List < SelectOption > getInternalEvaList() {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult = Employee_Skill__c.As_per_Internal_Evaluation__c.getDescribe();
           List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
           for( Schema.PicklistEntry f : ple)
           {
              options.add(new SelectOption(f.getLabel(), f.getValue()));
           }       
           return options;
    }


    public void availableSkill() {
        lstSkillsWrap = new List < SkillsWrapper > ();
        List < Employee_Skill__c> lstSkill_Available = [select Id,Name,Skill__r.Name,As_per_Internal_Evaluation__c,Employee__c,Skill__c,Survey_Score__c
                                                         from Employee_Skill__c where Employee__c =:empId order by CreatedDate Desc];

        //LoadXml
        SkillXml = '<Skills>';

        integer cnt = 1;
        for (Employee_Skill__c es: lstSkill_Available ) {
            setSkillsName.add(es.Skill__r.Name);
            SkillXml += '<Skill>';
            
            SkillsWrapper sw = new SkillsWrapper();
            
            sw.SkillId= es.Skill__c;
            sw.SkillName = es.Skill__r.Name;
           
            if (es.Survey_Score__c != null && es.Survey_Score__c != 0) {
                sw.surveyScore  = string.valueof(es.Survey_Score__c);
            }

            SkillXml += '<Rownum>' + cnt + '</Rownum>';
            SkillXml += '<SkillName><![CDATA[' + es.Skill__r.Name + ']]></SkillName>';
            SkillXml += '<Score><![CDATA[' + sw.surveyScore  + ']]></Score>';
            SkillXml += '<IEv><![CDATA[' + es.As_per_Internal_Evaluation__c+ ']]></IEv>';
            SkillXml += '<Size>'+lstSkill_Available.size() + '</Size>';
            
            
            SkillXml += '</Skill>';
            cnt++;
        }

        SkillXml += '</Skills>';
        
        
    }
    
    public void remainSkills(){
        for(Skill__c s: lstSkill ){
            if(!setSkillsName.contains(s.Name)){
                lstSkillRemain.add(s);
            }
        }
    }
    
    public void addRow(){
     cntr++;
     if(cntr <= lstSkillRemain.size()){
      //if(setSkillsName.contains())
        SkillsWrapper sw = new SkillsWrapper();
            
        sw.SkillId= lstSkillRemain[cntr-1].Id;
        sw.SkillName = lstSkillRemain[cntr-1].Name;
        
        sw.SkillLst = new List<Selectoption>();
       
        for(Skill__C sk : lstSkillRemain){
            sw.SkillLst.add(new SelectOption(sk.Id, sk.Name));
        }
        
      
        
        lstSkillRemainWrp.add(sw);
        isAdd = true;
        if(cntr == lstSkillRemain.size())
            isAdd = false;
     }
     
     else{
         isAdd = false;
     }
    
    }
    
    
    

    public class SkillsWrapper {
    
        public Skill__c objAvailableSkill {
            get;
            set;
        }
        
        public List<SelectOption> inteEvaOpt{get;set;}
        public List<SelectOption> SkillLst{get;set;}
        public String SelectedSkill{get;set;}
        public Id SkillId{get;set;}
        public String SkillName{get;set;}
        public string selectEvaOpt {
            get;
            set;
        }
        public string surveyScore {
            get;
            set;
        }
        public SkillsWrapper(){
            inteEvaOpt = new List<SelectOption>();
            //inteEvaOpt = getInternalEvaList();
        }
    }


    public void saveQna() {
        
    }

    public void saveSkillsTeam() {
        try {
            List<Employee_Skill__c> lstInsertEmpSkill = new List<Employee_Skill__c>();
            if(lstSkillRemainWrp.size()>0){
                for(SkillsWrapper ss:lstSkillRemainWrp){
                    Employee_Skill__c objES = new Employee_Skill__c();
                    objES.As_per_Internal_Evaluation__c= ss.selectEvaOpt;
                    objES.Employee__c=empId;
                    objES.Skill__c= ss.selectedSkill;//ss.SkillId;
                    if(ss.surveyScore != null && ss.surveyScore != ''){
                        objES.Survey_Score__c = Integer.valueof(ss.surveyScore) ;
                    }
                    
                    lstInsertEmpSkill.add(objES);
                }
            }
            
            if(lstInsertEmpSkill.size()>0)
                insert lstInsertEmpSkill;
           init();
            
        } catch (Exception e) {
            system.debug('--e--' + e);
        }
   }

    public void cancel() {
        
    }
    
    public void questionChange() {
        
    }
    
}