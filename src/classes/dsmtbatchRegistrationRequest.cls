global class dsmtbatchRegistrationRequest implements Schedulable,database.batchable<sObject>{
    
    public Set<String> inactiveRRs{get;set;}
    public Set<String> cancelledRRs{get;set;}
    
    public dsmtbatchRegistrationRequest (){
       inactiveRRs = new Set<String>();
       cancelledRRs = new Set<String>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Date lastDate = System.Today().addDays(-30);
        String query = 'select id,Name,Send_Inactive_Application_Date__c,Send_Cancelled_Application_Date__c,Status__c,LastModifiedDate,Dsmtracker_Contact__r.Contact__c,Email__c,Trade_Ally_Account__c,Owner.Email,Account_Manager__c,Account_Manager__r.Email from Registration_Request__c where status__c in (\'Registration Request Sent\',\'Registration Completed\') and LastModifiedDate <=: lastdate and Email__c != null and (Send_Inactive_Application_Date__c = null or Send_Cancelled_Application_Date__c = null) ';
       // String query = 'select id,Name,Send_Inactive_Application_Date__c,Send_Cancelled_Application_Date__c,Status__c,LastModifiedDate,Dsmtracker_Contact__r.Contact__c,Email__c,Trade_Ally_Account__c,Owner.Email,Account_Manager__c,Account_Manager__r.Email from Registration_Request__c where status__c in (\'Registration Request Sent\',\'Registration Completed\') and Email__c != null and (Send_Inactive_Application_Date__c = null or Send_Cancelled_Application_Date__c = null) and Id = \'a1S4D0000008fvfUAA\'';
        system.debug('--query--'+query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Registration_Request__c> rrlist)
    {
          system.debug('--rrlist--'+rrlist);
          Date Today = Date.Today();
          for(Registration_Request__c rr : rrlist){
             system.debug('--rr--'+rr);
             String status = rr.Status__c;
             
             Date lastmodifieddate = date.newinstance(rr.LastModifiedDate.year(), rr.LastModifiedDate.month(), rr.LastModifiedDate.day());
             Date sendInactivedate = rr.Send_Inactive_Application_Date__c;
             Date sendCancelleddate = rr.Send_Cancelled_Application_Date__c;
             
             system.debug('--last modified date--'+lastmodifieddate);
             system.debug('--sendInactivedate --'+sendInactivedate);
             system.debug('--sendCancelleddate --'+sendCancelleddate);
             
             String targetObjectId = rr.Dsmtracker_Contact__r.Contact__c;
             String objType = 'Registration_Request__c';
             List<String> toAddressList = new List<String>();
             toAddressList.add(rr.Email__c);
             String fromaddress = null;
             if(rr.Account_Manager__c != null){
                fromaddress = rr.Account_Manager__r.Email;
             }else{
                fromaddress = rr.Owner.Email;
             }
             
             
             if(sendInactivedate  == null){  //Send Inactive Application Email
                  inactiveRRs.add(rr.Name);
                  rr.Send_Inactive_Application_Date__c = today;
                  String templateName = Email_Custom_Setting__c.getorgDefaults().RR_Inactive_application_after_30_days__c;
                  
                  dsmtRegistrationRequestUtil.sendEmail(targetObjectId,rr,objType ,toAddressList,new List<String>(),new List<String>(),templateName,null,fromaddress,'Outbound');
                  
             }else{  //send Cancelled Application Email
                  cancelledRRs.add(rr.Name);
                  rr.Send_Cancelled_Application_Date__c = today;
                  rr.Status__c = 'Rejected';
                  
                  String templateName = Email_Custom_Setting__c.getorgDefaults().RR_Cancelled_application_after_60_days__c;
                  dsmtRegistrationRequestUtil.sendEmail(targetObjectId,rr,objType ,toAddressList,new List<String>(),new List<String>(),templateName,null,fromaddress,'Outbound');
             }
             
          }
          
          if(rrlist.size() > 0){
             update rrlist;
          }
             
    }  
    global void finish(Database.BatchableContext BC)
    {
       AsyncApexJob a = [select id,status,NumberOfErrors,JobItemsProcessed,TotalJobItems FROM AsyncApexJob WHERE Id =:BC.getJobId()];
       system.debug(a);
      /*  Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddress = new String[] {'support+rr@unitedcloudsolutions.com'};
        mail.setToAddresses(toAddress);
        mail.setSubject('Inactive Or Cancelled Registration Request : ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed, inactive requests :' + inactiveRRs +' and cancelled Requests '+ cancelledRRs + '.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); */
    }
    
    global void  execute(SchedulableContext sc){
        dsmtbatchRegistrationRequest  obj = new dsmtbatchRegistrationRequest();
        database.executebatch(obj,25);    
    }
}