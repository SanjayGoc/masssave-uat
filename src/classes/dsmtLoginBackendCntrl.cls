public with sharing class dsmtLoginBackendCntrl {

     
     public string hp{get;set;}
     public dsmtLoginBackendCntrl(){
     
     }
     
     public Set<String> allowedProfileIdToLogin{get;set;}
     
     
    public Pagereference queryUser()
    {
        String Username = userinfo.getUSerName();
       
        List<User> ulist =[Select Id,IsActive,EULA_Agreement_Date__c from User where (username=:username or Email =: username)];
        if(ulist.size() > 0 && ulist[0].EULA_Agreement_Date__c != null){
            system.debug('--callled---');
            //return new Pagereference('/home/home.jsp');
            return Auth.SessionManagement.finishLoginFlow();
        }
        
        return null;
    }
    
    public Pagereference updateUser(){
        
         String Username = userinfo.getUSerName();
         List<User> ulist = [SELECT Id, IsActive,EULA_Agreement_Date__c FROM User WHERE username =:username ];       
        if (ulist.size() > 0) {
                User u = ulist.get(0);
                
                if(u.EULA_Agreement_Date__c == null){
                    u.EULA_Agreement_Date__c = System.now();
                }
                system.debug('--user--'+u);
                update u;
        }
        return Auth.SessionManagement.finishLoginFlow();
    }
}