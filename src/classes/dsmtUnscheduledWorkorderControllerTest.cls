@isTest
Public class dsmtUnscheduledWorkorderControllerTest
{
    @isTest
    public static void runTest()
    {
        Account acc2 = new Account();
        acc2.Name = 'Xcel Energy NM';
        acc2.Billing_Account_Number__c= 'Gas-~~~1234';
        acc2.Utility_Service_Type__c= 'Gas';
        acc2.Account_Status__c= 'Active';
        insert acc2;
        
        Location__c loc = Datagenerator.createLocation();
        insert loc;
        
        Employee__c emp = Datagenerator.createEmployee(loc.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, emp.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Status__c = 'Unscheduled';
        wo.Work_Team__c = wt.Id;
        wo.Requested_Start_Date__c = System.now();
        wo.Early_Arrival_Time__c = datetime.now();
        wo.Late_Arrival_Time__c = datetime.now();
        insert wo;
        
        Appointment__c app = new Appointment__c();
        app.Appointment_Type__c = 'df';
        app.Appointment_Status__c = 'sd';
        app.Appointment_Start_Time__c = system.now();
        app.Appointment_End_Time__c = system.now().addDays(1);
        insert app;
        
        ApexPages.currentPage().getParameters().put('sel_loc', loc.Id);
        
        dsmtUnscheduledWorkorderController cntrl = new dsmtUnscheduledWorkorderController();
        
        cntrl.getWorkordersJSON();
        
        cntrl.CalculateDatetime('12/12/2000', true);
        cntrl.CalculateDatetime('12/12/2000', false);
    }
}