public without sharing class TradeAllyAccountTriggerHandler {
    public static  void AfterInsertUpdate(List<Trade_Ally_Account__c> newList,Boolean isInsert,Map<Id,Trade_Ally_Account__c> oldMap) {

        List<Group> queuelist = new List<Group>();
        List<Group> grouplist = new List<Group>();
        List<QueuesObject> queueObjlist = new List<QueuesObject>();
        Map<String, Id> queMap = new Map<String,Id>();
        Map<String,Id> groupMap = new Map<String,Id>();

        //For TA insert - Create Queue and Group -- START
        if(isInsert){ 
            for(Trade_Ally_Account__c tr : newList){
                queuelist.add(new Group(type='Queue',Name = tr.Name + ' Queue'));
                grouplist.add(new Group(Name = tr.Name));
            }      
        
            if(!Test.IsRunningTest()){
                if(!queuelist.isEmpty()){
                    Database.SaveResult[] srList = Database.insert(queuelist, false);
                    Set<Id> qIds = new Set<Id>();
                    for (Database.SaveResult sr : srList) {
                        if (sr.isSuccess()) {
                            qIds.add(sr.getId());
                        }
                    }
                    createQueue(qIds);
                    Database.SaveResult[] gpList = Database.insert(grouplist, false);
                }
            }
        }
         //For TA insert - Create Queue and Group -- END

        //For TA Update - Update Queue and Group Name -- START
        if(!isInsert){
            Map<String,String> trNamesMap = new Map<String,String>();
            Set<String> foundtrNames = new Set<String>();
            Map<String,String> queNamesMap = new Map<String,String>();
            List<Group> newqueuelist = new List<Group>();
            
            for(Trade_Ally_Account__c tr : newlist){
               if(oldMap.get(tr.id).Name != tr.Name){
                   queNamesMap.put(OldMap.get(tr.id).Name + ' Queue',tr.Name + ' Queue');
                   trNamesMap.put(OldMap.get(tr.id).Name,tr.Name);      
               }
            }   
            queuelist = new List<Group>();
            grouplist = new List<Group>();
            
            //For Queue
            if(!queNamesMap.isEmpty()){
                for(Group q : [Select Id, Name from Group WHERE Name In: queNamesMap.keySet()]){
                   
                    // DSST-12578 - START BY PP
                    string QueueName = queNamesMap.get(q.Name).replace(' ','_').replace('\'','_').replace(',','_').replace('.','').replace('-','_').replace('&','_');
                    while(QueueName.Contains('__')){
                        QueueName = QueueName.replaceAll('__','_');
                    }
                    queuelist.add(new Group(Id= q.Id,Name = queNamesMap.get(q.Name),DeveloperName = QueueName));
                    foundtrNames.add(q.Name);
                    // DSST-12578 - END BY PP
                }
                system.debug('queuelist--'+queuelist);
                for(String qname : queNamesMap.keySet()){
                    if(!foundtrNames.contains(qname)){
                        newqueuelist.add(new Group(type='Queue',Name = queNamesMap.get(qname)));    
                    }
                }   

                if(!newqueuelist.isEmpty())
                {
                    if(!Test.IsRunningTest()){
                        Database.SaveResult[] srList = Database.insert(newqueuelist, false);
                       
                        Set<Id> qIds = new Set<Id>();
                        for (Database.SaveResult sr : srList) {
                            if (sr.isSuccess()) {
                                qIds.add(sr.getId());
                            }
                        
                        }
                        createQueue(qIds);  
                    }
                }

                if(!queuelist.isEmpty())
                    update queuelist;
            }
            
            //For Group
            foundtrNames = new Set<String>();
            List<Group> newgrouplist = new List<Group>();
            
            if(!trNamesMap.isEmpty()){
                for(Group q : [Select Id, Name from Group WHERE Name In: trNamesMap.keySet()]){
                    
                    // DSST-12578 - START BY PP
                    string GroupName = trNamesMap.get(q.Name).replace(' ','_').replace('\'','_').replace(',','_').replace('.','').replace('-','_').replace('&','_');
                    while(GroupName.Contains('__')){
                        GroupName = GroupName.replaceAll('__','_');
                    }
                    
                    grouplist.add(new Group(Id= q.Id,Name = trNamesMap.get(q.Name),DeveloperName = GroupName));
                    foundtrNames.add(q.Name);
                    // DSST-12578 - END BY PP
                }
                
                for(String qname : trNamesMap.keySet()){
                    if(!foundtrNames.contains(qname)){
                        newgrouplist.add(new Group(Name = trNamesMap.get(qname)));    
                    }
                }   

                if(!newgrouplist.isEmpty())
                {
                    if(!Test.IsRunningTest()){
                        Database.SaveResult[] srList = Database.insert(newgrouplist, false);
                    }
                }

                if(!grouplist.isEmpty())
                    update grouplist;
            }
    
        }
        //For TA Update - Update Queue and Group Name -- START                   
    }

    @future
    public static void createQueue(Set<Id> groupIds) {

        List<QueueSObject> newQueueSobject = new List<QueueSObject>();
        for (Id queueId : groupIds) {
            newQueueSobject.add(new QueueSObject(SobjectType='Trade_Ally_Account__c',QueueId=queueId));
            newQueueSobject.add(new QueueSObject(SobjectType='Review__c',QueueId=queueId));
        }
        
        try{
            insert newQueueSobject;
        }
        catch(exception e){
            system.debug('Error: '+e);
        }   
    }
}