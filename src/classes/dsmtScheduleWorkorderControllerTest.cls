@isTest
Public class dsmtScheduleWorkorderControllerTest{
    @isTest
    public static void runTest(){
            Location__c  locobj = Datagenerator.createLocation();
        insert locObj;
        
        Region__c regObj = Datagenerator.CreateRegion(locObj.Id);
        insert regObj;
        
        Employee__c  emp = Datagenerator.createEmployee(locObj.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.createworkteam(locObj.Id,emp.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.Createwo();
        wo.Location__c = locObj.Id;
        wo.Work_Team__c = wt.Id;
        wo.early_Arrival_Time__c = DAtetime.now();
        wo.late_Arrival_Time__c = Datetime.now().Addminutes(150);
        wo.Scheduled_Start_Date__c= DAtetime.now().Adddays(-2);
        wo.Scheduled_End_Date__c= wo.Scheduled_Start_Date__c.Addminutes(150);
        wo.Status__c = 'Scheduled';
        insert wo;
        List< Workorder__c> work = new List< Workorder__c>();
        work.add(wo);
        ApexPages.currentPage().getParameters().put('id',wo.Id);
        dsmtScheduleWorkorderController cntrl = new dsmtScheduleWorkorderController();
        cntrl.CreateWorkOrder();
    }
    
}