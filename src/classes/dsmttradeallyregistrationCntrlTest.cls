@isTest
public class dsmttradeallyregistrationCntrlTest {
    @isTest
    public Static Void RunRR(){
        
        Account a = new Account(name= 'SITE SELF REGISTRATION TRADE ALLY');
        insert a;
        
        User u = Datagenerator.CreatePortalUser();
        system.runAs(u){
        Checklist__c chklst = new Checklist__c(Name='Trade Ally Registration Form',Unique_Name__c = 'Trade_Ally_Registration_Form', Reference_ID__c='erw343');
        insert chklst;
         
        Checklist_Items__c chkitm = new Checklist_Items__c(
            Help_text_for_public_portal__c = true,
            Special_instruction_for_public_portal__c = true,
            Checklist__c='test chk',
            Reference_ID__c='A123',
            Parent_Checklist__c=chklst.id
        );
        insert chkitm;
            
        Checklist_Items__c chkitm1 = new Checklist_Items__c(
                Special_instruction_for_public_portal__c = true,
                Checklist__c='test chk',
                Reference_ID__c='A123',
                Parent_Checklist__c=chklst.id
            );
            insert chkitm1;     
        
        
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        insert ta;
                
        DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
        dsmtc.Name='test';
        dsmtc.Super_User__c=true;
        dsmtc.Trade_Ally_Account__c=ta.id;
        dsmtc.contact__c= u.contactId;
        insert dsmtc;
        
        Registration_Request__c rr = new Registration_Request__c();
        rr.First_Name__c='test RR';
        rr.DSMTracker_Contact__c=dsmtc.id;
        rr.Trade_Ally_Account__c=ta.id;
        insert rr;
            
        dsmtc = [select id,DSMT_Contact_Number__c from DSMTracker_Contact__c where id =: dsmtc.id];        
        ApexPages.currentPage().getParameters().put('dcid',dsmtc.DSMT_Contact_Number__c);      
        dsmttradeallyregistrationCntrl dsmtRR = new dsmttradeallyregistrationCntrl();
        dsmtRR.password = 'public12345';
            
        rr = [select id,Registration_Number__c from Registration_Request__c where id =: rr.id];
        system.debug('--rr--'+rr );    
            
        ApexPages.currentPage().getParameters().put('id',rr.Registration_Number__c);     
        dsmtRR = new dsmttradeallyregistrationCntrl();
       
        dsmtRR.getChecklists();    
        Checklist__c cc = dsmtRR.pgCheckList;    
            
        dsmt_Reg_States__c dsmtstate = new dsmt_Reg_States__c();
        dsmtstate.Name = 'AL';
        dsmtstate.State_Name__c = 'AL';
        insert dsmtstate;
            
        Online_Portal_URLs_Hierarchy__c opuh = new Online_Portal_URLs_Hierarchy__c();
        opuh.TA_Manager_Portal_Profile__c='Customer Portal Manager Custom';
        opuh.Scheduler_Portal_Profile__c='Customer Portal Manager Custom';
        opuh.Energy_Specialist_Portal_Profile__c='Customer Portal Manager Custom';
        insert opuh;
            
        List<SelectOption> clist = dsmtRR.getCountriesSelectList();
        
         dsmtRR.password = '123456';
         dsmtRR.confirmPassword = '12345';   
         dsmtRR.registrationClick();   
         
         dsmtRR.password = '12345';
         dsmtRR.confirmPassword = '12345';   
         dsmtRR.registrationClick();   
         
         dsmtRR.Username = 'tuser@test.org';
         dsmtRR.password = 'public12345';
         dsmtRR.confirmPassword = 'public12345';   
         dsmtRR.registrationClick();   
         
         dsmtRR.Username = 'tuser@test.org111';
         dsmtRR.registrationClick();      
         
         dsmtRR.Username = 'test@test.com';
         dsmtRR.registrationClick();      
        }
    }
}