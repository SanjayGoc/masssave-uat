@isTest
Public class dsmtNewIntakeControllerTest{
    @isTest
    public static void runTest(){
            Account  acc= Datagenerator.createAccount();
            acc.Billing_Account_Number__c= 'Gas-~~~1234344';
            insert acc;
            
            Account acc2 = new Account();
            acc2.Name = 'Xcel Energy NM';
            acc2.Billing_Account_Number__c= 'Gas-~~~1234';
            acc2.Utility_Service_Type__c= 'Gas';
            acc2.Account_Status__c= 'Active';
            insert acc2;
            
            Call_List__c callListObj = new Call_List__c();
            callListObj.Status__c = 'Active';
            insert callListObj;
            
            Lead l = new Lead();
            l.LastName = 'test';
            l.Company = 'test';
            l.Read_date__c = Date.Today()-2;
            insert l;
            
            Call_List_Line_Item__c Obj = new Call_List_Line_Item__c();
            obj.Call_List__c = callListObj.Id;
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            obj.Followup_Date__c = date.Today();
            insert obj;
            
            Obj = new Call_List_Line_Item__c();
            obj.Call_List__c = callListObj.Id;
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            obj.Followup_Date__c = date.Today()-1;
            insert obj;
            
            Obj = new Call_List_Line_Item__c();
            obj.Call_List__c = callListObj.Id;
            obj.Status__c = 'Ready To Call';
            obj.First_Name_New__c = 'Test';
            obj.Last_Name_New__c = 'Test';
            obj.Next_Followup_date__c  = Date.Today();
            obj.Lead__c = l.Id;
            obj.Phone_New__c  = '9909240666';
            insert obj;
            
            Task tsk = new Task();    
            tsk.WhatId = obj.Id;
            // tsk.WhoId = l.Id;
            tsk.CallDisposition = 'Outbound';
            tsk.Status = 'Completed';
            tsk.Subject = ' Call To ';
            tsk.Description = 'test';
            tsk.ActivityDate = Date.Today();
            tsk.Type = 'Call';
            insert tsk;
            
            Note n = new Note();
            n.ParentId = obj.Id;
            n.Title = 'test';
            n.Body = 'test';
            insert n;
            Skill__c sk= Datagenerator.createSkill();
            insert sk;
            
            
            Location__c loc= Datagenerator.createLocation();
            insert loc;
            Employee__c em = Datagenerator.createEmployee(loc.id);
            em.Status__c='Approved';
            em.Employee_Id__c = 'test123';
            insert em;
            Employee__c em2 = Datagenerator.createEmployee(loc.id);
            em2.Status__c='Approved';
            em2.Employee_Id__c = 'test1234';
            insert em2;
            
            Work_Team__c wt =  Datagenerator.CreateWorkTeam(loc.id,em.id);
            insert wt;
            Employee_Skill__c emskill=  Datagenerator.createempSkill(em.id,sk.id);
            emskill.Survey_Score__c=89;
            insert emskill;
            Workorder_Type__c woTypeList = new Workorder_Type__c(name='test',Est_PreWork_Time__c=2,
                                        Est_PostWork_Time__c=34,Est_Work_Time__c=4,Visit_Size__c='S',Est_Deliverable_Time__c=65);
            insert woTypeList;
            Required_Skill__c rsk= new Required_Skill__c(Minimum_Score__c=7,Skill__c=sk.id,Workorder_Type__c=woTypeList.Id);
            insert rsk;
            Eligibility_Check__c EL= Datagenerator.createELCheck();
            EL.Workorder_Type__c=woTypeList.Id;
            EL.Service_Address__c='test';
            EL.City__c='test';
            EL.Start_Date__c=date.today().addDays(-5);
            EL.End_Date__c=date.today().addDays(5);
            upsert EL;
            EL.How_many_units__c='Single family';
            update EL;
            Program_Eligibility__c PE= Datagenerator.createProgramEL();
            Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
            Premise__c pre= Datagenerator.createPremise();
            insert pre;
            Customer__c cust = Datagenerator.createCustomer(acc2.id,pre.id);
            insert cust;
            DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
            dsmt.Trade_Ally_Account__c =Tacc.id; 
            update dsmt;
            Appointment__c app= Datagenerator.createAppointment(EL.Id);
            app.DSMTracker_Contact__c=dsmt.id;
            update app;
            //  dsmt.Contact__c= cont.id;
            //update dsmt;
            Registration_Request__c reg= Datagenerator.createRegistration();
            reg.DSMTracker_Contact__c=dsmt.id; 
            reg.Trade_Ally_Account__c =Tacc.ID;
            reg.HIC_Attached__c=true;
            update reg;
             customer_Eligibility__c cEL= Datagenerator.createCustomerEL(EL.id);
             cEL.Workorder_Type__c=woTypeList.Id;
             cEL.Gas_Customer__c= cust.id;
             cEl.Gas_Account__c = 'Gas-~~~1234';
             cEL.Interested_in_receiving_a_visit__c=false;
             cEL.Excluded_From_Scheduling__c=false;
             update cEL;
             customer_Eligibility__c cEL2= Datagenerator.createCustomerEL(EL.id);
             cEL2.Workorder_Type__c=woTypeList.Id;
             cEL2.Interested_in_receiving_a_visit__c=false;
             cEL2.Excluded_From_Scheduling__c=false;
            // cEL2.Gas_Customer__c= cust.id;
             update cEL2;
             customer_Eligibility__c  CustEl = new Customer_Eligibility__c (Eligibility_Check__c =EL.Id,Electric_Account__c='Electric',Gas_Account__c='Gas');
             CustEl.Workorder_Type__c=woTypeList.Id;
             CustEl.Gas_Customer__c= cust.id;
             CustEl.Interested_in_receiving_a_visit__c=false;
             CustEl.Excluded_From_Scheduling__c=false;
             insert CustEl;
            ApexPages.currentPage().getParameters().put('id',app.id);
            
            Test.StartTest();
            dsmtNewIntakeController controller = new dsmtNewIntakeController();
            dsmtNewIntakeController.GECheckWrapper wrap= new dsmtNewIntakeController.GECheckWrapper();
            controller.eleCheckId= EL.ID;
            controller.customerId= cust.Id;
            controller.saveEligibility();
            EL.How_many_units__c='2 Units';
            update EL;
            controller.getApartmentList();
            dsmtNewIntakeController.getContractors();
            dsmtNewIntakeController.fetchCustomer(obj.Id);
            dsmtNewIntakeController.LetsGo(false,false);
            //controller.SaveRecord();
            controller.customerId= cust.id;
            
            dsmtNewIntakeController.getContactValues(EL.Id);
            dsmtNewIntakeController.checkZipEligibility('1234','test','2','1','test');
            dsmtNewIntakeController.GetCity('1234');
             dsmtNewIntakeController.AccountCheck('1Gas-1234~~~1234','test','1234','');
            dsmtNewIntakeController.checkGEUtility('Gas');
            System.debug('--- ID1 ----' + cEL);
            System.debug('--- ID1 ----' + cEL2);
            System.debug('--- ID1 ----' + CustEl);
            controller.eleCheckId=EL.Id;
           // controller.getAppt();
          //  controller.getEmployee();
            controller.saveAppoinmentTime();
            dsmtNewIntakeController.CheckCustomer('1Gas-1234~~~1234','test','test','test');
            
            
            controller.WoTypeId=woTypeList.Id;
           // controller.SelectedWorkTeam=wt.id;
         //   controller.SelectedEmployee=em.id+'~~~'+em2.Id;
          //  controller.custInteraction =EL;
              
            //controller.getAppt();
            controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
            controller.CreateWorkOrder();
            EL.How_many_units__c='2 units';
            update EL;
            controller.SelectedWorkTeam='a1E4D0000004ORKUA2~~~10:30 AM-02:30 PM on Tuesday~~~10:00 AM-11:00 AM on 04/18/2017';
            controller.CreateWorkOrder();
            controller.Selectedcontacts =dsmt.id;
            controller.updateRecord();
            //controller.upsertCustomerEligibility();
            controller.EligibilityFaliRecord();
            //controller.VerifyMultiFamily();
           // controller.dsmtVerifyMultiFamily();
            controller.CreateEligibilityCheckRecord();
           // controller.updateIntakePending12();
            controller.ecaList = new List<Customer_Eligibility__c>();
            controller.ecaList.add(CustEl);
          //  controller.SaveRecord();
       //
      Test.StopTest();  
    }
     
     
     static testMethod void case2(){
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
         String hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        String hexDigest = EncodingUtil.convertToHex(hash);
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = hexDigest +'@test.com',
        Alias = 'batman',
        Email='bruce.wayne@wayneenterprises.com',
        EmailEncodingKey='UTF-8',
        Firstname='Bruce',
        Lastname='Wayne',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1;
        
        //User u1 = [Select ID From User Where Id =: portalAccountOwner1.Id];
        
        System.runAs ( portalAccountOwner1 ) {
        //Create account
        Account portalAccount1 = new Account(
        Name = 'TestAccount',
        OwnerId = portalAccountOwner1.Id,Billing_Account_Number__c= 'Gas-~~~1234344'
        );
        insert portalAccount1;
        
        //Create contact
        Contact contact1 = new Contact(
        FirstName = 'Test',
        Lastname = 'McTesty',
        AccountId = portalAccount1.Id,
        Email = System.now().millisecond() + 'test@test.com'
        );
        insert contact1;
        
        hashString = '1000' + String.valueOf(Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss.SSS'));
        hash = Crypto.generateDigest('MD5', Blob.valueOf(hashString));
        hexDigest = EncodingUtil.convertToHex(hash);
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name=:'CLEAResult Energy Specialist' Limit 1  ];
        User user1 = new User(
        Username = hexDigest +'@test.com',
        ContactId = contact1.Id,
        ProfileId = portalProfile.Id,
        Alias = 'test123',
        Email = 'test12345@test.com',
        EmailEncodingKey = 'UTF-8',
        LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US'
        );
        
       Test.StartTest(); 
        Database.insert(user1);
       
        System.runAs ( user1) {
         Trade_Ally_Account__c Tacc= Datagenerator.createTradeAccount();
        DSMTracker_Contact__c dsmt= Datagenerator.createDSMTracker();
         dsmt.Contact__c= user1.contactID;
         dsmt.Trade_Ally_Account__c= Tacc.id;
         update dsmt;
        ApexPages.currentPage().getParameters().put('uid',user1.id);
        dsmtNewIntakeController controller = new dsmtNewIntakeController();
        controller.GetContactInfo();
        }
      Test.StopTest();  
        }
       // controller2.gridpage();
    }
}