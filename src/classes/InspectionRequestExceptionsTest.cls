@istest
public class InspectionRequestExceptionsTest 
{
	@istest
    static void runtest()
    {
        Inspection_Request__c ir =new Inspection_Request__c();
 		ir.Air_Sealing_Only__c=false;
        ir.Address__c='surat';
        insert ir;
        
        Exception_Template__c et =new Exception_Template__c();
        et.Reference_ID__c='IR-00001';
        et.Type__c='Insurance';
        et.Online_Portal_Text__c='test';
        et.Active__c=true;
        et.Automatic__c=true;
        et.Inspection_Request_Level_Message__c=true;
        insert et;
        
        Exception__c exc =new Exception__c();
        exc.Inspection_Request__c=ir.id;
        exc.Disposition__c='Resolved';
        exc.Exception_Template__c=et.id;
        insert exc;
             
        //List<Inspection_Request__c> IRlist =new List<Inspection_Request__c>();
        
        InspectionRequestExceptions ire =new InspectionRequestExceptions();
        //InspectionRequestExceptions.InsertExceptionsOnir(IRlist);
    }
}