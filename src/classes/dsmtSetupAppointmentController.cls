public class dsmtSetupAppointmentController
{
    
    public Appointment__c appointment {get;set;}
    public list<SelectOption> customerList {get;set;}
    public string projectName {get;set;}
    
    private string projectId;
    
    public dsmtSetupAppointmentController()
    {
        init();
    }
    
    private void init()
    {
        projectId = ApexPages.currentPage().getParameters().get('id');
        customerList = new list<SelectOption>();
        appointment = new Appointment__c();
        
        fillCustomerList();
        queryProject();
    }
    
    private void queryProject()
    {
        list<Recommendation_Scenario__c> projectList = [select id, Name,
                                                       (select id, Name, Appointment_Start_Time__c,
                                                        Appointment_End_Time__c, DSMTracker_Contact__c,
                                                        Appointment_Status__c
                                                        from Appointments__r
                                                        order by CreatedDate desc
                                                        limit 1)
                                                        from Recommendation_Scenario__c
                                                        where id =: projectId];
        
        if(projectList.size() > 0)
        {
            projectName = projectList[0].Name;
            
            if(projectList[0].Appointments__r.size() > 0)
                appointment = projectList[0].Appointments__r[0];
        }
    }
    
    private void fillCustomerList()
    {
        customerList = new list<SelectOption>();
        customerList.add(new SelectOption('', '-None-'));
        for(DSMTracker_Contact__c d : [select id, Name from DSMTracker_Contact__c limit 5])
        {
            customerList.add(new SelectOption(d.Id, d.Name));
        }
    }
    
    public PageReference saveAppt()
    {
        string sideBar = ApexPages.currentPage().getParameters().get('showHeader');
        
        if(projectId != null)
        {
            if(sideBar == null){
                appointment.Project__c = projectId;
                upsert appointment;
            }
            else{
                appointment.Project__c = projectId;
                appointment.Appointment_Status__c = 'Scheduled';
                
                upsert appointment;
                
                update new Recommendation_Scenario__c(id = projectId,
                                                      Status__c = 'Scheduled');
            }
        }
        
        if(sideBar == null)
            return new PageReference('/'+projectId).setRedirect(true);
        else
            return null;
    }
}