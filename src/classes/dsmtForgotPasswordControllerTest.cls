@isTest
public class dsmtForgotPasswordControllerTest {

    testmethod static void testRun()
    {
        dsmtForgotPasswordController cntrl = new dsmtForgotPasswordController();
        
        list<Checklist_Items__c> listItem = cntrl.getChecklists();
        Checklist__c parent = new Checklist__c();
        parent.Unique_Name__c = 'Trade_Ally_Forgot_Password_Form';
        parent.Reference_ID__c='121221';
        insert parent;
        
        Checklist_Items__c item = new Checklist_Items__c();
        item.Parent_Checklist__c = parent.Id;
        item.Reference_ID__c='121221';
        item.Checklist__c='test';
        item.Special_instruction_for_public_portal__c=true;
        insert item;
        
        User u = Datagenerator.CreatePortalUser();
        
        cntrl.forgotPassword();
        cntrl.username = 'teererere';
        cntrl.forgotPassword();
        cntrl.username = 'tuser11@test.org';
        cntrl.forgotPassword();
        cntrl.username = u.Username;
        cntrl.forgotPassword();
        cntrl.password = 'Password';
        cntrl.forgotPassword();
        cntrl.sectionTitle='Forgot Password';
        
        cntrl.forgotPassword();
        cntrl.forgotPassword2();
        Checklist__c pgCheckList = cntrl.pgCheckList;
        listItem = cntrl.getChecklists();
    }
}