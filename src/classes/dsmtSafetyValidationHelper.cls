public class dsmtSafetyValidationHelper extends dsmtEnergyAssessmentValidationHelper{
    public ValidationCategory validate(String categoryTitle,String uniqueName,Id safetyAspectId,ValidationCategory otherValidations){
        ValidationCategory safety = new ValidationCategory(categoryTitle,uniqueName);
        
         if(safetyAspectId != null){
             String SafetyTestQuery = ',(SELECT ' + getSObjectFields('Safety_Test__c') + ',RecordType.Name FROM Safety_Tests__r) ';
             String safetyAspectItemsQuery = '(SELECT ' + getSObjectFields('Safety_Aspect_Item__c') + ' FROM Safety_Aspect_Items__r)';
             String soql = 'SELECT ' + getSObjectFields('Safety_Aspect__c') + ',{safetyAspectItemsQuery} ,{SafetyTestQuery} FROM Safety_Aspect__c WHERE Id=:safetyAspectId'.replace('{safetyAspectItemsQuery}',safetyAspectItemsQuery).replace(',{SafetyTestQuery}',SafetyTestQuery);
            
            List<Safety_Aspect__c> safetyAspects = Database.query(soql);
            
            if(safetyAspects.size() > 0){
                Safety_Aspect__c safetyAspect = safetyAspects.get(0);
                ValidationCategory safetyAspectValidations = new ValidationCategory('Safety Aspect General',safetyAspect.Id);
                validateGeneral(safetyAspectValidations,safetyAspect,otherValidations);
                if(safetyAspect.Safety_Aspect_Items__r != null && safetyAspect.Safety_Aspect_Items__r.size() > 0){
                    for(Safety_Aspect_Item__c item : safetyAspect.Safety_Aspect_Items__r){
                        ValidationCategory itemValidations = new ValidationCategory(item.Name,item.Id);
                        validateSafetyAspectItem(itemValidations,item,otherValidations);
                        safetyAspectValidations.addCategory(itemValidations);
                    }
                }
                if(safetyAspect.Safety_Tests__r != null && safetyAspect.Safety_Tests__r.size() > 0){
                    for(Safety_Test__c test : safetyAspect.Safety_Tests__r ){
                        ValidationCategory testValidations = new ValidationCategory(test.Name,test.Id);
                        validateSafetyAspectTest(testValidations,test,otherValidations);
                        safetyAspectValidations.addCategory(testValidations);
                    }
                }
                safety.addCategory(safetyAspectValidations);
            }
        }
        return safety;
    }
    public void validateGeneral(ValidationCategory validations,Safety_Aspect__c safetyAspect,ValidationCategory otherValidations){
        
    }
    public void validateSafetyAspectItem(ValidationCategory validations,Safety_Aspect_Item__c safetyAspectItem,ValidationCategory otherValidations){
    
    }
    public void validateSafetyAspectTest(ValidationCategory validations,Safety_Test__c safetyTest,ValidationCategory otherValidations){
        //validations.infos.add(new ValidationMessage(safetyTest.RecordType.Name));
        
        if((safetyTest.Test_Type__c == null || safetyTest.Test_Type__c.trim() == '') && safetyTest.RecordType.Name != 'Combustion Test'){
            validations.errors.add(new ValidationMessage('Test Type is required'));
        }else if(safetyTest.Test_Type__c != null && safetyTest.Test_Type__c.trim() == 'Unable To Perform' && (safetyTest.Notes__c == null || safetyTest.Notes__c.trim() == '')){
            validations.errors.add(new ValidationMessage('Please enter the reason for \'Unable to Perform\''));    
        }
        
        if(safetyTest.RecordType.Name == 'Appliance Test'){
            if(safetyTest.Appliance_Combustion_Device__c == null){
                validations.errors.add(new ValidationMessage('Applaince is required'));
            }
        }else if(safetyTest.RecordType.Name == 'Combustion Test Device'){
            if(safetyTest.Mechanical_Type_Combustion_Device__c == null){
                validations.errors.add(new ValidationMessage('Device is required'));
            }
        }
        
    }
}