public class ReviewExceptions
{
   public static boolean isStopValidateException = false;
   public static void InsertExceptionsOnRR(List<Review__c> RRlist){
       
        Map<String,Exception_Template__c> mapExt = new Map<String,Exception_Template__c>();     
        //Gets all expection with that check box checked.
        for(Exception_Template__c ext : [select id,name,Reference_ID__c,Type__c,Online_Portal_Title__c,Online_Portal_Text__c,Online_Application_Section__c,Required_Attachment_Type__c,Internal__c,Exception_Message__c,Outbound_Message__c,Short_Rejection_Reason__c,Attachment_Needed__c from Exception_template__c where 
        Automatic__c = true and Active__c = true and Review_Level_Message__c = true ]) {
          mapExt.put(ext.Reference_Id__c,ext);
        } 
        
        Set<Id> rrids = new Set<id>();
        for(Review__c rr :RRlist){
             rrids.add(rr.id);   
        }
        
        Map<String,Exception__c> mapExceptions = new Map<String,Exception__c>();
        for(Exception__c Ex :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Review__c, Review__r.id FROM Exception__c 
                                WHERE Review__r.id IN:rrids AND Disposition__c  != 'Resolved']){    
            mapExceptions.put(Ex.Review__c+'-'+Ex.Exception_Template__r.Reference_ID__c,Ex);     
        }      
        
        List<Exception__c> ExceptionsCreated = new List<Exception__c>();
          
        for(Review__c RRNew : RRlist) {
            
            List<string> AutoExceptionsList = new List<string>();
            
            for(String refId : mapExt.keyset()){
                Exception_Template__c temp = mapExt.get(refId);
               
                if(temp != null){
                      
                     if(refId == 'Review-001' && RRNew.Status__c != 'Approved' && RRNew.Status__c != 'Rejected' && RRNew.RecordType.Name == 'Change Order Review' && RRNew.Reviewed_COC__c ==false){
                        If(mapExceptions.get(RRNew.id+'-Review-001')==null) 
                            AutoExceptionsList.add('Review-001'); 
                     }
               }
                
           }
           
            system.debug('--AutoExceptionsList--'+AutoExceptionsList);
            for(string autolist :AutoExceptionsList){
                if(mapExt.get(autolist) !=null) {
                    Exception_Template__c Exceptiontemp = mapExt.get(autolist);
                    Exception__c ExceptionstoAdd = new Exception__c(); 
                    ExceptionstoAdd.Internal__c = Exceptiontemp.Internal__c; 
                    ExceptionstoAdd.Exception_Message__c= Exceptiontemp.Exception_Message__c; 
                    ExceptionstoAdd.Outbound_Message__c=Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Review__c =  RRNew.Billing_Review__c;
                    ExceptionstoAdd.Exception_Template__c = Exceptiontemp.id;
                    ExceptionstoAdd.FInal_Outbound_Message__c = Exceptiontemp.Outbound_Message__c;
                    ExceptionstoAdd.Short_Rejection_Reason__c = Exceptiontemp.Short_Rejection_Reason__c ;
                    ExceptionstoAdd.Automated__c = true;
                    ExceptionstoAdd.OwnerId =RRNew.OwnerId;
                    ExceptionstoAdd.Attachment_Needed__c = Exceptiontemp.Attachment_Needed__c;
                    ExceptionstoAdd.Required_Attachment_Type__c = Exceptiontemp.Required_Attachment_Type__c;
                    ExceptionstoAdd.Online_Application_Section__c = Exceptiontemp.Online_Application_Section__c;
                    ExceptionstoAdd.Type__c = Exceptiontemp.Type__c;
                    //ExceptionstoAdd.Automatic_Communication__c = Exceptiontemp.Automatic_Communication__c;
                    //ExceptionstoAdd.Automatic_Rejection__c = Exceptiontemp.Automatic_Rejection__c;
                    //ExceptionstoAdd.Move_to_Incomplete_Queue__c = Exceptiontemp.Move_to_Incomplete_Queue__c;
                    ExceptionstoAdd.Online_Portal_Title__c  = Exceptiontemp.Online_Portal_Title__c;
                    ExceptionstoAdd.Online_Portal_Text__c = Exceptiontemp.Online_Portal_Text__c;
                    ExceptionsCreated.add(ExceptionstoAdd);    
                }
            }     
       }
        
        system.debug('--ExceptionsCreated--'+ExceptionsCreated);
        ProjectExceptions.isStopValidateException = true;
        if(ExceptionsCreated.size()>0){
           insert ExceptionsCreated;  
        }   
   }
   
   public static void CheckUpdateExceptions(List<Review__c> RRlist , Map<Id,Review__c> oldmap){
        Map<string,List<Exception__c>> ExceptionsMap = new Map<string,List<Exception__c>>();
        for(Exception__c Exceptions :[SELECT id,name, Disposition__c, Exception_Template__c,Automated__c,Exception_Template__r.Reference_ID__c,Review__c, Review__r.id FROM Exception__c 
                                WHERE (Review__r.id=:oldmap.keyset() or Review__r.Id =: RRList.get(0).Billing_Review__c) AND Disposition__c  != 'Resolved']){
                List<Exception__c> lsExceptions = ExceptionsMap.get(Exceptions.Review__c+'-'+Exceptions.Exception_Template__r.Reference_ID__c);
                if(lsExceptions  == NULL)
                    lsExceptions  = new List<Exception__c>();
                lsExceptions.add(Exceptions );
                ExceptionsMap.put(Exceptions.Review__c+'-'+Exceptions.Exception_Template__r.Reference_ID__c,lsExceptions);        
        }
        
         List<Exception__c> ExceptionstoUpdate = new List<Exception__c>();
         Set<Id> RRId = new Set<Id>();
         for(Review__c RR : RRlist){
              RRId.add(RR.Id);        
         }
         
         for(Review__c rr : RRlist){
                 if(ExceptionsMap.containskey(rr.id + '-Review-001')){  
                     if(rr.Reviewed_COC__c){
                        for(Exception__c exp : ExceptionsMap.get(rr.id + '-Review-001')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                if(ExceptionsMap.containskey(rr.Billing_Review__c+ '-Review-001')){  
                     if(rr.Status__c == 'Approved' || rr.Status__c == 'Rejected'){
                        for(Exception__c exp : ExceptionsMap.get(rr.Billing_Review__c+ '-Review-001')){
                            exp.Disposition__c ='Resolved';
                            ExceptionstoUpdate.add(exp);
                        }
                    }    
                }
                
                
            }
         
            ReviewExceptions.isStopValidateException = true;
            system.debug('--ExceptionstoUpdate--'+ExceptionstoUpdate);
            if(ExceptionstoUpdate.size()>0){
                update ExceptionstoUpdate;
            }
   }
}