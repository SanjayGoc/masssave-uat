@istest
public class dsmtUpdateProjectsStatusControllerTest 
{
	@istest
    static void test()
    {
        Recommendation_Scenario__c  project =new Recommendation_Scenario__c();
        project.Status__c='Proposed';
        insert project;
        
        ApexPages.currentPage().getParameters().put('ids',project.id);
        dsmtUpdateProjectsStatusController dpsc =new dsmtUpdateProjectsStatusController();
        dpsc.saveAppt();
        dsmtUpdateProjectsStatusController.saveAppt(project.id, 'status');
        //dsmtUpdateProjectsStatusController.saveSchedule(project.id, 'dateStr', 'endDateStr');
        dsmtUpdateProjectsStatusController.CreateReview(project.id);
    }
}