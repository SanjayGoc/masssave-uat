@istest
public class dsmtValidateExceptionsRRTest 
{
    @istest
    static void runtrest()
    {
        Exception_Template__c et = new Exception_Template__c ();
        et.Reference_ID__c='ETN-0000001';
        et.Type__c='Insurance';
        et.Automatic__c = true;
        et.Active__c = true;
        //et.Project_Level_Message__c = true;
        et.Registration_Request_Level_Message__c=true;
        insert et;
        
         
        Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'test';
        ta.Internal_Account__c = true;
        insert ta;
        
        Registration_Request__c rr =new Registration_Request__c();
        
        //rr.id=et.id;
        rr.Trade_Ally_Account__c=ta.id;
        insert rr;
        
        Exception__C ex = new Exception__c();
        ex.Disposition__c='Resolved';
        ex.Registration_Request__c=rr.id; 
        ex.Exception_Template__c = et.id;
        insert ex; 
        
        dsmtValidateExceptionsRR dver =new dsmtValidateExceptionsRR();
        
        et.Reference_Id__c ='ETN-0000001';
        rr.HIC_Attached__c =false;
        update et;
        
        /*dsmtValidateExceptionsRR dver1 =new dsmtValidateExceptionsRR();
        et.Reference_Id__c ='ETN-0000002';
        rr.W_9_Attached__c=false;
        update et;
        
        dsmtValidateExceptionsRR dver2 =new dsmtValidateExceptionsRR();
        et.Reference_Id__c ='ETN-0000003';
        update et;*/
    }
}