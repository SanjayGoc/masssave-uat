global class dsmtCustomerSearchImplement implements dsmtCustomerSearchInterface {
    
    /*global List<Customer__c> get_customers(Map<String,String> filter_map,Map<String,String> filter_map_Account){
         List<Customer__c>custmr_lst  = new List<Customer__c>();
         if(filter_map.size() > 0){
           
             custmr_lst =  Customer_Utility.SearchCustomer(filter_map,filter_map_Account);
            
        }    
        
        return custmr_lst;
    }*/
    
    global List<Sobject> getSObjects(Map<String,String> filter_map,string Result_Fields){
        
        String ProgramId = filter_map.get('Program_Id');
        // Case ProgramId = MassSave Resedential
        List<Sobject> sobj_Lst = new List<Sobject>();
        
        sobj_Lst = dsmtMasSaveCustomerSearch.SearchCustomer(filter_map,Result_Fields);
        return sobj_Lst;
    }

}