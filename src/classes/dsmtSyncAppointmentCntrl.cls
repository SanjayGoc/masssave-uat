public with sharing class dsmtSyncAppointmentCntrl{

     public Employee__c filtertObj{get;set;}
     public String empid{get;set;}
     public String locid{get;set;}
     
     public Employee__c empObj{get;set;}
     public Location__c locObj{get;set;}
     public Date startDate{get;set;}
     public Date endDate{get;set;}
     public String stDate{get;set;}
     public String eDate{get;set;}
     public String recordId{get;set;}
     public String sObjType{get;set;}
     
     public dsmtSyncAppointmentCntrl(){
        
        filtertObj = new Employee__c();
        
        empid = apexpages.currentpage().getparameters().get('empid');
        locid = apexpages.currentpage().getparameters().get('locid');
        
        system.debug('--empid--'+empid);
        system.debug('--locid--'+locid);
        
        filtertObj.Employment_Start_Date__c = Date.Today();
        filtertObj.Employment_End_Date__c = Date.Today().addDays(30);
        
        startDate = System.today();
        stDate = System.today().format();
        endDate = startDate.Adddays(30);
        
        empObj = new Employee__c();
        locObj = new Location__c();
        
        if(empid != null && empid.trim().length() > 0){
            recordId = empid;
            List<Employee__c> emplist = [Select Id,Name From Employee__c Where Id =: empid];    
            if(emplist.size() > 0){
               empObj = emplist.get(0);
            }
        } else {
            recordId = locid;
            List<Location__c> loclist = [Select Id,Name From Location__c Where Id =: locid];          
            if(loclist.size() > 0){
               locObj = loclist.get(0);
            }
            
        }
    }
     
    public Pagereference saveAndUpdate(){
        
        
        System.debug('--stDate--'+stDate);
        System.debug('--eDate--'+eDate);
        
        if(stdate == null || stdate.trim().length() == 0){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please enter startDate.'));
           return null;
        }
        
        if(eDate == null || eDate.trim().length() == 0){
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please enter endDate.'));
           return null;
        }
        
        Date SD = date.parse(stDate);
        Date ED = date.parse(eDate);
        List<String> Status = new List<String>{'Scheduled','Cancelled'};
        Set<String> appointmentIds = new Set<String>();
 
        
        String query = 'SELECT id,name From Appointment__c WHERE' +
                       ' Appointment_Start_Date__c >=: SD AND Appointment_End_Date__c <=: ED and Appointment_Status__c != \'Unavailable\' and Appointment_Status__c != \'New\' '; 
                    /*   ' AND Appointment_Status__c In: Status';*/
                       
        if(empid != null && empid.trim().length() > 0){
            system.debug('--empObj --'+empObj );
            query += ' And Employee__c =: empid ' ;
        }else{
            system.debug('--locObj --'+locObj );
           query += ' And Employee__r.Location__c =: locid';
        }
        
        for(Appointment__c app : Database.query(query)){
            system.debug('--app--'+app);
            appointmentIds.add(app.id);
        } 
        
        if(appointmentIds.size() > 0 && !Test.isRunningtest())
           dsmtFieldToolCallout.SyncAppointmentSFDCToMobile(appointmentIds,true);
        
        
        DSMTracker_Log__c log = new DSMTracker_Log__c();
        log.Button_clicked__c = 'Sync Appointment to Server';
        log.User_Time__c = Datetime.now();
        if(empid != null && empid.trim().length() > 0){
            log.Type__c = 'Employee : '+empObj.Name+' ('+empid+')';
        }else{
            log.Type__c = 'Location : '+locObj.Name+' ('+locid+')';
        }
        log.Log__c = 'Date range => Startdate : '+SD+', EndDate : '+ED+', UserInfo => clicked by : '+userInfo.getName()+', Username : '+userinfo.getUsername()+', UserId : '+userinfo.getuserId();
        insert log;

        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info,'Sync appointments request has been completed.'));  
        return null; 
        
     }
     
}