/*    
        Name           : SendWelcomeNewEmailController
        Author         : 
        Date           : 18th March 2017
        Description    : This controller send welcome email to Registrant
*/

public class SendWelcomeNewEmailController{
    public String emailSubject{set;get;}
    public String emailBody{set;get;}
    public String emailTo{set;get;}
    public String emailCC{set;get;}
    public String emailBCC{set;get;}
    public String RRId{set;get;}
    public Boolean isError{set;get;}
    
    public String selTemplateId {
        get;
        set;
    }
    public string toId {
        get;
        set;
    }
    
    private String accId;
    
    public SendWelcomeNewEmailController(){
        isError = false;
        
        selFolderId = ApexPages.currentPage().getParameters().get('folderId');
        selTemplateId = ApexPages.currentPage().getParameters().get('TemplateID');
        
        toId       = emailTo = emailCC = '';
        
        if(selTemplateId == null){
           getAllEmailTemplateFolders();
        }
    }
    
    public String selFolderId{get;set;}
    public List<SelectOption> folderList{get;set;}
    public List<SelectOption> templateList{get;set;}
    
     /**
     * Get Email Templates By Folder to fill Folder dropdown 
     *
     * @param Nothing 
     * @return Nothing.
     */
    
    public void getAllEmailTemplateFolders(){
       folderList = new List<SelectOption>();
       templateList = new List<SelectOption>();
       
       Map<String,String> folderIdtoName = new Map<String,String>();
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true order by FolderId];
       
       for(EmailTemplate et : emailTemplatelist){
            
          // if(et.Folder.Name == 'DSMT MSD Custom' || et.Folder.Name == 'DSMT Smart $aver Custom'){
               folderIdtoName.put(et.folderId,et.Folder.Name);
          // }
       }
       system.debug('--folderIdtoName--'+folderIdtoName);
        folderList.add(new SelectOption('','--Select--'));
        templateList.add(new SelectOption('','--Select--'));
        for(String folderid : folderIdtoName.keySet()){
           system.debug('--folderid--'+folderid);
           system.debug('--folderIdtoName.get(folderid)--'+folderIdtoName.get(folderid));
           if(folderIdtoName.get(folderid) != null)
             folderList.add(new SelectOption(folderid, folderIdtoName.get(folderid)));
        }
    }
    
    
    /**
     * Get Email Templates By Folder to fill  template dropdown
     *
     * @param Nothing 
     * @return Nothing.
     */
   
    public void getEmailTemplatesByfolder(){
       templateList = new List<SelectOption>();
       templateList.add(new SelectOption('','--Select--'));
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true and FolderId =: selFolderId order by Name];
       for(EmailTemplate et : emailTemplatelist){
          templateList.add(new SelectOption(et.Id, et.Name));
       }
    
    }
    
    public void init(){
        emailSubject = '';
        emailBody = '';
        emailTo = '';
        emailCC = '';
        emailBCC = '';
        accId = '';
        isError = false;
        RRId = Apexpages.currentPage().getParameters().get('id');
        toId = RRId;
        if(RRId != null && RRId.length() >= 15){
            List<Registration_Request__c> listRR = [select Id,First_Name__c,Last_Name__c,Email__c,Status__c from Registration_Request__c where id =: RRId];
            if(listRR != null && listRR.size() > 0){
                if(listRR[0].Email__c != null){
                    //accId = listContact[0].AccountId;
                    if(listRR[0].Email__c != null){
                        emailTo = listRR[0].Email__c;
                    }
                    
                    List<Email_Custom_Setting__c> listEmailSett = [select id,Email_Template__c from Email_Custom_Setting__c LIMIT 1];
                    if(listEmailSett != null && listEmailSett.size() > 0){
                        List<EmailTemplate> listTemp = [select Id from EmailTemplate where DeveloperName =: listEmailSett[0].Email_Template__c];
                        if(listTemp != null && listTemp.size() > 0){
                        
                            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            
                            mail.setToAddresses(new String[] {emailTo});
                            mail.setTemplateId(listTemp[0].Id);
                            
                            List < Contact > lstContact = [select id, name, email from contact where Name = 'Do Not Delete'];
                            Id conId = null;
                            if(lstContact.size()>0){
                                conId = lstContact[0].id;
                            }
                            else{
                                List<Account> lstAcc = [select id from Account where Name='UCS'];
                                Id AccId = null;
                                if(lstAcc.size()>0)
                                    AccId = lstAcc[0].Id;
                                else{
                                    Account acc = new Account(Name='UCS');
                                    insert acc;
                                    
                                    AccId = acc.Id;
                                }
                                Contact c = new Contact(LastName='Do Not Delete',AccountId=AccId ,Email='support+dnd@unitedcloudsolutions.com');
                                insert c;
                                conId = c.Id;
                            }
                            system.debug('@@RRId@@'+RRID+'@@ConId@@'+conId);
                            mail.setTargetObjectId(conId );
                            mail.setWhatId(RRId);
                            mail.setSaveAsActivity(false);
                            mailList.add(mail);
                            
                            try{
                                Boolean isErr = false;
                                // send email
                                Savepoint sp = Database.setSavepoint();
                                if(!Test.isRunningTest()){
                                    try{
                                        Messaging.sendEmail(mailList,true);
                                    }catch(Exception expp){
                                        isErr = true;
                                     
                                    }
                                }
                                Database.rollback(sp);
                                //if(!isErr){
                                    if(mail.getHTMLBody() != null){
                                        //emailBody= htmlToText(mail.getHTMLBody());
                                        emailBody= mail.getHTMLBody();
                                    }else{
                                        emailBody = mail.getPlainTextBody();
                                    }
                                    emailSubject = mail.getSubject();
                                //}
                                
                                system.debug('##### emailSubject = '+emailSubject);
                                system.debug('##### emailBody = '+emailBody);
                            }catch(Exception exp){
                                isError = true;
                                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,exp.getMessage()));
                            }
                        }
                    }
                }else{
                    isError = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Address Not Found'));
                }
            }
        }
    }
    
    public Pagereference cancel(){
        return new Pagereference('/'+RRId);
    }
    
    public Pagereference sendEmail(){
        system.debug('######## emailTo = '+emailTo);
        system.debug('######## emailCC = '+emailCC);
        system.debug('######## emailBCC = '+emailBCC);
        
        List<String> toAddress = new List<String>();
        if(emailTo != null && emailTo.trim().length() > 0){
            for(string s :emailTo.split(',')){
                if(s!=null && s!='')
                   toAddress.add(s); 
            }
            
        }
       // emailBody = htmlToText(emailBody);
        if(toAddress.size() > 0){
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setSaveAsActivity(false);
            message.setSubject(emailSubject);
            message.setHtmlBody(emailBody);
            message.setToAddresses(toAddress);
            if(emailCC != null && emailCC.trim().length() > 0){
                message.setCcAddresses(emailCC.split(','));
            }
            if(emailBCC != null && emailBCC.trim().length() > 0){
                message.setBccAddresses(emailBCC.split(','));
            }
            
            
            
            List<Messaging.EmailFileAttachment> listFA = new List<Messaging.EmailFileAttachment>();
            List<Document> listDocuments = new List<Document>();
            List<Email_Custom_Setting__c> listEmailSett = [select id,Registration_Documents_Folder__c from Email_Custom_Setting__c LIMIT 1];
            if(listEmailSett != null && listEmailSett.size() > 0){
                String folderName = listEmailSett[0].Registration_Documents_Folder__c;
                listDocuments = [select id,Name,Body,description,ContentType from Document where Folder.Name =: folderName];
                for(Document doc : listDocuments){
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setBody(doc.Body);
                    efa.setFileName(doc.Name);
                    efa.setContentType(doc.ContentType);
                    listFA.add(efa);
                }
            }
            if(listFA.size() > 0){
                message.setFileAttachments(listFA);
            }
            mailList.add(message);
            
            try{
                if(!Test.isRunningTest()){
                    try{
                        Messaging.sendEmail(mailList,true);
                    }catch(Exception expp){
                        system.debug('###### expp = '+expp);
                    }
                }
                
                
                List<Registration_Request__c> listRR = [select id,Status__c,DSMTracker_Contact__c,DSMTracker_Contact__r.Trade_Ally_Account__c from Registration_Request__c where id =: RRId];
                if(listRR  != null && listRR.size() > 0){
                    listRR [0].Status__c = 'Registration Request Sent';
                    
                    update listRR;
                    
                    if(listRR[0].DSMTracker_Contact__r.Trade_Ally_Account__c!=null){
                        List<Trade_Ally_Account__c> lstTAC = [select Id,Status__c,Stage__c from Trade_Ally_Account__c where Id = :listRR[0].DSMTracker_Contact__r.Trade_Ally_Account__c AND Status__c='Prospecting' AND Stage__c = 'Prospecting' Limit 1];
                        if(lstTAC.size()>0){
                            lstTAC[0].Status__c = 'Registration Request Sent';
                            
                            update lstTAC;
                        }
                    }
                }
                
                string body = '';
                if(message.getHTMLBody()!=null){
                    body = message.getHTMLBody();
                    body = body.replace('<meta charset="utf-8">', '').replace('<br>', '<br/>');
                    body += '</body>';
                }
                
                Messages__c msg = new Messages__c();
                msg.From__c = userinfo.getUserEmail();
                msg.To__c = string.join(message.getToAddresses(),',');
                
                if(message.getCCAddresses()!=null)
                    msg.CC__c = string.join(message.getCCAddresses(),',');
                if(message.getBCCAddresses()!=null)
                    msg.BCC__c = string.join(message.getBCCAddresses(),',');
                    
                msg.Registration_Request__c = RRId;
                msg.Subject__c = message.getSubject();
                msg.Message__c = body ;
                msg.Mesage_Text_Only__c = htmlToText(body );
                msg.Message_Type__c = 'Send Email';
                msg.Message_Direction__c  = 'Outbound';
                msg.Status__c = 'Sent';
                msg.Sent__c = Datetime.Now();
                if(listRR[0].DSMTracker_Contact__c!=null)
                    msg.DSMTracker_Contact__c = listRR[0].DSMTracker_Contact__c;
                if(listRR[0].DSMTracker_Contact__r.Trade_Ally_Account__c !=null)
                    msg.Trade_Ally_Account__c = listRR[0].DSMTracker_Contact__r.Trade_Ally_Account__c;
                     
                insert msg;
                system.debug('Message' +msg);
                  system.debug('FromAddress' +msg.From__c);
                //Message.CreateMessage(fromAddress,toAddress,emailCC,emailBCC,RRId,emailSubject,emailBody,'Send Email');
                // create activity history
               // createTasks();
               
                
                // create attachments
                createAttachments(msg.id,listDocuments);
            }catch(Exception exp){
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,exp.getMessage()));
            }
        }
        return new Pagereference('/'+RRId);
        
    }
    
    private void createAttachments(String msgId,List<Document> listDocuments){
        if(listDocuments != null){
            List<Attachment__c> listAttachment = new List<Attachment__c>();
            
            /*for(Document doc : listDocuments){
                Attachment__c att = new Attachment__c();
                att.Attachment_Name__c = doc.Name;
                //att.Account__c = accId;
                att.Status__c = 'New';
                att.description__c=doc.description;
                att.Message__c = msgId;
                
                listAttachment.add(att);
            }*/
            
            //if(listAttachment.size() > 0){
                //insert listAttachment;
                
                List<Attachment> listAtt = new List<Attachment>();
                for(Document doc : listDocuments){
                    //for(Attachment__c att : listAttachment){
                        //if(att.Attachment_Name__c == doc.Name){
                            Attachment stdAtt = new Attachment();
                            stdAtt.Name = doc.Name;
                            stdAtt.ParentId = msgId;
                            stdAtt.ContentType = doc.ContentType;
                            stdAtt.body = doc.Body;
                            listAtt.add(stdAtt);
                        //}
                    //}
                }
                if(listAtt.size() > 0){
                    insert listAtt;
                }
           // }
            
        }
    }
    
    /*private void createTasks(){
        List<Task>lstTask = new List<Task>();
        Task tsk = new Task(WhatId = RRId,Subject=emailSubject);
        tsk.Description = emailBody;
        tsk.Priority = 'High';
        tsk.Status = 'Completed';
        tsk.ActivityDate = System.today();
        lstTask.add(tsk);
        
        
        if(lstTask.size() > 0){
            insert lstTask;
        }
    }*/
    
    
    
    public List<AttachmentModel> getDocumentsName(){
        List<AttachmentModel> listSt = new List<AttachmentModel>();
        List<Email_Custom_Setting__c> listEmailSett = [select id,Registration_Documents_Folder__c from Email_Custom_Setting__c LIMIT 1];
        if(listEmailSett != null && listEmailSett.size() > 0){
            String folderName = listEmailSett[0].Registration_Documents_Folder__c;
            for(Document doc : [select id,Name,ContentType,CreatedDate,CreatedById,CreatedBy.Name from Document where Folder.Name =: folderName]){
                AttachmentModel mod = new AttachmentModel();
                mod.name = doc.name;
                mod.type = doc.ContentType;
                mod.createdDate = doc.CreatedDate.date();
                mod.createdBy = doc.CreatedBy.Name;
                mod.createdById = doc.CreatedById;
                listSt.add(mod);
            }
        }
        return listSt;
    }
        
    private static String htmlToText(String htmlString) {
        String RegEx = '(</{0,1}[^>]+>)';
        if (htmlString == null)
            htmlString = '';
        return htmlString.replaceAll('&nbsp', '').replaceAll(';', '').replaceAll(RegEx, '').replaceAll('&#39','\'');
    } 
    
    public class AttachmentModel{
        public String name{set;get;}
        public String type{set;get;}
        public Date createdDate{set;get;}
        public String createdBy{set;get;}
        public String createdById{set;get;}
        
        public AttachmentModel(){
            name = '';
            type = '';
            createdBy = '';
            createdById = '';
        }
    }
    
    /**
     * Reload page based on Program
     *
     * @param Nothing 
     * @return Pagereference .
     */
   
    public PageReference reloadPage(){
       PageReference pg = null;
       //if(ea.Program_Name__c =='Smart $aver Custom' || ea.Program_Name__c =='MSD Custom'){
           pg = Page.WelcomeNewEmail;
       //}else{
       //    pg = Page.dsmtNewEmail;
       //}
       pg.getParameters().put('id', RRId);
       pg.getParameters().put('TemplateID', selTemplateId);
       //pg.getParameters().put('sendEmail', String.valueOf(isSendEmail));
       pg.getParameters().put('folderId',selFolderId);
       //pg.getParameters().put('MIR','true');
       //pg.getParameters().put('Pagename','sendemail');
       pg.setRedirect(true);
       return pg;
    }
    
}