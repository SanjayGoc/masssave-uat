public class dsmtAttachmentsController extends dsmtEnergyAssessmentBaseController {
    public List<Attachment__c> getEAAttachments(){
        return [
            SELECT Id,Attachment_Name__c,Attachment_Type__c,Description__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c,Energy_Assessment__c
            FROM Attachment__c
            WHERE Energy_Assessment__c =:getParam('assessid') AND Attachment_Type__c EXCLUDES('DownloadedFile')
            ORDER BY CreatedDate DESC 
        ];
    }
}