@isTest
    public class dsmtRecommendationHelperTest
    {  
        public static testmethod void test1()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =Datagenerator.setupAssessment();
            ea.Assessment_Type__c='Landlord Visit';
            ea.Primary_Provider__c='Eversource East Electric';
            ea.Secondary_Provider__c='National Grid Electric';
            update ea;
            test.starttest();
            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Name = 'Install an Aube Line Voltage Thermostat';
            dsm.Sub_Category__c='Air Sealing' ;
            dsm.Category__c = 'Direct Install' ;
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            test.stopTest();
            
        }
        public static testmethod void test2()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =Datagenerator.setupAssessment();
            ea.Assessment_Type__c='Landlord Visit';
            ea.Primary_Provider__c='Eversource West Electric';
            update ea;
            test.startTest();
            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Air Sealing' ;
            dsm.Category__c = 'Air Sealing' ;
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR416';
            insert rem;
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            test.stopTest();
        }
        
        public static testmethod void test3()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =Datagenerator.setupAssessment();
            
            ea.Assessment_Type__c='Landlord Visit';
            ea.Primary_Provider__c='Eversource East Gas';
            ea.Secondary_Provider__c='National Grid Electric';
            update ea;
            
            
            test.startTest();

            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Lighting';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;

            List<Recommendation__c> recomms = new List<Recommendation__c>();
            recomms.add(rem);
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'HVAC';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            test.stopTest();
        }
        
        public static testmethod void test4()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =Datagenerator.setupAssessment(); 
            ea.Assessment_Type__c='Renter Visit';
            ea.Primary_Provider__c='Eversource West Electric';
            ea.Secondary_Provider__c='National Grid Electric'; 
            update ea;
            test.startTest();
            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'HVAC';
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            test.stopTest();
            
        }
        public static testmethod void test5()
        {        
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =Datagenerator.setupAssessment();
            ea.Assessment_Type__c='Renter Visit';
            ea.Primary_Provider__c='Eversource East Electric';
            ea.Secondary_Provider__c='National Grid Electric';
            update ea;
            test.startTest();
            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c= 'HVAC';
            dsm.Category__c = 'Heating';
            dsm.Surface__C ='ThermostatdsmtRecommendationHelper';
            insert dsm;
            
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            Test.stopTest();
        }
        public static testmethod void test6()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =Datagenerator.setupAssessment();
            ea.Assessment_Type__c='Renter Visit';
            ea.Primary_Provider__c = 'Eversource East Gas';
            ea.Secondary_Provider__c = 'Eversource East Electric';
            update ea;
            test.startTest();
            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Duct Measures';
            dsm.Category__c = 'Duct Measures';
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;
            
            Set<Id> eaId = new Set<Id>();
            eaId.add(ea.id);
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            
            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            ea.Assessment_Type__c='Landlord Visit';
            update ea ;
            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            
            dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Lighting';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;

            List<Recommendation__c> recomms = new List<Recommendation__c>();
            recomms.add(rem);
            
            cntrl = new dsmtRecommendationHelper();
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'HVAC';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            test.stopTest();
        }
        public static testmethod void test7()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =Datagenerator.setupAssessment();
            ea.Assessment_Type__c='Renter Visit';
            ea.Primary_Provider__c = 'Eversource East Gas';
            ea.Secondary_Provider__c = 'Eversource West Electric';
            update ea;
            test.startTest();
            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Duct Measures';
            dsm.Category__c = 'Duct Measures';
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;
            
            Set<Id> eaId = new Set<Id>();
            eaId.add(ea.id);
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            
            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            ea.Assessment_Type__c='Landlord Visit';
            update ea ;
            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            
            dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Lighting';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            insert dsm;
            
            rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;

            List<Recommendation__c> recomms = new List<Recommendation__c>();
            recomms.add(rem);
            
            cntrl = new dsmtRecommendationHelper();
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'HVAC';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            test.stopTest();
        }

        public static testmethod void test8()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =dsmtEAModel.setupAssessment();
            ea.Assessment_Type__c='Renter Visit';
            ea.Primary_Provider__c = 'Eversource West Electric';
            ea.Secondary_Provider__c = 'Eversource West Gas';
            update ea;
            
            test.startTest();
            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Direct Install';
            dsm.Category__c = 'Lighting';
            dsm.Surface__C ='Thermostat';
            dsm.Name = 'Install an Aube Line Voltage Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;
            
            Set<Id> eaId = new Set<Id>();
            eaId.add(ea.id);
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            
            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            ea.Assessment_Type__c='Landlord Visit';
            update ea ;
            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            
            dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Lighting';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Lighting';
            insert dsm;
            
            rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;

            List<Recommendation__c> recomms = new List<Recommendation__c>();
            recomms.add(rem);
            
            cntrl = new dsmtRecommendationHelper();
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'HVAC';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            test.stopTest();
        }

         public static testmethod void test9()
        {
            dsmtRecursiveTriggerHandler.preventAppointmentTrigger = true;
            dsmtCreateEAssessRevisionController.stopTrigger = true;
            
            Energy_Assessment__c ea =dsmtEAModel.setupAssessment();
            ea.Assessment_Type__c='HEA (Home Energy Assessment)';
            ea.Primary_Provider__c = 'Eversource East Gas';
            ea.Secondary_Provider__c = 'Eversource West Gas';
            update ea;

            Set<Id> eaId = new Set<Id>();
            eaId.add(ea.id);
            
            test.startTest();

            DSMTracker_Product__c dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Direct Install';
            dsm.Category__c = 'Lighting';
            dsm.Surface__C ='Thermostat';
            dsm.Name = 'Install an Aube Line Voltage Thermostat';
            insert dsm;
            
            Recommendation__c rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';            
            insert rem;
            
            dsmtRecommendationHelper cntrl = new dsmtRecommendationHelper();
            
            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            ea.Assessment_Type__c='HEA (Home Energy Assessment)';
            update ea ;

            dsmtRecommendationHelper.setInvoiceTypeFuture(eaId);
            
            dsm = new DSMTracker_Product__c();
            dsm.Sub_Category__c='Lighting';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Lighting';
            insert dsm;
            
            rem = new Recommendation__c();
            rem.Energy_Assessment__c = ea.id;
            rem.Dsmtracker_Product__c =dsm.id;
            rem.Invoice_Type__c =  'NSTAR418';
            insert rem;

            List<Recommendation__c> recomms = new List<Recommendation__c>();
            recomms.add(rem);
            
            cntrl = new dsmtRecommendationHelper();
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'HVAC';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            
            cntrl = new dsmtRecommendationHelper();
            dsm.Sub_Category__c='Heating';
            dsm.Category__c = 'Direct Install';
            dsm.Surface__C ='Thermostat';
            update dsm;
            dsmtRecommendationHelper.setInvoiceType(recomms);
            test.stopTest();
        }
    }