public class dsmtWorkOrderHelper{
    
    public static boolean UpdatefieldsonChild = false;
    public static boolean WorkorderILICreated = false;

    @future(Callout = true)
    public static void updateWorkOrder(Set<Id> woId){
        List<Workorder__c> woList = [select id,Address__c,City__c,State__c,Zipcode__c,work_Team__c,Duration__c,Service_Date__c,
                                        Manually_Created_Workorder__c,Early_Arrival_Time__c,Late_Arrival_Time__c,
                                        Fixed_Workteam__c,Scheduled_Start_Date__c,Scheduled_End_Date__c,External_Reference_ID__c
                                        from workorder__c where id in : woId];
        
        List<dsmtCallOut.workOrderDetail> wdList = new List<dsmtCallOut.workOrderDetail>();
        
        String dtConverted = '';
        for(Workorder__c wo : woList ){
            dsmtCallOut.workOrderDetail wod = new dsmtCallOut.workOrderDetail();
            wod.workOrderId = wo.Id;
            wod.SQLRefId = wo.External_Reference_ID__c ;
            wod.serviceWorkTeamId = wo.Work_Team__c;
            wod.serviceDate = wo.Service_Date__c;
            wod.ManualWorkOrder = wo.Manually_Created_Workorder__c ;
            wod.serviceDuration = integer.valueOf(wo.duration__c);
            wod.IsFixedResource = wo.Fixed_Workteam__c;
            
            if(wo.Early_Arrival_Time__c != null){
                dtConverted = wo.Early_Arrival_Time__c.format('MM/dd/yyyy h:mm a');
                wod.earlyArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);
            }else{
                dtConverted = wo.Scheduled_Start_Date__c.AddMinutes(-30).format('MM/dd/yyyy h:mm a');
                wod.earlyArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);   
            }
            if(wo.Late_Arrival_Time__c != null){
                dtConverted = wo.Late_Arrival_Time__c.format('MM/dd/yyyy h:mm a');
                wod.LateArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);
            }else{
                dtConverted = wo.Scheduled_Start_Date__c.AddMinutes(30).format('MM/dd/yyyy h:mm a');
                wod.LateArrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
                system.debug('--dtConverted--'+dtConverted);   
            }
            dtConverted = wo.Scheduled_Start_Date__c.format('MM/dd/yyyy h:mm a');
            wod.arrivalTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
            dtConverted = wo.Scheduled_End_Date__c.format('MM/dd/yyyy h:mm a');
            wod.serviceFinishTime = dtConverted.split(' ').get(1)+' '+dtConverted.split(' ').get(2);
            wod.orgId = userinfo.getOrganizationId();

            wdList.add(wod);
        }
        String str = JSON.serialize(wdList);
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        
       // req.setEndpoint('https://gtesdemo.dsmtracker.com/RouteOptimizer/api/ScheduleWorkOrder?type=json');
        req.setEndpoint(System_Config__c.getInstance().URL__c+'UpdateWorkOrder?type=json');
        req.setMethod('POST');
        
        //String body = str;
        String body = '{"workOrderDetail":'+str+',"orgId":'+'"'+Userinfo.getOrganizationId()+'"'+'}';
        system.debug('--str+str1--'+body);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        http = new Http();
        if(!Test.isRunningTest()){
            res = http.send(req);
            system.debug('---'+res.getBody());
        }
    }
    
    @future(Callout = true)
    public static void CancelWorkOrder(Set<Id> WoIds){
        
        for(Id woId : woIds){
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        req.setEndpoint(System_Config__c.getInstance().URL__c+'UpdateWorkOrderStatus?type=json');
        req.setMethod('POST');
        dsmtCallOut.CancelWorkOrderRequest cworder = new dsmtCallOut.CancelWorkOrderRequest();
        cworder.workOrderId = WoId;
        cworder.status = 'Cancelled';
        cworder.orgId = userinfo.getOrganizationId();
        String body = JSON.serialize(cworder);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        http = new Http();
        if(!Test.IsrunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
        }
        }
    }
    
    @future(Callout = true)
    public static void ReqReschedulingWorkOrder(Set<Id> WoIds){
        
        for(Id woId : woIds){
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = null;
        req.setEndpoint(System_Config__c.getInstance().URL__c+'UpdateWorkOrderStatus?type=json');
        req.setMethod('POST');
        dsmtCallOut.CancelWorkOrderRequest cworder = new dsmtCallOut.CancelWorkOrderRequest();
        cworder.workOrderId = WoId;
        cworder.status = 'Requires Rescheduling';
        cworder.orgId = userinfo.getOrganizationId();
        String body = JSON.serialize(cworder);
        req.setBody(body);
        req.setHeader('content-type', 'application/json');
        http = new Http();
        if(!Test.IsrunningTest()){
            res = http.send(req);
            System.debug(res.getBody());
        }
        }
    }

}