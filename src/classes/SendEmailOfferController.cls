public class SendEmailOfferController {
    public String emailSubject{set;get;}
    public String emailBody{set;get;}
    public String emailTo{set;get;}
    public String emailCC{set;get;}
    public String emailBCC{set;get;}
    public String RRId{set;get;}
    public String DCID{get;set;}
    public String SRID{get;set;}
    public Boolean isError{set;get;}
    public String P1dId{get;set;}
    public String email_template_type{get;set;}
    
    public String selTemplateId {
        get;
        set;
    }
    public string WhatId {
        get;
        set;
    }
    
    private String accId;
    public String mailType{get;set;}
    public string object_id {get;set;}
     public Attachment__c attach{get;set;}
    
    public Boolean enable_poller{get;set;}
    public Boolean show_print_collateral_record{get;set;}
    private String session_id{get;set;}
    private String server_url{get;set;}
    private String environment_name{get;set;}
    private String toEmail = '';
    private Boolean create_attachment_record{get;set;}
    public Boolean filltemplate{get;set;}
    private Map<String, String> key_prefix_map{get;set;}
    public string AttachmentPrefix;
    String templateName = null;
    public SendEmailOfferController(){
        isError = false;
        
        selFolderId = ApexPages.currentPage().getParameters().get('folderId');
        selTemplateId = ApexPages.currentPage().getParameters().get('TemplateID');
        /*RRId = Apexpages.currentPage().getParameters().get('rrid'); //Registration request id 
        DCID = Apexpages.currentPage().getParameters().get('dcid'); //dsmtracker Contact Id
        SRID = Apexpages.currentPage().getParameters().get('srid'); //service request id*/
        
        template_type = ApexPages.currentPage().getParameters().get('template_type');

        mailType = Apexpages.currentPage().getParameters().get('mailType');
        P1dId = Apexpages.currentPage().getParameters().get('p1dId');
        object_id = ApexPages.currentPage().getParameters().get('p1dId');
        AttachmentPrefix = ApexPages.currentPage().getParameters().get('mailType');
        listAttmod = new List < AttachmentModel > ();

        
        WhatId = emailTo = emailCC = '';
        
        
        
       /* if(RRId != null && RRId.trim().length() > 0){
           templateName = Email_Custom_Setting__c.getorgdefaults().Email_Template__c;
        }else if(DCID != null && DCID.trim().length() > 0){
           templateName = Email_Custom_Setting__c.getorgdefaults().DSMT_ES_Registration_Email_Template__c; 
        }
        else if(P1dId != null && P1dId.trim().length() > 0){
            templateName = Email_Custom_Setting__c.getorgdefaults().P1Detail_Template__c;
        }*/
        
        templateName = ApexPages.currentPage().getParameters().get('templateName');
        
        if(templateName != null){
            List<EmailTemplate> listTemp = [select Id from EmailTemplate where DeveloperName =: templateName];
            if(listTemp != null && listTemp.size() > 0){
               selTemplateId = listTemp.get(0).Id;
            }
        }
        
        if(selTemplateId == null){
           getAllEmailTemplateFolders();
        }
        
        String create = ApexPages.currentPage().getParameters().get('create');
        if(create  != null){
            create_attachment_record = Boolean.valueOf(create);
        }else{
            create_attachment_record  = true;
        }
        
         
      session_id = UserInfo.getSessionId();
      server_url = System.Label.PartnerURL;
      environment_name = UserInfo.getOrganizationId();  
      Map<String, Schema.SObjectType> global_describe = Schema.getGlobalDescribe();
        key_prefix_map = new Map<String, String>();
        Set<String> key_prefix_set = global_describe.keySet();
        for(String s_object : key_prefix_set){
            Schema.DescribeSobjectResult result = global_describe.get(s_object).getDescribe();
            String temp_name = result.getName();
            String temp_prefix = result.getKeyPrefix();
            key_prefix_map.put(temp_prefix, temp_name);   
    
        }
        attach  =new Attachment__c(); 
              String record_id = ApexPages.currentPage().getParameters().get('id'); 

        if(record_id != null){
      
        attach = [SELECT id,Attachment_Offer_Template__c,File_Url__c from Attachment__c WHERE Id = :record_id LIMIT 1];
        
        }

        fillAttachmentModel();

        
    }
    
    
    public String selFolderId{get;set;}
    public List<SelectOption> folderList{get;set;}
    public List<SelectOption> templateList{get;set;}
    
     /**
     * Get Email Templates By Folder to fill Folder dropdown 
     *
     * @param Nothing 
     * @return Nothing.
     */
    
    public void getAllEmailTemplateFolders(){
       folderList = new List<SelectOption>();
       templateList = new List<SelectOption>();
       
       Map<String,String> folderIdtoName = new Map<String,String>();
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true order by FolderId];
       
       for(EmailTemplate et : emailTemplatelist){
               folderIdtoName.put(et.folderId,et.Folder.Name);
       }
       system.debug('--folderIdtoName--'+folderIdtoName);
        folderList.add(new SelectOption('','--Select--'));
        templateList.add(new SelectOption('','--Select--'));
        for(String folderid : folderIdtoName.keySet()){
           system.debug('--folderid--'+folderid);
           system.debug('--folderIdtoName.get(folderid)--'+folderIdtoName.get(folderid));
           if(folderIdtoName.get(folderid) != null)
             folderList.add(new SelectOption(folderid, folderIdtoName.get(folderid)));
        }
    }
    
    
    public void getEmailTemplatesByfolder(){
       templateList = new List<SelectOption>();
       templateList.add(new SelectOption('','--Select--'));
       List<EmailTemplate> emailTemplatelist = [Select Id, Name, Subject, TemplateType, FolderId, Folder.Name From EmailTemplate Where IsActive = true and FolderId =: selFolderId order by Name];
       for(EmailTemplate et : emailTemplatelist){
          templateList.add(new SelectOption(et.Id, et.Name));
       }
    }
    
    public void init(){
        emailSubject = '';
        emailBody = '';
        emailTo = '';
        emailCC = '';
        emailBCC = '';
        accId = '';
        isError = false;
        
        filltemplate = True;
        
        if(SRID != null && SRID.length() > = 15){
            loadContentsForServiceRequest();
        }else if(DCID != null && DCID.length() > = 15){
            loadContentsForDsmTrackerContact();
        }else if(RRId != null && RRId.length() >= 15){
            loadContentForRegistrationRequest();
        }else if(P1dId != null && p1dId.length() >= 15){
            loadContentForp1d();
        }
                  
            //send dummy email to generate HTML body and Subject      
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            
            mail.setToAddresses(new String[] {'support+dnd@unitedcloudsolutions.com'});
            mail.setTemplateId(selTemplateId);
            System.debug('SELECTED TEMPLATE ID :: ' + selTemplateId);
                            
            List < Contact > lstContact = [select id, name, email from contact where Name = 'Do Not Delete'];
            Id conId = null;
            if(lstContact.size()>0){
                  conId = lstContact[0].id;
            }else{
               List<Account> lstAcc = [select id from Account where Name='UCS'];
               Id AccId = null;
               if(lstAcc.size()>0)
                     AccId = lstAcc[0].Id;
               else{
                     Account acc = new Account(Name='UCS');
                     insert acc;
                     AccId = acc.Id;
               }
                Contact c = new Contact(LastName='Do Not Delete',AccountId=AccId ,Email='support+dnd@unitedcloudsolutions.com');
                insert c;
                conId = c.Id;
             }
              mail.setTargetObjectId(conId );
              mail.setWhatId(WhatId);
              mail.setSaveAsActivity(false);
              mailList.add(mail);
                            
                try{
                    Savepoint sp = Database.setSavepoint();
                    if(!Test.isRunningTest()){
                        try{
                            Messaging.sendEmail(mailList,true);
                        }catch(Exception expp){
                        }
                    }
                    Database.rollback(sp);
                    if(mail.getHTMLBody() != null){
                            emailBody= mail.getHTMLBody();
                     }else{
                            emailBody = mail.getPlainTextBody();
                    }
                     emailSubject = mail.getSubject();
                 
                    system.debug('##### emailSubject = '+emailSubject);
                    system.debug('##### emailBody = '+emailBody);
                }catch(Exception exp){
                    isError = true;
                    Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,exp.getMessage()));
                }
            
        create_print_collateral_record();
    }
    
    public void loadContentForRegistrationRequest(){
           WhatId = RRId;
           List<Registration_Request__c> listRR = [select Id,First_Name__c,Last_Name__c,Email__c,Status__c from Registration_Request__c where id =: RRId];
           if(listRR != null && listRR.size() > 0){
                if(listRR[0].Email__c != null){
                        emailTo = listRR[0].Email__c;
                }else{
                    isError = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Address Not Found'));
                }
           }              
    }
    
    public void loadContentsForServiceRequest(){
        WhatId = SRID;
        List<Service_Request__c> listSR = [select Id,Name,DSMTracker_Contact__c,DSMTracker_Contact__r.First_Name__c,DSMTracker_Contact__r.Last_Name__c,DSMTracker_Contact__r.Email__c,DSMTracker_Contact__r.Status__c from Service_Request__c where id =: SRID];
        if(listSR != null && listSR.size() > 0){
            if(listSR[0].DSMTracker_Contact__r.Email__c != null){
                emailTo = listSR[0].DSMTracker_Contact__r.Email__c;
            }else{
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Address Not Found'));
            }   
        }
    }
    public void loadContentsForDsmTrackerContact(){
        WhatId = DCID;
        List<DSMTracker_Contact__c> listCT = [select Id,First_Name__c,Last_Name__c,Email__c,Status__c from DSMTracker_Contact__c where id =: DCID];
        if(listCT != null && listCT.size() > 0){
            if(listCT[0].Email__c != null){
                emailTo = listCT[0].Email__c;
            }else{
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Address Not Found'));
            }   
        }
    }
    
    public void loadContentForp1d(){
        WhatId = P1dId;
        List<Phase_I_Ticket_Detail__c> listPd = [select Id,Service_Request__r.Customer_Email__c from Phase_I_Ticket_Detail__c where id =: P1dId];
        if(listPd != null && listPd.size() > 0){
            if(listPd[0].Service_Request__r.Customer_Email__c!= null){
                emailTo = listPd[0].Service_Request__r.Customer_Email__c;
            }else{
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Address Not Found'));
            }   
        }
    }
    
    public Pagereference cancel(){
        if(WhatId != null)
            return new Pagereference('/'+WhatId);
        return null;
    }
    
    public Pagereference sendEmail(){
        system.debug('######## emailTo = '+emailTo);
        system.debug('######## emailCC = '+emailCC);
        system.debug('######## emailBCC = '+emailBCC);
        system.debug('--emailBody --'+emailBody );
        
        if(emailSubject == null || emailSubject.trim().length() == 0){
           isError = true;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email Subject is Required.'));
           return null;
        }
        
        if(emailBody == null || emailBody.trim().length() == 0){
           isError = true;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Email body is required.'));
           return null;
        }
        
        
        List<String> toAddress = new List<String>();
        if(emailTo != null && emailTo.trim().length() > 0){
            for(string s :emailTo.split(',')){
                if(s!=null && s!='')
                   toAddress.add(s); 
            }
            
        }
        
          boolean mvAttachFound = false;
            String attachmentUrl = '';
            for (AttachmentModel mod: listAttmod) {
                if (mod.isAttach) {
                    /* @author Hemanshu Patel - GTES
                    * @date 20/02/2017
                    *
                    * @group Ticket#-000655 
                    * @group-content https://na16.salesforce.com/a0vj0000004vrkz?srPos=0&srKp=a0v
                    *
                    * @description Added code for Initial M&V Specification Attachment type.
                    */
                    if(mod.attachment.Attachment_Type__c == 'Initial M&V Specification'){
                        mvAttachFound = true;
                    }
                    String newURL = '';
                     String oldURL =  mod.attachment.Email_File_Download_URL__c;
                     String[] questionsplit = oldURL.split('\\?');
                     String[] ampSplit = questionsplit[1].split('&');
                     newURL = questionsplit[0]+'?';
                     system.debug('--newURL--'+newURL);
                     system.debug('--ampSplit'+ampSplit);
                     for(String parameter : ampSplit){
                       String[] equalsplit = parameter.split('=');
                       newURL += equalsplit[0]+'='+EncodingUtil.urlEncode(equalsplit[1],'UTF-8')+'&'; 
                     }
                     newURL = newURL.subString(0,newURL.length() - 1);
                     system.debug('--newURL--'+newURL);
                
                    attachmentUrl += '<li><a target="_self" href="' + newURL  + '">' + mod.attachment.Attachment_Name__c + '</a></li>';
                    //attIds.add(mod.attachment.id);
                }
            }
            
        if(toAddress.size() > 0){
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setSaveAsActivity(false);
            email.setSubject(emailSubject);
            email.setHtmlBody(emailBody+attachmentUrl );
            email.setToAddresses(toAddress);
            if(emailCC != null && emailCC.trim().length() > 0){
                email.setCcAddresses(emailCC.split(','));
            }
            if(emailBCC != null && emailBCC.trim().length() > 0){
                email.setBccAddresses(emailBCC.split(','));
            }
            
            mailList.add(email);
            
            try{
                if(!Test.isRunningTest()){
                    try{
                        Messaging.sendEmail(mailList,true);
                    }catch(Exception expp){
                        system.debug('###### expp = '+expp);
                    }
                }
                
                string body = '';
                if(email.getHTMLBody()!=null){
                    body = email.getHTMLBody();
                    body = body.replace('<meta charset="utf-8">', '').replace('<br>', '<br/>');
                    body += '</body>';
                }
                
                Messages__c msg = new Messages__c();
                msg.From__c = userinfo.getUserEmail();
                msg.To__c = string.join(email.getToAddresses(),',');
                
                if(email.getCCAddresses()!=null)
                    msg.CC__c = string.join(email.getCCAddresses(),',');
                if(email.getBCCAddresses()!=null)
                    msg.BCC__c = string.join(email.getBCCAddresses(),',');
                
                msg.Subject__c = email.getSubject();
                msg.Message__c = body ;
                msg.Mesage_Text_Only__c = htmlToText(body );
                msg.Message_Type__c = 'Send Email';
                msg.Message_Direction__c  = 'Outbound';
                msg.Status__c = 'Sent';
                msg.Sent__c = Datetime.Now();
                system.debug('--from address--'+msg.From__c);
                
                //if Registration Request Id is not null
                if(RRId != null){
                    msg.Registration_Request__c = RRId;
                    
                    List<Registration_Request__c> listRR;
                    listRR = [select id,Status__c,DSMTracker_Contact__c,DSMTracker_Contact__r.Trade_Ally_Account__c from Registration_Request__c where id =: RRId];
                    if(listRR  != null && listRR.size() > 0){
                        Registration_Request__c RRObj = listRR[0];
                        
                        if(RRObj.DSMTracker_Contact__c!=null)
                            msg.DSMTracker_Contact__c = RRObj.DSMTracker_Contact__c;
                        if(RRObj.DSMTracker_Contact__r.Trade_Ally_Account__c !=null)
                            msg.Trade_Ally_Account__c = RRObj.DSMTracker_Contact__r.Trade_Ally_Account__c;
                        
                        if(mailType == 'Registration'){
                            RRObj.Status__c = 'Registration Request Sent';
                        }
                        update listRR;
                        
                        if(RRObj .DSMTracker_Contact__r.Trade_Ally_Account__c!=null){
                            List<Trade_Ally_Account__c> lstTAC = [select Id,Status__c,Stage__c from Trade_Ally_Account__c where Id = :RRObj.DSMTracker_Contact__r.Trade_Ally_Account__c AND Status__c='Prospecting' AND Stage__c = 'Prospecting' Limit 1];
                            if(lstTAC.size()>0){
                              if(mailType == 'Registration'){ 
                                lstTAC[0].Status__c = 'Registration Request Sent';
                              }  
                                update lstTAC;
                            }
                        }
                    }
                }
                
                // if dsmtracker contact id is not null    
                if(DCID != null && DCId.trim().length() > 0){
                    msg.DSMTracker_Contact__c = DCID;
                    List<DSMTracker_Contact__c> tcObjlist = [SELECT Id,Name,Registration_Request__c,Trade_Ally_Account__c FROM DSMTracker_Contact__c WHERE Id =:DCID];
                    if(tcObjlist.size() > 0){
                        DSMTracker_Contact__c tcObj = tcObjlist.get(0); 
                        if(tcObj.Registration_Request__c != null)
                            msg.Registration_Request__c = tcObj.Registration_Request__c;
                        if(tcObj.Trade_Ally_Account__c != null)
                            msg.Trade_Ally_Account__c = tcObj.Trade_Ally_Account__c;
                    }
                }
                
                //if service Request id is not null
                if(SRID != null && SRID.trim().length() > 0){
                   msg.Service_Request__c = SRID;
                   List<Service_Request__c> srlist = [select id,Dsmtracker_contact__c,Trade_Ally_Account__c from service_Request__c where id =: SRID];
                   if(srlist.size() > 0){
                       Service_Request__c srObj = srlist.get(0);
                       if(srObj.Dsmtracker_Contact__c != null){
                          msg.Dsmtracker_Contact__c = srObj.Dsmtracker_Contact__c;
                       }
                       if(srObj.Trade_Ally_Account__c != null){
                          msg.Trade_ally_Account__c = srObj.Trade_Ally_Account__c;
                       }
                       
                       if(mailType == 'MIR'){
                          srObj.Status__c = 'More Information Requested';
                          update srObj;
                       }
                   }
                }
                 
                if(p1dId != null && p1dId.trim().length() > 0){
                    msg.Phase_I_Ticket_Detail__c = p1dId;
                }
                system.debug('--msg--'+msg);     
                insert msg;
              
                createTasks();
               
                
               
            }catch(Exception exp){
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,exp.getMessage()));
            }
        }else{
           isError = true;
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'To Email Address is not found.'));
        }
        if(WhatId != null)
            return new Pagereference('/'+WhatId);
        return null;
    }
    
  
    
    private void createTasks(){
        List<Task>lstTask = new List<Task>();
        Task tsk = new Task(WhatId = WhatId,Subject=emailSubject);
        tsk.Description = emailBody;
        tsk.Priority = 'High';
        tsk.Status = 'Completed';
        tsk.ActivityDate = System.today();
        lstTask.add(tsk);
        
        
        if(lstTask.size() > 0){
            insert lstTask;
        }
    }
    
       String template_type = 'EFR';
       public PageReference create_print_collateral_record() {
       // try{
            
            if(create_attachment_record){
                Attachment__c at_record = new Attachment__c();
                at_record.Status__c = 'New';
                at_record.attachment_type__c = 'Offer Letter';
                at_record.Attachment_Offer_Template__c = template_type;
                String object_prefix = object_id.substring(0, 3);
                //String field_name = key_prefix_map.get(object_prefix).replaceAll('TREES__', '');
                String field_name = key_prefix_map.get(object_prefix);
                at_record.put(field_name, object_id);
                at_record.Category__c = 'PRINT_EBR_EFR';
                at_record.Attachment_Prefix__c = AttachmentPrefix;
                insert at_record;
                
                attach = at_record;
    
                enable_poller = true;
                system.debug('callout' + session_id + server_url + attach.Id + object_id + environment_name);
                perform_callout(session_id, server_url, attach.Id, object_id, environment_name);
                
            }
            if(filltemplate){
              fillHtmlBody();
            }
       /* }catch(Exception e){
            //Apexpages.addMessage(addMessage(e.getMessage()));
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,e.getMessage()));
        }*/
        return null;
    }
    
    @future(callout=true)
    private static void perform_callout(String session_id, String server_url, String print_collateral_id, String object_id, String environment_name){
        if(!Test.IsRunningTest()){
            Greendocs.TreesGreenDocWSSoap pc_webservice = new Greendocs.TreesGreenDocWSSoap ();
            pc_webservice.timeout_x = 120000;
            
            system.debug('PC RECORD ID: ' + print_collateral_id);
            system.debug('OBJECT ID: ' + object_id);
            system.debug('SESSION ID: ' + session_id);
            system.debug('SERVER URL: ' + server_url);
            system.debug('ENVIRONMENT NAME: ' + environment_name);
            //server_url = 'https://cs30.salesforce.com/services/Soap/u/25.0';
            system.debug('PC WEBSERVICE RESULT: ' + pc_webservice.GenerateGreenDoc(session_id, server_url, print_collateral_id, object_id, environment_name, True));
        }
    } 
    
     public PageReference check_print_collateral_status() {
        System.Debug('***** function');
        List<Attachment__c> list_pc_records1 = [SELECT Id, Status__c FROM Attachment__c WHERE Id = :attach.Id LIMIT 1];
        List<Attachment__c> list_pc_records = [SELECT Id, Status__c FROM Attachment__c WHERE Id = :attach.Id AND Status__c = 'Completed' LIMIT 1];
       
        if(list_pc_records.size() > 0){
            enable_poller = false;
            //fillHtmlBody();
          //  System.Debug('***** RemoveSideTrue'+RemoveSideTrue);
            PageReference p = new PageReference('/apex/SendEmailOfferLetter?id=' + attach.Id + '&show=true' + '&object_id=' + object_id + '&template_type=' + template_type +'&p1dId='+p1dId+'&email_template_type='+email_template_type+'&create=false&templateName='+templateName+'&mailType='+mailType);
            p.setRedirect(true);
            return p;
        }
         
        return null;
    }
    
     public void fillAttachmentModel() {
        listAttmod = new List < AttachmentModel > ();
        
        Set<Id> mvAttId = new Set<Id>();
        integer count = 0;
        for (Attachment__c att : [select id, Name, Attachment_Name__c, Attachment_Type__c, File_Download_Url__c, File_Url__c, CreatedDate, CreatedBy.Name,Email_File_Download_URL__c  from Attachment__c
            WHERE Phase_I_Ticket_Detail__c = : p1dId AND Attachment_Type__c Includes('Offer Letter','Initial M&V Specification') order by CreatedDate DESC
        ]) {
        
            AttachmentModel mod = new AttachmentModel();
            mod.Attachment = att;
            if(att.id == attach.id)
                mod.isAttach = true;
            
            /* @author Hemanshu Patel - GTES
            * @date 20/02/2017
            *
            * @group Ticket#-000655 
            * @group-content https://na16.salesforce.com/a0vj0000004vrkz?srPos=0&srKp=a0v
            *
            * @description Added code for Initial M&V Specification Attachment Type.
            */
            if(att.Attachment_Type__c == 'Initial M&V Specification' && count == 0){
                mod.isAttach = true;
                count++;
            }
            listAttmod.add(mod);
        }
    }
    
    private static String htmlToText(String htmlString) {
        String RegEx = '(</{0,1}[^>]+>)';
        if (htmlString == null)
            htmlString = '';
        return htmlString.replaceAll('&nbsp', '').replaceAll(';', '').replaceAll(RegEx, '').replaceAll('&#39','\'');
    } 
    
    public class AttachmentModel{
        public String name{set;get;}
        public String type{set;get;}
        public Date createdDate{set;get;}
        public String createdBy{set;get;}
        public String createdById{set;get;}
        
        public Boolean isAttach {
            set;
            get;
        }
        
        public Attachment__c attachment {
            set;
            get;
        }
        
        public AttachmentModel(){
            name = '';
            type = '';
            createdBy = '';
            createdById = '';
              attachment = new Attachment__c();
            isAttach = false;
        }
    }
    
    /**
     * Reload page based on Program
     *
     * @param Nothing 
     * @return Pagereference .
     */
   
    public PageReference reloadPage(){
       PageReference pg = null;
       //if(ea.Program_Name__c =='Smart $aver Custom' || ea.Program_Name__c =='MSD Custom'){
           pg = Page.SendEmail;
       //}else{
       //    pg = Page.dsmtNewEmail;
       //}
       pg.getParameters().put('id', RRId);
       pg.getParameters().put('TemplateID', selTemplateId);
       //pg.getParameters().put('sendEmail', String.valueOf(isSendEmail));
       pg.getParameters().put('folderId',selFolderId);
       //pg.getParameters().put('MIR','true');
       //pg.getParameters().put('Pagename','sendemail');
       pg.setRedirect(true);
       return pg;
    }
    
    public PageReference regenerateOfferLetter(){
        create_attachment_record = true;
        create_print_collateral_record();
        return null;
    }
    
    public PageReference fillHtmlBody() {
    
        selTemplateId = ApexPages.Currentpage().getParameters().get('TemplateID');
        
        if((email_template_type != null && email_template_type.trim().length()>0) || (selTemplateId != null && selTemplateId != '')){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{'rajendra.rathore121@gmail.com'};
            mail.setToAddresses(toAddresses);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(true);
            mail.setSenderDisplayName('MMPT');
            mail.setWhatId(p1dId);
            List<Contact>lstContact = [select id,name,email from contact where Name ='Do Not Delete'];
            List<EmailTemplate> emailTemp=[select TemplateType,Id from EmailTemplate where name=:email_template_type limit 1];
            
            
            
            if(selTemplateId != null && selTemplateId != ''){
                emailTemp=[select TemplateType,Id from EmailTemplate where Id =: selTemplateId  limit 1];
            }
            
            system.debug('--emailTemp---'+emailTemp);
            mail.setTargetObjectId(lstContact[0].id);
            mail.setTemplateId(emailTemp[0].id);

            Savepoint sp = Database.setSavepoint();
            
            try{
                if(!Test.isRunningTest())
                 Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                  
                    if(emailTemp!=null && emailTemp.size()>0){
                    if(emailTemp[0].TemplateType.equalsIgnoreCase('text')){
                       emailBody =mail.getPlainTextBody();
                    }else{
                     emailBody = mail.getHtmlBody();
                     emailBody = emailBody.replace('<meta charset="utf-8">','').replace('<br>','<br/>');
                     emailBody += '</body>';
                    }
                    
                }
                emailsubject= mail.getSubject();
                Database.rollback(sp);
            }Catch(Exception e){
              //  addMessage(e.getMessage());
                Database.rollback(sp);
            }
            
        }
        return null;
    }
    
      public List < AttachmentModel > listAttmod {
        set;
        get;
    }
   
}