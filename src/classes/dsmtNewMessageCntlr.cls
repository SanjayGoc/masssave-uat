global class dsmtNewMessageCntlr{
    
    public string msgId{get;set;}
    public Messages__c objMsg{get;set;}
    public string body{get;set;}
    public boolean isNew{get;set;}
    public string msgDir{get;set;}
    public boolean isMsgRead{get;set;}
    public String RRNumber{get;set;}
    public Registration_Request__c RRObj{get;set;}
    public Dsmtracker_Contact__c dsmtCon{get;set;}
    public Trade_Ally_Account__c ta{get;set;}
    public boolean isPortal{get;set;}
    public String headerPageName{get;set;}
    
    public String PortalURL{
        get {
            return Dsmt_Salesforce_Base_Url__c.getOrgDefaults().Base_Portal_Attachment_URL__c;
        }
    }
    public String orgId {
        get {
            return UserInfo.getOrganizationId().substring(0,15);
        }
    }
    
    public dsmtNewMessageCntlr(){
        
       isPortal = true;
           headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';
           List<User> lstUser = [select Id,IsPortalEnabled,Username from User where Id = :Userinfo.getUserId() limit 1];
           if(lstUser.size()>0){
               if(lstUser[0].IsPortalEnabled){
                   headerPageName = 'dsmtTradeallyHomePageHeaderTemplate';        
               }
               else{
                   headerPageName = 'dsmtConsoleTradeallyHomePageHeader';
                   isPortal = false;
               }
           } 
       isNew = true;
       isMsgRead= false;
       msgId = ApexPages.currentPage().getParameters().get('id');
       System.debug('--ABC-- '+msgId);
       RRNumber = ApexPages.currentPage().getParameters().get('RRId');
       msgDir = ApexPages.currentPage().getParameters().get('msgFilter');
       string msgRead = ApexPages.currentPage().getParameters().get('msgRead');
       
      
       if(msgRead == 'true'){
           isMsgRead = true;
       }
       
       loadContactAndTA();
       init();
       
       if(RRNumber != null && RRNumber != ''){
          loadRegistrationRequest();
       }
    }
    
    public PageReference checkMessage(){
        if(isMsgRead == true){
            objMsg.Opened__c = System.now();
            update objMsg;
        }
        return null;
    }
    
    public void init(){
        if(msgId!=null){
           List<Messages__c> lstMsgs = [
               select 
                    Id,Message_Date__c,Sent__c,From__c,CC__c,BCC__c,To__c,Offer_Letter_Message_Title__c,Name,Subject__c,Deleted__c,Message__c,Accepted_Rejected__c,Message_Type__c,Opened__c,Archive__c,Message_Direction__c 
               from Messages__c 
               where Id = :msgId Limit 1
           ];
           if(lstMsgs.size()>0){
               objMsg = lstMsgs[0];
               body = objMsg.Message__c!=null ? objMsg.Message__c: '' ;
               isNew = false;
           }
       }else{
           objMsg = new Messages__c(); 
           objMsg.From__c = userinfo.getuserEmail();
           objMsg.Message_Direction__c = 'Inbound';
           objMsg.To__c = ta.Owner.Email;
           objMsg.To_Email__c = ta.Owner.Email;
           objMsg.Trade_Ally_Account__c = ta.id;
           objMsg.Dsmtracker_Contact__c = dsmtCon.Id;
           objMsg.Message_Type__c = 'Notification';
       }
       
    }
    
    public String conId{get;set;}
    public void loadContactAndTA(){
        String usrid = UserInfo.getUserId();
        List<User> usrList  = [select Id,IsPortalEnabled,ContactId,Contact.AccountId,Contact.Account.Name,Contact.Account.RecordType.Name,Contact.Name from user Where Id =: usrid];
        
        if(usrList != null && usrList.size() > 0){
            if(usrList[0].IsPortalEnabled){
                 if(usrList.get(0).ContactId != null){
                    conId   = usrList.get(0).ContactId;
                    List<DSMTracker_Contact__c> dsmtList = [select id,Trade_Ally_Account__c from DSMTracker_Contact__c where Contact__c =: conId limit 1];
                    if(dsmtList != null && dsmtList.size() > 0){
                        dsmtCon = dsmtList.get(0);
                        List<Trade_Ally_Account__c> taList = [select id,Owner.Email from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                        if(taList != null && taList.size() > 0){
                            ta = taList.get(0);
                        }
                    }
                }
            }else{
            List<DSMTracker_Contact__c> dsmtList = [select id,Contact__c,Trade_Ally_Account__c from DSMTracker_Contact__c where Portal_User__c =:usrList[0].id limit 1];
                if(dsmtList != null && dsmtList.size() > 0){
                    dsmtCon = dsmtList.get(0);
                    conId = dsmtList.get(0).Contact__c;
                    List<Trade_Ally_Account__c> taList = [select id,Owner.Email from Trade_Ally_Account__c where Id =: dsmtList.get(0).Trade_Ally_Account__c];
                    if(taList != null && taList.size() > 0){
                        ta = taList.get(0);
                    }
                }
        }
      }  
    }
    
    public void loadRegistrationRequest(){
       List<Registration_Request__c> lstRR = [select Id,Name,First_Name__c,Last_Name__c,Email__c,Phone__c,DSMTracker_Contact__c,Registration_Number__c,Trade_Ally_Account__c,Trade_Ally_Account__r.Name,Trade_Ally_Account__r.Account__c,Trade_Ally_Account__r.Trade_Ally_Type__c,Steps_Completed__c,Trade_Ally_Type__c,
                                                 Street_Address__c,Street_City__c,Street_State__c,Street_Zip__c,Mailing_Address__c,Mailing_City__c,Mailing_State__c,Mailing_Zip__c,Operational_First_Name__c,Operational_Last_Name__c,
                                                 Operational_Phone__c,Operational_Email__c,Legal_First_Name__c,Legal_Last_Name__c,Legal_Phone__c,Legal_Email__c,Online_Profile_Company_Name__c,Online_Profile_City__c,
                                                 Online_Profile_Company_Website__c,Proficiences__c,Counties__c,Program_Sponsors__c,Sub_Counties__c,Show_Splash__c,Status__c from Registration_Request__c where Registration_Number__c= :RRNumber];
         if(lstRR.size()>0){
             RRObj = lstRR[0];
             if(msgId == null){
                 objMsg.Registration_Request__c = RRObj.Id;
                 objMsg.Subject__c = 'Reg : '+RRNumber;
             }
         }
    }
    
  
    
    public PageReference CancelAction(){
        PageReference pg = null;
        if(RRNumber != null){
            pg = Page.dsmtTradeAllyRegistrationProcess;
            pg.getparameters().put('id',RRObj.Registration_Number__c);
            pg.setRedirect(true);
            
        }
        else{
            pg = Page.dsmtMessages;
            //pg.getparameters().put('id',msgId);
            pg.setRedirect(true);
        }
        return pg;
    }
    
    public void SaveAsDraft(){
        if(!isMsgRead){
            objMsg.status__c = 'Draft';
            
            system.debug('@@body@@'+body);
            
            if(body!=null){
                objMsg.Message__c = body;
            }
            upsert objMsg;
        }
        msgId = objMsg.Id;
    }
    
    public void saveAndSubmit(){
            objMsg.status__c = 'Sent';
            
            objMsg.Message_Direction__c = 'Outbound';
            
            
            system.debug('@@body@@'+body);
            
            if(body!=null){
                objMsg.Message__c = body;
            }
            upsert objMsg;
            msgId = objMsg.Id;
            
            String templateId = null;
            List < EmailTemplate > emailTemp = [select TemplateType, Id from EmailTemplate where DeveloperName = 'New_Message_Created_From_Portal' limit 1];
            if (emailTemp != null && emailTemp.size() > 0) {
                 templateId = emailTemp.get(0).Id;
            }
            
             //send dumy mail for getting body of template
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {
                'support+pradip@unitecloudsolutions.com'
            };
            mail.setToAddresses(toAddresses);
            mail.setSaveAsActivity(false);
             mail.setTargetObjectId(conId);
            mail.setTemplateId(templateId);
            mail.setWhatId(objMsg.Id);
            Savepoint sp = Database.setSavepoint();
            
            if(!Test.isRunningTest()){
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            }
        
            body = mail.getHtmlBody();
            if(body == null){
               body = '';
            }
            body = body.replace('<meta charset="utf-8">', '').replace('<br>', '<br/>');
            body += '</body>';
            Database.rollback(sp);
            
            
            String attachmentUrl = '';
            list<Attachment__c> listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,Email_File_Download_URL__c,CreatedDate   from Attachment__c where  Message__c = :objMsg.Id order by CreatedDate desc];
            for (Attachment__c attach: listAtts) {
                 String newURL = '';
                 String oldURL =  attach.Email_File_Download_URL__c;
                 String[] questionsplit = oldURL.split('\\?');
                 String[] ampSplit = questionsplit[1].split('&');
                 newURL = questionsplit[0]+'?';
                 system.debug('--newURL--'+newURL);
                 system.debug('--ampSplit'+ampSplit);
                 for(String parameter : ampSplit){
                   String[] equalsplit = parameter.split('=');
                   if(equalsplit.size() > 1)
                   newURL += equalsplit[0]+'='+EncodingUtil.urlEncode(equalsplit[1],'UTF-8')+'&'; 
                 }
                 newURL = newURL.subString(0,newURL.length() - 1);
                 
                 system.debug('--newURL--'+newURL);
                attachmentUrl += '<li><a target="_self" href="' +newURL+ '">' + attach.Attachment_Name__c + '</a></li>';
            }
            
             if (attachmentUrl != '') {
                attachmentUrl = '<br/><br/><b>Download Attachments Here:</b><br/>' + attachmentUrl;
            }
            
            system.debug('--attachmentUrl--'+attachmentUrl );
            
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            List<Messaging.SingleEmailMessage> emaillist = new List<Messaging.SingleEmailMessage>();
             email.setSubject(objMsg.Subject__c);
            email.setTargetObjectId(conId);
            email.setSaveAsActivity(false);
            
            List<String> toaddressList = new List<String>();
            toaddressList.add(objMsg.To__c);
            email.setToAddresses(toaddressList);
            email.setSenderDisplayName(objMsg.From__c);
            email.setTreatTargetObjectAsRecipient(false);
            email.setHtmlBody(body + attachmentUrl);
              system.debug('--email--'+email);
            emaillist.add(email);
            if(!Test.isRunningTest()){
                List<Messaging.SendEmailResult> sendEmailResults = Messaging.sendEmail(emaillist);
            }
    }
    
    @RemoteAction
    global static String getAttachmentsByTicketId(String pid) {
            
            list<Attachment__c> listAtts;
            if(pid!=null && pid!=''){
                //Integer count = [Select COUNT() from Attachment__c where Attachment_Type__c = 'OfferLetter' and CreatedBy =: uid and Messages__c = :pid];
                Integer count = [Select COUNT() from Attachment__c where Message__c = :pid];
                if(count > 0){
                  listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Message__c = :pid order by CreatedDate desc];
                } 
            }else{
                system.debug('--listAtts--');
                return null;
            }
            system.debug('--listAtts--'+JSON.serializePretty(listAtts));
            return JSON.serializePretty(listAtts);
            
          // return count;
            //return listAtts      
     }
     @RemoteAction
     global static String  delAttachmentsById(String aid, String tid) {
                
            Attachment__c a = [select id from Attachment__c where id=:aid];
            delete a;
            list<Attachment__c> listAtts;
            Integer count = [Select COUNT() from Attachment__c where  Message__c = :tid];
            if(count > 0){
              listAtts = [Select Id,Name,File_Url__c,Attachment_Type__c,Status__c,Attachment_Name__c,File_Download_Url__c,CreatedDate   from Attachment__c where  Message__c = :tid order by CreatedDate desc];
            } 
            return JSON.serializePretty(listAtts);
            //return listAtts      
      }
}