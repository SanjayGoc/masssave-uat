global class dsmtUpdateProjectsStatusController {

    global Recommendation_Scenario__c project {
        get;
        set;
    }

    private string projectIds;

    global dsmtUpdateProjectsStatusController() {
        init();
    }

    private void init() {
        projectIds = ApexPages.currentPage().getParameters().get('ids');
        project = new Recommendation_Scenario__c();
    }

    private list < Recommendation_Scenario__c > queryProject() {
        list < Recommendation_Scenario__c > projectList = [select id, Status__c
            from Recommendation_Scenario__c
            where id in: projectIds.split(',')
        ];

        if (projectList.size() > 0) {
            for (Recommendation_Scenario__c p: projectList) {
                p.Status__c = project.Status__c;
            }
        }

        return projectList;
    }

    global PageReference saveAppt() {
        list < Recommendation_Scenario__c > projectList = queryProject();

        if (projectList.size() > 0) {
            update projectList;
        }

        return null;
    }

    webservice static String saveAppt(String projectIds, String status) {
        list < Recommendation_Scenario__c > projectList = [select id, Status__c
            from Recommendation_Scenario__c
            where id in: projectIds.split(',')
        ];

        if (projectList.size() > 0) {
            for (Recommendation_Scenario__c p: projectList) {
                p.Status__c = status;
            }

            update projectList;
        }

        return 'Success';
    }

    webservice static String saveSchedule(String projectIds, String dateStr, String endDateStr) {
        
        system.debug('--projectIds--'+projectIds);
        system.debug('--dateStr--'+dateStr);
        system.debug('--endDateStr--'+endDateStr);
        
        // --dateStr--01/13/2018
        // -dateStr--01/15/2018 4:42 PM
        
        if(dateStr != null && endDateStr != null){
            
            List<String> dateLst = dateStr.split(' ').get(0).split('/');
            String timeStr = dateStr.split(' ').get(1);
            
            List<String> timeStrLst = timeStr.split(' ');
            Integer hour = integer.valueOf(timeStrLst.get(0).split(':').get(0));
            Integer Min = integer.valueOf(timeStrLst.get(0).split(':').get(1));
            
            system.debug('--timeStrLst ---'+timeStrLst);
            
            if(dateStr.split(' ').get(2) == 'PM' && hour != 12){
                hour += 12;
            }
            
            Time myTime = Time.newInstance(hour, min, 0, 0);
            Date myDate = Date.newInstance(integer.valueOf(dateLst.get(2)), integer.valueOf(dateLst.get(0)), integer.valueOf(dateLst.get(1)));
            DateTime startdt = DateTime.newInstance(myDate, myTime);
            
            
            List<string> dateLstEnd = endDateStr.split(' ').get(0).split('/');
            //List<String> dateLstEnd = endDateStr.split('/');
            Date myEndDate = Date.newInstance(integer.valueOf(dateLstEnd.get(2)), integer.valueOf(dateLstEnd.get(0)), integer.valueOf(dateLstEnd.get(1)));
            
            timeStr = endDateStr.split(' ').get(1);
            
            timeStrLst = timeStr.split(' ');
            hour = integer.valueOf(timeStrLst.get(0).split(':').get(0));
            Min = integer.valueOf(timeStrLst.get(0).split(':').get(1));
            
            if(endDateStr.split(' ').get(2) == 'PM' && hour != 12){
                hour += 12;
            }
            
            myTime = Time.newInstance(hour, min, 0, 0);
            
            DateTime Enddt = DateTime.newInstance(myEndDate, myTime);
            
            list < Recommendation_Scenario__c > projectList = [select id,Name, Status__c,Start_Date__c
            from Recommendation_Scenario__c where id in: projectIds.split(',')];
            
            List<appointment__c> lstApp = new List<Appointment__c>();
            
                    if (projectList.size() > 0) {
                        for (Recommendation_Scenario__c p: projectList) {
                            p.Status__c = 'Scheduled';
                            //p.Start_Date__c = myDate;
                            p.Start_Date__c = startdt ;
                            p.End_Date__c = Enddt ;
                            
                            Appointment__c app = new Appointment__c();
                            app.Title__c = 'Appointment for '+p.Name;
                            app.Appointment_Start_Time__c = startdt;
                            app.Appointment_End_Time__c = Enddt ;
                            app.Project__c = p.Id;
                            lstApp.add(app);
                        }
                        
                        update projectList;
                        
                        if(lstApp.size()>0)
                            insert lstApp;
                    }
        }

        
        /*list < Recommendation_Scenario__c > projectList = [select id, Status__c
            from Recommendation_Scenario__c
            where id in: projectIds.split(',')
        ];

        if (projectList.size() > 0) {
            for (Recommendation_Scenario__c p: projectList) {
                p.Status__c = status;
            }

            update projectList;
        }*/

        return 'Success';
    }
    
    webservice static String CreateReview(String projectIds) {
        List<Exception__c> lstExcep = [select Id,Exception_Template__r.Reference_ID__c from Exception__c where Project__c In :projectIds.split(',') AND (Exception_Template__r.Reference_ID__c = 'Project-001' OR Exception_Template__r.Reference_ID__c = 'Project-003' OR Exception_Template__r.Reference_ID__c = 'Project-005' OR Exception_Template__r.Reference_ID__c = 'Project-006')];
        
        if(lstExcep.size()>0){
            return 'Exceptions must be resolved in order to create Pre Work Review';
        }
        else{
             List<Recommendation_Scenario__c> projList = [select id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__c,
                                                            Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c,
                                                            Energy_Assessment__r.Customer__r.First_Name__c,Energy_Assessment__r.Customer__r.Last_Name__c
                                                            ,Address__c,City__c,State__c,Zip__c,Phone__c,Email__c from Recommendation_Scenario__c where id in: projectIds.split(',')];
            
            Recommendation_Scenario__c Proj = projList.get(0);
            Id preRevRecordTypeId = Schema.SObjectType.Review__c.getRecordTypeInfosByName().get('Pre Work Review').getRecordTypeId();
            Review__c rev = new Review__c();
            //rev.Project__c = projList.get(0).Id;
            rev.Type__c = 'Pre Work Review';
            rev.Review_Type__c = 'Pre Work Review';
            rev.Requested_By__c = userinfo.getUSerId();
            rev.RecordTypeId = preRevRecordTypeId ;
            rev.Status__c = 'Pending Review';
            rev.Address__c = Proj.Address__c;
            rev.City__c = Proj.City__c ;
            rev.Zipcode__c = Proj.Zip__c;
            rev.State__c = Proj.State__c;
            rev.Phone__c = proj.Phone__c;
            rev.Email__c = Proj.Email__c;
            rev.Trade_Ally_Account__c = proj.Energy_Assessment__r.Trade_Ally_Account__c;
            rev.DSMTracker_Contact__c= proj.Energy_Assessment__r.Energy_Advisor__r.DSMTracker_Contact__c;
            rev.First_Name__c = proj.Energy_Assessment__r.Customer__r.First_Name__c;
            rev.Last_Name__c = proj.Energy_Assessment__r.Customer__r.Last_Name__c;
            insert rev;
            
            List<Project_Review__c> prList = new List<Project_Review__c>();
            Project_Review__c pr = null;
            
            for(String str : projectIds.split(',')){
                pr = new Project_Review__c();
                pr.Project__c = str;
                pr.Review__c = rev.Id;
                prList.add(pr);
            }
            
            if(prList.size() > 0){
                insert prList;
            }
            
            return 'Success';
        }
    }
}