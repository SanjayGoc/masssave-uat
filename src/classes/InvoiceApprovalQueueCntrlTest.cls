@isTest
Public class InvoiceApprovalQueueCntrlTest{

    static testmethod void runtest(){
    
    Review__c revObj = new Review__c();
    insert RevObj;
    
    Payment__c pay = new Payment__c();
    insert pay;
    
    Invoice__c inv = new Invoice__c();
    insert inv;
    
    Invoice_Line_Item__c ilt = new Invoice_Line_Item__c();
    ilt.Invoice__c = inv.id;
    ilt.Review__c = RevObj.Id;
    insert ilt; 
    
    Exception__c ex = new Exception__c();
    insert ex;
    
    ApexPages.currentPage().getParameters().put('payIds', pay.Id);
    ApexPages.currentPage().getParameters().put('voidcomment', 'Test');
    
      
    InvoiceApprovalQueueCntrl cntrl = new InvoiceApprovalQueueCntrl();
    //cntrl.rejectionReason();
    //cntrl.paymentFieldsNeeded();
    //cntrl.getNeededPaymentFields();
    //cntrl.queryInvoice();
    cntrl.buildPaymentWrapperList();
    cntrl.set_EnrlApps_stage();
    cntrl.checkBatchStatus();
    cntrl.applyPayments();
  /*  PageReference pageRef = Page.applyPayments;
    Test.setCurrentPage(pageRef);
  
    pageRef.getParameters().put('payids',applyPayments.id);
*/

    //cntrl.processInsMap();
    cntrl.voidPayments();
    
    //List<Invoice_Line_Item__c> listinv = InvoiceApprovalQueueCntrl.queryInvoice();
    Map<id, ProcessInstance> mappro = InvoiceApprovalQueueCntrl.processInsMap();
    
    }
    
}