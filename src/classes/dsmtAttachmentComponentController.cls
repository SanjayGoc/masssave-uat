public class dsmtAttachmentComponentController {
    public Id parentId{get;set;}
    /*
    public List<Attachment> getAttachments(){
        if(this.parentId != null && this.parentId != null){
            return [select id, Name, CreatedDate from Attachment where ParentId =: parentId ORDER BY CreatedDate DESC NULLS FIRST LIMIT 50];  
        }else{
            return new List<Attachment>();   
        }
    }
    */
    public void deleteAttachment(){
        String attachmentId = getParam('attachmentId');
        if(attachmentId != null)
            delete new Attachment(Id = attachmentId);
    }
    
    public String getParam(String paramKey){
        return ApexPages.currentPage().getParameters().get(paramKey);
    }
    
    @RemoteAction 
    public static List<Attachment> queryAttachments(Id parentId){
        if(parentId != null && parentId != null){
            return [select Id, Name, ContentType, CreatedDate from Attachment where ParentId =: parentId ORDER BY CreatedDate DESC NULLS FIRST];  
        }else{
            return new List<Attachment>();   
        }
    }
}