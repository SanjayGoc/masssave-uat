@isTest
public class dsmtTradeAllyHeaderControllerTest 
{
    @isTest
    Public Static Void RunTest()
    {
        
        //Test.Starttest();
        
        User u = Datagenerator.CreatePortalUser();
        
        system.runAs(u) // normally you want to verify the list has data first as this could cause null exception
        {
            Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
            ta.Name = 'test';
            ta.Internal_Account__c=true;
            ta.Trade_Ally_Type__c = 'IIC';
            ta.name='test anme';
            ta.Street_Address__c='Test Add';
            ta.Street_City__c='Atlanta';
            ta.Street_State__c='MA';
            ta.Street_Zip__c='12345';
            ta.Website__c='www.google.com';
            ta.Phone__c='96385274120';
            ta.Email__c='test@gmail.com';
            ta.Registration_Renewal_Date__c=date.today();
            insert ta;   
            
            DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
            dsmtc.Name='test';
            dsmtc.First_Name__c='Test FN';
            dsmtc.Last_Name__c='Test LN';
            dsmtc.Email__c='test@pliant.com';
            dsmtc.Phone__c='9638527410';
            dsmtc.Title__c='COO';
            dsmtc.Super_User__c=true;
            dsmtc.Trade_Ally_Account__c=ta.id;
            dsmtc.contact__c=u.contactid;
            
            insert dsmtc;
            
            dsmtTradeAllyHeaderController dsmtHeader = new dsmtTradeAllyHeaderController();
        }
        
        //Test.Stoptest();
        
    }
}