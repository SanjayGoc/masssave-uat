Public class dsmtcreateprojectreviewcntrl{
    Public String dataSource {get; set;}
    public String selectedIds{get;set;}
    String RevId = '';
    public String ReviewName{get;set;}
    
    Public dsmtcreateprojectreviewcntrl() {
        dataSource = '';
        List<ProjectWrapperClass> lstWrapperClass = new List<ProjectWrapperClass>();
        RevId = ApexPages.currentPage().getParameters().get('RevId');
        
        if(RevId != null && RevId != ''){
            List<Review__c> revList = [select id,Pre_Work_Review__c,Name from Review__c where id =: revId];
            
            if(revList != null && revList.size() > 0 && revList.get(0).Pre_Work_Review__c != null){
                ReviewName = revList.get(0).Name;
                List<Project_Review__c> prList = [select id,Review__c,Project__c from Project_Review__c where Review__c =: revList.get(0).Pre_Work_Review__c];
                
                if(prList != null && prList.size() > 0){
                    Set<Id> projId = new Set<Id>();
                    
                    for(Project_Review__c pr : prList){
                        projId.add(pr.Project__c);
                    }
                    for(Recommendation_Scenario__c proj  : [select Id,Name,type__c,Energy_Assessment__r.Name,Address__c,City__c,Zip__c 
                                                                from Recommendation_Scenario__c where id in : projId]) {
                        ProjectWrapperClass wrapperobj = new ProjectWrapperClass();
                        wrapperobj.Id = proj.Id;
                        wrapperobj.Name = proj.Name;
                        wrapperobj.Type = proj.type__c;
                        wrapperobj.EA = proj.Energy_Assessment__r.Name;
                        wrapperobj.address = proj.Address__c;
                        wrapperobj.city = proj.City__c;
                        wrapperobj.zip = proj.Zip__c;
                        lstWrapperClass.add(wrapperobj);
                    }
                    dataSource = JSON.serialize(lstWrapperClass);
                }
            }
        }
    }
    
    public Pagereference doAddAction(){
        selectedIds = getParam('selectedIds');
        
        List<String> selectedIdsList = selectedIds.split(',');
        System.debug('SELECTED IDS : ' + selectedIdsList);
        List<Project_Review__c> prList = new List<Project_Review__c>();
        Project_Review__c pr = null;
        
        for(String str : selectedIdsList){
            pr = new Project_Review__c();
            pr.Review__c = revId;
            pr.Project__c = str;
            prList.add(pr);
        }
        
        if(prList.size() > 0){
            insert prList;
        }
        
        return new Pagereference('/'+revId);
        
    }
    public String getParam(String key){
        return ApexPages.currentPage().getParameters().get(key);
    }
 
    Public class ProjectWrapperClass {
        public String Id;
        Public String Name;
        Public String Type;
        Public String EA;
        public String address;
        public String city;
        public String zip;
        
    }
}