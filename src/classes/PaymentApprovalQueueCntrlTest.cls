@isTest
Public class PaymentApprovalQueueCntrlTest{

   static testmethod void runtest(){
   
    
    Program__c pr = new Program__c();
    pr.GL_String__c = 'test';
    insert pr;
    
    
    Enrollment_Application__c ea = new Enrollment_Application__c();
    ea.Program__c = pr.id;
    insert ea;
    
      
    Exception__c ex = new Exception__c();
    insert ex;
    
    Payment__c pymnt = new Payment__c();
    insert pymnt;
   
       
   PaymentApprovalQueueCntrl cntrl = new PaymentApprovalQueueCntrl();
   cntrl.applyPayments();
   cntrl.checkBatchStatus();
   cntrl.check_print_collateral_status();
   cntrl.pageReload = true;
   cntrl.set_EnrlApps_stage();
   //cntrl.processPayments(pymnt.id);
   cntrl.buildPaymentWrapperList();
 
    }    
  
}