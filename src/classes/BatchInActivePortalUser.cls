/*
02018-01-02-Tue: stephan.spiegel@clearesult.com: Refactored for readability; made logic for determining user inactivity disregard certain SObject types.
        
    How to schedule this to run at the top of every hour:
    String CRON_EXP = '0 0 * * * ?';
    System.schedule('Deactivate portal users hourly', CRON_EXP, new BatchInActivePortalUser());

*/

public class BatchInActivePortalUser implements Database.batchable<SObject>,Database.Stateful,schedulable{  
   
    @TestVisible private Set<Id> recentlyLoggedInUserIds;
    private List<String> sObjectAPINames;
    private Datetime oneHourAgo = System.now().addHours(-1);
    @TestVisible private String userNamesForLog = '';
    @TestVisible private String errorsForLog = '';

    public BatchInActivePortalUser(){
        recentlyLoggedInUserIds = getRecentlyLoggedInUsers(); 
        sObjectAPINames = getRelevantSObjectNames();  
        system.debug('--sObjectAPINames.size()--'+sObjectAPINames.size());
    }
    
    private static Set<Id> getRecentlyLoggedInUsers(){
        Set<Id> recentUsers = new Set<Id>();
        Datetime fourHoursAgo = System.now().addHours(-4);
        for (LoginHistory login : [SELECT UserId FROM LoginHistory where LoginTime > :fourHoursAgo]){
            recentUsers.add(login.UserId);
        }
        return recentUsers;
    }

    private static List<String> getRelevantSObjectNames(){
        List<String> wantedSObjectNames = new List<String>();
        for(Schema.SObjectType objectType : Schema.getGlobalDescribe().Values())
        {
            Schema.DescribeSObjectResult sObjectDescribe = objectType.getDescribe();
            String sObjectName = sObjectDescribe.getName();
            if(sObjectDescribe.isCustom()
                && !sObjectDescribe.isCustomSetting()
                && !isThirdPartyObject(sObjectName)
                && hasLastModifiedDate(objectType)){
                    wantedSObjectNames.add(sObjectName);
            }
        }
        return wantedSObjectNames;
    }

    private static Boolean isThirdPartyObject(String SObjectName){
        Integer doubleUnderLineIndex = SObjectName.indexOf('__');
        return doubleUnderLineIndex > -1 && doubleUnderLineIndex < SObjectName.length()-3;
    }

    private static Boolean hasLastModifiedDate(Schema.SObjectType objectType){
        return objectType.getDescribe().fields.getMap().containsKey('LastModifiedDate');
    }

    public Database.Querylocator start(Database.BatchableContext batchableContext){
        Online_Portal_URLs_Hierarchy__c credentials = Online_Portal_URLs_Hierarchy__c.getInstance();
        String sysAdminUserName  = credentials.system_adminu__c;
        String sQuery ='SELECT Id, Name, isActive, UserName FROM User WHERE isActive = true and Username !=: sysAdminUserName and (UserType = \'PowerCustomerSuccess\' OR UserType = \'CustomerSuccess\')';
        System.debug('===sQuery===='+sQuery);
        return Database.getQueryLocator(sQuery);
    }

    public void execute(Database.BatchableContext bc, List<sObject> userRecords){ 
        try{
            if(userRecords == null) return;
            List<User> usersToUpdate = getStaleUsers(userRecords);
            System.debug('====usersToUpdate===='+usersToUpdate);
            if(usersToUpdate.size() > 0){
                update usersToUpdate;    
            }
        }catch(exception e){
            System.debug('#####===e===='+e);
            errorsForLog += '\n' + e.getMessage();
        }
    }
    
    private List<User> getStaleUsers(List<sObject> userRecords){
        List<User> staleUsers = new List<User>();
        for(Sobject recSobject : userRecords)
        {
            System.debug('======recSobject======'+recSobject);
            User userRecord = (User) recSobject;
            system.debug('---userRecord---'+userRecord);
            if (recentlyLoggedInUserIds.contains(userRecord.Id)) continue; // This user has logged in in the last 4 hours, so skip deactivation
            if (userHasBeenRecentlyActive(userRecord)) continue; // This user has made changes in the last hour, so skip deactivation
            userRecord.isActive = false;
            userNamesForLog += userRecord.UserName + '\n';
            staleUsers.add(userRecord);
        }
        return staleUsers;
    }

    private Boolean userHasBeenRecentlyActive(User userRecord){
        for(String sObjectName :sObjectAPINames){
            String query = 'SELECT COUNT() FROM '+ sObjectName +' WHERE LastModifiedById =\'' +userRecord.id+'\' AND LastModifiedDate > :oneHourAgo'; 
            if(database.countQuery(query) > 0){
                return true;
            }
        }
        return false;
    }

    public void finish(Database.BatchableContext batchableContext) {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {'support+deactivatedusersMssave@unitedcloudsolutions.com'};
            mail.setToAddresses(toAddresses);
            mail.setSenderDisplayName(UserInfo.getName());
            mail.setPlainTextBody(buildEmailBody());
            mail.SetSubject('User Deactivation job ran ' + (String.isBlank(errorsForLog) ? 'succesfully' : 'with errors'));
            // mail.setHtmlBody(htmlBody);
            if(!test.isrunningTest()){
                Messaging.SendEmailResult [] sr = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
    }
    
    private String buildEmailBody(){
        String emailBody = 'User deactivation job ran ';
        if (String.isBlank(errorsForLog)) {
            emailBody += 'successfully.\n';
        }
        else {
            emailBody += 'with errors: \n' + errorsForLog +'\n';
        }
        emailBody += String.isBlank(userNamesForLog) ? 'No users deactivated.' : 'Deactivated these users: \n' + userNamesForLog;
        return emailBody;
    }

    public void execute(SchedulableContext sc) {
    
        BatchInActivePortalUser badpd = new BatchInActivePortalUser();
        Database.executeBatch(badpd , 1);
        
    }
}