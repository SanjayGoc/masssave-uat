Public Class dsmtSpecifiedMeasuresAgreementcntrl{
    public string saId{get;set;}
    public SMA_Record__c SMA1{get;set;}
    public SMA_Record__c SMA2{get;set;}
    public SMA_Record__c SMA3{get;set;}
    public String smaids{get;set;}
    
    
    public dsmtSpecifiedMeasuresAgreementcntrl(){
        saId = ApexPages.currentPage().getParameters().get('saId');
        smaids = ApexPages.currentPage().getParameters().get('smaids');
    }

    public PageReference saveSMA(){
      List<SMA_Record__c> smaRecordsToDelete = new List<SMA_Record__c>();
      List<SMA_Record__c> smaRecords = new List<SMA_Record__c>();
      
      if(SMA1.Id != null) {
            SMA_Record__c sma1ToDelete = new SMA_Record__c(Id=SMA1.id);
            smaRecordsToDelete.add(sma1ToDelete);
      }
      
      if(SMA2.Id != null) {
            SMA_Record__c sma2ToDelete = new SMA_Record__c(Id=SMA2.id);
            smaRecordsToDelete.add(sma2ToDelete);
      }
      
      if(SMA3.Id != null) {
            SMA_Record__c sma3ToDelete = new SMA_Record__c(Id=SMA3.id);
            smaRecordsToDelete.add(sma3ToDelete);
      }
        
      if(SMA1.Type__c != null) {
        SMA1.Safety_Aspect__c = saId;
        SMA1.Id = null;
        smaRecords.add(SMA1);
      }
      
      if(SMA2.Type__c != null) {
        SMA2.Safety_Aspect__c = saId;
        SMA2.Id = null;
        smaRecords.add(SMA2);
      }
      
      if(SMA3.Type__c != null) {        
        SMA3.Safety_Aspect__c = saId;
        SMA3.Id = null;
        smaRecords.add(SMA3);
      }
      delete smaRecordsToDelete;
      insert smaRecords;
      
      Safety_Aspect__C aspect = new Safety_Aspect__C(Id=saId);
      update aspect;
      
      PageReference page = new PageReference('/'+saId);
      page.setRedirect(true);
      return page;
    }
    
    public void init() {
        SMA1 = new SMA_Record__c();
        SMA2 = new SMA_Record__c();
        SMA3 = new SMA_Record__c();
        
        if(smaids != '') {

            String[] smaidsarr = smaids.split(',');
            List<SMA_Record__c> smaRecords = [Select Id, Type__c, value__c, Other_SMA__c, Safety_Aspect__c from SMA_Record__c Where id in :smaidsarr];
            if(smaRecords.size() == 3) {
                SMA1 = smaRecords.get(0);
                SMA2 = smaRecords.get(1);
                SMA3 = smaRecords.get(2);
            } else if(smaRecords.size() == 2) {
                SMA1 = smaRecords.get(0);
                SMA2 = smaRecords.get(1);
            } else if(smaRecords.size() == 1) {
                SMA1 = smaRecords.get(0);
            }
        }
    }
}