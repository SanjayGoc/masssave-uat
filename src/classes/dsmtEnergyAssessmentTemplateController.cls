global with sharing class dsmtEnergyAssessmentTemplateController extends dsmtEnergyAssessmentBaseController {
    global String assessId {get;set;}
    global boolean EACompleted{get;set;}
    // DSST-9852 by HP on 10th Sep 2018
    global String primaryProvider{get;set;}
    global dsmtEnergyAssessmentTemplateController()
    {
        assessId = ApexPages.currentPage().getParameters().get('assessId');
        
        List<Energy_Assessment__c> eaList = [select id,Status__c,Primary_Provider__c from Energy_Assessment__c  where id =: assessId];
        if(eaList != null && eaList.size() > 0 && eaList.get(0).Status__c == 'Assessment Complete'){
            EACompleted = true;
            primaryProvider = eaList.get(0).Primary_Provider__c;
        }
    }
    
    global String getCustomerInfo()
    {
        String custId = ApexPages.currentPage().getParameters().get('custId');
        
        if(custId != null)
        {
            List<Customer__c> customerList = [select id, Name, Service_Address__c,
                                              Service_City__c, Service_State__c, Service_Zipcode__c
                                              from Customer__c
                                              where id =: custId
                                              limit 1];
            
            if(customerList.size() > 0)
            {
                String custInfo = customerList[0].Name + ' | ';
                
                custInfo += customerList[0].Service_Address__c + ', ' + customerList[0].Service_City__c + ', ' + customerList[0].Service_State__c
                            + ', ' + customerList[0].Service_Zipcode__c;
                
                return custInfo;
            }
            
        }
        
        return null;
    }
    
    public boolean getShowIAmDone()
    {
        if(assessId != null && assessId.trim() != null){
            list<Proposal__c> proposalList = [select id 
                                              from Proposal__c
                                              where Energy_Assessment__c =: assessId
                                              limit 1];
            
            if(proposalList.size() > 0)
                return true;
            else
                return false;
        }else{
            return false;
        }
    }
    
    public List<SideBarMenuItem> getMenuItems(){
        String queryString = '?assessId='+getParam('assessId')+'&id='+getParam('id')+'&custId='+getParam('custId');
        return new List<SideBarMenuItem>{
            new SideBarMenuItem('dsmtBuildingInfo','fa fa-home','Building',queryString,true, 'backGreen'),
            new SideBarMenuItem('dsmtSafety','fa fa-fire-extinguisher','Safety Aspects',queryString,true, 'backGreen'),
            new SideBarMenuItem('dsmtrecommendation','fa fa-lightbulb-o','Recommendations',queryString,true, 'backGreen'),
            new SideBarMenuItem('dsmtProposal','fa fa-sticky-note','Proposals',queryString,true, 'backGreen'),
            new SideBarMenuItem('dsmtgeneralinfo','fa fa-key','General',queryString,true, ''),
            new SideBarMenuItem('dsmtAirFlowInfo','fa fa-random','Air Flow',queryString,true, ''),
            new SideBarMenuItem('dsmtThermalEnvelope','fa fa-cube','Thermal Envelope',queryString,true, ''),
            new SideBarMenuItem('dsmtmechanical','fa fa-gears','Mechanical',queryString,true, ''),
            new SideBarMenuItem('dsmtLighting','fa fa-lightbulb-o','Lighting',queryString,true, ''),
            new SideBarMenuItem('dsmtAppliances','fa fa-plug','Appliance',queryString,true, ''),
            new SideBarMenuItem('dsmtWaterFixtures','fa fa-shower','Water Fixtures',queryString,true, ''),
            new SideBarMenuItem('dsmtAttachments','fa fa-paperclip','Attachments',queryString,true, ''), // DSST-13518 | Kaushik Rathore | 12 Oct, 2018
            new SideBarMenuItem('dsmtMarketOutreach','fa fa-file-text-o','Market Outreach',queryString,true, ''), // DSST-15299 | Kaushik Rathore | 20 Nov, 2018
            //new SideBarMenuItem('dsmtMiscellaneous','fa fa-puzzle-piece','Miscellaneous',queryString,true),
            new SideBarMenuItem('dsmtSummary','fa fa-dashboard','Summary',queryString,false, '')
        };
    }
    
    public PageReference completeAssessmentv1()
    {
        return new PageReference('/'+assessId).setRedirect(true);
    }
    
    public PageReference completeAssessment()
    {
        try{
            String isComplete = ApexPages.currentPage().getParameters().get('isComplete');
            
            system.debug('--isComplete ---'+isComplete);
            
            if(isComplete == 'yes')
                update new Energy_Assessment__c(id = assessId,
                                                Status__c = 'Assessment Complete',
                                                Assessment_Complete_Date__c = Date.today());
            else if(isComplete == 'no')
            {
                list<Energy_Assessment__c> aList = [select id, Appointment__c, Appointment__r.Workorder__c
                                                    from Energy_Assessment__c
                                                    where id =: assessId];
                                                    
                Service_Request__c newServiceReq = new Service_Request__c();
                
                if(aList.size() > 0 && aList[0].Appointment__r.Workorder__c != null)
                {
                    update new Workorder__c(Id = aList[0].Appointment__r.Workorder__c,
                                            Status__c = 'Assessment Incomplete');
                
                    newServiceReq.Appointment__c = aList[0].Appointment__c;
                    newServiceReq.Workorder__c = aList[0].Appointment__r.Workorder__c;
                }
                
                newServiceReq.Energy_Assessment__c = assessId;
                
                //insert newServiceReq;
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: ' + ex.getMessage()));
        }
        
        return new PageReference('/'+assessId).setRedirect(true);
    }
    
    public Class SideBarMenuItem{
        public String pageName{get;set;}
        public String iconClassName{get;set;}
        public String label{get;set;}
        public String queryString{get;set;}
        public String pageUrl{get;set;}
        public boolean render{get;set;}
        public String backColorClass {get;set;}
        
        public SideBarMenuItem(String pageName,String iconClassName,String label,String queryString,boolean render, String backColorClass){
            this.pageName = pageName;
            this.iconClassName = iconClassName;
            this.label = label;
            this.queryString = queryString;
            this.pageUrl = '/apex/'+pageName + queryString;
            this.render = render;
            this.backColorClass = backColorClass;
        }
    }
    
    // DSST-9852 by HP on 19th Sep 2018
    
    @RemoteAction
    public static string getPrimaryProvider(String assessmentId){
        List<Energy_Assessment__c> eaList = [select id,Status__c,Primary_Provider__c from Energy_Assessment__c  where id =: assessmentId];
        String primaryProvider  = '';
        if(eaList != null && eaList.size() > 0){
            primaryProvider = eaList.get(0).Primary_Provider__c;
        }
        return primaryProvider;
    }
    
    @RemoteAction
    public static Map<String,Object> totalRecommendation(String assessmentId){
        Map<String,Object> result = new Map<String,Object>();
        if(assessmentId != null && assessmentId.trim() != ''){
            AggregateResult[] ar = [
                SELECT 
                    SUM(Annual_Energy_Savings__c) totalEnergySavings,
                    SUM(Calculated_Cost__c) totalCost,
                    SUM(Calculated_Incentive__c) totalIncentives,
                    SUM(Calculated_Payback__c) totalPayback ,
                    COUNT(Id) totalRecommendations
                FROM Recommendation__c 
                WHERE Energy_Assessment__c =:assessmentId
            ];
            
            result.put('totalEnergySavings',ar[0].get('totalEnergySavings'));
            result.put('totalCost',ar[0].get('totalCost'));
            result.put('totalIncentives',ar[0].get('totalIncentives'));
            result.put('totalPayback',ar[0].get('totalPayback'));
            result.put('totalRecommendations',ar[0].get('totalRecommendations'));
        }
        return result;
    }

    /* DSST-13521 | Kaushik Rathore | 16 Oct,2018 : START */
    @RemoteAction
    public static Map<String,Object> getTAandAttachmentInfo(String assessmentId){
        Map<String,Object> result = new Map<String,Object>{
            'dataCollectionForms' => 0,
            'tradeAllyAccountName' => ''
        };
        List<Energy_Assessment__c> eas = [
            SELECT 
                Id,Name,Trade_Ally_Account__c,Trade_Ally_Account__r.Name,
                (SELECT Id,Name,Customer_Segmentation__c,Customer_likelihood_to_convert__c,Preferred_Contact_Method__c FROM Market_Outreach__r),// DSST-15299 | Kaushik Rathore | 29 Nov, 2018
                (SELECT Id,Name FROM Attachments__r WHERE Attachment_Type__c INCLUDES('Data Collection Form'))
            FROM Energy_Assessment__c
            WHERE Id=:assessmentId
        ];
        if(eas.size() > 0){
            Energy_Assessment__c ea = eas.get(0);
            result.put('dataCollectionForms',ea.Attachments__r.size());
            result.put('tradeAllyAccountName',(ea.Trade_Ally_Account__c != null) ? ea.Trade_Ally_Account__r.Name : '');

            /** DSST-15299 | Kaushik Rathore | 29 Nov, 2018 : END **/
            Set<String> validValues = new Set<String>{'Signed at audit with no deposit','Needs a follow-up','Road Block'};
            Market_Outreach__c mo = (ea.Market_Outreach__r != NULL && ea.Market_Outreach__r.size() > 0) ? ea.Market_Outreach__r.get(0) : new Market_Outreach__c();
            boolean isMarketOutreachValid1 = true,isMarketOutreachValid2 = true,isMarketOutreachValid3 = true;
                    isMarketOutreachValid1 = mo.Id != NULL && mo.Customer_Segmentation__c != NULL && mo.Customer_Segmentation__c != ''; 
                    if(mo.Customer_Segmentation__c != NULL && mo.Customer_Segmentation__c != '' && validValues.contains(mo.Customer_Segmentation__c)){
                        isMarketOutreachValid2 = mo.Customer_likelihood_to_convert__c != NULL && mo.Customer_likelihood_to_convert__c != '';
                        isMarketOutreachValid3 = mo.Preferred_Contact_Method__c != NULL && mo.Preferred_Contact_Method__c != '';
                    }

            result.put('isMarketOutreachRecordValid', isMarketOutreachValid1 && isMarketOutreachValid2 && isMarketOutreachValid3); 
            result.put('mo', mo); 
            result.put('userprofile',UserInfo.getProfileId());
            /** DSST-15299 | Kaushik Rathore | 29 Nov, 2018 : END **/
        }
        return result;
    }
    /* DSST-13521 | Kaushik Rathore | 16 Oct,2018 : END */
}