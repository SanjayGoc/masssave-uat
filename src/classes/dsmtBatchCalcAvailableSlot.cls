global class dsmtBatchCalcAvailableSlot implements Database.Batchable<sObject>, Database.Stateful {
    
    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;

    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(
            'Select Id, Name, Work_Team__c,(Select id, Name,Work_Team_Member__c,Work_Team__c,Appointment_End_Time__c,Appointment_Start_Time__c from Appointments__r where Appointment_Status__c = \'Scheduled\') from Work_Team_Member__c where CreatedDate = Today'
        );
    }

    global void execute(Database.BatchableContext bc, List<Work_Team_Member__c> scope){
        // process each batch of records
        List<Appointment__c> appList = new List<Appointment__c>();
        for (Work_Team_Member__c wtm : scope) 
        {
            for (Appointment__c appt : wtm.Appointments__r) 
            {
                system.debug(appt);
            }
        }
    }    

    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed');
    }    

}