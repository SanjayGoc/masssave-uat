@isTest
public class dsmtEnergyAssessmentValidationCntrlTest extends dsmtEnergyAssessmentValidationHelper{
    public testmethod static void test1(){

        Energy_Assessment__c ea = Datagenerator.setupAssessment();
  
        Attachment att = new Attachment(Name='Test',ParentId=ea.id,body=blob.valueof('test'));
        insert att;
        
        customer__c cus = new customer__c();
        cus.name = 'test';
        insert cus;
  
        Appointment__c app = new Appointment__c();
        app.Energy_Assessment__c = ea.id;
        app.Appointment_Start_Time__c = DateTime.now();
        app.Appointment_End_Time__c = DateTime.now().addDays(3);
        app.Appointment_Type__c = 'Home Energy Assessment';
        insert app;
        
        Eligibility_Check__c ec = new Eligibility_Check__c();
        insert ec;

        Thermal_Envelope__c te = new Thermal_Envelope__c(Energy_Assessment__c=ea.id);
        insert te;

        

        /*Air_Flow_and_Air_Leakage__c afal = new Air_Flow_and_Air_Leakage__c();
        INSERT afal;
        Recommendation__c airFlowRecommendation = new Recommendation__c(
            RecordTypeId = Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().get('Air Sealing Recommendation').getRecordTypeId(),
            Air_Flow_and_Air_Leakage__c = afal.Id,
            Hours__c = null
        ); 
        INSERT airFlowRecommendation;

        Recommendation__c lightBulbRecommendation = new Recommendation__c(
            RecordTypeId = Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().get('Light Bulb Recommendation').getRecordTypeId(),
            Lighting_Location_Picklist__c  = null,
            Quantity__c = null
        ); 
        INSERT lightBulbRecommendation;
        
        Recommendation__c applianceRecommendation = new Recommendation__c(
            RecordTypeId = Schema.SObjectType.Recommendation__c.getRecordTypeInfosByName().get('Appliance Recommendation').getRecordTypeId(),
            Category__c = 'Appliance',
            ApplianceType__c = 'Refrigeration',
            Appliance__c = null,
            Rated_kWh__c = 654
        ); 
        INSERT applianceRecommendation;*/
        
        test.StartTest();

        ValidationResponse response = dsmtEnergyAssessmentValidationController.doValidations(ea.Id,cus.Id);

        List<Database.SaveResult> result = dsmtEnergyAssessmentValidationController.saveSObject('Energy_Assessment__c',ea.Id,'Appointment__c',app.Id,'Appointment__c',app.Id);
        
        test.StopTest();
    }

}