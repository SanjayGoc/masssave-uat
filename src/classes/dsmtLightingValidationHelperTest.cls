@isTest
public class dsmtLightingValidationHelperTest
{
    public testmethod static void test1()
    {  
        Saving_Constant__c sc = new Saving_Constant__c();
        sc.LightingLocation_DaysPerYear__c = 1;
        sc.Kwh_To_BTUH__c = 1;
        insert sc;     
         
        Lighting__c light = new Lighting__c();
        light.Name = 'test';
        insert light;
        
        Lighting_Location__c lloc = new Lighting_Location__c();
        lloc .Name = 'test';
        lloc.Lighting__c = light.Id;
        lloc.LightBulbSetLoadKWH__c = 0;
        lloc.Total_Watts__c = -1;
        lloc.Usage__c = -1;
        lloc.Quantity__c = 11;
        insert lloc;
        
        dsmtEnergyAssessmentValidationHelper.ValidationCategory testWrap=new dsmtEnergyAssessmentValidationHelper.ValidationCategory('test','test');
        
        dsmtLightingValidationHelper cont = new dsmtLightingValidationHelper(); 
        cont.validate('test','test',light.Id,testWrap);  
        
        lloc.Usage__c = 31;
        lloc.Quantity__c = -1;
        update lloc;   
        
        dsmtLightingValidationHelper cont1 = new dsmtLightingValidationHelper(); 
        
        cont1.validate('test','test',light.Id,testWrap);    
    }
}