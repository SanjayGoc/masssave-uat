public class dsmtPreviewAndEmailDocumentController {
    
    public Attachment__c Attachment{get;set;}
    public String Id{get;set;}
    public String viewName{get;set;}
    public String objectType{get;set;}
    public String attachType{get;set;}
    public String attachmentId{get;set;}
    public Boolean InternalAccountTA{get;set;}
    public Boolean showSignboard{get;set;}
    public boolean enable_poller{get;set;}
    public String signatureAttachmentType{get;set;}
    public List<Recommendation_Scenario__c> ProjList{get;set;}
    public String templateID{get;set;}
    public String query{get;set;}
    public String queryID{get;set;}
    public String SessionId{get;set;}
    public String ofn{get;set;}
    public String assessId{get;set;}
    public String proposalId{get;set;}
    public boolean isContract{get;set;}
    public Date signDate{get;set;}
    public Attachment__c customAttachment{get;set;}
    public Energy_Assessment__c eaObj{get;set;}
    public  string IRid{get;set;}
    public String stdAttachmentId{get;set;}
    public String isFromGeneratedoc{get;set;} //DSST-12141
    
    public dsmtPreviewAndEmailDocumentController() {
        Id = ApexPages.currentPage().getParameters().get('Id');
        viewName = ApexPages.currentPage().getParameters().get('viewName');
        objectType = ApexPages.currentPage().getParameters().get('objectType');
        attachType = ApexPages.currentPage().getParameters().get('attachType');
        attachmentId = ApexPages.currentPage().getParameters().get('attachmentId');
        showSignboard = ApexPages.currentPage().getParameters().get('showSignboard') == 'true' ? True : False; 
        isContract  = ApexPages.currentPage().getParameters().get('isContract') == 'true' ? True : False; 
        isFromGeneratedoc = ApexPages.currentPage().getParameters().get('isGeneratedoc'); //DSST-12141
        
        eaObj =  new Energy_Assessment__c();
        init();
        
        List<Recommendation_Scenario__c> ProjList = [select id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c from Recommendation_Scenario__c where id = : id];
        if(ProjList != null && ProjList.size() > 0){
            boolean isInternalAccount = ProjList[0].Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c;
            InternalAccountTA = isInternalAccount;
        }
        List<Barrier__c> brList = [select id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c  from Barrier__c where id = : id];
        if(brList != null && brList.size() > 0){
            boolean isInternalAccountBR = brList[0].Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c;
            InternalAccountTA = isInternalAccountBR;
        }
        
        List<Inspection_Request__c> inspectionReqList = [select id,Energy_Assessment__c,Contractor_Name__r.Internal_Account__c  from Inspection_Request__c where id = : id];
        if(inspectionReqList != null && inspectionReqList.size() > 0) {
            boolean isInternalAccountIR = inspectionReqList[0].Contractor_Name__r.Internal_Account__c;
            InternalAccountTA = isInternalAccountIR;
            if(attachType == 'Customer Service Survey'){
                signatureAttachmentType = 'Customer Service Survey';
                ofn='Customer Service Survey With Signature';
            }else{
                signatureAttachmentType = 'Certificate Of Inspection';
                ofn='COI Document With Signature';
            }
            IRid = Id;
        }
        List<Review__c> reviewList = [select id,Project__r.Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c  from Review__c where id = : id];
        if(reviewList != null && reviewList.size() > 0) {
            InternalAccountTA = false;
        }
        
        List<Proposal__c> proposalList = [select id,Energy_Assessment__c,Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c  from Proposal__c where id = : id];
        if(proposalList != null && proposalList.size() > 0){
            InternalAccountTA  = proposalList[0].Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c;
            assessId = proposalList[0].Energy_Assessment__c;
            proposalId = Id;
        }
        
        List<Energy_Assessment__c> eaList = [Select id,Trade_Ally_Account__r.Internal_Account__c FROM Energy_Assessment__c WHERE Id =: id];
        if(eaList != null && eaList.size() > 0){
            InternalAccountTA  = eaList[0].Trade_Ally_Account__r.Internal_Account__c;
            
        }
    }
    
    public PageReference checkCongaStatus(){
        try{
            system.debug('--attachmentId--'+attachmentId);
            enable_poller = true;
            List<Attachment__c> customAttachmentlist = [SELECT Id,Name,Standard_Attachment_ID__c , (SELECT Id,Name FROM Attachments) FROM Attachment__c WHERE Id =: attachmentId];
            
            if(customAttachmentlist.size() > 0 && customAttachmentlist.get(0).Attachments.size() > 0) {
                customAttachment = customAttachmentlist.get(0);

                DocumentServiceWSFile.clsSalesForceLoginDetail loginDetail = new DocumentServiceWSFile.clsSalesForceLoginDetail();
                loginDetail.SessionID = SessionId;
                loginDetail.serverURL = Login_Detail__c.getInstance().Server_URL__c;
                loginDetail.orgID = Login_Detail__c.getInstance().Org_Id__c;
                loginDetail.userName = Login_Detail__c.getInstance().Attachment_User_Name__c;
                loginDetail.password = Login_Detail__c.getInstance().Attachment_User_Password__c;
                loginDetail.securityToken = Login_Detail__c.getInstance().Attachment_User_Security_Token__c;
                
                DocumentServiceWSFile.clsQueryStringParameters clsQueryStringParametersObj = new DocumentServiceWSFile.clsQueryStringParameters();
                clsQueryStringParametersObj.SessionID = SessionId;
                clsQueryStringParametersObj.ServerURL = Login_Detail__c.getInstance().Server_URL__c;
                clsQueryStringParametersObj.OrgId  = Login_Detail__c.getInstance().Org_Id__c;
                clsQueryStringParametersObj.AttachmentId = attachmentId;
                clsQueryStringParametersObj.SecurityKey = 'intelcore2duo';
                clsQueryStringParametersObj.AccountKey = 'nokia710lumia';
                clsQueryStringParametersObj.Replace = 'YES';

                if (objectType != null && objectType == 'Inspection_Request__c') {
                    clsQueryStringParametersObj.RecordId = Id;
                    clsQueryStringParametersObj.AttachmentLookupCol = 'Inspection_Request__c';
                    if(attachType == 'Customer Service Survey'){
                        clsQueryStringParametersObj.AttachmentType = 'Customer Service Survey';
                    }else{
                        clsQueryStringParametersObj.AttachmentType = 'Certificate Of Inspection';
                    }    
                    
                } else if(objectType != null && objectType == 'Proposal__c' && isContract) {
                    clsQueryStringParametersObj.RecordId = Id;
                    clsQueryStringParametersObj.AttachmentLookupCol = 'Proposal__c';
                    clsQueryStringParametersObj.AttachmentType = attachType;
                }

                
                
                DocumentServiceWSFile.DocumentServiceWSFileSoap documentServiceWSFile = new DocumentServiceWSFile.DocumentServiceWSFileSoap();
                DocumentServiceWSFile.ArrayOfClsUploadProcessResult result = documentServiceWSFile.MigrateAttachment(clsQueryStringParametersObj, customAttachment.Attachments[0].id, 'attachment', loginDetail);
                List<Attachment__c> list_pc_records = [SELECT Id, Status__c,Project__c,Attachment_Type__c FROM Attachment__c WHERE Id = :attachmentId AND Status__c = 'Completed' LIMIT 1];
                if(list_pc_records.size() > 0){
                     enable_poller = false;
                     
                     List<Attachment__c> attlist = new List<Attachment__c>();
                     if(objectType == 'Inspection_Request__c') {
                         attlist = [SELECT Id FROM attachment__c WHERE Inspection_Request__c = :Id and attachment_Type__c in ('Customer Signature','Inspector Signature')];
                     } else if(objectType == 'Proposal__c') {
                         attlist = [SELECT Id FROM attachment__c WHERE Proposal__c = :Id and attachment_Type__c in ('Customer Signature', 'Contractor Signature')];
                     }                     
                     
                     DELETE attlist;
                    
                    if(objectType != 'Inspection_Request__c'){
                        Energy_Assessment__c ess = new Energy_Assessment__c(id=assessId);
                        ess.Participating_Contractor_Initials__c = '';
                        ess.Customer_Sign_Date__c = null;
                        ess.ESSIGNDATE__c = null;
                        ess.Preferred_Contractor_Name__c = ''; //DSST-14093
                        update ess;
                    } 
                    
                    
                     if(showSignboard) {
                         return redirectToCompleted();
                     }
                }
            }
        }catch(Exception ex) {
            system.debug('--ex--'+ex);
            enable_poller = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        return null;
    }
    
    public PageReference redirectToCompleted() {
    
      String url = '';
      // url = '/apex/dsmtPreviewAndEmailDocument?';
      url = '/apex/dsmtPreviewAndEmailDocument?';
      url += 'Id='+Id;
      url += '&objectType='+ObjectType;
      url += '&attachType='+attachType;
      url += '&attachmentId='+attachmentId;
      url += '&showSignboard=false';

      PageReference page = new PageReference(url);
      page.setRedirect(true);
      return page;
    }

    public void init(){
       
       List<Attachment__c> attachlist = null;
       String query = 'select Id,Name,Standard_Attachment_ID__c ,Attachment_Type__c,Attachment_Name__c,CreatedDate,Email_File_Download_URL__c, File_Url__c,(SELECT Id,Name FROM Attachments) FROM Attachment__c WHERE Id != null ';
       if(objectType == 'Barrier__c'){
          if(attachmentId != null && attachmentId != ''){
             query += ' and Id =: attachmentId ';
          }else{
             query += ' and Barrier__c=:Id and Attachment_Type__c =: attachType ';
          }
          query += ' ORDER BY CreatedDate DESC limit 1';
          system.debug('--query--'+query);
          attachlist = database.query(query);
       } else if(objectType == 'Inspection_Request__c'){
          if(attachmentId != null && attachmentId != ''){
             query += ' and Id =: attachmentId ';
          }else{
             query += ' and Inspection_Request__c=:Id and Attachment_Type__c =: attachType ';
          }
          query += ' ORDER BY CreatedDate DESC limit 1';
          system.debug('--query--'+query);
          attachlist = database.query(query);
       } else if(objectType == 'Project__c'){
          if(attachmentId != null && attachmentId != ''){
             query += ' and Id =: attachmentId ';
          }else{
             query += ' and Project__c=:Id and Attachment_Type__c =: attachType ';
          }
          query += ' ORDER BY CreatedDate DESC limit 1';
          system.debug('--query--'+query);
          attachlist = database.query(query);
       } else if(objectType == 'Proposal__c'){
          if(attachmentId != null && attachmentId != ''){
             query += ' and Id =: attachmentId ';
          }else{
             query += ' and Proposal__c=:Id and Attachment_Type__c =: attachType ';
          }
          query += ' ORDER BY CreatedDate DESC limit 1';
          system.debug('--query--'+query);
          attachlist = database.query(query);
       } else if(objectType == 'Review__c'){
          if(attachmentId != null && attachmentId != ''){
             query += ' and Id =: attachmentId ';
          }else{
             query += ' and Review__c=:Id and Attachment_Type__c =: attachType ';
          }
          query += ' ORDER BY CreatedDate DESC limit 1';
          system.debug('--query--'+query);
          attachlist = database.query(query);
          
          //--------DSST-8416 - START---------
       }else if(objectType == 'Energy_Assessment__c'){
           if(attachmentId != null && attachmentId != ''){
             query += ' and Id =: attachmentId ';
          }else{
             query += ' and Energy_Assessment__c =:Id and Attachment_Type__c =: attachType ';
          }
          query += ' ORDER BY CreatedDate DESC limit 1';
          system.debug('--query--'+query);
          attachlist = database.query(query);
       }
       
       
       if(attachlist != null && attachlist.size() > 0){
          Attachment = attachlist.get(0);
          stdAttachmentId = attachment.Standard_Attachment_ID__c;
          if(stdAttachmentId == null && attachment.Attachments.size() > 0){
              stdAttachmentId = attachment.Attachments[0].id;
          }
          
       }
       
       //--------DSST-8416 - END---------
       
       
    }
    
    public PageReference Cancel(){
       String url = '';
       if(objectType == 'Energy_Assessment__c'){
          url = '/apex/dsmtGenerateDocuments?id='+Id;
       }else{
          url = '/'+Id;
       }
       
       PageReference page = new PageReference(url);
       page.setRedirect(true);
       return page;

    }
    
    public PageReference SendEmail(){
       Pagereference pg = Page.SendEmail;
       pg.getParameters().put('Id', Id);
       pg.getParameters().put('ObjectType', objectType);
       pg.getParameters().put('mailType', attachType);
       pg.getParameters().put('attachIds', Attachment.Id);
       pg.getParameters().put('isGeneratedoc',isFromGeneratedoc); //DSST-12141
       
       pg.setRedirect(true);
       return pg;

    }
    
    public void getdataForInspectionReportDocument(){
        system.debug('--attachmentId--'+attachmentId);
        
         SessionId = UserInfo.getsessionId();
        system.debug('--Login SessionId --'+SessionId);
        
        List<Inspection_Request__c> IRList = [Select id,name from Inspection_Request__c where id =: IRid];
        
        if(SessionId == null){
          SessionId = dsmtFutureHelper.getGuestSession();
        }
        system.debug('--Login SessionId --'+SessionId);
        
        String TemplateName = 'Inspection Report Template';
        
        List<APXTConga4__Conga_Template__c> Templatelist = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c =:TemplateName];
        
        if(Templatelist.size() > 0){
            templateID = Templatelist.get(0).Id;
        }
        
        List<APXTConga4__Conga_Merge_Query__c> querylist = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Inspection Report Query']; 
        if(querylist.size() > 0){
            query = querylist.get(0).Id+'?pv0='+Id;
        }
        
        if(IRList.size() > 0){
        Inspection_request__c ir = IRList.get(0);
        Attachment__c cAttpdf = new Attachment__c();
        cAttpdf.Status__c = 'New';
        //cAttpdf.Energy_Assessment__c = assessId;
        cAttpdf.Inspection_request__c = ir.Id;
        cAttpdf.Attachment_Type__c = 'Certificate Of Inspection';
      
        insert cAttpdf;
        
        attachmentId = cAttpdf.id;       
        }
    }
    
    Public void getDataforCustomerSurveyDocument(){
        List<Inspection_Request__c> IRList = [Select id,name from Inspection_Request__c where id =: IRid];
        SessionId = UserInfo.getsessionId();
        if(SessionId == null){
          SessionId = dsmtFutureHelper.getGuestSession();
        }
        
        String TemplateName = 'Customer Service Survey Form';
        
        List<APXTConga4__Conga_Template__c> Templatelist = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c =:TemplateName];
        
        if(Templatelist.size() > 0){
            templateID = Templatelist.get(0).Id;
        }
        
        List<APXTConga4__Conga_Merge_Query__c> querylist = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c IN ('Customer Service Survey Query','Customer Service Survey Query 01','Customer Service Survey Query 02')]; 
        query = '';
        if(querylist.size() > 0){
            For(APXTConga4__Conga_Merge_Query__c q : querylist){
                query += q.ID +'?pv0='+Id+ ',';
            }
        }
        query = query.removeEnd(',');
        if(IRList.size() > 0){
            Inspection_request__c ir = IRList.get(0);
            Attachment__c cAttpdf = new Attachment__c();
            cAttpdf.Status__c = 'New';
            //cAttpdf.Energy_Assessment__c = assessId;
            cAttpdf.Inspection_request__c = ir.Id;
            cAttpdf.Attachment_Type__c = 'Customer Service Survey';
          
            insert cAttpdf;
            
            attachmentId = cAttpdf.id; 
        }
    }
    
    public void getdataForSignContractDocument(){
        
        generateContract();
        
        ofn = 'Contract Document With Signature';
    }
    
    public boolean migrateAttachServiceCall{get;set;}
    public void generateContract(){
        SessionId = UserInfo.getsessionId();
        system.debug('--Login SessionId --'+SessionId);
        if(SessionId == null){
          SessionId = dsmtFutureHelper.getGuestSession();
        }
        
        migrateAttachServiceCall = false;
        
        List<Proposal__c> proplist = [select Name,id,Type__c,Add_to_Contract__C,Name_Type__c ,Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c from proposal__c where Id=: proposalId];
        String AttachType = null;
        List<String> types = new List<String>();
        
        if(proplist.size() > 0){           
            Proposal__c proposal = proplist.get(0);
            
            if(proposal.Type__c == 'Insulation'){
                AttachType = 'Contract for Product and Services - Weatherization';
            }else if(proposal.Type__c  == 'Air Sealing'){
                AttachType  ='Contract for Product and Services - Air Sealing';
            }else if(proposal.Type__c  == 'Electric Thermostats'){
                AttachType  = 'Contract for Product and Services - TStats';
            } else {
                AttachType  = 'Contract';
            }
            
            system.debug('--proposal--'+proposal);
            String Congatemp = 'Mass Proposal Contract clearesult template';  //DSST-14093
            system.debug('--proposal.Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c--'+proposal.Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c);
            if(proposal.Energy_Assessment__r.Trade_Ally_Account__r.Internal_Account__c == false){
                
                List<APXTConga4__Conga_Template__c> TradeallyCongaTemplateList = [select Id,Unique_Template_Name__c from APXTConga4__Conga_Template__c Where Trade_Ally_Account__c =: proposal.Energy_Assessment__r.Trade_Ally_Account__c limit 1];
                Congatemp = !TradeallyCongaTemplateList.isEmpty() ? TradeallyCongaTemplateList[0].Unique_Template_Name__c : 'HPC Mass Proposal Template';
                //Congatemp = docType == 'Single' ? 'HPC Proposal Template': 'HPC Mass Proposal Template';
            }
            
            system.debug('--Congatemp--'+Congatemp);
            
            List<String> queryList = new List<String>();
            queryList.add('Proposal_contract_Query1');
            queryList.add('Proposal_contract_Query2');
            queryList.add('Proposal_contract_Query3');
            queryList.add('Proposal_contract_Query4');
            queryList.add('Proposal_contract_Query5');
            queryList.add('Proposal_contract_Query6');
            queryList.add('Proposal_contract_Query7');
            queryList.add('Proposal_contract_Query8');
            queryList.add('Proposal_contract_Query9');
            queryList.add('Proposal_contract_Query91');
            //--------DSST-6505 - START---------
            queryList.add('Proposal_contract_Query92');
            //--------DSST-6505 - END---------
            List<APXTConga4__Conga_Template__c> CongaTemplateList = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c =: Congatemp];
            List<APXTConga4__Conga_Merge_Query__c> CongaqueryList = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In: queryList order By Unique_Conga_Query_Name__c ASC];
            query = '';
            for(APXTConga4__Conga_Merge_Query__c q : CongaqueryList)
            {
                query += q.ID +'?pv0='+assessId+ ',';
            }
            query = query.removeEnd(',');
            
            templateID = CongaTemplateList.get(0).id;
            system.debug('--query--'+query);
            
            Attachment__c cAttpdf = new Attachment__c();
            cAttpdf.Status__c = 'New';
            cAttpdf.Energy_Assessment__c = assessId;
            cAttpdf.Proposal__c = proposal.Id;
            cAttpdf.Attachment_Type__c = AttachType;
          
            insert cAttpdf;  

            attachmentId = cAttpdf.id;
        }
        
    }
    
    @RemoteAction
    public static String saveDualSignCOI(String id, String imgURI1, String imgURI2, String attachType, String objectType){
        try{
            Datetime now = Datetime.now();
            String datetimestr = now.format('MMddyyyyhhmm');
            dsmtCreateEAssessRevisionController.StopEATrigger = true;
            if(objectType == 'Inspection_Request__c'){
                    
                    List<Attachment__c> listattach = new List<Attachment__c>();
                    
                    Attachment__c cAtt = new Attachment__c();
                    cAtt.Attachment_Name__c = 'Customer_Sign_'+datetimestr+'.png';
                    cAtt.Application_Type__c = 'Custom';
                    cAtt.Attachment_Type__c = 'Customer Signature';
                    cAtt.Inspection_request__c = id;
                    //cAtt.Energy_Assessment__c = assessId;
                    cAtt.Status__c = 'New';
                    listattach.add(cAtt);
                    //insert cAtt;
                    
                    Attachment__c cAtt2 = new Attachment__c();
                    cAtt2.Attachment_Name__c = 'Contractor_Sign_'+datetimestr+'.png';
                    cAtt2.Application_Type__c = 'Custom';
                    cAtt2.Attachment_Type__c = 'Inspector Signature';
                    cAtt2.Inspection_request__c = id;
                    //cAtt2.Energy_Assessment__c = assessId;
                    cAtt2.Status__c = 'New';
                    listattach.add(cAtt2);
                    //insert cAtt2;
                    
                    insert listattach;
                    
                    Attachment sAttSign = new Attachment();
                    sAttSign.ParentID = cAtt.id;
                    sAttSign.Body = EncodingUtil.base64Decode(imgURI1);
                    sAttSign.contentType = 'image/png';
                    sAttSign.Name = 'Customer_Sign_'+datetimestr+'.png';
                    insert sAttSign;
                    
                    Attachment sAttSign2 = new Attachment();
                    sAttSign2.ParentID = cAtt2.id;
                    sAttSign2.Body = EncodingUtil.base64Decode(imgURI2);
                    sAttSign2.contentType = 'image/png';
                    sAttSign2.Name = 'Contractor_Sign_'+datetimestr+'.png';
                    insert sAttSign2;
                    
                    Inspection_Request__c IR = new Inspection_Request__c(id=id);
                    IR.Signature_Date__c = Date.Today();
                    IR.Customer_Acceptance_Signature_Id__c = sAttSign.id;
                    IR.Inspector_Sign_Id__c = sAttSign2.id;
                    IR.COI_Signed__c = true;
                    update IR;
                    
                    return 'Success~~~'+cAtt.Id+'~~~COI Report~~~';
            } else{
                return 'Fail To COI Report';
            }
        }catch(Exception ex){
            system.debug('--ex--'+ex);
            return 'Fail'+' : '+ex.getmessage();
        }  
    
    }
    
    @RemoteAction
    public static String saveDualSign(String assessId, String id, String imgURI1, String imgURI2, String attachType, String objectType, String customerSignDateStr, String esSignDateStr,Boolean isInternal, String preferredContractor) {
        system.debug('--Id--'+id+'--imgURI--'+imgURI1 + ' '+ imgURI2);
        
        try{
            Datetime now = Datetime.now();
            String datetimestr = now.format('MMddyyyyhhmm');
            dsmtCreateEAssessRevisionController.StopEATrigger = true;
            if(objectType == 'Proposal__c'){
                    
                    List<Attachment__c> listattach = new List<Attachment__c>();
                    
                    Attachment__c cAtt = new Attachment__c();
                    cAtt.Attachment_Name__c = 'Customer_Sign_'+datetimestr+'.png';
                    cAtt.Application_Type__c = 'Custom';
                    cAtt.Attachment_Type__c = 'Customer Signature';
                    cAtt.Proposal__c = id;
                    cAtt.Energy_Assessment__c = assessId;
                    cAtt.Status__c = 'New';
                    listattach.add(cAtt);
                    //insert cAtt;
                    
                    Attachment__c cAtt2 = new Attachment__c();
                    cAtt2.Attachment_Name__c = 'Contractor_Sign_'+datetimestr+'.png';
                    cAtt2.Application_Type__c = 'Custom';
                    cAtt2.Attachment_Type__c = 'Contractor Signature';
                    cAtt2.Proposal__c = id;
                    cAtt2.Energy_Assessment__c = assessId;
                    cAtt2.Status__c = 'New';
                    listattach.add(cAtt2);
                    //insert cAtt2;
                    
                    insert listattach;
                    
                    Attachment sAttSign = new Attachment();
                    sAttSign.ParentID = cAtt.id;
                    sAttSign.Body = EncodingUtil.base64Decode(imgURI1);
                    sAttSign.contentType = 'image/png';
                    sAttSign.Name = 'Customer_Sign_'+datetimestr+'.png';
                    insert sAttSign;
                    
                    Attachment sAttSign2 = new Attachment();
                    sAttSign2.ParentID = cAtt2.id;
                    sAttSign2.Body = EncodingUtil.base64Decode(imgURI2);
                    sAttSign2.contentType = 'image/png';
                    sAttSign2.Name = 'Contractor_Sign_'+datetimestr+'.png';
                    insert sAttSign2;
                    
                    Proposal__c proposal = new Proposal__c(id=id);
                    proposal.Signature_Date__c = Date.Today();
                    update proposal;
                    
                    String[] dateParts = customerSignDateStr.split('/');
                    Date customerSignDateVal = Date.newInstance(Integer.valueOf(dateParts[2]), Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]));
                    
                    Date esSignDateVal; //DSST-14093
                    if(!IsInternal) { //DSST-14093
                        dateParts = esSignDateStr.split('/');
                        esSignDateVal = Date.newInstance(Integer.valueOf(dateParts[2]), Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]));
                    }
                    
                    Energy_Assessment__c ess = new Energy_Assessment__c(id=assessId);
                    ess.Customer_Signature_Id__c = sAttSign.id;
                    ess.CLR_Signature_Id__c = sAttSign2.id;
                    ess.Customer_Sign_Date__c = customerSignDateVal;
                    ess.ESSIGNDATE__c = esSignDateVal;
                    ess.Preferred_Contractor_Name__c = preferredContractor;  //DSST-14093
                    update ess;
                    
                    return 'Success~~~'+cAtt.Id+'~~~GenerateContract~~~';
            } else{
                return 'Fail To Generate Contract';
            }
        }catch(Exception ex){
            system.debug('--ex--'+ex);
            return 'Fail'+' : '+ex.getmessage();
        }        
    }

    @RemoteAction
    public static String saveSign(String projectId, String docname, String imgURI, String attachType, String objectType){
        system.debug('--projectId --'+projectId+'--imgURI--'+imgURI);
        system.debug('--docname--'+docname);
        try{
            if(objectType.equalsIgnoreCase('Inspection_Request__c')){
                List<Inspection_Request__c> projectList  = [SELECT Id,Name, Customer_Acceptance_Signature_Id__c from Inspection_Request__c where Id = :projectId limit 1];
                if(projectList.size() > 0){
                    Inspection_Request__c enApp  = projectList.get(0);
                    
                    Datetime now = Datetime.now();
                    String datetimestr = now.format('MMddyyyyhhmm');
                    
                    docname = docname.replace('Pdf','Doc');
                    system.debug('--docname--'+docname);
                    
                    Attachment__c cAtt = new Attachment__c();
                    cAtt.Attachment_Name__c = 'Customer_Sign_'+datetimestr+'.png';
                    cAtt.Application_Type__c = 'Custom';//Prescriptive';
                    cAtt.Attachment_Type__c = 'Customer Signature'; //'Trade Ally Signature'; //
                    cAtt.Inspection_Request__c = enApp.Id;
                    cAtt.Status__c = 'New';
                    insert cAtt;
                    
                    Attachment sAttSign = new Attachment();
                    sAttSign.ParentID = cAtt.id;
                    sAttSign.Body = EncodingUtil.base64Decode(imgURI);
                    sAttSign.contentType = 'image/png';
                    sAttSign.Name = 'Customer_Sign_'+enApp.Name+'_'+datetimestr+'.png';
                    insert sAttSign;
                    
                    enApp.Customer_Acceptance_Signature_Id__c = sAttSign.id;
                    enApp.Signature_Date__c = Date.Today();
                    if(attachType == 'Certificate of Inspection'){
                        enApp.COI_Signed__c = true;
                    } 
                    UPDATE enApp;
                    
                    //String session_id = UserInfo.getSessionId();
                    //String session_id = 'USERNAME:' + Login_Detail__c.getOrgDefaults().Attachment_User_Name__c + ';PASSWORD:' +  + Login_Detail__c.getOrgDefaults().Attachment_User_Password__c+  + Login_Detail__c.getOrgDefaults().Attachment_User_Security_Token__c;
                    //String server_url = Dsmt_Salesforce_Base_Url__c.getOrgDefaults().PartnerURL__c;
                    //String environment_name = UserInfo.getOrganizationId(); 
              
                    Attachment__c cAtt1 = new Attachment__c();
                    cAtt1.Attachment_Name__c = docname;
                    cAtt1.Application_Type__c = 'Custom';
                    cAtt1.Attachment_Type__c = attachType;
                    cAtt1.Inspection_Request__c = enApp.Id;
                    //cAtt1.Attachment_Offer_Template__c = 'Contract Documents - ' + enApp.Id + '.pdf';
                    cAtt1.Category__c = 'INSPECTION_REQUEST';
                    cAtt1.Attachment_Offer_Template__c = docname;
                    cAtt1.Status__c = 'New';
                    insert cAtt1;
                    
                    //system.debug('--webservice call init--');
                    //dsmtFuture.perform_callout(session_id, server_url, cAtt1.Id, enApp.Id, environment_name);
                    // invokeWebservice(enApp.Id, cAtt1.Id);
                    //system.debug('--webservice call intiated--');
                    if(attachType == 'Customer Service Survey'){
                        return 'Success~~~'+cAtt1.Id+'~~~Customer Service Survey';
                    }else{
                        return 'Success~~~'+cAtt1.Id+'~~~COI Report';
                    }
                    
                }else{
                    return 'Fail'+' : '+'Invalid Id for Inspection Report!!!';
                }     
            }
        }catch(Exception ex){
            system.debug('--ex--'+ex);
            return 'Fail'+' : '+ex.getmessage();
        }
        
        return 'Success~~~12112121';
    }
}