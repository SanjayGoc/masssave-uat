@istest
public class WaterFixtureTriggerHandlerTest 
{
    @istest
    static void test()
    {
        /*Test.startTest();
dsmtEAModel.setupAssessment();

Test.stopTest();  

List<Water_Fixture__c> waterList = [Select Id, Type__c, RecordTypeId From Water_Fixture__c];

update waterList;*/
        
        Energy_Assessment__c ea = Datagenerator.setupAssessment();
        
        List<Water_Fixture__c> waterList = new List<Water_Fixture__c>();
        
        Water_Fixture__c waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Pool').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterobj.Type__c = 'Pool';
        waterobj.NumberOperatingMonths__c =10;
        waterobj.TimerHours__c = 4;
        waterobj.WinterHours__c = 5;
        waterobj.PoolPumpWatts__c = 20;
        waterList.add(waterobj);
        Test.startTest();
        dsmtWaterFixtureHelper.ComputePoolHasOffSeasonUseCommon(waterobj);
        
        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Sink').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);
        
        waterobj = new Water_Fixture__c();
        waterobj.RecordTypeId = Schema.SObjectType.Water_Fixture__c.getRecordTypeInfosByName().get('Spa').getRecordTypeId();
        waterobj.Energy_Assessment__c = ea.Id;
        waterList.add(waterobj);
        
        
        insert waterList;
        update waterList;
        Test.stopTest();   
        
        
        
    }
}
/*@istest
public class WaterFixtureTriggerHandlerTest 
{
@istest
static void WaterFixtureTriggerHandlerTesting()
{
Energy_Assessment__c ea =Datagenerator.setupAssessment();

test.startTest();
List<Water_Fixture__c> wflist =new List<Water_Fixture__c>();

Water_Fixture__c wf =new Water_Fixture__c();
wf.Energy_Assessment__c=ea.id;
wf.Actual_Year__c=5;
wf.Area__c=10;
wf.Type__c='Shower';
wf.Months_Used__c='Feb';
wf.Fuel_Type__c='Electricity';
insert wf;
wflist.add(wf);

wf.Type__c='Sink';
update wf;
wflist.add(wf);

wf.Type__c='Bath';
update wf;
wflist.add(wf);

wf.Type__c='Pool';
update wf;
wflist.add(wf);
wf.Type__c='Spa';
update wf;
wflist.add(wf);
test.stopTest();

dsmtEAModel.Surface newSurfaceObj = new dsmtEAModel.Surface();
newSurfaceObj.waterObj=wf;
newSurfaceObj.waterList=wflist;
String str = WaterFixtureTriggerHandler.getSObjectFields('Water_Fixture__c');
WaterFixtureTriggerHandler wfth =new WaterFixtureTriggerHandler();

}
}*/