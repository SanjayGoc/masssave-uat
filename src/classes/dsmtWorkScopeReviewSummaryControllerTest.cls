@istest
public class dsmtWorkScopeReviewSummaryControllerTest 
{
	@istest
    static void test()
    {
        Review__c rv =new Review__c();
        //rv.Id='test';
        insert rv;
        
        Recommendation_Scenario__c pro= new Recommendation_Scenario__c();
        insert pro;
        
        Project_Review__c pr =new Project_Review__c();
        pr.Project__c=pro.id;
        insert pr;
        
        Change_Order_Line_Item__c colt =new Change_Order_Line_Item__c();
        insert colt;

        Recommendation__c rc =new Recommendation__c ();
        //rc.id='a0I4D000000XUT3';
        rc.Add_Depth__c='3';
        insert rc;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(rv);
        dsmtWorkScopeReviewSummaryController test = new dsmtWorkScopeReviewSummaryController(sc);
        
        ApexPages.currentPage().getParameters().put('prjid',pr.id);
        ApexPages.currentPage().getParameters().put('id',rv.id);
        ApexPages.currentPage().getParameters().put('projIds',rc.Id);
        Apexpages.currentPage().getParameters().put('projid',pro.Id);
        
        dsmtWorkScopeReviewSummaryController dwsrv= new dsmtWorkScopeReviewSummaryController();
        dwsrv.createReviewRec();
      	dwsrv.loadRecommendations();
    }
}