global with sharing class AtticRoofsManagerComponentCntrl {
    public Id thermalEnvelopeTypeId{get;set;}
    public Ceiling__c newAtticRoof{get{
        if(newAtticRoof == null){
            newAtticRoof = new Ceiling__c();
        }
        return newAtticRoof;
    }set;}
    public void addNewAtticRoof(){
        System.debug('@@...addNewAtticRoof...');
        try{
            if(newAtticRoof != null && thermalEnvelopeTypeId != null){
                Thermal_Envelope_Type__c tet = [SELECT Id,Name,Next_Attic_Roof_Number__c FROM Thermal_Envelope_Type__c WHERE Id=:thermalEnvelopeTypeId LIMIT 1];
                Decimal nextNumber = (tet.Next_Attic_Roof_Number__c != null) ? tet.Next_Attic_Roof_Number__c : 1;
                newAtticRoof = newAtticRoof.clone(false);
                newAtticRoof.Thermal_Envelope_Type__c = thermalEnvelopeTypeId;
                newAtticRoof.Name = 'Attic Roof ' + nextNumber;
                newAtticRoof.Exposed_To__c = newAtticRoof.Roof_To__c;
                INSERT newAtticRoof;
                if(newAtticRoof.Id != null){
                    tet.Next_Attic_Roof_Number__c = nextNumber + 1;
                    UPDATE tet;
                }
                newAtticRoof = null;
            }
        }catch(Exception ex){
            System.debug('@@ EXCEPTION :: ' + ex.getMessage());
        }
    }
    public void deleteRoof(){
        String roofId = ApexPages.currentPage().getParameters().get('roofId');
        if(roofId != null){
            DELETE new Ceiling__c(Id = roofId);
        }
    }
    public List<Ceiling__c> getAtticRoofs(){
        System.debug('thermalEnvelopeTypeId :: ' + thermalEnvelopeTypeId);
        if(thermalEnvelopeTypeId != null){
            return Database.query('SELECT '+ getSObjectFields('Ceiling__c') + ' FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId');
        }else{
            return new List<Ceiling__c>();
        }
    }
    public List<Id> getAtticRoofIds(){
        List<Id> roofIds = new List<Id>();
        if(thermalEnvelopeTypeId != null){
            for(Ceiling__c roof : [SELECT Id,Name FROM Ceiling__c WHERE Thermal_Envelope_Type__c =:thermalEnvelopeTypeId]){
                roofIds.add(roof.Id);
            }
        }
        return roofIds;
    }
    
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
    
}