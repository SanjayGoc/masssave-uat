@isTest
public with sharing class dsmtDispatchConsoleControllerTest 
{

    public static testmethod void testSchedulerScreen()
    {
        System_Config__c s = new System_Config__c(Arrival_Window_min_before_Appointment__c = 40,
                                                  Arrival_Window_min_after_Appointment__c = 60);
        insert s;
        
        Workorder_Type__c woType = new Workorder_Type__c(Name = 'Test');
        insert woType;
        
        Account acc = Datagenerator.createAccount();
        insert acc;
        
        Premise__c premise = Datagenerator.createPremise();
        insert premise;
        
        Customer__c cust =new Customer__c();
        cust.Account__c=acc.Id;
        cust.Service_Address__c='test';
        cust.Service_City__c='test';
        cust.Service_State__c='CO';
        cust.Service_Postal_Code__c='test';
        insert cust;
        
        //location
        Location__c location = new Location__c(Name = 'Fresno');
        insert location;
        
        //Scheduler
        Scheduler__c scheduler = new Scheduler__c(User__c = UserInfo.getUserId(),
                                                  Location__c = location.Id,
                                                  Default_Location__c = true);
        insert scheduler;
        
        Employee__c emp = Datagenerator.createEmployee(location.Id);
        emp.Status__c='Approved';
        emp.Lunch_Start_Time__c='12:00 AM';
        emp.Lunch_End_Time__c='01:00 AM';
        insert emp;

        Work_Team__c wt = Datagenerator.CreateWorkTeam(location.Id, emp.Id);
        wt.Service_Date__c = system.today();
        wt.Captain__c = emp.Id;
        wt.Default_Region__c='Boston';
        wt.Unavailable__c=false;
        wt.Start_Date__c=date.today();
        wt.End_Date__c=date.today();
        wt.Lunch_Start_Date__c=date.today();
        wt.Lunch_End_Date__c=date.today();
        insert wt;
        
        Time_Slots__c ts = new Time_Slots__c();
        ts.Work_Team__c = wt.Id;
        insert ts;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Work_Team__c = wt.Id;
        wo.Third_Visit__c = system.today();
        wo.Requested_Date__c = system.today().addDays(2);
        wo.Scheduled_Start_Date__c = system.today();
        wo.Scheduled_Date__c=date.today();  
        wo.Scheduled_End_Date__c = system.today().addDays(2);
        wo.Requested_Start_Date__c = system.today();
        wo.Requested_End_Date__c = system.today().addDays(2);
        wo.Customer__c = cust.Id;
        wo.Address__c = 'test';
        wo.City__c = 'city';
        wo.State__c = 'state';
        wo.Zipcode__c = '12234';
        wo.Time_Slot__c = ts.Id;
        wo.Duration__c = 90;
        wo.Alert__c='test';
        insert wo;
        
        string woId = wo.Id;
        
        wo = Datagenerator.createWo();
        wo.Work_Team__c = wt.Id;
        wo.Third_Visit__c = system.today();
        wo.Requested_Date__c = system.today().addDays(2);
        wo.Scheduled_Start_Date__c = system.today();
        wo.Scheduled_End_Date__c = system.today().addDays(2);
        wo.Requested_Start_Date__c = system.today();
        wo.Requested_End_Date__c = system.today().addDays(2);
        wo.Customer__c = cust.Id;
        wo.Address__c = 'test';
        wo.City__c = 'city';
        wo.State__c = 'state';
        wo.Zipcode__c = '12234';
        wo.Time_Slot__c = ts.Id;
        wo.Duration__c = 90;
        wo.Cancelled_date__c=date.today();
        wo.Notes__c='testing';
        insert wo;
        
        /*Skill__c skill =new Skill__c();
        skill.SampleData__c=true;
        insert skill;
        
        Required_Skill__c rskill =new Required_Skill__c();
        rskill.Workorder_Type__c=woType.id;
        rskill.Skill__c=skill.id;
        insert rskill;
        
        Employee_Skill__c eskill =new Employee_Skill__c();
        insert eskill;*/
        
        Review__c review = new Review__c();
        review.Review_Type__c='Document';
        review.State__c = 'Approved';
        review.Status__c='New';
        //review.DSMTracker_Contact__c = dsmt.Id;
        //review.Trade_Ally_Account__c =ta.Id;
        insert review;
        
        Eligibility_Check__c ec =new Eligibility_Check__c();
        ec.How_many_units__c='2 units';
        ec.How_many_do_you_own__c  = '1';
        ec.Do_you_own_or_rent__c = 'Own';
        insert ec;
        
        //ApexPages.currentPage().getParameters().put('custId',cust.Id);
        ApexPages.currentPage().getParameters().put('WoTypeId',wotype.Id);
        //ApexPages.currentPage().getParameters().put('ReviewId',review.Id);
        //sApexPages.currentPage().getParameters().put('ElcId',ec.Id);
        //ApexPages.currentPage().getParameters().put('worderId',wo.Id);
        
        Test.startTest();
        dsmtDispatchConsoleController cntrl = new dsmtDispatchConsoleController();
        
    cntrl.getRegions();
        cntrl.view='week';
        cntrl.changeWTId='test';
        cntrl.timeStr=':';
        cntrl.wo.Third_Visit__c = system.today();
        cntrl.wo2.Third_Visit__c = system.today();
        cntrl.movedIds = wo.Id+'-';
        
        cntrl.queryWT();
        cntrl.queryWT2();
        cntrl.queryWO();
        
        cntrl.wrtmId = wt.Id;
        cntrl.moveWO();
        
        cntrl.wrtmId2 = wt.Id;
        cntrl.moveWO();
        
        cntrl.selectedDate = system.today().month()+'/'+system.today().day()+'/'+system.today().year();
        
        cntrl.wteamid = wt.Id;
        cntrl.worderid = wo.Id;
        cntrl.scheduleWorder();
        
        cntrl.woId = wo.Id;
        cntrl.startTme = ' 08:00AM';
        
        cntrl.fetchworkorder();
        cntrl.reloadData();
        cntrl.scheduleAllWorders();
        cntrl.refreshPage();
        cntrl.getEmployees();
        cntrl.getPolylineColors();
        cntrl.getWorkteams();
        cntrl.getWorkteams2();
        cntrl.getCompletedWOWorkteams();


        cntrl.wtId = wt.Id;
        cntrl.saveWorkOdr();
        
        cntrl.workorder.Third_Visit__c = system.today();
        cntrl.selected_time = '08:00AM';
        cntrl.CreateSchedule();
        cntrl.refetchSchedule();
        cntrl.prevPage();
        cntrl.nextPage();
        cntrl.getBlockedTimeStr();
        cntrl.getHourText('08 am');
        
        cntrl.AppointmentId='test';
        cntrl.CancenAppointment();


        //wo.Work_Team__c = wt.Id;
        //update wo;
        
        try{
            dsmtDispatchConsoleController.OptimizeRoute('[{"wt":"'+wt.Id+'","wo:":"'+wo.Id+'","distance":1000,"duration":122}]', '');
        }catch(Exception ex){}
        
        dsmtDispatchConsoleController.getWorkorderForRouting(wt.Id, system.today().month()+'/'+system.today().day()+'/'+system.today().year());
        //dsmtDispatchConsoleController.getWorkTeamForRouting(wt.Id, system.today().month()+'/'+system.today().day()+'/'+system.today().year());
    
        //dsmtDispatchConsoleController.CheckAvailiability(system.today().month()+'/'+system.today().day()+'/'+system.today().year(),
        //                                               '08:00AM', 'Test');
        //dsmtDispatchConsoleController.CheckAvailiability(system.today().month()+'/'+system.today().day()+'/'+system.today().year(),
         //                                             '08:00PM', 'Test');
                                                      
        //dsmtDispatchConsoleController.deleteWorkoder(wo.Id);
        dsmtDispatchConsoleController.getWeeks(null, location.Id);

        
        //dsmtDispatchConsoleController.EmployessWrapper ew = new dsmtDispatchConsoleController.EmployessWrapper();
        dsmtDispatchConsoleController.WorkorderModel wm = new dsmtDispatchConsoleController.WorkorderModel();
        dsmtDispatchConsoleController.WorkorderModel2 wm2 = new dsmtDispatchConsoleController.WorkorderModel2();
        //dsmtDispatchConsoleController.WorkTeamOrderModal wtm =new dsmtDispatchConsoleController.WorkTeamOrderModal(wt.id,wo.id);
        //dsmtDispatchConsoleController.RA_UpdateSchedule(wo.Id,emp.Id,location.Id,system.today().month()+'',system.today().year()+'',system.today().day()+'','8','0','Test','Test');        
        Test.stopTest();
        
    }
    @istest
    static void runtest2()
    {

        /*Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Email__c='test@gmail.com';
        ta.Name = 'test';
        ta.First_Name__c='test';
        ta.Last_Name__c='test';
        ta.Phone__c='1111';
        insert ta;
        DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
        dsmtc.Name='test';
        dsmtc.Super_User__c=true;
        dsmtc.Trade_Ally_Account__c=ta.id;
        dsmtc.contact__c= u.contactId;
        dsmtc.Portal_User__c=userinfo.getUserId();
        insert dsmtc;*/
        
        Account acc = new Account();
        acc.Name = 'Energy NM';
        acc.Billing_Account_Number__c= 'Gas-~~~1234';
        acc.Utility_Service_Type__c= 'Gas';
        acc.Account_Status__c= 'Active';
        insert acc;

        Customer__c cust =new Customer__c();
        cust.Account__c=acc.Id;
        cust.Service_Address__c='test';
        cust.Service_City__c='test';
        cust.Service_State__c='CO';
        cust.Service_Postal_Code__c='test';
        insert cust;
        
        Location__c loc = new Location__c();
        loc.Name = 'Fresno';
        insert loc;
        
        Employee__c emp =new Employee__c();
        emp.Location__c=loc.id;
        emp.Status__c='Approved';
        emp.Employee_Id__c = 'test123';
        insert emp;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, emp.Id);
        wt.Service_Date__c = system.today();
        wt.Captain__c = emp.Id;
        wt.Default_Region__c='Boston';
        wt.Unavailable__c=false;
        wt.Start_Date__c=date.today();
        wt.End_Date__c=date.today();
        wt.Lunch_Start_Date__c=date.today();
        wt.Lunch_End_Date__c=date.today();
        insert wt;
        
        Workorder_Type__c wty = new Workorder_Type__c();
        wty.name='test';
        wty.Est_PreWork_Time__c=2;
        wty.Est_PostWork_Time__c=34;
        wty.Est_Work_Time__c=4;
        wty.Visit_Size__c='S';
        wty.Est_Deliverable_Time__c=65;
        wty.Visit_Size__c='';       
        insert wty;
        
        List<Note> noteToInsert = new List<Note>();
        Note__c n = new Note__c();
        n.Title__c = 'WO Alert Added/Changed';
        n.Body__c = 'test';
        insert n;
        
        Workorder__c work =new Workorder__c();
        work.Cancelled_date__c=date.today();
        work.Notes__c='test';
        work.Customer__c=cust.id;
        work.Workorder_Type__c=wty.id;
        work.Early_Arrival_Time__c=date.today();
        work.Late_Arrival_Time__c=date.today();
        work.Scheduled_Date__c=date.today();
        work.Alert__c = 'testing';
        //work.Work_Team__c=wt.id;
        insert work;

        Review__c rv = new Review__c();
        //rv.Energy_Assessment__c=ea.id;
        //rv.Project__c=pro.id;
        rv.Status__c='New';

        Eligibility_Check__c elc =new Eligibility_Check__c();
        //elc.Workorder_Type__c=wty.id;
        elc.How_many_do_you_own__c='1';
        elc.City__c='test';
        elc.Zip__c='test';
        elc.Gas_Account_Holder_First_Name__c='test';
        elc.Gas_Account_Holder_Last_Name__c='test';
        elc.Start_Date__c=date.today().addDays(-5);
        elc.End_Date__c=date.today().addDays(5);
        elc.First_Name__c='test';
        elc.Last_Name__c='test';
        elc.override_zip__c=true;
        elc.Override_Reason__c='Low_Income_Approved';
        elc.Do_you_heat_with_natural_Gas__c='Yes';
        elc.Electric_Account__c='Electric';
        elc.Gas_Account__c='Gas';
        insert elc;
        
        Scheduler__c sche =new Scheduler__c();
        sche.Location__c=loc.id;
        sche.Default_Location__c=true;
        sche.User__c=userinfo.getUserId();
        insert sche;
        
        ApexPages.currentPage().getParameters().put('custId',cust.Id);
        ApexPages.currentPage().getParameters().put('ReviewId',rv.id);
        ApexPages.currentPage().getParameters().put('WoTypeId',wty.id);
        ApexPages.currentPage().getParameters().put('worderId',work.Id);
        ApexPages.currentPage().getParameters().put('ElcId',elc.id);
        
        dsmtDispatchConsoleController ddcc =new dsmtDispatchConsoleController();
        ddcc.AppointmentId=work.id;
        ddcc.ChangeAppointment();
        ddcc.CancenAppointment();
        ddcc.getRegions();
        ddcc.getWorkTypeslist();
        ddcc.overrideAppt();
        dsmtDispatchConsoleController.verifiedAddress(work.Id, 'streetaddress', 'city', 'state', 'zip');
    dsmtDispatchConsoleController.deleteWorkoder(work.Id);
        dsmtDispatchConsoleController.EmployessWrapper ew = new dsmtDispatchConsoleController.EmployessWrapper();
        dsmtDispatchConsoleController.getWorkTeamForRouting(wt.Id, system.today().month()+'/'+system.today().day()+'/'+system.today().year());
        dsmtDispatchConsoleController.CheckAvailiability(system.today().month()+'/'+system.today().day()+'/'+system.today().year(),'08:00PM', 'Test');
        dsmtDispatchConsoleController.RA_UpdateSchedule(work.Id,emp.Id,loc.Id,system.today().month()+'',system.today().year()+'',system.today().day()+'','8','0','Test','Test');
    }
    @istest
    static void runtest()
    {
        Workorder_Type__c wtype = new Workorder_Type__c();
        wtype.name='test';
        wtype.Est_PreWork_Time__c=2;
        wtype.Est_PostWork_Time__c=34;
        wtype.Est_Work_Time__c=4;
        wtype.Visit_Size__c='S';
        wtype.Est_Deliverable_Time__c=65;
        wtype.Visit_Size__c='';       
        insert wtype;
        
        test.startTest();
        Skill__c skill =new Skill__c();
        skill.SampleData__c=true;
        insert skill;
        
        Required_Skill__c rskill =new Required_Skill__c();
        rskill.Workorder_Type__c=wtype.id;
        rskill.Skill__c=skill.id;
        insert rskill;
        
        Employee_Skill__c eskill =new Employee_Skill__c();
        eskill.Skill__c=skill.id;
        insert eskill;
        Account acc = new Account();
        acc.Name = 'Energy NM';
        acc.Billing_Account_Number__c= 'Gas-~~~1234';
        acc.Utility_Service_Type__c= 'Gas';
        acc.Account_Status__c= 'Active';
        insert acc;

        Customer__c cust =new Customer__c();
        cust.Account__c=acc.Id;
        cust.Service_Address__c='test';
        cust.Service_City__c='test';
        cust.Service_State__c='CO';
        cust.Service_Postal_Code__c='test';
        insert cust;
        
        Location__c loc = new Location__c();
        loc.Name = 'Fresno';
        insert loc;
        
        Employee__c emp =new Employee__c();
        emp.Location__c=loc.id;
        emp.Status__c='Approved';
        emp.Employee_Id__c = 'test123';
        insert emp;
        
        /*Work_Team__c wt = Datagenerator.CreateWorkTeam(loc.Id, emp.Id);
        wt.Service_Date__c = system.today();
        wt.Captain__c = emp.Id;
        wt.Default_Region__c='Boston';
        wt.Unavailable__c=false;
        wt.Start_Date__c=date.today();
        wt.End_Date__c=date.today();
        wt.Lunch_Start_Date__c=date.today();
        wt.Lunch_End_Date__c=date.today();
        insert wt;*/
        
        Workorder_Type__c wty = new Workorder_Type__c();
        wty.name='test';
        wty.Est_PreWork_Time__c=2;
        wty.Est_PostWork_Time__c=34;
        wty.Est_Work_Time__c=4;
        wty.Visit_Size__c='S';
        wty.Est_Deliverable_Time__c=65;
        wty.Visit_Size__c='';       
        insert wty;
        
        
        Work_Team__c wt = new Work_Team__c();
        wt.name = 'test work team';
        wt.Captain__c = emp.Id;
        insert wt;
        
        Work_Team_Member__c wtm = new Work_Team_Member__c();
        wtm.Work_Team__c = wt.id;
        wtm.Employee__c = emp.Id;
        insert wtm;
     List<Note> noteToInsert = new List<Note>();
        Note__c n = new Note__c();
        n.Title__c = 'WO Alert Added/Changed';
        n.Body__c = 'test';
        insert n;
       
        Workorder__c work =new Workorder__c();
        work.Cancelled_date__c=date.today();
        work.Notes__c='testing';
        work.Customer__c=cust.id;
        work.Workorder_Type__c=wty.id;
        work.Early_Arrival_Time__c=date.today();
        work.Late_Arrival_Time__c=date.today();
        work.Scheduled_Date__c=date.today();
        work.Work_Team__c = wt.id;
        work.Alert__c = 'test';
        insert work;

      
        /*Review__c rv = new Review__c();
        rv.Energy_Assessment__c=ea.id;
        //rv.Project__c=pro.id;
        rv.Status__c='New';*/

        /*Eligibility_Check__c elc =new Eligibility_Check__c();
        //elc.Workorder_Type__c=wty.id;
        elc.How_many_do_you_own__c='1';
        elc.City__c='test';
        elc.Zip__c='test';
        elc.Gas_Account_Holder_First_Name__c='test';
        elc.Gas_Account_Holder_Last_Name__c='test';
        elc.Start_Date__c=date.today().addDays(-5);
        elc.End_Date__c=date.today().addDays(5);
        elc.First_Name__c='test';
        elc.Last_Name__c='test';
        elc.override_zip__c=true;
        elc.Override_Reason__c='Low_Income_Approved';
        elc.Do_you_heat_with_natural_Gas__c='Yes';
        elc.Electric_Account__c='Electric';
        elc.Gas_Account__c='Gas';
        insert elc;*/
        
        Scheduler__c sche =new Scheduler__c();
        sche.Location__c=loc.id;
        sche.Default_Location__c=true;
        sche.User__c=userinfo.getUserId();
        insert sche;
        
        ApexPages.currentPage().getParameters().put('custId',cust.Id);
        //ApexPages.currentPage().getParameters().put('ReviewId',rv.id);
        ApexPages.currentPage().getParameters().put('WoTypeId',wty.id);
        ApexPages.currentPage().getParameters().put('worderId',work.Id);
        //ApexPages.currentPage().getParameters().put('ElcId',elc.id);
        
        dsmtDispatchConsoleController ddcc1 =new dsmtDispatchConsoleController();
        ddcc1.RescheduleWorkTypeId=wtype.id;
        ddcc1.SelectedWorkTeam='test';
        ddcc1.GetRescheudleOptions();
        ddcc1.Rescheduleworkorder();
        ddcc1.ChangeAppointment();
        ddcc1.changeWTId = 'test';
        ddcc1.view  = 'month';
        ddcc1.GetnewWorkTeams();
        test.stopTest();
    }
    
    @istest
    static void runtest3()
    {
       
        
        Location__c l = new Location__c();
        insert l;
        
        Employee__c emp = Datagenerator.createEmployee(l.Id);
        insert emp;
        
        Work_Team__c wt = Datagenerator.CreateWorkTeam(l.Id,emp.Id);
        insert wt;
        
        Workorder__c wo = Datagenerator.createWo();
        wo.Scheduled_Date__c = date.today();
        wo.Early_Arrival_Time__c = date.today();
        wo.Late_Arrival_Time__c = date.today();
        wo.Work_Team__c = wt.Id;
        insert wo;
        
        test.startTest();
        
        
        dsmtDispatchConsoleController dsmtDispatch = new dsmtDispatchConsoleController();
        dsmtDispatch.wodr = wo;
        dsmtDispatch.wrtmId = wt.Id;
        dsmtDispatch.changeWTId = wt.Id;
        dsmtDispatch.timeStr = '12:35';
        dsmtDispatch.EstdurHr = 1;
        dsmtDispatch.EstdurMn = 1;
        dsmtDispatch.queryWO();
        dsmtDispatch.ChangeAppointment();
        
        
    }
}