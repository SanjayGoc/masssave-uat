global class dsmtLocateEmployeeController{
    global Employee__c employee {get;set;}
    global Employee__c tempEmp {get;set;}
    global string empId {get;set;}
    global string woWTJson {get;set;}
    global string woJson {get;set;}
    global list<Workorder__c> woList {get;set;}
    set<string> woIncludeStatusSet = new set<string>{'Scheduled', 'Completed'};
    
    global dsmtLocateEmployeeController(ApexPages.StandardController controller) {
        init();
    }

    global dsmtLocateEmployeeController()
    {
        init();
    }
    
    private void init()
    {
        employee = new Employee__c();
        tempEmp = new Employee__c();
        tempEmp.Employment_Start_Date__c = system.today();
        woList = new list<Workorder__c>();
        empId = ApexPages.currentPage().getParameters().get('id');
        
        employee = fetchEmployee(empId);
        
        list<Battery_And_Geo_Info__c> geoInfoList = queryBatteryGeoInfos(empId);
        woWTJson = JSON.serialize(geoInfoList);
        
        woList = queryWorkOrders(empId);
        woJson = JSON.serialize(woList);
    }
    
    public void dateChangeLocate(){
        list<Battery_And_Geo_Info__c> geoInfoList = queryBatteryGeoInfos(empId);
        woWTJson = JSON.serialize(geoInfoList);
        
        woList = queryWorkOrders(empId);
        woJson = JSON.serialize(woList);
    }
    
    private list<Battery_And_Geo_Info__c> queryBatteryGeoInfos(string employeeId)
    {
        list<Battery_And_Geo_Info__c> returnList = new list<Battery_And_Geo_Info__c>();
        
        list<Battery_And_Geo_Info__c> geoInfoList =  [select id, Name, Bettery_Life__c, Date_Time__c, Device_Description__c, Device_Type__c,
                                                      Employee__c, Geo_coordinates__c, PIN__c, Geo_coordinates__latitude__s, Geo_coordinates__longitude__s
                                                      from Battery_And_Geo_Info__c
                                                      where Employee__c =: employeeId
                                                      and CreatedDate >=: tempEmp.Employment_Start_Date__c.addDays(-1)
                                                      and CreatedDate <=: tempEmp.Employment_Start_Date__c
                                                      order by CreatedDate
                                                      limit 100];
        
        
        for(integer i=0; i < geoInfoList.size(); i++)
        {
            if(i == 0){
                returnList.add(geoInfoList[i]);
            }
            else{
                if(geoInfoList[i-1].Geo_coordinates__latitude__s != geoInfoList[i].Geo_coordinates__latitude__s && 
                   geoInfoList[i-1].Geo_coordinates__longitude__s != geoInfoList[i].Geo_coordinates__longitude__s)
                    returnList.add(geoInfoList[i]);   
            }
        }    
        
        return returnList;
    }
    
    //Get current Location record
    private Employee__c fetchEmployee(string employeeId) {
        return [select id, Name, Address__c, City__c, 
                Zip__c from Employee__c where id =: employeeId
                order by name limit 1];
    }
    
    private list<Workorder__c> queryWorkOrders(string empId)
    {
        list<Workorder__c> woList = [select id, Name, Address_Formula__c
                                     from Workorder__c
                                     where Work_Team__r.Captain__c =: empId
                                     and Work_Team__r.Service_Date__c =: tempEmp.Employment_Start_Date__c
                                     and Status__c in : woIncludeStatusSet];
        
        return woList;
    }
}