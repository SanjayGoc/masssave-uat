public with sharing class dsmtDocumentGridControllerClone{
    
    public List<Attachment__c> lstAttachments{get;set;}
    Public String RecId {get;set;}
    
    Public boolean isProject;
    public boolean isEA;
    public boolean isReview;
    public boolean isRecommendation;
    public boolean isInvoice;
    public boolean isInvoiceLineItem;
    public boolean isInspectionRequest;
    public boolean isInspectionLineItem;
    public boolean isBarrier;
    public Review__c review;
    Public Profile Pro;
    
    Public List<AttachmentObj> attachObjlist{get;set;}
    Public List<AttachmentTypeObj> attachmentTypeObjlist{get;set;}
    
    
    public dsmtDocumentGridControllerClone(ApexPages.StandardController sc){
        
        isProject = false;
        isEA = false;
        isReview = false;
        isRecommendation = false;
        isInvoice = false;
        isInvoiceLineItem = false;
        isInspectionRequest = false;
        isInspectionLineItem = false;
        isBarrier = false;
        lstAttachments = new List<Attachment__c>();
        
        String type = sc.getRecord().getSObjectType().getDescribe().getName();
        if(type  == 'Recommendation_Scenario__c'){
            RecId = sc.getId();
            isProject = true;
        }
        if(type  == 'Energy_Assessment__c'){
            RecId = sc.getId();
            isEA = true;
        }
        
        if(type  == 'Review__c'){
            RecId = sc.getId();
            List<Review__c> reviews = [select id,recordType.DeveloperName from REview__C where id =: RecId];
            if(reviews.size() > 0){
                review = reviews.get(0);
            }
            isReview = true;
        }
        
        if(type  == 'Recommendation__c'){
            RecId = sc.getId();
            isRecommendation = true;
        }
        
        if(type  == 'Invoice__c'){
            RecId = sc.getId();
            isInvoice = true;
        }
        
        if(type  == 'Invoice_Line_Item__c'){
            RecId = sc.getId();
            isInvoiceLineItem = true;
        }
        
        if(type  == 'Inspection_Request__c'){
            RecId = sc.getId();
            isInspectionRequest = true;
        }
           
        if(type  == 'Inspection_Line_Item__c'){
            RecId = sc.getId();
            isInspectionLineItem = true;
        }
        
        if(type  == 'Barrier__c'){
            RecId = sc.getId();
            isBarrier = true;
        }
        
        
        getAllExistingAttachments();
        attachObjlist = new List<AttachmentObj>();
        Map<String, List<AttachmentObj>> attachmentObjMap = new Map<String, List<AttachmentObj>>();
        for(Attachment__c attach : lstAttachments){
            AttachmentObj attachobj = new AttachmentObj();
            attachObj.attachId = attach.Id;
            attachobj.Name = attach.Attachment_Name__c;
            attachObj.attachType = attach.Attachment_Type__c;
            attachObj.createDate = attach.CreatedDate.format();
            attachObj.lastmodDate = attach.LastModifiedDate.format();
            attachObj.downloadUrl = attach.File_Download_Url__c;
            attachObj.MergedDocumentNames = attach.Merged_Document_Names__c;
            attachObjlist.add(attachObj);

            if(attachmentObjMap.containsKey(attach.Attachment_Type__c)) {
                attachmentObjMap.get(attach.Attachment_Type__c).add(attachobj);
            } else{
                attachmentObjMap.put(attach.Attachment_Type__c, new List<AttachmentObj>{attachobj});
            }
        }

        attachmentTypeObjlist = new List<AttachmentTypeObj>();
        for(String attachtype : attachmentObjMap.keySet()){
            AttachmentTypeObj attachTypeobj = new AttachmentTypeObj();
            attachTypeobj.attachType = attachtype;
            attachTypeobj.downloadUrl = attachmentObjMap.get(attachtype).get(0).downloadUrl;
            attachTypeobj.noOfAttachments = attachmentObjMap.get(attachtype).size();
            attachTypeobj.attachmentObjList = attachmentObjMap.get(attachtype);
            attachmentTypeObjlist.add(attachTypeobj);

        }

    }
    
    public void getAllExistingAttachments(){
        
        //DSST-12143 add field Merged_Document_Names__c in all Attachment queary BY PP
        
        List<Profile> ProfileList = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId()];
        if(ProfileList.size() > 0){
            Pro = ProfileList.get(0);
        }
        
        List<Clone_And_Display_Attachments__c> cloneAttachments = Clone_And_Display_Attachments__c.getall().values();
        Map<String,List<String>> cloneTypeToAttachTypes = new Map<String,List<String>>();
        for(Clone_And_Display_Attachments__c cAttach : cloneAttachments){
           if(cAttach.Is_View__c){
               List<String> attachtypes = cloneTypeToAttachTypes.get(cAttach.Clone_Display_Type__c);
               if(attachtypes == null){
                    attachtypes = new List<String>();
               }
               attachtypes.add(cAttach.Attachment_Type__c);
               cloneTypeToAttachTypes.put(cAttach.Clone_Display_Type__c,attachtypes);
           }
           
        }
        
        system.debug('--pro.Name--'+pro.Name);
        List<String> attachTypes = cloneTypeToAttachTypes.get(Pro.Name);
        
        if(isProject){
            
            if(attachTypes != null && attachTypes.size() > 0){
                lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Project__c = : RecId AND Attachment_Type__c IN : attachTypes ORDER BY CreatedDate desc limit 999]; 
            }else{
                lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Project__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            }
            
        }        
        
        if(isEA ){
            
            if(attachTypes != null && attachTypes.size() > 0){
                lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Energy_Assessment__c = : RecId AND Attachment_Type__c IN : attachTypes ORDER BY CreatedDate desc limit 999];    
            }else{
                lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Energy_Assessment__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            }
        }
        
        if(isReview){
            if(review != null){
                String query = 'select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Id != null ';
                if(review.RecordType.developerName == 'Pre_Work_Review' ){
                    query += ' and Pre_work_review__c = : RecId ';
                }else if(review.RecordType.developerName == 'Work_Scope_Review' ){
                    query += ' and Work_Scope_Review__c = : RecId ';
                }else if(review.RecordType.developerName == 'Billing_Review' ){
                    query += ' and Billing_Review__c = : RecId ';
                }else if(review.RecordType.developerName == 'Post_Inspection_Result_Review'){
                    query += ' and Post_Inspection_Review__c = : RecId ';
                }else{
                   query += ' and Review__c = : RecId ';
                }
                
                
                if(attachTypes != null && attachTypes.size() > 0){
                   query += ' and Attachment_Type__c IN: attachTypes ';
                }
                
                query += 'ORDER BY CreatedDate desc limit 999';
                system.debug('--query--'+query);
                
                lstAttachments = Database.query(query);
            }
            
            
        }
        
        if(isRecommendation){
            
            lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Recommendation__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            
        }
        
        if(isInvoice){
            lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Invoice__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            
        }
        
        if(isInvoiceLineItem){
            lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Invoice_Line_Item__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            
        }
        
        if(isInspectionRequest){
            lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Inspection_Request__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            
        }
        
        if(isInspectionLineItem){
            lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Inspection_Line_Item__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            
        }
        
        if(isBarrier){
            lstAttachments = [select Id,Attachment_Name__c,Attachment_Type__c,CreatedDate,LastModifiedDate,File_Download_Url__c,Merged_Document_Names__c from Attachment__c where Barrier__c = : RecId ORDER BY CreatedDate desc limit 999]; 
            
        }
    }
    
    
    Public class AttachmentObj{
        public String attachId{get;set;}
        public String Name{get;set;}
        public String attachType{get;set;}
        public String createDate{get;set;}
        public String lastmodDate{get;set;}
        public String downloadUrl{get;set;}
        public String MergedDocumentNames{get;set;}
    }

    public class AttachmentTypeObj {
        public String attachType{get;set;}
        public Integer noOfAttachments{get;set;}
        public String downloadUrl{get;set;}
        public List<AttachmentObj> attachmentObjList {get;set;}
    }
}