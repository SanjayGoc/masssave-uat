@isTest
Public class dsmtbatchRegistrationRequestTest{
    @isTest
    public static void runTest(){
        
        Registration_Request__c reg = datagenerator.createRegistration();
        reg.Status__c = 'Registration Request Sent';
        update reg;
        
        reg = [ Select id,Name,Send_Inactive_Application_Date__c,Send_Cancelled_Application_Date__c,Status__c,LastModifiedDate,
                    Dsmtracker_Contact__r.Contact__c,Email__c,Trade_Ally_Account__c,Owner.Email,Account_Manager__c,Account_Manager__r.Email from 
                    Registration_Request__c where id =: reg.Id];
                    
        List<Registration_Request__c> regList = new List<Registration_Request__c>();
        regList.add(reg);
        
        dsmtbatchRegistrationRequest cntrl = new dsmtbatchRegistrationRequest();
        cntrl.execute(null);
        cntrl.execute(null,regList);
    }
}