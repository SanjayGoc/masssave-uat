@isTest
public class SendEmailOfferControllerTest {
    static TestMethod void test(){
        
       Account acc = DAtagenerator.createAccount();
        acc.Name = 'Test';
        acc.Billing_Account_Number__c = 'Gas~~~1234';
       insert acc;
       
       Contact con = new contact();
       con.AccountId = acc.Id;
       con.LastName = 'Do Not Delete';
       insert con;
        
       Premise__c pr = new Premise__c();
        insert pr;
        
        Customer__c cust = new Customer__c();
        cust.Account__c = acc.Id;
        cust.Last_Name__c = 'test';
        cust.Premise__c = pr.Id;
        cust.Email__c = 'test1@test.com';
        insert cust;
            
       Trade_Ally_Account__c ta = new Trade_Ally_Account__c();
        ta.Name = 'Test';
       insert ta;
       
        
        
        DSMTracker_Contact__c dsmt = new DSMTracker_Contact__c();
        dsmt.Trade_Ally_Account__c = ta.Id;
        dsmt.Active__c=true;
        dsmt.Contact__c = con.Id;
        dsmt.Email__c = 'Test@test.com';
        insert dsmt;
        
        Registration_Request__c reg = new Registration_Request__c(First_Name__c='hemanshu',Last_Name__c='patel',Email__c='test@test.com',Status__c='test',DSMTracker_Contact__c=dsmt.Id);
        insert reg;
        
        Service_Request__c sr3 = new Service_Request__c();
        sr3.Customer__c = cust.Id;
        sr3.DSMTracker_Contact__c = dsmt.id;
        insert sr3;
         
        Phase_I_Ticket_Detail__c ph = new Phase_I_Ticket_Detail__c();
        ph.Service_Request__c = sr3.Id;
        insert ph;  
        
        Attachment__c att = new Attachment__c();
        att.Registration_Request__c =reg.id;
        att.Attachment_Type__c = 'Initial M&V Specification'; 
        insert att;
        
        
              
        ApexPages.Currentpage().getParameters().put('rrid',reg.Id);
        ApexPages.Currentpage().getParameters().put('id',att.Id);
        ApexPages.Currentpage().getParameters().put('p1dId',ph.Id);
        ApexPages.currentPage().getParameters().put('mailType','Registration');
        SendEmailOfferController controller = new SendEmailOfferController();   
        controller.selTemplateId='00X4D000000QGdK';
        controller.emailSubject = 'test';
        controller.emailBody = 'test';
        controller.emailTo = 'test@test.com';
        controller.emailCC = 'tsset@test.com';
        controller.emailBCC= 'tsset3@test.com';
        controller.WhatId = reg.Id;
        controller.RRId = reg.Id;
        controller.SRID = sr3.Id;
        controller.DCID = dsmt.Id;
        controller.P1dId = ph.Id;
        controller.getEmailTemplatesByfolder();
        controller.loadContentForRegistrationRequest();
        controller.loadContentsForServiceRequest();
        controller.loadContentsForDsmTrackerContact();
        controller.loadContentForp1d();
        controller.fillAttachmentModel();
        controller.sendEmail();
        controller.fillHtmlBody();
        controller.init();
        controller.check_print_collateral_status();
        controller.cancel();
        controller.reloadPage();
        controller.regenerateOfferLetter();
        
    }
    
    static TestMethod void test2(){
         
       Account acc = DAtagenerator.createAccount();
        acc.Name = 'Test';
        acc.Billing_Account_Number__c = 'Gas~~~1234';
       insert acc;
       
       Contact con = new contact();
       con.AccountId = acc.Id;
       con.LastName = 'Do Not Delete';
       insert con;
        
       Premise__c pr = new Premise__c();
        insert pr;
        
        Customer__c cust = new Customer__c();
        cust.Account__c = acc.Id;
        cust.Last_Name__c = 'test';
        cust.Premise__c = pr.Id;
        cust.Email__c = 'test1@test.com';
        insert cust;
            
        

        
       Registration_Request__c reg = DAtagenerator.createRegistration();
       
        Attachment__c att = new Attachment__c(Registration_Request__c =reg.id);
        att.Attachment_Type__c = 'Offer Letter';
        insert att;
        
        DSMTracker_Contact__c dsmt = new DSMTracker_Contact__c();
        dsmt.Active__c=true;
        dsmt.Contact__c = con.Id;
        dsmt.Email__c = 'Test@test.com';
        insert dsmt;
        
        Service_Request__c sr3 = new Service_Request__c();
        sr3.Customer__c = cust.Id;
        sr3.DSMTracker_Contact__c = dsmt.id;
        insert sr3;
         
        Phase_I_Ticket_Detail__c ph = new Phase_I_Ticket_Detail__c();
        ph.Service_Request__c = sr3.Id;
        insert ph;     
        
      SendEmailOfferController cntrl = new SendEmailOfferController();
      cntrl.selTemplateId = '00X4D000000QGdK';
      cntrl.emailSubject = null;
      cntrl.emailBCC = null;
	  cntrl.emailBody = null;
      cntrl.emailCC = null;
      cntrl.emailTo = null;        
      cntrl.getEmailTemplatesByfolder();
      cntrl.loadContentForp1d();
      cntrl.loadContentForRegistrationRequest();
      cntrl.loadContentsForDsmTrackerContact();
      cntrl.loadContentsForServiceRequest();
      cntrl.sendEmail();        
        
    }
}