public class dsmtSignAndEmailDocumentController {

    public String eaId{get;set;}
    public string selectedDocuments {get;set;}
    public String templateID{get;set;}
    public String queryID{get;set;}
    public String newCustomAttachmentId{get;set;}
    public Integer counter{get;set;}
    public String docName{get;set;}
    public String attachmentIds{get;set;}
    public String query{get;set;}
    Public String stdAttachmentId{get;set;}
    Public Integer innerCounter{get;set;}
    Public Map<String, List<mechanical_sub_type__c>> mechsubtypeMap = new Map<String, List<mechanical_sub_type__c>>();
    Public Map<String, List<Appliance__c>> applianceMap = new Map<String, List<Appliance__c>>();
    Public Integer totalSubTypes{get;set;}
    Public String OFN{get;set;}
    public List<Attachment__c> attachments{get;set;}
    Public Boolean isHPC{get;set;}
    public String newAttachmentIds{get;set;}
    Public boolean clearMap{get;set;}
    public Energy_Assessment__c eaObj{get;set;}
    public String contractorInitials{get;set;}
    public List<Id> newCustomAttachmentIds;
    public String strAttachmentIds{get;set;}
    
    //DSST-12136 Start
    public Boolean isSingleSign{get;Set;}
    public Integer totalSelected{get;set;}
    public List<string> documentList;
    //DSST-12136 End
    public String isFromGeneratedoc{get;set;}  //DSST-12141
    
    public dsmtSignAndEmailDocumentController() {
        innerCounter = 0;
        totalSubTypes = 0;
        totalSelected = 0;
        eaId = ApexPages.currentPage().getParameters().get('Id');
        docName = ApexPages.currentPage().getParameters().get('docName');
        attachmentIds = ApexPages.currentPage().getParameters().get('attachIds'); 
        isFromGeneratedoc = ApexPages.currentPage().getParameters().get('isGeneratedoc');  //DSST-12141
        
        //DSST-12136 Start
        documentList = new List<string>();
        isSingleSign = false;
        String singleSign = ApexPages.currentPage().getParameters().get('singleSign'); 
        if(singleSign != null && singleSign != '')
        {
            isSingleSign = Boolean.valueOf(singleSign);
        }
        //DSST-12136 End
        strAttachmentIds = '';
        
        newCustomAttachmentIds = new List<Id>();        
        list<User> userList = [select id, Profile.Name from User where Id =: UserInfo.getUserId()];
        String profileName = userList[0].Profile.name;
        isHPC = profileName.contains('HPC');
        eaObj =  new Energy_Assessment__c();
        List<String> attachids;

        if(attachmentIds != null && attachmentIds.trim().length() > 0){
            attachids = attachmentIds.split(',');
            //DSST-12136 - Start
            attachments = [SELECT Id,Name,Attachment_Type__c,Attachment_Name__c,CreatedDate,Email_File_Download_URL__c, File_Url__c,Standard_Attachment_ID__c,Merged_Document_Names__c,(select id,name from Attachments) FROM Attachment__c WHERE Id in: attachids]; 
            //DSST-12136 - END
            if(attachments.size() > 0){
               Attachment__c attach = attachments.get(0);
               stdAttachmentId = attach.Standard_Attachment_Id__c;
               
               if(stdAttachmentId == null && attach.Attachments.size() > 0){
                  stdAttachmentId = attach.Attachments[0].Id;
               }

               //DSST-12136 Start
               if(isSingleSign)
                {
                    if(attach.attachment_Type__c == 'Merged Documents'){
                        documentList = attach.Merged_Document_Names__c.split(','); 
                        docName = attach.Merged_Document_Names__c;  
                    }
                    // DSST-14505 Start
                    else {
                        documentList.add(attach.Attachment_Type__c);
                    }
                    // DSST-14505 End
                } else {
                    documentList.add(attach.Attachment_Type__c);
                }

                totalSelected = documentList.size();
                //DSST-12136 End
                
            }
        }       
        newAttachmentIds = '';
        contractorInitials = '';
    }

    public void generateCongaDocument() {
        attachmentIds = '';
        String subtypeid = '';

        //DSST-12136 Start
        if(isSingleSign && documentList.size() > counter)
        {
            docName = documentList[counter];
        }
        //DSST-12136 ENd

        if(innerCounter == 0 || innerCounter < totalSubTypes) {
            if(docName == 'Enclosed Cavity Sheet') {
                //get Conga Template
                List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Enclosed Cavity Insulation Template'];
                templateID = templates.get(0).ID;
                
                //get Conga Query                 
                List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Enclosed Cavity Insulation Query']; 
                queryID = queries.get(0).ID;
                query = queryID+'?pv0='+eaId;
                totalSubTypes = 0;
            } else if(docName == 'Early CAC and CHP Rebate Form') {
                //DSST-8020 START
                if(clearMap) {
                    mechsubtypeMap.clear();
                    clearMap = false;
                }   

                //DSST-8020 END

                //get Conga Template
                List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Early CAC and CHP Rebate Form Template'];
                templateID = templates.get(0).ID;
                
                //get Conga Query
                if(innerCounter == 0)
                {
                    List<mechanical_sub_type__c> mechsubtypes = [SELECT Make__c, Model__c, Cooling_Tons__c,Heating_Tons__c, Tons__c,Year__c, Energy_Assessment__r.Appointment_Date__c, Energy_Assessment__r.Customer__c, Energy_Assessment__r.Energy_Advisor__c 
                                                                    FROM Mechanical_Sub_Type__c 
                                                                    WHERE Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId 
                                                                    AND RecordType.Name In ('ASHP','Central A/C') 
                                                                    AND Send_for_Early_Rebate_Eligibility__c = true];
                    if(mechsubtypes != null && mechsubtypes.size() > 0)
                    {
                        if(mechsubtypeMap.containsKey(eaId)==false)
                        {
                            totalSubTypes = mechsubtypes.size();
                            mechsubtypeMap.put(eaId, mechsubtypes);
                        }
                    }                  
                }
                List<sObject> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Early CAC and CHP Rebate Form Query','Early CAC and CHP Rebate Form Query 2') ORDER BY Unique_Conga_Query_Name__c DESC]; 
                
                queryID = '';
                for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries)
                {                
                    //system.debug('mechsubtypeMap.containsKey(eaId)--'+mechsubtypeMap.containsKey(eaId));
                    if(mechsubtypeMap.containsKey(eaId) == true)
                    {
                        List<mechanical_sub_type__c> mechsubTypes = (List<mechanical_sub_type__c>)mechsubtypeMap.get(eaId);
                        subtypeid = (mechsubTypes[innerCounter]).id;
                    }
                    
                    if(q.Unique_Conga_Query_Name__c == 'Early CAC and CHP Rebate Form Query 2') {
                        queryID += q.ID +'?pv0='+ subtypeid + ',';
                    } else {
                        queryID += q.ID +'?pv0='+ eaId + ',';
                    }
                }
                
                query = queryID.removeEnd(',');
                
                /**queryID = '';
                for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries){
                   queryID += q.ID +'?pv0='+ eaId +',';
                }
                
                query = queryID.removeEnd(',');*/
                 
            } else if(docName == 'Permit Authorization Form') {
    
                //get Conga Template
                List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Permit Authorization Customer Signoff Template'];
                templateID = templates.get(0).ID;
                
                //get Conga Query                 
                List<sObject> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Permit Authorization Customer Signoff Query')]; 
                queryID = '';
                for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries){
                   queryID += q.ID +'?pv0='+ eaId +',';
                }
                
                query = queryID.removeEnd(',');
                totalSubTypes = 0;
            } else if(docName == 'Specified Measures Agreement') {
                
                //get Conga Template
                List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Specified Measures Agreement Form Template'];
                templateID = templates.get(0).ID;
    
                //get Conga Query                 
                List<sObject> queries = [select Id,Unique_Conga_Query_Name__c  from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Specified Measures Agreement Form Query')]; 
                queryID = '';
                for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries){
                   queryID += q.ID +'?pv0='+ eaId +',';
                }
                
                query = queryID.removeEnd(',');
                totalSubTypes = 0;
            } else if(docName == 'Master Disclosure Form') {
                //get Conga Template
                List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Master Disclosure Form'];
                templateID = templates.get(0).ID;
                
                //get Conga Query                 
                List<sObject> queries = [select Id from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c = 'Master Disclosure Query']; 
                queryID = queries.get(0).ID;
                query = queryID+'?pv0='+eaId;
                totalSubTypes = 0;
            } 
            // DSST-8020 START
            else if(docName == 'Duct Seal Verification Form') { 
                if(clearMap) {
                    mechsubtypeMap.clear();
                    clearMap = false;
                }

                //get Conga Template
                List<sObject> templates = [select Id from APXTConga4__Conga_Template__c Where Unique_Template_Name__c ='Duct Seal Verification Form'];
                templateID = templates.get(0).ID;
                
                //get Conga Query
                if(innerCounter == 0)
                {

                    List<mechanical_sub_type__c> mechsubtypes = [SELECT Id,Name
                                                                        FROM Mechanical_Sub_Type__c 
                                                                        WHERE Mechanical_Type__r.Mechanical__r.Energy_Assessment__c = : eaId  
                                                                        AND RecordType.Name = 'Duct Distribution System' ];
                    if(mechsubtypes != null && mechsubtypes.size() > 0)
                    {
                        if(mechsubtypeMap.containsKey(eaId)==false)
                        {
                            totalSubTypes = mechsubtypes.size();
                            mechsubtypeMap.put(eaId, mechsubtypes);
                        }
                    }                  
                }
                
                //get Conga Query                 
                List<sObject> queries = [select Id,Unique_Conga_Query_Name__c from APXTConga4__Conga_Merge_Query__c Where Unique_Conga_Query_Name__c In ('Duct Seal Verification Form Query','Duct Seal Verification Form Query 1')]; 
                queryID = '';

                for(APXTConga4__Conga_Merge_Query__c q : (List<APXTConga4__Conga_Merge_Query__c>)queries)
                {                
                    //system.debug('mechsubtypeMap.containsKey(eaId)--'+mechsubtypeMap.containsKey(eaId));
                    if(mechsubtypeMap.containsKey(eaId) == true)
                    {
                        List<mechanical_sub_type__c> mechsubTypes = (List<mechanical_sub_type__c>)mechsubtypeMap.get(eaId);
                        subtypeid = (mechsubTypes[innerCounter]).id;
                    }
                    
                    if(q.Unique_Conga_Query_Name__c == 'Duct Seal Verification Form Query 1') {
                        queryID += q.ID +'?pv0='+ subtypeid + ',';
                    } else {
                        queryID += q.ID +'?pv0='+ eaId + ',';
                    }
                }
                
                query = queryID.removeEnd(',');
                 
            }
            //DSST-8020 END
            
        } else if(innerCounter == totalSubTypes){
            //DSST-12136 Start
            templateID = strAttachmentIds;
            query = '';
            newAttachmentIds = '';
            //DSST-12136 End
        } 
        
        OFN = docName + ' With Signature';
        system.debug('query --' + query); 
        Attachment__c newCustomAttachment = new Attachment__c();
        newCustomAttachment.Status__c = 'New';
        newCustomAttachment.Attachment_Name__c = docName+' - ' + eaId + ' - ' + subtypeid +' With Signature.pdf';
        newCustomAttachment.Energy_Assessment__c = eaId ;
        newCustomAttachment.Is_Signed_Attachment__c = true;
        newCustomAttachment.Generate_Document__c = true;
        
        insert newCustomAttachment;
        newCustomAttachmentId = newCustomAttachment.ID;
        newAttachmentIds += newCustomAttachmentId+',';
        
        //DSST-12136 Start
        if(totalSubTypes > 0)
        {

            if(innerCounter < totalSubTypes)
            {
                newCustomAttachmentIds.add(newCustomAttachmentId); 
            } else {
                totalSubTypes = 0;
                List<Attachment__c> childattachments = new List<Attachment__c>();
                for(Id attachmentId : newCustomAttachmentIds) {
                    childattachments.add(new Attachment__c(Id = attachmentId, Parent_attachment__c = newCustomAttachmentId));
                }
                
                if(!childattachments.isEmpty()) {
                    update childattachments;
                }
            }
        }
        //DSST-12136 End
        
        System.debug('templateID === '+ templateID + ' & '+query + ' & '+newCustomAttachmentId);
    } 

    public boolean enablePollar{get;set;}
    public Id congaAttachmentId {get;set;}
    public boolean pollarSuccess{get;set;}

    public void checkCongaStatus(){
        try{
            pollarSuccess = false;
            enablePollar = true;
            Attachment__c customAttachment = [SELECT Id,Name,Standard_Attachment_ID__c , (SELECT Id,Name FROM Attachments) FROM Attachment__c WHERE Id =: newCustomAttachmentId];
            if(customAttachment != null && customAttachment.Attachments.size() > 0) {
                pollarSuccess = true;
                enablePollar = false;
                
                DocumentServiceWSFile.clsSalesForceLoginDetail loginDetail = new DocumentServiceWSFile.clsSalesForceLoginDetail();
                loginDetail.SessionID = UserInfo.getSessionId();
                loginDetail.serverURL = Login_Detail__c.getInstance().Server_URL__c;
                loginDetail.orgID = Login_Detail__c.getInstance().Org_Id__c;
                loginDetail.userName = Login_Detail__c.getInstance().Attachment_User_Name__c;
                loginDetail.password = Login_Detail__c.getInstance().Attachment_User_Password__c;
                loginDetail.securityToken = Login_Detail__c.getInstance().Attachment_User_Security_Token__c;
                
                DocumentServiceWSFile.clsQueryStringParameters clsQueryStringParametersObj = new DocumentServiceWSFile.clsQueryStringParameters();
                clsQueryStringParametersObj.SessionID = UserInfo.getSessionId();
                clsQueryStringParametersObj.ServerURL = Login_Detail__c.getInstance().Server_URL__c;
                clsQueryStringParametersObj.OrgId  = Login_Detail__c.getInstance().Org_Id__c;
                clsQueryStringParametersObj.AttachmentId = newCustomAttachmentId;
                clsQueryStringParametersObj.SecurityKey = 'intelcore2duo';
                clsQueryStringParametersObj.AccountKey = 'nokia710lumia';
                clsQueryStringParametersObj.Replace = 'YES';
                clsQueryStringParametersObj.RecordId = eaId;
                clsQueryStringParametersObj.AttachmentLookupCol = 'Energy_Assessment__c';
                clsQueryStringParametersObj.AttachmentType = docName;

                stdAttachmentId = customAttachment.Attachments.get(0).Id;
                DocumentServiceWSFile.DocumentServiceWSFileSoap documentServiceWSFile = new DocumentServiceWSFile.DocumentServiceWSFileSoap();
                DocumentServiceWSFile.ArrayOfClsUploadProcessResult result = documentServiceWSFile.MigrateAttachment(clsQueryStringParametersObj, stdAttachmentId, 'attachment', loginDetail);
                
                //DSST-12136 Start
                if(totalSubTypes > 0)
                    strAttachmentIds += ',' + stdAttachmentId ;
                    
                //DSST-12136 End
                
                customAttachment.Standard_Attachment_Id__c = stdAttachmentId;
                update customAttachment;
                
                //DSST-12136 Start
                //DSST-8020 START -sanjay
                if  ( documentList.size() > 0 && counter == documentList.size() -1 
                        && ( 
                                (totalSubTypes > 0 && innerCounter >= totalSubTypes) ||  totalSubTypes == 0
                           )
                    )
                {
                    List<Attachment__c> attlist = new List<Attachment__c>();
                    attlist = [SELECT Id FROM attachment__c WHERE Energy_Assessment__c = :eaId and attachment_Type__c in ('Customer Signature')];
                    DELETE attlist;
                }
                
                //DSST-8020 END -sanjay
                
                //DSST-12136 End
                
                if(docName == 'Master Disclosure Form'){
                    Energy_Assessment__c ess = new Energy_Assessment__c(id=eaId);
                    ess.Participating_Contractor_Initials__c = '';
                    update ess;
                }
            }
        } catch(Exception ex) {
            enablePollar = false;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
    

    
    public PageReference Cancel(){
       String url = '/'+eaId;
       PageReference page = new PageReference(url);
       page.setRedirect(true);
       return page;

    }
    
    public PageReference SendEmail(){
       Pagereference pg = Page.SendEmail;
       pg.getParameters().put('Id', eaId);
       pg.getParameters().put('ObjectType', 'Energy_Assessment__c');
       pg.getParameters().put('mailType', docName);
       pg.getParameters().put('attachIds', String.valueOf(Attachment.Id));
       
       pg.setRedirect(true);
       return pg;

    }
    
   
    @RemoteAction
    public static String saveSign(String assessId, String imgURI1,String objectType,String initials){
        system.debug('-imgURI--'+imgURI1);
        
        try{
            Datetime now = Datetime.now();
            String datetimestr = now.format('MMddyyyyhhmm');
            dsmtCreateEAssessRevisionController.StopEATrigger = true;
            
                    
            List<Attachment__c> listattach = new List<Attachment__c>();
            
            Attachment__c cAtt = new Attachment__c();
            cAtt.Attachment_Name__c = 'Customer_Sign_'+datetimestr+'.png';
            cAtt.Application_Type__c = 'Custom';
            cAtt.Attachment_Type__c = 'Customer Signature';
            cAtt.Energy_Assessment__c = assessId;
            cAtt.Status__c = 'New';
            listattach.add(cAtt);
            
            insert listattach;
            
            Attachment sAttSign = new Attachment();
            sAttSign.ParentID = cAtt.id;
            sAttSign.Body = EncodingUtil.base64Decode(imgURI1);
            sAttSign.contentType = 'image/png';
            sAttSign.Name = 'Customer_Sign_'+datetimestr+'.png';
            insert sAttSign;
            
            Energy_Assessment__c ess = new Energy_Assessment__c(id=assessId);
            ess.Customer_Signature_Id__c = sAttSign.id;
            ess.Participating_Contractor_Initials__c = initials;
            
            // DSST-12136 Start
            if(objectType.contains('Permit Authorization Form')){    
                ess.Permit_Authorization_Form__c = true;
            }
            if(objectType.contains('Enclosed Cavity Sheet')){
                ess.Enclosed_Cavity_Sheet__c = true;
            }
            if(objectType.contains('Master Disclosure Form')){
                ess.Master_Disclosure_Form__c = true;
            }
            if(objectType.contains('Specified Measures Agreement')){
                ess.Specified_Measures_Agreement__c = true;
            }
            if(objectType.contains('Early CAC and CHP Rebate Form')){
                ess.Early_CAC_and_CHP_Rebate_Form__c = true;
            } 
            //DSST-8020 Start
            if(objectType.contains('Duct Seal Verification Form')){
                ess.Duct_Seal_Verification_Form__c = true;
            }
            
            //DSST-8020 END
            // DSST-12136 End   
            update ess;
            
            return 'Success~~~'+cAtt.Id+'~~~GenerateContract~~~';
           
        }catch(Exception ex){
            system.debug('--ex--'+ex);
            return 'Fail'+' : '+ex.getmessage();
        }        
    }
    
}