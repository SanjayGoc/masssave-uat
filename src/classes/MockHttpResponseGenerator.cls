@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        //System.assertEquals('http://example.com/example/test', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('[{"input_index":0,"candidate_index":0,"delivery_line_1":"1553 N 39th Rd","last_line":"Earlville IL 60518-6050","delivery_point_barcode":"605186050531","components":{"primary_number":"1553","street_predirection":"N","street_name":"39th","street_suffix":"Rd","city_name":"Earlville","state_abbreviation":"IL","zipcode":"60518","plus4_code":"6050","delivery_point":"53","delivery_point_check_digit":"1"},"metadata":{"record_type":"S","zip_type":"Standard","county_fips":"17099","county_name":"La Salle","carrier_route":"R002","congressional_district":"16","rdi":"Residential","elot_sequence":"0259","elot_sort":"A","latitude":41.49913,"longitude":-88.8689,"precision":"Zip9","time_zone":"Central","utc_offset":-6,"dst":true},"analysis":{"dpv_match_code":"Y","dpv_footnotes":"AABB","dpv_cmra":"N","dpv_vacant":"N","active":"Y"}}]');
        res.setStatusCode(200);
        return res;
    }
}