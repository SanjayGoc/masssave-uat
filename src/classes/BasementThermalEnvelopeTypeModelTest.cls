@istest
public class BasementThermalEnvelopeTypeModelTest
{
	@istest
    static void test()
    {
        Thermal_Envelope_Type__c tet =new Thermal_Envelope_Type__c();
        insert tet;
        
        Ceiling__c ce =new Ceiling__c();
        insert ce;  
        
        Floor__c fl = new Floor__c();
        insert fl;
        
        BasementThermalEnvelopeTypeModel btet =new BasementThermalEnvelopeTypeModel (tet);
        //btet.addNewBasementCeiling('Unfloored', 'exposedTo');  to many soql error will come 
       	btet.addNewBasementWall('Unfloored', 'exposedTo');
        //btet.addNewBasementFloor('Unfloored', 'exposedTo');
        
    }
}