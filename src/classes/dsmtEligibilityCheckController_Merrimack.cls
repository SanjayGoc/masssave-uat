public class dsmtEligibilityCheckController_Merrimack
{
    public string comments {get;set;}
    public Eligibility_Check__c custInteraction {get;set;}
    public string customerId {get;set;}
    
    public boolean ShowEligibleMessage{get;set;}
    public boolean Ineligiblefound{get;set;}
    
    public boolean isWorkorderError{get;set;}
    public String selectedunits {get;set;}
    public Customer__c cust {get;set;}
    
    public dsmtEligibilityCheckController_Merrimack()
    {
        init();
    }
    
    private void init()
    {
        isWorkorderError = false;
        custInteraction = new Eligibility_Check__c();
        cust = new Customer__c ();
        ShowEligibleMessage = Customer_Eligibility_Config__c.getOrgDefaults().Show_Eligible_Message__c;
        apptMap = new List<SelectOption>();
        Ineligiblefound = false;
        eleCheckId = ApexPages.currentPage().getParameters().get('ecid');
        selectedunits = '';
    }
    
    public List<SelectOption> gethowmanyunits() {
            List<SelectOption> options = new List<SelectOption>();
        
               Schema.DescribeFieldResult fieldResult =  Eligibility_Check__c.How_many_units__c.getDescribe();
              
               List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
               options.add(new SelectOption('', '--None--'));
               for( Schema.PicklistEntry f : ple)
               {
                  if(f.getValue() != '4 units' && f.getValue() != '5+ units' && f.getValue() != '3 units'){
                     if(f.getValue() == '3 units'){
                       String pickval = f.getValue();
                       String[] splitval = pickval.split(' '); 
                       options.add(new SelectOption(splitval.get(0)+'+ '+splitval.get(1), splitval.get(0)+'+ '+splitval.get(1)));  
                     }else{
                     options.add(new SelectOption(f.getLabel(), f.getValue()));
                     }
                   }
               }       
               return options;
        }
        
    
    
    
    @RemoteAction
    public static string LetsGo(){
    
        Eligibility_Check__c custInteraction = new Eligibility_Check__c();
        /*custInteraction.Purchase_a_discounted_smart_thermostat__c = therm;
        custInteraction.No_cost_home_energy_assessment__c = hes;
       */ custInteraction.Channel__c = 'Online';
        upsert custInteraction;
        system.debug('-----custInteraction.id-------'+custInteraction.id);
        return custInteraction.Id;
    }
    
    @RemoteAction
    public static Boolean checkZip(String zip) {
        List<Program_Eligibility__c> peList = [Select id, Zip_Code__c, Is_Merrimack_ZipCode__c from Program_Eligibility__c where Is_Merrimack_ZipCode__c = true and Zip_Code__c =: zip];
        system.debug('-------peList ----------'+peList );
        if(peList.size() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    public void SaveRecord(){
        custInteraction.Customer__c = customerId;
        upsert custInteraction;
    }
    
    
    
    public pageReference saveEligibility()
    {
        try{
             system.debug('--customerId---'+customerId);
            if(customerId != null && customerId != ''){
                custInteraction.Customer__c = customerId;
                List<Customer__c> custList = [select id,First_Name__c,Last_Name__c,Phone__c,Email__c,Service_ZipCode__c from customer__c
                                                    where id =: customerId];

                custInteraction.First_Name__c = custList.get(0).First_Name__c;
                custInteraction.Last_Name__c = custList.get(0).Last_Name__c;
                custInteraction.Email__c = custList.get(0).Email__c;
                custInteraction.Phone__c = custList.get(0).Phone__c;
                custInteraction.Zip__c = custList.get(0).Service_ZipCode__c ;
                
                if(custInteraction.Gas_Account_Holder_First_Name__c != null && custInteraction.Gas_Account_Holder_Last_Name__c != null){
                    DSMTracker_Contact__c dsmtc = new DSMTracker_Contact__c();
                    dsmtc.First_Name__c = custInteraction.Gas_Account_Holder_First_Name__c;
                    dsmtc.Last_Name__c = custInteraction.Gas_Account_Holder_Last_Name__c;
                    dsmtc.customer__c = customerId;
                    insert dsmtc;  
                }
            }
            upsert custInteraction;
         
            return null;//new Pagereference('/apex/ucsProspector');
        }catch(Exception ex){
            return null;
        }
    }
    
    Map<String,dsmtCallOut.AppointmentOption> apptMap1 = null;
    
    Map<Integer,Map<String,dsmtCallOut.AppointmentOption>> newapptmap = new map<Integer,Map<String,dsmtCallOut.AppointmentOption>>();
    
    public string WoTypeId{get;set;}
    public string SelectedEmployee{get;set;}
    public List<SelectOption> apptMap{get;set;}
    
    
    public List<Selectoption> employeeOption{get;set;}
   
  
    public void getEmployee(){
        //WoTypeId
       // woTypeId = 'a1F4D00000095qF';
        woTypeId = dsmtHelperClass.GetWorkOrderType(eleCheckId);
        
        if(woTypeId == null || woTypeId == ''){
            woTypeId = 'a1F4D00000095q2';
        }
        if(WoTypeId != null && WoTypeId != ''){
            list<Required_Skill__c> rsLst = [select id, Skill__c from Required_Skill__c where Workorder_Type__c =: WoTypeId];
            
            set<string> skillIds = new set<string>();
            
            for(Required_Skill__c r : rsLst){
                skillIds.add(r.Skill__c);
            }
            
            list<Employee_Skill__c> esLst = [select id, Skill__c,Employee__c,Employee__r.Name from Employee_Skill__c where skill__c in : skillIds];
    
            Set<String> empId = new Set<String>();
            employeeOption = new List<SelectOption>();
            
            for(Employee_Skill__c  es : esLst){
                if(empId.add(es.Employee__r.Name)){
                    if(es.employee__c != null){
                        employeeOption.add(new SelectOption(es.employee__c,es.Employee__r.Name));
                    }
                }
            }
        } 
        custInteraction.Start_Date__c = Date.Today();
        custInteraction.End_Date__c = Date.Today().Adddays(Integer.valueof(system.Label.Get_Appointment_Days_SelfScheduling)); 
        
       
    }
    
    public boolean isError{get;set;}
    public String AppterrorMsg{get;set;}
    public String TimeHours{get;set;}
    
    boolean IsOvertime = false;
     public string SessionId;
    
    public void getAppt(){  
        
        
        Integer duration = 0;
        
        Set<String> CustId = new Set<String>();
        
        String Address1 = '';
        String City1 = '';
        
        Set<String> woTypeIdLst = new Set<String>();
        
        woTypeId = label.Merrimack;//dsmtHelperClass.GetWorkOrderType(eleCheckId);
        
        if(Test.IsRunningTest()){
        woTypeId=[select id from Workorder_Type__c Limit 1].id;
        }    
        woTypeIdLst.add(woTypeId);
        
        if(woTypeId == null || woTypeId == ''){
            woTypeId = 'a1F4D00000095q2';
        }
        
        List<Workorder_Type__c> woTypeList = [select id,Est_Total_Time__c from Workorder_Type__c where id =: WoTypeId];
        
        duration = 0;
        
       // system.debug('--woTypeList.get(0).Est_Total_Time__c---'+woTypeList.get(0).Est_Total_Time__c);
        
        if(woTypeList != null && woTypeList.size() > 0){
            duration = Integer.valueOf(woTypeList.get(0).Est_Total_Time__c);
        }    
        else{
            duration = 120;
           
        }
        
        system.debug('---duration---'+duration);
        
        if(cust.Service_Street_Number__c != null){
            Address1 = cust.Service_Street_Number__c;
        }
        Address1  += cust.Service_Street__c ;
        City1 = cust.Service_City__c;
        
        system.debug('--duration ---'+duration );
        
        if(duration > 480){
            duration = 480;
            IsOvertime = true;
        }
        
        
        system.debug('--woTypeIdLst---'+woTypeIdLst);
        
        if(duration == 0){
            isError = true;
        }else{
        
        Decimal duration1 = duration/60;
        
        system.debug('---duration --'+duration1 );
        
        Integer i1 = Math.round(duration1);
        
        
        system.debug('--i1---'+i1);
        
        if( i1 == 0){
            TimeHours = ' up to one hour';  
        }else{
            TimeHours = ' up to '+i1 + ' hours';  
        }
       
        system.debug('--TimeHours ---'+TimeHours);
        
        if(custInteraction.Start_Date__c == null){
            custInteraction.Start_Date__c = Date.Today();
        }
        if(custInteraction.End_Date__c == null){
            custInteraction.End_Date__c = Date.Today().Adddays(Integer.valueof(system.Label.Get_Appointment_Days_SelfScheduling));
        }
        
        Date myDate = Date.newInstance(custInteraction.Start_Date__c.year(), custInteraction.Start_Date__c.month(), custInteraction.Start_Date__c.day());

        Date myDate1 = Date.newInstance(custInteraction.End_Date__c.year(), custInteraction.End_Date__c.month(), custInteraction.End_Date__c.day());
        
        
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        HTTPResponse res = new HTTPResponse();
        dsmtRouteParser.AddressParser obj1 = null;
        
        try{
        
        string address = EncodingUtil.urlEncode(Address1  +' '+City1, 'UTF-8');
        
        
        req.setEndpoint(System_Config__c.getInstance().URL__c+'GetAddress?type=json&address='+Address+'&orgId='+userinfo.getOrganizationId());
        
        
        req.setMethod('GET');
        req.setTimeOut(120000);
        
        http = new Http();
        if(!Test.isRunningTest()){
            res = http.send(req);
        }else{
         Map<String, String> boady2 = new Map<String, String>();
            boady2.put('status','test');
            boady2.put('address','test');
            boady2.put('City','test');
            boady2.put('latitude','1.32');
            boady2.put('longitude','12.4');
            boady2.put('formatedAddress','test');
            boady2.put('state_short','test');
            boady2.put('postalCode','test');
            String sbody = JSON.serialize(boady2);
            res.setBody(sbody);
            
        }
        
      //  System.debug(res.getBody());
        
        obj1 = (dsmtRouteParser.AddressParser) System.JSON.deserialize(res.getBody(), dsmtRouteParser.AddressParser.class);
        
        system.debug('--obj--'+obj1.formatedAddress);
        
        
        if(woTypeIdLst != null && woTypeIdLst.size() > 0){
           
            list<Required_Skill__c> rsLst = [select id, Skill__c,Skill__r.Name,Minimum_Score__c from Required_Skill__c where Workorder_Type__c In :woTypeIdLst ];
            
            set<string> skillIds = new set<string>();
            
            for(Required_Skill__c r : rsLst){
                skillIds.add(r.Skill__c);
            }
            
            system.debug('--skillIds---'+skillIds);
            
            list<Employee_Skill__c> esLst = [select id, Skill__c,Employee__c,Employee__r.Name,Survey_Score__c from Employee_Skill__c where skill__c in : skillIds];
    
            
            Map<Id,list<Employee_Skill__c>> esMap = new Map<Id,list<Employee_Skill__c>>();
            list<Employee_Skill__c> temp = null;
            
            for(Employee_Skill__c  es : esLst){
                    
                    if(esMap.get(es.Employee__c) != null){
                        temp = esMap.get(es.Employee__c);
                        temp.add(es);
                        esmap.put(es.Employee__c,temp);
                    }else{
                        temp = new list<Employee_Skill__c>();
                        temp.add(es);
                        esmap.put(es.Employee__c,temp);
                    }
            }
            system.debug('--esmap--'+esmap);
            Set<Id> empSkill = new Set<Id>();
            boolean add = false;
            Set<Id> empIdset = new Set<Id>();
            boolean reqadd = false;
            string strEmpId ='';
                        map<string, decimal> empSkillRateMap = new map<string, decimal>();

            
             for(Id empId : esMap.keyset()){
                add = false;
                temp = esmap.get(empId);
                decimal count = 0;
                for(Required_Skill__c r : rsLst){
                    reqadd = false;
                    for(Employee_Skill__c  es : temp){
                        if(es.Survey_Score__c >= r.Minimum_Score__c){
                            if(r.Skill__c == es.Skill__c){
                                count += es.Survey_Score__c;
                                reqadd = true;
                            }
                        }
                    }
                    if(reqadd == false){
                        break;
                    }
                }
                if(!reqadd){
                    add = false;
                }else{
                    add = true;
                }
                
                if(add){
                    empSkillRateMap.put(empId, count);
                }
            }
            
            system.debug('empSkillRateMap :::::'+ empSkillRateMap);
            
            if(empSkillRateMap.size() > 0){
                list<decimal> rateList = empSkillRateMap.values();
                rateList.sort();
                system.debug('rateList :::::'+ rateList);
                for(integer i = rateList.size()-1; i >= 0; i--){
                    for(string s : empSkillRateMap.keyset()){
                        if(rateList[i] == empSkillRateMap.get(s))
                        {
                            if(strEmpId  == ''){
                                strEmpId = s;
                            }else{
                                strEmpId += '~~~'+ s;
                            }
                        }
                    }
                }
            }
            
            
            SelectedEmployee = strEmpId ;
        }
        
        if(duration > 480){
            duration = 480;
            IsOvertime = true;
        }
        
        dsmtCallOut.StaticSessionId = SessionId;
        Integer minut = Integer.valueof(system.Label.Get_Appointment_Start_time_SelfScheduling);               
        apptMap1  =  dsmtCallOut.GetAppoitments(SelectedEmployee,myDate,myDate1,obj1.formatedAddress.replace('#',''),duration,null,null,minut);
        
        system.debug('--apptMap1---'+apptMap1);
        if(Test.IsRunningTest()){
            apptMap1 = new Map<String, dsmtCallOut.AppointmentOption>();
            dsmtCallOut.AppointmentOption ao = new dsmtCallOut.AppointmentOption();
            ao.serviceDate = '01/01/2018';
            ao.serviceTime = '06:06';
            ao.serviceDuration = '20';
            ao.earlyArrivalTime = '06:06 PM';
            ao.lateArrivalTime  = '06:06 PM';
            apptMap1.put('false', ao);//= '{a1X1g000000Vu92EAC~~~01:45 PM-02:15 PM~~~01:15 PM-02:15 PM on 10/10/2018=AppointmentOption:[city=, displaySlotText=01:45 PM-02:15 PM on Wednesday arriving between 01:15 PM to 02:15 PM, distanceText=1 m, distanceValue=0.0, drivingTime=1 min, drivingTimeInSeconds=900.0, earlyArrivalTime=01:15 PM, error=null, geoLatitude=42.434778, geoLongitude=-71.451025, lateArrivalTime=02:15 PM, postalCode=, serviceAddress=35 Acton St, Maynard, MA 01754, USA, serviceDate=10/10/2018, serviceDuration=30, serviceTime=13:45:00, serviceTimeSlot=01:45 PM-02:15 PM, sessionId=2018-09-22 06:02:50:183a0f41000008NnYMAA0, state=, workOrderRefId=7b6022c2-e6ee-4b82-a9bb-1d98c3fd7cb5, workTeamId=a1X1g000000Vu92EAC]}';
        }
        
        if(apptMap1.get('false') != null){
            apptMap = new List<SelectOption>();
            isError = true;
            dsmtCallOut.AppointmentOption obj = apptMap1.get('false');  
            system.debug('--obj.error---'+obj.error); 
            
            List<DSM_Tracker_Configuration_Line_Item__c> dsmtList = [select Config_Value__c,Config_HTML_Value__c from DSM_Tracker_Configuration_Line_Item__c where
                                                    Config_Name__c = 'No Appointment Found' and DSM_Tracker_Configuration__r.Configuration_Name__c = 'Program Eligibility'];
                                                     
            if(dsmtList != null && dsmtList.size() > 0){
                AppterrorMsg  = dsmtList.get(0).Config_Value__c;
            }
           // AppterrorMsg  = obj.error;
            AppterrorMsg = AppterrorMsg.replace('[[CUSTOMER PHONE]]',custinteraction.phone__c);
              List<call_List__c> callList = [select id from call_List__c where name = 'Requires Call Back Queue'];
                
                Call_List_Line_Item__c cli = new Call_List_Line_Item__c();
                
                if(callList != null && callList.size() > 0){
                    cli.Call_List__c = callList.get(0).Id;
                }
                cli.Call_Type__c = 'Outbound';
                
                List<Eligibility_Check__c> newecList = [select id,First_Name__c,Last_Name__c,Phone__c,Email__c from Eligibility_Check__c
                                                            where id =: eleCheckId];
                if(newecList  != null && newecList.size() > 0){
                    cli.Eligibility_Check__c = newecList.get(0).Id;
                    cli.First_Name_New__c = newecList .get(0).First_Name__c;
                    cli.Last_Name_New__c = newecList .get(0).Last_Name__c;
                    cli.Phone_New__c = newecList .get(0).Phone__c;
                    cli.Email_New__c = newecList .get(0).Email__c;
                }
                cli.Call_Notes__c = AppterrorMsg;
                cli.Status__c = 'Ready To Call';
                cli.Call_List_Type__c = 'Requires Call Backs';
                insert cli;
            //AppterrorMsg =   obj.error;
        }
        else{
            isError = false;
            system.debug('--SelectedEmployee---'+SelectedEmployee);
            
            system.debug('--apptMap1---'+apptMap1);
            apptMap = new List<SelectOption>();
            //boolean isSendEmail = false;
            //string strHtml = '<html><head><style>table.woCLS {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;} .woCLS td,.woCLS th {border: 1px solid #dddddd;text-align: left;padding: 8px;} .woCLS tr:nth-child(even) {background-color: #dddddd;}</style></head><table class="woCLS" border="1"><tr><th>Workorder #</th><th>Start Date</th><th>End Date</th></tr>';
            for(String str : apptMap1.keySet()){
                dsmtCallOut.AppointmentOption obj = apptMap1.get(str); 
                //boolean isAvailable = dsmtAvailableAppointment.getAvailableAppoit(obj); 
                string strRes = dsmtAvailableAppointment.getAvailableAppoit1(obj);
               if(strRes == 'success'){ 
                system.debug('--obj.SessionId--'+obj.SessionId);
                system.debug('--obj---'+obj);
                SessionId =  obj.SessionId;
                system.debug('--SessionId--'+SessionId );
                dsmtCallOut.StaticSessionId =  obj.SessionId;
                //apptMap.add(new SelectOption(str,obj.earlyArrivalTime+' - '+obj.LateArrivalTime + ' on '+ obj.ServiceDate));   
               // apptMap.add(new SelectOption(str,obj.displaySlotText.split('on').get(1)+ ' on '+ obj.ServiceDate)); 
               
                String timeslotDescription = dsmtArrivalWindowCalculator.buildTimeslotDescription(obj.serviceDate, obj.serviceTime); 
                system.debug('--timeslotDescription----'+timeslotDescription);
                system.debug('--str----'+str);
                
                apptMap.add(new SelectOption(str, timeslotDescription));  
              }else{
                  //isSendEmail = true;
                  //strHtml +=strRes;
              }
                 
            }
            
        system.debug('---apptMap---'+apptMap);
        //getEmployee();
        }
        
        }catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Address Is Invalid');
            ApexPages.addMessage(myMsg);
           // return null;
        }
        }
    }

    
    public string SelectedWorkTeam{get;set;}
    
    public void CreateWorkOrder(){
       try{
        Set<Id> setCustId = new Set<Id>(); //Added by Eshan DSST-3351
        isWorkorderError = false;
        list<Eligibility_Check__c> tempEcList = [select id,How_many_units__c,Customer__c,Do_you_own_or_rent__c from Eligibility_Check__c where id =: eleCheckId];
        
        if(tempEcList != null && tempEcList.size() > 0){
            CustomerId = tempecList.get(0).Customer__c;
        }
        if(apptMap1 != null){
            dsmtCallOut.AppointmentOption obj = apptMap1.get(SelectedWorkTeam);   
            system.debug('--obj ---'+obj);
            Integer year = integer.valueOf(obj.serviceDate.split('/').get(2));
            Integer month = integer.valueOf(obj.serviceDate.split('/').get(0));
            Integer day  = integer.valueOf(obj.serviceDate.split('/').get(1));
            Integer hour = integer.valueOf(obj.serviceTime.split(':').get(0));
            Integer min = integer.valueOf(obj.serviceTime.split(':').get(1));
            
            datetime StartDate = datetime.newInstance(year,month,day,hour,min,0);
            datetime EndDate = StartDate.AddMinutes(integer.valueOf(obj.serviceDuration));
            
            datetime ArrvStartDate = null;
            String str = '';
            
            if(obj.earlyArrivalTime != null){
                if(obj.earlyArrivalTime.contains('AM')){
                    str = obj.earlyArrivalTime.split(' ').get(0);
                    ArrvStartDate = datetime.newInstance(year,month,day,integer.valueOf(str.split(':').get(0)),integer.valueOf(str.split(':').get(1)),0);
                }else{
                    str = obj.earlyArrivalTime.split(' ').get(0);
                    hour = integer.valueOf(str.split(':').get(0));
                    min = integer.valueOf(str.split(':').get(1));
                    if( hour > 12){
                        hour += 12;    
                    }
                    ArrvStartDate = datetime.newInstance(year,month,day,hour,min,0);
                }
            }
            system.debug('--ArrvStartDate ---'+ArrvStartDate);
            
            datetime LateArrvDate = null;
            str = '';
            
            if(obj.lateArrivalTime != null){
                if(obj.lateArrivalTime.contains('AM')){
                    str = obj.lateArrivalTime.split(' ').get(0);
                    LateArrvDate = datetime.newInstance(year,month,day,integer.valueOf(str.split(':').get(0)),integer.valueOf(str.split(':').get(1)),0);
                }else{
                    str = obj.lateArrivalTime.split(' ').get(0);
                    hour = integer.valueOf(str.split(':').get(0));
                    min = integer.valueOf(str.split(':').get(1));
                    if( hour > 12){
                        hour += 12;    
                    }
                    LateArrvDate = datetime.newInstance(year,month,day,hour,min,0);
                }
            }
            
           
                Workorder__c wo = new Workorder__c();
                wo.Requested_Start_Date__c = StartDate;
                wo.Requested_End_Date__c = EndDate;
                wo.Requested_Date__c = StartDate.Date();
                wo.Early_Arrival_Time__c = ArrvStartDate;
                wo.Late_Arrival_Time__c = LateArrvDate;
                
                wo.Scheduled_Start_Date__c = StartDate;
                wo.Scheduled_End_Date__c = EndDate;
                wo.Scheduled_Date__c = wo.Requested_Date__c;
                if(!Test.IsrunningTest()){
                    wo.Work_Team__c = obj.workTeamId;
                }
                wo.Eligibility_Check__c = eleCheckId;
                
                List<Customer__c> customerList = [select id,Service_Address_Full__c,Service_City__c,Service_State__c,Service_Postal_Code__c
                            from customer__c where Id =: customerId];
                
                if(customerList != null && customerList.size() > 0){
                    //wo.Address__c = customerList.get(0).Service_Address_Full__c;
                    wo.City__c = customerList.get(0).Service_City__c;
                    wo.State__c = customerList.get(0).Service_State__c;
                    wo.Zipcode__c = customerList.get(0).Service_Postal_Code__c;
                    wo.Zip__c = customerList.get(0).Service_Postal_Code__c;
                    
                    wo.customer__c = customerList.get(0).Id;
                }
                wo.Address__c = custInteraction.Service_Address__c;
                
                
                List<Work_Team__c> wtList = [select id,Captain__r.DSMTracker_Contact__r.Trade_Ally_Account__c,location__c from work_Team__c where id =: obj.workTeamId];
                
                if(wtList != null && wtList.size() > 0){
                    wo.location__c = wtList.get(0).location__c;
                    wo.Trade_Ally_Account_New__c = wtList.get(0).Captain__r.DSMTracker_Contact__r.Trade_Ally_Account__c;
                }
                wo.Status__c = 'Scheduled';
                if(woTypeId != null && woTypeId != ''){
                    wo.WorkOrder_Type__c = woTypeId;
                }
                if(obj.serviceDuration != null && obj.serviceDuration != ''){
                    wo.Duration__c = double.valueOf(obj.serviceDuration);
                }
                if(custinteraction.Phone__c != null){
                    wo.Phone__c = custinteraction.Phone__c;
                }
                wo.External_Reference_ID__c = obj.workOrderRefId;
                wo.Flexible__c = custInteraction.Customer_is_Flexible__c;
                wo.Do_Not_Call__c = custInteraction.Do_Not_Call__c;
                wo.Notes__c = comments; //Ticket 229
                insert wo;
                List<Eligibility_Check__c> ecList = [select id,Status__c from Eligibility_Check__c where id =: eleCheckId];
            
                if(ecList != null && ecList.size() > 0){
                    ecList.get(0).Status__c = 'Passed';
                    update ecList;
                }
                
                Customer_Interaction__c ci = new Customer_Interaction__c();
                ci.Customer__c = wo.customer__c;
                ci.Eligibility_Check__c = eleCheckId;
                ci.Type__c = 'Online Eligibility check';
                ci.Interaction_Date_Time__c = Datetime.now();
                
                String dtConverted = DateTime.now().format('MM/dd/yyyy h:mm a');
            
                ci.Interaction_Notes__c = 'Customer check eligibility on ' ;
                ci.Interaction_Notes__c += dtConverted;
                ci.Interaction_Notes__c += ' and eligibility check passed';
                insert ci;
                
                List<Workorder__c> woList = [select id,name,Work_Team__r.Name,Workorder_Type__r.Name from workOrder__c where id =: wo.Id];
                
                ci = new Customer_Interaction__c();
                ci.Customer__c = wo.customer__c;
                ci.Eligibility_Check__c = eleCheckId;
                ci.Type__c = 'Work Order Created';
                ci.Interaction_Date_Time__c = Datetime.now();
                ci.Interaction_Notes__c = 'Work Order ' + woList.get(0).Name + ' of type '+ woList.get(0).Workorder_Type__r.Name+' has been scheduled for ' ;
                dtConverted = wo.Scheduled_Start_Date__c.format('MM/dd/yyyy h:mm a');
                ci.Interaction_Notes__c += dtConverted;
                ci.Interaction_Notes__c += ' For ' +woList.get(0).work_team__r.Name;
                insert ci;
                dsmtCallOut.ScheduleWorkOrder(obj.workOrderRefId,wo.Id,wo.Scheduled_Date__c,obj.workTeamId,obj.serviceTimeSlot);
            
            
            
            
            

        }
        }catch(Exception e){
           system.debug('--e---'+e);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.label.Invalid_Workorder_Message);
            ApexPages.addMessage(myMsg);
            isWorkorderError = false;
        }
    }
    public string eleCheckId{get;set;}
    public string ErrorMsg{get;set;}
    public void updateRecord(){
        custinteraction.Id = eleCheckId;
        upsert custinteraction;
        cust.id = customerId;
        cust.First_name__c = custinteraction.First_Name__c;
        cust.Last_name__c = custinteraction.Last_Name__c;
        cust.Phone__c = custinteraction.Phone__c ;
        cust.Email__c = custinteraction.Email__c ;
        upsert cust;
        customerId = cust.Id;
        custinteraction.Customer__c = customerId;
        update custinteraction;
    }
    
    
    
    public void EligibilityFaliRecord(){
        custinteraction.Id = eleCheckId;
        custinteraction.status__c = 'Failed';
        custinteraction.Eligibility_Failure_Message__c = ErrorMsg;
        upsert custinteraction;
        
        Customer_Interaction__c ci = new Customer_Interaction__c();
        ci.Customer__c = custinteraction.customer__c;
        ci.Eligibility_Check__c = eleCheckId;
        ci.Type__c = 'Online Eligibility check';
        ci.Interaction_Date_Time__c = Datetime.now();
        
        String dtConverted = DateTime.now().format('MM/dd/yyyy h:mm a');
    
        ci.Interaction_Notes__c = 'Customer check eligibility on ' ;
        ci.Interaction_Notes__c += dtConverted;
        ci.Interaction_Notes__c += ' and eligibility check failed';
        ci.Status__c = 'Unsuccessful Interaction';
        insert ci;
    }
    
    public string  multiFamilyResult{get;set;}
    
    
    Map<String,String> ErrorMap = new Map<String,String>();
   
     public String htmlToText(String htmlString) {
        String RegEx = '(</{0,1}[^>]+>)';
        if (htmlString == null)
            htmlString = '';
        return htmlString.replaceAll('&nbsp', '').replaceAll(';', '').replaceAll(RegEx, '');
    }
}