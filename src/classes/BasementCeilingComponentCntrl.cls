public with sharing class BasementCeilingComponentCntrl{
    public Id basementCeilingId{get;set;}
    public String newLayerType{get;set;}
    public Ceiling__c basementCeiling{get{
        if(basementCeiling == null){
            List<Ceiling__c> ceilings = Database.query('SELECT ' + getSObjectFields('Ceiling__c') + ',Thermal_Envelope_Type__r.SpaceConditioning__c FROM Ceiling__c WHERE Id=:basementCeilingId'); 
            if(ceilings.size() > 0){
                basementCeiling = ceilings.get(0);
            }
        }
        return basementCeiling;
    }set;}
    public void saveCeiling(){
        saveCeilingLayers();
        if(basementCeiling != null && basementCeiling.Id != null){
            basementCeiling.Exposed_To__c = basementCeiling.Roof_To__c;
            UPDATE basementCeiling;
            basementCeiling = null;
        }
    }
     public list<SelectOption> getLayerTypeOptions(){
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = Layer__c.Type__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(new SelectOption(f.getValue(),f.getLabel()));
        }   
        return options;
    }
    public List<Layer__c> ceilingLayers{get;set;}
    public Map<Id,Layer__c> getCeilingLayersMap(){
        Id ceilingId = basementCeiling.Id;
        String query = 'SELECT ' + getSObjectFields('Layer__c') + ',RecordType.Name,Thermal_Envelope_Type__r.RecordType.Name FROM Layer__c WHERE Ceiling__c =:ceilingId AND HideLayer__c = false';
        ceilingLayers = Database.query(query);
        return new Map<Id,Layer__c>(ceilingLayers);
    }
    public void saveCeilingLayers(){
        if(ceilingLayers != null && ceilingLayers.size() > 0){
            UPSERT ceilingLayers;
        }
    }
    public void addNewCeilingLayer(){
        String layerType = ApexPages.currentPage().getParameters().get('layerType');
        Map<String,Object> m = Schema.SObjectType.Layer__c.getRecordTypeInfosByName();
        Id recordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId();
        List<Layer__c> layers = [SELECT Id,Name FROM Layer__c WHERE Ceiling__c =:basementCeiling.Id AND RecordTypeId =:recordTypeId];
        if(layerType != null && m.containsKey(layerType)){
            Decimal nextNumber= layers.size() + 1;
            Layer__c newLayer = new Layer__c(
                Name = layerType + ' Layer ' + nextNumber,
                Type__c = layerType,
                Ceiling__c = basementCeiling.Id,
                RecordTypeId = Schema.SObjectType.Layer__c.getRecordTypeInfosByName().get(layerType).getRecordTypeId()
            );
            INSERT newLayer;
            if(newLayer.Id != null){
                basementCeiling.Next_Basement_Ceiling_Layer_Number__c = nextNumber + 1;
                UPDATE basementCeiling;
            }
            ceilingLayers.add(newLayer);
        }
    }
    public void deleteCeilingLayer(){
        Integer index = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
        DELETE new Layer__c(Id = ceilingLayers.get(index).Id);
        ceilingLayers.remove(index);
        saveCeiling();
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }
}