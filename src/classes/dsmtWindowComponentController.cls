public class dsmtWindowComponentController {
    public Id windowId{get;set;}
    
    public Window__c window{get{
        if(window == null){
            window = new Window__c();
        }
        return window;
    }set;}
    
    public dsmtWindowComponentController(){
        if(windowId != null){
            String query = 'SELECT '+ getSObjectFields('Window__c') + ' FROM Window__c WHERE Id=:windowId';
            List<Window__c> windows = Database.query(query);
            if(windows.size() > 0){
                window = windows.get(0);
            }
        }
    }
    
    private static Schema.DescribeSObjectResult dsr = Window__c.sObjectType.getDescribe();
    private static Schema.FieldSet getFieldSet(String fieldSetName){
        Schema.FieldSet fset = null;
        Map<String,Schema.FieldSet> fsMap = dsr.fieldSets.getMap();
        for(String key : fsMap.keySet()){
            if(key.equalsIgnoreCase(fieldSetName)){
                fset = fsMap.get(fieldSetName);
            }
        }
        return fset;
    }
    private static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        List<String> fields = new List<String>();
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            fields.add(dfield.getName());
        }
        return String.valueOf(fields).replace('(','').replace(')','');
    }
}