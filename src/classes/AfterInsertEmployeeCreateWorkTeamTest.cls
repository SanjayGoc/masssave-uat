@isTest
public class AfterInsertEmployeeCreateWorkTeamTest {
    static testmethod void testmethod1(){
        
        Test.startTest();
        
        Schedules__c sc = new Schedules__c(Name='test',Start_Time__c='08:00 AM',End_Time__c='05:00 AM');
        insert sc;
        
        List<Schedule_Line_Item__c> sclList = new  List<Schedule_Line_Item__c>();
        Schedule_Line_Item__c sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,End_Time__c='05:00 AM',Schedule_Week__c='WEEK A',Start_Time__c='08:00 AM',WeekDay__c='Monday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK A',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Tuesday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK A',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Wednesday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK A',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Friday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK A',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Saturday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK A',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Sunday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK B',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Monday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK B',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Tuesday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK B',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Wednesday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK B',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Friday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK B',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Saturday');
        sclList.add(sli);
        sli = new Schedule_Line_Item__c(Schedules__c=sc.Id,Schedule_Week__c='WEEK B',End_Time__c='05:00 AM',Start_Time__c='08:00 AM',WeekDay__c='Sunday');
        sclList.add(sli);
        
        insert sclList;
       
        Location__c loc = new Location__c(Name='test');
        insert loc;
        
        Employee__c ec = new Employee__c(Schedule__c=sc.Id, Location__c=loc.Id,Employee_Id__c='EMP0010',Active__c=true,Status__c = 'Active');
        insert ec;
            
        
   
        Work_Team__c wt = new Work_Team__c(Captain__c=ec.Id);
        insert wt;
        
        Workorder__c wo = new Workorder__c(work_team__c=wt.Id,Status__c='Unscheduled');
        wo.Early_Arrival_Time__c = Datetime.now();
        wo.Late_Arrival_Time__c = Datetime.now();
        insert wo;
        
        
        ec.Schedule__c = null;
        ec.Schedule__c = sc.Id;
        update ec;
        
		delete wt;
        
        Test.stopTest();
    }
    
    
   
}