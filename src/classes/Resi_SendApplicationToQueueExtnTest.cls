@isTest
public class Resi_SendApplicationToQueueExtnTest
{
    public testmethod static void test1()
    {
        
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'IIC User' LIMIT 1];
        ID rectypeid = Schema.SObjectType.review__c.getRecordTypeInfosByName().get('Post-Inspection Result Review').getRecordTypeId();
        User usr = new User(LastName = 'testlastname',
                            FirstName='testfirstname',
                            Alias = 'lasttest',
                            Email = 'first.last@qsdf.com',
                            Username = 'first.last@qsdf.com',
                            ProfileId = profileId.id,
                            TimeZoneSidKey = 'GMT',
                            LanguageLocaleKey = 'en_US',
                            EmailEncodingKey = 'UTF-8',
                            LocaleSidKey = 'en_US'
                           );
        insert usr;
        
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Contact_First_Name__c='ABC';
        //ir.Contractor_Name__c=ta.id;
        ir.Inspection_Result__c='Pass';
        insert ir;
        
        Review__c rev = new Review__c();
        rev.Inspection_Request__c=ir.id;
        rev.type__c='Pre Work Review';
        rev.Status__c='Batched';
        rev.Fail_Reason__c='Missing Signature~~~Missing Paperwork';
        rev.Failure_Notes__c = 'test';
        rev.Notes__c='test';
        rev.OwnerId = usr.id;
        rev.RecordTypeId = rectypeid;       
        insert rev;
        
        rev.Status__c='Scheduled';
        update rev;
        rev.Status__c = 'Customer contacted';
        update rev;
        rev.Status__c='Passed Review';
        update rev;
        
        test.startTest();
        review__c rev1 = new review__c();
        rev1.Failure_Notes__c='test';
        rev1.Notes__c='test';
        rev1.Status__c='Passed Review';
        insert rev1;
        
        System.RunAs(usr) 
        {
            ApexPages.StandardController sc = new ApexPages.StandardController(rev);
            Resi_SendApplicationToQueueExtn testAccPlan = new Resi_SendApplicationToQueueExtn(sc);
            testAccPlan.noteTitle ='test';
            testAccPlan.noteBody='blankBody';
            testAccPlan.selectedStatus='NoStatus';
            testAccPlan.conts = 'noConts';
            testAccPlan.ResubmitAndSched = false;
            testAccPlan.schedOnly = false;
            
            testAccPlan.newrev=rev1;
            testAccPlan.saveAndUpdate();  
            testAccPlan.Cancel();  
            test.stopTest();
        }
    }
    @istest
    static void dsmtBUReviewChangeOwnertesting()
    {
        ID rectypeid = Schema.SObjectType.review__c.getRecordTypeInfosByName().get('Post-Inspection Result Review').getRecordTypeId();
        
        Trade_Ally_Account__c taa =new Trade_Ally_Account__c();
        taa.Internal_Account__c=false;
        taa.Name='test';
        insert taa;
        
        Inspection_Request__c ir =new Inspection_Request__c();
        ir.Contact_First_Name__c='ABC';
        ir.Inspection_Result__c='Pass';
        insert ir;
        
        
        Review__c rev = new Review__c();
        rev.Trade_Ally_Account__c=taa.id;
        rev.Inspection_Request__c=ir.id;
        rev.type__c='Billing Review';
        rev.Status__c='Failed Completion Review';
        rev.Fail_Reason__c='Missing Signature~~~Missing Paperwork';
        rev.Failure_Notes__c = 'test';
        rev.Notes__c='test';
        rev.RecordTypeId = rectypeid;       
        insert rev;
        
        test.startTest();
        rev.Status__c = 'Customer contacted';
        update rev;    
        rev.Status__c='Passed Review';
        update rev;
        
        review__c rev1 = new review__c();
        rev1.Trade_Ally_Account__c=taa.id;
        rev1.Failure_Notes__c='test';
        rev1.Type__c='Billing Review';
        rev1.Status__c='Failed Completion Review Remedied';
        rev1.Notes__c='test';
        rev1.Billing_Review__c=rev.id;
        insert rev1;
        
        rev1.Status__c='Passed Admin Review';
        update rev1;
        
        test.stopTest();
    }
}