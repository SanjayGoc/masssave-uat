global class ReportsToCsv implements Schedulable {


//   public static String CRON_EXP = '0 0 0 3 9 ? 2022';
   global void execute(SchedulableContext ctx) {
               List<Report_Configuration__c> mcs = Report_Configuration__c.getall().values();    
               List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>(); 
                for(Report_Configuration__c rc:mcs)
                {
                    system.debug('----id---'+rc);        
                    ApexPages.PageReference objPage = new ApexPages.PageReference('/'+rc.Report_Id__c+'?csv=1');
                    
                    List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                    Messaging.EmailFileAttachment objMsgEmailAttach = new Messaging.EmailFileAttachment();
                    objMsgEmailAttach.setFileName(rc.Report_Name__c+'.csv');
                    if(!test.isRunningTest())
                    {
                    objMsgEmailAttach.setBody(objPage.getContent());
                    }
                    objMsgEmailAttach.setContentType('text/csv');
                    fileAttachments.add(objMsgEmailAttach);
                    
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.toAddresses = new String[] {Label.Boomi_Report_Email};
                    mail.setSaveAsActivity(false);
                    mail.subject = rc.Report_name__c;
                    mail.setFileAttachments(fileAttachments);
                    mail.plainTextBody = 'This is the message body.';
                    allmsg.add(mail);
                    
            }
            Messaging.sendEmail(allmsg,false);
       }
}