@istest
public class CrawlspaceThermalEnvelopeTypeModelTest
{
	@istest
    static void runtest()
    {
        Thermal_Envelope_Type__c tet =new Thermal_Envelope_Type__c ();
        insert tet;
        Ceiling__c ce =new Ceiling__c ();
        insert ce;
        
        CrawlspaceThermalEnvelopeTypeModel ctet =new CrawlspaceThermalEnvelopeTypeModel(tet);
        ctet.setEnvelopeType(tet);
        ctet.addNewCrawlspaceCeiling('Unfloored', 'exposedTo');
        ctet.updateCrawlspaceCeilingById(ce.id);
    }
}