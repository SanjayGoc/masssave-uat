global class dsmtSyncBatteryGeoInfoToSFDCScheduler implements Schedulable , Database.AllowsCallouts {

   global void execute(SchedulableContext SC) {
   
         //call .net service syncToMobile 
         dsmtFieldToolCallout.ProcessSyncBatteryGeoInfoToSFDC();
   }
}