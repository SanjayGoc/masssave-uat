@istest
public class AutoCreateInvoiceScheduledTest 
{
    /*public static testMethod void testschedule()
    {
        Test.StartTest();
        
        Invoice__c i = new Invoice__c();
        insert i;
		
        
        AutoCreateInvoiceScheduled acis =new AutoCreateInvoiceScheduled ();
        acis.invoicetype();
        acis.execute(sc);
    }*/
	@istest
    static void runTest()
    {
        Invoice__c i = new Invoice__c();
        i.Invoice_Type__c = 'NSTAR413';
        i.Work_Type__c ='ISM';
  
        insert i;
        
        InvoiceType__c it =new InvoiceType__c();
        it.TypeName__c ='test';
        it.Name ='test';
        insert it;
        
        AutoCreateInvoiceScheduled acis =new AutoCreateInvoiceScheduled ();
        acis.invoicetype();
        acis.createInvoice(it);
    }
    
}