public class LightingTriggerHandler extends TriggerHandler implements ITrigger{
    
    public void bulkBefore() {
        if (trigger.isInsert) {
            //Here we will call before insert actions
            calculateDefaultValues(trigger.new, (Map<Id, Lighting__c>) trigger.oldmap);
        } else if (trigger.isUpdate) {
            //Here we will call before update actions
            calculateDefaultValues(trigger.new, (Map<Id, Lighting__c>) trigger.oldmap);
        } else if (trigger.isDelete) {
            //Here we will call before delete actions
        } else if (trigger.isUndelete) {
            //Here we will call before undelete actions
        }
    }

    public void bulkAfter() {
        if (trigger.isInsert) {
            //Here we will call after insert actions
        } else if (trigger.isUpdate) {
            //Here we will call after update actions
        } else if (trigger.isDelete) {
            //Here we will call after delete actions
        } else if (trigger.isUndelete) {
            //Here we will call after undelete actions
        }
    }
    
    private static void calculateDefaultValues(list<Lighting__c> newList, Map<Id, Lighting__c> oldMap)
    {
        Set<String> eaIdSet = new Set<String>();
        Set<Id> eaID = new Set<Id>();
        for(Lighting__c a : newList)
        {
            if(a.IsFromFieldTool__c) continue;
            eaIdSet.add(a.Energy_Assessment__c);
            eaId.add(a.Energy_Assessment__c);
        }

        if(eaIdSet==null || eaIdSet.size() ==0) return;
        
        dsmtEAModel.Surface bp  = dsmtEAModel.InitializeBuildingProfile(eaID);
        bp.temp = dsmtTempratureHelper.ComputeTemprature(bp);
        bp.mo = dsmtBuildingModelHelper.ComputeBuildingModel(bp);

        Map<Id, Lighting_Location__c> lsList = null;
        if(bp.allLightingLocationMap !=null)
            lsList = bp.allLightingLocationMap.get(bp.EaObj.Id);
            
        for(Lighting__c a : newList)
        {
            bp.Efficiency = 1;
            bp.FuelType = 'Electricity';
            bp.FuelUnits = 'kWh';
            bp.genLigtObj = a;
            
            if(a.Name == 'General Lighting')
            {
                if(a.General_Indoor_Lighting__c ==null)
                    a.General_Indoor_Lighting__c = surfaceConstants.DefaultIndoorLightingUsage; 
                    
                if(a.General_Outdoor_Lighting__c==null)
                    a.General_Outdoor_Lighting__c= surfaceConstants.DefaultOutdoorLightingUsage; 
                    
                a.Efficient_Lighting_Fraction__c = dsmtLightingHelper.ComputeEfficientLightingFractionCommon(a);
                
                a.DefaultIndoorLighting__c = dsmtLightingHelper.ComputeDefaultIndoorLightingCommon(bp.bs, a);
                
                a.DefaultOutdoorLighting__c = dsmtLightingHelper.ComputeDefaultOutdoorLightingCommon(bp.bs, a);
                
            
                if(oldMap==null)
                    a.Has_Outdoor_Fixtures__c = true;
                    
                if(a.Has_Outdoor_Fixtures__c == false)
                {
                    a.General_Outdoor_Lighting__c = 'None';
                    a.General_Outdoor_Lighting_kWh_yr__c = 0;
                }
                
                
                bp.Location = 'Living Space';
                a.IndoorRegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');
                a.IndoorRegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');
                
                bp.Location = 'Exterior';
                a.OutdoorRegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'H');
                a.OutdoorRegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(bp, 'C');
            
                if(a.ActualDefaultIndoorLighting__c == null)
                    a.RemainingIndoorLighting__c = dsmtLightingHelper.ComputeRemainingIndoorLightingCommon(a,lsList);

                if(a.ActualDefaultIndoorLighting__c == null)
                    a.RemainingOutdoorLighting__c = dsmtLightingHelper.ComputeRemainingOutdoorLightingCommon(a,lsList);

                a.KWHPerYear__c = dsmtLightingHelper.ComputeDefaultLightingKWhPerYearCommon(a);                

                a.IndoorLoad__c = dsmtLightingHelper.ComputeDefaultLightingLoadCommon(a,'Indoor');
                a.OutDoorLoad__c = dsmtLightingHelper.ComputeDefaultLightingLoadCommon(a,'OutDoor');
                a.Load__c = dsmtLightingHelper.ComputeDefaultLightingLoadCommon(a, 'Both');

                bp.Load = a.IndoorLoad__c;

                a.IndoorEnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'H');
                a.IndoorEnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'C');
                a.IndoorEnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'Sh');
                
                bp.Load = a.Load__c;
                
                a.EnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'H');
                a.EnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'C');
                a.EnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(bp, 'Sh');
                
                bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
                bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
                bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;
            
                a.FuelConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'H');
                a.FuelConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'C');
                a.FuelConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(bp, 'Sh');
                
                a.AnnualEnergyConsumption__c =a.Load__c;
                a.AnnualFuelConsumption__c =a.KWHPerYear__c;

                bp.Load = a.IndoorLoad__c;
                bp.EnergyConsumptionH = a.IndoorEnergyConsumptionH__c;
                bp.EnergyConsumptionC = a.IndoorEnergyConsumptionC__c;
                bp.EnergyConsumptionSh = a.IndoorEnergyConsumptionSh__c;

                a.IndoorGainPlugC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'C');
                a.IndoorGainPlugH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'H');
                a.IndoorGainPlugCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'CLat');

                a.IndoorGainC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'C');
                a.IndoorGainH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'H');
                a.IndoorGainCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(bp,'CLat');

                bp.Load = a.Load__c;
                bp.Efficiency =1;

                bp.EnergyConsumptionH = a.EnergyConsumptionH__c;
                bp.EnergyConsumptionC = a.EnergyConsumptionC__c;
                bp.EnergyConsumptionSh = a.EnergyConsumptionSh__c;

                date dt = Date.today();
                bp.FuelUsage = dsmtEnergyConsumptionHelper.GetFuelUsageList(bp, dt);

                a.AnnualOperatingCost__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectAnnualOperatingCostCommon(bp);
            }
        }
    }
    
    public static String getSObjectFields(String sObjectApiName){
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(sObjectApiName).getDescribe().fields.getMap();
        String fields = '';
        Integer i = 0;
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            System.debug(dfield.getName());
            fields += dfield.getName();
            i++;
            if(i < fieldMap.size()){
                fields += ',';
            }
        }
        return fields;
    }

    public static Lighting__c UpdateDefaultLight(dsmtEAModel.Surface newBP)
    {
        Lighting__c a = newBP.genLigtObj;

        //system.debug(' lighting Energy_Assessment__c ' + a.Energy_Assessment__c);

        Map<Id, Lighting_Location__c> lsList = null;
        if(newBP.AllLightingLocationMap !=null)
            lsList = newBP.AllLightingLocationMap.get(a.Energy_Assessment__c);

        //system.debug(' lighting location list ' + lsList);
        
        a.Efficient_Lighting_Fraction__c = dsmtLightingHelper.ComputeEfficientLightingFractionCommon(a);
                
        a.DefaultIndoorLighting__c = dsmtLightingHelper.ComputeDefaultIndoorLightingCommon(newBP.bs, a);
                
        a.DefaultOutdoorLighting__c = dsmtLightingHelper.ComputeDefaultOutdoorLightingCommon(newBP.bs, a);

        newBP.Location = 'Living Space';
        a.IndoorRegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(newBP, 'H');
        a.IndoorRegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(newBP, 'C');
        
        newBP.Location = 'Exterior';
        a.OutdoorRegainFactorH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(newBP, 'H');
        a.OutdoorRegainFactorC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectRegainFactorCommon(newBP, 'C');
    
        if(a.ActualDefaultIndoorLighting__c == null)
            a.RemainingIndoorLighting__c = dsmtLightingHelper.ComputeRemainingIndoorLightingCommon(a,lsList);

        if(a.ActualDefaultIndoorLighting__c == null)
            a.RemainingOutdoorLighting__c = dsmtLightingHelper.ComputeRemainingOutdoorLightingCommon(a,lsList);

        a.KWHPerYear__c = dsmtLightingHelper.ComputeDefaultLightingKWhPerYearCommon(a);                

        a.IndoorLoad__c = dsmtLightingHelper.ComputeDefaultLightingLoadCommon(a,'Indoor');
        a.OutDoorLoad__c = dsmtLightingHelper.ComputeDefaultLightingLoadCommon(a,'OutDoor');
        a.Load__c = dsmtLightingHelper.ComputeDefaultLightingLoadCommon(a, 'Both');

        newBP.Load = a.IndoorLoad__c;
        newBP.Efficiency =1;

        a.IndoorEnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(newBP, 'H');
        a.IndoorEnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(newBP, 'C');
        a.IndoorEnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(newBP, 'Sh');

        newBP.Load = a.IndoorLoad__c;
        newBP.EnergyConsumptionH = a.IndoorEnergyConsumptionH__c;
        newBP.EnergyConsumptionC = a.IndoorEnergyConsumptionC__c;
        newBP.EnergyConsumptionSh = a.IndoorEnergyConsumptionSh__c;

        a.IndoorGainPlugC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(newBP,'C');
        a.IndoorGainPlugH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(newBP,'H');
        a.IndoorGainPlugCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(newBP,'CLat');

        a.IndoorGainC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(newBP,'C');
        a.IndoorGainH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(newBP,'H');
        a.IndoorGainCLat__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectIndoorGainPlugCommon(newBP,'CLat');

        newBP.Load = a.Load__c;
        
        a.EnergyConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(newBP, 'H');
        a.EnergyConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(newBP, 'C');
        a.EnergyConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectEnergyConsumptionPartCommon(newBP, 'Sh');
        
        newBP.EnergyConsumptionH = a.EnergyConsumptionH__c;
        newBP.EnergyConsumptionC = a.EnergyConsumptionC__c;
        newBP.EnergyConsumptionSh = a.EnergyConsumptionSh__c;
    
        a.FuelConsumptionH__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(newBP, 'H');
        a.FuelConsumptionC__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(newBP, 'C');
        a.FuelConsumptionSh__c = dsmtEnergyConsumptionHelper.ComputeBuildingProfileObjectFuelConsumptionPartCommon(newBP, 'Sh');
        
        a.AnnualEnergyConsumption__c =a.Load__c;
        a.AnnualFuelConsumption__c =a.KWHPerYear__c;
        
        return a;
    }
}