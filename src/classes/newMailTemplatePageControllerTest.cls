@istest
public class newMailTemplatePageControllerTest
{
	@istest
    static void test()
    {
        Contact con = new Contact();
        con.FirstName = 'Nitin';
        con.LastName = 'Kakadiya';
        con.Email = 'nitin.kakadiya@clearesult.com';
        insert con;
        
        DSMTracker_Contact__c dsmtcontact = new DSMTracker_Contact__c();
        dsmtcontact.Contact__c =  con.id;
        insert dsmtcontact;
        
        Invoice__c iv =new Invoice__c();
        iv.DSMTracker_Contact__c = dsmtcontact.id;
        insert iv;
        
        Attachment__c att = new Attachment__c();
        att.Invoice__c = iv.id;
        insert att;
        
        
        ApexPages.currentPage().getParameters().put('folderId','12345');
        ApexPages.currentPage().getParameters().put('invid',iv.id);
        ApexPages.currentPage().getParameters().put('toId','nitin.kakadiya@clearesult.com');
        ApexPages.currentPage().getParameters().put('ccEmails','pradip.patel@clearesult.com');
        newMailTemplatePageController  tpc =new newMailTemplatePageController ();
        tpc.subject = 'test subject';
        tpc.addMessage('Msg');
        tpc.sendEmail();
        tpc.cancel();
        tpc.reloadPage();
        tpc.fillhtmlbody();
        
    }
    
}